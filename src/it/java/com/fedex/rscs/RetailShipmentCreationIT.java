package com.fedex.rscs;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fedex.common.cxs.dto.CXSEnvelope;
import com.fedex.rscs.controller.RetailShipmentCreationController;
import com.fedex.rscs.dto.AccountType;
import com.fedex.rscs.dto.Address;
import com.fedex.rscs.dto.AddressClassification;
import com.fedex.rscs.dto.AppName;
import com.fedex.rscs.dto.ConfirmShipmentRequestData;
import com.fedex.rscs.dto.ConfirmedShipmentRequest;
import com.fedex.rscs.dto.ConfirmedShipmentResponse;
import com.fedex.rscs.dto.CreateOpenShipmentRequest;
import com.fedex.rscs.dto.CreateOpenShipmentRequestData;
import com.fedex.rscs.dto.CreateOpenShipmentResponse;
import com.fedex.rscs.dto.CreditCard;
import com.fedex.rscs.dto.Currency;
import com.fedex.rscs.dto.CustomerContact;
import com.fedex.rscs.dto.CustomerEmailDetail;
import com.fedex.rscs.dto.DeleteOpenShipmentResponse;
import com.fedex.rscs.dto.DeleteSoldShipmentRequest;
import com.fedex.rscs.dto.DeleteSoldShipmentRequestData;
import com.fedex.rscs.dto.DeletedSoldShipmentResponse;
import com.fedex.rscs.dto.DimensionUnit;
import com.fedex.rscs.dto.Dimensions;
import com.fedex.rscs.dto.ImageType;
import com.fedex.rscs.dto.InterlineShippingDetail;
import com.fedex.rscs.dto.LabelSpecification;
import com.fedex.rscs.dto.LabelType;
import com.fedex.rscs.dto.LocationDetail;
import com.fedex.rscs.dto.LocationInfo;
import com.fedex.rscs.dto.OpenShipmentIdentifier;
import com.fedex.rscs.dto.PackageSpecialHandlingType;
import com.fedex.rscs.dto.Party;
import com.fedex.rscs.dto.PaymentSource;
import com.fedex.rscs.dto.PaymentType;
import com.fedex.rscs.dto.Payor;
import com.fedex.rscs.dto.Person;
import com.fedex.rscs.dto.PersonName;
import com.fedex.rscs.dto.PhoneNumber;
import com.fedex.rscs.dto.PhoneNumberDetail;
import com.fedex.rscs.dto.Price;
import com.fedex.rscs.dto.PrintingOrientation;
import com.fedex.rscs.dto.RequestHeaderDetail;
import com.fedex.rscs.dto.RequestedPackageLineItem;
import com.fedex.rscs.dto.RequestedPaymentType;
import com.fedex.rscs.dto.RequestedShipment;
import com.fedex.rscs.dto.ResponsibleParty;
import com.fedex.rscs.dto.ShipmentCheckoutRequest;
import com.fedex.rscs.dto.ShipmentCheckoutRequestData;
import com.fedex.rscs.dto.ShipmentCheckoutResponse;
import com.fedex.rscs.dto.ShipmentPaymentOption;
import com.fedex.rscs.dto.ShipmentQueryType;
import com.fedex.rscs.dto.ShipmentSpecialServiceDescription;
import com.fedex.rscs.dto.ShippingContact;
import com.fedex.rscs.dto.ShippingPaymentDetail;
import com.fedex.rscs.dto.SoldShipmentResponse;
import com.fedex.rscs.dto.SoldShipmentSummaryResponse;
import com.fedex.rscs.dto.StockType;
import com.fedex.rscs.dto.TrackingSequence;
import com.fedex.rscs.dto.Weight;
import com.fedex.rscs.dto.WeightSource;
import com.fedex.rscs.dto.WeightUnit;
import com.fedex.rscs.dto.WorkstationDetails;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {TAZTokenRetrieval.class}, initializers = {TestInitializer.class})
public class RetailShipmentCreationIT {

    @Value("${integration-test.uri.retail-shipment-creation-service}")
    private String serviceUri;

    private RestTemplate template;

    @Autowired
    private TAZTokenRetrieval tazGetter;

    String tazToken;

    private static final String CREATE_OPEN_SHIPMENTS_ENDPOINT_URI = "/retailshipping/fedexoffice/v1/openshipments";
    private static final String RETRIEVE_OPEN_SHIPMENTS_ENDPOINT_URI = "/retailshipping/fedexoffice/v1/openshipments/";
    private static final String CONFIRM_SHIPMENT_URI = "/retailshipping/fedexoffice/v1/confirmedshipments";
    private static final String SHIPMENT_CHECKOUTS_ENDPOINT_URI = "/retailshipping/fedexoffice/v1/shipmentcheckouts";
    private static final String DELETE_OPEN_SHIPMENTS_ENDPOINT_URI = "/retailshipping/fedexoffice/v1/openshipments/";
    private static final String RETRIEVE_SOLD_SHIPMENTS_ENDPOINT_URI = "/retailshipping/fedexoffice/v1/locations/{locationId}/soldshipments?id={id}&type={type}";
    private static final String RETRIEVE_SOLD_SHIPMENT_SUMMARIES_ENDPOINT_URI = "/retailshipping/fedexoffice/v1/locations/{locationId}/soldshipmentsummaries?operationaldate={operationaldate}";
    private static final String DELETE_SOLD_SHIPMENTS_ENDPOINT_URI = "/retailshipping/fedexoffice/v1/deletesoldshipments";
    
    @Before
    public void setUp()
            throws Exception {

        template = new RestTemplate();

        tazToken = tazGetter.getJWT();
        assertNotNull(tazToken);

        HttpEntity<String> entity = new HttpEntity<>(new HttpHeaders());
        String healthCheckURI = serviceUri + "/health";

        // If the target service is down, usually this line will throw an exception and your test
        // will stop here
        ResponseEntity<String> pingResponse = template.exchange(healthCheckURI, HttpMethod.GET, entity, String.class);

        // Did we get a 200 from the health endpoint?
        assertNotNull("Server is not up, or not responding to health check at: " + healthCheckURI, pingResponse);
        assertEquals("Server is up but not responding with a 200 to health check", HttpStatus.OK,
                pingResponse.getStatusCode());
    }

    /**
     * Utility method to create a header with a TAZ token Makes the code more readable
     *
     * @param tazToken
     * @return
     */
    private static HttpHeaders createHeaders(
            String tazToken) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "Bearer " + tazToken);
        return headers;
    }

    /**
     * Method to build the mock instance of {@link CreateOpenShipmentRequest} object
     * 
     * @return
     */
    private CreateOpenShipmentRequest buildCreateOpenShipmentRequest() {

        CreateOpenShipmentRequest createOpenShipmentRequest = new CreateOpenShipmentRequest();
        createOpenShipmentRequest.setCreateOpenShipmentRequest(buildCreateOpenShipmentRequestData());

        return createOpenShipmentRequest;

    }

    /**
     * Method to build the mock instance of {@link CreateOpenShipmentRequestData} object
     * 
     * @return
     */
    private CreateOpenShipmentRequestData buildCreateOpenShipmentRequestData() {

        CreateOpenShipmentRequestData createOpenShipmentRequestData = new CreateOpenShipmentRequestData();
        createOpenShipmentRequestData.setHeaderDetails(buildRequestHeaderDetail());
        createOpenShipmentRequestData.setLocationDetails(buildLocationDetail());
        createOpenShipmentRequestData.setRequestedShipment(buildRequestedShipment());
        createOpenShipmentRequestData.setPaymentOption(buildShipmentPaymentOptions());
        createOpenShipmentRequestData.setInterlineShippingDetails(buildInterlineShippingDetail());
        createOpenShipmentRequestData.setLabelSpecification(createMockLabelSpecification());
        createOpenShipmentRequestData.setCustomer(buildPerson("Mr. Customer"));

        return createOpenShipmentRequestData;
    }

    /**
     * Method to build the mock instance of {@link ShipmentPaymentOption} object
     * 
     * @return
     */
    private ShipmentPaymentOption buildShipmentPaymentOptions() {

        ShipmentPaymentOption shipmentPaymentOption = new ShipmentPaymentOption();
        shipmentPaymentOption.setPaymentSource(PaymentSource.SPOS);
        shipmentPaymentOption.setPaymentType(RequestedPaymentType.ACCOUNT);
        shipmentPaymentOption.setResponsibleParty(ResponsibleParty.SENDER);
        shipmentPaymentOption.setAccountNumber("038000004");
        shipmentPaymentOption.setInterlineId("100127");

        return shipmentPaymentOption;
    }

    /**
     * Method to build the mock instance of {@link ShippingPaymentDetail} object
     * 
     * @return
     */
    private ShippingPaymentDetail buildShipmentPaymentDetails() {

        ShippingPaymentDetail shippingPaymentDetail = new ShippingPaymentDetail();
        shippingPaymentDetail.setPaymentSource(PaymentSource.SPOS);
        shippingPaymentDetail.setPaymentType(PaymentType.ACCOUNT);
        shippingPaymentDetail.setResponsibleParty(ResponsibleParty.THIRD_PARTY);

        Price price = new Price();
        price.setAmount(1.05);
        price.setCurrency(Currency.USD);

        Payor payor = new Payor();
        payor.setAccountNumber("038000004");
        payor.setAccountType(AccountType.FEDEX_EXPRESS);
        payor.setAddress(buildAddress());
        payor.setCreditCard(new CreditCard());

        shippingPaymentDetail.setAmount(price);
        shippingPaymentDetail.setPayor(payor);

        return shippingPaymentDetail;
    }

    /**
     * Method to build the build instance of {@link RequestHeaderDetail} object .
     * 
     * @return
     */
    private RequestHeaderDetail buildRequestHeaderDetail() {

        return new RequestHeaderDetail("548796554", "8745896532", ZonedDateTime.now()
                .plusDays(1)
                .toInstant()
                .toString(), buildWorkstationDetails());
    }

    /**
     * Method to build the build instance of {@link WorkstationDetails} object.
     * 
     * @return
     */
    private WorkstationDetails buildWorkstationDetails() {

        return new WorkstationDetails("038000004", "6990076", "SSFE", AppName.FASTLANE, "2120");
    }

    /**
     * Method to build the build instance of {@link LocationDetail} object.
     * 
     * @return
     */
    private LocationDetail buildLocationDetail() {

        LocationInfo transactionLocationInfo = new LocationInfo("DFWD", buildAddress());
        LocationInfo servingLocationInfo = new LocationInfo("DFWD", buildAddress());

        return new LocationDetail("038000004", "123456789", transactionLocationInfo, true, true, servingLocationInfo);
    }

    /**
     * Prepare build address object.
     * 
     * @return
     */
    public Address buildAddress() {

        List<String> streetLines = new ArrayList<>();
        streetLines.add("H.No 31, XYZ sector");
        return new Address(streetLines, "Dallas", "TX", "75024", "US", AddressClassification.HOME);

    }

    /**
     * Prepare build RequestedShipment object.
     * 
     * @return
     */
    private RequestedShipment buildRequestedShipment() {

        RequestedShipment requestedShipment = new RequestedShipment();
        requestedShipment.setServiceType("FIRST_OVERNIGHT");
        requestedShipment.setPackagingType("YOUR_PACKAGING");
        requestedShipment.setShipper(buildParty("Mr. Shipper"));
        requestedShipment.setRecipient(buildParty("Mr. Recipient"));
        requestedShipment.setSpecialServicesRequested(new ShipmentSpecialServiceDescription());
        requestedShipment.setRequestedPackageLineItems(buildRequestedPackageLineItems());
        return requestedShipment;
    }

    /**
     * Prepare build Party object.
     * 
     * @return
     */
    private Party buildParty(
            String personName) {

        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("3112556789");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);
        ShippingContact shippingContact = new ShippingContact();
        shippingContact.setPhoneNumberDetails(phoneNumberDetails);
        shippingContact.setPersonName(personName);

        return new Party(buildAddress(), shippingContact);
    }

    /**
     * Prepare build List<RequestedPackageLineItems> object.
     * 
     * @return
     */
    private List<RequestedPackageLineItem> buildRequestedPackageLineItems() {

        Weight weight = new Weight();
        weight.setValue("20");
        weight.setUnits(WeightUnit.LB);
        weight.setMeasurementType(WeightSource.MANUAL);

        Dimensions dimensions = new Dimensions("1", "1", "2", DimensionUnit.IN);

        List<RequestedPackageLineItem> listOfRequestedPackageLineItems = new ArrayList<>();

        RequestedPackageLineItem requestedPackageLineItems = new RequestedPackageLineItem();
        requestedPackageLineItems.setDimensions(dimensions);
        requestedPackageLineItems.setInsuredValue(new Price(Currency.USD, 1.0));
        requestedPackageLineItems.setSpecialHandlingDetail(PackageSpecialHandlingType.CUSTOMER);
        requestedPackageLineItems.setSpecialServicesRequested(new ArrayList<>());
        requestedPackageLineItems.setWeight(weight);

        listOfRequestedPackageLineItems.add(requestedPackageLineItems);

        return listOfRequestedPackageLineItems;
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequestData)} then
     * successful response is returned
     */
    @Test
    public void testCreateOpenShipments() {

        ResponseEntity<CXSEnvelope<CreateOpenShipmentResponse>> response =
                template.exchange(serviceUri + CREATE_OPEN_SHIPMENTS_ENDPOINT_URI, HttpMethod.POST,
                        new HttpEntity<CreateOpenShipmentRequest>(buildCreateOpenShipmentRequest(),
                                createHeaders(tazToken)),
                        new ParameterizedTypeReference<CXSEnvelope<CreateOpenShipmentResponse>>() {});

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#retrieveOpenShipment(String, String, String)} then
     * successful response is returned
     */
    @Test
    public void testRetrieveOpenShipments() {

        String locationId = "LASA";
        String trackingId = "794973860000";
        String referenceId = "3456";

        String endPointUri = serviceUri + RETRIEVE_OPEN_SHIPMENTS_ENDPOINT_URI + "?referenceId=" + referenceId
                + "&locationId=" + locationId + "&trackingId=" + trackingId;

        ResponseEntity<CXSEnvelope<CreateOpenShipmentResponse>> response =
                template.exchange(endPointUri, HttpMethod.GET, new HttpEntity<Object>(createHeaders(tazToken)),
                        new ParameterizedTypeReference<CXSEnvelope<CreateOpenShipmentResponse>>() {});

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#deleteOpenShipment(String, String, String)} then
     * shipment will be deleted
     */
    @Test
    public void testDeleteOpenShipments() {

        String locationId = "LASA";
        String trackingId = "794973860000";
        String referenceId = "3456";

        String endPointUri = serviceUri + DELETE_OPEN_SHIPMENTS_ENDPOINT_URI + "?referenceId=" + referenceId
                + "&locationId=" + locationId + "&trackingId=" + trackingId;

        ResponseEntity<CXSEnvelope<DeleteOpenShipmentResponse>> response =
                template.exchange(endPointUri, HttpMethod.DELETE, new HttpEntity<Object>(createHeaders(tazToken)),
                        new ParameterizedTypeReference<CXSEnvelope<DeleteOpenShipmentResponse>>() {});

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipments(ConfirmShipmentRequestData)} then
     * successful response is returned
     */
    @Test
    public void testConfirmShipments() {

        ResponseEntity<CXSEnvelope<ConfirmedShipmentResponse>> response =
                template.exchange(serviceUri + CONFIRM_SHIPMENT_URI, HttpMethod.POST,
                        new HttpEntity<ConfirmedShipmentRequest>(createMockConfirmedShipmentRequest(),
                                createHeaders(tazToken)),
                        new ParameterizedTypeReference<CXSEnvelope<ConfirmedShipmentResponse>>() {});

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#retrieveSoldShipments(String, String, ShipmentQueryType)}
     * then shipment will be returned
     */
    @Test
    public void testRetrieveSoldShipments() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "Bearer " + tazToken);

        Map<String, String> variableMap = new HashMap<>();
        variableMap.put("locationId", "LASA");
        variableMap.put("id", "794973860000");
        variableMap.put("type", ShipmentQueryType.TRACK.name());

        String uri = UriComponentsBuilder.fromUriString(serviceUri + RETRIEVE_SOLD_SHIPMENTS_ENDPOINT_URI)
                .buildAndExpand(variableMap)
                .toString();

        ResponseEntity<CXSEnvelope<SoldShipmentResponse>> response =
                template.exchange(uri, HttpMethod.GET, new HttpEntity<Object>(headers),
                        new ParameterizedTypeReference<CXSEnvelope<SoldShipmentResponse>>() {});

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#retrieveSoldShipmentSummaries(String, String)} then
     * shipments will be returned
     */
    @Test
    public void testRetrieveSoldShipmentSummaries() {

        Map<String, String> variableMap = new HashMap<>();
        variableMap.put("locationId", "LASA");
        variableMap.put("operationaldate", "2021-03-02T02:25:47Z");

        String uri = UriComponentsBuilder.fromUriString(serviceUri + RETRIEVE_SOLD_SHIPMENT_SUMMARIES_ENDPOINT_URI)
                .buildAndExpand(variableMap)
                .toString();

        ResponseEntity<CXSEnvelope<SoldShipmentSummaryResponse>> response =
                template.exchange(uri, HttpMethod.GET, new HttpEntity<Object>(createHeaders(tazToken)),
                        new ParameterizedTypeReference<CXSEnvelope<SoldShipmentSummaryResponse>>() {});

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    /**
     * Method to create mock confirmed shipments request
     * 
     * @return
     */
    private ConfirmedShipmentRequest createMockConfirmedShipmentRequest() {

        ConfirmedShipmentRequest confirmedShipmentRequest = new ConfirmedShipmentRequest();
        confirmedShipmentRequest.setConfirmedShipmentRequest(createMockConfirmShipmentRequestData());

        return confirmedShipmentRequest;
    }

    /**
     * Method to create mock confirm shipments request data
     * 
     * @return
     */
    private ConfirmShipmentRequestData createMockConfirmShipmentRequestData() {

        ConfirmShipmentRequestData confirmShipmentRequestData = new ConfirmShipmentRequestData();
        confirmShipmentRequestData.setHeaderDetails(buildRequestHeaderDetail());
        confirmShipmentRequestData.setLocationDetails(buildLocationDetail());
        confirmShipmentRequestData.setRequestedShipment(createMockRequestedConfirmShipment());
        confirmShipmentRequestData.setPaymentDetails(buildShipmentPaymentDetails());
        confirmShipmentRequestData.setLabelSpecification(createMockLabelSpecification());

        return confirmShipmentRequestData;
    }

    /**
     * Prepare mock OpenShipmentIdentifier object.
     * 
     * @return
     */
    private OpenShipmentIdentifier createMockRequestedConfirmShipment() {

        OpenShipmentIdentifier requestedOpenShipment = new OpenShipmentIdentifier();
        requestedOpenShipment.setReferenceId("ALSAKA5544331");
        requestedOpenShipment.setMasterTrackingId(createMockMasterTrackingId());
        return requestedOpenShipment;
    }

    /**
     * Prepare mock TrackingSequence object.
     * 
     * @return
     */
    private TrackingSequence createMockMasterTrackingId() {

        TrackingSequence trackingSequence = new TrackingSequence();
        trackingSequence.setBarcode("1009997351190390058700794960774655");
        trackingSequence.setTrackingNumber("794960774655");
        trackingSequence.setSequence(1);
        return trackingSequence;
    }

    /**
     * Prepare mock LabelSpecification object.
     * 
     * @return
     */
    private LabelSpecification createMockLabelSpecification() {

        LabelSpecification labelSpecification = new LabelSpecification();
        labelSpecification.setProcessingOptionsRequested("IGNORE_RESIDENTIAL_DELIVERY_POLICIES");
        labelSpecification.setFormatType(LabelType.OPERATIONAL_LABEL);
        labelSpecification.setStockType(StockType.STOCK_4X6);
        labelSpecification.setImageType(ImageType.PNG);
        labelSpecification.setPrintingOrientation(PrintingOrientation.TOP_EDGE_OF_TEXT_FIRST);
        labelSpecification.setRotation("NA");
        labelSpecification.setLabelOrigin("ALOVA");
        labelSpecification.setCustomerSpecifiedDetail("BUSINESS");
        return labelSpecification;
    }

    /**
     * Method to create mock shipment checkout request
     * 
     * @return
     */
    private ShipmentCheckoutRequest createMockShipmentCheckoutRequest() {

        ShipmentCheckoutRequest shipmentCheckoutRequest = new ShipmentCheckoutRequest();
        shipmentCheckoutRequest.setShipmentCheckoutRequest(createMockShipmentCheckoutRequestData());

        return shipmentCheckoutRequest;
    }

    /**
     * Method to create mock shipment checkout request data
     * 
     * @return
     */
    private ShipmentCheckoutRequestData createMockShipmentCheckoutRequestData() {

        ShipmentCheckoutRequestData shipmentCheckoutRequestData = new ShipmentCheckoutRequestData();
        shipmentCheckoutRequestData.setHeaderDetails(buildRequestHeaderDetail());
        shipmentCheckoutRequestData.setLocationDetails(buildLocationDetail());
        List<OpenShipmentIdentifier> requestedShipments = new ArrayList<>();
        requestedShipments.add(createMockRequestedConfirmShipment());
        requestedShipments.add(createMockRequestedConfirmShipment());
        shipmentCheckoutRequestData.setRequestedShipment(requestedShipments);
        shipmentCheckoutRequestData.setLabelSpecification(createMockLabelSpecification());
        shipmentCheckoutRequestData.setPaymentDetails(buildShipmentPaymentDetails());

        return shipmentCheckoutRequestData;
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipmentsCheckout(ShipmentCheckoutRequestData)}
     * then successful response is returned
     */
    @Test
    public void testConfirmShipmentsCheckout() {

        ResponseEntity<CXSEnvelope<ShipmentCheckoutResponse>> response =
                template.exchange(serviceUri + SHIPMENT_CHECKOUTS_ENDPOINT_URI, HttpMethod.POST,
                        new HttpEntity<ShipmentCheckoutRequest>(createMockShipmentCheckoutRequest(),
                                createHeaders(tazToken)),
                        new ParameterizedTypeReference<CXSEnvelope<ShipmentCheckoutResponse>>() {});

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }
    
    /**
     * Method to create mock delete sold shipment request
     * 
     * @return
     */
    private DeleteSoldShipmentRequest createDeleteSoldShipmentRequest() {

        DeleteSoldShipmentRequest deleteSoldShipmentRequest = new DeleteSoldShipmentRequest();
        deleteSoldShipmentRequest.setDeleteSoldShipmentsRequest(createDeleteSoldShipmentRequestData());

        return deleteSoldShipmentRequest;
    }

    /**
     * Method to create mock delete sold shipment request data
     * 
     * @return
     */
    private DeleteSoldShipmentRequestData createDeleteSoldShipmentRequestData() {

        DeleteSoldShipmentRequestData deleteSoldShipmentRequestData = new DeleteSoldShipmentRequestData();
        deleteSoldShipmentRequestData.setHeaderDetails(buildRequestHeaderDetail());
        deleteSoldShipmentRequestData.setLocationDetails(new LocationInfo("DFWD", buildAddress()));
        deleteSoldShipmentRequestData.setTrackingId("794984201540");
        deleteSoldShipmentRequestData.setRetailTransactionId("794984201540");
        deleteSoldShipmentRequestData.setCustomerName(new PersonName("Mr.", "Customer"));

        return deleteSoldShipmentRequestData;
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#deleteSoldShipments(DeleteSoldShipmentRequestData)} then
     * successful response is returned
     */
    @Test
    public void testDeleteSoldShipments() {

        ResponseEntity<CXSEnvelope<DeletedSoldShipmentResponse>> response =
                template.exchange(serviceUri + DELETE_SOLD_SHIPMENTS_ENDPOINT_URI, HttpMethod.POST,
                        new HttpEntity<DeleteSoldShipmentRequest>(createDeleteSoldShipmentRequest(),
                                createHeaders(tazToken)),
                        new ParameterizedTypeReference<CXSEnvelope<DeletedSoldShipmentResponse>>() {});

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }
    
    /**
     * Method to build the mock instance of {@link InterlineShippingDetail} object
     * 
     * @return
     */
    private InterlineShippingDetail buildInterlineShippingDetail() {

        InterlineShippingDetail interlineShippingDetail = new InterlineShippingDetail();
        interlineShippingDetail.setInterlineId("100127");
        interlineShippingDetail.setInterlineCode("G4");
        interlineShippingDetail.setInterlineName("ALLEGIANT AIRLINES");
        interlineShippingDetail.setInterlineNumber("008");
        interlineShippingDetail.setEmployeeId("3932");

        return interlineShippingDetail;
    }

    /**
     * Method to build the mock instance of {@link Person} object
     * 
     * @return
     */
    private Person buildPerson(
            String personName) {

        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("3112556789");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);
        CustomerEmailDetail custEmailDtls = new CustomerEmailDetail();
        custEmailDtls.setEmailAddress("mainEmail123@testmail.com");
        custEmailDtls.setAlternateEmailAddress("alternateEmail123@testmail.com");
        CustomerContact customerContact = new CustomerContact();
        customerContact.setPhoneNumberDetails(phoneNumberDetails);
        customerContact.setPersonName(personName);
        customerContact.setEmailDetails(custEmailDtls);
        return new Person(buildAddress(), customerContact);
    }
    
}
