package com.fedex.rscs;

import java.io.IOException;
import java.rmi.UnexpectedException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Use this class to ask the API Gateway for a TAZ token
 */
@Component
public class TAZTokenRetrieval {

    private String clientId;
    private String clientSecret;
    private String accessTokenUri;

    @Autowired
    public TAZTokenRetrieval(@Value("${service.client.id}") String clientId,
                    @Value("${service.client.secret}") String clientSecret,
                    @Value("${security.oauth.client.access-token-uri}") String accessTokenUri) {

        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.accessTokenUri = accessTokenUri;
    }

    /**
     * Given a session with the gateway, request a TAZ token.
     * <p>
     * This method will throw HTTP exceptions if the server doesn't respond
     *
     * @return The plain text token (which is encrypted JSON)
     * @throws IOException 
     */
    public String getJWT()
            throws IOException {

        if (StringUtils.isBlank(clientId) || StringUtils.isBlank(clientSecret)) {
            clientId = System.getProperty("tazClientId");
            clientSecret = System.getProperty("tazDevClientSecret");
        }

        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(headers);
        template.getInterceptors()
                .add(new BasicAuthorizationInterceptor(clientId, clientSecret));

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(accessTokenUri)
                .queryParam("grant_type", "client_credentials");
        ResponseEntity<String> response =
                template.exchange(builder.toUriString(), HttpMethod.POST, entity, String.class);
        ObjectMapper obj = new ObjectMapper();

        if (response.getStatusCode() == HttpStatus.OK) {
            JsonNode body = obj.readTree(response.getBody());
            return body.get("access_token")
                    .asText();

        } else {
            throw new UnexpectedException("Actual status: " + response.getStatusCode());
        }
    }
}
