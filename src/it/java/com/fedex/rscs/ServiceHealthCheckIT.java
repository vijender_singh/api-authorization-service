package com.fedex.rscs;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import com.fedex.rscs.controller.HealthCheckController;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Integration test cases for RSCS micro service.
 *
 * @author 3802280
 */
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration(initializers = {TestInitializer.class})
public class ServiceHealthCheckIT {

    @Value("${integration-test.uri.retail-shipment-creation-service}")
    private String serviceUri;

    private RestTemplate template;

    @Before
    public void setUp()
            throws Exception {

        template = new RestTemplate();
    }

    /**
     * Test case to validate {@link HealthCheckController#respondUp()} if the service is up. Fails
     * if server fails to connect or doesn't return 200.
     *
     * @throws IOException
     */
    @Test
    public void testServerUp()
            throws IOException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        String healthCheckURI = serviceUri + "/health";

        // If the target service is down, usually this line will throw an exception and your test
        // will stop here
        ResponseEntity<String> pingResponse = template.exchange(healthCheckURI, HttpMethod.GET, entity, String.class);

        assertNotNull("Server is not up, or not responding to health check at: " + healthCheckURI, pingResponse);
        assertEquals("Server is up but not responding with a 200 to health check", HttpStatus.OK,
                pingResponse.getStatusCode());

    }
}
