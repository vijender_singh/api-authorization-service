package com.fedex.rscs;

import java.io.IOException;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.DefaultPropertySourceFactory;
import org.springframework.core.io.support.EncodedResource;

/**
 * This class can be used to allow specifying which config file we want to use at runtime.
 * 
 * @author 3883424
 *
 */
public class TestInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(
            ConfigurableApplicationContext configurableApplicationContext) {

        ConfigurableEnvironment environment = configurableApplicationContext.getEnvironment();
        String configFile = environment.getSystemProperties()
                .getOrDefault("config.file", "local.properties")
                .toString();
        EncodedResource resource = new EncodedResource(new ClassPathResource(configFile), "UTF-8");
        try {
            PropertySource source = (new DefaultPropertySourceFactory()).createPropertySource(configFile, resource);
            environment.getPropertySources()
                    .addFirst(source);
        } catch (IOException ex) {
            throw new RuntimeException("Unable to load test properties from file in classpath: " + configFile, ex);
        }
    }
}
