package com.fedex.common.cxs.controlleradvice;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;


public class BeanValidationParserTest {

    private static Validator validator;

    private BeanValidationParser cut;

    @BeforeClass
    public static void setupOnce() {

        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @Before
    public void setup() {

        cut = new BeanValidationParser(new ObjectMapper());
    }

    // region test mapping method

    @Test
    public void testErrorTypeMappingMissingProperty() {

        // arrange
        MyChildDto dto = new MyChildDto(null, "description", 1);
        List<ConstraintViolation<MyChildDto>> violations = getViolations(dto);

        // act
        ValidationErrorType actualErrorType = cut.getErrorType(violations.get(0));

        // assert
        assertThat(violations.size(), equalTo(1));
        assertThat(actualErrorType, is(ValidationErrorType.MISSING_PROPERTY));
    }

    @Test
    public void testErrorTypeMappingInvalidLength() {

        // arrange
        MyChildDto dto = new MyChildDto("hello world", "a really long description", 1);
        List<ConstraintViolation<MyChildDto>> violations = getViolations(dto);

        // act
        ValidationErrorType actualErrorType = cut.getErrorType(violations.get(0));

        // assert
        assertThat(violations.size(), equalTo(1));
        assertThat(actualErrorType, is(ValidationErrorType.INVALID_LENGTH));
    }

    @Test
    public void testErrorTypeMappingInvalidValue() {

        // arrange
        MyChildDto dto = new MyChildDto("hello world", "description", 12);
        List<ConstraintViolation<MyChildDto>> violations = getViolations(dto);

        // act
        ValidationErrorType actualErrorType = cut.getErrorType(violations.get(0));

        // assert
        assertThat(violations.size(), equalTo(1));
        assertThat(actualErrorType, is(ValidationErrorType.INVALID_VALUE));
    }

    // region test property path names method

    @Test
    public void testHeavilyNestedDto()
            throws NoSuchFieldException {

        // arrange
        MyChildDto childDto = new MyChildDto("hello world", "description", 100);
        MyParentDto parentDto = new MyParentDto(childDto);
        MyGrandparentDto grandparentDto = new MyGrandparentDto(parentDto);
        List<ConstraintViolation<MyGrandparentDto>> violations = getViolations(grandparentDto);

        // act
        PropertyPathNames actualPropertyPath = cut.getPropertyPathNames(violations.get(0));

        // assert
        assertThat(violations.size(), equalTo(1));
        assertThat(actualPropertyPath.getFieldName(), equalTo("occurrences"));
        assertThat(actualPropertyPath.getSectionName(), equalTo("child"));
    }

    @Test
    public void testSingleNestedDto()
            throws NoSuchFieldException {

        // arrange
        MyChildDto childDto = new MyChildDto("hello world", "description", 100);
        MyParentDto parentDto = new MyParentDto(childDto);
        List<ConstraintViolation<MyParentDto>> violations = getViolations(parentDto);

        // act
        PropertyPathNames actualPropertyPath = cut.getPropertyPathNames(violations.get(0));

        // assert
        assertThat(violations.size(), equalTo(1));
        assertThat(actualPropertyPath.getFieldName(), equalTo("occurrences"));
        assertThat(actualPropertyPath.getSectionName(), equalTo("child"));
    }

    @Test
    public void testNoNestingDto()
            throws NoSuchFieldException {

        // arrange
        MyChildDto childDto = new MyChildDto(null, "description", 5);
        List<ConstraintViolation<MyChildDto>> violations = getViolations(childDto);

        // act
        PropertyPathNames actualPropertyPath = cut.getPropertyPathNames(violations.get(0));

        // assert
        assertThat(violations.size(), equalTo(1));
        assertThat(actualPropertyPath.getFieldName(), equalTo("name"));
        assertThat(actualPropertyPath.getSectionName(), nullValue());
    }

    @Test
    public void testHeavilyNestedList()
            throws NoSuchFieldException {

        // Parent List 1
        List<MyChildDto> myChildList1 = new ArrayList<MyChildDto>();
        MyChildDto childDto1 = new MyChildDto("hello world1", "description1", 100);
        MyChildDto childDto2 = new MyChildDto("hello world2", "description2", 100);
        myChildList1.add(childDto1);
        myChildList1.add(childDto2);
        MyParentDto parentDto1 = new MyParentDto(myChildList1);

        // Parent List 2
        List<MyChildDto> myChildList2 = new ArrayList<MyChildDto>();
        MyChildDto childDto3 = new MyChildDto("hello world3", "description3", 100);
        MyChildDto childDto4 = new MyChildDto("hello world4", "description4", 100);
        myChildList1.add(childDto3);
        myChildList1.add(childDto4);
        MyParentDto parentDto2 = new MyParentDto(myChildList2);

        // set Parent list
        List<MyParentDto> myParentList = new ArrayList<MyParentDto>();
        myParentList.add(parentDto1);
        myParentList.add(parentDto2);

        MyGrandparentDto grandparentDto = new MyGrandparentDto(myParentList);
        List<ConstraintViolation<MyGrandparentDto>> violations = getViolations(grandparentDto);

        // act
        PropertyPathNames actualPropertyPath = cut.getPropertyPathNames(violations.get(0));

        // assert
        assertThat(violations.size(), equalTo(4));
        assertThat(actualPropertyPath.getFieldName(), equalTo("occurrences"));
        assertThat(actualPropertyPath.getSectionName()
                .split("\\[")[0], equalTo("myChildList"));
    }

    @Test
    public void testSingleNestedList()
            throws NoSuchFieldException {

        // Parent List
        List<MyChildDto> myChildList = new ArrayList<MyChildDto>();
        MyChildDto childDto1 = new MyChildDto("hello world1", "description1", 100);
        MyChildDto childDto2 = new MyChildDto("hello world2", "description2", 100);
        myChildList.add(childDto1);
        myChildList.add(childDto2);

        MyParentDto parentDto = new MyParentDto(myChildList);
        List<ConstraintViolation<MyParentDto>> violations = getViolations(parentDto);

        // act
        PropertyPathNames actualPropertyPath = cut.getPropertyPathNames(violations.get(0));

        // assert
        assertThat(violations.size(), equalTo(2));
        assertThat(actualPropertyPath.getFieldName(), equalTo("occurrences"));
        assertThat(actualPropertyPath.getSectionName()
                .split("\\[")[0], equalTo("myChildList"));
    }

    /**
     * Convenience method to validate a dto and return all constraint violations found
     */
    private <T> List<ConstraintViolation<T>> getViolations(
            final T dto) {

        final Set<ConstraintViolation<T>> violations = validator.validate(dto);
        return new ArrayList<>(violations);
    }

    // region sample dto classes for testing

    @SuppressWarnings({"WeakerAccess", "unused"})
    private static class MyChildDto {

        @NotEmpty
        @JsonProperty("name")
        private String title;

        @Size(min = 1, max = 12)
        private String description;

        @Max(10)
        private int occurrences;

        @JsonCreator
        public MyChildDto(@JsonProperty("name") final String title,
                @JsonProperty("description") final String description,
                @JsonProperty("occurrences") final int occurrences) {

            this.title = title;
            this.description = description;
            this.occurrences = occurrences;
        }

        public String getTitle() {

            return title;
        }

        public String getDescription() {

            return description;
        }

        public int getOccurrences() {

            return occurrences;
        }
    }

    @SuppressWarnings({"WeakerAccess", "unused"})
    private static class MyParentDto {

        @Valid
        private MyChildDto myChildDto;
        @Valid
        private List<MyChildDto> myChildList;

        public MyParentDto(MyChildDto child) {

            this.myChildDto = child;
        }

        public MyParentDto(List<MyChildDto> myChildList) {

            this.myChildList = myChildList;
        }

        // json property intentionally on getter instead of property
        @JsonProperty("child")
        public MyChildDto getMyChildDto() {

            return myChildDto;
        }

        public List<MyChildDto> getMyChildList() {

            return myChildList;
        }
    }

    @SuppressWarnings({"WeakerAccess", "unused"})
    private static class MyGrandparentDto {

        @Valid
        @JsonProperty("parent")
        private MyParentDto myParentDto;
        @Valid
        private List<MyParentDto> myParentList;

        public MyGrandparentDto(MyParentDto myParentDto) {

            this.myParentDto = myParentDto;
        }

        public MyGrandparentDto(List<MyParentDto> myParentList) {

            this.myParentList = myParentList;
        }

        public MyParentDto getMyParentDto() {

            return myParentDto;
        }

        public List<MyParentDto> getMyParentList() {

            return myParentList;
        }
    }

    // endregion

}
