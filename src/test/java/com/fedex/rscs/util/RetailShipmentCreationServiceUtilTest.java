package com.fedex.rscs.util;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fedex.nxgen.ship.v17.ientities.PackageSpecialServicesRequested;
import com.fedex.nxgen.ship.v17.ientities.ShippingDocument;
import com.fedex.nxgen.ship.v17.ientities.ShippingDocumentPart;
import com.fedex.rscs.model.RequestedPackageLineItemDetail;
import com.fedex.rscs.model.RequestedPackageSpecialServices;
import com.fedex.rscs.model.ServiceType;
import com.fedex.rscs.model.ShippingDocumentType;
import com.fedex.rscs.model.SpecialServiceDescriptionDetail;
import com.fedex.rscs.model.SpecialServiceDetail;
import com.fedex.rscs.model.cshp.SpecialServiceCode;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;

/**
 * Test class for RetailShipmentCreationServiceUtil
 * 
 * @author Dhruv.Sood
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class RetailShipmentCreationServiceUtilTest {

    @InjectMocks
    private RetailShipmentCreationServiceUtil retailShipmentCreationServiceUtil;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    ObjectMapper objectMapper = null;

    @Before
    public void initialize() {

        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationServiceUtil#decodePackageDocument(ShippingDocument, ShippingDocumentType)}
     * then image response in string format is received
     * 
     * @throws IOException
     */
    @Test
    public void testDecodePackageDocumentSuccess()
            throws IOException {

        ShippingDocumentPart[] shippingDocumentParts = new ShippingDocumentPart[1];

        String actualResponse =
                "JVBERi0xLjQKMSAwIG9iago8PAovVHlwZSAvQ2F0YWxvZwovUGFnZXMgMyAwIFIKPj4KZW5kb2JqCjIgMCBvYmoKPDwKL1R5cGUgL091dGxpbmVzCi9Db3VudCAwCj4+CmVuZG9iagozIDAgb2JqCjw8Ci9UeXBlIC9QYWdlcwovQ291bnQgMQovS2lkcyBbMTggMCBSXQo+PgplbmRvYmoKNCAwIG9iagpbL1BERiAvVGV4dCAvSW1hZ2VCIC9JbWFnZUMgL0ltYWdlSV0KZW5kb2JqCjUgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUxCi9CYXNlRm9udCAvSGVsdmV0aWNhCi9FbmNvZGluZyAvTWFjUm9tYW5FbmNvZGluZwo+PgplbmRvYmoKNiAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9IZWx2ZXRpY2EtQm9sZAovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjcgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUxCi9CYXNlRm9udCAvSGVsdmV0aWNhLU9ibGlxdWUKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iago4IDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0hlbHZldGljYS1Cb2xkT2JsaXF1ZQovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjkgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUxCi9CYXNlRm9udCAvQ291cmllcgovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjEwIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0NvdXJpZXItQm9sZAovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjExIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0NvdXJpZXItT2JsaXF1ZQovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjEyIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0NvdXJpZXItQm9sZE9ibGlxdWUKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iagoxMyAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9UaW1lcy1Sb21hbgovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjE0IDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL1RpbWVzLUJvbGQKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iagoxNSAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9UaW1lcy1JdGFsaWMKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iagoxNiAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9UaW1lcy1Cb2xkSXRhbGljCi9FbmNvZGluZyAvTWFjUm9tYW5FbmNvZGluZwo+PgplbmRvYmoKMTcgMCBvYmogCjw8Ci9DcmVhdGlvbkRhdGUgKEQ6MjAwMykKL1Byb2R1Y2VyIChGZWRFeCBTZXJ2aWNlcykKL1RpdGxlIChGZWRFeCBTaGlwcGluZyBMYWJlbCkNL0NyZWF0b3IgKEZlZEV4IEN1c3RvbWVyIEF1dG9tYXRpb24pDS9BdXRob3IgKENMUyBWZXJzaW9uIDUxMjAxMzUpCj4+CmVuZG9iagoxOCAwIG9iago8PAovVHlwZSAvUGFnZQ0vUGFyZW50IDMgMCBSCi9SZXNvdXJjZXMgPDwgL1Byb2NTZXQgNCAwIFIgCiAvRm9udCA8PCAvRjEgNSAwIFIgCi9GMiA2IDAgUiAKL0YzIDcgMCBSIAovRjQgOCAwIFIgCi9GNSA5IDAgUiAKL0Y2IDEwIDAgUiAKL0Y3IDExIDAgUiAKL0Y4IDEyIDAgUiAKL0Y5IDEzIDAgUiAKL0YxMCAxNCAwIFIgCi9GMTEgMTUgMCBSIAovRjEyIDE2IDAgUiAKID4+Ci9YT2JqZWN0IDw8IC9GZWRFeEV4cHJlc3MgMjAgMCBSCi9FeHByZXNzRSAyMSAwIFIKL2JhcmNvZGUwIDIyIDAgUgo+Pgo+PgovTWVkaWFCb3ggWzAgMCA3OTIgNjEyXQovVHJpbUJveFswIDAgNzkyIDYxMl0KL0NvbnRlbnRzIDE5IDAgUgovUm90YXRlIDkwPj4KZW5kb2JqCjE5IDAgb2JqCjw8IC9MZW5ndGggMjk4NwovRmlsdGVyIFsvQVNDSUk4NURlY29kZSAvRmxhdGVEZWNvZGVdIAo+PgpzdHJlYW0KR2F0VTY5b2c6JyZcU15KczNPImAxKF5pYSg5Pk8xZ1RHMGkqXyJkX0ciNl1Xa2BYVEArYywmRTdNNmxaNSonNyMlIiRXSTtmV2cyS1JVaGoKPFE0ZVtRaSdEImkzQytVPUdkMkIpOXEpaypET0lmJzdwWzByRiMsPHEiVDtaWl86dWotXyZgTyhdW1ojQ2tCLG8vWz85YWowOy4uIVhFYGUKP10nQl86VjwzSEdCO0QhXlU2RUslWSo0OSpZLzUpJSFLRmluOiwoVVFIO1tuJkE9LGUwVzExayYiRFBCXChCQjghTFA/aTJVPEhlRGcsO2sKXixjKz5aVEFrKVtuLGdaQDg1MVQtXGk1T0UrRCpvcjhEakAkPl1KdGo8Um1LcUBIL15BNVckZFEiPXBHXEBnXVFDJmM/UGE3Jl8/TEArWUIKRztZSi1xPWBAby1OL2xpOCZqazUlbVJdZD5YPVkmUixRVD9mQz0sTG5ubi0pKzJSWFdyc2VrOzNFb1RuWlRdLV8wQyVKZ2lXbnBKcDNQQkkKPzRIM0dTanBoK0xBYlRTLF49Qm5CUmZkVGY1ZUFHYS4pckkkNi9xUyk9Jjg0WzVHcEhwWj9FZFpLVC9DRj9bPEQ3IStQOCdNZmQvKHBGWkAKZFAhaSc2IVQzSFQnZlA4UlBBRDMsMj5wXlItU0YoKEJwRGlldC5CMj11YFVDLkNeOGNeJWY0Y2RKXidSXlNcUl8zQD8mdEUhMj4jXURoY0gKcmwra1NmUGRyJyYkTix0JHUkbGEsWUs+dEdsaFIlcD9qQVFwdFtsUlY5Ym1gTlxGV0ptTW5bKV1DIyNeTzFXJGttYnQ0NnBeOGpxX1hfZEMKaDsuUC9mM2NTV0laayZVYTVvODNZN0NIXXBDRm1uJiROSHBaZC1Sb0BXWCEsSXNcWGhtJ2FnbCFLJDhiR2wsIUcjQUoyZSkudTVgXyR0NSwKRmw8NkNNcGhfMGdKS15uakBEZ2EuRWJrRGM3LTZzMDxYTDZtMGVgL3ErTnM5LFhCWlJtIlR1ajlER1Z1WVx1R3VcRUMiRmRoUEApNk5UbG4KZT5gSyg7W0dITjRbOE5QakkjXDFMNHVfMTIoVjgyZ2g0Y3RjY28tb0dgKyRZMkQ1dFxUKExMY0I4TTwiKC4kQjhhYD5OWzpkXCFvYCxiSXQKNGNlQGBnWkxMXWpNSzlWK0hWNyMlazsmcEE8akg8JzQiRzxrVixoOiw0cURYZ2g4QE1rLmFcTTQ7Q284UGdebHFaRkVPNm5BP0deRXRcO2gKUEtpLU9OUC5fYytxdWhmciMsY19MNFxBP2k8ISRuZFQiW0hnK2I4JWE8OzVmQk04OmhOWSIrUmliUDNVREhVRHFkV2NXNFdeW0xQNTE7QzkKXSIzSTRPRy0uKTZRcURScS0nX0wlT1RMRzckP2FrWz9XLE4jJFAoZGcrZDFPMXJAbT9oM2tGIjNwPEJQTC9aJyFJOl10b2tvZUdwW2JFWkQKIjBMJCRqaUJuVllUZ0UsMUwjS1JPTkgiQzZeb0pJPDRnak4yO0xeWCQvJ3M1WihvTEU7SUczWEQ3LURQZlhkRE9fbzNGbilGYTY0PnRyc1oKbFMzIjJOZylgZzlYTFVFYGNoYGtTJ047M2pqLG4mNFBNXmhTYlpIajZUdG5AOlBPI0tIJ2hYQy0+XW1yJFkkTTpPdXBaRkVFWnE9ImZ0KCYKYSg8dElfMyFVJlJdc1ZTI1whSFk3MT4sbDFmPXVGPUdJP0I3W2xLcVhJJ0dzTGIpcFMyQydgQ0pWclNFSigsZl1URlFcJ2guPS1SND0xc1wKZzU8KVFIM2pLNVpPZjxWb2UrZXNbVEYmSyo0JFlWNFIvO1hvcDJzZSM5aCJPYWBUJTlvdGpWSThXMzZROjJMIlwxNVcmVXBfKWc2JzpXMkEKSy9PJFc+QCNyZFc1RG1ELUk2SD0icFhJPF10Wi9NLEA7a2ZkSipsM05OTiIvVG9TbVJZMmcwT0IsSVtKRGc6RFtZbXVIaUpmb0tfI0czPU8KWHBzI20qPUFcUzAvI1pqcEknLl1CcVByaSxFOC11MVpXL3RTcEViLVhAJ109TWNbLC06Pyw7QkdDYWp0V0BSLTlOLzckX1JdOFNTXmo2OV8KJ1BBW04wa2NcYl4uVy1mR09iOlFJJ1EuW09YZmc4VSIncHApKEdJJ2w1XUBKZjUtZE1PKVIvIm9HKSciMFgvJ1VtTGlcXipJXnVtRGg0TkwKVUtmKzIiZnQnVGJHMzc9aS9xXE9QXFMmPj5QRFxbKGFVaEU5U2BbYCIxRDxlYjBIJmpvMWkwczJIWWFXQTRqZiMkTllcPkNsO04hTENmM2QKO1A7OEJJUSJfZ0AtXUY5VGVtTUstMl9wNWheLWNcNWE6Vl88I1VRSDFLMUBhZk9AIXNJYWZXJEZ0Ym1RWG5hVEMlZUtLbV1IVCcrOmxdRT8KP1w8SjRaNUkuMSciVTFcb2xTXE1sYXQhSVM0ZVVYMWBMYFtVZG02bm1YVztGOnA8W10uUDkzYERXOGZUKWotMTBiYFo8OEZfaVlQb2sxL1kKKEAlUCNbXlxcOE8jYDI3Ok11ZzhsTko9c3JlUDVLYDMsOzwiKk42S1JhT2hzNyQ9Z11cN2hORkFQb0c1ZCMuO01XU09fLkhESyNxSUhsMyUKVzc8Y10tLkg/YjwsLC5yJ24yXFhUU2ZuJzNnPFZzTSpvXFJiUW1BSldwMChPPXUnX084amRIZWhaNGkmazMicD06LktCNyYsWT49ISZvQiYKcD0zUXEncSpgQEMtTyhYTiRJTlRrJGBjPllXciYyXEdxJFBFNDo+ZWg0XWBfcVFjYCZmaWhCSjJfTSNfS2FTKWZAWG8rK1dJZU1xVWJtI1oKbCw3cD46TkohOWR0LV1xPEhiI1gnVUE2bjYpVHEnaVV1b3RyITQ/K0A/RHAyOG5sKCpkaCModTQ1S2g9VC0mY01uJTRJUmRmSyFwb2wuWm8KSUNdYV1mNElRcDxGLHQ4ZGJ1SXA3P0YvXk5GKSx0L1tKJChQV3NXNGZBSFY4cW0/N21jXUBPRXEuODRYXV9RJ3MsK2M9ODU+SmFdZD9OY1QKRFs3PyY3JjphIjsrV1RDOWU8bWk6JFRob2svaGhnVzQmNEEuLVZGblx1QzQ/UFhQKitHcWgkYVQiazlkaixaRylvXEw6UmFbRWRocUo9L2wKPSwvZTQsSiYuXWhaZ2JmXClvOyNLSjo2V3MiZGcjUT5tS2AlOztDczZxQC9AWkAlbiY6Ki49bChzRChtcj09Zl8zZ2M+cGNjajNeOkYldWwKbVc3OyNROGhvXSVDL1RMbG1cI1Q9W2Z1cGZiVTY8REU2N0M0LDNbJ1h0QHRBL1FDK0wpVT9DSFMjXD5MJGBzY1ZbWXQ+Kz1ALSFbZl5CTDIKJyIyTzsiQFtTZFIkXDFzWkVGTk1ZXVU4X0ZHcSVsJ1tAXDYhRic2XkhxOi0yN21eV1AtczljNFt0KFVHLyUtRyM/c1xbPDxEa2ojIWpudTAKcSEhVWYpUGhfO1EkPFxgUl5QYTMzTzskXC5vLEVEPUZhSCpSYCJiUj43N1E6MCJCPlhnMnVzcyxsSW5iT190MidWclFlOEJHb2A4XyxXTCcKIUV1Qzo6P1RyRi9bVC0kMFYpQkxAM201KidqRCdKJDVdVT0kUUVQOTh1RzprIl9hSVA6T2RuZ2BjM2k8ZzUsdUhTU2dJalotJyR0NlNFZWAKQ1M8VzdUaV1HcWJUU09qOTpfXmtUSlxYKCZwRUw+S0pjYFZnKjNTVEMrJytDN0tcbzBTYFFFcT4lakhaOj9tJ1coMUMxcUQjbWhoMXUvRHUKRD9hVUJUUFhMX0RAJ05BOiYtdGRJZWNlUFtiLlVvZXE/cDM7Iy9VVlRORGw5ZlhmKU8qc2VGUDtYcS5zYFY0anBycCRvfj4KZW5kc3RyZWFtCmVuZG9iagoyMCAwIG9iago8PCAvVHlwZSAvWE9iamVjdAovU3VidHlwZSAvSW1hZ2UKL1dpZHRoIDExOAovSGVpZ2h0IDQ5Ci9Db2xvclNwYWNlIC9EZXZpY2VHcmF5Ci9CaXRzUGVyQ29tcG9uZW50IDgKL0xlbmd0aCA0NjEKL0ZpbHRlciBbL0FTQ0lJODVEZWNvZGUgL0ZsYXRlRGVjb2RlXQo+PnN0cmVhbQpHYiIvZUpJXVI/I1huWGtUNkE+WEtuRGJXQiJNWkVhalBwZDRdJyheQFJsdCJrQiJTMCRmJT89bT00Qzs7LiROKGpSY1lSJXNkJmw5JE90XgopJjc9TCg3LWVxcWVALigvU0M5ODQtZkxYZUxaNXFjWjVsQUVCNTU+M0AwPWsiO1JoMmhCY1FMMyZKPzVvW2NIMGJTRk5ePDs7QDQxU1k5bQpjIlpJPVlsI2M0KWBfIllsbU4zJS5BLmxOU2BWZS9ZPCU/LjcoKCkvLmRCWztrIUpbJSclREpcST8rYTc6NW9Oa1lccUVTPjM8STViWm0oSQolZDxAQik4RGJ0M3BLR2NsYlFrMSdpWlZzN05NMFxvJlIqcVU+Z0h1LUZyQ2toPGtIRkcqXCRNPV9zVi8tJFxKRjplLz8+ZlhBK0NDbXBEZQpkU1A2R2N1LFxYJ0FMPWE1SkNbNDZHLmhwMj1VNGVeJGVvXFEjYiptN0M4JC1KQShjWj9ZYCdDOzZKcEhlUGxcJEhFQyF1XipRYSs2Z0VNawpbZkVWKmIuIz5wcj4tV1NmIz5oJlc/dWtrVHNoWWA9L1guNjZBKjtiaXMvZGdAUVhYc3I4bTt+PgplbmRzdHJlYW0KZW5kb2JqCjIxIDAgb2JqCjw8IC9UeXBlIC9YT2JqZWN0Ci9TdWJ0eXBlIC9JbWFnZQovV2lkdGggNTQKL0hlaWdodCA1NAovQ29sb3JTcGFjZSAvRGV2aWNlR3JheQovQml0c1BlckNvbXBvbmVudCA4Ci9MZW5ndGggNzcKL0ZpbHRlciBbL0FTQ0lJODVEZWNvZGUgL0ZsYXRlRGVjb2RlXQo+PnN0cmVhbQpHYiIwSmQwVGRxJGo0b0ZeVSwiSFRzOUVJRTswQVQsX0UqTFolb0A3Smw1VjtIJ0NzPVRycURhSC40QmYjYzRPVlQ7KGQjZjxHRTl+PgplbmRzdHJlYW0KZW5kb2JqCjIyIDAgb2JqCjw8IC9UeXBlIC9YT2JqZWN0Ci9TdWJ0eXBlIC9JbWFnZQovV2lkdGggMjA5Ci9IZWlnaHQgNjYKL0NvbG9yU3BhY2UgL0RldmljZUdyYXkKL0JpdHNQZXJDb21wb25lbnQgOAovTGVuZ3RoIDEyNjEKL0ZpbHRlciBbL0FTQ0lJODVEZWNvZGUgL0ZsYXRlRGVjb2RlXQo+PnN0cmVhbQpHYiIvY2dRRUxBI1hlRmZULWZoR2xVXEIxPFIqdSZZY0xkbk5yJmtqRi5pRW5vQ01lKFQvclRuclZPOUkqa2ldQU06XyVFRikrXV0/VFtCLApBJDkzKTMmYExeTHAtR1dDSUxAYmI5c0Nua3BeKHUrb1BBKjAtLDBNVkdIV1sjYCFIYFgsTDc0PkZnIllVO1AlRVM3dWNKVSEkV2xXPy5LaAplMjwtIS0tTiZNLiFOOG8ib0w0WjBuLCEiPW5XY15EWVs7VS1SVXJiRT9CVTFwVHA5Uk4+dG07KCFJP2oqZ1BKcWE9aGkyM0kzJCtAdV5PLQpEOzNOOUNSIlVMTVhubUE6ZGpUXmRoJVs2PVFMT11QTUAxKmVLJlgtJHQ2SlkzcmQkKGFYXlwpQl11Uig0KE49YUdWYjBGMzw2RzQsS29wNApOSXVbNjdtYyQoNlRQPmcsYGRdWmUhMD5BaU8yI2pfTiRhMT84T2ZVJCI6RlQlMlJhYjklPzpXTi9jKyReaDYuR3JUQWhVVDRTKSNiRCQsMgpdblRALzNkTzNRLFdXYnRhJSIkOnBPMms1Q1AuVixtQ1pvaDwxbTNzZD8qN2wlMmdvLU1aWUxFXjkwS2NkLD4pXlFtODBySTNULSNAcXVQTAomKklCJS9pNkNBJGQnNWZGLU8lUThaMkE9YkhrO1IjX3VaI1ciQmFSaDZObkpuQytzZW91Ilg2NlloKDY9MDQ/YjtgLUBVOERlKCRiYjlMZApPcyFtczNdMC4/VGE0JDA5alcwOzRPJ1ZKJzhtPUlOWV9dWWMrQ2tmZGJFRVkvUGFFMFZnbzNQWVJrK0NEISt1OjgyKG80SzlJbDlgNlE/bQpcXkJcUS8kMS8wL0hcRVFiPDtpJGFpXUtLOSs/a1dMbSREQzxBSilDM0ZhYi0sbWtnKmAxSUQ7WzJtUF5UXUdEIkw+OEJrNiZWNGo7V2BjNgpOUlpLJ24vKiojREVyJiVXZi1SI19RR2EhJystUiIxNFYsWyFLSmtvQjoxaShbdCZNckohL29YTUU/Lm9qbml1WytEYlwiYWJbcWAzNCZzcwpQNk9DL09NOkphNVpEQikoJUNbPGthTkgpMjdyWlVWJVkyUkQ5Sy9wZUVsUF9sNjNqQjJScTYsKiFhKGIjciFMLlVmYHRoYCg5NFFAQEI0OwpGVyxqTT9eT1tkOHFpWU9MOkJvLEYyXUpuQC1jXSddTiZGR1RvKk4qa111ZElTSGIqcSx0TGNSPiRmRm0kal45RmVRKkR0KSZGIXRGRU9pMwpWPz1dTmQsUTVmUmF0Mj5oN2RgJmoxP2E0JUMpL0A7aFtQLzRjcmJgaV5RIjxCc0opRURPLkxIQ2AndSJUUW46WSgnbTEvZCpDMU5nUlgncQo7TW8/U1UpQE1YPWIvJEVBM2FeXkMpI1drTzp0KmpYaTBGdUdJbyNtVHNmP2ZEL1EwL0JBOD5QbD9lT0o3b1FhRUV1aFBnU2VGKGZCXiErXwomMHEqLl1LOCRUXFMnKkpjQyM7VTFqJS8sOE0hUENkO0NXJXFbQy5FSVE3YSQ4WEhqakRTXzxkTDBocDk1LEMjWjw3Rz1NRTRzaUxtLS9VOgptVHM2dWVcUnBzNFo7JlohVEEuSVJBSWcsR1Vrak1pM3NSIkF0TkYpSWhISn4+CmVuZHN0cmVhbQplbmRvYmoKeHJlZgowIDIzCjAwMDAwMDAwMDAgNjU1MzUgZiAKMDAwMDAwMDAwOSAwMDAwMCBuIAowMDAwMDAwMDU4IDAwMDAwIG4gCjAwMDAwMDAxMDQgMDAwMDAgbiAKMDAwMDAwMDE2MiAwMDAwMCBuIAowMDAwMDAwMjE0IDAwMDAwIG4gCjAwMDAwMDAzMTIgMDAwMDAgbiAKMDAwMDAwMDQxNSAwMDAwMCBuIAowMDAwMDAwNTIxIDAwMDAwIG4gCjAwMDAwMDA2MzEgMDAwMDAgbiAKMDAwMDAwMDcyNyAwMDAwMCBuIAowMDAwMDAwODI5IDAwMDAwIG4gCjAwMDAwMDA5MzQgMDAwMDAgbiAKMDAwMDAwMTA0MyAwMDAwMCBuIAowMDAwMDAxMTQ0IDAwMDAwIG4gCjAwMDAwMDEyNDQgMDAwMDAgbiAKMDAwMDAwMTM0NiAwMDAwMCBuIAowMDAwMDAxNDUyIDAwMDAwIG4gCjAwMDAwMDE2MjIgMDAwMDAgbiAKMDAwMDAwMjAwMiAwMDAwMCBuIAowMDAwMDA1MDgxIDAwMDAwIG4gCjAwMDAwMDU3MjggMDAwMDAgbiAKMDAwMDAwNTk4OSAwMDAwMCBuIAp0cmFpbGVyCjw8Ci9JbmZvIDE3IDAgUgovU2l6ZSAyMwovUm9vdCAxIDAgUgo+PgpzdGFydHhyZWYKNzQzNwolJUVPRgo=";
        String partJson =
                "{\"documentPartSequenceNumber\": \"1\",\"image\": \"JVBERi0xLjQKMSAwIG9iago8PAovVHlwZSAvQ2F0YWxvZwovUGFnZXMgMyAwIFIKPj4KZW5kb2JqCjIgMCBvYmoKPDwKL1R5cGUgL091dGxpbmVzCi9Db3VudCAwCj4+CmVuZG9iagozIDAgb2JqCjw8Ci9UeXBlIC9QYWdlcwovQ291bnQgMQovS2lkcyBbMTggMCBSXQo+PgplbmRvYmoKNCAwIG9iagpbL1BERiAvVGV4dCAvSW1hZ2VCIC9JbWFnZUMgL0ltYWdlSV0KZW5kb2JqCjUgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUxCi9CYXNlRm9udCAvSGVsdmV0aWNhCi9FbmNvZGluZyAvTWFjUm9tYW5FbmNvZGluZwo+PgplbmRvYmoKNiAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9IZWx2ZXRpY2EtQm9sZAovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjcgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUxCi9CYXNlRm9udCAvSGVsdmV0aWNhLU9ibGlxdWUKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iago4IDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0hlbHZldGljYS1Cb2xkT2JsaXF1ZQovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjkgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUxCi9CYXNlRm9udCAvQ291cmllcgovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjEwIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0NvdXJpZXItQm9sZAovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjExIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0NvdXJpZXItT2JsaXF1ZQovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjEyIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0NvdXJpZXItQm9sZE9ibGlxdWUKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iagoxMyAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9UaW1lcy1Sb21hbgovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjE0IDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL1RpbWVzLUJvbGQKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iagoxNSAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9UaW1lcy1JdGFsaWMKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iagoxNiAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9UaW1lcy1Cb2xkSXRhbGljCi9FbmNvZGluZyAvTWFjUm9tYW5FbmNvZGluZwo+PgplbmRvYmoKMTcgMCBvYmogCjw8Ci9DcmVhdGlvbkRhdGUgKEQ6MjAwMykKL1Byb2R1Y2VyIChGZWRFeCBTZXJ2aWNlcykKL1RpdGxlIChGZWRFeCBTaGlwcGluZyBMYWJlbCkNL0NyZWF0b3IgKEZlZEV4IEN1c3RvbWVyIEF1dG9tYXRpb24pDS9BdXRob3IgKENMUyBWZXJzaW9uIDUxMjAxMzUpCj4+CmVuZG9iagoxOCAwIG9iago8PAovVHlwZSAvUGFnZQ0vUGFyZW50IDMgMCBSCi9SZXNvdXJjZXMgPDwgL1Byb2NTZXQgNCAwIFIgCiAvRm9udCA8PCAvRjEgNSAwIFIgCi9GMiA2IDAgUiAKL0YzIDcgMCBSIAovRjQgOCAwIFIgCi9GNSA5IDAgUiAKL0Y2IDEwIDAgUiAKL0Y3IDExIDAgUiAKL0Y4IDEyIDAgUiAKL0Y5IDEzIDAgUiAKL0YxMCAxNCAwIFIgCi9GMTEgMTUgMCBSIAovRjEyIDE2IDAgUiAKID4+Ci9YT2JqZWN0IDw8IC9GZWRFeEV4cHJlc3MgMjAgMCBSCi9FeHByZXNzRSAyMSAwIFIKL2JhcmNvZGUwIDIyIDAgUgo+Pgo+PgovTWVkaWFCb3ggWzAgMCA3OTIgNjEyXQovVHJpbUJveFswIDAgNzkyIDYxMl0KL0NvbnRlbnRzIDE5IDAgUgovUm90YXRlIDkwPj4KZW5kb2JqCjE5IDAgb2JqCjw8IC9MZW5ndGggMjk4NwovRmlsdGVyIFsvQVNDSUk4NURlY29kZSAvRmxhdGVEZWNvZGVdIAo+PgpzdHJlYW0KR2F0VTY5b2c6JyZcU15KczNPImAxKF5pYSg5Pk8xZ1RHMGkqXyJkX0ciNl1Xa2BYVEArYywmRTdNNmxaNSonNyMlIiRXSTtmV2cyS1JVaGoKPFE0ZVtRaSdEImkzQytVPUdkMkIpOXEpaypET0lmJzdwWzByRiMsPHEiVDtaWl86dWotXyZgTyhdW1ojQ2tCLG8vWz85YWowOy4uIVhFYGUKP10nQl86VjwzSEdCO0QhXlU2RUslWSo0OSpZLzUpJSFLRmluOiwoVVFIO1tuJkE9LGUwVzExayYiRFBCXChCQjghTFA/aTJVPEhlRGcsO2sKXixjKz5aVEFrKVtuLGdaQDg1MVQtXGk1T0UrRCpvcjhEakAkPl1KdGo8Um1LcUBIL15BNVckZFEiPXBHXEBnXVFDJmM/UGE3Jl8/TEArWUIKRztZSi1xPWBAby1OL2xpOCZqazUlbVJdZD5YPVkmUixRVD9mQz0sTG5ubi0pKzJSWFdyc2VrOzNFb1RuWlRdLV8wQyVKZ2lXbnBKcDNQQkkKPzRIM0dTanBoK0xBYlRTLF49Qm5CUmZkVGY1ZUFHYS4pckkkNi9xUyk9Jjg0WzVHcEhwWj9FZFpLVC9DRj9bPEQ3IStQOCdNZmQvKHBGWkAKZFAhaSc2IVQzSFQnZlA4UlBBRDMsMj5wXlItU0YoKEJwRGlldC5CMj11YFVDLkNeOGNeJWY0Y2RKXidSXlNcUl8zQD8mdEUhMj4jXURoY0gKcmwra1NmUGRyJyYkTix0JHUkbGEsWUs+dEdsaFIlcD9qQVFwdFtsUlY5Ym1gTlxGV0ptTW5bKV1DIyNeTzFXJGttYnQ0NnBeOGpxX1hfZEMKaDsuUC9mM2NTV0laayZVYTVvODNZN0NIXXBDRm1uJiROSHBaZC1Sb0BXWCEsSXNcWGhtJ2FnbCFLJDhiR2wsIUcjQUoyZSkudTVgXyR0NSwKRmw8NkNNcGhfMGdKS15uakBEZ2EuRWJrRGM3LTZzMDxYTDZtMGVgL3ErTnM5LFhCWlJtIlR1ajlER1Z1WVx1R3VcRUMiRmRoUEApNk5UbG4KZT5gSyg7W0dITjRbOE5QakkjXDFMNHVfMTIoVjgyZ2g0Y3RjY28tb0dgKyRZMkQ1dFxUKExMY0I4TTwiKC4kQjhhYD5OWzpkXCFvYCxiSXQKNGNlQGBnWkxMXWpNSzlWK0hWNyMlazsmcEE8akg8JzQiRzxrVixoOiw0cURYZ2g4QE1rLmFcTTQ7Q284UGdebHFaRkVPNm5BP0deRXRcO2gKUEtpLU9OUC5fYytxdWhmciMsY19MNFxBP2k8ISRuZFQiW0hnK2I4JWE8OzVmQk04OmhOWSIrUmliUDNVREhVRHFkV2NXNFdeW0xQNTE7QzkKXSIzSTRPRy0uKTZRcURScS0nX0wlT1RMRzckP2FrWz9XLE4jJFAoZGcrZDFPMXJAbT9oM2tGIjNwPEJQTC9aJyFJOl10b2tvZUdwW2JFWkQKIjBMJCRqaUJuVllUZ0UsMUwjS1JPTkgiQzZeb0pJPDRnak4yO0xeWCQvJ3M1WihvTEU7SUczWEQ3LURQZlhkRE9fbzNGbilGYTY0PnRyc1oKbFMzIjJOZylgZzlYTFVFYGNoYGtTJ047M2pqLG4mNFBNXmhTYlpIajZUdG5AOlBPI0tIJ2hYQy0+XW1yJFkkTTpPdXBaRkVFWnE9ImZ0KCYKYSg8dElfMyFVJlJdc1ZTI1whSFk3MT4sbDFmPXVGPUdJP0I3W2xLcVhJJ0dzTGIpcFMyQydgQ0pWclNFSigsZl1URlFcJ2guPS1SND0xc1wKZzU8KVFIM2pLNVpPZjxWb2UrZXNbVEYmSyo0JFlWNFIvO1hvcDJzZSM5aCJPYWBUJTlvdGpWSThXMzZROjJMIlwxNVcmVXBfKWc2JzpXMkEKSy9PJFc+QCNyZFc1RG1ELUk2SD0icFhJPF10Wi9NLEA7a2ZkSipsM05OTiIvVG9TbVJZMmcwT0IsSVtKRGc6RFtZbXVIaUpmb0tfI0czPU8KWHBzI20qPUFcUzAvI1pqcEknLl1CcVByaSxFOC11MVpXL3RTcEViLVhAJ109TWNbLC06Pyw7QkdDYWp0V0BSLTlOLzckX1JdOFNTXmo2OV8KJ1BBW04wa2NcYl4uVy1mR09iOlFJJ1EuW09YZmc4VSIncHApKEdJJ2w1XUBKZjUtZE1PKVIvIm9HKSciMFgvJ1VtTGlcXipJXnVtRGg0TkwKVUtmKzIiZnQnVGJHMzc9aS9xXE9QXFMmPj5QRFxbKGFVaEU5U2BbYCIxRDxlYjBIJmpvMWkwczJIWWFXQTRqZiMkTllcPkNsO04hTENmM2QKO1A7OEJJUSJfZ0AtXUY5VGVtTUstMl9wNWheLWNcNWE6Vl88I1VRSDFLMUBhZk9AIXNJYWZXJEZ0Ym1RWG5hVEMlZUtLbV1IVCcrOmxdRT8KP1w8SjRaNUkuMSciVTFcb2xTXE1sYXQhSVM0ZVVYMWBMYFtVZG02bm1YVztGOnA8W10uUDkzYERXOGZUKWotMTBiYFo8OEZfaVlQb2sxL1kKKEAlUCNbXlxcOE8jYDI3Ok11ZzhsTko9c3JlUDVLYDMsOzwiKk42S1JhT2hzNyQ9Z11cN2hORkFQb0c1ZCMuO01XU09fLkhESyNxSUhsMyUKVzc8Y10tLkg/YjwsLC5yJ24yXFhUU2ZuJzNnPFZzTSpvXFJiUW1BSldwMChPPXUnX084amRIZWhaNGkmazMicD06LktCNyYsWT49ISZvQiYKcD0zUXEncSpgQEMtTyhYTiRJTlRrJGBjPllXciYyXEdxJFBFNDo+ZWg0XWBfcVFjYCZmaWhCSjJfTSNfS2FTKWZAWG8rK1dJZU1xVWJtI1oKbCw3cD46TkohOWR0LV1xPEhiI1gnVUE2bjYpVHEnaVV1b3RyITQ/K0A/RHAyOG5sKCpkaCModTQ1S2g9VC0mY01uJTRJUmRmSyFwb2wuWm8KSUNdYV1mNElRcDxGLHQ4ZGJ1SXA3P0YvXk5GKSx0L1tKJChQV3NXNGZBSFY4cW0/N21jXUBPRXEuODRYXV9RJ3MsK2M9ODU+SmFdZD9OY1QKRFs3PyY3JjphIjsrV1RDOWU8bWk6JFRob2svaGhnVzQmNEEuLVZGblx1QzQ/UFhQKitHcWgkYVQiazlkaixaRylvXEw6UmFbRWRocUo9L2wKPSwvZTQsSiYuXWhaZ2JmXClvOyNLSjo2V3MiZGcjUT5tS2AlOztDczZxQC9AWkAlbiY6Ki49bChzRChtcj09Zl8zZ2M+cGNjajNeOkYldWwKbVc3OyNROGhvXSVDL1RMbG1cI1Q9W2Z1cGZiVTY8REU2N0M0LDNbJ1h0QHRBL1FDK0wpVT9DSFMjXD5MJGBzY1ZbWXQ+Kz1ALSFbZl5CTDIKJyIyTzsiQFtTZFIkXDFzWkVGTk1ZXVU4X0ZHcSVsJ1tAXDYhRic2XkhxOi0yN21eV1AtczljNFt0KFVHLyUtRyM/c1xbPDxEa2ojIWpudTAKcSEhVWYpUGhfO1EkPFxgUl5QYTMzTzskXC5vLEVEPUZhSCpSYCJiUj43N1E6MCJCPlhnMnVzcyxsSW5iT190MidWclFlOEJHb2A4XyxXTCcKIUV1Qzo6P1RyRi9bVC0kMFYpQkxAM201KidqRCdKJDVdVT0kUUVQOTh1RzprIl9hSVA6T2RuZ2BjM2k8ZzUsdUhTU2dJalotJyR0NlNFZWAKQ1M8VzdUaV1HcWJUU09qOTpfXmtUSlxYKCZwRUw+S0pjYFZnKjNTVEMrJytDN0tcbzBTYFFFcT4lakhaOj9tJ1coMUMxcUQjbWhoMXUvRHUKRD9hVUJUUFhMX0RAJ05BOiYtdGRJZWNlUFtiLlVvZXE/cDM7Iy9VVlRORGw5ZlhmKU8qc2VGUDtYcS5zYFY0anBycCRvfj4KZW5kc3RyZWFtCmVuZG9iagoyMCAwIG9iago8PCAvVHlwZSAvWE9iamVjdAovU3VidHlwZSAvSW1hZ2UKL1dpZHRoIDExOAovSGVpZ2h0IDQ5Ci9Db2xvclNwYWNlIC9EZXZpY2VHcmF5Ci9CaXRzUGVyQ29tcG9uZW50IDgKL0xlbmd0aCA0NjEKL0ZpbHRlciBbL0FTQ0lJODVEZWNvZGUgL0ZsYXRlRGVjb2RlXQo+PnN0cmVhbQpHYiIvZUpJXVI/I1huWGtUNkE+WEtuRGJXQiJNWkVhalBwZDRdJyheQFJsdCJrQiJTMCRmJT89bT00Qzs7LiROKGpSY1lSJXNkJmw5JE90XgopJjc9TCg3LWVxcWVALigvU0M5ODQtZkxYZUxaNXFjWjVsQUVCNTU+M0AwPWsiO1JoMmhCY1FMMyZKPzVvW2NIMGJTRk5ePDs7QDQxU1k5bQpjIlpJPVlsI2M0KWBfIllsbU4zJS5BLmxOU2BWZS9ZPCU/LjcoKCkvLmRCWztrIUpbJSclREpcST8rYTc6NW9Oa1lccUVTPjM8STViWm0oSQolZDxAQik4RGJ0M3BLR2NsYlFrMSdpWlZzN05NMFxvJlIqcVU+Z0h1LUZyQ2toPGtIRkcqXCRNPV9zVi8tJFxKRjplLz8+ZlhBK0NDbXBEZQpkU1A2R2N1LFxYJ0FMPWE1SkNbNDZHLmhwMj1VNGVeJGVvXFEjYiptN0M4JC1KQShjWj9ZYCdDOzZKcEhlUGxcJEhFQyF1XipRYSs2Z0VNawpbZkVWKmIuIz5wcj4tV1NmIz5oJlc/dWtrVHNoWWA9L1guNjZBKjtiaXMvZGdAUVhYc3I4bTt+PgplbmRzdHJlYW0KZW5kb2JqCjIxIDAgb2JqCjw8IC9UeXBlIC9YT2JqZWN0Ci9TdWJ0eXBlIC9JbWFnZQovV2lkdGggNTQKL0hlaWdodCA1NAovQ29sb3JTcGFjZSAvRGV2aWNlR3JheQovQml0c1BlckNvbXBvbmVudCA4Ci9MZW5ndGggNzcKL0ZpbHRlciBbL0FTQ0lJODVEZWNvZGUgL0ZsYXRlRGVjb2RlXQo+PnN0cmVhbQpHYiIwSmQwVGRxJGo0b0ZeVSwiSFRzOUVJRTswQVQsX0UqTFolb0A3Smw1VjtIJ0NzPVRycURhSC40QmYjYzRPVlQ7KGQjZjxHRTl+PgplbmRzdHJlYW0KZW5kb2JqCjIyIDAgb2JqCjw8IC9UeXBlIC9YT2JqZWN0Ci9TdWJ0eXBlIC9JbWFnZQovV2lkdGggMjA5Ci9IZWlnaHQgNjYKL0NvbG9yU3BhY2UgL0RldmljZUdyYXkKL0JpdHNQZXJDb21wb25lbnQgOAovTGVuZ3RoIDEyNjEKL0ZpbHRlciBbL0FTQ0lJODVEZWNvZGUgL0ZsYXRlRGVjb2RlXQo+PnN0cmVhbQpHYiIvY2dRRUxBI1hlRmZULWZoR2xVXEIxPFIqdSZZY0xkbk5yJmtqRi5pRW5vQ01lKFQvclRuclZPOUkqa2ldQU06XyVFRikrXV0/VFtCLApBJDkzKTMmYExeTHAtR1dDSUxAYmI5c0Nua3BeKHUrb1BBKjAtLDBNVkdIV1sjYCFIYFgsTDc0PkZnIllVO1AlRVM3dWNKVSEkV2xXPy5LaAplMjwtIS0tTiZNLiFOOG8ib0w0WjBuLCEiPW5XY15EWVs7VS1SVXJiRT9CVTFwVHA5Uk4+dG07KCFJP2oqZ1BKcWE9aGkyM0kzJCtAdV5PLQpEOzNOOUNSIlVMTVhubUE6ZGpUXmRoJVs2PVFMT11QTUAxKmVLJlgtJHQ2SlkzcmQkKGFYXlwpQl11Uig0KE49YUdWYjBGMzw2RzQsS29wNApOSXVbNjdtYyQoNlRQPmcsYGRdWmUhMD5BaU8yI2pfTiRhMT84T2ZVJCI6RlQlMlJhYjklPzpXTi9jKyReaDYuR3JUQWhVVDRTKSNiRCQsMgpdblRALzNkTzNRLFdXYnRhJSIkOnBPMms1Q1AuVixtQ1pvaDwxbTNzZD8qN2wlMmdvLU1aWUxFXjkwS2NkLD4pXlFtODBySTNULSNAcXVQTAomKklCJS9pNkNBJGQnNWZGLU8lUThaMkE9YkhrO1IjX3VaI1ciQmFSaDZObkpuQytzZW91Ilg2NlloKDY9MDQ/YjtgLUBVOERlKCRiYjlMZApPcyFtczNdMC4/VGE0JDA5alcwOzRPJ1ZKJzhtPUlOWV9dWWMrQ2tmZGJFRVkvUGFFMFZnbzNQWVJrK0NEISt1OjgyKG80SzlJbDlgNlE/bQpcXkJcUS8kMS8wL0hcRVFiPDtpJGFpXUtLOSs/a1dMbSREQzxBSilDM0ZhYi0sbWtnKmAxSUQ7WzJtUF5UXUdEIkw+OEJrNiZWNGo7V2BjNgpOUlpLJ24vKiojREVyJiVXZi1SI19RR2EhJystUiIxNFYsWyFLSmtvQjoxaShbdCZNckohL29YTUU/Lm9qbml1WytEYlwiYWJbcWAzNCZzcwpQNk9DL09NOkphNVpEQikoJUNbPGthTkgpMjdyWlVWJVkyUkQ5Sy9wZUVsUF9sNjNqQjJScTYsKiFhKGIjciFMLlVmYHRoYCg5NFFAQEI0OwpGVyxqTT9eT1tkOHFpWU9MOkJvLEYyXUpuQC1jXSddTiZGR1RvKk4qa111ZElTSGIqcSx0TGNSPiRmRm0kal45RmVRKkR0KSZGIXRGRU9pMwpWPz1dTmQsUTVmUmF0Mj5oN2RgJmoxP2E0JUMpL0A7aFtQLzRjcmJgaV5RIjxCc0opRURPLkxIQ2AndSJUUW46WSgnbTEvZCpDMU5nUlgncQo7TW8/U1UpQE1YPWIvJEVBM2FeXkMpI1drTzp0KmpYaTBGdUdJbyNtVHNmP2ZEL1EwL0JBOD5QbD9lT0o3b1FhRUV1aFBnU2VGKGZCXiErXwomMHEqLl1LOCRUXFMnKkpjQyM7VTFqJS8sOE0hUENkO0NXJXFbQy5FSVE3YSQ4WEhqakRTXzxkTDBocDk1LEMjWjw3Rz1NRTRzaUxtLS9VOgptVHM2dWVcUnBzNFo7JlohVEEuSVJBSWcsR1Vrak1pM3NSIkF0TkYpSWhISn4+CmVuZHN0cmVhbQplbmRvYmoKeHJlZgowIDIzCjAwMDAwMDAwMDAgNjU1MzUgZiAKMDAwMDAwMDAwOSAwMDAwMCBuIAowMDAwMDAwMDU4IDAwMDAwIG4gCjAwMDAwMDAxMDQgMDAwMDAgbiAKMDAwMDAwMDE2MiAwMDAwMCBuIAowMDAwMDAwMjE0IDAwMDAwIG4gCjAwMDAwMDAzMTIgMDAwMDAgbiAKMDAwMDAwMDQxNSAwMDAwMCBuIAowMDAwMDAwNTIxIDAwMDAwIG4gCjAwMDAwMDA2MzEgMDAwMDAgbiAKMDAwMDAwMDcyNyAwMDAwMCBuIAowMDAwMDAwODI5IDAwMDAwIG4gCjAwMDAwMDA5MzQgMDAwMDAgbiAKMDAwMDAwMTA0MyAwMDAwMCBuIAowMDAwMDAxMTQ0IDAwMDAwIG4gCjAwMDAwMDEyNDQgMDAwMDAgbiAKMDAwMDAwMTM0NiAwMDAwMCBuIAowMDAwMDAxNDUyIDAwMDAwIG4gCjAwMDAwMDE2MjIgMDAwMDAgbiAKMDAwMDAwMjAwMiAwMDAwMCBuIAowMDAwMDA1MDgxIDAwMDAwIG4gCjAwMDAwMDU3MjggMDAwMDAgbiAKMDAwMDAwNTk4OSAwMDAwMCBuIAp0cmFpbGVyCjw8Ci9JbmZvIDE3IDAgUgovU2l6ZSAyMwovUm9vdCAxIDAgUgo+PgpzdGFydHhyZWYKNzQzNwolJUVPRgo=\"}";
        shippingDocumentParts[0] = objectMapper.readValue(partJson, ShippingDocumentPart.class);

        ShippingDocument shippingDocument = new ShippingDocument();
        shippingDocument.setType("OUTBOUND_LABEL");
        shippingDocument.setParts(shippingDocumentParts);

        String imageResponse = retailShipmentCreationServiceUtil.decodePackageDocument(shippingDocument,
                ShippingDocumentType.OUTBOUND_LABEL);
        assertThat(imageResponse, notNullValue());
        assertThat(imageResponse, equalTo(actualResponse));
    }

    /**
     * Test case to verify that when request is made with different image type than OUTBOUND_LABEL to
     * {@link RetailShipmentCreationServiceUtil#decodePackageDocument(ShippingDocument, ShippingDocumentType)}
     * then image response null is received
     * 
     */
    @Test
    public void testDecodePackageDocumentDifferentType() {

        ShippingDocument shippingDocument = new ShippingDocument();
        shippingDocument.setType("OUTBOUND_LABEL");

        String imageResponse = retailShipmentCreationServiceUtil.decodePackageDocument(shippingDocument,
                ShippingDocumentType.OUTBOUND_LABEL);
        assertThat(imageResponse, nullValue());
    }

    /**
     * Test case to verify that when request is made with empty part to
     * {@link RetailShipmentCreationServiceUtil#decodePackageDocument(ShippingDocument, ShippingDocumentType)}
     * then image response null is received
     * 
     */
    @Test
    public void testDecodePackageDocumentNoPart() {

        ShippingDocument shippingDocument = new ShippingDocument();
        shippingDocument.setType("CERTIFICATE_OF_ORIGIN");

        String imageResponse = retailShipmentCreationServiceUtil.decodePackageDocument(shippingDocument,
                ShippingDocumentType.OUTBOUND_LABEL);
        assertThat(imageResponse, nullValue());
    }

    /**
     * Test case to verify that when empty request is made to
     * {@link RetailShipmentCreationServiceUtil#decodePackageDocument(ShippingDocument, ShippingDocumentType)}
     * then image response null is received
     * 
     */
    @Test
    public void testDecodePackageDocumentEmptyRequest() {

        String imageResponse =
                retailShipmentCreationServiceUtil.decodePackageDocument(null, ShippingDocumentType.OUTBOUND_LABEL);
        assertThat(imageResponse, nullValue());
    }

    /**
     * Test case to verify that when request is made with empty image to
     * {@link RetailShipmentCreationServiceUtil#decodePackageDocument(ShippingDocument, ShippingDocumentType)}
     * then image response in empty string format is received
     * 
     * @throws IOException
     */
    @Test
    public void testDecodePackageDocumentNoImage()
            throws IOException {

        ShippingDocumentPart[] shippingDocumentParts = new ShippingDocumentPart[1];

        String partJson = "{\"documentPartSequenceNumber\": \"1\",\"image\": \"\"}";
        shippingDocumentParts[0] = objectMapper.readValue(partJson, ShippingDocumentPart.class);

        ShippingDocument shippingDocument = new ShippingDocument();
        shippingDocument.setType("OUTBOUND_LABEL");
        shippingDocument.setParts(shippingDocumentParts);

        String imageResponse = retailShipmentCreationServiceUtil.decodePackageDocument(shippingDocument,
                ShippingDocumentType.OUTBOUND_LABEL);
        assertThat(imageResponse, notNullValue());
        assertThat(imageResponse, equalTo(""));
    }
    
    /**
     * Test case to verify that
     * {@link RetailShipmentCreationServiceUtil#getSpecialServiceCode(SpecialServiceCode)} returns
     * valid special service code when passed BATTERY as service type in request.
     * 
     * @throws IOException
     */
    @Test
    public void testServiceCode() {

        SpecialServiceDetail specialServicesList = new SpecialServiceDetail();
        specialServicesList.setSpecialServiceType("BATTERY");
        String code = retailShipmentCreationServiceUtil.getSpecialServiceCode(specialServicesList);
        assertThat(code, notNullValue());
        assertThat(code, equalTo("93"));
    }
    
    /**
     * Test case to verify that
     * {@link RetailShipmentCreationServiceUtil#getSpecialServiceCode(SpecialServiceCode)} returns
     * valid special service code when passed SIGNATURE_OPTION as service type in request.
     * 
     */
    @Test
    public void testServiceCodeSignatureOption() {

        SpecialServiceDetail specialServicesList = new SpecialServiceDetail();
        specialServicesList.setSpecialServiceType("SIGNATURE_OPTION");
        specialServicesList.setSpecialServiceSubType("ADULT");
        String code = retailShipmentCreationServiceUtil.getSpecialServiceCode(specialServicesList);
        assertThat(code, notNullValue());
        assertThat(code, equalTo("35"));
    }
    
    /**
     * Test case to verify that
     * {@link RetailShipmentCreationServiceUtil#getApiClientId()} returns
     * api client id.
     * 
     */
    @Test
    public void testgetApiClientId() {

        String appName = "FUSE";
        String apiVersionId = "2120";
        
        String response = retailShipmentCreationServiceUtil.getApiClientId(appName, apiVersionId);
        
        assertThat(response, notNullValue());
    }
    
    /**
     * Test case to verify that
     * {@link RetailShipmentCreationServiceUtil#getCustomerTransactionId()} returns
     * customer transaction id.
     * 
     */
    @Test
    public void testgetCustomerTransactionId() {

        String appName = "FUSE";
        String requestDateTime = "2021-01-15";
        String location = "DFWD";
        
        String response = retailShipmentCreationServiceUtil.getCustomerTransactionId(appName, requestDateTime, location);
        
        assertThat(response, notNullValue());
    }
    
    /**
     * Test CreatePackageSpecialService for Ground service with BATTERY
     */
    @Test
    public void testCreatePackageSpecialServiceForGround() {

        PackageSpecialServicesRequested request = retailShipmentCreationServiceUtil
                .createPackageSpecialService(createRequestedPackageLineItemDetail(), ServiceType.FEDEX_GROUND.name());
        
        assertThat(request, notNullValue());
        assertThat(request.getDangerousGoodsDetail(), notNullValue());
        
    }
    
    /**
     * Test CreatePackageSpecialService for home delivery service with BATTERY
     */
    @Test
    public void testCreatePackageSpecialServiceForHD() {

        PackageSpecialServicesRequested request = retailShipmentCreationServiceUtil
                .createPackageSpecialService(createRequestedPackageLineItemDetail(), ServiceType.GROUND_HOME_DELIVERY.name());
        
        assertThat(request, notNullValue());
        assertThat(request.getDangerousGoodsDetail(), notNullValue());
        
    }
    
    /**
     * Test CreatePackageSpecialService for Express service
     */
    @Test
    public void testCreatePackageSpecialService() {

        PackageSpecialServicesRequested request = retailShipmentCreationServiceUtil
                .createPackageSpecialService(createRequestedPackageLineItemDetail(), ServiceType.FIRST_OVERNIGHT.name());
        
        assertThat(request, notNullValue());
        assertThat(request.getDangerousGoodsDetail(), nullValue());
        
    }
    
    /**
     * method to create the line Item.
     */
    private RequestedPackageLineItemDetail createRequestedPackageLineItemDetail() {

        RequestedPackageLineItemDetail details = new RequestedPackageLineItemDetail();
        RequestedPackageSpecialServices service = new RequestedPackageSpecialServices(new ArrayList<>(), null, null);
        service.getSpecialServicesRequested()
                .add(new SpecialServiceDescriptionDetail("BATTERY", null, null, null));
        details.setPackageSpecialServicesRequested(service);

        return details;
    }
}
