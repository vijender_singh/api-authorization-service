package com.fedex.rscs.processor;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.netflix.hystrix.exception.HystrixRuntimeException;

import com.fedex.rscs.config.AppConfig;
import com.fedex.rscs.dto.Address;
import com.fedex.rscs.dto.AddressClassification;
import com.fedex.rscs.dto.AppName;
import com.fedex.rscs.dto.ConfirmShipmentRequestData;
import com.fedex.rscs.dto.ConfirmedShipmentRequest;
import com.fedex.rscs.dto.ConfirmedShipmentResponse;
import com.fedex.rscs.dto.CreateOpenShipmentRequest;
import com.fedex.rscs.dto.CreateOpenShipmentRequestData;
import com.fedex.rscs.dto.CreateOpenShipmentResponse;
import com.fedex.rscs.dto.CustomerContact;
import com.fedex.rscs.dto.CustomerEmailDetail;
import com.fedex.rscs.dto.DeleteOpenShipmentResponse;
import com.fedex.rscs.dto.DeleteSoldShipmentRequest;
import com.fedex.rscs.dto.DeleteSoldShipmentRequestData;
import com.fedex.rscs.dto.DeletedSoldShipmentResponse;
import com.fedex.rscs.dto.HomeDeliveryType;
import com.fedex.rscs.dto.HomeDeliveryDetail;
import com.fedex.rscs.dto.LabelSpecification;
import com.fedex.rscs.dto.LocationDetail;
import com.fedex.rscs.dto.LocationInfo;
import com.fedex.rscs.dto.OpenShipmentCore;
import com.fedex.rscs.dto.OpenShipmentIdentifier;
import com.fedex.rscs.dto.PackageSpecialHandlingType;
import com.fedex.rscs.dto.Party;
import com.fedex.rscs.dto.PaymentSource;
import com.fedex.rscs.dto.PaymentType;
import com.fedex.rscs.dto.Payor;
import com.fedex.rscs.dto.Person;
import com.fedex.rscs.dto.PersonName;
import com.fedex.rscs.dto.PhoneNumber;
import com.fedex.rscs.dto.PhoneNumberDetail;
import com.fedex.rscs.dto.RequestHeaderDetail;
import com.fedex.rscs.dto.RequestedPackageLineItem;
import com.fedex.rscs.dto.RequestedPaymentType;
import com.fedex.rscs.dto.RequestedShipment;
import com.fedex.rscs.dto.ResponsibleParty;
import com.fedex.rscs.dto.RetrieveOpenShipmentResponse;
import com.fedex.rscs.dto.ShipmentCheckoutRequest;
import com.fedex.rscs.dto.ShipmentCheckoutRequestData;
import com.fedex.rscs.dto.ShipmentCheckoutResponse;
import com.fedex.rscs.dto.ShipmentPaymentOption;
import com.fedex.rscs.dto.ShipmentQueryType;
import com.fedex.rscs.dto.ShipmentSpecialServiceDescription;
import com.fedex.rscs.dto.ShipmentTransactionDetail;
import com.fedex.rscs.dto.ShippingContact;
import com.fedex.rscs.dto.ShippingPaymentDetail;
import com.fedex.rscs.dto.SoldShipmentResponse;
import com.fedex.rscs.dto.SoldShipmentSummaryResponse;
import com.fedex.rscs.dto.SpecialServiceDescription;
import com.fedex.rscs.dto.TrackingSequence;
import com.fedex.rscs.dto.Weight;
import com.fedex.rscs.dto.WeightUnit;
import com.fedex.rscs.dto.WorkstationDetails;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.ServiceInputValidationException;
import com.fedex.rscs.exception.ServiceProcessorTerminalException;
import com.fedex.rscs.exception.client.ClientTerminalException;
import com.fedex.rscs.exception.client.ClientTransientException;
import com.fedex.rscs.model.CarrierDetail;
import com.fedex.rscs.model.DeleteSoldShipmentData;
import com.fedex.rscs.model.DeletedSoldShipmentDetail;
import com.fedex.rscs.model.LocationContextDetail;
import com.fedex.rscs.model.PackageLineItemsDetail;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ServiceConstant;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.SoldShipmentDetail;
import com.fedex.rscs.model.SoldShipmentSummary;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.cshp.ConfirmedShipmentData;
import com.fedex.rscs.model.cshp.OpenShipmentResponse;
import com.fedex.rscs.model.cshp.TrackingSequenceDetail;
import com.fedex.rscs.repository.ConfirmShipmentRepository;
import com.fedex.rscs.service.ConfirmShipmentTransactionService;
import com.fedex.rscs.service.ConfirmShipmentTransactionServiceImpl;
import com.fedex.rscs.service.OpenShipmentTransactionService;
import com.fedex.rscs.service.RetailLocationDataProvider;
import com.fedex.rscs.service.TransactionRouter;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * This class contains test cases for RetailShipmentCreationProcessor
 * 
 * @author Dhruv.Sood
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class RetailShipmentCreationProcessorTest {

    @Mock
    OpenShipmentTransactionService openShipmentTransactionService;

    @Mock
    RetailShipmentCreationProcessorMapper retailShipmentCreationProcessorMapper;

    @InjectMocks
    private RetailShipmentCreationProcessor retailShipmentCreationProcessor;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Mock
    public ConfirmShipmentTransactionService confirmShipmentTransactionService;

    @Mock
    private RetailLocationDataProvider locationDataProvider;
    
    @Mock
    private AppConfig applicationConfig;

    @Mock
    private ConfirmShipmentRepository confirmShipmentRepository;
    
    @Mock
    private TransactionRouter<ShipmentConfirmationRequest, ConfirmedShipmentData> checkoutRouter;

    private static final String LOCATIONID = "LASA";
    private static final String TRACKINGID = "794973860000";
    private static final String ID = "123456789";
    private static final String TIME_STAMP = "2020-07-30T11:55:28.687+05:30";
    private static final String OPERATIONAL_DATE = "2020-10-09";
    private static final String REFERENCE_ID_REQUIRED = "The referenceId is missing.";
    private static final String LOCATION_ID_REQUIRED = "The locationId is missing.";
    private static final String TRACKING_ID_REQUIRED = "The trackingId is missing.";
    private static final String SOLDSHIPMENT_ID_REQUIRED = "The id is missing.";
    private static final String SOLDSHIPMENT_TYPE_REQUIRED = "The type is missing.";
    private static final String SOLDSHIPMENT_OPERATIONALDATE_REQUIRED = "The operationaldate is missing.";

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#createOpenShipment(CreateOpenShipmentRequestData)}
     * then it's executed successfully
     */
    @Test
    public void testCreateOpenShipment() {

        CreateOpenShipmentRequest createOpenShipmentRequest = createRequest();
        CreateOpenShipmentResponse createOpenShipmentResponse = mock(CreateOpenShipmentResponse.class);
        OpenShipmentResponse openShipmentResponse = mock(OpenShipmentResponse.class);
        RequestedShipmentDetail requestedShipmentDetail = mock(RequestedShipmentDetail.class);
        WorkstationDetail workstationDetail = mock(WorkstationDetail.class);

        when(openShipmentTransactionService.create(requestedShipmentDetail, workstationDetail))
                .thenReturn(openShipmentResponse);
        when(retailShipmentCreationProcessorMapper.toDto(openShipmentResponse)).thenReturn(createOpenShipmentResponse);
        when(retailShipmentCreationProcessorMapper.toModel(createOpenShipmentRequest))
                .thenReturn(requestedShipmentDetail);
        when(retailShipmentCreationProcessorMapper.updateWorkstationDetail(createOpenShipmentRequest))
                .thenReturn(workstationDetail);
        when(locationDataProvider.getContextDetails(any(String.class)))
                .thenReturn(new LocationContextDetail("123456", "123456", "123456", "123456", "FXE", "NQAA"));

        CreateOpenShipmentResponse response =
                retailShipmentCreationProcessor.createOpenShipment(createOpenShipmentRequest);

        assertThat(response, notNullValue());
        verify(openShipmentTransactionService, times(1)).create(any(RequestedShipmentDetail.class),
                any(WorkstationDetail.class));
        verify(retailShipmentCreationProcessorMapper, times(1)).toDto(any(OpenShipmentResponse.class));
        verify(retailShipmentCreationProcessorMapper, times(1)).toModel(any(CreateOpenShipmentRequest.class));
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#createOpenShipment(CreateOpenShipmentRequestData)} and
     * home delivery details are not provided then error is occurred and handled accordingly
     */
    @Test
    public void testCreateOpenShipmentWdHomeDeliveryDetailNull() {

        CreateOpenShipmentRequest createOpenShipmentRequest = createRequest();
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .setHomeDeliveryDetail(null);

        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo("The " + ServiceConstant.HOME_DELIVERY_DETAIL + " is missing."));

        retailShipmentCreationProcessor.createOpenShipment(createOpenShipmentRequest);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveOpenShipment(String, String, String)} then
     * it's executed successfully
     */
    @Test
    public void testRetrieveOpenShipment() {

        CreateOpenShipmentResponse createOpenShipmentResponse = mock(CreateOpenShipmentResponse.class);
        OpenShipmentResponse openShipmentResponse = mock(OpenShipmentResponse.class);
        when(openShipmentTransactionService.retrieveOpenShipment(any(String.class), any(String.class),
                any(String.class))).thenReturn(openShipmentResponse);
        when(retailShipmentCreationProcessorMapper.toDto(openShipmentResponse)).thenReturn(createOpenShipmentResponse);
        RetrieveOpenShipmentResponse response =
                retailShipmentCreationProcessor.retrieveOpenShipment("3456", "LASA", "794973860000");

        assertThat(response, notNullValue());
        verify(retailShipmentCreationProcessorMapper, times(1)).toDto(any(OpenShipmentResponse.class));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveOpenShipment(String, String, String)} with
     * details then it's executed successfully
     */
    @Test
    public void testRetrieveOpenShipmentWithDetails() {

        CreateOpenShipmentResponse createOpenShipmentResponse = mock(CreateOpenShipmentResponse.class);
        OpenShipmentResponse openShipmentResponse = mock(OpenShipmentResponse.class);
        OpenShipmentCore openShipmentCore = mock(OpenShipmentCore.class);
        ShippingPaymentDetail paymentDetails = mock(ShippingPaymentDetail.class);
        Payor payor = mock(Payor.class);
        when(openShipmentTransactionService.retrieveOpenShipment(any(String.class), any(String.class),
                any(String.class))).thenReturn(openShipmentResponse);
        when(retailShipmentCreationProcessorMapper.toDto(openShipmentResponse)).thenReturn(createOpenShipmentResponse);
        when(createOpenShipmentResponse.getOpenShipment()).thenReturn(openShipmentCore);
        when(openShipmentCore.getPaymentDetails()).thenReturn(paymentDetails);
        when(paymentDetails.getPayor()).thenReturn(payor);
        when(paymentDetails.getPaymentType()).thenReturn(PaymentType.NON_ACCOUNT);
        RetrieveOpenShipmentResponse response =
                retailShipmentCreationProcessor.retrieveOpenShipment("3456", "LASA", "794973860000");

        assertThat(response, notNullValue());
        verify(retailShipmentCreationProcessorMapper, times(1)).toDto(any(OpenShipmentResponse.class));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveOpenShipment(String, String, String)} with
     * payment type account then it's executed successfully
     */
    @Test
    public void testRetrieveOpenShipmentAccount() {

        CreateOpenShipmentResponse createOpenShipmentResponse = mock(CreateOpenShipmentResponse.class);
        OpenShipmentResponse openShipmentResponse = mock(OpenShipmentResponse.class);
        OpenShipmentCore openShipmentCore = mock(OpenShipmentCore.class);
        ShippingPaymentDetail paymentDetails = mock(ShippingPaymentDetail.class);
        Payor payor = mock(Payor.class);
        when(openShipmentTransactionService.retrieveOpenShipment(any(String.class), any(String.class),
                any(String.class))).thenReturn(openShipmentResponse);
        when(retailShipmentCreationProcessorMapper.toDto(openShipmentResponse)).thenReturn(createOpenShipmentResponse);
        when(createOpenShipmentResponse.getOpenShipment()).thenReturn(openShipmentCore);
        when(openShipmentCore.getPaymentDetails()).thenReturn(paymentDetails);
        when(paymentDetails.getPayor()).thenReturn(payor);
        when(paymentDetails.getPaymentType()).thenReturn(PaymentType.ACCOUNT);
        RetrieveOpenShipmentResponse response =
                retailShipmentCreationProcessor.retrieveOpenShipment("3456", "LASA", "794973860000");

        assertThat(response, notNullValue());
        verify(retailShipmentCreationProcessorMapper, times(1)).toDto(any(OpenShipmentResponse.class));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveOpenShipment(String, String, String)} with
     * empty payment detail then it's executed successfully
     */
    @Test
    public void testRetrieveOpenShipmentEmptyPaymentDetail() {

        CreateOpenShipmentResponse createOpenShipmentResponse = mock(CreateOpenShipmentResponse.class);
        OpenShipmentResponse openShipmentResponse = mock(OpenShipmentResponse.class);
        OpenShipmentCore openShipmentCore = mock(OpenShipmentCore.class);
        when(openShipmentTransactionService.retrieveOpenShipment(any(String.class), any(String.class),
                any(String.class))).thenReturn(openShipmentResponse);
        when(retailShipmentCreationProcessorMapper.toDto(openShipmentResponse)).thenReturn(createOpenShipmentResponse);
        when(createOpenShipmentResponse.getOpenShipment()).thenReturn(openShipmentCore);
        RetrieveOpenShipmentResponse response =
                retailShipmentCreationProcessor.retrieveOpenShipment("3456", "LASA", "794973860000");

        assertThat(response, notNullValue());
        verify(retailShipmentCreationProcessorMapper, times(1)).toDto(any(OpenShipmentResponse.class));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveOpenShipment(String, String, String)} with
     * empty payor then it's executed successfully
     */
    @Test
    public void testRetrieveOpenShipmentEmptyPayor() {

        CreateOpenShipmentResponse createOpenShipmentResponse = mock(CreateOpenShipmentResponse.class);
        OpenShipmentResponse openShipmentResponse = mock(OpenShipmentResponse.class);
        OpenShipmentCore openShipmentCore = mock(OpenShipmentCore.class);
        ShippingPaymentDetail paymentDetails = mock(ShippingPaymentDetail.class);
        when(openShipmentTransactionService.retrieveOpenShipment(any(String.class), any(String.class),
                any(String.class))).thenReturn(openShipmentResponse);
        when(retailShipmentCreationProcessorMapper.toDto(openShipmentResponse)).thenReturn(createOpenShipmentResponse);
        when(createOpenShipmentResponse.getOpenShipment()).thenReturn(openShipmentCore);
        when(openShipmentCore.getPaymentDetails()).thenReturn(paymentDetails);
        RetrieveOpenShipmentResponse response =
                retailShipmentCreationProcessor.retrieveOpenShipment("3456", "LASA", "794973860000");

        assertThat(response, notNullValue());
        verify(retailShipmentCreationProcessorMapper, times(1)).toDto(any(OpenShipmentResponse.class));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveOpenShipment(String, String, String)} and
     * referenceId is not provided then error is occurred and handled accordingly
     */
    @Test
    public void testRetrieveOpenShipmentRefIdNull() {

        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo(REFERENCE_ID_REQUIRED));

        retailShipmentCreationProcessor.retrieveOpenShipment(null, "LASA", "794973860000");
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveOpenShipment(String, String, String)} and
     * locationId is not provided then error is occurred and handled accordingly
     */
    @Test
    public void testRetrieveOpenShipmentLocIdNull() {

        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo(LOCATION_ID_REQUIRED));

        retailShipmentCreationProcessor.retrieveOpenShipment("3456", null, "794973860000");
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveOpenShipment(String, String, String)} and
     * trackingId is not provided then error is occurred and handled accordingly
     */
    @Test
    public void testRetrieveOpenShipmentTrkIdNull() {

        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo(TRACKING_ID_REQUIRED));

        retailShipmentCreationProcessor.retrieveOpenShipment("3456", "LASA", null);
    }

    /**
     * Test for bulk checkout success.
     */
    @Test
    public void testConfirmShipmentsCheckoutForSuccess() {

        ShipmentCheckoutRequest checkoutRequest = createShipmentCheckoutRequest();
        ShipmentConfirmationRequest confirmRequest = mock(ShipmentConfirmationRequest.class);
        ConfirmedShipmentData confrimData = mock(ConfirmedShipmentData.class);
        ShipmentTransactionDetail transactionDetail = mock(ShipmentTransactionDetail.class);

        when(confirmShipmentTransactionService.confirmShipment(confirmRequest))
                .thenReturn(confrimData);
        when(retailShipmentCreationProcessorMapper.toDto(confrimData)).thenReturn(transactionDetail);
        when(retailShipmentCreationProcessorMapper.toModel(any(ConfirmShipmentRequestData.class),
                any(LocationContextDetail.class))).thenReturn(confirmRequest);

        when(locationDataProvider.getContextDetails(any(String.class)))
                .thenReturn(new LocationContextDetail("123456", "123456", "123456", "123456", "FXE", "NQAA"));

        ShipmentCheckoutResponse checkoutResource = retailShipmentCreationProcessor.checkoutShipments(checkoutRequest);

        assertThat(checkoutResource, notNullValue());
        verify(confirmShipmentTransactionService, times(1)).confirmShipment(confirmRequest);

    }

    /**
     * Test for bulk checkout when no line item data.
     */
    @Test
    public void testConfirmShipmentsCheckoutForEmptyList() {

        ShipmentCheckoutRequest checkoutRequest = createShipmentCheckoutRequest();
        checkoutRequest.getShipmentCheckoutRequest()
                .setRequestedShipment(new ArrayList<>());

        ShipmentCheckoutResponse checkoutResource = retailShipmentCreationProcessor.checkoutShipments(checkoutRequest);

        assertThat(checkoutResource.getShipmentCheckout(), nullValue());

    }

    /**
     * Test for bulk checkout when service return ShipmentProcessorTerminalException.
     */
    @Test
    public void testConfirmShipmentsCheckoutForShipmentProcessorTerminalException() {

        ShipmentCheckoutRequest checkoutRequest = createShipmentCheckoutRequest();
        ShipmentConfirmationRequest confirmRequest = mock(ShipmentConfirmationRequest.class);

        when(confirmShipmentTransactionService.confirmShipment(confirmRequest))
                .thenThrow(new ServiceProcessorTerminalException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE));
        when(retailShipmentCreationProcessorMapper.toModel(any(ConfirmShipmentRequestData.class),
                any(LocationContextDetail.class))).thenReturn(confirmRequest);

        when(applicationConfig.isAsyncCheckout()).thenReturn(false);
        when(locationDataProvider.getContextDetails(any(String.class)))
                .thenReturn(new LocationContextDetail("123456", "123456", "123456", "123456", "FXE", "NQAA"));
        Map<String, ConfirmedShipmentData> transactionResponse=new HashMap<>();
        transactionResponse.put("12345", mock(ConfirmedShipmentData.class));
        ShipmentCheckoutResponse checkoutResource = retailShipmentCreationProcessor.checkoutShipments(checkoutRequest);

        assertThat(checkoutResource, notNullValue());
        assertThat(checkoutResource.getShipmentCheckout(), notNullValue());
        assertThat(checkoutResource.getShipmentCheckout()
                .getTransactionDetails()
                .get(0)
                .getPackageNotifications(), notNullValue());
        verify(confirmShipmentTransactionService, times(1)).confirmShipment(confirmRequest);

    }

    /**
     * Test for bulk checkout when async true  service return ShipmentProcessorTerminalException.
     */
    @Test
    public void testConfirmShipmentsCheckoutAsyncForShipmentProcessorTerminalException() {

        ShipmentCheckoutRequest checkoutRequest = createShipmentCheckoutRequest();
        ShipmentConfirmationRequest confirmRequest = mock(ShipmentConfirmationRequest.class);

        when(retailShipmentCreationProcessorMapper.toModel(any(ConfirmShipmentRequestData.class),
                any(LocationContextDetail.class))).thenReturn(confirmRequest);

        when(applicationConfig.isAsyncCheckout()).thenReturn(true);
        when(locationDataProvider.getContextDetails(any(String.class)))
                .thenReturn(new LocationContextDetail("123456", "123456", "123456", "123456", "FXE", "NQAA"));
        Map<String, ConfirmedShipmentData> transactionResponse=new HashMap<>();
        transactionResponse.put("12345", mock(ConfirmedShipmentData.class));
        when(checkoutRouter.process(any())).thenReturn(transactionResponse);
     
        ShipmentCheckoutResponse checkoutResource = retailShipmentCreationProcessor.checkoutShipments(checkoutRequest);

        assertThat(checkoutResource, notNullValue());
        assertThat(checkoutResource.getShipmentCheckout(), notNullValue());

    }

    /**
     * Test for bulk checkout when location service get failed and return
     * ShipmentProcessorTerminalException.
     */
    @Test(expected = ServiceProcessorTerminalException.class)
    public void testConfirmShipmentsCheckoutWhenLocationCallGetFailed() {

        ShipmentCheckoutRequest checkoutRequest = createShipmentCheckoutRequest();

        when(locationDataProvider.getContextDetails(any(String.class)))
                .thenThrow(new ServiceProcessorTerminalException(ServiceErrorCode.INVALID_LOCATION_ID));

        retailShipmentCreationProcessor.checkoutShipments(checkoutRequest);

    }

    /**
     * Test for bulk checkout when location service get failed and return
     * ShipmentProcessorTerminalException.
     */
    @Test(expected = ServiceProcessorTerminalException.class)
    public void testCreateOpenShipmentWhenLocationCallGetFailed() {

        CreateOpenShipmentRequest createOpenShipmentRequest = createRequest();
        when(locationDataProvider.getContextDetails(any(String.class)))
                .thenThrow(new ServiceProcessorTerminalException(ServiceErrorCode.INVALID_LOCATION_ID));

        retailShipmentCreationProcessor.createOpenShipment(createOpenShipmentRequest);

    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#deleteOpenShipment(String, String, String)} then it's
     * executed successfully
     */
    @Test
    public void testDeleteOpenShipment() {

        String locationId = "LASA";
        String trackingId = "794973860000";
        String referenceId = "3456";

        doNothing().when(openShipmentTransactionService)
                .deleteOpenShipment(referenceId, locationId, trackingId);

        DeleteOpenShipmentResponse response =
                retailShipmentCreationProcessor.deleteOpenShipment(referenceId, locationId, trackingId);
        assertThat(response, notNullValue());
        verify(openShipmentTransactionService, times(1)).deleteOpenShipment(referenceId, locationId, trackingId);
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#deleteOpenShipment(String, String, String)} and
     * referenceId is not provided then error is occurred and handled accordingly
     */
    @Test
    public void testDeleteOpenShipmentRefIdNull() {

        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo(REFERENCE_ID_REQUIRED));

        retailShipmentCreationProcessor.deleteOpenShipment(null, "LASA", "794973860000");
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#deleteOpenShipment(String, String, String)} and
     * locationId is not provided then error is occurred and handled accordingly
     */
    @Test
    public void testDeleteOpenShipmentLocIdNull() {

        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo(LOCATION_ID_REQUIRED));

        retailShipmentCreationProcessor.deleteOpenShipment("3456", null, "794973860000");
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#deleteOpenShipment(String, String, String)} and
     * trackingId is not provided then error is occurred and handled accordingly
     */
    @Test
    public void testDeleteOpenShipmentTrkIdNull() {

        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo(TRACKING_ID_REQUIRED));

        retailShipmentCreationProcessor.deleteOpenShipment("3456", "LASA", null);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveSoldShipmentSummaries(String, String)} then
     * it's executed successfully
     */
    @Test
    public void testRetrieveSoldShipmentSummaries() {

        List<SoldShipmentSummary> soldShipmentDetails = createShipmentDetails(5);
        when(confirmShipmentRepository.getShipmentsByLocation(LOCATIONID, OPERATIONAL_DATE))
                .thenReturn(soldShipmentDetails);
        when(retailShipmentCreationProcessorMapper.toSoldShipmentSummaryDto(soldShipmentDetails))
                .thenReturn(new SoldShipmentSummaryResponse());

        SoldShipmentSummaryResponse response =
                retailShipmentCreationProcessor.retrieveSoldShipmentSummaries(LOCATIONID, OPERATIONAL_DATE);

        assertThat(response, notNullValue());
        verify(confirmShipmentRepository, times(1)).getShipmentsByLocation(LOCATIONID, OPERATIONAL_DATE);
        verify(retailShipmentCreationProcessorMapper, times(1)).toSoldShipmentSummaryDto(soldShipmentDetails);
    }
        
    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveSoldShipmentSummaries(String, String)} then it
     * returns corresponding error.
     */
    @Test
    public void testRetrieveSoldShipmentSummariesWhenSoldShipmentDetailsNull() {

        when(confirmShipmentRepository.getShipmentsByLocation(LOCATIONID, OPERATIONAL_DATE)).thenReturn(null);

        SoldShipmentSummaryResponse response =
                retailShipmentCreationProcessor.retrieveSoldShipmentSummaries(LOCATIONID, OPERATIONAL_DATE);

        assertThat(response, nullValue());
        verify(confirmShipmentRepository, times(1)).getShipmentsByLocation(LOCATIONID, OPERATIONAL_DATE);
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveSoldShipmentSummaries(String, String)} and
     * operationaldate is not provided then error is occurred and handled accordingly
     */
    @Test
    public void testRetrieveSoldShipmentsWdOperationalDateNull() {

        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo(SOLDSHIPMENT_OPERATIONALDATE_REQUIRED));

        retailShipmentCreationProcessor.retrieveSoldShipmentSummaries(LOCATIONID, null);
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveSoldShipmentSummaries(String, String)} and
     * locationId is not provided then error is occurred and handled accordingly
     */
    @Test
    public void testRetrieveSoldShipmentsWdLocationIdNull() {

        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo(LOCATION_ID_REQUIRED));

        retailShipmentCreationProcessor.retrieveSoldShipmentSummaries(null, OPERATIONAL_DATE);
    }

    /**
     * Test case to validate that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveSoldShipmentSummaries(String, String)} and
     * endpoint and it throws TransientDbAccessException then it's handled successfully
     */
    @Test
    public void testRetrieveSoldShipmentTransientException() {

        ClientTransientException clientTransientException =
                new ClientTransientException(ServiceErrorCode.DB_SEARCH_FAILURE);
        when(confirmShipmentRepository.getShipmentsByLocation(LOCATIONID, OPERATIONAL_DATE))
                .thenThrow(clientTransientException);
        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_SEARCH_FAILURE.getMessage()));
        retailShipmentCreationProcessor.retrieveSoldShipmentSummaries(LOCATIONID, OPERATIONAL_DATE);
    }

    /**
     * Test case to validate that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveSoldShipmentSummaries(String, String)} and
     * endpoint and it throws TerminalDbAccessException then it's handled successfully
     */
    @Test
    public void testRetrieveSoldShipmentTerminalException() {

        ClientTerminalException clientTerminalException =
                new ClientTerminalException(ServiceErrorCode.DB_SEARCH_FAILURE);
        when(confirmShipmentRepository.getShipmentsByLocation(LOCATIONID, OPERATIONAL_DATE))
                .thenThrow(clientTerminalException);
        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_SEARCH_FAILURE.getMessage()));
        retailShipmentCreationProcessor.retrieveSoldShipmentSummaries(LOCATIONID, OPERATIONAL_DATE);
    }

    /**
     * Test case to validate that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveSoldShipmentSummaries(String, String)} and
     * endpoint and it throws HystrixException then it's handled successfully
     */
    @Test
    public void testRetrieveSoldShipmentHystrixException() {

        when(confirmShipmentRepository.getShipmentsByLocation(LOCATIONID, OPERATIONAL_DATE))
                .thenThrow(HystrixRuntimeException.class);
        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.SYSTEM_UNAVAILABLE.getMessage()));
        retailShipmentCreationProcessor.retrieveSoldShipmentSummaries(LOCATIONID, OPERATIONAL_DATE);
    }

    /**
     * Create SoldShipmentSummary's List
     * 
     * @return
     */
    private List<SoldShipmentSummary> createShipmentDetails(
            int size) {

        List<SoldShipmentSummary> shipmentDetails = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            SoldShipmentSummary shipmentDetail = new SoldShipmentSummary();
            List<PackageLineItemsDetail> packageLineItems = new ArrayList<>();
            PackageLineItemsDetail packageLineItemsDetail = new PackageLineItemsDetail();
            packageLineItemsDetail.setMaster(i % 2 == 0);
            TrackingSequenceDetail trackingSequenceDetail = new TrackingSequenceDetail();
            trackingSequenceDetail.setTrackingNumber("12345678912" + i);
            trackingSequenceDetail.setBarcode("12345678912" + i);
            trackingSequenceDetail.setSequence(i);
            packageLineItemsDetail.setTrackingId(trackingSequenceDetail);
            packageLineItems.add(packageLineItemsDetail);
            shipmentDetail.setGuid(("1234567899" + i));
            shipmentDetail.setPackageLineItems(packageLineItems);
            shipmentDetail.setMasterTrackingId(trackingSequenceDetail);
            shipmentDetail.setRetailTransactionId("12345678" + i);
            shipmentDetail.setServiceType("FIRST_OVERNIGHT");
            shipmentDetail.setStatus("CREATED");
            shipmentDetail.setTransactionDateTime(TIME_STAMP);
            CarrierDetail carrierDetail = new CarrierDetail();
            if (i % 2 == 0) {
                carrierDetail.setCarrierCode("FDXE");
                carrierDetail.setOpco("EXPRESS");
            } else {
                carrierDetail.setCarrierCode("FDXG");
                carrierDetail.setOpco("GROUND");
            }

            shipmentDetail.setCarrierDetails(carrierDetail);
            shipmentDetails.add(shipmentDetail);
        }
        return shipmentDetails;
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveSoldShipments(String, String, ShipmentQueryType)}
     * by trackingid then it's executed successfully
     */
    @Test
    public void testRetrieveSoldShipments() {


        List<SoldShipmentDetail> soldShipments = new ArrayList<>(0);
        when(confirmShipmentRepository.getSoldShipmentByTrackingNumber(TRACKINGID, LOCATIONID))
                .thenReturn(soldShipments);
        when(retailShipmentCreationProcessorMapper.toDto(soldShipments)).thenReturn(new SoldShipmentResponse());
        SoldShipmentResponse response =
                retailShipmentCreationProcessor.retrieveSoldShipments(LOCATIONID, TRACKINGID, ShipmentQueryType.TRACK);
        assertThat(response, notNullValue());
        verify(confirmShipmentRepository, times(1)).getSoldShipmentByTrackingNumber(TRACKINGID, LOCATIONID);
        verify(retailShipmentCreationProcessorMapper, times(1)).toDto(soldShipments);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveSoldShipments(String, String, ShipmentQueryType)}
     * by barcode then it's executed successfully
     */
    @Test
    public void testRetrieveSoldShipmentsByBarCode() {

        List<SoldShipmentDetail> soldShipments = new ArrayList<>(0);
        when(confirmShipmentRepository.getSoldShipmentByBarCode(ID, LOCATIONID)).thenReturn(soldShipments);
        when(retailShipmentCreationProcessorMapper.toDto(soldShipments)).thenReturn(new SoldShipmentResponse());
        SoldShipmentResponse response =
                retailShipmentCreationProcessor.retrieveSoldShipments(LOCATIONID, ID, ShipmentQueryType.BARCODE);
        assertThat(response, notNullValue());
        verify(confirmShipmentRepository, times(1)).getSoldShipmentByBarCode(ID, LOCATIONID);
        verify(retailShipmentCreationProcessorMapper, times(1)).toDto(soldShipments);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveSoldShipments(String, String, ShipmentQueryType)}
     * by Guid then it's executed successfully
     */
    @Test
    public void testRetrieveSoldShipmentsByGuid() {

        List<SoldShipmentDetail> soldShipments = new ArrayList<>(0);
        when(confirmShipmentRepository.getSoldShipmentByGuid(ID, LOCATIONID)).thenReturn(soldShipments);
        when(retailShipmentCreationProcessorMapper.toDto(soldShipments)).thenReturn(new SoldShipmentResponse());
        SoldShipmentResponse response =
                retailShipmentCreationProcessor.retrieveSoldShipments(LOCATIONID, ID, ShipmentQueryType.REFID);
        assertThat(response, notNullValue());
        verify(confirmShipmentRepository, times(1)).getSoldShipmentByGuid(ID, LOCATIONID);
        verify(retailShipmentCreationProcessorMapper, times(1)).toDto(soldShipments);
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#getSoldShipmentByBarCode(id,locId)} endpoint
     * then it's handled successfully
     */
    @Test
    public void testgetSoldShipmentByBarCode() {

        List<SoldShipmentDetail> soldShipments = new ArrayList<>();
        when(confirmShipmentRepository.getSoldShipmentByBarCode(ID, LOCATIONID)).thenReturn(soldShipments);
        when(retailShipmentCreationProcessorMapper.toDto(soldShipments)).thenReturn(new SoldShipmentResponse());
        SoldShipmentResponse response =
                retailShipmentCreationProcessor.retrieveSoldShipments(LOCATIONID, ID, ShipmentQueryType.BARCODE);
        assertThat(response, notNullValue());
        verify(confirmShipmentRepository, times(1)).getSoldShipmentByBarCode(ID, LOCATIONID);
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#getSoldShipmentByBarCode(id,locId)} endpoint and
     * it throws TransientDbAccessException then it's handled successfully
     */
    @Test
    public void testgetSoldShipmentByBarCodeException() {

        ClientTransientException clientTransientException =
                new ClientTransientException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);
        when(confirmShipmentRepository.getSoldShipmentByBarCode(ID, LOCATIONID)).thenThrow(clientTransientException);
        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_SEARCH_FAILURE.getMessage()));
        retailShipmentCreationProcessor.retrieveSoldShipments(LOCATIONID, ID, ShipmentQueryType.BARCODE);
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#getSoldShipmentByBarCode(id,locId)} endpoint and
     * it throws TerminalDbAccessException then it's handled successfully
     */
    @Test
    public void testgetSoldShipmentByBarCodeTerminalException() {

        ClientTerminalException clientTransientException =
                new ClientTerminalException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);
        when(confirmShipmentRepository.getSoldShipmentByBarCode(ID, LOCATIONID)).thenThrow(clientTransientException);
        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_SEARCH_FAILURE.getMessage()));
        retailShipmentCreationProcessor.retrieveSoldShipments(LOCATIONID, ID, ShipmentQueryType.BARCODE);
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#getSoldShipmentByBarCode(id,locId)} endpoint and
     * it throws HystrixException then it's handled successfully
     */
    @Test
    public void testgetSoldShipmentByBarCodeHystrixException() {

        when(confirmShipmentRepository.getSoldShipmentByBarCode(ID, LOCATIONID))
                .thenThrow(HystrixRuntimeException.class);
        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.SYSTEM_UNAVAILABLE.getMessage()));
        retailShipmentCreationProcessor.retrieveSoldShipments(LOCATIONID, ID, ShipmentQueryType.BARCODE);
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#getSoldShipmentByGuid(id,locId)} endpoint and it
     * throws TransientDbAccessException then it's handled successfully
     */
    @Test
    public void testgetSoldShipmentByGuidException() {

        ClientTransientException clientTransientException =
                new ClientTransientException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);
        when(confirmShipmentRepository.getSoldShipmentByGuid(ID, LOCATIONID)).thenThrow(clientTransientException);
        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_SEARCH_FAILURE.getMessage()));
        retailShipmentCreationProcessor.retrieveSoldShipments(LOCATIONID, ID, ShipmentQueryType.REFID);
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#getSoldShipmentByGuid(id,locId)} endpoint and it
     * throws TerminalDbAccessException then it's handled successfully
     */
    @Test
    public void testgetSoldShipmentByGuidTerminalException() {

        ClientTerminalException clientTransientException =
                new ClientTerminalException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);
        when(confirmShipmentRepository.getSoldShipmentByGuid(ID, LOCATIONID)).thenThrow(clientTransientException);
        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_SEARCH_FAILURE.getMessage()));
        retailShipmentCreationProcessor.retrieveSoldShipments(LOCATIONID, ID, ShipmentQueryType.REFID);
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#getSoldShipmentByGuid(id,locId)} endpoint and it
     * throws HystrixException then it's handled successfully
     */
    @Test
    public void testgetSoldShipmentByGuidHystrixException() {

        when(confirmShipmentRepository.getSoldShipmentByGuid(ID, LOCATIONID)).thenThrow(HystrixRuntimeException.class);
        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.SYSTEM_UNAVAILABLE.getMessage()));
        retailShipmentCreationProcessor.retrieveSoldShipments(LOCATIONID, ID, ShipmentQueryType.REFID);
    }


    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#getSoldShipmentByTrackingNumber(id,locId)}
     * endpoint and it throws TransientDbAccessException then it's handled successfully
     */
    @Test
    public void testgetSoldShipmentByTrackingNumberException() {

        ClientTransientException clientTransientException =
                new ClientTransientException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);
        when(confirmShipmentRepository.getSoldShipmentByTrackingNumber(TRACKINGID, LOCATIONID))
                .thenThrow(clientTransientException);
        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_SEARCH_FAILURE.getMessage()));
        retailShipmentCreationProcessor.retrieveSoldShipments(LOCATIONID, TRACKINGID, ShipmentQueryType.TRACK);
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#getSoldShipmentByTrackingNumber(id,locId)}
     * endpoint and it throws TerminalDbAccessException then it's handled successfully
     */
    @Test
    public void testgetSoldShipmentByTrackingNumberTerminalException() {

        ClientTerminalException clientTransientException =
                new ClientTerminalException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);
        when(confirmShipmentRepository.getSoldShipmentByTrackingNumber(TRACKINGID, LOCATIONID))
                .thenThrow(clientTransientException);
        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_SEARCH_FAILURE.getMessage()));
        retailShipmentCreationProcessor.retrieveSoldShipments(LOCATIONID, TRACKINGID, ShipmentQueryType.TRACK);
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#getSoldShipmentByTrackingNumber(id,locId)}
     * endpoint and it throws HystrixException then it's handled successfully
     */
    @Test
    public void testgetSoldShipmentByTrackingNumberHystrixException() {

        when(confirmShipmentRepository.getSoldShipmentByTrackingNumber(TRACKINGID, LOCATIONID))
                .thenThrow(HystrixRuntimeException.class);
        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.SYSTEM_UNAVAILABLE.getMessage()));
        retailShipmentCreationProcessor.retrieveSoldShipments(LOCATIONID, TRACKINGID, ShipmentQueryType.TRACK);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveSoldShipmentst(String, String, ShipmentQueryType)}
     * and id is not provided then error is occurred and handled accordingly
     */
    @Test
    public void testRetrieveSoldShipmentsIdNull() {

        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo(SOLDSHIPMENT_ID_REQUIRED));

        retailShipmentCreationProcessor.retrieveSoldShipments(LOCATIONID, null, ShipmentQueryType.TRACK);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#retrieveSoldShipments(String, String, ShipmentQueryType)}
     * and type is not provided then error is occurred and handled accordingly
     */
    @Test
    public void testRetrieveSoldShipmentsTypeNull() {

        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo(SOLDSHIPMENT_TYPE_REQUIRED));

        retailShipmentCreationProcessor.retrieveSoldShipments(LOCATIONID, TRACKINGID, null);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#deleteSoldShipments(DeleteSoldShipmentRequest)} then
     * it's executed successfully
     */
    @Test
    public void testDeleteSoldShipments() {

        DeleteSoldShipmentRequest deleteSoldShipmentRequest = createDeleteSoldShipmentRequest();
        DeletedSoldShipmentDetail deleteSoldShipmentDetail = createDeleteSoldShipmentResponse();
        LocationContextDetail context =
                new LocationContextDetail("123456", "123456", "123456", "123456", "FXE", "NQAA");
        when(locationDataProvider.getContextDetails(any(String.class))).thenReturn(context);
        when(retailShipmentCreationProcessorMapper
                .toDeletSoldShipmentData(deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest(), context))
                        .thenReturn(new DeleteSoldShipmentData());
        when(confirmShipmentTransactionService.stop(any(DeleteSoldShipmentData.class)))
                .thenReturn(deleteSoldShipmentDetail);

        when(retailShipmentCreationProcessorMapper.toDto(deleteSoldShipmentDetail))
                .thenReturn(new DeletedSoldShipmentResponse());
        DeletedSoldShipmentResponse response =
                retailShipmentCreationProcessor.deleteSoldShipments(deleteSoldShipmentRequest);
        assertThat(response, notNullValue());
        verify(locationDataProvider, times(1)).getContextDetails(any(String.class));
        verify(retailShipmentCreationProcessorMapper, times(1)).toDto(deleteSoldShipmentDetail);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#deleteSoldShipments(DeleteSoldShipmentRequest)} then
     * it's executed successfully
     */
    @Test
    public void testDeleteSoldShipmentsWithAlert() {

        DeleteSoldShipmentRequest deleteSoldShipmentRequest = createDeleteSoldShipmentRequest();
        DeletedSoldShipmentDetail deleteSoldShipmentDetail = createDeleteSoldShipmentResponse();
        deleteSoldShipmentDetail.setUpdationError(true);
        LocationContextDetail context =
                new LocationContextDetail("123456", "123456", "123456", "123456", "FXE", "NQAA");
        when(locationDataProvider.getContextDetails(any(String.class))).thenReturn(context);
        when(retailShipmentCreationProcessorMapper
                .toDeletSoldShipmentData(deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest(), context))
                        .thenReturn(new DeleteSoldShipmentData());
        when(confirmShipmentTransactionService.stop(any(DeleteSoldShipmentData.class)))
                .thenReturn(deleteSoldShipmentDetail);

        when(retailShipmentCreationProcessorMapper.toDto(deleteSoldShipmentDetail))
                .thenReturn(new DeletedSoldShipmentResponse());
        DeletedSoldShipmentResponse response =
                retailShipmentCreationProcessor.deleteSoldShipments(deleteSoldShipmentRequest);
        assertThat(response, notNullValue());
        verify(locationDataProvider, times(1)).getContextDetails(any(String.class));
        verify(retailShipmentCreationProcessorMapper, times(1)).toDto(deleteSoldShipmentDetail);
    }

    
    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#deleteSoldShipments(DeleteSoldShipmentRequest)} then
     * it throws exception.
     */
    @Test
    public void testDeleteSoldShipmentsWithException() {

        DeleteSoldShipmentRequest deleteSoldShipmentRequest = createDeleteSoldShipmentRequest();
        when(locationDataProvider.getContextDetails(any(String.class)))
                .thenThrow(ServiceProcessorTerminalException.class);
        expectedEx.expect(Exception.class);
        DeletedSoldShipmentResponse response =
                retailShipmentCreationProcessor.deleteSoldShipments(deleteSoldShipmentRequest);
        assertThat(response, notNullValue());
        verify(locationDataProvider, times(1)).getContextDetails(any(String.class));
    }
    
    /**
     * Test for confirmedShipments success.
     */
    @Test
    public void testConfirmedShipmentsForSuccess() {

        ConfirmedShipmentRequest confirmShipmentRequest = createConfirmedShipmentRequest();
        ShipmentConfirmationRequest confirmRequest = mock(ShipmentConfirmationRequest.class);
        ConfirmedShipmentData confrimData = mock(ConfirmedShipmentData.class);

        when(confirmShipmentTransactionService.confirmShipment(confirmRequest)).thenReturn(confrimData);
        when(retailShipmentCreationProcessorMapper.toModel(any(ConfirmShipmentRequestData.class),
                any(LocationContextDetail.class))).thenReturn(confirmRequest);

        when(locationDataProvider.getContextDetails(any(String.class)))
                .thenReturn(new LocationContextDetail("123456", "123456", "123456", "123456", "FXE", "NQAA"));

        ConfirmedShipmentResponse confirmedShipmentResponse =
                retailShipmentCreationProcessor.confirmShipments(confirmShipmentRequest);

        assertThat(confirmedShipmentResponse, notNullValue());
        verify(confirmShipmentTransactionService, times(1)).confirmShipment(confirmRequest);

    }

    /**
     * Method to create mock response for DeleteSoldShipmentDetail.
     * 
     * @return DeleteSoldShipmentDetail
     */
    private DeletedSoldShipmentDetail createDeleteSoldShipmentResponse() {

        DeletedSoldShipmentDetail deletedSoldShipmentDetail=new DeletedSoldShipmentDetail();
        deletedSoldShipmentDetail.setCarrierType("EXPRESS");
        deletedSoldShipmentDetail.setTrackingNumber("23456788");
        deletedSoldShipmentDetail.setUpdationError(false);
        return deletedSoldShipmentDetail;
    }

    /**
     * Method to create mock request for DeleteSoldShipmentRequest.
     * 
     * @return DeleteSoldShipmentRequest
     */
    private DeleteSoldShipmentRequest createDeleteSoldShipmentRequest() {

        DeleteSoldShipmentRequest deleteSoldShipmentRequest = new DeleteSoldShipmentRequest();
        deleteSoldShipmentRequest.setDeleteSoldShipmentsRequest(createMockDeleteSoldShipmentRequestData());

        return deleteSoldShipmentRequest;
    }

    /**
     * Method to create mock delete sold shipment request data
     * 
     * @return
     */
    private DeleteSoldShipmentRequestData createMockDeleteSoldShipmentRequestData() {

        DeleteSoldShipmentRequestData deleteSoldShipmentRequestData = new DeleteSoldShipmentRequestData();
        deleteSoldShipmentRequestData.setHeaderDetails(mockRequestHeaderDetail());
        deleteSoldShipmentRequestData.setLocationDetails(new LocationInfo("DFWD", mockAddress()));
        deleteSoldShipmentRequestData.setTrackingId("794984201540");
        deleteSoldShipmentRequestData.setRetailTransactionId("794984201540");
        deleteSoldShipmentRequestData.setCustomerName(new PersonName("Mr.", "Customer"));

        return deleteSoldShipmentRequestData;
    }

    /**
     * Method to build the mock instance of {@link RequestHeaderDetail} object .
     * 
     * @return
     */
    private RequestHeaderDetail mockRequestHeaderDetail() {

        return new RequestHeaderDetail("548796554", "8745896532", ZonedDateTime.now()
                .toString(), mockWorkstationDetails());
    }

    /**
     * Method to build the mock instance of {@link WorkstationDetails} object.
     * 
     * @return
     */
    private WorkstationDetails mockWorkstationDetails() {

        return new WorkstationDetails("BTC-50", "ROSA-01", "123456", AppName.FASTLANE, "SSFE");
    }

    /**
     * Prepare mock address object.
     * 
     * @return
     */
    public Address mockAddress() {

        return new Address(new ArrayList<String>(), "Dallas", "TX", "75024", "US", AddressClassification.HOME);

    }

    /**
     * create ShipmentCheckoutRequest
     * 
     * @return
     */
    private ShipmentCheckoutRequest createShipmentCheckoutRequest() {

        RequestHeaderDetail headerDetails = new RequestHeaderDetail("", "20172", "2020-9-10", new WorkstationDetails());
        LocationDetail locDetail = new LocationDetail();
        locDetail.setTransactionLocationInfo(createLocationInfo());
        locDetail.setCityCenterAccountNumber("1111111111");
        locDetail.setTransactionLocationInfo(createLocationInfo());
        OpenShipmentIdentifier indeIdentifier =
                new OpenShipmentIdentifier("123456", new TrackingSequence("789456359874", null, 1));
        locDetail.setTransactionLocationInfo(createLocationInfo());

        ShipmentCheckoutRequestData request = new ShipmentCheckoutRequestData(headerDetails, locDetail,
                Arrays.asList(indeIdentifier), new LabelSpecification(), new ShippingPaymentDetail());

        return new ShipmentCheckoutRequest(request);
    }

    /**
     * Method is used to create LocationInfo
     * 
     * @return
     */
    private LocationInfo createLocationInfo() {

        LocationInfo locInfo = new LocationInfo();
        locInfo.setCode("DFWD");
        Address address = new Address();
        address.setAddressClassification(AddressClassification.HOME);
        address.setCity("Dallas");
        address.setCountryCode("USA");
        address.setPostalCode("7408");
        address.setStateOrProvinceCode("state");
        address.setStreetLines(new ArrayList<>());
        locInfo.setAddress(address);

        return locInfo;
    }

    /**
     * Create openShipmentRequest
     */
    private CreateOpenShipmentRequest createRequest() {

        RequestHeaderDetail headerDetails = new RequestHeaderDetail("", "20172", "2020-9-10", new WorkstationDetails());
        LocationDetail locDetail = new LocationDetail();
        locDetail.setTransactionLocationInfo(createLocationInfo());
        locDetail.setCityCenterAccountNumber("1111111111");
        locDetail.setTransactionLocationInfo(createLocationInfo());
        CreateOpenShipmentRequestData data =
                new CreateOpenShipmentRequestData(headerDetails, locDetail,
                        createMockRequestedShipment(), new ShipmentPaymentOption(RequestedPaymentType.ACCOUNT, "",
                                ResponsibleParty.RECIPIENT, PaymentSource.SPOS),
                        new LabelSpecification(),  createPerson());

        return new CreateOpenShipmentRequest(data);
    }
    
    /**
     * Create Person
     * 
     * @return
     */
    private Person createPerson() {

        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("3112556789");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);
        CustomerEmailDetail custEmailDtls = new CustomerEmailDetail();
        custEmailDtls.setEmailAddress("mainEmail123@testmail.com");
        custEmailDtls.setAlternateEmailAddress("alternateEmail123@testmail.com");
        CustomerContact custContact = new CustomerContact();
        custContact.setPhoneNumberDetails(phoneNumberDetails);
        custContact.setPersonName("Mr Customer");
        custContact.setEmailDetails(custEmailDtls);

        return new Person(createAddress(), custContact);
    }

    /**
     * Create Address
     * 
     * @return
     */
    private Address createAddress() {

        List<String> streetLines=new ArrayList<>();
        streetLines.add("9400 WADE BLVD");
        
        return new Address(streetLines, "Dallas", "TX", "75024", "US", AddressClassification.HOME);

    }
    
    /**
     * Create mock ConfirmedShipmentRequest
     * 
     * @return
     */
    private ConfirmedShipmentRequest createConfirmedShipmentRequest() {

        ConfirmedShipmentRequest confirmShipmentRequest = new ConfirmedShipmentRequest();
        RequestHeaderDetail headerDetails = new RequestHeaderDetail("", "20172", "2020-9-10", new WorkstationDetails());
        LocationDetail locDetail = new LocationDetail();
        locDetail.setTransactionLocationInfo(createLocationInfo());
        locDetail.setCityCenterAccountNumber("1111111111");
        locDetail.setTransactionLocationInfo(createLocationInfo());
        OpenShipmentIdentifier identifier =
                new OpenShipmentIdentifier("123456", new TrackingSequence("789456359874", null, 1));
        locDetail.setTransactionLocationInfo(createLocationInfo());

        ConfirmShipmentRequestData confirmedShipmentRequestData = new ConfirmShipmentRequestData(headerDetails,
                locDetail, identifier, new LabelSpecification(), new ShippingPaymentDetail());
        confirmShipmentRequest.setConfirmedShipmentRequest(confirmedShipmentRequestData);

        return confirmShipmentRequest;
    }
    
    /**
     * Prepare mock RequestedShipment object.
     * 
     * @return
     */
    private RequestedShipment createMockRequestedShipment() {

        RequestedShipment requestedShipment = new RequestedShipment();
        requestedShipment.setServiceType("GROUND_HOME_DELIVERY");
        requestedShipment.setPackagingType("YOUR_PACKAGING");
        requestedShipment.setShipper(createMockParty("Mr. Shipper"));
        requestedShipment.setRecipient(createMockParty("Mr. Recipient"));
        requestedShipment.setSpecialServicesRequested(createSpecialServiceRequested());
        requestedShipment.setRequestedPackageLineItems(createMockRequestedPackageLineItems());
        return requestedShipment;
    }

    /**
     * Prepare mock ShipmentSpecialServiceDescription object.
     * 
     * @return
     */
    private ShipmentSpecialServiceDescription createSpecialServiceRequested() {

        ShipmentSpecialServiceDescription shipmentSpecialServiceDesc = new ShipmentSpecialServiceDescription();

        List<SpecialServiceDescription> specialServiceDescriptionList = new ArrayList<>();
        specialServiceDescriptionList.add(new SpecialServiceDescription("HOME_DELIVERY_PREMIUM", null, null, null));
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("9948587320");
        phoneNumber.setExtension("01202");

        HomeDeliveryDetail homeDeliveryDetail = new HomeDeliveryDetail();
        homeDeliveryDetail.setHomeDeliveryType(HomeDeliveryType.DATE_CERTAIN);
        homeDeliveryDetail.setDeliveryDate("2021-10-11");
        homeDeliveryDetail.setPhoneNumber(phoneNumber);

        shipmentSpecialServiceDesc.setHomeDeliveryDetail(homeDeliveryDetail);
        shipmentSpecialServiceDesc.setSpecialServices(specialServiceDescriptionList);

        return shipmentSpecialServiceDesc;
    }

    /**
     * Prepare mock Party object.
     * 
     * @return
     */
    private Party createMockParty(
            String personName) {

        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("3112556789");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);
        ShippingContact shippingContact = new ShippingContact();
        shippingContact.setPhoneNumberDetails(phoneNumberDetails);
        shippingContact.setPersonName(personName);

        return new Party(createAddress(), shippingContact);
    }

    /**
     * Prepare mock List<RequestedPackageLineItems> object.
     * 
     * @return
     */
    private List<RequestedPackageLineItem> createMockRequestedPackageLineItems() {

        Weight weight = new Weight();
        weight.setValue("20");
        weight.setUnits(WeightUnit.KG);

        List<RequestedPackageLineItem> listOfRequestedPackageLineItems = new ArrayList<>();

        RequestedPackageLineItem requestedPackageLineItems = new RequestedPackageLineItem();
        requestedPackageLineItems.setDimensions(null);
        requestedPackageLineItems.setInsuredValue(null);
        requestedPackageLineItems.setSpecialHandlingDetail(PackageSpecialHandlingType.CUSTOMER);
        requestedPackageLineItems.setSpecialServicesRequested(new ArrayList<>());
        requestedPackageLineItems.setWeight(weight);

        listOfRequestedPackageLineItems.add(requestedPackageLineItems);

        return listOfRequestedPackageLineItems;
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#createOpenShipment(createOpenShipmentRequest)} and
     * Customer streetLines each element is null then error is occurred and handled accordingly
     */
    @Test
    public void testCreateOpenShipmentCustomerStreetLinesCodeEachElementNull() {

        CreateOpenShipmentRequest createOpenShipmentRequest = createRequest();
        List<String> streetLines = new ArrayList<>();
        streetLines.add(null);
        streetLines.add(null);
        streetLines.add(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress()
                .setStreetLines(streetLines);
        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo("The streetLines is missing."));
        retailShipmentCreationProcessor.createOpenShipment(createOpenShipmentRequest);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#createOpenShipment(createOpenShipmentRequest)} and
     * Shipper is not provided then successful response is handled
     */
    @Test
    public void testCreateOpenShipmentShipperNull() {

        CreateOpenShipmentRequest createOpenShipmentRequest = createRequest();
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .setShipper(null);
        CreateOpenShipmentResponse createOpenShipmentResponse = mock(CreateOpenShipmentResponse.class);
        OpenShipmentResponse openShipmentResponse = mock(OpenShipmentResponse.class);
        RequestedShipmentDetail requestedShipmentDetail = mock(RequestedShipmentDetail.class);
        WorkstationDetail workstationDetail = mock(WorkstationDetail.class);

        when(openShipmentTransactionService.create(requestedShipmentDetail, workstationDetail))
                .thenReturn(openShipmentResponse);
        when(retailShipmentCreationProcessorMapper.toDto(openShipmentResponse)).thenReturn(createOpenShipmentResponse);
        when(retailShipmentCreationProcessorMapper.toModel(createOpenShipmentRequest))
                .thenReturn(requestedShipmentDetail);
        when(retailShipmentCreationProcessorMapper.updateWorkstationDetail(createOpenShipmentRequest))
                .thenReturn(workstationDetail);
        when(locationDataProvider.getContextDetails(any(String.class)))
                .thenReturn(new LocationContextDetail("123456", "123456", "123456", "123456", "FXE", "NQAA"));

        CreateOpenShipmentResponse response =
                retailShipmentCreationProcessor.createOpenShipment(createOpenShipmentRequest);

        assertThat(response, notNullValue());
        verify(openShipmentTransactionService, times(1)).create(any(RequestedShipmentDetail.class),
                any(WorkstationDetail.class));
        verify(retailShipmentCreationProcessorMapper, times(1)).toDto(any(OpenShipmentResponse.class));
        verify(retailShipmentCreationProcessorMapper, times(1)).toModel(any(CreateOpenShipmentRequest.class));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#createOpenShipment(createOpenShipmentRequest)} and
     * Shipper streetLines is provided with allowed maximum length then successful response is
     * handled
     */
    @Test
    public void testCreateOpenShipmentShipperStreetLinesBoundaryMaxLength() {

        CreateOpenShipmentRequest createOpenShipmentRequest = createRequest();
        CreateOpenShipmentResponse createOpenShipmentResponse = mock(CreateOpenShipmentResponse.class);
        OpenShipmentResponse openShipmentResponse = mock(OpenShipmentResponse.class);
        RequestedShipmentDetail requestedShipmentDetail = mock(RequestedShipmentDetail.class);
        WorkstationDetail workstationDetail = mock(WorkstationDetail.class);

        List<String> streetLines = new ArrayList<>();
        streetLines.add("House number 1234, Gill Colony, CHD");
        streetLines.add("House number 1236, Gill Colony, CHD");
        streetLines.add("House number 1237, Gill Colony, CHD");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .setStreetLines(streetLines);

        when(openShipmentTransactionService.create(requestedShipmentDetail, workstationDetail))
                .thenReturn(openShipmentResponse);
        when(retailShipmentCreationProcessorMapper.toDto(openShipmentResponse)).thenReturn(createOpenShipmentResponse);
        when(retailShipmentCreationProcessorMapper.toModel(createOpenShipmentRequest))
                .thenReturn(requestedShipmentDetail);
        when(retailShipmentCreationProcessorMapper.updateWorkstationDetail(createOpenShipmentRequest))
                .thenReturn(workstationDetail);
        when(locationDataProvider.getContextDetails(any(String.class)))
                .thenReturn(new LocationContextDetail("123456", "123456", "123456", "123456", "FXE", "NQAA"));

        CreateOpenShipmentResponse response =
                retailShipmentCreationProcessor.createOpenShipment(createOpenShipmentRequest);

        assertThat(response, notNullValue());
        assertThat(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getStreetLines()
                .size(), equalTo(3));
        assertThat(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getStreetLines()
                .get(0)
                .length(), equalTo(35));
        verify(openShipmentTransactionService, times(1)).create(any(RequestedShipmentDetail.class),
                any(WorkstationDetail.class));
        verify(retailShipmentCreationProcessorMapper, times(1)).toDto(any(OpenShipmentResponse.class));
        verify(retailShipmentCreationProcessorMapper, times(1)).toModel(any(CreateOpenShipmentRequest.class));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#createOpenShipment(createOpenShipmentRequest)} and
     * Recipient streetLines is provided with allowed maximum length then successful response is
     * handled
     */
    @Test
    public void testCreateOpenShipmentRecipientStreetLinesBoundaryMaxLength() {

        CreateOpenShipmentRequest createOpenShipmentRequest = createRequest();
        CreateOpenShipmentResponse createOpenShipmentResponse = mock(CreateOpenShipmentResponse.class);
        OpenShipmentResponse openShipmentResponse = mock(OpenShipmentResponse.class);
        RequestedShipmentDetail requestedShipmentDetail = mock(RequestedShipmentDetail.class);
        WorkstationDetail workstationDetail = mock(WorkstationDetail.class);

        List<String> streetLines = new ArrayList<>();
        streetLines.add("House number 1234, Gill Colony, CHD");
        streetLines.add("House number 1236, Gill Colony, CHD");
        streetLines.add("House number 1237, Gill Colony, CHD");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .setStreetLines(streetLines);

        when(openShipmentTransactionService.create(requestedShipmentDetail, workstationDetail))
                .thenReturn(openShipmentResponse);
        when(retailShipmentCreationProcessorMapper.toDto(openShipmentResponse)).thenReturn(createOpenShipmentResponse);
        when(retailShipmentCreationProcessorMapper.toModel(createOpenShipmentRequest))
                .thenReturn(requestedShipmentDetail);
        when(retailShipmentCreationProcessorMapper.updateWorkstationDetail(createOpenShipmentRequest))
                .thenReturn(workstationDetail);
        when(locationDataProvider.getContextDetails(any(String.class)))
                .thenReturn(new LocationContextDetail("123456", "123456", "123456", "123456", "FXE", "NQAA"));

        CreateOpenShipmentResponse response =
                retailShipmentCreationProcessor.createOpenShipment(createOpenShipmentRequest);

        assertThat(response, notNullValue());
        assertThat(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getStreetLines()
                .size(), equalTo(3));
        assertThat(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getStreetLines()
                .get(0)
                .length(), equalTo(35));
        verify(openShipmentTransactionService, times(1)).create(any(RequestedShipmentDetail.class),
                any(WorkstationDetail.class));
        verify(retailShipmentCreationProcessorMapper, times(1)).toDto(any(OpenShipmentResponse.class));
        verify(retailShipmentCreationProcessorMapper, times(1)).toModel(any(CreateOpenShipmentRequest.class));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationProcessor#createOpenShipment(createOpenShipmentRequest)} and
     * Customer streetLines is provided with allowed maximum length then successful response is
     * handled
     */
    @Test
    public void testCreateOpenShipmentCustomerStreetLinesBoundaryMaxLength() {

        CreateOpenShipmentRequest createOpenShipmentRequest = createRequest();
        CreateOpenShipmentResponse createOpenShipmentResponse = mock(CreateOpenShipmentResponse.class);
        OpenShipmentResponse openShipmentResponse = mock(OpenShipmentResponse.class);
        RequestedShipmentDetail requestedShipmentDetail = mock(RequestedShipmentDetail.class);
        WorkstationDetail workstationDetail = mock(WorkstationDetail.class);

        List<String> streetLines = new ArrayList<>();
        streetLines.add("House number 1234, Gill Colony, CHD");
        streetLines.add("House number 1236, Gill Colony, CHD");
        streetLines.add("House number 1237, Gill Colony, CHD");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress()
                .setStreetLines(streetLines);

        when(openShipmentTransactionService.create(requestedShipmentDetail, workstationDetail))
                .thenReturn(openShipmentResponse);
        when(retailShipmentCreationProcessorMapper.toDto(openShipmentResponse)).thenReturn(createOpenShipmentResponse);
        when(retailShipmentCreationProcessorMapper.toModel(createOpenShipmentRequest))
                .thenReturn(requestedShipmentDetail);
        when(retailShipmentCreationProcessorMapper.updateWorkstationDetail(createOpenShipmentRequest))
                .thenReturn(workstationDetail);
        when(locationDataProvider.getContextDetails(any(String.class)))
                .thenReturn(new LocationContextDetail("123456", "123456", "123456", "123456", "FXE", "NQAA"));

        CreateOpenShipmentResponse response =
                retailShipmentCreationProcessor.createOpenShipment(createOpenShipmentRequest);

        assertThat(response, notNullValue());
        assertThat(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress()
                .getStreetLines()
                .size(), equalTo(3));
        assertThat(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress()
                .getStreetLines()
                .get(0)
                .length(), equalTo(35));
        verify(openShipmentTransactionService, times(1)).create(any(RequestedShipmentDetail.class),
                any(WorkstationDetail.class));
        verify(retailShipmentCreationProcessorMapper, times(1)).toDto(any(OpenShipmentResponse.class));
        verify(retailShipmentCreationProcessorMapper, times(1)).toModel(any(CreateOpenShipmentRequest.class));
    }
    
}

