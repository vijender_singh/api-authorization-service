package com.fedex.rscs.processor;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.fedex.rscs.dto.AccountType;
import com.fedex.rscs.dto.Address;
import com.fedex.rscs.dto.AddressClassification;
import com.fedex.rscs.dto.AppName;
import com.fedex.rscs.dto.CarrierCodeType;
import com.fedex.rscs.dto.ConfirmShipmentRequestData;
import com.fedex.rscs.dto.ConfirmedShipmentResource;
import com.fedex.rscs.dto.CreateOpenShipmentRequest;
import com.fedex.rscs.dto.CreateOpenShipmentRequestData;
import com.fedex.rscs.dto.CreateOpenShipmentResponse;
import com.fedex.rscs.dto.Currency;
import com.fedex.rscs.dto.CustomerContact;
import com.fedex.rscs.dto.CustomerEmailDetail;
import com.fedex.rscs.dto.DeleteSoldShipmentRequestData;
import com.fedex.rscs.dto.DeletedSoldShipmentResponse;
import com.fedex.rscs.dto.HomeDeliveryType;
import com.fedex.rscs.dto.HomeDeliveryDetail;
import com.fedex.rscs.dto.ImageType;
import com.fedex.rscs.dto.LabelSpecification;
import com.fedex.rscs.dto.LabelType;
import com.fedex.rscs.dto.LocationDetail;
import com.fedex.rscs.dto.LocationInfo;
import com.fedex.rscs.dto.OpenShipmentIdentifier;
import com.fedex.rscs.dto.Party;
import com.fedex.rscs.dto.PaymentSource;
import com.fedex.rscs.dto.PaymentType;
import com.fedex.rscs.dto.Payor;
import com.fedex.rscs.dto.Person;
import com.fedex.rscs.dto.PersonName;
import com.fedex.rscs.dto.PhoneNumber;
import com.fedex.rscs.dto.PhoneNumberDetail;
import com.fedex.rscs.dto.Price;
import com.fedex.rscs.dto.PrintingOrientation;
import com.fedex.rscs.dto.RequestHeaderDetail;
import com.fedex.rscs.dto.RequestedPackageLineItem;
import com.fedex.rscs.dto.RequestedShipment;
import com.fedex.rscs.dto.ResponsibleParty;
import com.fedex.rscs.dto.ShipmentQueryType;
import com.fedex.rscs.dto.ShipmentSpecialServiceDescription;
import com.fedex.rscs.dto.ShipmentTransactionDetail;
import com.fedex.rscs.dto.ShippingContact;
import com.fedex.rscs.dto.ShippingPaymentDetail;
import com.fedex.rscs.dto.SoldShipmentResponse;
import com.fedex.rscs.dto.SoldShipmentSummaryResponse;
import com.fedex.rscs.dto.SpecialServiceDescription;
import com.fedex.rscs.dto.StockType;
import com.fedex.rscs.dto.TrackingSequence;
import com.fedex.rscs.dto.Usage;
import com.fedex.rscs.dto.WorkstationDetails;
import com.fedex.rscs.model.CarrierDetail;
import com.fedex.rscs.model.CommitInfo;
import com.fedex.rscs.model.ContactDetail;
import com.fedex.rscs.model.DeleteSoldShipmentData;
import com.fedex.rscs.model.DeleteSoldShipmentDetail;
import com.fedex.rscs.model.DeletedSoldShipmentDetail;
import com.fedex.rscs.model.LocationContextDetail;
import com.fedex.rscs.model.PackageLineItemsDetail;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.SoldShipmentDetail;
import com.fedex.rscs.model.SoldShipmentSummary;
import com.fedex.rscs.model.WeightDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.common.LocationDataDetail;
import com.fedex.rscs.model.common.PersonDetail;
import com.fedex.rscs.model.cshp.CompleteShipmentDetail;
import com.fedex.rscs.model.cshp.CompletedPackageData;
import com.fedex.rscs.model.cshp.ConfirmedPackageData;
import com.fedex.rscs.model.cshp.ConfirmedShipmentData;
import com.fedex.rscs.model.cshp.OpenShipmentResponse;
import com.fedex.rscs.model.cshp.ShippingDocument;
import com.fedex.rscs.model.cshp.ShippingRateData;
import com.fedex.rscs.model.cshp.TrackingSequenceDetail;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

/**
 * Test Class for Processor Mapper tests
 * 
 * @author 3798910
 *
 */
public class RetailShipmentCreationProcessorMapperTest {

    private RetailShipmentCreationProcessorMapper mapper;

    @Before
    public void setup() {

        this.mapper = new RetailShipmentCreationProcessorMapper();
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toModel(createOpenShipmentRequest)} to set the model
     * object fields from dto.
     * 
     */
    @Test
    public void testToModel() {

        // arrange
        CreateOpenShipmentRequest createRequest = buildCreateOpenShipmentRequest();
        
        // act
        RequestedShipmentDetail reply = mapper.toModel(createRequest);

        // verify
        assertThat(reply.getSender(), notNullValue());
        assertThat(reply.getRecipient(), notNullValue());
        assertThat(reply.getPackageLineItems(), hasSize(1));
        assertThat(reply.getShipTimestamp(), equalTo("07-12-20018"));
        assertThat(reply.getPackageLineItems()
                .get(0)
                .getInsuredValue(), notNullValue());
        assertThat(reply.getLabelSpecification(), notNullValue());
        assertThat(reply.getCustomer(), notNullValue());
        assertThat(reply.getCustomer()
                .getContact(), notNullValue());
        assertThat(reply.getCustomer()
                .getContact()
                .getAlternateEmailId(), notNullValue());
    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toModel(createOpenShipmentRequest)} to set the model
     * object fields from dto object missing shipment special service.
     * 
     */
    @Test
    public void testToModelMissingShipSpecServices() {

        // arrange
        CreateOpenShipmentRequest createRequest = buildCreateOpenShipmentRequest();
        createRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .setSpecialServicesRequested(null);

        // act
        RequestedShipmentDetail reply = mapper.toModel(createRequest);

        // verify
        assertThat(reply.getSender(), notNullValue());
        assertThat(reply.getRecipient(), notNullValue());
        assertThat(reply.getPackageLineItems(), hasSize(1));
        assertThat(reply.getShipTimestamp(), equalTo("07-12-20018"));
        assertThat(reply.getPackageLineItems()
                .get(0)
                .getInsuredValue(), notNullValue());
        assertThat(reply.getLabelSpecification(), notNullValue());
    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toModel(createOpenShipmentRequest)} to set the model
     * object fields from dto object missing special services.
     * 
     */
    @Test
    public void testToModelMissingSpecServices() {

        // arrange
        CreateOpenShipmentRequest createRequest = buildCreateOpenShipmentRequest();
        createRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested().setSpecialServices(null);

        // act
        RequestedShipmentDetail reply = mapper.toModel(createRequest);

        // verify
        assertThat(reply.getSender(), notNullValue());
        assertThat(reply.getRecipient(), notNullValue());
        assertThat(reply.getPackageLineItems(), hasSize(1));
        assertThat(reply.getShipTimestamp(), equalTo("07-12-20018"));
        assertThat(reply.getPackageLineItems()
                .get(0)
                .getInsuredValue(), notNullValue());
        assertThat(reply.getLabelSpecification(), notNullValue());
        assertThat(reply.getSpecialServicesRequested().getHoldAtLocationDetail(), nullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toModel(createOpenShipmentRequest)} to set the model
     * object fields from dto object with HAL as shipment special service, missing shipper and customer
     * contact information.
     * 
     */
    @Test
    public void testToModelWithHalAndMissingShipperCustomerContact() {

        // arrange
        CreateOpenShipmentRequest createRequest = buildCreateOpenShipmentRequest();
        createRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .setShipper(null);
        createRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .setContact(null);
        List<SpecialServiceDescription> specServesc = new ArrayList<>();
        SpecialServiceDescription specDes = new SpecialServiceDescription();
        specDes.setSpecialServiceType("HOLD_AT_LOCATION");
        specServesc.add(specDes);
        ShipmentSpecialServiceDescription shipSpecDes = new ShipmentSpecialServiceDescription();
        shipSpecDes.setSpecialServices(specServesc);
        createRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .setSpecialServicesRequested(shipSpecDes);

        // act
        RequestedShipmentDetail reply = mapper.toModel(createRequest);

        // verify
        assertThat(reply.getSender(), notNullValue());
        assertThat(reply.getRecipient(), notNullValue());
        assertThat(reply.getCustomer()
                .getContact()
                .getCompanyName(), nullValue());
        assertThat(reply.getPackageLineItems(), hasSize(1));
        assertThat(reply.getShipTimestamp(), equalTo("07-12-20018"));
        assertThat(reply.getPackageLineItems()
                .get(0)
                .getInsuredValue(), notNullValue());
        assertThat(reply.getLabelSpecification(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toModel(createOpenShipmentRequest)} to set the
     * model object fields from dto successfully when SpecialServicesRequested is set to null .
     * 
     */
    @Test
    public void testToModelWdSpclSrvcReqstdNull() {

        // arrange
        CreateOpenShipmentRequest createRequest = buildCreateOpenShipmentRequest();
        createRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .setSpecialServicesRequested(null);
        // act
        RequestedShipmentDetail reply = mapper.toModel(createRequest);

        // verify
        assertThat(reply.getSender(), notNullValue());
        assertThat(reply.getRecipient(), notNullValue());
        assertThat(reply.getPackageLineItems(), hasSize(1));
        assertThat(reply.getShipTimestamp(), equalTo("07-12-20018"));
        assertThat(reply.getPackageLineItems()
                .get(0)
                .getInsuredValue(), notNullValue());
        assertThat(reply.getLabelSpecification(), notNullValue());
        assertThat(reply.getCustomer(), notNullValue());
        assertThat(reply.getCustomer()
                .getContact(), notNullValue());
        assertThat(reply.getCustomer()
                .getContact()
                .getAlternateEmailId(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toModel(createOpenShipmentRequest)} to set the
     * model object fields from dto successfully when home delivery details is set to null .
     * 
     */
    @Test
    public void testToModelWdHDDetailsdNull() {

        // arrange
        CreateOpenShipmentRequest createRequest = buildCreateOpenShipmentRequest();

        createRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .setHomeDeliveryDetail(null);
        // act
        RequestedShipmentDetail reply = mapper.toModel(createRequest);

        // verify
        assertThat(reply.getSender(), notNullValue());
        assertThat(reply.getRecipient(), notNullValue());
        assertThat(reply.getPackageLineItems(), hasSize(1));
        assertThat(reply.getShipTimestamp(), equalTo("07-12-20018"));
        assertThat(reply.getPackageLineItems()
                .get(0)
                .getInsuredValue(), notNullValue());
        assertThat(reply.getLabelSpecification(), notNullValue());
        assertThat(reply.getCustomer(), notNullValue());
        assertThat(reply.getCustomer()
                .getContact(), notNullValue());
        assertThat(reply.getCustomer()
                .getContact()
                .getAlternateEmailId(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toModel(createOpenShipmentRequest)} to set the
     * model object fields from dto successfully when home delivery type is set to EVENING .
     * 
     */
    @Test
    public void testToModelWdDlvryTypeEvening() {

        // arrange
        CreateOpenShipmentRequest createRequest = buildCreateOpenShipmentRequest();

        createRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryDetail()
                .setHomeDeliveryType(HomeDeliveryType.EVENING);
        // act
        RequestedShipmentDetail reply = mapper.toModel(createRequest);

        // verify
        assertThat(reply.getSender(), notNullValue());
        assertThat(reply.getRecipient(), notNullValue());
        assertThat(reply.getPackageLineItems(), hasSize(1));
        assertThat(reply.getShipTimestamp(), equalTo("07-12-20018"));
        assertThat(reply.getPackageLineItems()
                .get(0)
                .getInsuredValue(), notNullValue());
        assertThat(reply.getLabelSpecification(), notNullValue());
        assertThat(reply.getCustomer(), notNullValue());
        assertThat(reply.getCustomer()
                .getContact(), notNullValue());
        assertThat(reply.getCustomer()
                .getContact()
                .getAlternateEmailId(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toModel(createOpenShipmentRequest)} to set the
     * model object fields from dto successfully when home delivery phone number is set to null .
     * 
     */
    @Test
    public void testToModelWdHDPhoneNumberNull() {

        // arrange
        CreateOpenShipmentRequest createRequest = buildCreateOpenShipmentRequest();

        createRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryDetail()
                .setPhoneNumber(null);
        // act
        RequestedShipmentDetail reply = mapper.toModel(createRequest);

        // verify
        assertThat(reply.getSender(), notNullValue());
        assertThat(reply.getRecipient(), notNullValue());
        assertThat(reply.getPackageLineItems(), hasSize(1));
        assertThat(reply.getShipTimestamp(), equalTo("07-12-20018"));
        assertThat(reply.getPackageLineItems()
                .get(0)
                .getInsuredValue(), notNullValue());
        assertThat(reply.getLabelSpecification(), notNullValue());
        assertThat(reply.getCustomer(), notNullValue());
        assertThat(reply.getCustomer()
                .getContact(), notNullValue());
        assertThat(reply.getCustomer()
                .getContact()
                .getAlternateEmailId(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toModel(WorkstationDetails)} to set the model object
     * fields from dto.
     * 
     */
    @Test
    public void testToModelWorkstation() {

        WorkstationDetails workDetails = new WorkstationDetails();
        workDetails.setDeviceId("2120");
        workDetails.setAppName(AppName.FASTLANE);

        CreateOpenShipmentRequestData createOpenShipmentRequest = new CreateOpenShipmentRequestData();
        RequestHeaderDetail reqHeadDetail = new RequestHeaderDetail();
        reqHeadDetail.setGuid("1111");
        reqHeadDetail.setTeamMemberId("981840");
        reqHeadDetail.setWorkstationDetails(workDetails);
        createOpenShipmentRequest.setHeaderDetails(reqHeadDetail);
        LocationDetail locDetail = new LocationDetail();
        locDetail.setCityCenterAccountNumber("0300000014");
        locDetail.setGroundAccountNumber("5555");
        createOpenShipmentRequest.setLocationDetails(locDetail);

        CreateOpenShipmentRequest mainOpenShipmentRequest = new CreateOpenShipmentRequest();
        mainOpenShipmentRequest.setCreateOpenShipmentRequest(createOpenShipmentRequest);

        WorkstationDetail workDetail = mapper.updateWorkstationDetail(mainOpenShipmentRequest);
        assertThat(workDetail.getDeviceId(), equalTo("2120"));
        assertThat(workDetail.getAppName(), equalTo("FASTLANE"));
        assertThat(workDetail.getAccountNumber(), equalTo("0300000014"));
        assertThat(workDetail.getGuid(),equalTo("1111"));

    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toDto(openShipmentResponse)} to set the dto object
     * fields from model .
     * 
     */
    @Test
    public void testToDto() {

        // arrange
        OpenShipmentResponse openShipmentResponse = new OpenShipmentResponse();
        CompleteShipmentDetail completeShipmentDetail = new CompleteShipmentDetail();
        ShippingRateData shippingRateData = new ShippingRateData();
        List<CompletedPackageData> completedPackageDetails = new ArrayList<>();
        CompletedPackageData completedPackageData = new CompletedPackageData();
        WeightDetail weight = new WeightDetail();
        weight.setUnit("LB");
        completedPackageData.setWeight(weight);
        completedPackageDetails.add(completedPackageData);
        completeShipmentDetail.setCompletedPackageDetails(completedPackageDetails);
        CommitInfo commitInfo = new CommitInfo();
        commitInfo.setDayOfWeek("FRI");
        commitInfo.setCommitTimestamp("2021-02-09T12:00:00");
        shippingRateData.setCommitDetails(commitInfo);
        completeShipmentDetail.setCarrierCode("FDXE");
        ContactDetail contact = new ContactDetail();
        contact.setPhoneNumber("123456");
        contact.setPhoneExtension("123");
        contact.setPhoneUsage("Primary");
        PartyDetail recepient = new PartyDetail();
        recepient.setContact(contact);
        completeShipmentDetail.setRecipient(recepient);
        openShipmentResponse.setShippingRateData(shippingRateData);
        openShipmentResponse.setCompleteShipmentDetail(completeShipmentDetail);
        CreateOpenShipmentResponse reply = mapper.toDto(openShipmentResponse);
        assertThat(reply, notNullValue());
        assertThat(reply.getOpenShipment(), notNullValue());
        assertThat(reply.getOpenShipment()
                .getCarrierDetails()
                .getCarrierCode(), equalTo(CarrierCodeType.FDXE));
        assertThat(reply.getOpenShipment()
                .getCommitDetails(), notNullValue());
        assertThat(reply.getOpenShipment()
                .getPackageLineItems()
                .get(0), notNullValue());
    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toDto(openShipmentResponse)} to set the dto object
     * fields from model of ground shipment.
     * 
     */
    @Test
    public void testToDtoForGround() {

        // arrange
        OpenShipmentResponse openShipmentResponse = new OpenShipmentResponse();
        CompleteShipmentDetail completeShipmentDetail = new CompleteShipmentDetail();
        completeShipmentDetail.setCarrierCode("FDXG");
        ContactDetail contact =new ContactDetail();
        contact.setPhoneNumber("123456");
        contact.setPhoneExtension("123");
        contact.setPhoneUsage("Primary");
        PartyDetail recepient = new PartyDetail();
        recepient.setContact(contact);
        completeShipmentDetail.setRecipient(recepient);
        openShipmentResponse.setCompleteShipmentDetail(completeShipmentDetail);
        CreateOpenShipmentResponse reply = mapper.toDto(openShipmentResponse);
        assertThat(reply, notNullValue());
        assertThat(reply.getOpenShipment(), notNullValue());
        assertThat(reply.getOpenShipment()
                .getCarrierDetails()
                .getCarrierCode(), equalTo(CarrierCodeType.FDXG));
    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toDto(openShipmentResponse)} to set the dto object
     * fields from model of smartpost shipment.
     * 
     */
    @Test
    public void testToDtoForSmartPost() {

        // arrange
        OpenShipmentResponse openShipmentResponse = new OpenShipmentResponse();
        CompleteShipmentDetail completeShipmentDetail = new CompleteShipmentDetail();
        completeShipmentDetail.setCarrierCode("FXSP");
        ContactDetail contact =new ContactDetail();
        contact.setPhoneNumber("123456");
        contact.setPhoneExtension("123");
        contact.setPhoneUsage("Primary");
        PartyDetail recepient = new PartyDetail();
        recepient.setContact(contact);
        completeShipmentDetail.setRecipient(recepient);
        openShipmentResponse.setCompleteShipmentDetail(completeShipmentDetail);
        CreateOpenShipmentResponse reply = mapper.toDto(openShipmentResponse);
        assertThat(reply, notNullValue());
        assertThat(reply.getOpenShipment(), notNullValue());
        assertThat(reply.getOpenShipment()
                .getCarrierDetails()
                .getCarrierCode(), equalTo(CarrierCodeType.FXSP));
    }
    

    /**
     * test to covert ShipmentConfirmationRequest
     */
    @Test
    public void testConvertToShipmentConfirmationReuest() {

        ConfirmShipmentRequestData confirmRequest = createConfirmShipmentRequest();
        ShipmentConfirmationRequest request = mapper.toModel(confirmRequest,new LocationContextDetail());
        assertThat(request, notNullValue());
        assertThat(request.getReferenceId(), notNullValue());
        assertThat(request.getShipmentTimeStamp(), notNullValue());
        assertThat(request.getWorkstationDetail(), notNullValue());
        assertThat(request.getTransactionLocationId(), notNullValue());
        assertThat(request.getLabelSpecificationDetail(), notNullValue());
        assertThat(request.getPaymentDetail(), notNullValue());
    }


    /**
     * 
     */
    @Test
    public void testConvertToShipmentTransactionDetail() {

        ConfirmedShipmentData shipmentData = createConfirmedShipmentData();

        ShipmentTransactionDetail detail = mapper.toDto(shipmentData);
        assertThat(detail, notNullValue());
        assertThat(detail.getMasterTrackingId(), notNullValue());
        assertThat(detail.getPackageCount(), equalTo(1));
        assertThat(detail.getPackageLineItems(), notNullValue());
    }

    /**
     * Test case to verify that when mapping is made to
     * {@link RetailShipmentCreationProcessorMapper#toDto(String, String, ShipmentQueryType)} then
     * it's executed successfully
     */
    @Test
    public void testToDtoSoldShipment() {

        SoldShipmentResponse resource = mapper.toDto(createSoldShipmentResourceDetails(3));
        assertThat(resource, notNullValue());
        assertThat(resource.getSoldShipments(), hasSize(3));
        assertThat(resource.getAlerts(), hasSize(3));
    }

    /**
     * Test case to verify that when mapping is made to
     * {@link RetailShipmentCreationProcessorMapper#toDto(String, String, ShipmentQueryType)} when
     * empty list is passed then it's executed successfully
     */
    @Test
    public void testToDtoSoldShipmentEmptyList() {

        SoldShipmentResponse resource = mapper.toDto(new ArrayList<>(0));
        assertThat(resource, notNullValue());
        assertThat(resource.getSoldShipments(), nullValue());
    }
    
    /**
     * Test case to verify that when mapping is made to
     * {@link RetailShipmentCreationProcessorMapper#toDto(String, String, ShipmentQueryType)} when no
     * shipper, recipient and customer information is passed then it's executed successfully
     */
    @Test
    public void testToDtoSoldShipmentMissingShipperRecipientCustomerInfo() {

        List<SoldShipmentDetail> soldShipments = createSoldShipmentResourceDetails(3);
        soldShipments.get(0)
                .setShipper(null);
        soldShipments.get(0)
                .setRecipient(null);
        soldShipments.get(0)
                .setCustomer(null);
        soldShipments.get(0)
                .setValidLocation(true);
        SoldShipmentResponse resource = mapper.toDto(soldShipments);
        assertThat(resource, notNullValue());
        assertThat(resource.getSoldShipments(), hasSize(3));
        assertThat(resource.getAlerts(), hasSize(2));
    }

    /**
     * Method is used to create ConfirmShipmentRequest object
     * 
     * @return ConfirmShipmentRequest
     */
    private ConfirmShipmentRequestData createConfirmShipmentRequest() {

        RequestHeaderDetail headerDetail = new RequestHeaderDetail("", "20172", "2020-09-10", new WorkstationDetails());
        LocationDetail locDetail = new LocationDetail();
        locDetail.setCityCenterAccountNumber("1111111111");
        locDetail.setTransactionLocationInfo(createLocationInfo());
        OpenShipmentIdentifier indeIdentifier =
                new OpenShipmentIdentifier("123456", new TrackingSequence("789456359874", null, 1));
        locDetail.setTransactionLocationInfo(createLocationInfo());
        
        return new ConfirmShipmentRequestData(
                headerDetail, locDetail, indeIdentifier, new LabelSpecification(LabelType.OPERATIONAL_LABEL,
                        ImageType.PNG, StockType.STOCK_4X6, PrintingOrientation.TOP_EDGE_OF_TEXT_FIRST),
                createShippingPaymentDetail());
    }


    /**
     * method is used to create ShippingPaymentDetail
     * 
     * @return
     */
    private ShippingPaymentDetail createShippingPaymentDetail() {

        ShippingPaymentDetail paymentDetail = new ShippingPaymentDetail();
        paymentDetail.setAmount(new Price(Currency.USD, 10.00));
        paymentDetail.setPaymentSource(PaymentSource.SPOS);
        paymentDetail.setPaymentType(PaymentType.ACCOUNT);
        paymentDetail.setResponsibleParty(ResponsibleParty.SENDER);
        paymentDetail.setPayor(new Payor(AccountType.FEDEX_EXPRESS, "1111111111", null, null));
        return paymentDetail;
    }

    /**
     * Method is used to create LocationInfo
     * 
     * @return
     */
    private LocationInfo createLocationInfo() {

        LocationInfo locInfo = new LocationInfo();
        locInfo.setCode("DFWD");
        Address address = new Address();
        address.setAddressClassification(AddressClassification.HOME);
        address.setCity("Dallas");
        address.setCountryCode("USA");
        address.setPostalCode("7408");
        address.setStateOrProvinceCode("state");
        address.setStreetLines(new ArrayList<>());
        locInfo.setAddress(address);

        return locInfo;
    }

    /**
     * Method to create openShipment request
     * 
     * @return
     */
    private CreateOpenShipmentRequest buildCreateOpenShipmentRequest() {

        CreateOpenShipmentRequest createRequest = new CreateOpenShipmentRequest();
        createRequest.setCreateOpenShipmentRequest(buildCreateOpenShipmentRequestData());

        return createRequest;
    }

    /**
     * Method to create openShipment request data
     * 
     * @return
     */
    private CreateOpenShipmentRequestData buildCreateOpenShipmentRequestData() {

        CreateOpenShipmentRequestData createOpenShipmentRequestData = new CreateOpenShipmentRequestData();
        RequestedShipment reqShip = new RequestedShipment();
        LocationDetail locDet = new LocationDetail();
        LocationInfo locInfo = new LocationInfo();
        Address address = new Address();
        ShippingContact contact = new ShippingContact();
        CustomerContact custContact = new CustomerContact();
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail(new PhoneNumber("9987876545", "1234"), Usage.PRIMARY);
        phoneNumberDetails.add(phoneNumberDetail);
        CustomerEmailDetail custEmailDtls = new CustomerEmailDetail();
        custEmailDtls.setEmailAddress("mainEmail123@testmail.com");
        custEmailDtls.setAlternateEmailAddress("alternateEmail123@testmail.com");
        contact.setPhoneNumberDetails(phoneNumberDetails);
        custContact.setPhoneNumberDetails(phoneNumberDetails);
        custContact.setEmailDetails(custEmailDtls);
        address.setAddressClassification(AddressClassification.HOME);
        address.setCity("Dallas");
        address.setCountryCode("USA");
        address.setPostalCode("7408");
        address.setStateOrProvinceCode("state");
        address.setStreetLines(new ArrayList<>());
        locInfo.setAddress(address);
        locDet.setServingLocationInfo(locInfo);
        locDet.setTransactionLocationInfo(locInfo);
        createOpenShipmentRequestData.setLocationDetails(locDet);
        RequestHeaderDetail headerDetails = new RequestHeaderDetail();
        headerDetails.setRequestDateTime("07-12-20018");
        createOpenShipmentRequestData.setHeaderDetails(headerDetails);
        Party shipper = new Party();
        shipper.setAddress(address);
        shipper.setContact(contact);
        reqShip.setShipper(shipper);
        Party recepient = new Party();
        recepient.setAddress(address);
        recepient.setContact(contact);
        reqShip.setRecipient(recepient);
        Person customer = new Person();
        customer.setAddress(address);
        customer.setContact(custContact);
        createOpenShipmentRequestData.setCustomer(customer);
        List<RequestedPackageLineItem> requestedPackageLineItems = new ArrayList<>();
        RequestedPackageLineItem reqItems = new RequestedPackageLineItem();
        List<SpecialServiceDescription> specServesc = new ArrayList<>();
        SpecialServiceDescription specDes = new SpecialServiceDescription();
        specDes.setSpecialServiceType("SIGNATURE_OPTION");
        specServesc.add(specDes);
        reqItems.setSpecialServicesRequested(specServesc);
        Price price = new Price();
        price.setAmount(24.7);
        price.setCurrency(Currency.USD);
        reqItems.setInsuredValue(price);
        requestedPackageLineItems.add(reqItems);
        reqShip.setRequestedPackageLineItems(requestedPackageLineItems);
        ShipmentSpecialServiceDescription shipSpecDes = new ShipmentSpecialServiceDescription();
        shipSpecDes.setSpecialServices(specServesc);
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("9948587320");
        phoneNumber.setExtension("01202");
        HomeDeliveryDetail homeDeliveryDetail = new HomeDeliveryDetail();
        homeDeliveryDetail.setHomeDeliveryType(HomeDeliveryType.DATE_CERTAIN);
        homeDeliveryDetail.setDeliveryDate("2021-10-11");
        homeDeliveryDetail.setPhoneNumber(phoneNumber);
        shipSpecDes.setHomeDeliveryDetail(homeDeliveryDetail);
        reqShip.setSpecialServicesRequested(shipSpecDes);
        createOpenShipmentRequestData.setRequestedShipment(reqShip);

        createOpenShipmentRequestData.setLabelSpecification(new LabelSpecification(LabelType.COMMON2D, ImageType.ZPLII,
                StockType.STOCK_4X6, PrintingOrientation.TOP_EDGE_OF_TEXT_FIRST));
        return createOpenShipmentRequestData;
    }

    /**
     * Method is used to create ConfirmedShipmentData
     * 
     * @return
     */
    private ConfirmedShipmentData createConfirmedShipmentData() {

        List<ConfirmedPackageData> packageLineItems = new ArrayList<>();
        ConfirmedPackageData item = new ConfirmedPackageData();
        item.setTrackingId(new TrackingSequenceDetail("789456359874", null, 1));
        item.setLabel(new ShippingDocument());
        packageLineItems.add(item);

        return new ConfirmedShipmentData("FDXE", "EXPRESS",
                new TrackingSequenceDetail("789456359874", null, 1), 1, packageLineItems, null, false);
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toLocationDetailModel(LocationDetail)} to set
     * the model object fields from dto .
     * 
     */
    @Test
    public void testToLocationDetailModel() {

        LocationDetail location = createLocationDetail();
        LocationContextDetail details=new LocationContextDetail();
        LocationDataDetail response = mapper.toLocationDetailModel(location,details);
        assertThat(response, notNullValue());
        assertThat(response.getTransactionLocationInfo(), notNullValue());
        assertThat(response.getTransactionLocationInfo()
                .getAddress(), notNullValue());
        assertThat(response.getTransactionLocationInfo()
                .getCode(), notNullValue());
    }

    /**
     * Method to build LocationDetail object
     * 
     * @return
     */
    private LocationDetail createLocationDetail() {

        LocationDetail locDetail = new LocationDetail();
        locDetail.setCityCenterAccountNumber("0300000014");
        locDetail.setGroundAccountNumber("5555");
        locDetail.setTransactionLocationInfo(createLocationInfo());

        return locDetail;
    }
    
    /**
     * Method is used to create SoldShipmentDetail's List
     * 
     * @return
     */
    private List<SoldShipmentDetail> createSoldShipmentResourceDetails(
            int size) {

        List<SoldShipmentDetail> shipmentDetails = new ArrayList<SoldShipmentDetail>(size);
        for (int i = 0; i < size; i++) {
            SoldShipmentDetail shipmentDetail = new SoldShipmentDetail();
            TrackingSequenceDetail trackingSequenceDetail=new TrackingSequenceDetail();
            trackingSequenceDetail.setBarcode("123456789" + i);
            trackingSequenceDetail.setTrackingNumber("12345678912" + i);
            shipmentDetail.setMasterTrackingId(trackingSequenceDetail);
            shipmentDetail.setGuid("12345" + i);
            CarrierDetail carrierDetail=new CarrierDetail();
            if (i % 2 == 0) {
                carrierDetail.setCarrierCode("FDXE");
                carrierDetail.setOpco("EXPRESS");
            } else {
                carrierDetail.setCarrierCode("FDXG");
                carrierDetail.setOpco("GROUND");
            }
            
            shipmentDetail.setCarrierDetails(carrierDetail);
            PartyDetail shipper = new PartyDetail();
            shipper.setAccountNumber("3834004");
            ContactDetail contactDetail = new ContactDetail();
            contactDetail.setPhoneNumber("123456789");
            contactDetail.setPhoneExtension("1234");
            shipper.setContact(contactDetail);
            shipmentDetail.setShipper(shipper);
            PartyDetail recepient = new PartyDetail();
            recepient.setAccountNumber("3834004");
            ContactDetail recepientContact = new ContactDetail();
            recepientContact.setPhoneNumber("123456789");
            recepientContact.setPhoneExtension("1234");
            recepient.setContact(recepientContact);
            shipmentDetail.setRecipient(recepient);
            PartyDetail customer = new PartyDetail();
            customer.setAccountNumber("3834004");
            ContactDetail customerContact = new ContactDetail();
            customerContact.setPhoneNumber("123456789");
            customerContact.setPhoneNumber("1234");
            customer.setContact(customerContact);
            shipmentDetail.setCustomer(customer);
            shipmentDetails.add(shipmentDetail);
        }
        return shipmentDetails;
    }

    /**
     * Test case to verify that when mapping is made to
     * {@link RetailShipmentCreationProcessorMapper#toSoldShipmentSummaryDto(List<SoldShipmentSummary>)}
     * then it's executed successfully
     */
    @Test
    public void testToDtoSoldShipmentSummaryResponse() {

        SoldShipmentSummaryResponse resource =
                mapper.toSoldShipmentSummaryDto(createSoldShipmentSummaryResponseDetails(3));
        assertThat(resource, notNullValue());
        assertThat(resource.getSoldShipmentSummaries(), hasSize(3));
    }

    /**
     * Test case to verify that when mapping is made to
     * {@link RetailShipmentCreationProcessorMapper#toSoldShipmentSummaryDto(List<SoldShipmentSummary>)}
     * when empty list is passed then it's executed successfully
     */
    @Test
    public void testToDtoSoldShipmentSummaryResponseEmptyList() {

        SoldShipmentSummaryResponse resource = mapper.toSoldShipmentSummaryDto(new ArrayList<>(0));
        assertThat(resource, notNullValue());
        assertThat(resource.getSoldShipmentSummaries(), nullValue());
    }

    /**
     * Create SoldShipmentSummary's List
     * 
     * @return
     */
    private List<SoldShipmentSummary> createSoldShipmentSummaryResponseDetails(
            int size) {

        List<SoldShipmentSummary> shipmentDetails = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            SoldShipmentSummary shipmentDetail = new SoldShipmentSummary();
            List<PackageLineItemsDetail> packageLineItems = new ArrayList<>();
            PackageLineItemsDetail packageLineItemsDetail = new PackageLineItemsDetail();
            packageLineItemsDetail.setMaster(i % 2 == 0);
            TrackingSequenceDetail trackingSequenceDetail = new TrackingSequenceDetail();
            trackingSequenceDetail.setTrackingNumber("12345678912" + i);
            trackingSequenceDetail.setBarcode("12345678912" + i);
            trackingSequenceDetail.setSequence(i);
            packageLineItemsDetail.setTrackingId(trackingSequenceDetail);
            packageLineItems.add(packageLineItemsDetail);
            shipmentDetail.setGuid(("1234567899" + i));
            shipmentDetail.setPackageLineItems(packageLineItems);
            shipmentDetail.setMasterTrackingId(trackingSequenceDetail);
            shipmentDetail.setRetailTransactionId("12345678" + i);
            shipmentDetail.setServiceType("FIRST_OVERNIGHT");
            shipmentDetail.setStatus("CREATED");
            shipmentDetail.setTransactionDateTime("2020-07-30T11:55:28.687+05:30");
            CarrierDetail carrierDetail = new CarrierDetail();
            if (i % 2 == 0) {
                carrierDetail.setCarrierCode("FDXE");
                carrierDetail.setOpco("EXPRESS");
            } else {
                carrierDetail.setCarrierCode("FDXG");
                carrierDetail.setOpco("GROUND");
            }

            shipmentDetail.setCarrierDetails(carrierDetail);
            shipmentDetails.add(shipmentDetail);
        }
        return shipmentDetails;
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toLocationModel(LocationInfo)} to set the model
     * object fields from dto .
     * 
     */
    @Test
    public void testToLocationModel() {

        LocationInfo location = createLocationInfo();
        LocationContextDetail details = new LocationContextDetail();
        LocationDataDetail response = mapper.toLocationModel(location, details);
        assertThat(response, notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toPersonModel(PersonName)} to set the model
     * object fields from dto .
     * 
     */
    @Test
    public void testToPersonModel() {

        PersonName customerName = new PersonName("Ram", "kumar");
        PersonDetail response = mapper.toPersonModel(customerName);
        assertThat(response, notNullValue());
        assertThat(response.getFirstName(), notNullValue());
        assertThat(response.getLastName(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toWorkStationModel(RequestHeaderDetail,LocationContextDetail)}
     * to set the model object fields from dto.
     * 
     */
    @Test
    public void testToWorkStationModel() {

        WorkstationDetails workDetails = new WorkstationDetails();
        workDetails.setDeviceId("2120");
        workDetails.setAppName(AppName.FASTLANE);

        RequestHeaderDetail reqHeadDetail = new RequestHeaderDetail();
        reqHeadDetail.setGuid("1111");
        reqHeadDetail.setTeamMemberId("981840");
        reqHeadDetail.setWorkstationDetails(workDetails);
        LocationContextDetail details = new LocationContextDetail();
        WorkstationDetail workDetail = mapper.toWorkStationModel(reqHeadDetail, details);
        assertThat(workDetail.getDeviceId(), equalTo("2120"));
        assertThat(workDetail.getAppName(), equalTo("FASTLANE"));
        assertThat(workDetail.getGuid(), equalTo("1111"));
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toWorkStationModel(RequestHeaderDetail,LocationContextDetail)}
     * to set the model object fields from dto.
     * 
     */
    @Test
    public void testToWorkStationModelWhenContextNull() {

        WorkstationDetails workDetails = new WorkstationDetails();
        workDetails.setDeviceId("2120");
        workDetails.setAppName(AppName.FASTLANE);

        RequestHeaderDetail reqHeadDetail = new RequestHeaderDetail();
        reqHeadDetail.setGuid("1111");
        reqHeadDetail.setTeamMemberId("981840");
        reqHeadDetail.setWorkstationDetails(workDetails);
        LocationContextDetail details = null;
        WorkstationDetail workDetail = mapper.toWorkStationModel(reqHeadDetail, details);
        assertThat(workDetail.getDeviceId(), equalTo("2120"));
        assertThat(workDetail.getAppName(), equalTo("FASTLANE"));
        assertThat(workDetail.getGuid(), equalTo("1111"));
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toDto(DeleteSoldShipmentDetail)} to set the dto
     * object fields from model.
     * 
     */
    @Test
    public void testDeletedSoldShipmentResponseToDto() {

        DeletedSoldShipmentDetail deleteSoldShipmentDetail = new DeletedSoldShipmentDetail("54321356", "EXPRESS");
        DeletedSoldShipmentResponse response = mapper.toDto(deleteSoldShipmentDetail);
        assertThat(response, notNullValue());
        assertThat(response.getDeletedSoldShipment(), notNullValue());
        assertThat(response.getDeletedSoldShipment()
                .getCarrierType(), notNullValue());
        assertThat(response.getDeletedSoldShipment()
                .getTrackingNumber(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toDto(DeleteSoldShipmentDetail)} to set the dto
     * object fields from model.
     * 
     */
    @Test
    public void testDeletedSoldShipmentResponseToDtoWhenCarrierTypeNull() {

        DeletedSoldShipmentDetail deleteSoldShipmentDetail = new DeletedSoldShipmentDetail("54321356", null);
        DeletedSoldShipmentResponse response = mapper.toDto(deleteSoldShipmentDetail);
        assertThat(response, notNullValue());
        assertThat(response.getDeletedSoldShipment(), notNullValue());
        assertThat(response.getDeletedSoldShipment()
                .getCarrierType(), nullValue());
        assertThat(response.getDeletedSoldShipment()
                .getTrackingNumber(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toWorkStationModel(RequestHeaderDetail,LocationContextDetail)}
     * to set the model object fields from dto.
     * 
     */
    @Test
    public void testToDeletSoldShipmentData() {

        DeleteSoldShipmentRequestData request = createMockDeleteSoldShipmentRequestData();
        LocationContextDetail details = new LocationContextDetail();
        DeleteSoldShipmentData response = mapper.toDeletSoldShipmentData(request, details);
        assertThat(response, notNullValue());
    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link RetailShipmentCreationProcessorMapper#toConfirmedShipmentResourceDto(confirmedShipmentData)}
     * to set the dto object fields from model.
     * 
     */
    @Test
    public void testConfirmedShipmentResourceToDto() {

        ConfirmedShipmentData confirmedShipmentData = createConfirmedShipmentData();
        ConfirmedShipmentResource confirmedShipmentResource =
                mapper.toConfirmedShipmentResourceDto(confirmedShipmentData);
        assertThat(confirmedShipmentResource, notNullValue());
        assertThat(confirmedShipmentResource.getMasterTrackingId()
                .getTrackingNumber(), notNullValue());
    }

    /**
     * Method to create mock delete sold shipment request data
     * 
     * @return
     */
    private DeleteSoldShipmentRequestData createMockDeleteSoldShipmentRequestData() {

        DeleteSoldShipmentRequestData deleteSoldShipmentRequestData = new DeleteSoldShipmentRequestData();
        deleteSoldShipmentRequestData.setHeaderDetails(mockRequestHeaderDetail());
        deleteSoldShipmentRequestData.setLocationDetails(new LocationInfo("DFWD", mockAddress()));
        deleteSoldShipmentRequestData.setTrackingId("794984201540");
        deleteSoldShipmentRequestData.setRetailTransactionId("794984201540");
        deleteSoldShipmentRequestData.setCustomerName(new PersonName("Mr.", "Customer"));

        return deleteSoldShipmentRequestData;
    }

    /**
     * Method to build the mock instance of {@link RequestHeaderDetail} object .
     * 
     * @return
     */
    private RequestHeaderDetail mockRequestHeaderDetail() {

        return new RequestHeaderDetail("548796554", "8745896532", ZonedDateTime.now()
                .toString(), mockWorkstationDetails());
    }

    /**
     * Method to build the mock instance of {@link WorkstationDetails} object.
     * 
     * @return
     */
    private WorkstationDetails mockWorkstationDetails() {

        return new WorkstationDetails("BTC-50", "ROSA-01", "123456", AppName.FASTLANE, "SSFE");
    }

    /**
     * Prepare mock address object.
     * 
     * @return
     */
    public Address mockAddress() {

        return new Address(new ArrayList<String>(), "Dallas", "TX", "75024", "US", AddressClassification.HOME);

    }
}
