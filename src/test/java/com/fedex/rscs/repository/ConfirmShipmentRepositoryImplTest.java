package com.fedex.rscs.repository;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;

import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.ServiceProcessorTerminalException;
import com.fedex.rscs.model.AddressDetail;
import com.fedex.rscs.model.CarrierDetail;
import com.fedex.rscs.model.ContactDetail;
import com.fedex.rscs.model.InterlineShipmentDetail;
import com.fedex.rscs.model.PackageLineItemsDetail;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.ShipmentContactDetail;
import com.fedex.rscs.model.ShipmentDetail;
import com.fedex.rscs.model.ShipmentPackageAdditionalService;
import com.fedex.rscs.model.ShipmentPackageDetail;
import com.fedex.rscs.model.SoldShipmentDetail;
import com.fedex.rscs.model.SoldShipmentSummary;
import com.fedex.rscs.model.cshp.CreditCardData;
import com.fedex.rscs.model.cshp.Price;
import com.fedex.rscs.model.cshp.TrackingSequenceDetail;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test cases required to unit test ConfirmShipmentRepositoryImpl class
 * 
 * @author shakti.saurabh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ConfirmShipmentRepositoryImplTest {

    @InjectMocks
    private ConfirmShipmentRepositoryImpl confirmShipmentRepositoryImpl;

    @Mock
    private NamedParameterJdbcTemplate namedJdbcTemplate;

    @Mock
    private PlatformTransactionManager txManager;

    private static final String TIME_STAMP = "2020-07-30T11:55:28.687+05:30";

    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#save(ShipmentDetail)} then it's executed successfully
     */
    @Test
    public void testSaveSuccess() {

        ShipmentDetail shipmentDetail = createShipmentDetail();

        when(namedJdbcTemplate.update(any(String.class), any(MapSqlParameterSource.class))).thenReturn(1);

        boolean result = confirmShipmentRepositoryImpl.save(shipmentDetail);

        verify(namedJdbcTemplate, times(3)).update(any(String.class), any(MapSqlParameterSource.class));
        verify(namedJdbcTemplate, times(2)).batchUpdate(any(String.class), (MapSqlParameterSource[]) any()); 
        assertTrue(result);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#save(ShipmentDetail)} then it's executed successfully with
     * some reversed flags values.
     */
    @Test
    public void testSaveSuccessWithReversedFlagVaues() {

        ShipmentDetail shipmentDetail = createShipmentDetail();
        shipmentDetail.setAfterPickupFlag(true);
        shipmentDetail.setStandardPackageFlag(true);
        shipmentDetail.getShipmentContactDetail()
                .setResidence(true);
        shipmentDetail.getShipmentContactDetail()
                .setSenderCustomer(true);
        shipmentDetail.getShipmentPackageDetails()
                .get(0)
                .setMasterFlag(true);
        shipmentDetail.getShipmentPackageDetails()
                .get(0)
                .setManualWeightFlag(true);

        when(namedJdbcTemplate.update(any(String.class), any(MapSqlParameterSource.class))).thenReturn(1);

        boolean result = confirmShipmentRepositoryImpl.save(shipmentDetail);

        verify(namedJdbcTemplate, times(3)).update(any(String.class), any(MapSqlParameterSource.class));
        verify(namedJdbcTemplate, times(2)).batchUpdate(any(String.class), (MapSqlParameterSource[]) any());   
        assertTrue(result);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#save(ShipmentDetail)} then it's not executed successfully
     */
    @Test(expected = ServiceProcessorTerminalException.class)
    public void testSaveFailure() {

        ShipmentDetail shipmentDetail = createShipmentDetail();

        doThrow(new ServiceProcessorTerminalException(ServiceErrorCode.DB_INSERTION_FAILURE))
                .when(namedJdbcTemplate)
                .update(any(String.class), any(MapSqlParameterSource.class));
        confirmShipmentRepositoryImpl.save(shipmentDetail);
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#save(ShipmentDetail)} then it's executed successfully
     */
    @Test
    public void testSaveSuccessWhenNullInterlineDetails() {

        ShipmentDetail shipmentDetail = createShipmentDetail();
        shipmentDetail.setInterlineShipmentDetail(null);

        when(namedJdbcTemplate.update(any(String.class), any(MapSqlParameterSource.class))).thenReturn(1);

        boolean result = confirmShipmentRepositoryImpl.save(shipmentDetail);

        verify(namedJdbcTemplate, times(2)).update(any(String.class), any(MapSqlParameterSource.class));
        verify(namedJdbcTemplate, times(2)).batchUpdate(any(String.class), (MapSqlParameterSource[]) any());
        assertTrue(result);
    }

    /**
     * Method to create object of ShipmentDetail
     * 
     * @return
     */
    private ShipmentDetail createShipmentDetail() {

        ShipmentDetail shipmentDetail = new ShipmentDetail();
        shipmentDetail.setAcceptTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentDetail.setAfterPickupFlag(false);
        shipmentDetail.setApiClientId("1234");
        shipmentDetail.setApiClientTypeName("apiClientType");
        shipmentDetail.setBillToAccountType("bill");
        shipmentDetail.setCityCenterAccountNumber("1234556");
        shipmentDetail.setChargeAmount(1.23);
        shipmentDetail.setCurrency("USD");
        shipmentDetail.setCurrentShipmentStatTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentDetail.setCurrentShipmentStatus("status");
        shipmentDetail.setCustomerAccountNumber("1234556");
        shipmentDetail.setDelCommitDate("2020-07-30T11:55:28");
        shipmentDetail.setDeviceId("SSFE");
        shipmentDetail.setDevicePlatformType("devicePlatformType");
        shipmentDetail.setDimensionUom("IN");
        shipmentDetail.setDiscountAmount(1.23);
        shipmentDetail.setInitialAcceptanceTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentDetail.setPaymentType("ACCOUNT");
        shipmentDetail.setTaxAmount(1.23);
        shipmentDetail.setSoftwareId("SSFE");
        shipmentDetail.setWeightUom("LB");
        shipmentDetail.setShipmentTrackingNumber("12345");
        shipmentDetail.setMeterNumber("033600003");
        shipmentDetail.setOpCoCode("FXO");
        shipmentDetail.setOpCoLocationId("DREK");
        shipmentDetail.setShipmentContactDetail(createShipmentContactDetail());
        shipmentDetail.setShipmentPackageDetails(createShipmentPackageDetails());
        shipmentDetail.setInterlineShipmentDetail(createInterlineShipmentDetail());
        
        return shipmentDetail;
    }

    private List<ShipmentPackageDetail> createShipmentPackageDetails() {

        List<ShipmentPackageDetail> shipmentPackageDetailsList = new ArrayList<>();
        ShipmentPackageDetail shipmentPackageDetail = new ShipmentPackageDetail();
        shipmentPackageDetail.setAcceptTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentPackageDetail.setAmount(1.2);
        shipmentPackageDetail.setTrackingNumber("123456");
        shipmentPackageDetail.setShipmentPackageAdditionalServices(createShipmentPackageAdditionalServices());
        shipmentPackageDetailsList.add(shipmentPackageDetail);
        return shipmentPackageDetailsList;
    }

    private List<ShipmentPackageAdditionalService> createShipmentPackageAdditionalServices() {

        List<ShipmentPackageAdditionalService> shipmentPackageAdditionalServiceList = new ArrayList<>();
        ShipmentPackageAdditionalService shipmentPackageAdditionalService = new ShipmentPackageAdditionalService();
        shipmentPackageAdditionalService.setAcceptTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentPackageAdditionalServiceList.add(shipmentPackageAdditionalService);
        return shipmentPackageAdditionalServiceList;
    }

    /**
     * Method to create object of ShipmentContactDetail
     * 
     * @return
     */
    private ShipmentContactDetail createShipmentContactDetail() {

        ShipmentContactDetail shipmentContactDetail = new ShipmentContactDetail();
        shipmentContactDetail.setAcceptTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentContactDetail.setCustomerName("customer");
        shipmentContactDetail.setSenderName("sender");
        shipmentContactDetail.setRecipientName("recipient");

        return shipmentContactDetail;
    }
    
    /**
     * Method to create object of InterlineShipmentDetail
     * 
     * @return
     */
    private InterlineShipmentDetail createInterlineShipmentDetail() {

        InterlineShipmentDetail interlineShipmentDetail = new InterlineShipmentDetail();
        interlineShipmentDetail.setAcceptTmStmp("2020-07-30T11:55:28.687+05:30");
        interlineShipmentDetail.setShipmentTrackingNumber("794794045678");
        interlineShipmentDetail.setInterlineId("100038");
        interlineShipmentDetail.setEmployeeId("3932956");
        interlineShipmentDetail.setShipmentWeight("9");

        return interlineShipmentDetail;
    }

    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#delete(String)} then it's executed successfully
     */
    @Test
    public void testDelete() {

        boolean reply = confirmShipmentRepositoryImpl.delete("123456789");
        assertThat(reply, equalTo(false));
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#getShipmentDetails(shipmentTrackingNumber,paymentRefId)}
     * then it's executed successfully
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testGetShipmentDetails() {

        when(namedJdbcTemplate.queryForObject(any(String.class), any(MapSqlParameterSource.class),
                any(RowMapper.class))).thenReturn(new ShipmentDetail(), new ArrayList<ShipmentPackageDetail>());

        ShipmentDetail result = confirmShipmentRepositoryImpl.getShipmentDetails("123", "1234");
        verify(namedJdbcTemplate, times(1)).queryForObject(any(String.class), any(MapSqlParameterSource.class),
                any(RowMapper.class));
        assertThat(result, notNullValue());

    }

    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#getShipmentDetails(shipmentTrackingNumber,paymentRefId)}
     * then it's return null and no other query executed.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testGetShipmentDetailsWhenShipmentNotFound() {

        when(namedJdbcTemplate.queryForObject(any(String.class), any(MapSqlParameterSource.class),
                any(RowMapper.class))).thenReturn(null);

        ShipmentDetail result = confirmShipmentRepositoryImpl.getShipmentDetails("123", "1234");
        verify(namedJdbcTemplate, times(1)).queryForObject(any(String.class), any(MapSqlParameterSource.class),
                any(RowMapper.class));
        assertThat(result, nullValue());
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#getSoldShipmentByBarCode(barcode,locationId)} then it's
     * executed successfully.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testGetSoldShipmentByBarCode() {

        when(namedJdbcTemplate.query(any(String.class), any(MapSqlParameterSource.class), any(RowMapper.class)))
                .thenReturn(createSoldShipmentResourceDetails("barcode", "123456789", 1),
                        createPackageItems("barcode", "123456789", 1));
        List<SoldShipmentDetail> result = confirmShipmentRepositoryImpl.getSoldShipmentByBarCode("123456789", "DFWD");
        assertThat(result, notNullValue());
        assertThat(result, hasSize(1));
        assertThat(result.get(0)
                .getPackageLineItems(), hasSize(1));
    }

    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#getSoldShipmentByBarCode(barcode,locationId)} then it's
     * executed successfully with empty package line tems.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testGetSoldShipmentByBarCodeEmptyPackage() {

        when(namedJdbcTemplate.query(any(String.class), any(MapSqlParameterSource.class), any(RowMapper.class)))
                .thenReturn(createSoldShipmentResourceDetails("barcode", "123456789", 1), null);
        List<SoldShipmentDetail> result = confirmShipmentRepositoryImpl.getSoldShipmentByBarCode("123456789", "DFWD");
        assertThat(result, notNullValue());
        assertThat(result, hasSize(1));
        assertThat(result.get(0)
                .getPackageLineItems(), nullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#getSoldShipmentByBarCode(barcode,locationId)} then it's
     * returns null.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testGetSoldShipmentByBarCodeNull() {

        when(namedJdbcTemplate.query(any(String.class), any(MapSqlParameterSource.class), any(RowMapper.class)))
                .thenReturn(null);
        List<SoldShipmentDetail> result = confirmShipmentRepositoryImpl.getSoldShipmentByBarCode("123456789", "DFWD");
        assertThat(result, nullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#getSoldShipmentByGuid(guid,locationId)} then it's
     * executed successfully.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testGetSoldShipmentByGuid() {

        when(namedJdbcTemplate.query(any(String.class), any(MapSqlParameterSource.class), any(RowMapper.class)))
                .thenReturn(createSoldShipmentResourceDetails("guid", "123456789", 1),
                        createPackageItems("guid", "123456789", 1));
        List<SoldShipmentDetail> result = confirmShipmentRepositoryImpl.getSoldShipmentByGuid("123456789", "DFWD");
        assertThat(result, notNullValue());
        assertThat(result, hasSize(1));
        assertThat(result.get(0)
                .getPackageLineItems(), hasSize(1));
    }

    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#getSoldShipmentByGuid(guid,locationId)} then it's
     * executed successfully with empty package line tems.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testGetSoldShipmentByGuidEmptyPackage() {

        when(namedJdbcTemplate.query(any(String.class), any(MapSqlParameterSource.class), any(RowMapper.class)))
                .thenReturn(createSoldShipmentResourceDetails("guid", "123456789", 1), null);
        List<SoldShipmentDetail> result = confirmShipmentRepositoryImpl.getSoldShipmentByGuid("123456789", "DFWD");
        assertThat(result, notNullValue());
        assertThat(result, hasSize(1));
        assertThat(result.get(0)
                .getPackageLineItems(), nullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#getSoldShipmentByGuid(guid,locationId)} then it's
     * returns null.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testGetSoldShipmentByGuidNull() {

        when(namedJdbcTemplate.query(any(String.class), any(MapSqlParameterSource.class), any(RowMapper.class)))
                .thenReturn(null);
        List<SoldShipmentDetail> result = confirmShipmentRepositoryImpl.getSoldShipmentByGuid("123456789", "DFWD");
        assertThat(result, nullValue());
    }

    /**
<<<<<<< HEAD
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#getSoldShipmentByTrackingNumber(trackingNumber,locationId)} then it's
     * executed successfully.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testGetSoldShipmentByTrackingNumberd() {

        when(namedJdbcTemplate.query(any(String.class), any(MapSqlParameterSource.class), any(RowMapper.class)))
                .thenReturn(createSoldShipmentResourceDetails("track", "123456789", 1),
                        createPackageItems("track", "123456789", 1));
        List<SoldShipmentDetail> result = confirmShipmentRepositoryImpl.getSoldShipmentByTrackingNumber("123456789", "DFWD");
        assertThat(result, notNullValue());
        assertThat(result, hasSize(1));
        assertThat(result.get(0)
                .getPackageLineItems(), hasSize(1));
    }

    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#getSoldShipmentByTrackingNumber(trackingNumber,locationId)} then it's
     * executed successfully with empty package line tems.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testGetSoldShipmentByTrackingNumberEmptyPackage() {

        when(namedJdbcTemplate.query(any(String.class), any(MapSqlParameterSource.class), any(RowMapper.class)))
                .thenReturn(createSoldShipmentResourceDetails("track", "123456789", 1), null);
        List<SoldShipmentDetail> result = confirmShipmentRepositoryImpl.getSoldShipmentByTrackingNumber("123456789", "DFWD");
        assertThat(result, notNullValue());
        assertThat(result, hasSize(1));
        assertThat(result.get(0)
                .getPackageLineItems(), nullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#getSoldShipmentByTrackingNumber(trackingNumber,locationId)} then it's
     * returns null.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testGetSoldShipmentByTrackingNumberNull() {

        when(namedJdbcTemplate.query(any(String.class), any(MapSqlParameterSource.class), any(RowMapper.class)))
                .thenReturn(null);
        List<SoldShipmentDetail> result = confirmShipmentRepositoryImpl.getSoldShipmentByTrackingNumber("123456789", "DFWD");
        assertThat(result, nullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#getShipmentsByLocation(locationId,date)} then it's
     * executed successfully.
     */

    @SuppressWarnings("unchecked")
    @Test
    public void testGetSoldShipmentSummaryByLocationDetail() {

        when(namedJdbcTemplate.query(any(String.class), any(MapSqlParameterSource.class), any(ResultSetExtractor.class)))
                .thenReturn(createSoldShipmentResourceDetails(1), createPackageItems(1));
        List<SoldShipmentSummary> result =
                confirmShipmentRepositoryImpl.getShipmentsByLocation("DFWD", "2020-10-18T10:59:10.273Z");
        assertThat(result, notNullValue());
        assertThat(result, hasSize(1));
        assertThat(result.get(0)
                .getPackageLineItems(), hasSize(1));

    }

    /**
     * Test case to verify that when request is made to
     * {@link ConfirmShipmentRepositoryImpl#getShipmentsByLocation(locationId,date)} then it's
     * returns null.
     */

    @SuppressWarnings("unchecked")
    @Test
    public void testGetSoldShipmentSummaryNullByLocationDetail() {

        when(namedJdbcTemplate.query(any(String.class), any(MapSqlParameterSource.class), any(ResultSetExtractor.class)))
                .thenReturn(null);
        List<SoldShipmentSummary> result =
                confirmShipmentRepositoryImpl.getShipmentsByLocation("DFWD", "2020-10-18T10:59:10.273Z");
        assertThat(result, nullValue());
    }

    /**
     * Create PackageLineItemsDetail's List
     * 
     * @return
     */
    private Object createPackageItems(
            int size) {

        List<PackageLineItemsDetail> packageLineItems = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            PackageLineItemsDetail packageLineItemsDetail = new PackageLineItemsDetail();
            packageLineItemsDetail.setMaster(i % 2 == 0);
            TrackingSequenceDetail trackingSequenceDetail = new TrackingSequenceDetail();
            trackingSequenceDetail.setTrackingNumber("12345678912" + i);
            trackingSequenceDetail.setBarcode("12345678912" + i);
            trackingSequenceDetail.setSequence(i);
            packageLineItemsDetail.setTrackingId(trackingSequenceDetail);
            packageLineItems.add(packageLineItemsDetail);
        }
        return packageLineItems;
    }

    /**
     * Create SoldShipmentSummary's List
     * 
     * @return
     */
    private List<SoldShipmentSummary> createSoldShipmentResourceDetails(
            int size) {

        List<SoldShipmentSummary> shipmentSummary = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            SoldShipmentSummary shipmentsummary = new SoldShipmentSummary();
            shipmentsummary.setGuid("12345678912" + i);
            List<PackageLineItemsDetail> packageLineItems = new ArrayList<>();
            PackageLineItemsDetail packageLineItemsDetail = new PackageLineItemsDetail();
            packageLineItemsDetail.setMaster(i % 2 == 0);
            TrackingSequenceDetail trackingSequenceDetail = new TrackingSequenceDetail();
            trackingSequenceDetail.setTrackingNumber("12345678912" + i);
            trackingSequenceDetail.setBarcode("12345678912" + i);
            trackingSequenceDetail.setSequence(i);
            packageLineItemsDetail.setTrackingId(trackingSequenceDetail);
            packageLineItems.add(packageLineItemsDetail);
            shipmentsummary.setPackageLineItems(packageLineItems);
            shipmentsummary.setMasterTrackingId(trackingSequenceDetail);
            shipmentsummary.setRetailTransactionId("12345678" + i);
            shipmentsummary.setServiceType("FIRST_OVERNIGHT");
            shipmentsummary.setStatus("CREATED");
            CarrierDetail carrierDetail = new CarrierDetail();
            if (i % 2 == 0) {
                carrierDetail.setCarrierCode("FDXE");
                carrierDetail.setOpco("EXPRESS");
            } else {
                carrierDetail.setCarrierCode("FDXG");
                carrierDetail.setOpco("GROUND");
            }

            shipmentsummary.setCarrierDetails(carrierDetail);
            shipmentSummary.add(shipmentsummary);
        }
        return shipmentSummary;
    }
    
    /**
     * Create PackageLineItemsDetail's List
     * 
     * @return
     */
    private Object createPackageItems(
            String type,
            String id,
            int size) {

        List<PackageLineItemsDetail> packageLineItems = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            PackageLineItemsDetail packageLineItemsDetail = new PackageLineItemsDetail();
            packageLineItemsDetail.setMaster(i % 2 == 0);
            TrackingSequenceDetail trackingSequenceDetail = new TrackingSequenceDetail();
            trackingSequenceDetail.setTrackingNumber(type.equalsIgnoreCase("track") ? id : ("12345678912" + i));
            trackingSequenceDetail.setBarcode(type.equalsIgnoreCase("barcode") ? id : ("12345678912" + i));
            trackingSequenceDetail.setSequence(i);
            packageLineItemsDetail.setTrackingId(trackingSequenceDetail);
            packageLineItems.add(packageLineItemsDetail);
        }
        return packageLineItems;
    }

    /**
     * Create SoldShipmentDetail's List
     * 
     * @return
     */
    private List<SoldShipmentDetail> createSoldShipmentResourceDetails(
            String type,
            String id,
            int size) {

        List<SoldShipmentDetail> shipmentDetails = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            SoldShipmentDetail shipmentDetail = new SoldShipmentDetail();
            shipmentDetail.setCustomer(createPartyDetail());
            shipmentDetail.setGuid(type.equalsIgnoreCase("guid") ? id : ("12345678912" + i));
            shipmentDetail.setPackageCount(i + "");
            List<PackageLineItemsDetail> packageLineItems = new ArrayList<>();
            PackageLineItemsDetail packageLineItemsDetail = new PackageLineItemsDetail();
            packageLineItemsDetail.setMaster(i % 2 == 0);
            TrackingSequenceDetail trackingSequenceDetail = new TrackingSequenceDetail();
            trackingSequenceDetail.setTrackingNumber(type.equalsIgnoreCase("track") ? id : ("12345678912" + i));
            trackingSequenceDetail.setBarcode(type.equalsIgnoreCase("barcode") ? id : ("12345678912" + i));
            trackingSequenceDetail.setSequence(i);
            packageLineItemsDetail.setTrackingId(trackingSequenceDetail);
            packageLineItems.add(packageLineItemsDetail);
            shipmentDetail.setPackageLineItems(packageLineItems);
            shipmentDetail.setMasterTrackingId(trackingSequenceDetail);
            shipmentDetail.setPackagingType("FEDEX_PAK");
            shipmentDetail.setPaymentType("ACCOUNT");
            shipmentDetail.setRecipient(createPartyDetail());
            shipmentDetail.setRetailTransactionId("12345678" + i);
            shipmentDetail.setServiceType("FIRST_OVERNIGHT");
            shipmentDetail.setShipper(createPartyDetail());
            shipmentDetail.setStatus("CREATED");
            shipmentDetail.setTotalAmount(new Price("USD", 100d));
            shipmentDetail.setTransactionDateTime(TIME_STAMP);
            CarrierDetail carrierDetail = new CarrierDetail();
            if (i % 2 == 0) {
                carrierDetail.setCarrierCode("FDXE");
                carrierDetail.setOpco("EXPRESS");
            } else {
                carrierDetail.setCarrierCode("FDXG");
                carrierDetail.setOpco("GROUND");
            }

            shipmentDetail.setCarrierDetails(carrierDetail);
            shipmentDetails.add(shipmentDetail);
        }
        return shipmentDetails;
    }

    /**
     * Create PartyDetail
     * 
     * @return
     */
    private PartyDetail createPartyDetail() {

        List<String> streetLines = new ArrayList<>();
        streetLines.add("4071 North Trail");

        AddressDetail addressDetail = new AddressDetail();
        addressDetail.setPostalCode("75023");
        addressDetail.setCountryCode("US");
        addressDetail.setStreetLines(streetLines);
        return new PartyDetail(addressDetail, createContactDetail(), "038000004",
                new CreditCardData("4005554444444460", "VISA", "12/2020"), "FEDEX_EXPRESS");
    }

    /**
     * Create ContactDetail
     * 
     * @return
     */
    private ContactDetail createContactDetail() {

        ContactDetail contactDetail = new ContactDetail();
        contactDetail.setContactId("test123");
        contactDetail.setFullName("Shakti Saurabh");
        contactDetail.setPhoneNumber("3900090");

        return contactDetail;
    }
    
    /**
     * Test case to validate the scenario
     * {@link ConfirmShipmentRepositoryImpl#updateStatus(updateShipmentStatusData))} shipment
     * status updated successfully
     * 
     * @throws SQLException
     * 
     */
    @Test
    public void testupdateShipmentStatusSuccess() {

        UpdateOpenShipmentStatusData updateShipmentStatusData = createShipmentStatusData();

        when(namedJdbcTemplate.update(any(String.class), any(MapSqlParameterSource.class))).thenReturn(1);
        boolean rows = confirmShipmentRepositoryImpl.updateStatus(updateShipmentStatusData);

        verify(namedJdbcTemplate, times(1)).update(any(String.class), any(MapSqlParameterSource.class));
        assertThat(rows, equalTo(true));
    }
    
    /**
     * Test case to validate the scenario
     * {@link ConfirmShipmentRepositoryImpl#updateGroundRateNotification(String,String, boolean))}
     * shipment status updated successfully
     * 
     * @throws SQLException
     * 
     */
    @Test
    public void testUpdateGroundRateNotificationSuccess() {

        String trackingId = "794989633673";
        String shipTimeStamp = "2021-04-08T19:03:07.812Z";
        when(namedJdbcTemplate.update(any(String.class), any(MapSqlParameterSource.class))).thenReturn(1);
        confirmShipmentRepositoryImpl.updateGroundRateNotification(trackingId, shipTimeStamp);

        verify(namedJdbcTemplate, times(1)).update(any(String.class), any(MapSqlParameterSource.class));
    }

    /**
     * Test case to validate the scenario
     * {@link ConfirmShipmentRepositoryImpl#updateStatus(updateShipmentStatusData))} shipment
     * status doesn't updated.
     * 
     * @throws SQLException
     * 
     */
    @Test
    public void testupdateShipmentStatusFailure() {

        UpdateOpenShipmentStatusData updateShipmentStatusData = createShipmentStatusData();

        when(namedJdbcTemplate.update(any(String.class), any(MapSqlParameterSource.class))).thenReturn(0);
        boolean rows = confirmShipmentRepositoryImpl.updateStatus(updateShipmentStatusData);

        verify(namedJdbcTemplate, times(1)).update(any(String.class), any(MapSqlParameterSource.class));
        assertThat(rows, equalTo(false));
    }

    /**
     * Method to create UpdateShipmentStatusData object.
     * 
     * @return UpdateShipmentStatusData
     */
    private UpdateOpenShipmentStatusData createShipmentStatusData() {

        UpdateOpenShipmentStatusData updateShipmentStatusData = new UpdateOpenShipmentStatusData();
        updateShipmentStatusData.setLocationId("DFWD");
        updateShipmentStatusData.setOpenShipmentStatusCode("confirmed");
        updateShipmentStatusData.setReferenceId("1234");
        updateShipmentStatusData.setTeamMemberId("999990000");
        updateShipmentStatusData.setTrackingId("3456");
        return updateShipmentStatusData;
    }

}
