package com.fedex.rscs.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.fedex.rscs.model.SoldShipmentSummary;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test cases required to unit test SoldShipmentSummaryRowMapper class
 * 
 * @author 3798910
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SoldShipmentSummaryRowMapperTest {

    private SoldShipmentSummaryRowMapper soldShipmentSummaryRowMapper;

    @Before
    public void setup() {

        this.soldShipmentSummaryRowMapper = new SoldShipmentSummaryRowMapper();
    }

    /**
     * Test case to verify that when request is made to
     * {@link SoldShipmentSummaryRowMapper#extractData(ResultSet)} then it's executed successfully
     * for only one shipment.
     */
    @Test
    public void testExtractData() {

        List<SoldShipmentSummary> soldShipmentDetails = null;
        try {
            soldShipmentDetails =
                    (List<SoldShipmentSummary>) soldShipmentSummaryRowMapper.extractData(getMockResultSet());
            assertThat(soldShipmentDetails, notNullValue());
            assertThat(soldShipmentDetails.get(0)
                    .getGuid(), equalTo("SDFWD-DFWD-FUSE-2007b37a-436d-4cbc-beb7-5baf6892827f"));
            assertThat(soldShipmentDetails.get(0)
                    .getPackageLineItems(), hasSize(1));

        } catch (Exception e) {
            fail();
        }

    }

    /**
     * Test case to verify that when request is made to
     * {@link SoldShipmentSummaryRowMapper#extractData(ResultSet)} then it's executed successfully
     * for only two shipment.
     */
    @Test
    public void testExtractDataForElse() {

        List<SoldShipmentSummary> soldShipmentDetails = null;
        try {
            soldShipmentDetails =
                    (List<SoldShipmentSummary>) soldShipmentSummaryRowMapper.extractData(getMockResultSetForElse());
            assertThat(soldShipmentDetails, notNullValue());
            assertThat(soldShipmentDetails.get(0)
                    .getPackageLineItems()
                    .get(0)
                    .getTrackingId()
                    .getBarcode(), equalTo("123456789"));
            assertThat(soldShipmentDetails.get(0)
                    .getPackageLineItems(), hasSize(2));

        } catch (Exception e) {
            fail();
        }

    }
    
    /**
     * Test case to verify that when request is made to
     * {@link SoldShipmentSummaryRowMapper#extractData(ResultSet)} then it's executed successfully
     * for only two shipment and master package flag set to "N".
     */
    @Test
    public void testExtractDataForElseWithNoPkgFlag() {

        List<SoldShipmentSummary> soldShipmentDetails = null;
        try {
            ResultSet rs = getMockResultSetForElse();
            when(rs.getString("MPS_MSTR_PKG_FLG")).thenReturn("N");
            soldShipmentDetails =
                    (List<SoldShipmentSummary>) soldShipmentSummaryRowMapper.extractData(rs);
            assertThat(soldShipmentDetails, notNullValue());
            assertThat(soldShipmentDetails.get(0)
                    .getPackageLineItems()
                    .get(0)
                    .getTrackingId()
                    .getBarcode(), equalTo("123456789"));
            assertThat(soldShipmentDetails.get(0)
                    .getPackageLineItems(), hasSize(2));

        } catch (Exception e) {
            fail();
        }

    }
    
    /**
     * Test case to verify that when request is made to
     * {@link SoldShipmentSummaryRowMapper#extractData(ResultSet)} then it's executed successfully
     * for only one ground shipment and master package flag set to "N".
     */
    @Test
    public void testExtractDataForGround() {

        List<SoldShipmentSummary> soldShipmentDetails = null;
        try {
            ResultSet rs = getMockResultSet();
            when(rs.getString("SHPNG_SVC_OPCO_NM")).thenReturn("FEDEX_GROUND");
            when(rs.getString("MPS_MSTR_PKG_FLG")).thenReturn("N");
            soldShipmentDetails =
                    (List<SoldShipmentSummary>) soldShipmentSummaryRowMapper.extractData(rs);
            assertThat(soldShipmentDetails, notNullValue());
            assertThat(soldShipmentDetails.get(0)
                    .getGuid(), equalTo("SDFWD-DFWD-FUSE-2007b37a-436d-4cbc-beb7-5baf6892827f"));
            assertThat(soldShipmentDetails.get(0)
                    .getPackageLineItems(), hasSize(1));

        } catch (Exception e) {
            fail();
        }

    }

    /**
     * Test case to verify that when request is made to
     * {@link SoldShipmentSummaryRowMapper#extractData(ResultSet)} then it's executed successfully
     * for only one shipment with master package flag as null.
     */
    @Test
    public void testExtractDataWithNullPkgFlag() {

        List<SoldShipmentSummary> soldShipmentDetails = null;
        try {
            ResultSet rs = getMockResultSet();
            when(rs.getString("MPS_MSTR_PKG_FLG")).thenReturn(null);
            soldShipmentDetails =
                    (List<SoldShipmentSummary>) soldShipmentSummaryRowMapper.extractData(rs);
            assertThat(soldShipmentDetails, notNullValue());
            assertThat(soldShipmentDetails.get(0)
                    .getGuid(), equalTo("SDFWD-DFWD-FUSE-2007b37a-436d-4cbc-beb7-5baf6892827f"));
            assertThat(soldShipmentDetails.get(0)
                    .getPackageLineItems(), hasSize(1));

        } catch (Exception e) {
            fail();
        }

    }

    /**
     * Test case to verify that when request is made to
     * {@link SoldShipmentSummaryRowMapper#extractData(ResultSet)} then it's executed successfully
     * for only two shipment with master package flag as null.
     */
    @Test
    public void testExtractDataForElseWithNullPkgFlag() {

        List<SoldShipmentSummary> soldShipmentDetails = null;
        try {
            ResultSet rs = getMockResultSetForElse();
            when(rs.getString("MPS_MSTR_PKG_FLG")).thenReturn(null);
            soldShipmentDetails =
                    (List<SoldShipmentSummary>) soldShipmentSummaryRowMapper.extractData(rs);
            assertThat(soldShipmentDetails, notNullValue());
            assertThat(soldShipmentDetails.get(0)
                    .getPackageLineItems()
                    .get(0)
                    .getTrackingId()
                    .getBarcode(), equalTo("123456789"));
            assertThat(soldShipmentDetails.get(0)
                    .getPackageLineItems(), hasSize(2));

        } catch (Exception e) {
            fail();
        }

    }
    
    /**
     * Method to mock ResultSet for one shipment
     *
     * @return
     * @throws SQLException
     */
    private ResultSet getMockResultSet()
            throws SQLException {

        ResultSet rs = mock(ResultSet.class);
        when(rs.next()).thenReturn(true)
                .thenReturn(false);
        when(rs.getString("SHPMT_TRKNG_NBR")).thenReturn("123456789");
        when(rs.getString("SHPNG_SVC_DESC")).thenReturn("FedEx 2Day");
        when(rs.getString("CUR_SHPMT_STATUS_CD")).thenReturn("CREATED");
        when(rs.getString("TRAN_SHPMT_REF_ID")).thenReturn("SDFWD-DFWD-FUSE-2007b37a-436d-4cbc-beb7-5baf6892827f");
        when(rs.getString("POS_TRAN_REF_ID")).thenReturn("12345");
        when(rs.getString("MPS_MSTR_PKG_FLG")).thenReturn("Y");
        when(rs.getString("SHPNG_SVC_OPCO_NM")).thenReturn("FEDEX_EXPRESS");
        when(rs.getTimestamp("INITL_ACCPT_TMSTP")).thenReturn(new Timestamp(10000));
        return rs;
    }

    /**
     * Method to mock ResultSet for two shipment
     * 
     * @return
     * @throws SQLException
     */
    private ResultSet getMockResultSetForElse()
            throws SQLException {

        ResultSet rs = mock(ResultSet.class);
        when(rs.next()).thenReturn(true)
                .thenReturn(true)
                .thenReturn(false);
        when(rs.getString("SHPMT_TRKNG_NBR")).thenReturn("123456789");
        when(rs.getString("SHPNG_SVC_DESC")).thenReturn("FedEx 2Day");
        when(rs.getString("CUR_SHPMT_STATUS_CD")).thenReturn("CREATED");
        when(rs.getString("TRAN_SHPMT_REF_ID")).thenReturn("SDFWD-DFWD-FUSE-2007b37a-436d-4cbc-beb7-5baf6892827f");
        when(rs.getTimestamp("INITL_ACCPT_TMSTP")).thenReturn(new Timestamp(1000));
        when(rs.getString("POS_TRAN_REF_ID")).thenReturn("12345");
        when(rs.getString("MPS_MSTR_PKG_FLG")).thenReturn("Y");
        when(rs.getString("PKG_BARCD_CD")).thenReturn("123456789");
        return rs;
    }
}
