package com.fedex.rscs.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.fedex.rscs.dto.PaymentType;
import com.fedex.rscs.model.SoldShipmentDetail;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test cases required to unit test SoldShipmentRowMapper class
 * 
 * @author Shakti.Saurabh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SoldShipmentRowMapperTest {

    private SoldShipmentRowMapper soldShipmentRowMapper;

    @Before
    public void setup() {

        this.soldShipmentRowMapper = new SoldShipmentRowMapper("DFWD");
    }

    /**
     * Test case to verify that when request is made to
     * {@link SoldShipmentRowMapper#mapRows(ResultSet)} then it's executed successfully for carrier
     * code FDXE.
     */
    @Test
    public void testMapRowWithFDXE() {

        SoldShipmentDetail soldShipmentDetails = null;
        try {
            soldShipmentDetails = soldShipmentRowMapper.mapRow(getMockResultSet("FEDEX_EXPRESS", "N"), 1);
            assertThat(soldShipmentDetails, notNullValue());
            assertThat(soldShipmentDetails.getCarrierDetails(), notNullValue());
            assertThat(soldShipmentDetails.getCarrierDetails()
                    .getCarrierCode(), equalTo("FDXE"));
            assertThat(soldShipmentDetails.getShipper()
                    .getAddress()
                    .isResidential(), equalTo(false));
        } catch (Exception e) {
            fail();
        }

    }

    /**
     * Test case to verify that when request is made to
     * {@link SoldShipmentRowMapper#mapRows(ResultSet)} then it's executed successfully for carrier
     * code FDXG.
     */
    @Test
    public void testMapRowWithFDXG() {

        SoldShipmentDetail soldShipmentDetails = null;
        try {
            soldShipmentDetails = soldShipmentRowMapper.mapRow(getMockResultSet("FEDEX_GROUND", "Y"), 1);
            assertThat(soldShipmentDetails, notNullValue());
            assertThat(soldShipmentDetails.getCarrierDetails(), notNullValue());
            assertThat(soldShipmentDetails.getCarrierDetails()
                    .getCarrierCode(), equalTo("FDXG"));
            assertThat(soldShipmentDetails.getShipper()
                    .getAddress()
                    .isResidential(), equalTo(true));

        } catch (Exception e) {
            fail();
        }

    }

    /**
     * Test case to verify that when request is made to
     * {@link SoldShipmentRowMapper#mapRows(ResultSet)} then it's executed successfully for carrier
     * code Null.
     */
    @Test
    public void testMapRowWithNull() {

        SoldShipmentDetail soldShipmentDetails = null;
        try {
            soldShipmentDetails = soldShipmentRowMapper.mapRow(getMockResultSet(null, null), 1);
            assertThat(soldShipmentDetails, notNullValue());
            assertThat(soldShipmentDetails.getCarrierDetails(), notNullValue());
            assertThat(soldShipmentDetails.getCarrierDetails()
                    .getCarrierCode(), nullValue());
            assertThat(soldShipmentDetails.getShipper()
                    .getAddress()
                    .isResidential(), equalTo(false));

        } catch (Exception e) {
            fail();
        }

    }

    /**
     * Test case to verify that when request is made to
     * {@link SoldShipmentRowMapper#mapRows(ResultSet)} then it's executed successfully for payment
     * type is CREDITCARD.
     */
    @Test
    public void testMapRowWithPaymntTypCrdCard() {

        SoldShipmentDetail soldShipmentDetails = null;
        try {
            soldShipmentDetails = soldShipmentRowMapper.mapRow(getMockResultSetPaymentType("CREDITCARD"), 1);
            assertThat(soldShipmentDetails, notNullValue());
            assertThat(soldShipmentDetails.getPaymentType(), equalTo(PaymentType.CREDIT_CARD.toString()));

        } catch (Exception e) {
            fail();
        }
    }

    /**
     * Test case to verify that when request is made to
     * {@link SoldShipmentRowMapper#mapRows(ResultSet)} then it's executed successfully for payment
     * type is ACCOUNT.
     */
    @Test
    public void testMapRowWithPaymntTypAccount() {

        SoldShipmentDetail soldShipmentDetails = null;
        try {
            soldShipmentDetails = soldShipmentRowMapper.mapRow(getMockResultSetPaymentType("ACCOUNT"), 1);
            assertThat(soldShipmentDetails, notNullValue());
            assertThat(soldShipmentDetails.getPaymentType(), equalTo(PaymentType.ACCOUNT.toString()));

        } catch (Exception e) {
            fail();
        }
    }

    /**
     * Method to mock ResultSet for Payment Type
     *
     * @return
     * @throws SQLException
     */
    private ResultSet getMockResultSetPaymentType(
            String paymentType)
            throws SQLException {

        ResultSet rs = mock(ResultSet.class);
        when(rs.getString("PAYMENT_TYPE_CD")).thenReturn(paymentType);
        when(rs.getTimestamp("INITL_ACCPT_TMSTP")).thenReturn(new Timestamp(10000));
        return rs;
    }

    /**
     * Method to mock ResultSet for FDXE or FXE
     *
     * @return
     * @throws SQLException
     */
    private ResultSet getMockResultSet(
            String carrierCode,
            String residence)
            throws SQLException {

        ResultSet rs = mock(ResultSet.class);
        when(rs.getString("SHPNG_SVC_OPCO_NM")).thenReturn(carrierCode);
        when(rs.getString("SNDR_SAME_AS_CUST_FLG")).thenReturn(residence);
        when(rs.getTimestamp("INITL_ACCPT_TMSTP")).thenReturn(new Timestamp(10000));
        return rs;
    }

}
