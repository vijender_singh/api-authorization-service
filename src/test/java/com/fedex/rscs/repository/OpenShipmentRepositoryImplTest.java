package com.fedex.rscs.repository;

import java.sql.SQLException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fedex.rscs.model.OpenShipment;
import com.fedex.rscs.model.OpenShipmentData;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * This covers all the test cases required to unit test the Open Shipment Repository operations.
 * 
 * @author Surbhi.Gupta
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class OpenShipmentRepositoryImplTest {

    
    private OpenShipmentRepositoryImpl repo;

    @Mock
    private NamedParameterJdbcTemplate namedJdbcTemplate;
    
    ObjectMapper objectMapper = null;

    @Before
    public void initialize() {

        objectMapper = new ObjectMapper();
        repo = new OpenShipmentRepositoryImpl(namedJdbcTemplate);
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * Test case to validate the scenario {@link OpenShipmentRepositoryImpl#save(OpenShipment))} open
     * shipment saved successfully
     * 
     */
    @Test
    public void testDeleteOpenShipment() {

        final OpenShipment mockOpenShipment = mock(OpenShipment.class);
        boolean actual = repo.delete(mockOpenShipment);

        assertThat(actual, is(false));
    }

    /**
     * Test case to validate the scenario
     * {@link OpenShipmentRepositoryImpl#retrieve(referenceId,locationId,trackingId))} open shipment
     * retrieved successfully
     * 
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testRetrieveOpenShipment() {

        when(namedJdbcTemplate.queryForObject(any(String.class), any(MapSqlParameterSource.class),
                any(RowMapper.class))).thenReturn(new OpenShipment());

        OpenShipment actual = repo.retrieve("794867538175", "LASA", "794115538175");

        verify(namedJdbcTemplate, times(1)).queryForObject(any(String.class), any(MapSqlParameterSource.class),
                any(RowMapper.class));
        assertThat(actual, notNullValue());
    }

    /**
     * Test case to validate the scenario {@link OpenShipmentRepositoryImpl#save(OpenShipment))} open
     * shipment saved successfully
     * 
     * @throws SQLException
     * 
     */
    @Test
    public void testSaveSuccess() {

        OpenShipment openShipment = createOpenShipment();

        when(namedJdbcTemplate.update(any(String.class), any(MapSqlParameterSource.class))).thenReturn(1);
        boolean rows = repo.save(openShipment);
        verify(namedJdbcTemplate, times(1)).update(any(String.class), any(MapSqlParameterSource.class));
        assertThat(rows, equalTo(true));

    }

    /**
     * Test case to validate the scenario {@link OpenShipmentRepositoryImpl#save(OpenShipment))} open
     * shipment doesn't saved.
     * 
     * @throws SQLException
     * 
     */
    @Test
    public void testSaveError() {

        OpenShipment openShipment = createOpenShipment();

        when(namedJdbcTemplate.update(any(String.class), any(MapSqlParameterSource.class))).thenReturn(0);
        boolean rows = repo.save(openShipment);
        verify(namedJdbcTemplate, times(1)).update(any(String.class), any(MapSqlParameterSource.class));
        assertThat(rows, equalTo(false));

    }

    /**
     * Test case to validate the scenario
     * {@link OpenShipmentRepositoryImpl#updateStatus(updateOpenShipmentStatusData))} open shipment
     * status updated successfully
     * 
     * @throws SQLException
     * 
     */
    @Test
    public void testupdateOpenShipmentStatusSuccess() {

        UpdateOpenShipmentStatusData updateOpenShipmentStatusData = createOpenShipmentStatusData();

        when(namedJdbcTemplate.update(any(String.class), any(MapSqlParameterSource.class))).thenReturn(1);
        boolean rows = repo.updateStatus(updateOpenShipmentStatusData);

        verify(namedJdbcTemplate, times(1)).update(any(String.class), any(MapSqlParameterSource.class));
        assertThat(rows, equalTo(true));
    }

    /**
     * Test case to validate the scenario
     * {@link OpenShipmentRepositoryImpl#updateStatus(updateOpenShipmentStatusData))} open shipment
     * status doesn't updated.
     * 
     * @throws SQLException
     * 
     */
    @Test
    public void testupdateOpenShipmentStatusFailure() {

        UpdateOpenShipmentStatusData updateOpenShipmentStatusData = createOpenShipmentStatusData();

        when(namedJdbcTemplate.update(any(String.class), any(MapSqlParameterSource.class))).thenReturn(0);
        boolean rows = repo.updateStatus(updateOpenShipmentStatusData);

        verify(namedJdbcTemplate, times(1)).update(any(String.class), any(MapSqlParameterSource.class));
        assertThat(rows, equalTo(false));
    }

    /**
     * Method to create UpdateOpenShipmentStatusData object.
     * 
     * @return UpdateOpenShipmentStatusData
     */
    private UpdateOpenShipmentStatusData createOpenShipmentStatusData() {

        UpdateOpenShipmentStatusData updateOpenShipmentStatusData = new UpdateOpenShipmentStatusData();
        updateOpenShipmentStatusData.setLocationId("DFWD");
        updateOpenShipmentStatusData.setOpenShipmentStatusCode("confirmed");
        updateOpenShipmentStatusData.setReferenceId("1234");
        updateOpenShipmentStatusData.setTeamMemberId("999990000");
        updateOpenShipmentStatusData.setTrackingId("3456");
        return updateOpenShipmentStatusData;
    }

    /**
     * It will set the values for OpenShipment objects
     * 
     * @return
     */
    private OpenShipment createOpenShipment() {

        OpenShipment openShipment = new OpenShipment();
        DateTimeFormatter formatDate = DateTimeFormatter.ISO_DATE_TIME.withZone(ZoneOffset.UTC);
        openShipment.setInitialAcceptanceTmStmp(ZonedDateTime.parse("2012-01-01T00:00", formatDate)
                .toString());
        openShipment.setOpenShipmentStatusTmStmp(ZonedDateTime.parse("2012-01-01T00:00", formatDate)
                .toString());
        openShipment.setPackageAcceptTmStmp(ZonedDateTime.parse("2012-01-01T00:00", formatDate)
                .toString());
        openShipment.setShipmentData(
                "{\"openShipment\": {\"locationId\": \"TRLA\",\"shipmentIndex\": \"123456789\",\"carrierDetails\": {\"carrierCode\": \"FDXE\",\"opco\": \"EXPRESS\"},\"serviceType\": \"PRIORITY_OVERNIGHT\",\"packagingType\": \"YOUR_PACKAGING\",\"packageCount\": 0,\"recipient\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"specialServicesRequested\": {\"specialServices\": [{\"specialServiceType\": \"SIGNATURE_OPTION\",\"specialServiceSubType\": \"\",\"displayText\": \"ASR\",\"description\": \"Adult signature\"}],\"holdAtLocationDetail\": {\"locationId\": \"DREK\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"}}},\"totalInsuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"totalWeight\": {\"units\": \"LB\",\"value\": \"3\",\"measurementType\": \"MANUAL\"},\"packageLineItems\": [{\"dimensions\": {\"length\": \"1\",\"width\": \"1\",\"height\": \"1\",\"units\": \"IN\"},\"insuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"specialHandlingDetail\": \"CUSTOMER\",\"specialServicesDetails\": [{\"specialServiceType\": \"SIGNATURE_OPTION\",\"specialServiceSubType\": \"\",\"displayText\": \"ASR\",\"description\": \"Adult signature required\"}],\"weight\": {\"units\": \"LB\",\"value\": \"2\",\"measurementType\": \"MANUAL\"},\"signatureOption\": \"ADULT\",\"trackingId\": {\"trackingNumber\": \"123456789\",\"barcode\": \"11111111111111111111111111111111\",\"sequence\": 1},\"master\": true}],\"shipper\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"customer\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"rateDetails\": {\"serviceDetails\": {\"serviceType\": \"SIGNATURE_OPTTION\",\"serviceName\": \"\",\"description\": \"ASR\"},\"deliveryDayOfWeek\": \"FRI\",\"hal\": false,\"commitDetails\": {\"commitTimestamp\": \"2020-08-21T23:59:00\",\"dayOfWeek\": \"FRI\"},\"currency\": \"USD\",\"ratingType\": \"ONE_RATE\",\"totalBaseCharges\": \"1.23\",\"totalRebates\": \"1.23\",\"totalNetCharges\": \"1.23\",\"totalSurcharges\": \"1.23\",\"totalTaxes\": \"1.23\",\"totalFreightDiscounts\": \"1.23\",\"totalNetFreight\": \"1.23\",\"totalNetFedExCharge\": \"1.23\",\"totalDutiesAndTaxes\": \"1.23\",\"totalAncillaryFeesAndTaxes\": \"1.23\",\"totalDutiesTaxesAndFees\": \"1.23\",\"totalNetChargeWithDutiesAndTaxes\": \"1.23\",\"surcharges\": [{\"surchargeType\": \"INSURED_VALUE\",\"description\": \"Declared value\",\"amount\": \"1.23\"}]},\"paymentDetails\": {\"paymentType\": \"CASH\",\"paymentSource\": \"SPOS\",\"paymentAmount\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"responsibleParty\": \"SENDER\",\"payor\": {\"accountType\": \"FEDEX_EXPRESS\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"accountNumber\": \"1110093\",\"creditCard\": {\"maskedcreditCard\": \"string\",\"type\": \"VISA\",\"expirationDate\": \"string\"}}},\"labelSpecification\": {\"processingOptionsRequested\": \"string\",\"disposition\": [\"string\"],\"formatType\": \"OPERATIONAL_LABEL\",\"imageType\": \"PNG\",\"stockType\": \"STOCK_4X6\",\"printingOrientation\": \"TOP_EDGE_OF_TEXT_FIRST\",\"rotation\": \"string\",\"labelOrder\": \"string\",\"localization\": \"string\",\"labelOrigin\": \"string\",\"customerSpecifiedDetail\": \"string\"}}}");
        return openShipment;
    }

    /**
     * Test case to verify that when request is made to
     * {@link OpenShipmentRepositoryImpl#getOpenShipmentDataFromBlob(String)} then OpenShipmentData
     * is received
     * 
     */
    @Test
    public void testGetOpenShipmentDataFromBlob() {

        // Instance of ShipmentData as JSON
        String shipmentData =
                "{\"openShipment\": {\"locationId\": \"TRLA\",\"shipmentIndex\": \"123456789\",\"carrierDetails\": {\"carrierCode\": \"FDXE\",\"opco\": \"EXPRESS\"},\"serviceType\": \"PRIORITY_OVERNIGHT\",\"packagingType\": \"YOUR_PACKAGING\",\"packageCount\": 0,\"recipient\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"specialServicesRequested\": {\"specialServices\": [{\"specialServiceType\": \"SIGNATURE_OPTION\",\"specialServiceSubType\": \"\",\"displayText\": \"ASR\",\"description\": \"Adult signature\"}],\"holdAtLocationDetail\": {\"locationId\": \"DREK\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"}}},\"totalInsuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"totalWeight\": {\"units\": \"LB\",\"value\": \"3\",\"measurementType\": \"MANUAL\"},\"packageLineItems\": [{\"dimensions\": {\"length\": \"1\",\"width\": \"1\",\"height\": \"1\",\"units\": \"IN\"},\"insuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"specialHandlingDetail\": \"CUSTOMER\",\"specialServicesDetails\": [{\"specialServiceType\": \"SIGNATURE_OPTION\",\"specialServiceSubType\": \"\",\"displayText\": \"ASR\",\"description\": \"Adult signature required\"}],\"weight\": {\"units\": \"LB\",\"value\": \"2\",\"measurementType\": \"MANUAL\"},\"signatureOption\": \"ADULT\",\"trackingId\": {\"trackingNumber\": \"123456789\",\"barcode\": \"11111111111111111111111111111111\",\"sequence\": 1},\"master\": true}],\"shipper\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"customer\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"rateDetails\": {\"serviceDetails\": {\"serviceType\": \"SIGNATURE_OPTTION\",\"serviceName\": \"\",\"description\": \"ASR\"},\"deliveryDayOfWeek\": \"FRI\",\"hal\": false,\"commitDetails\": {\"commitTimestamp\": \"2020-08-21T23:59:00\",\"dayOfWeek\": \"FRI\"},\"currency\": \"USD\",\"ratingType\": \"ONE_RATE\",\"totalBaseCharges\": \"1.23\",\"totalRebates\": \"1.23\",\"totalNetCharges\": \"1.23\",\"totalSurcharges\": \"1.23\",\"totalTaxes\": \"1.23\",\"totalFreightDiscounts\": \"1.23\",\"totalNetFreight\": \"1.23\",\"totalNetFedExCharge\": \"1.23\",\"totalDutiesAndTaxes\": \"1.23\",\"totalAncillaryFeesAndTaxes\": \"1.23\",\"totalDutiesTaxesAndFees\": \"1.23\",\"totalNetChargeWithDutiesAndTaxes\": \"1.23\",\"surcharges\": [{\"surchargeType\": \"INSURED_VALUE\",\"description\": \"Declared value\",\"amount\": \"1.23\"}]},\"paymentDetails\": {\"paymentType\": \"CASH\",\"paymentSource\": \"SPOS\",\"paymentAmount\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"responsibleParty\": \"SENDER\",\"payor\": {\"accountType\": \"FEDEX_EXPRESS\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"accountNumber\": \"1110093\",\"creditCard\": {\"maskedcreditCard\": \"string\",\"type\": \"VISA\",\"expirationDate\": \"string\"}}},\"labelSpecification\": {\"processingOptionsRequested\": \"string\",\"disposition\": [\"string\"],\"formatType\": \"OPERATIONAL_LABEL\",\"imageType\": \"PNG\",\"stockType\": \"STOCK_4X6\",\"printingOrientation\": \"TOP_EDGE_OF_TEXT_FIRST\",\"rotation\": \"string\",\"labelOrder\": \"string\",\"localization\": \"string\",\"labelOrigin\": \"string\",\"customerSpecifiedDetail\": \"string\"}}}";
        OpenShipmentData openShipmentData = repo.getOpenShipmentDataFromBlob(shipmentData);
        assertThat(openShipmentData, notNullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link OpenShipmentRepositoryImpl#getOpenShipmentDataFromBlob(String)} with invalid data then
     * it is handled successfully
     * 
     */
    @Test
    public void testGetOpenShipmentDataFromBlobForInvalidData() {

        // Instance of ShipmentData as JSON
        String shipmentData = "{\"openShipment\": {\"locationId\\";
        OpenShipmentData openShipmentData = repo.getOpenShipmentDataFromBlob(shipmentData);
        assertThat(openShipmentData, nullValue());
    }

}
