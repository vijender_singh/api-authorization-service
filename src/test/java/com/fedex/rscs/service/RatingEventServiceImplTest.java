package com.fedex.rscs.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fedex.rscs.config.AppConfig;
import com.fedex.rscs.model.OpenShipmentData;
import com.fedex.rscs.model.OpenShipmentDetail;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.repository.ConfirmShipmentRepository;
import com.fedex.services.common.jms.publish.FedExJmsPublisher;
import com.fedex.services.common.jms.publish.exception.JmsPublishUnavailableException;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test cases to valid the functionality of {@link RatingEventServiceImpl} component
 * 
 * @author Vijender.Singh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class RatingEventServiceImplTest {

    @InjectMocks
    private RatingEventServiceImpl ratingEventServce;
    @Mock
    private FedExJmsPublisher groundTransactionPublisher;
    @Mock
    private GroundShippingRequestMapper mapper;
    @Mock
    private AppConfig appConfig;
    @Mock
    private ConfirmShipmentRepository confirmShipmentRepository;

    @Before
    public void setup() {

        ratingEventServce =
                new RatingEventServiceImpl(groundTransactionPublisher, mapper, appConfig, confirmShipmentRepository);
    }

    /**
     * Test case to validate the scenario if the
     * {@link RatingEventServiceImpl#sendGroundTransactionRate(ShipmentConfirmationRequest, openShipment)}
     * is called and ground rate data is published to JMS successfully
     */
    @Test
    public void testSendGroundTransactionRateSuccess() {

        ShipmentConfirmationRequest scr = new ShipmentConfirmationRequest();
        scr.setTrackingId("12345678");
        scr.setShipmentTimeStamp("2021-04-06T13:10:28.234+05:30");
        OpenShipmentData osd = new OpenShipmentData();
        osd.setOpenShipment(new OpenShipmentDetail());
        when(appConfig.isGroundShippingEvent()).thenReturn(true);
        ratingEventServce.sendGroundTransactionRate(scr, osd);
        verify(confirmShipmentRepository, times(1)).updateGroundRateNotification("12345678",
                "2021-04-06T13:10:28.234+05:30");

    }

    /**
     * Test case to validate the scenario if the config is disabled
     * {@link RatingEventServiceImpl#sendGroundTransactionRate(ShipmentConfirmationRequest, openShipment)}
     * is called and ground rate data is not published to JMS
     */
    @Test
    public void testSendGroundTransactionRateFailure() {

        ShipmentConfirmationRequest scr = new ShipmentConfirmationRequest();
        scr.setTrackingId("12345678");
        OpenShipmentData osd = new OpenShipmentData();
        osd.setOpenShipment(new OpenShipmentDetail());
        when(appConfig.isGroundShippingEvent()).thenReturn(false);
        ratingEventServce.sendGroundTransactionRate(scr, osd);
        verify(confirmShipmentRepository, times(0)).updateGroundRateNotification("12345678",
                "2021-04-06T13:10:28.234+05:30");
    }

    /**
     * Test case to validate the scenario if the config is enabled and Exception is handled
     * {@link RatingEventServiceImpl#sendGroundTransactionRate(ShipmentConfirmationRequest, openShipment)}
     * is called and ground rate data is not published to JMS
     */
    @Test
    public void testSendGroundTransactionRateFailureWithJMSException() {

        ShipmentConfirmationRequest scr = new ShipmentConfirmationRequest();
        scr.setTrackingId("12345678");
        OpenShipmentData osd = new OpenShipmentData();
        osd.setOpenShipment(new OpenShipmentDetail());
        when(appConfig.isGroundShippingEvent()).thenReturn(true);
        when(mapper.createGroundShippingData(scr, osd)).thenThrow(JmsPublishUnavailableException.class);
        ratingEventServce.sendGroundTransactionRate(scr, osd);
        verify(confirmShipmentRepository, times(0)).updateGroundRateNotification("12345678",
                "2021-04-06T13:10:28.234+05:30");
    }

    /**
     * Test case to validate the scenario if the config is enabled and any runtime Exception is handled
     * {@link RatingEventServiceImpl#sendGroundTransactionRate(ShipmentConfirmationRequest, openShipment)}
     * is called and ground rate data is not published to JMS
     */
    @Test
    public void testSendGroundTransactionRateFailureWithRuntimeException() {

        ShipmentConfirmationRequest scr = new ShipmentConfirmationRequest();
        scr.setTrackingId("12345678");
        OpenShipmentData osd = new OpenShipmentData();
        osd.setOpenShipment(new OpenShipmentDetail());
        when(appConfig.isGroundShippingEvent()).thenReturn(true);
        when(mapper.createGroundShippingData(scr, osd)).thenThrow(RuntimeException.class);
        ratingEventServce.sendGroundTransactionRate(scr, osd);
        verify(confirmShipmentRepository, times(0)).updateGroundRateNotification("12345678",
                "2021-04-06T13:10:28.234+05:30");
    }
}
