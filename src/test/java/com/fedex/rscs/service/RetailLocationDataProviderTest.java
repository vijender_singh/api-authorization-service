package com.fedex.rscs.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.netflix.hystrix.exception.HystrixRuntimeException;

import com.fedex.rscs.client.rtls.RetailLocationServiceClient;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.ServiceProcessorTerminalException;
import com.fedex.rscs.exception.client.ClientTerminalException;
import com.fedex.rscs.model.LocationContextDetail;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test cases to valid the functionality of {@link RetailLocationDataProvider} component.
 * 
 * @author shashank.jain
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class RetailLocationDataProviderTest {

    @Mock
    private RetailLocationServiceClient locationServiceClient;

    private RetailLocationDataProvider locationDataProvider;

    private static final String TEST_LOCATION_ID = "DNEK";

    @Before
    public void setup() {

        locationDataProvider = new RetailLocationDataProvider(locationServiceClient);
    }

    /**
     * Method to build the mock {@link LocationContextDetail} response of service client.
     * 
     * @return
     */
    private LocationContextDetail getLocationContextDetail() {

        return new LocationContextDetail("033600003", "4341903", "6988194", null, "FXE", "NQAA");
    }

    /**
     * Test case to validate the scenario if the
     * {@link RetailLocationDataProvider#getContextDetails(String)} is called and retail location
     * service client responded with the location context details, then same information should be
     * returned from this method.
     * 
     */
    @Test
    public void testContextDetailsWhenLocationServiceCallSuccess() {

        final LocationContextDetail expected = getLocationContextDetail();
        when(locationServiceClient.getLocationContextDetails(TEST_LOCATION_ID)).thenReturn(expected);
        LocationContextDetail actual = locationDataProvider.getContextDetails(TEST_LOCATION_ID);
        verify(locationServiceClient, times(1)).getLocationContextDetails(TEST_LOCATION_ID);
        assertThat(actual, notNullValue());
        assertThat(actual.getCityCenterAccountNumber(), equalTo(expected.getCityCenterAccountNumber()));
        assertThat(actual.getBtcMeterNumber(), equalTo(expected.getBtcMeterNumber()));
        assertThat(actual.getGroundAccountNumber(), equalTo(expected.getGroundAccountNumber()));

    }

    /**
     * Test case to validate the scenario if the
     * {@link RetailLocationDataProvider#getContextDetails(String)} is called and retail location
     * service client responded with exception, then same exception is thrown from this method.
     * 
     */
    @Test(expected = ServiceProcessorTerminalException.class)
    public void testContextDetailsWhenLocationServiceCallFailure() {

        doThrow(new ClientTerminalException(ServiceErrorCode.INVALID_LOCATION_ID)).when(locationServiceClient)
                .getLocationContextDetails(TEST_LOCATION_ID);
        locationDataProvider.getContextDetails(TEST_LOCATION_ID);
    }
    
    /**
     * Test case to validate the scenario if the
     * {@link RetailLocationDataProvider#getContextDetails(String)} is called and retail location
     * service client responded with exception, then same exception is thrown from this method.
     * 
     */
    @Test(expected = ServiceProcessorTerminalException.class)
    public void testContextDetailsWhenLocationServiceCallTimeout() {

        doThrow(new HystrixRuntimeException(HystrixRuntimeException.FailureType.TIMEOUT, null,
                "Unable to process request. Please try again later.", null, null)).when(locationServiceClient)
                        .getLocationContextDetails(TEST_LOCATION_ID);
        locationDataProvider.getContextDetails(TEST_LOCATION_ID);
    }
    
}
