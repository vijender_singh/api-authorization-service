package com.fedex.rscs.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.fedex.rscs.dto.PaymentType;
import com.fedex.rscs.model.AddressDetail;
import com.fedex.rscs.model.InterlineShippingData;
import com.fedex.rscs.model.LabelSpecificationDetail;
import com.fedex.rscs.model.OpenShipmentData;
import com.fedex.rscs.model.OpenShipmentDetail;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.PaymentDetail;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.ShipmentSpecialServiceDetail;
import com.fedex.rscs.model.SpecialServiceDescriptionDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.common.CurrencyType;
import com.fedex.rscs.model.common.Location;
import com.fedex.rscs.model.common.LocationDataDetail;
import com.fedex.rscs.model.common.PackagePreauthorizedRatedDetail;
import com.fedex.rscs.model.common.PreauthorizedRateCommitDetail;
import com.fedex.rscs.model.common.PreauthorizedRateServiceDetail;
import com.fedex.rscs.model.common.Price;
import com.fedex.rscs.model.common.ShipmentPreauthorizedRatedDetail;
import com.fedex.rscs.model.cshp.CompletedPackageData;
import com.fedex.rscs.model.cshp.CreditCardData;
import com.fedex.rscs.model.cshp.PreauthorizedRateData;
import com.fedex.rscs.model.cshp.TrackingSequenceDetail;
import com.fedex.rscs.model.ground.jms.GroundShippingData;
import com.fedex.rscs.service.GroundShippingRequestMapper.PrePaymentType;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test cases to valid the functionality of {@link GroundShippingRequestMapper} component.
 * 
 * @author Vijender.Singh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class GroundShippingRequestMapperTest {

    private GroundShippingRequestMapper mapper;

    @Before
    public void setup() {

        mapper = new GroundShippingRequestMapper();
    }

    /**
     * Test case to validate the scenario if the
     * {@link GroundShippingRequestMapper#createGroundShippingData(ShipmentConfirmationRequest,OpenShipmentData)}
     * is called then correct GroundShippingData object is returned
     * 
     */
    @Test
    public void testCreateGroundShippingDataSuccess() {

        ShipmentConfirmationRequest request = createShipmentConfirmationRequest();
        OpenShipmentData openShipment = createOpenShipmentData();
        GroundShippingData response = mapper.createGroundShippingData(request, openShipment);
        assertThat(response, notNullValue());
    }

    /**
     * Test case to validate the scenario when payment details is null
     * {@link GroundShippingRequestMapper#createGroundShippingData(ShipmentConfirmationRequest,OpenShipmentData)}
     * is called then correct GroundShippingData object is returned
     * 
     */
    @Test
    public void testCreateGroundShippingDataWhenPaymentDetailsIsNull() {

        ShipmentConfirmationRequest request = createShipmentConfirmationRequest();
        OpenShipmentData openShipment = createOpenShipmentData();
        request.setPaymentDetail(null);
        GroundShippingData response = mapper.createGroundShippingData(request, openShipment);
        assertThat(response, notNullValue());
        assertThat(response.getPrePaymentInfo(), nullValue());
    }

    /**
     * Test case to validate the scenario when payment details is not null and payment type is check
     * {@link GroundShippingRequestMapper#createGroundShippingData(ShipmentConfirmationRequest,OpenShipmentData)}
     * is called then correct GroundShippingData object is returned
     * 
     */
    @Test
    public void testCreateGroundShippingDataWhenPaymentIsCheck() {

        ShipmentConfirmationRequest request = createShipmentConfirmationRequest();
        OpenShipmentData openShipment = createOpenShipmentData();
        request.getPaymentDetail()
                .setPaymentType(PaymentType.CHECK.name());;
        GroundShippingData response = mapper.createGroundShippingData(request, openShipment);
        assertThat(response, notNullValue());
        assertEquals(response.getPrePaymentInfo()
                .getMode(), PrePaymentType.CHK.name());
    }

    /**
     * Test case to validate the scenario when payment details is not null and payment type is credit
     * card
     * {@link GroundShippingRequestMapper#createGroundShippingData(ShipmentConfirmationRequest,OpenShipmentData)}
     * is called then correct GroundShippingData object is returned
     * 
     */
    @Test
    public void testCreateGroundShippingDataWhenPaymentIsCreditCard() {

        ShipmentConfirmationRequest request = createShipmentConfirmationRequest();
        OpenShipmentData openShipment = createOpenShipmentData();
        request.getPaymentDetail()
                .setPaymentType(PaymentType.CREDIT_CARD.name());
        request.getPaymentDetail()
                .setPayor(createPayor());;
        GroundShippingData response = mapper.createGroundShippingData(request, openShipment);
        assertThat(response, notNullValue());
        assertEquals(response.getPrePaymentInfo()
                .getMode(), PrePaymentType.CCA.name());
        assertEquals(response.getPrePaymentInfo()
                .getCcType(),
                request.getPaymentDetail()
                        .getPayor()
                        .getCreditCard()
                        .getType());
        assertEquals(response.getPrePaymentInfo()
                .getCcExpirationDate(),
                request.getPaymentDetail()
                        .getPayor()
                        .getCreditCard()
                        .getExpirationDate());
        assertEquals(response.getPrePaymentInfo()
                .getMaskedCCNbr(),
                request.getPaymentDetail()
                        .getPayor()
                        .getCreditCard()
                        .getMaskedCreditCard());
    }

    /**
     * Method to create payor request
     * 
     * @return PartyDetail
     */
    private PartyDetail createPayor() {

        PartyDetail partyDetail = new PartyDetail();
        partyDetail.setAccountNumber("123456");
        partyDetail.setCreditCard(new CreditCardData("1343", "VISA", "12/20"));
        return partyDetail;

    }

    /**
     * Method to create open shipment Data
     * 
     * @return openShpmtData
     */
    private OpenShipmentData createOpenShipmentData() {

        OpenShipmentData openShpmtData = new OpenShipmentData();
        OpenShipmentDetail openShipmentDetail = new OpenShipmentDetail();
        openShipmentDetail.setLabelSpecification(createLabelSpecificationDetail());
        openShipmentDetail.setSpecialServicesRequested(createShipmentSpecialServiceDetail());
        openShipmentDetail.setInterlineShippingData(
                new InterlineShippingData("10038", "AA", "123456", "test", "9810090", "981001"));
        openShipmentDetail.setPreauthorizedRateDetails(createPreauthorizedRateDetails());
        openShipmentDetail.setPackageLineItems(createPackageLineItems());
        openShpmtData.setOpenShipment(openShipmentDetail);
        return openShpmtData;
    }

    /**
     * Method to create package line items
     * 
     * @return completedPackageDataList
     */
    private List<CompletedPackageData> createPackageLineItems() {

        List<CompletedPackageData> completedPackageDataList = new ArrayList<>();
        CompletedPackageData completedPackageData = new CompletedPackageData();
        completedPackageData.setTrackingId(new TrackingSequenceDetail("123456789", "987654321", 1));
        completedPackageDataList.add(completedPackageData);
        return completedPackageDataList;
    }

    /**
     * Method to create PreauthorizedRateData
     * 
     * @return rateData
     */
    private PreauthorizedRateData createPreauthorizedRateDetails() {

        PreauthorizedRateData rateData = new PreauthorizedRateData();
        rateData.setDeliveryTimestamp("2020-07-29T10:30:00.812Z");

        rateData.setServiceType("PRIORITY_OVERNIGHT");
        rateData.setPackagingType("YOUR_PACKAGING");
        rateData.setSignatureOption("Direct");
        rateData.setServiceDetails(new PreauthorizedRateServiceDetail());
        rateData.setCommitDetails(new PreauthorizedRateCommitDetail());
        rateData.setRatedPackages(createRatedPackages());
        rateData.setShipmentRateDetails(createShipmentRateDetails());
        return rateData;
    }

    /**
     * Method to create shipment rate details
     * 
     * @return shipmentPreauthorizedRatedDetail
     */
    private ShipmentPreauthorizedRatedDetail createShipmentRateDetails() {

        ShipmentPreauthorizedRatedDetail sprd = new ShipmentPreauthorizedRatedDetail();
        sprd.setRateZone("3");
        return sprd;
    }

    /**
     * Method to create PackagePreauthorizedRatedDetail
     * 
     * @return packagePreauthorizedRatedDetails
     */
    private List<PackagePreauthorizedRatedDetail> createRatedPackages() {

        List<PackagePreauthorizedRatedDetail> pprdList = new ArrayList<>();
        PackagePreauthorizedRatedDetail pprd = new PackagePreauthorizedRatedDetail();
        pprd.setBaseCharge(new Price(CurrencyType.USD, 10.00));
        pprdList.add(pprd);
        return pprdList;
    }

    /**
     * Method to create ShipmentSpecialServiceDetail request
     * 
     * @return ShipmentSpecialServiceDetail
     */
    private ShipmentSpecialServiceDetail createShipmentSpecialServiceDetail() {

        ShipmentSpecialServiceDetail shipmentSpecialServiceDetail = new ShipmentSpecialServiceDetail();
        List<SpecialServiceDescriptionDetail> listSpecialServicesRequestedType = new ArrayList<>();
        SpecialServiceDescriptionDetail specialServiceDescriptionDetail = new SpecialServiceDescriptionDetail();
        specialServiceDescriptionDetail.setSpecialServiceType("FEDEX_ONE_RATE");
        listSpecialServicesRequestedType.add(specialServiceDescriptionDetail);
        shipmentSpecialServiceDetail.setSpecialServicesRequestedType(listSpecialServicesRequestedType);
        return shipmentSpecialServiceDetail;

    }

    /**
     * Method to create LabelSpecificationDetail object
     * 
     * @return
     */
    private LabelSpecificationDetail createLabelSpecificationDetail() {

        LabelSpecificationDetail labelSpecificationDetail = new LabelSpecificationDetail();

        labelSpecificationDetail.setProcessingOptionsRequested("IGNORE_RESIDENTIAL_DELIVERY_POLICIES");
        labelSpecificationDetail.setFormatType("OPERATIONAL_LABEL");
        labelSpecificationDetail.setStockType("STOCK_4X6");
        labelSpecificationDetail.setImageType("PNG");
        labelSpecificationDetail.setPrintingOrientation("TOP_EDGE_OF_TEXT_FIRST");
        labelSpecificationDetail.setRotation("NA");
        labelSpecificationDetail.setLabelOrigin("ALVA");
        labelSpecificationDetail.setCustomerSpecifiedDetail("BUSINESS");

        return labelSpecificationDetail;

    }

    /**
     * Method to create shipment confirmation request
     * 
     * @return shipmentConfirmationRequest
     */
    private ShipmentConfirmationRequest createShipmentConfirmationRequest() {

        ShipmentConfirmationRequest request = new ShipmentConfirmationRequest();
        request.setWorkstationDetail(createWorkstationDetail());
        request.setReferenceId("3456");
        request.setTrackingId("7898981234");
        request.setTransactionLocationId("LASA");
        request.setShipmentTimeStamp("2020-10-01T10:59:10.273Z");
        request.setPaymentDetail(new PaymentDetail());
        request.getPaymentDetail()
                .setPaymentType(PaymentType.CASH.name());
        request.setLocationDataDetail(new LocationDataDetail("1232341", "9876543", new Location("DFWD", getAddress()),
                false, false, new Location("DFWD", getAddress()), "FXE", "DFWD"));
        return request;
    }

    /**
     * Method to create workstation details
     * 
     * @return workstationDetail
     */
    private WorkstationDetail createWorkstationDetail() {

        WorkstationDetail workstationDetail = new WorkstationDetail();
        workstationDetail.setAccountNumber("038000004");
        workstationDetail.setMeterNumber("6997006");
        workstationDetail.setSoftwareId("SSFE");
        workstationDetail.setAppName("FUSE");
        workstationDetail.setAppVersionId("2.0.2");
        return workstationDetail;
    }

    /**
     * Prepare address object.
     * 
     * @return
     */
    public AddressDetail getAddress() {

        AddressDetail address = new AddressDetail(new ArrayList<String>(), "Dallas", "TX", "75024", "US", true);
        return address;
    }
}
