package com.fedex.rscs.service.task;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fedex.rscs.exception.ErrorCategory;
import com.fedex.rscs.exception.ServiceProcessorTerminalException;
import com.fedex.rscs.exception.ShipmentServiceErrorCode;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.cshp.ConfirmedShipmentData;
import com.fedex.rscs.service.ConfirmShipmentTransactionService;

/**
 * Class for code coverage of ShipmentCheckoutTask class
 * 
 * @author Shakti.Saurabh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ShipmentCheckoutTaskTest {

    @Mock
    private ConfirmShipmentTransactionService confirmShipmentTransactionService;

    @Mock
    private ShipmentConfirmationRequest shipmentConfirmationRequest;

    @InjectMocks
    private ShipmentCheckoutTask shipmentCheckoutTask;

    /**
     * Test case to validate that when request is made to {@link ShipmentCheckoutTask#call()} then
     * executed successfully
     * 
     */
    @Test
    public void testCall() {

        when(confirmShipmentTransactionService.confirmShipment(shipmentConfirmationRequest))
                .thenReturn(new ConfirmedShipmentData());
        try {
            ConfirmedShipmentData confirmedShipmentData = shipmentCheckoutTask.call();
            assertThat(confirmedShipmentData, notNullValue());
            assertThat(confirmedShipmentData.isLineItemSuccess(), equalTo(true));
        } catch (Exception e) {

        }
    }
    
    /**
     * Test case to validate that when request is made to {@link ShipmentCheckoutTask#call()} then
     * exception thrown
     * 
     */
    @Test
    public void testCallWithException() {

        when(confirmShipmentTransactionService.confirmShipment(shipmentConfirmationRequest))
                .thenThrow(new ServiceProcessorTerminalException(new ShipmentServiceErrorCode("400","Bad request",ErrorCategory.BAD_REQUEST)));
        try {
            ConfirmedShipmentData confirmedShipmentData = shipmentCheckoutTask.call();
            assertThat(confirmedShipmentData, notNullValue());
            assertThat(confirmedShipmentData.isLineItemSuccess(), equalTo(false));
            assertThat(confirmedShipmentData.isLineItemSuccess(), notNullValue());
            assertThat(confirmedShipmentData.getNotifications(),hasSize(1));
        } catch (Exception e) {

        }
    }

}
