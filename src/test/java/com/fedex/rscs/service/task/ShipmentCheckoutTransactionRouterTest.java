package com.fedex.rscs.service.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.cshp.ConfirmedShipmentData;
import com.fedex.rscs.service.ConfirmShipmentTransactionService;

/**
 * Class for code coverage of ShipmentCheckoutTransactionRouter class
 * 
 * @author Shakti.Saurabh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ShipmentCheckoutTransactionRouterTest {

    @Mock
    private ExecutorService executorService;

    @Mock
    private ConfirmShipmentTransactionService transactionService;

    @InjectMocks
    private ShipmentCheckoutTransactionRouter shipmentCheckoutTransactionRouter;

    /**
     * Test case to validate that when request is made to
     * {@link ShipmentCheckoutTransactionRouter#fork()} then executed successfully
     * 
     */
    @Test
    public void testFork() {

        List<ShipmentConfirmationRequest> arrays = new ArrayList<>();
        ShipmentConfirmationRequest request = new ShipmentConfirmationRequest();
        request.setTrackingId("1234");
        arrays.add(request);
        Map<String, Future<ConfirmedShipmentData>> map = shipmentCheckoutTransactionRouter.fork(arrays);
        assertThat(map, notNullValue());
    }
    
    /**
     * Test case to validate that when request is made to
     * {@link ShipmentCheckoutTransactionRouter#join()} then executed successfully
     * 
     */
    @Test
    public void testJoin() {

        Future<ConfirmedShipmentData> future = mock(Future.class);
        when(future.isDone()).thenReturn(true);
        try {
            when(future.get()).thenReturn(new ConfirmedShipmentData());

            Map<String, Future<ConfirmedShipmentData>> map = new HashMap<>();
            map.put("1234", future);
            Map<String, ConfirmedShipmentData> resultMap = shipmentCheckoutTransactionRouter.join(map);
            assertThat(resultMap, notNullValue());
        } catch (InterruptedException | ExecutionException e) {
        }

    }
    
    /**
     * Test case to validate that when request is made to
     * {@link ShipmentCheckoutTransactionRouter#join()} then exception thrown
     * 
     */
    @Test
    public void testJoinException() {

        Future<ConfirmedShipmentData> future = mock(Future.class);
        when(future.isDone()).thenReturn(true);
        try {
            when(future.get()).thenThrow(new InterruptedException());

            Map<String, Future<ConfirmedShipmentData>> map = new HashMap<>();
            map.put("1234", future);
            shipmentCheckoutTransactionRouter.join(map);
        } catch (Exception e) {
            assertThat(e.getMessage(), equalTo("failed to process and join transaction routes."));
        }
    }
}
