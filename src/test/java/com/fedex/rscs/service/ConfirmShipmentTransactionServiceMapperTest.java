package com.fedex.rscs.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.fedex.rscs.dto.CarrierCodeType;
import com.fedex.rscs.model.AddressDetail;
import com.fedex.rscs.model.InterlineShippingData;
import com.fedex.rscs.model.LabelSpecificationDetail;
import com.fedex.rscs.model.OpenShipment;
import com.fedex.rscs.model.OpenShipmentData;
import com.fedex.rscs.model.OpenShipmentDetail;
import com.fedex.rscs.model.OperatingCompany;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.PaymentDetail;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ServiceConstant;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.ShipmentContactDetail;
import com.fedex.rscs.model.ShipmentDetail;
import com.fedex.rscs.model.ShipmentPackageAdditionalService;
import com.fedex.rscs.model.ShipmentPackageDetail;
import com.fedex.rscs.model.ShipmentSpecialServiceDetail;
import com.fedex.rscs.model.SpecialServiceDescriptionDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.common.Location;
import com.fedex.rscs.model.common.LocationDataDetail;
import com.fedex.rscs.model.common.PackageCancellationEventDetailRequest;
import com.fedex.rscs.model.common.PackagePossessionEventDetailRequest;
import com.fedex.rscs.model.common.PersonDetail;
import com.fedex.rscs.model.cshp.CompleteShipmentDetail;
import com.fedex.rscs.model.cshp.CompletedPackageData;
import com.fedex.rscs.model.cshp.ConfirmedShipmentData;
import com.fedex.rscs.model.cshp.CreditCardData;
import com.fedex.rscs.model.cshp.PackageBarcodeDetail;
import com.fedex.rscs.model.cshp.PackageOperationData;
import com.fedex.rscs.model.cshp.Price;
import com.fedex.rscs.model.cshp.ProductDescription;
import com.fedex.rscs.model.cshp.ShipmentOperationData;
import com.fedex.rscs.model.cshp.ShippingDocument;
import com.fedex.rscs.model.cshp.StringBarcodeDetail;
import com.fedex.rscs.model.cshp.TrackingIdDetail;
import com.fedex.rscs.model.cshp.TrackingSequenceDetail;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;
import com.fedex.rscs.util.RetailShipmentCreationServiceUtil;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Test cases required to unit test ConfirmShipmentTransactionServiceMapperTest class
 * 
 * @author 5034922
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ConfirmShipmentTransactionServiceMapperTest {

    private ConfirmShipmentTransactionServiceMapper mapper;

    @Mock
    RetailShipmentCreationServiceUtil serviceUtil;

    @Before
    public void setup() {

        mapper = new ConfirmShipmentTransactionServiceMapper(serviceUtil);
        mapper.objectMapper = new ObjectMapper();
    }

    /**
     * test when response is null
     * 
     */
    @Test
    public void testBuildReponseWhenResponseNull() {

        ConfirmedShipmentData actual = mapper.buildResponse(null);

        assertThat(actual, nullValue());

    }

    /**
     * Test buildResponse method.
     */
    @Test
    public void testBuildResponse() {

        ConfirmedShipmentData actual = mapper.buildResponse(createCompleteShipmentDetail());

        assertThat(actual, notNullValue());
        assertThat(actual.getCarrierCode(), equalTo("FDXE"));
        assertThat(actual.getPackageLineItems(), notNullValue());
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getTrackingId(), notNullValue());
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getTrackingId()
                .getBarcode(), notNullValue());
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getLabel(), notNullValue());


    }

    /**
     * test when packageLineItem is null
     * 
     */
    @Test
    public void testBuildReponseWhenPackageItemNull() {

        CompleteShipmentDetail details = createCompleteShipmentDetail();
        details.setCompletedPackageDetails(null);
        ConfirmedShipmentData actual = mapper.buildResponse(details);

        assertThat(actual, notNullValue());
        assertThat(actual.getCarrierCode(), equalTo("FDXE"));
        assertThat(actual.getCarrierType(), equalTo("EXPRESS"));
        assertThat(actual.getPackageLineItems(), nullValue());

    }

    /**
     * test when packageLineItem list is empty
     * 
     */
    @Test
    public void testBuildReponseWhenPackageItemEmpty() {

        CompleteShipmentDetail details = createCompleteShipmentDetail();
        details.setCompletedPackageDetails(new ArrayList<CompletedPackageData>());
        details.setCarrierCode("FDXG");
        ConfirmedShipmentData actual = mapper.buildResponse(details);

        assertThat(actual, notNullValue());
        assertThat(actual.getCarrierCode(), equalTo("FDXG"));
        assertThat(actual.getCarrierType(), equalTo("GROUND"));
        assertThat(actual.getPackageLineItems(), nullValue());

    }

    /**
     * test when packageLineItem list is empty
     * 
     */
    @Test
    public void testBuildReponseWhenLabelNull() {

        CompleteShipmentDetail details = createCompleteShipmentDetail();
        details.setShipmentDocuments(null);
        ConfirmedShipmentData actual = mapper.buildResponse(details);

        assertThat(actual, notNullValue());
        assertThat(actual.getCarrierCode(), equalTo("FDXE"));
        assertThat(actual.getPackageLineItems(), notNullValue());
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getLabel(), nullValue());

    }

    /**
     * test when packageLineItem list is empty
     * 
     */
    @Test
    public void testBuildReponseWhenLabelEmpty() {

        CompleteShipmentDetail details = createCompleteShipmentDetail();
        details.setShipmentDocuments(new ArrayList<ShippingDocument>());
        ConfirmedShipmentData actual = mapper.buildResponse(details);

        assertThat(actual, notNullValue());
        assertThat(actual.getCarrierCode(), equalTo("FDXE"));
        assertThat(actual.getPackageLineItems(), notNullValue());
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getLabel(), nullValue());

    }

    /**
     * test when operationDetail is null
     * 
     */
    @Test
    public void testBuildReponseWhenOperationDataNull() {

        CompleteShipmentDetail details = createCompleteShipmentDetail();
        details.getCompletedPackageDetails()
                .get(0)
                .setOperationalDetail(null);
        ConfirmedShipmentData actual = mapper.buildResponse(details);

        assertThat(actual, notNullValue());
        assertThat(actual.getCarrierCode(), equalTo("FDXE"));
        assertThat(actual.getCarrierType(), equalTo("EXPRESS"));
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getTrackingId(), nullValue());

    }

    /**
     * test when barcode is null
     * 
     */
    @Test
    public void testBuildReponseWhenBarcodeNull() {

        CompleteShipmentDetail details = createCompleteShipmentDetail();
        details.getCompletedPackageDetails()
                .get(0)
                .getOperationalDetail()
                .setBarcode(null);
        ConfirmedShipmentData actual = mapper.buildResponse(details);

        assertThat(actual, notNullValue());
        assertThat(actual.getCarrierCode(), equalTo("FDXE"));
        assertThat(actual.getCarrierType(), equalTo("EXPRESS"));
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getTrackingId(), nullValue());

    }

    /**
     * test when StringBarcode list is empty
     * 
     */
    @Test
    public void testBuildReponseWhenStringBarcodeListEmpty() {

        CompleteShipmentDetail details = createCompleteShipmentDetail();
        details.getCompletedPackageDetails()
                .get(0)
                .getOperationalDetail()
                .getBarcode()
                .setStringBarcodes(new ArrayList<StringBarcodeDetail>());
        ConfirmedShipmentData actual = mapper.buildResponse(details);

        assertThat(actual, notNullValue());
        assertThat(actual.getCarrierCode(), equalTo("FDXE"));
        assertThat(actual.getCarrierType(), equalTo("EXPRESS"));
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getTrackingId(), nullValue());

    }

    /**
     * test when StringBarcode list is null
     * 
     */
    @Test
    public void testBuildReponseWhenStringBarcodeListNull() {

        CompleteShipmentDetail details = createCompleteShipmentDetail();
        details.getCompletedPackageDetails()
                .get(0)
                .getOperationalDetail()
                .getBarcode()
                .setStringBarcodes(null);
        ConfirmedShipmentData actual = mapper.buildResponse(details);

        assertThat(actual, notNullValue());
        assertThat(actual.getCarrierCode(), equalTo("FDXE"));
        assertThat(actual.getCarrierType(), equalTo("EXPRESS"));
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getTrackingId(), nullValue());

    }

    /**
     * test when StringBarcode list is null
     * 
     */
    @Test
    public void testBuildReponseWhenTRackingIDsNull() {

        CompleteShipmentDetail details = createCompleteShipmentDetail();
        details.getCompletedPackageDetails()
                .get(0)
                .setTrackingIds(null);
        ConfirmedShipmentData actual = mapper.buildResponse(details);

        assertThat(actual, notNullValue());
        assertThat(actual.getCarrierCode(), equalTo("FDXE"));
        assertThat(actual.getCarrierType(), equalTo("EXPRESS"));
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getTrackingId(), nullValue());

    }

    /**
     * Test buildConfirmedShipmentResponse(ShipmentDetail) when object passed is null
     * 
     */
    @Test
    public void testBuildConfirmedShipmentResponseForNull() {

        ConfirmedShipmentData actual = mapper.buildConfirmedShipmentResponse(null);
        assertThat(actual, nullValue());
    }

    /**
     * Test buildConfirmedShipmentResponse(ShipmentDetail) when ShipmentDetail object passed with
     * ServiceOpCoName as null is mapped successfully
     * 
     */
    @Test
    public void testBuildConfirmedShipmentResponseWithServiceOpCoNameNull() {

        ShipmentDetail shipmentDetail = createShipmentDetail();
        shipmentDetail.setOpcoShippingServiceName(null);
        shipmentDetail.setCurrentShipmentStatus(null);

        ConfirmedShipmentData actual = mapper.buildConfirmedShipmentResponse(shipmentDetail);

        assertThat(actual, notNullValue());
        assertThat(actual.getCarrierType(), nullValue());
        assertThat(actual.getPackageLineItems(), notNullValue());
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getTrackingId(), notNullValue());
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getTrackingId()
                .getBarcode(), notNullValue());
    }

    /**
     * Test buildConfirmedShipmentResponse(ShipmentDetail) when ShipmentDetail object passed is mapped
     * successfully
     * 
     */
   @Test
    public void testBuildConfirmedShipmentResponse() {

        ConfirmedShipmentData actual = mapper.buildConfirmedShipmentResponse(createShipmentDetail());

        assertThat(actual, notNullValue());
        assertThat(actual.getCarrierType(), equalTo("EXPRESS"));
        assertThat(actual.getPackageLineItems(), notNullValue());
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getTrackingId(), notNullValue());
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getTrackingId()
                .getBarcode(), notNullValue());
    }

    /**
     * Test buildConfirmedShipmentResponse(ShipmentDetail) when object passed has master package set and
     * ground shipment then it's mapped successfully
     * 
     */
    @Test
    public void testBuildConfirmedShipmentResponseForGroundAndMasterTrkngId() {

        ShipmentDetail shipmentDetail = createShipmentDetail();
        shipmentDetail.setOpcoShippingServiceName("FEDEX_GROUND");
        shipmentDetail.getShipmentPackageDetails()
                .get(0)
                .setMasterFlag(true);
        ConfirmedShipmentData actual = mapper.buildConfirmedShipmentResponse(shipmentDetail);

        assertThat(actual, notNullValue());
        assertThat(actual.getCarrierType(), equalTo("GROUND"));
        assertThat(actual.getPackageLineItems(), notNullValue());
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getTrackingId(), notNullValue());
        assertThat(actual.getPackageLineItems()
                .get(0)
                .getTrackingId()
                .getBarcode(), notNullValue());
    }

    /**
     * Test case to create ShipmentDetail when call is made to
     * {@link ConfirmShipmentTransactionServiceMapper#buildShipmentDetails(ShipmentConfirmationRequest,OpenShipment,LocationDataDetail,ConfirmedShipmentData)}
     * to set the object fields and return a valid response ShipmentDetail object with all mandatory
     * fields covered.
     * 
     */
    @Test
    public void testBuildShipmentDetail() {

        OpenShipment openShipment = buildOpenShipmentWithInterline();
        LocationDataDetail locationDetail = new LocationDataDetail("1232341", "9876543",
                new Location("DFWD", getAddress()), false, false, new Location("DFWD", getAddress()), "FXE", "DFWD");
        ShipmentDetail shipmentDetail = mapper.buildShipmentDetails(openShipment, createShipmentConfirmationRequest(),
                locationDetail, createReply(), buildConfirmedShipmentData());
        assertThat(shipmentDetail, notNullValue());
        assertThat(shipmentDetail.getCityCenterAccountNumber(), equalTo(createWorkstationDetail().getAccountNumber()));
        assertThat(shipmentDetail.getShipmentContactDetail()
                .getCustomerCountryName(), equalTo("US"));
        assertThat(shipmentDetail.getOriginCountryCode(), equalTo(locationDetail.getTransactionLocationInfo()
                .getAddress()
                .getCountryCode()));
        assertThat(shipmentDetail.isStandardPackageFlag(), equalTo(false));
        assertThat(shipmentDetail.getApiVersionId(), equalTo(ServiceConstant.API_VERSION_ID));
        assertThat(shipmentDetail.getApiClientTypeName(),
                equalTo(createShipmentConfirmationRequest().getWorkstationDetail()
                        .getAppName()));
        assertThat(shipmentDetail.getOpcoShippingServiceName(), equalTo(OperatingCompany.FEDEX_GROUND.name()));
    }

    /**
     * Test case to create ShipmentDetail when call is made to
     * {@link ConfirmShipmentTransactionServiceMapper#buildShipmentDetails(ShipmentConfirmationRequest,OpenShipment,LocationDataDetail,ConfirmedShipmentData)}
     * to set the object fields and return a valid response ShipmentDetail object with all mandatory
     * fields covered.
     * 
     */
    @Test
    public void testBuildShipmentDetailEmpty() {

        OpenShipment openShipment = new OpenShipment();
        // Instance of ShipmentData as JSON
        String shipmentData =
                "{\"openShipment\": {\"locationId\": \"TRLA\",\"shipmentIndex\": \"123456789\",\"carrierDetails\": {\"carrierCode\": \"FXO\",\"opco\": \"EXPRESS\"},\"serviceType\": \"PRIORITY_OVERNIGHT\",\"packagingType\": \"FEDEX_PAK\",\"packageCount\": 0,\"recipient\": {\"address\": {\"streetLines\": [\"432 new house\",\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"specialServicesRequested\": {\"specialServices\": [{\"specialServiceType\": \"SIGNATURE_OPTION\",\"specialServiceSubType\": \"NON_STANDARD_CONTAINER\",\"displayText\": \"ASR\",\"description\": \"Adult signature\"}],\"holdAtLocationDetail\": {\"locationId\": \"DREK\",\"address\": {\"streetLines\": [\"347 sector\",\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"}}},\"totalInsuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"totalWeight\": {\"units\": \"LB\",\"value\": \"3\",\"measurementType\": \"MANUAL\"},\"packageLineItems\": [{\"dryIceWeightDetails\": {\"unit\": \"KG\",\"value\": \"12\",\"measurementType\":\"Manual\"},\"dimensions\": {\"length\": \"1\",\"width\": \"1\",\"height\": \"1\",\"units\": \"IN\"},\"insuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"specialHandlingDetail\": \"CUSTOMER\",\"specialServicesDetails\": [{\"specialServiceType\": \"NON_STANDARD_CONTAINER\",\"specialServiceSubType\": \"\",\"displayText\": \"ASR\",\"description\": \"Adult signature required\"}],\"weight\": {\"units\": \"LB\",\"value\": \"2\",\"measurementType\": \"MANUAL\"},\"signatureOption\": \"ADULT\",\"sequenceNumber\":\"2\",\"trackingId\": {\"trackingNumber\": \"123456789\",\"barcode\": \"11111111111111111111111111111111\",\"sequence\": 1},\"master\": true}],\"shipper\": {\"address\": {\"streetLines\": [\"52 sector\",\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"customer\": {\"address\": {\"streetLines\": [\"23 sector\",\"delhi\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"rateDetails\": {\"ratingType\": \"STANDARD_RATE\",\"serviceDetails\": {\"serviceType\": \"BATTERY\",\"serviceName\": \"\",\"description\": \"Expected Lithium Battery\"},\"deliveryDayOfWeek\": \"FRI\",\"hal\": false,\"commitDetails\": {\"commitTimestamp\": \"2020-08-21T23:59:00\",\"dayOfWeek\": \"FRI\"},\"rateDetails\": {\"currency\": \"USD\",\"ratingType\": \"ONE_RATE\",\"totalBaseCharge\": \"1.23\",\"totalRebates\": \"1.23\",\"totalNetCharges\": \"1.23\",\"totalSurcharges\": \"1.23\",\"totalTaxes\": \"1.23\",\"totalFreightDiscounts\": \"1.23\",\"totalNetFreight\": \"1.23\",\"totalNetFedExCharge\": \"1.23\",\"totalDutiesAndTaxes\": \"1.23\",\"totalAncillaryFeesAndTaxes\": \"1.23\",\"totalDutiesTaxesAndFees\": \"1.23\",\"totalNetChargeWithDutiesAndTaxes\": \"1.23\",\"surcharges\": [{\"surchargeType\": \"INSURED_VALUE\",\"description\": \"Declared value\",\"amount\": \"1.23\"}]}},\"paymentDetails\": {\"paymentType\": \"CASH\",\"paymentSource\": \"SPOS\",\"paymentAmount\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"responsibleParty\": \"SENDER\",\"payor\": {\"accountType\": \"FEDEX_EXPRESS\",\"address\": {\"streetLines\": [\"29 sector\",\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"accountNumber\": \"1110093\",\"creditCard\": {\"maskedcreditCard\": \"string\",\"type\": \"VISA\",\"expirationDate\": \"string\"}}},\"labelSpecification\": {\"processingOptionsRequested\": \"string\",\"disposition\": [\"string\"],\"formatType\": \"OPERATIONAL_LABEL\",\"imageType\": \"PNG\",\"stockType\": \"STOCK_4X6\",\"printingOrientation\": \"TOP_EDGE_OF_TEXT_FIRST\",\"rotation\": \"string\",\"labelOrder\": \"string\",\"localization\": \"string\",\"labelOrigin\": \"string\",\"customerSpecifiedDetail\": \"string\"}}}";
        openShipment.setShipmentData(shipmentData);
        LocationDataDetail locationDetail = new LocationDataDetail("1232341", "9876543",
                new Location("DFWD", getAddress()), true, true, new Location("DFWD", getAddress()), "FXE", "DFWD");
        ShipmentDetail shipmentDetail = mapper.buildShipmentDetails(openShipment, createShipmentConfirmationRequest(),
                locationDetail, createReply(), buildConfirmedShipmentData());
        assertThat(shipmentDetail, notNullValue());
        assertThat(shipmentDetail.getRatingType(), equalTo("SR"));
        assertThat(shipmentDetail.isAfterPickupFlag(), equalTo(true));
        assertThat(shipmentDetail.getShipmentContactDetail()
                .getCustomerAddress2Text(), equalTo("delhi"));
        assertThat(shipmentDetail.getShipmentContactDetail()
                .getSenderAddress2Text(), equalTo("noida"));

    }

    /**
     * Test case to create ShipmentDetail when call is made to
     * {@link ConfirmShipmentTransactionServiceMapper#buildShipmentDetails(ShipmentConfirmationRequest,OpenShipment,LocationDataDetail,ConfirmedShipmentData)}
     * to set the object fields and return a valid response ShipmentDetail object with all mandatory
     * fields covered.
     * 
     */
    @Test
    public void testBuildShipmentDetailForAddress3() {

        OpenShipment openShipment = new OpenShipment();
        // Instance of ShipmentData as JSON
        String shipmentData =
                "{\"openShipment\": {\"locationId\": \"TRLA\",\"shipmentIndex\": \"123456789\",\"carrierDetails\": {\"carrierCode\": \"FXO\",\"opco\": \"EXPRESS\"},\"serviceType\": \"PRIORITY_OVERNIGHT\",\"packagingType\": \"YOUR_PACKAGING\",\"physicalPackagingType\":\"ABC\",\"packageCount\": 0,\"recipient\": {\"address\": {\"streetLines\": [\"432 new house\",\"noida\",\"UP\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"specialServicesRequested\": {\"specialServices\": [{\"specialServiceType\": \"SIGNATURE_OPTION\",\"specialServiceSubType\": \"ADULT\",\"displayText\": \"ASR\",\"description\": \"Adult signature\"}],\"holdAtLocationDetail\": {\"locationId\": \"DREK\",\"address\": {\"streetLines\": [\"347 sector\",\"noida\",\"UP\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"}}},\"totalInsuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"totalWeight\": {\"units\": \"LB\",\"value\": \"3\",\"measurementType\": \"MANUAL\"},\"packageLineItems\": [{\"dryIceWeightDetails\": {\"unit\": \"KG\",\"value\":\"12\",\"measurementType\":\"Manual\"},\"dimensions\": {\"length\": \"1\",\"width\": \"1\",\"height\": \"1\",\"units\": \"IN\"},\"insuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"specialHandlingDetail\": \"CUSTOMER\",\"specialServicesDetails\": [{\"specialServiceType\": \"SIGNATURE_OPTION\",\"specialServiceSubType\": \"ADULT\",\"displayText\": \"ASR\",\"description\": \"Adult signature required\"}],\"weight\": {\"units\": \"LB\",\"value\": \"2\",\"measurementType\": \"MANUAL\"},\"signatureOption\": \"ADULT\",\"sequenceNumber\":\"2\",\"trackingId\": {\"trackingNumber\": \"123456789\",\"barcode\": \"11111111111111111111111111111111\",\"sequence\": 1},\"master\": true}],\"shipper\": {\"address\": {\"streetLines\": [\"52 sector\",\"noida\",\"UP\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"customer\": {\"address\": {\"streetLines\": [\"23 sector\",\"delhi\",\"DELHI\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"rateDetails\": {\"ratingType\": \"STANDARD_RATE\",\"serviceDetails\": {\"serviceType\": \"SIGNATURE_OPTTION\",\"serviceName\": \"\",\"description\": \"ASR\"},\"deliveryDayOfWeek\": \"FRI\",\"hal\": false,\"commitDetails\": {\"commitTimestamp\": \"2020-08-21T23:59:00\",\"dayOfWeek\": \"FRI\"},\"rateDetails\": {\"currency\": \"USD\",\"ratingType\": \"ONE_RATE\",\"totalBaseCharge\": \"1.23\",\"totalRebates\": \"1.23\",\"totalNetCharges\": \"1.23\",\"totalSurcharges\": \"1.23\",\"totalTaxes\": \"1.23\",\"totalFreightDiscounts\": \"1.23\",\"totalNetFreight\": \"1.23\",\"totalNetFedExCharge\": \"1.23\",\"totalDutiesAndTaxes\": \"1.23\",\"totalAncillaryFeesAndTaxes\": \"1.23\",\"totalDutiesTaxesAndFees\": \"1.23\",\"totalNetChargeWithDutiesAndTaxes\": \"1.23\",\"surcharges\": [{\"surchargeType\": \"INSURED_VALUE\",\"description\": \"Declared value\",\"amount\": \"1.23\"}]}},\"paymentDetails\": {\"paymentType\": \"CASH\",\"paymentSource\": \"SPOS\",\"paymentAmount\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"responsibleParty\": \"SENDER\",\"payor\": {\"accountType\": \"FEDEX_EXPRESS\",\"address\": {\"streetLines\": [\"29 sector\",\"noida\",\"UP\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"accountNumber\": \"1110093\",\"creditCard\": {\"maskedcreditCard\": \"string\",\"type\": \"VISA\",\"expirationDate\": \"string\"}}},\"labelSpecification\": {\"processingOptionsRequested\": \"string\",\"disposition\": [\"string\"],\"formatType\": \"OPERATIONAL_LABEL\",\"imageType\": \"PNG\",\"stockType\": \"STOCK_4X6\",\"printingOrientation\": \"TOP_EDGE_OF_TEXT_FIRST\",\"rotation\": \"string\",\"labelOrder\": \"string\",\"localization\": \"string\",\"labelOrigin\": \"string\",\"customerSpecifiedDetail\": \"string\"}}}";
        openShipment.setShipmentData(shipmentData);
        LocationDataDetail locationDetail = new LocationDataDetail("1232341", "9876543",
                new Location("DFWD", getAddress()), true, true, new Location("DFWD", getAddress()), "FXE", "DFWD");
        ShipmentDetail shipmentDetail = mapper.buildShipmentDetails(openShipment, createShipmentConfirmationRequest(),
                locationDetail, createReply(), buildConfirmedShipmentData());
        assertThat(shipmentDetail, notNullValue());
        assertThat(shipmentDetail.getRatingType(), equalTo("SR"));
        assertThat(shipmentDetail.isAfterPickupFlag(), equalTo(true));
        assertThat(shipmentDetail.getShipmentContactDetail()
                .getCustomerAddress3Text(), equalTo("DELHI"));
        assertThat(shipmentDetail.getShipmentContactDetail()
                .getSenderAddress3Text(), equalTo("UP"));

    }

    /**
     * Test build Accept package request
     */
    @Test
    public void testbuildPESRequest() {

        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        CompleteShipmentDetail completeShipmentDetail = createCompleteShipmentDetail();
        OpenShipment openShipment = buildOpenShipment();
        ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(createShipmentDetail());
        LocationDataDetail locationDetail = getLocationDetail();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        PackagePossessionEventDetailRequest request = mapper.buildPESRequest(shipmentConfirmationRequest, openShipment,
                confirmedShipmentData, completeShipmentDetail, locationDetail, openShipmentData);
        assertThat(request, notNullValue());
        assertThat(request.getPackageLineItems(), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0)
                .getRoutingId(), equalTo("40PNXA"));
        assertThat(request.getPackageLineItems()
                .get(0)
                .getTrackingNumber(), equalTo("798456321546"));

    }

    /**
     * Test build Accept package request when Operational Detail is null
     */
    @Test
    public void testbuildPESRequestWithNullOperationalDetail() {

        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        CompleteShipmentDetail completeShipmentDetail = createCompleteShipmentDetail();
        completeShipmentDetail.setOperationalDetail(null);
        OpenShipment openShipment = buildOpenShipment();
        ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(createShipmentDetail());
        LocationDataDetail locationDetail = getLocationDetail();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        PackagePossessionEventDetailRequest request = mapper.buildPESRequest(shipmentConfirmationRequest, openShipment,
                confirmedShipmentData, completeShipmentDetail, locationDetail, openShipmentData);
        assertThat(request, notNullValue());
        assertThat(request.getPackageLineItems(), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0)
                .getRoutingId(), nullValue());
        assertThat(request.getPackageLineItems()
                .get(0)
                .getTrackingNumber(), equalTo("798456321546"));

    }

    /**
     * Test build Accept package request when UrsaPrefix is missing
     */
    @Test
    public void testbuildPESRequestWithMissingUrsaPrefix() {

        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        CompleteShipmentDetail completeShipmentDetail = createCompleteShipmentDetail();
        completeShipmentDetail.getOperationalDetail()
                .setUrsaPrefixCode(null);
        OpenShipment openShipment = buildOpenShipment();
        ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(createShipmentDetail());
        LocationDataDetail locationDetail = getLocationDetail();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        PackagePossessionEventDetailRequest request = mapper.buildPESRequest(shipmentConfirmationRequest, openShipment,
                confirmedShipmentData, completeShipmentDetail, locationDetail, openShipmentData);
        assertThat(request, notNullValue());
        assertThat(request.getPackageLineItems(), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0)
                .getRoutingId(), nullValue());
        assertThat(request.getPackageLineItems()
                .get(0)
                .getTrackingNumber(), equalTo("798456321546"));

    }

    /**
     * Test build Accept package request when UrsaSuffix is missing
     */
    @Test
    public void testbuildPESRequestWithMissingUrsaSuffix() {

        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        CompleteShipmentDetail completeShipmentDetail = createCompleteShipmentDetail();
        completeShipmentDetail.getOperationalDetail()
                .setUrsaSuffixCode(null);
        OpenShipment openShipment = buildOpenShipment();
        ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(createShipmentDetail());
        LocationDataDetail locationDetail = getLocationDetail();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        PackagePossessionEventDetailRequest request = mapper.buildPESRequest(shipmentConfirmationRequest, openShipment,
                confirmedShipmentData, completeShipmentDetail, locationDetail, openShipmentData);
        assertThat(request, notNullValue());
        assertThat(request.getPackageLineItems(), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0)
                .getRoutingId(), nullValue());
        assertThat(request.getPackageLineItems()
                .get(0)
                .getTrackingNumber(), equalTo("798456321546"));

    }
    
    /**
     * Test build Accept package request when openShipmentData is null
     */
    @Test
    public void testbuildPESRequestOpenShipmentDataNull() {

        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        CompleteShipmentDetail completeShipmentDetail = createCompleteShipmentDetail();
        OpenShipment openShipment = buildOpenShipment();
        ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(createShipmentDetail());
        LocationDataDetail locationDetail = getLocationDetail();
        PackagePossessionEventDetailRequest request = mapper.buildPESRequest(shipmentConfirmationRequest, openShipment,
                confirmedShipmentData, completeShipmentDetail, locationDetail, null);
        assertThat(request, notNullValue());
        assertThat(request.getPackageLineItems(), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0)
                .getRoutingId(), equalTo("40PNXA"));
        assertThat(request.getPackageLineItems()
                .get(0)
                .getTrackingNumber(), equalTo("798456321546"));

    }
    
    /**
     * Test build Accept package request when openShipment is null
     */
    @Test
    public void testbuildPESRequestOpenShipmentNull() {

        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        CompleteShipmentDetail completeShipmentDetail = createCompleteShipmentDetail();
        OpenShipment openShipment = buildOpenShipment();
        ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(createShipmentDetail());
        LocationDataDetail locationDetail = getLocationDetail();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        openShipmentData.setOpenShipment(null);
        PackagePossessionEventDetailRequest request = mapper.buildPESRequest(shipmentConfirmationRequest, openShipment,
                confirmedShipmentData, completeShipmentDetail, locationDetail, openShipmentData);
        assertThat(request, notNullValue());
        assertThat(request.getPackageLineItems(), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0)
                .getRoutingId(), equalTo("40PNXA"));
        assertThat(request.getPackageLineItems()
                .get(0)
                .getTrackingNumber(), equalTo("798456321546"));

    }
    
    /**
     * Test build Accept package request when specialServicesRequested is null
     */
    @Test
    public void testbuildPESRequestSpecialServicesRequestedNull() {

        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        CompleteShipmentDetail completeShipmentDetail = createCompleteShipmentDetail();
        OpenShipment openShipment = buildOpenShipment();
        ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(createShipmentDetail());
        LocationDataDetail locationDetail = getLocationDetail();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        openShipmentData.getOpenShipment().setSpecialServicesRequested(null);
        PackagePossessionEventDetailRequest request = mapper.buildPESRequest(shipmentConfirmationRequest, openShipment,
                confirmedShipmentData, completeShipmentDetail, locationDetail, openShipmentData);
        assertThat(request, notNullValue());
        assertThat(request.getPackageLineItems(), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0)
                .getRoutingId(), equalTo("40PNXA"));
        assertThat(request.getPackageLineItems()
                .get(0)
                .getTrackingNumber(), equalTo("798456321546"));

    }
    
    /**
     * Test build Accept package request when specialServicesRequestedType is null
     */
    @Test
    public void testbuildPESRequestSpecialServicesRequestedTypeNull() {

        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        CompleteShipmentDetail completeShipmentDetail = createCompleteShipmentDetail();
        OpenShipment openShipment = buildOpenShipment();
        ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(createShipmentDetail());
        LocationDataDetail locationDetail = getLocationDetail();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        openShipmentData.getOpenShipment().getSpecialServicesRequested().setSpecialServicesRequestedType(null);
        PackagePossessionEventDetailRequest request = mapper.buildPESRequest(shipmentConfirmationRequest, openShipment,
                confirmedShipmentData, completeShipmentDetail, locationDetail, openShipmentData);
        assertThat(request, notNullValue());
        assertThat(request.getPackageLineItems(), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0), notNullValue());
        assertThat(request.getPackageLineItems()
                .get(0)
                .getRoutingId(), equalTo("40PNXA"));
        assertThat(request.getPackageLineItems()
                .get(0)
                .getTrackingNumber(), equalTo("798456321546"));

    }
    
    /**
     * Test build PES Cancel request
     */
    @Test
    public void testbuildPESCancelRequest() {

        ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(createShipmentDetail());
        LocationDataDetail locationDetail = getLocationDetail();
        confirmedShipmentData.setMasterTrackingId(new TrackingSequenceDetail("123", "1234455", 1));
        PackageCancellationEventDetailRequest request = mapper.buildPESCancelRequest("2020-11-04", confirmedShipmentData, createWorkstationDetails(), locationDetail, new PersonDetail("Mr.","Customer"));
        assertThat(request, notNullValue());
        assertThat(request.getHeaderDetails(), notNullValue());
        assertThat(request.getHeaderDetails().getWorkstationDetails(), notNullValue());
        
    }
    
    /**
     * Test build PES Cancel request with empty master trackingId
     */
    @Test
    public void testbuildPESCancelRequestWithEmptyMasterTrackingId() {

        ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(createShipmentDetail());
        LocationDataDetail locationDetail = getLocationDetail();
        confirmedShipmentData.setMasterTrackingId(null);
        PackageCancellationEventDetailRequest request = mapper.buildPESCancelRequest("2020-11-04", confirmedShipmentData, createWorkstationDetails(), locationDetail, new PersonDetail("Mr.","Customer"));
        assertThat(request, notNullValue());
        assertThat(request.getHeaderDetails(), notNullValue());
        assertThat(request.getHeaderDetails().getWorkstationDetails(), notNullValue());
        
    }
    
    /**
     * Test build UpdateOpenShipmentStatusData
     */
    @Test
    public void testbuildUpdateOpenShipmentStatusData() {

        UpdateOpenShipmentStatusData updateOpenShipmentStatusData =
                mapper.buildUpdateOpenShipmentStatusData("12345", "LASA", "RETURNED", "12345", "12345");
        assertThat(updateOpenShipmentStatusData, notNullValue());

    }

    /**
     * Test build Accept package request with CompleteShipmentDetail Empty Master Tracking Number.
     */
    @Test
    public void testBuildAcceptPackageRequestWithEmptyMasterTrackingId() {

        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        OpenShipment openShipment = buildOpenShipment();
        ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(createShipmentDetail());
        confirmedShipmentData.setMasterTrackingId(new TrackingSequenceDetail("123", "1234455", 1));
        CompleteShipmentDetail completeShipmentDetail = createCompleteShipmentDetail();
        completeShipmentDetail.setMasterTrackingId(null);
        LocationDataDetail locationDetail = getLocationDetail();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        PackagePossessionEventDetailRequest request = mapper.buildPESRequest(shipmentConfirmationRequest, openShipment,
                confirmedShipmentData, completeShipmentDetail, locationDetail, openShipmentData);
        assertThat(request.getPackageLineItems(), notNullValue());

    }

    /**
     * Test build Accept package request with Empty complete ShipmentDetail
     */
    @Test
    public void testBuildAcceptPackageRequestWithEmptycompleteShipmentDetail() {

        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        CompleteShipmentDetail completeShipmentDetail = new CompleteShipmentDetail();
        completeShipmentDetail.setCompletedPackageDetails(null);
        OpenShipment openShipment = buildOpenShipment();
        ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(createShipmentDetail());
        LocationDataDetail locationDetail = getLocationDetail();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        PackagePossessionEventDetailRequest request = mapper.buildPESRequest(shipmentConfirmationRequest, openShipment,
                confirmedShipmentData, completeShipmentDetail, locationDetail, openShipmentData);
        assertThat(request.getPackageLineItems()
                .get(0)
                .getBarcode(), nullValue());

    }

    /**
     * Test build Accept package request with FDXG carrier code.
     */
    @Test
    public void testBuildAcceptPackageRequestWithFDXGCarrierCode() {

        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        CompleteShipmentDetail completeShipmentDetail = createCompleteShipmentDetail();
        completeShipmentDetail.setCarrierCode("FDXG");
        OpenShipment openShipment = buildOpenShipment();
        ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(createShipmentDetail());
        LocationDataDetail locationDetail = getLocationDetail();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        PackagePossessionEventDetailRequest request = mapper.buildPESRequest(shipmentConfirmationRequest, openShipment,
                confirmedShipmentData, completeShipmentDetail, locationDetail, openShipmentData);
        assertThat(request.getPackageLineItems()
                .get(0)
                .getOperatingCompany(), notNullValue());

    }

    /**
     * Test build Accept package request with Empty paymentDetails
     */
    @Test
    public void testBuildAcceptPackageRequestWithEmptyPaymentDetails() {

        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        CompleteShipmentDetail completeShipmentDetail = createCompleteShipmentDetail();
        OpenShipment openShipment = buildOpenShipment();
        ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(createShipmentDetail());
        LocationDataDetail locationDetail = getLocationDetail();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        PackagePossessionEventDetailRequest request = mapper.buildPESRequest(shipmentConfirmationRequest, openShipment,
                confirmedShipmentData, completeShipmentDetail, locationDetail, openShipmentData);
        assertThat(request.getPaymentDetails()
                .getPaymentSource(), nullValue());

    }

    /**
     * Test build Accept package request with Empty interlineDetails
     */
    @Test
    public void testBuildAcceptPackageRequestWithEmptyInterLineDetails() {

        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        CompleteShipmentDetail completeShipmentDetail = createCompleteShipmentDetail();
        OpenShipment openShipment = buildOpenShipment();
        ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(createShipmentDetail());
        LocationDataDetail locationDetail = getLocationDetail();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        openShipmentData.getOpenShipment()
                .setInterlineShippingData(null);
        PackagePossessionEventDetailRequest request = mapper.buildPESRequest(shipmentConfirmationRequest, openShipment,
                confirmedShipmentData, completeShipmentDetail, locationDetail, openShipmentData);
        assertThat(request.getPaymentDetails()
                .getPaymentSource(), nullValue());

    }

    /**
     * Prepare address object.
     * 
     * @return
     */
    public AddressDetail getAddress() {

        AddressDetail address = new AddressDetail(new ArrayList<String>(), "Dallas", "TX", "75024", "US", true);
        return address;
    }

    /**
     * Method to create OpenShipment
     * 
     * @return
     */
    private OpenShipment buildOpenShipment() {

        OpenShipment openShipment = new OpenShipment();
        // Instance of ShipmentData as JSON
        String shipmentData =
                "{\"openShipment\": {\"locationId\": \"TRLA\",\"shipmentIndex\": \"123456789\",\"carrierDetails\": {\"carrierCode\": \"FXO\",\"opco\": \"EXPRESS\"},\"serviceType\": \"PRIORITY_OVERNIGHT\",\"packagingType\": \"YOUR_PACKAGING\",\"physicalPackagingType\":\"ABC\",\"packageCount\": 0,\"recipient\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"specialServicesRequested\": {\"specialServices\": [{\"specialServiceType\": \"BATTERY\",\"specialServiceSubType\": \"BATTERY\",\"displayText\": \"ASR\",\"description\": \"Adult signature\"}],\"holdAtLocationDetail\": {\"locationId\": \"DREK\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"}}},\"totalInsuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"totalWeight\": {\"units\": \"LB\",\"value\": \"3\",\"measurementType\": \"MANUAL\"},\"packageLineItems\": [{\"dimensions\": {\"length\": \"1\",\"width\": \"1\",\"height\": \"1\",\"units\": \"IN\"},\"dryIceWeightDetails\": {\"unit\": \"KG\",\"value\": \"12\",\"measurementType\":\"Manual\"},\"insuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"specialHandlingDetail\": \"CUSTOMER\",\"specialServicesDetails\": [{\"specialServiceType\": \"BATTERY\",\"specialServiceSubType\": \"\",\"displayText\": \"ASR\",\"description\": \"Adult signature required\"}],\"weight\": {\"units\": \"LB\",\"value\": \"2\",\"measurementType\": \"MANUAL\"},\"signatureOption\": \"ADULT\",\"sequenceNumber\":\"2\",\"trackingId\": {\"trackingNumber\": \"123456789\",\"barcode\": \"11111111111111111111111111111111\",\"sequence\": 1},\"master\": true}],\"shipper\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"customer\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"rateDetails\": {\"ratingType\": \"ONE_RATE\",\"serviceDetails\": {\"serviceType\": \"FIRST_OVERNIGHT\",\"serviceName\": \"FedEx First Overnight\",\"description\": \"First Overnight\"},\"deliveryDayOfWeek\": \"FRI\",\"hal\": false,\"commitDetails\": {\"commitTimestamp\": \"2020-08-21T23:59:00\",\"dayOfWeek\": \"FRI\"},\"rateDetails\": {\"currency\": \"USD\",\"ratingType\": \"ONE_RATE\",\"totalBaseCharge\": \"1.23\",\"totalRebates\": \"1.23\",\"totalNetCharges\": \"1.23\",\"totalSurcharges\": \"1.23\",\"totalTaxes\": \"1.23\",\"totalFreightDiscounts\": \"1.23\",\"totalNetFreight\": \"1.23\",\"totalNetFedExCharge\": \"1.23\",\"totalDutiesAndTaxes\": \"1.23\",\"totalAncillaryFeesAndTaxes\": \"1.23\",\"totalDutiesTaxesAndFees\": \"1.23\",\"totalNetChargeWithDutiesAndTaxes\": \"1.23\",\"surcharges\": [{\"surchargeType\": \"INSURED_VALUE\",\"description\": \"Declared value\",\"amount\": \"1.23\"}]}},\"paymentDetails\": {\"paymentType\": \"CASH\",\"paymentSource\": \"SPOS\",\"paymentAmount\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"responsibleParty\": \"SENDER\",\"payor\": {\"accountType\": \"FEDEX_EXPRESS\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"accountNumber\": \"1110093\",\"creditCard\": {\"maskedcreditCard\": \"string\",\"type\": \"VISA\",\"expirationDate\": \"string\"}}},\"labelSpecification\": {\"processingOptionsRequested\": \"string\",\"disposition\": [\"string\"],\"formatType\": \"OPERATIONAL_LABEL\",\"imageType\": \"PNG\",\"stockType\": \"STOCK_4X6\",\"printingOrientation\": \"TOP_EDGE_OF_TEXT_FIRST\",\"rotation\": \"string\",\"labelOrder\": \"string\",\"localization\": \"string\",\"labelOrigin\": \"string\",\"customerSpecifiedDetail\": \"string\"}}}";
        openShipment.setShipmentData(shipmentData);
        return openShipment;

    }

    /**
     * Method to prepare the reply of CompleteShipmentDetail
     *
     * @return
     */
    private CompleteShipmentDetail createReply() {

        CompleteShipmentDetail completeShipmentDetail = new CompleteShipmentDetail();
        TrackingIdDetail masterTrackingId = new TrackingIdDetail();
        List<CompletedPackageData> completedPackageDetails = new ArrayList<>();
        CompletedPackageData completedPackageData=new CompletedPackageData();
        ProductDescription packageDescription = new ProductDescription();
        PackageOperationData operationalDetail =new PackageOperationData();
        List<StringBarcodeDetail> stringBarcodes =new ArrayList<>();
        StringBarcodeDetail StringBarcodeDetail=new StringBarcodeDetail("FEDEX_1D","11111111111111111111");
        stringBarcodes.add(StringBarcodeDetail);
        PackageBarcodeDetail packageBarcodeDetail=new PackageBarcodeDetail(null,stringBarcodes);
        operationalDetail.setBarcode(packageBarcodeDetail);
        completedPackageData.setOperationalDetail(operationalDetail);
        completedPackageDetails.add(completedPackageData);
        
        
        masterTrackingId.setTrackingIdType("794978662786");
        List<TrackingIdDetail> trackingIds = new ArrayList<>();
        trackingIds.add(masterTrackingId);
        packageDescription.setCode("FEDEX_PAK");
        completedPackageDetails
                .add(createCompletePackageDetails(1, trackingIds, 0, "SERVICE_DEFAULT", new PackageOperationData()));
        completeShipmentDetail.setCompletedPackageDetails(completedPackageDetails);
        completeShipmentDetail.setMasterTrackingId(masterTrackingId);
        completeShipmentDetail.setUsDomestic(true);
        completeShipmentDetail.setIndex("12345");
        completeShipmentDetail.setCarrierCode(CarrierCodeType.FDXG.name());
        completeShipmentDetail.setPackagingDescription(packageDescription);

        return completeShipmentDetail;
    }

    /**
     * Create CompletedPackageData object
     *
     * @param sequenceNumber
     * @param trackingIds
     * @param groupNumber
     * @param signatureOption
     * @param operationalDetail
     * @return
     */
    private CompletedPackageData createCompletePackageDetails(
            Integer sequenceNumber,
            List<TrackingIdDetail> trackingIds,
            Integer groupNumber,
            String signatureOption,
            PackageOperationData operationalDetail) {

        CompletedPackageData completedPackageData = new CompletedPackageData();
        completedPackageData.setGroupNumber(groupNumber);
        completedPackageData.setSequenceNumber(sequenceNumber);
        completedPackageData.setTrackingIds(trackingIds);
        completedPackageData.setSignatureOption(signatureOption);
        completedPackageData.setOperationalDetail(operationalDetail);

        return completedPackageData;
    }

    /**
     * Create CompleteShipmentDetail
     * 
     * @return
     */
    private CompleteShipmentDetail createCompleteShipmentDetail() {

        CompleteShipmentDetail details = new CompleteShipmentDetail();
        details.setCarrierCode("FDXE");
        details.setMasterTrackingId(new TrackingIdDetail("FDXE", "0200", null, "798456321546"));
        details.setShipmentDocuments(Arrays.asList(new ShippingDocument()));
        details.setCompletedPackageDetails(createPackageDetails());
        ShipmentOperationData operationalDetail = new ShipmentOperationData();
        operationalDetail.setUrsaPrefixCode("40");
        operationalDetail.setUrsaSuffixCode("PNXA");
        details.setOperationalDetail(operationalDetail);

        return details;
    }

    /**
     * create CompletedPackageData
     * 
     * @return
     */
    private List<CompletedPackageData> createPackageDetails() {

        List<CompletedPackageData> details = new ArrayList<>();
        CompletedPackageData detail = new CompletedPackageData();
        detail.setTrackingId(new TrackingSequenceDetail());
        detail.setTrackingIds(Arrays.asList(new TrackingIdDetail("FDXE", "0200", null, "798456321546")));
        PackageOperationData operationData = new PackageOperationData();
        operationData.setBarcode(new PackageBarcodeDetail(null,
                Arrays.asList((new StringBarcodeDetail("1_D", "9622085140004341917100794824741560")))));
        detail.setOperationalDetail(operationData);
        details.add(detail);

        return details;
    }

    /**
     * Method to create object of ShipmentConfirmationRequest
     * 
     * @return
     */
    private ShipmentConfirmationRequest createShipmentConfirmationRequest() {

        ShipmentConfirmationRequest request = new ShipmentConfirmationRequest();
        request.setWorkstationDetail(createWorkstationDetail());
        request.setReferenceId("3456");
        request.setTrackingId("7898981234");
        request.setTransactionLocationId("LASA");
        request.setShipmentTimeStamp("2020-10-01T10:59:10.273Z");
        request.setPaymentDetail(new PaymentDetail());
        request.getPaymentDetail().setPaymentType("CASH");
        request.getPaymentDetail().setAmount(new Price("USD", 10.00));
        return request;
    }

    /**
     * Method to prepare the WorkstationDetail request
     * 
     * @return
     */
    private WorkstationDetail createWorkstationDetail() {

        WorkstationDetail workstationDetail = new WorkstationDetail();
        workstationDetail.setAccountNumber("038000004");
        workstationDetail.setMeterNumber("6997006");
        workstationDetail.setSoftwareId("SSFE");
        workstationDetail.setAppName("FUSE");
        workstationDetail.setAppVersionId("2.0.2");
        return workstationDetail;
    }
    
    /**
     * Method to prepare the WorkstationDetail request
     * 
     * @return
     */
    private WorkstationDetail createWorkstationDetails() {

        WorkstationDetail workstationDetail = new WorkstationDetail();
        workstationDetail.setAccountNumber("038000004");
        workstationDetail.setMeterNumber("6997006");
        workstationDetail.setSoftwareId("SSFE");
        workstationDetail.setAppName("FUSE");
        workstationDetail.setAppVersionId("2.0.2");
        workstationDetail.setGuid("12345");
        workstationDetail.setGroundAccountNumber("123456789");
        workstationDetail.setTeamMemberId("12345");
        workstationDetail.setDeviceId("12345");
        
        return workstationDetail;
    }

    /**
     * Method to create object of ShipmentDetail
     * 
     * @return
     */
    private ShipmentDetail createShipmentDetail() {

        ShipmentDetail shipmentDetail = new ShipmentDetail();
        shipmentDetail.setAcceptTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentDetail.setAfterPickupFlag(false);
        shipmentDetail.setApiClientId("1234");
        shipmentDetail.setApiClientTypeName("apiClientType");
        shipmentDetail.setBillToAccountType("bill");
        shipmentDetail.setCityCenterAccountNumber("1234556");
        shipmentDetail.setChargeAmount(1.23);
        shipmentDetail.setCurrency("USD");
        shipmentDetail.setCurrentShipmentStatTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentDetail.setCurrentShipmentStatus("CREATED");
        shipmentDetail.setCustomerAccountNumber("1234556");
        shipmentDetail.setDelCommitDate("2020-07-30T11:55:28.687+05:30");
        shipmentDetail.setDeviceId("SSFE");
        shipmentDetail.setDevicePlatformType("devicePlatformType");
        shipmentDetail.setDimensionUom("IN");
        shipmentDetail.setDiscountAmount(1.23);
        shipmentDetail.setInitialAcceptanceTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentDetail.setPaymentType("ACCOUNT");
        shipmentDetail.setTaxAmount(1.23);
        shipmentDetail.setSoftwareId("SSFE");
        shipmentDetail.setWeightUom("LB");
        shipmentDetail.setShipmentTrackingNumber("12345");
        shipmentDetail.setMeterNumber("033600003");
        shipmentDetail.setOpCoCode("FDXE");
        shipmentDetail.setOpCoLocationId("DREK");
        shipmentDetail.setShipmentContactDetail(createShipmentContactDetail());
        shipmentDetail.setShipmentPackageDetails(createShipmentPackageDetails());
        shipmentDetail.setOpcoShippingServiceName(OperatingCompany.FEDEX_EXPRESS.name());
        return shipmentDetail;
    }

    /**
     * Method to create shipment package details
     */
    private List<ShipmentPackageDetail> createShipmentPackageDetails() {

        List<ShipmentPackageDetail> shipmentPackageDetailsList = new ArrayList<>();
        ShipmentPackageDetail shipmentPackageDetail = new ShipmentPackageDetail();
        shipmentPackageDetail.setAcceptTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentPackageDetail.setAmount(1.2);
        shipmentPackageDetail.setBarcode("12345678");
        shipmentPackageDetail.setTrackingNumber("123456");
        shipmentPackageDetail.setShipmentPackageAdditionalServices(createShipmentPackageAdditionalServices());
        shipmentPackageDetailsList.add(shipmentPackageDetail);
        return shipmentPackageDetailsList;
    }

    /**
     * Method to create shipment package additional services
     */
    private List<ShipmentPackageAdditionalService> createShipmentPackageAdditionalServices() {

        List<ShipmentPackageAdditionalService> shipmentPackageAdditionalServiceList = new ArrayList<>();
        ShipmentPackageAdditionalService shipmentPackageAdditionalService = new ShipmentPackageAdditionalService();
        shipmentPackageAdditionalService.setAcceptTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentPackageAdditionalServiceList.add(shipmentPackageAdditionalService);
        return shipmentPackageAdditionalServiceList;
    }

    /**
     * Method to create object of ShipmentContactDetail
     * 
     * @return
     */
    private ShipmentContactDetail createShipmentContactDetail() {

        ShipmentContactDetail shipmentContactDetail = new ShipmentContactDetail();
        shipmentContactDetail.setAcceptTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentContactDetail.setCustomerName("customer");
        shipmentContactDetail.setSenderName("sender");
        shipmentContactDetail.setRecipientName("recipient");

        return shipmentContactDetail;
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link ConfirmShipmentTransactionServiceMapper#createRequestedShipmentDetail(OpenShipment)} to
     * set the object fields and return a valid response object with all mandatory fields covered.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailSuccess() {

        ShipmentConfirmationRequest confirmShipmentRequested = createShipmentConfirmationRequest();
        OpenShipment openShipment = buildOpenShipment();

        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(confirmShipmentRequested, openShipment);

        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getIndex(), notNullValue());
        assertThat(requestedShipmentDetail.getLabelSpecification(), notNullValue());
        assertThat(requestedShipmentDetail.getOrigin(), notNullValue());
        assertThat(requestedShipmentDetail.getOrigin()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getOrigin()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getRecipient(), notNullValue());
        assertThat(requestedShipmentDetail.getRecipient()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getRecipient()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getSender(), notNullValue());
        assertThat(requestedShipmentDetail.getSender()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getSender()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getCustomer(), notNullValue());
        assertThat(requestedShipmentDetail.getCustomer()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getCustomer()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getDimensions(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getInsuredValue(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getDryIceWeightDetails(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getDescription(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getDisplayText(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceSubType(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceSubType(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link ConfirmShipmentTransactionServiceMapper#createRequestedShipmentDetail(OpenShipment)}
     * to set the object fields and return a valid response object with all mandatory fields
     * covered.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailWithCreditCard() {

        ShipmentConfirmationRequest confirmShipmentRequested = createShipmentConfirmationRequest();
        PartyDetail payor = createPayorRequest();
        confirmShipmentRequested.getPaymentDetail()
                .setPayor(payor);
        OpenShipment openShipment = buildOpenShipment();

        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(confirmShipmentRequested, openShipment);

        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getIndex(), notNullValue());
        assertThat(requestedShipmentDetail.getLabelSpecification(), notNullValue());
        assertThat(requestedShipmentDetail.getOrigin(), notNullValue());
        assertThat(requestedShipmentDetail.getOrigin()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getOrigin()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getRecipient(), notNullValue());
        assertThat(requestedShipmentDetail.getRecipient()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getRecipient()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getSender(), notNullValue());
        assertThat(requestedShipmentDetail.getSender()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getSender()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getCustomer(), notNullValue());
        assertThat(requestedShipmentDetail.getCustomer()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getCustomer()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getDimensions(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getInsuredValue(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getDryIceWeightDetails(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getDescription(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getDisplayText(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceSubType(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceSubType(), notNullValue());
        assertThat(requestedShipmentDetail.getShippingChargesPayment()
                .getPayor()
                .getAccountNumber(), notNullValue());
        assertThat(requestedShipmentDetail.getShippingChargesPayment()
                .getPayor()
                .getCreditCard(), notNullValue());
        assertThat(requestedShipmentDetail.getShippingChargesPayment()
                .getPayor()
                .getCreditCard()
                .getType(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link ConfirmShipmentTransactionServiceMapper#createRequestedShipmentDetail(OpenShipment)}
     * to set the object fields and return a valid response object with all mandatory fields
     * covered.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailWithPayorNull() {

        ShipmentConfirmationRequest confirmShipmentRequested = createShipmentConfirmationRequest();
        confirmShipmentRequested.getPaymentDetail()
                .setPayor(null);
        OpenShipment openShipment = buildOpenShipment();

        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(confirmShipmentRequested, openShipment);

        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getIndex(), notNullValue());
        assertThat(requestedShipmentDetail.getLabelSpecification(), notNullValue());
        assertThat(requestedShipmentDetail.getOrigin(), notNullValue());
        assertThat(requestedShipmentDetail.getOrigin()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getOrigin()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getRecipient(), notNullValue());
        assertThat(requestedShipmentDetail.getRecipient()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getRecipient()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getSender(), notNullValue());
        assertThat(requestedShipmentDetail.getSender()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getSender()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getCustomer(), notNullValue());
        assertThat(requestedShipmentDetail.getCustomer()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getCustomer()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getDimensions(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getInsuredValue(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getDryIceWeightDetails(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getDescription(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getDisplayText(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceSubType(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceSubType(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link ConfirmShipmentTransactionServiceMapper#createRequestedShipmentDetail(OpenShipment)}
     * to set the object fields and return a valid response object with all mandatory fields
     * covered.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailWithCreditCardNull() {

        ShipmentConfirmationRequest confirmShipmentRequested = createShipmentConfirmationRequest();
        PartyDetail payor = createPayorRequest();
        payor.setCreditCard(null);
        confirmShipmentRequested.getPaymentDetail()
                .setPayor(payor);
        OpenShipment openShipment = buildOpenShipment();

        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(confirmShipmentRequested, openShipment);

        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getIndex(), notNullValue());
        assertThat(requestedShipmentDetail.getLabelSpecification(), notNullValue());
        assertThat(requestedShipmentDetail.getOrigin(), notNullValue());
        assertThat(requestedShipmentDetail.getOrigin()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getOrigin()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getRecipient(), notNullValue());
        assertThat(requestedShipmentDetail.getRecipient()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getRecipient()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getSender(), notNullValue());
        assertThat(requestedShipmentDetail.getSender()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getSender()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getCustomer(), notNullValue());
        assertThat(requestedShipmentDetail.getCustomer()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getCustomer()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getDimensions(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getInsuredValue(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getDryIceWeightDetails(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getDescription(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getDisplayText(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceSubType(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceSubType(), notNullValue());
        assertThat(requestedShipmentDetail.getShippingChargesPayment()
                .getPayor()
                .getAccountNumber(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link ConfirmShipmentTransactionServiceMapper#createRequestedShipmentDetail(OpenShipment)}
     * to set the object fields and return a valid response object with all mandatory fields
     * covered.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailWithExpiryNull() {

        ShipmentConfirmationRequest confirmShipmentRequested = createShipmentConfirmationRequest();
        PartyDetail payor = createPayorRequest();
        payor.getCreditCard()
                .setExpirationDate(null);
        confirmShipmentRequested.getPaymentDetail()
                .setPayor(payor);
        OpenShipment openShipment = buildOpenShipment();

        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(confirmShipmentRequested, openShipment);

        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getIndex(), notNullValue());
        assertThat(requestedShipmentDetail.getLabelSpecification(), notNullValue());
        assertThat(requestedShipmentDetail.getOrigin(), notNullValue());
        assertThat(requestedShipmentDetail.getOrigin()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getOrigin()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getRecipient(), notNullValue());
        assertThat(requestedShipmentDetail.getRecipient()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getRecipient()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getSender(), notNullValue());
        assertThat(requestedShipmentDetail.getSender()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getSender()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getCustomer(), notNullValue());
        assertThat(requestedShipmentDetail.getCustomer()
                .getAddress(), notNullValue());
        assertThat(requestedShipmentDetail.getCustomer()
                .getContact(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getDimensions(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getInsuredValue(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getDryIceWeightDetails(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getDescription(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getDisplayText(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceSubType(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceSubType(), notNullValue());
        assertThat(requestedShipmentDetail.getShippingChargesPayment()
                .getPayor()
                .getAccountNumber(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link ConfirmShipmentTransactionServiceMapper#createRequestedShipmentDetail(OpenShipment)} when
     * input OpenShipment is null then response comes as null.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailWdOpenShpmntNull() {

        ShipmentConfirmationRequest confirmShipmentRequested = createShipmentConfirmationRequest();
        OpenShipment openShipment = null;

        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(confirmShipmentRequested, openShipment);

        assertThat(requestedShipmentDetail, nullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link ConfirmShipmentTransactionServiceMapper#createRequestedShipmentDetail(OpenShipment)} when
     * input ShipmentData is null then response comes not as null and some fields get set.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailWdShpmntDataNull() {

        ShipmentConfirmationRequest confirmShipmentRequested = createShipmentConfirmationRequest();
        OpenShipment openShipment = buildOpenShipment();
        openShipment.setShipmentData(null);

        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(confirmShipmentRequested, openShipment);

        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getDropOffType(), notNullValue());
        assertThat(requestedShipmentDetail.getShipTimestamp(), notNullValue());
        assertThat(requestedShipmentDetail.getVariationOptions(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link ConfirmShipmentTransactionServiceMapper#createRequestedShipmentDetail(OpenShipment)} when
     * invalid ShipmentData data then response comes not as null and some fields get set..
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailWdInvalidShipmentData() {

        ShipmentConfirmationRequest confirmShipmentRequested = createShipmentConfirmationRequest();
        OpenShipment openShipment = buildOpenShipment();
        openShipment.setShipmentData("{\\\"openShipment\\\": {\\\"locationId\\\":");

        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(confirmShipmentRequested, openShipment);

        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getDropOffType(), notNullValue());
        assertThat(requestedShipmentDetail.getShipTimestamp(), notNullValue());
        assertThat(requestedShipmentDetail.getVariationOptions(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link ConfirmShipmentTransactionServiceMapper#updateLabelSpecification(confirmShipmentRequested, openShipment)}
     * when valid shipment data and and update the request with label specifications.
     * 
     */
    @Test
    public void testUpdateLabelSpecificationSuccess() {

        ShipmentConfirmationRequest confirmShipmentRequested = createShipmentConfirmationRequest();
        OpenShipmentData openShipmentData = createOpenShipmentData();

        mapper.updateLabelSpecification(confirmShipmentRequested, openShipmentData);
        
        assertThat(confirmShipmentRequested.getLabelSpecificationDetail(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link ConfirmShipmentTransactionServiceMapper#updateLabelSpecification(confirmShipmentRequested, openShipment)}
     * when valid shipment data and and update the request with label specifications.
     * 
     */
    @Test
    public void testUpdateLabelSpecificationWdLabelSpecification() {

        ShipmentConfirmationRequest confirmShipmentRequested = createShipmentConfirmationRequest();
        confirmShipmentRequested.setLabelSpecificationDetail(createLabelSpecificationDetail());
        OpenShipmentData openShipmentData = createOpenShipmentData();

        mapper.updateLabelSpecification(confirmShipmentRequested, openShipmentData);

        assertThat(confirmShipmentRequested.getLabelSpecificationDetail(), notNullValue());
    }

    /**
     * Method to create OpenShipmentData object
     * 
     * @return
     */
    private OpenShipmentData createOpenShipmentData() {

        OpenShipmentData openShpmtData = new OpenShipmentData();
        OpenShipmentDetail openShipmentDetail = new OpenShipmentDetail();
        openShipmentDetail.setLabelSpecification(createLabelSpecificationDetail());
        openShipmentDetail.setSpecialServicesRequested(createShipmentSpecialServiceDetail());
        openShipmentDetail.setInterlineShippingData(
                new InterlineShippingData("10038", "AA", "123456", "test", "9810090", "981001"));
        openShpmtData.setOpenShipment(openShipmentDetail);
        return openShpmtData;
    }

    /**
     * Method to create LabelSpecificationDetail object
     * 
     * @return
     */
    private LabelSpecificationDetail createLabelSpecificationDetail() {

        LabelSpecificationDetail labelSpecificationDetail = new LabelSpecificationDetail();

        labelSpecificationDetail.setProcessingOptionsRequested("IGNORE_RESIDENTIAL_DELIVERY_POLICIES");
        labelSpecificationDetail.setFormatType("OPERATIONAL_LABEL");
        labelSpecificationDetail.setStockType("STOCK_4X6");
        labelSpecificationDetail.setImageType("PNG");
        labelSpecificationDetail.setPrintingOrientation("TOP_EDGE_OF_TEXT_FIRST");
        labelSpecificationDetail.setRotation("NA");
        labelSpecificationDetail.setLabelOrigin("ALVA");
        labelSpecificationDetail.setCustomerSpecifiedDetail("BUSINESS");

        return labelSpecificationDetail;

    }

    /**
     * Create location data details
     * 
     * @return locationDataDetail
     */
    private LocationDataDetail getLocationDetail() {

        return new LocationDataDetail("1232341", "9876543", new Location("DFWD", getAddress()), false, false,
                new Location("DFWD", getAddress()), "FXE", "DFWD");
    }
    
    /**
     * 
     * 
     */
    private ConfirmedShipmentData buildConfirmedShipmentData() {

        ConfirmedShipmentData confirmedShipmentData = new ConfirmedShipmentData();
        TrackingSequenceDetail trackingSequenceDetail =
                new TrackingSequenceDetail("7898981234", "989192092340120902957898981234", 1);
        confirmedShipmentData.setMasterTrackingId(trackingSequenceDetail);
        return confirmedShipmentData;
    }
    
    /**
     * Prepare UpdateOpenShipmentStatusData object.
     * 
     * @return
     */
    public UpdateOpenShipmentStatusData getUpdateOpenShipmentStatusData()
    {
        UpdateOpenShipmentStatusData updateOpenShipmentStatusData = new UpdateOpenShipmentStatusData();
        updateOpenShipmentStatusData.setTrackingId("794973860000");
        updateOpenShipmentStatusData.setReferenceId("794973860000");
        updateOpenShipmentStatusData.setTeamMemberId("123456");
        updateOpenShipmentStatusData.setLocationId("LASA");
        updateOpenShipmentStatusData.setOpenShipmentStatusCode("RETURNED");
        
        return updateOpenShipmentStatusData;
    }
    
    /**
     * Test case to create ShipmentDetail when call is made with Fedex Express as a carrier code to
     * {@link ConfirmShipmentTransactionServiceMapper#buildShipmentDetails(ShipmentConfirmationRequest,OpenShipment,LocationDataDetail,ConfirmedShipmentData)}
     * to set the object fields and return a valid response ShipmentDetail object with all mandatory
     * fields covered.
     * 
     */
    @Test
    public void testBuildShipmentDetailWithFedexExpress() {

        OpenShipment openShipment = buildOpenShipment();
        LocationDataDetail locationDetail = new LocationDataDetail("1232341", "9876543",
                new Location("DFWD", getAddress()), false, false, new Location("DFWD", getAddress()), "FXE", "DFWD");
        CompleteShipmentDetail completeShipmentDetail = createReply();
        completeShipmentDetail.setCarrierCode(CarrierCodeType.FDXE.name());
        ShipmentDetail shipmentDetail = mapper.buildShipmentDetails(openShipment, createShipmentConfirmationRequest(),
                locationDetail, completeShipmentDetail, buildConfirmedShipmentData());
        assertThat(shipmentDetail, notNullValue());
        assertThat(shipmentDetail.getCityCenterAccountNumber(), equalTo(createWorkstationDetail().getAccountNumber()));
        assertThat(shipmentDetail.getShipmentContactDetail()
                .getCustomerCountryName(), equalTo("US"));
        assertThat(shipmentDetail.getOriginCountryCode(), equalTo(locationDetail.getTransactionLocationInfo()
                .getAddress()
                .getCountryCode()));
        assertThat(shipmentDetail.isStandardPackageFlag(), equalTo(false));
        assertThat(shipmentDetail.getApiVersionId(), equalTo(ServiceConstant.API_VERSION_ID));
        assertThat(shipmentDetail.getApiClientTypeName(),
                equalTo(createShipmentConfirmationRequest().getWorkstationDetail()
                        .getAppName()));
        assertThat(shipmentDetail.getOpcoShippingServiceName(), equalTo(OperatingCompany.FEDEX_EXPRESS.name()));
        assertThat(shipmentDetail.getShippingServiceName(), equalTo("FIRST_OVERNIGHT"));
        assertThat(shipmentDetail.getShippingServiceDesc(), equalTo("FedEx First Overnight"));

    }

    /**
     * Method to create payor request
     * 
     * @return PartyDetail
     */
    private PartyDetail createPayorRequest() {

        PartyDetail partyDetail = new PartyDetail();
        partyDetail.setAccountNumber("123456");
        partyDetail.setCreditCard(new CreditCardData("1343", "VISA", "12/20"));
        return partyDetail;
    }
    
    /**
     * Method to create ShipmentSpecialServiceDetail request
     * 
     * @return ShipmentSpecialServiceDetail
     */
    private ShipmentSpecialServiceDetail createShipmentSpecialServiceDetail() {

        ShipmentSpecialServiceDetail shipmentSpecialServiceDetail = new ShipmentSpecialServiceDetail();
        List<SpecialServiceDescriptionDetail> listSpecialServicesRequestedType = new ArrayList<>();
        SpecialServiceDescriptionDetail specialServiceDescriptionDetail = new SpecialServiceDescriptionDetail();
        specialServiceDescriptionDetail.setSpecialServiceType("FEDEX_ONE_RATE");
        listSpecialServicesRequestedType.add(specialServiceDescriptionDetail);
        shipmentSpecialServiceDetail.setSpecialServicesRequestedType(listSpecialServicesRequestedType);
        return shipmentSpecialServiceDetail;
    }

    /**
     * Test case to validate the scenario when call is made with interlineData to
     * {@link ConfirmShipmentTransactionServiceMapper#createRequestedShipmentDetail(ShipmentConfirmationRequest,OpenShipment)}
     * to set the object fields and return a valid response object with all mandatory fields
     * covered.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailSuccessWithInterline() {

        ShipmentConfirmationRequest confirmShipmentRequested = createShipmentConfirmationRequest();
        OpenShipment openShipment = buildOpenShipmentWithInterline();

        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(confirmShipmentRequested, openShipment);

        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getShippingChargesPayment()
                .getPayor()
                .getAccountNumber(), equalTo("164462765"));
    }

    /**
     * Test case to validate the scenario when call is made with Payor and interlineData null to
     * {@link ConfirmShipmentTransactionServiceMapper#createRequestedShipmentDetail(ShipmentConfirmationRequest,OpenShipment)}
     * to set the object fields and return a valid response object with all mandatory fields
     * covered.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailSuccessWithInterlineAndPayorNull() {

        ShipmentConfirmationRequest confirmShipmentRequested = createShipmentConfirmationRequest();
        OpenShipment openShipment = buildOpenShipmentWithPayorAndInterlineNull();

        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(confirmShipmentRequested, openShipment);

        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getShippingChargesPayment()
                .getPayor()
                .getAccountNumber(), notNullValue());
        assertThat(requestedShipmentDetail.getShippingChargesPayment()
                .getPayor()
                .getAccountNumber(), equalTo("038000004"));
    }

    /**
     * Method to create OpenShipment with Interline
     * 
     * @return
     */
    private OpenShipment buildOpenShipmentWithInterline() {

        OpenShipment openShipment = new OpenShipment();
        // Instance of ShipmentData as JSON
        String shipmentData =
                "{\"openShipment\": {\"locationId\": \"TRLA\",\"shipmentIndex\": \"123456789\",\"carrierDetails\": {\"carrierCode\": \"FXO\",\"opco\": \"EXPRESS\"},\"serviceType\": \"PRIORITY_OVERNIGHT\",\"packagingType\": \"YOUR_PACKAGING\",\"physicalPackagingType\":\"ABC\",\"packageCount\": 0,\"recipient\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"specialServicesRequested\": {\"specialServices\": [{\"specialServiceType\": \"BATTERY\",\"specialServiceSubType\": \"BATTERY\",\"displayText\": \"ASR\",\"description\": \"Adult signature\"}],\"holdAtLocationDetail\": {\"locationId\": \"DREK\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"}}},\"totalInsuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"totalWeight\": {\"units\": \"LB\",\"value\": \"3\",\"measurementType\": \"MANUAL\"},\"packageLineItems\": [{\"dimensions\": {\"length\": \"1\",\"width\": \"1\",\"height\": \"1\",\"units\": \"IN\"},\"dryIceWeightDetails\": {\"unit\": \"KG\",\"value\": \"12\",\"measurementType\":\"Manual\"},\"insuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"specialHandlingDetail\": \"CUSTOMER\",\"specialServicesDetails\": [{\"specialServiceType\": \"BATTERY\",\"specialServiceSubType\": \"\",\"displayText\": \"ASR\",\"description\": \"Adult signature required\"}],\"weight\": {\"units\": \"LB\",\"value\": \"2\",\"measurementType\": \"MANUAL\"},\"signatureOption\": \"ADULT\",\"sequenceNumber\":\"2\",\"trackingId\": {\"trackingNumber\": \"123456789\",\"barcode\": \"11111111111111111111111111111111\",\"sequence\": 1},\"master\": true}],\"shipper\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"customer\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"rateDetails\": {\"ratingType\": \"ONE_RATE\",\"serviceDetails\": {\"serviceType\": \"FIRST_OVERNIGHT\",\"serviceName\": \"FedEx First Overnight\",\"description\": \"First Overnight\"},\"deliveryDayOfWeek\": \"FRI\",\"hal\": false,\"commitDetails\": {\"commitTimestamp\": \"2020-08-21T23:59:00\",\"dayOfWeek\": \"FRI\"},\"rateDetails\": {\"currency\": \"USD\",\"ratingType\": \"ONE_RATE\",\"totalBaseCharge\": \"1.23\",\"totalRebates\": \"1.23\",\"totalNetCharges\": \"1.23\",\"totalSurcharges\": \"1.23\",\"totalTaxes\": \"1.23\",\"totalFreightDiscounts\": \"1.23\",\"totalNetFreight\": \"1.23\",\"totalNetFedExCharge\": \"1.23\",\"totalDutiesAndTaxes\": \"1.23\",\"totalAncillaryFeesAndTaxes\": \"1.23\",\"totalDutiesTaxesAndFees\": \"1.23\",\"totalNetChargeWithDutiesAndTaxes\": \"1.23\",\"surcharges\": [{\"surchargeType\": \"INSURED_VALUE\",\"description\": \"Declared value\",\"amount\": \"1.23\"}]}},\"paymentDetails\": {\"paymentType\": \"CASH\",\"paymentSource\": \"SPOS\",\"paymentAmount\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"responsibleParty\": \"SENDER\",\"payor\": {\"accountType\": \"FEDEX_EXPRESS\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"accountNumber\": \"164462765\",\"creditCard\": {\"maskedcreditCard\": \"string\",\"type\": \"VISA\",\"expirationDate\": \"string\"}}},\"labelSpecification\": {\"processingOptionsRequested\": \"string\",\"disposition\": [\"string\"],\"formatType\": \"OPERATIONAL_LABEL\",\"imageType\": \"PNG\",\"stockType\": \"STOCK_4X6\",\"printingOrientation\": \"TOP_EDGE_OF_TEXT_FIRST\",\"rotation\": \"string\",\"labelOrder\": \"string\",\"localization\": \"string\",\"labelOrigin\": \"string\",\"customerSpecifiedDetail\": \"string\"},\"interlineShippingData\": {\"interlineId\": \"100038\",\"employeeId\": \"3232\",\"interlineCode\": \"WO\",\"interlineNumber\": \"001\",\"accountNumber\": \"164462765\"}}}";
        openShipment.setShipmentData(shipmentData);
        return openShipment;

    }

    /**
     * Method to create OpenShipment with null Payor and Interline
     * 
     * @return
     */
    private OpenShipment buildOpenShipmentWithPayorAndInterlineNull() {

        OpenShipment openShipment = new OpenShipment();
        // Instance of ShipmentData as JSON
        String shipmentData =
                "{\"openShipment\": {\"locationId\": \"TRLA\",\"shipmentIndex\": \"123456789\",\"carrierDetails\": {\"carrierCode\": \"FXO\",\"opco\": \"EXPRESS\"},\"serviceType\": \"PRIORITY_OVERNIGHT\",\"packagingType\": \"YOUR_PACKAGING\",\"physicalPackagingType\":\"ABC\",\"packageCount\": 0,\"recipient\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"specialServicesRequested\": {\"specialServices\": [{\"specialServiceType\": \"BATTERY\",\"specialServiceSubType\": \"BATTERY\",\"displayText\": \"ASR\",\"description\": \"Adult signature\"}],\"holdAtLocationDetail\": {\"locationId\": \"DREK\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"}}},\"totalInsuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"totalWeight\": {\"units\": \"LB\",\"value\": \"3\",\"measurementType\": \"MANUAL\"},\"packageLineItems\": [{\"dimensions\": {\"length\": \"1\",\"width\": \"1\",\"height\": \"1\",\"units\": \"IN\"},\"dryIceWeightDetails\": {\"unit\": \"KG\",\"value\": \"12\",\"measurementType\":\"Manual\"},\"insuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"specialHandlingDetail\": \"CUSTOMER\",\"specialServicesDetails\": [{\"specialServiceType\": \"BATTERY\",\"specialServiceSubType\": \"\",\"displayText\": \"ASR\",\"description\": \"Adult signature required\"}],\"weight\": {\"units\": \"LB\",\"value\": \"2\",\"measurementType\": \"MANUAL\"},\"signatureOption\": \"ADULT\",\"sequenceNumber\":\"2\",\"trackingId\": {\"trackingNumber\": \"123456789\",\"barcode\": \"11111111111111111111111111111111\",\"sequence\": 1},\"master\": true}],\"shipper\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"customer\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"rateDetails\": {\"ratingType\": \"ONE_RATE\",\"serviceDetails\": {\"serviceType\": \"FIRST_OVERNIGHT\",\"serviceName\": \"FedEx First Overnight\",\"description\": \"First Overnight\"},\"deliveryDayOfWeek\": \"FRI\",\"hal\": false,\"commitDetails\": {\"commitTimestamp\": \"2020-08-21T23:59:00\",\"dayOfWeek\": \"FRI\"},\"rateDetails\": {\"currency\": \"USD\",\"ratingType\": \"ONE_RATE\",\"totalBaseCharge\": \"1.23\",\"totalRebates\": \"1.23\",\"totalNetCharges\": \"1.23\",\"totalSurcharges\": \"1.23\",\"totalTaxes\": \"1.23\",\"totalFreightDiscounts\": \"1.23\",\"totalNetFreight\": \"1.23\",\"totalNetFedExCharge\": \"1.23\",\"totalDutiesAndTaxes\": \"1.23\",\"totalAncillaryFeesAndTaxes\": \"1.23\",\"totalDutiesTaxesAndFees\": \"1.23\",\"totalNetChargeWithDutiesAndTaxes\": \"1.23\",\"surcharges\": [{\"surchargeType\": \"INSURED_VALUE\",\"description\": \"Declared value\",\"amount\": \"1.23\"}]}},\"paymentDetails\": {\"paymentType\": \"CASH\",\"paymentSource\": \"SPOS\",\"paymentAmount\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"responsibleParty\": \"SENDER\"},\"labelSpecification\": {\"processingOptionsRequested\": \"string\",\"disposition\": [\"string\"],\"formatType\": \"OPERATIONAL_LABEL\",\"imageType\": \"PNG\",\"stockType\": \"STOCK_4X6\",\"printingOrientation\": \"TOP_EDGE_OF_TEXT_FIRST\",\"rotation\": \"string\",\"labelOrder\": \"string\",\"localization\": \"string\",\"labelOrigin\": \"string\",\"customerSpecifiedDetail\": \"string\"}}}";
        openShipment.setShipmentData(shipmentData);
        return openShipment;

    }

}
