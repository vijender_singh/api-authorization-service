package com.fedex.rscs.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixException;
import com.netflix.hystrix.exception.HystrixRuntimeException;

import com.fedex.common.cxs.exception.TerminalDbAccessException;
import com.fedex.common.cxs.exception.TransientDbAccessException;
import com.fedex.rscs.client.cshp.ShipServiceClient;
import com.fedex.rscs.client.pes.PackageEventServiceClient;
import com.fedex.rscs.dto.CarrierCodeType;
import com.fedex.rscs.dto.CarrierType;
import com.fedex.rscs.dto.NotificationType;
import com.fedex.rscs.dto.PaymentType;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.ServiceProcessorTerminalException;
import com.fedex.rscs.exception.client.ClientTerminalException;
import com.fedex.rscs.exception.client.ClientTransientException;
import com.fedex.rscs.model.AddressDetail;
import com.fedex.rscs.model.DeleteSoldShipmentData;
import com.fedex.rscs.model.LabelSpecificationDetail;
import com.fedex.rscs.model.OpenShipment;
import com.fedex.rscs.model.OpenShipmentData;
import com.fedex.rscs.model.OpenShipmentDetail;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.PaymentDetail;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ServiceType;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.ShipmentContactDetail;
import com.fedex.rscs.model.ShipmentDetail;
import com.fedex.rscs.model.ShipmentPackageAdditionalService;
import com.fedex.rscs.model.ShipmentPackageDetail;
import com.fedex.rscs.model.ShipmentSpecialServiceDetail;
import com.fedex.rscs.model.SpecialServiceDescriptionDetail;
import com.fedex.rscs.model.VariationOptionDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.common.DeviceDetail;
import com.fedex.rscs.model.common.EventNotification;
import com.fedex.rscs.model.common.Location;
import com.fedex.rscs.model.common.LocationDataDetail;
import com.fedex.rscs.model.common.PackageCancellationEventDetailRequest;
import com.fedex.rscs.model.common.PackageLineItemDetail;
import com.fedex.rscs.model.common.PackagePossessionEventDetailRequest;
import com.fedex.rscs.model.common.PersonDetail;
import com.fedex.rscs.model.common.TransactionHeaderDetail;
import com.fedex.rscs.model.cshp.CompleteShipmentDetail;
import com.fedex.rscs.model.cshp.CompletedPackageData;
import com.fedex.rscs.model.cshp.ConfirmedPackageData;
import com.fedex.rscs.model.cshp.ConfirmedShipmentData;
import com.fedex.rscs.model.cshp.CreditCardData;
import com.fedex.rscs.model.cshp.PackageOperationData;
import com.fedex.rscs.model.cshp.Price;
import com.fedex.rscs.model.cshp.TrackingIdDetail;
import com.fedex.rscs.model.cshp.TrackingSequenceDetail;
import com.fedex.rscs.model.cshp.TransactionStatus;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;
import com.fedex.rscs.repository.ConfirmShipmentRepository;
import com.fedex.rscs.repository.OpenShipmentRepository;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Class for code coverage of ConfirmShipmentTransactionServiceImplTest class
 * 
 * @author 5034922
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ConfirmShipmentTransactionServiceImplTest {

    @InjectMocks
    private ConfirmShipmentTransactionServiceImpl shipmentCreationService;

    @Mock
    private ShipServiceClient shipServiceClient;

    @Mock
    private ConfirmShipmentTransactionServiceMapper mapper;

    @Mock
    private OpenShipmentRepository openShipmentRepository;

    @Mock
    private ConfirmShipmentRepository confirmShipmentRepository;
    
    @Mock
    private OpenShipmentTransactionServiceImpl openShipmentTransactionServiceImpl;
    
    @Mock
    private OpenShipmentTransactionServiceMapper openShipmentServiceMapper;
    
    @Mock
    private PackageEventServiceClient packageEventServiceClient;
    

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    
    @Mock
    public RatingEventService eventService;

    private static final String CARRIER_TYPE="EXPRESS";
    
    String referenceId;
    String locationId;
    String trackingId;
    String requestedTimeStamp;

    @Before
    public void setup() {

        shipmentCreationService = new ConfirmShipmentTransactionServiceImpl(shipServiceClient, mapper,
                openShipmentRepository, confirmShipmentRepository, openShipmentServiceMapper,packageEventServiceClient, eventService);
        referenceId = "3456";
        locationId = "LASA";
        trackingId = "794973860000";
        requestedTimeStamp = "2020-09-10T10:59:10.273Z";

    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint and it is executed successfully with payment type as ACCOUNT
     */
    @Test
    public void testConfirmShipmentWdAccountPaymentType() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        CompleteShipmentDetail completeShipmentDetail = createReply();
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(shipServiceClient.confirmOpenShipment(shipmentConfirmationRequest)).thenReturn(completeShipmentDetail);
        when(mapper.buildResponse(completeShipmentDetail)).thenReturn(createResponse(completeShipmentDetail));

        ConfirmedShipmentData actual =
                shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
        assertThat(actual.getMasterTrackingId(), notNullValue());
        assertThat(actual.getCarrierType(), notNullValue());
        assertThat(actual.getCarrierCode(), notNullValue());
        assertThat(actual.getPackageCount(), notNullValue());
        assertThat(actual.getPackageLineItems(), notNullValue());
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint and it is executed successfully with payment type as CASH
     */
    @Test
    public void testConfirmShipmentWdNonAccountPaymentType() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        shipmentConfirmationRequest.getPaymentDetail()
                .setPaymentType("CASH");

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(mapper.createRequestedShipmentDetail(shipmentConfirmationRequest, openShipment)).thenReturn(createRequestedShipmentDetail());
        when(shipServiceClient.modifyOpenShipment(any(RequestedShipmentDetail.class),any(WorkstationDetail.class))).thenReturn(createReply());
        when(mapper.buildResponse(any(CompleteShipmentDetail.class))).thenReturn(createConfirmedShipmentData(createShipmentDetail()));

        ConfirmedShipmentData actual = shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint with shipment already confirmed then it is executed successfully
     */
    @Test
    public void testConfirmShipmentAlreadyConfirmed() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        openShipment.setOpenShipmentStatusCode(TransactionStatus.CONFIRMED.name());
        ShipmentDetail shipmentDetail = createShipmentDetail();
        ConfirmedShipmentData confirmedShipmentData = createConfirmedShipmentData(shipmentDetail);

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId)).thenReturn(shipmentDetail);
        when(mapper.buildConfirmedShipmentResponse(shipmentDetail)).thenReturn(confirmedShipmentData);
        ConfirmedShipmentData actual = shipmentCreationService.confirmShipment(createShipmentConfirmationRequest());

        assertThat(actual, notNullValue());
        assertThat(actual.getMasterTrackingId(), notNullValue());
        assertThat(actual.getCarrierType(), notNullValue());
        assertThat(actual.getCarrierCode(), notNullValue());
        assertThat(actual.getPackageCount(), notNullValue());
        assertThat(actual.getPackageLineItems(), notNullValue());
    }
    
    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint with shipment gets stored in the db successfully
     */
    @Test
    public void testConfirmShipmentSave() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        shipmentConfirmationRequest.getPaymentDetail()
                .setPaymentType("CASH");

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(mapper.createRequestedShipmentDetail(shipmentConfirmationRequest, openShipment))
                .thenReturn(createRequestedShipmentDetail());
        when(shipServiceClient.modifyOpenShipment(any(RequestedShipmentDetail.class), any(WorkstationDetail.class)))
                .thenReturn(createReply());
        when(mapper.buildResponse(any(CompleteShipmentDetail.class)))
                .thenReturn(createConfirmedShipmentData(createShipmentDetail()));
        when(openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData())).thenReturn(createOpenShipmentData());
        when(mapper.buildPESRequest(any(ShipmentConfirmationRequest.class), any(OpenShipment.class),
                any(ConfirmedShipmentData.class), any(CompleteShipmentDetail.class), any(LocationDataDetail.class),
                any(OpenShipmentData.class))).thenReturn(new PackagePossessionEventDetailRequest());
        when(packageEventServiceClient.postPackagePossessionEvents(any(PackagePossessionEventDetailRequest.class)))
                .thenReturn(true);
        when(mapper.buildShipmentDetails(any(OpenShipment.class), any(ShipmentConfirmationRequest.class),
                any(LocationDataDetail.class), any(CompleteShipmentDetail.class), any(ConfirmedShipmentData.class)))
                        .thenReturn(new ShipmentDetail());
        when(confirmShipmentRepository.save(any(ShipmentDetail.class))).thenReturn(true);
        when(openShipmentServiceMapper.buildUpdateOpenShipmentStatusData(any(ShipmentConfirmationRequest.class)))
                .thenReturn(new UpdateOpenShipmentStatusData());


        ConfirmedShipmentData actual =
                shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
        verify(openShipmentRepository,times(1)).updateStatus(any(UpdateOpenShipmentStatusData.class));
    }

    /**
     * Test case to validate that when request is made with Non account payment and Ground service
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint with shipment gets stored in the db successfully and event service is invoked
     */
    @Test
    public void testConfirmShipmentSaveWithGroundService() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        openShipmentData.getOpenShipment()
                .setServiceType(ServiceType.FEDEX_GROUND.name());
        shipmentConfirmationRequest.getPaymentDetail()
                .setPaymentType(PaymentType.NON_ACCOUNT.name());
        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(mapper.createRequestedShipmentDetail(shipmentConfirmationRequest, openShipment))
                .thenReturn(createRequestedShipmentDetail());
        when(shipServiceClient.modifyOpenShipment(any(RequestedShipmentDetail.class), any(WorkstationDetail.class)))
                .thenReturn(createReply());
        when(mapper.buildResponse(any(CompleteShipmentDetail.class)))
                .thenReturn(createConfirmedShipmentData(createShipmentDetail()));
        when(openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData()))
                .thenReturn(openShipmentData);
        when(mapper.buildPESRequest(any(ShipmentConfirmationRequest.class), any(OpenShipment.class),
                any(ConfirmedShipmentData.class), any(CompleteShipmentDetail.class), any(LocationDataDetail.class),
                any(OpenShipmentData.class))).thenReturn(new PackagePossessionEventDetailRequest());
        when(packageEventServiceClient.postPackagePossessionEvents(any(PackagePossessionEventDetailRequest.class)))
                .thenReturn(true);
        when(mapper.buildShipmentDetails(any(OpenShipment.class), any(ShipmentConfirmationRequest.class),
                any(LocationDataDetail.class), any(CompleteShipmentDetail.class), any(ConfirmedShipmentData.class)))
                        .thenReturn(new ShipmentDetail());
        when(confirmShipmentRepository.save(any(ShipmentDetail.class))).thenReturn(true);
        when(openShipmentServiceMapper.buildUpdateOpenShipmentStatusData(any(ShipmentConfirmationRequest.class)))
                .thenReturn(new UpdateOpenShipmentStatusData());

        ConfirmedShipmentData actual = shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
        verify(eventService, times(1)).sendGroundTransactionRate(any(ShipmentConfirmationRequest.class),
                any(OpenShipmentData.class));
    }

    /**
     * Test case to validate that when request is made with Non account payment and Ground service
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint with shipment gets stored in the db successfully and event service is invoked
     */
    @Test
    public void testConfirmShipmentSaveWithGroundHomeDeliveryService() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        openShipmentData.getOpenShipment()
                .setServiceType(ServiceType.GROUND_HOME_DELIVERY.name());
        shipmentConfirmationRequest.getPaymentDetail()
                .setPaymentType(PaymentType.NON_ACCOUNT.name());
        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(mapper.createRequestedShipmentDetail(shipmentConfirmationRequest, openShipment))
                .thenReturn(createRequestedShipmentDetail());
        when(shipServiceClient.modifyOpenShipment(any(RequestedShipmentDetail.class), any(WorkstationDetail.class)))
                .thenReturn(createReply());
        when(mapper.buildResponse(any(CompleteShipmentDetail.class)))
                .thenReturn(createConfirmedShipmentData(createShipmentDetail()));
        when(openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData()))
                .thenReturn(openShipmentData);
        when(mapper.buildPESRequest(any(ShipmentConfirmationRequest.class), any(OpenShipment.class),
                any(ConfirmedShipmentData.class), any(CompleteShipmentDetail.class), any(LocationDataDetail.class),
                any(OpenShipmentData.class))).thenReturn(new PackagePossessionEventDetailRequest());
        when(packageEventServiceClient.postPackagePossessionEvents(any(PackagePossessionEventDetailRequest.class)))
                .thenReturn(true);
        when(mapper.buildShipmentDetails(any(OpenShipment.class), any(ShipmentConfirmationRequest.class),
                any(LocationDataDetail.class), any(CompleteShipmentDetail.class), any(ConfirmedShipmentData.class)))
                        .thenReturn(new ShipmentDetail());
        when(confirmShipmentRepository.save(any(ShipmentDetail.class))).thenReturn(true);
        when(openShipmentServiceMapper.buildUpdateOpenShipmentStatusData(any(ShipmentConfirmationRequest.class)))
                .thenReturn(new UpdateOpenShipmentStatusData());

        ConfirmedShipmentData actual = shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
        verify(eventService, times(1)).sendGroundTransactionRate(any(ShipmentConfirmationRequest.class),
                any(OpenShipmentData.class));
    }

    /**
     * Test case to validate that when request is made with Non account payment and Ground service
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)} end
     * point with shipment gets stored in the db successfully and ground rate notification throws
     * Terminal DB exception
     */
    @Test
    public void testConfirmShipmentSaveWithGroundHomeDeliveryServiceWithTerminalDBException() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        openShipmentData.getOpenShipment()
                .setServiceType(ServiceType.GROUND_HOME_DELIVERY.name());
        shipmentConfirmationRequest.getPaymentDetail()
                .setPaymentType(PaymentType.NON_ACCOUNT.name());
        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(mapper.createRequestedShipmentDetail(shipmentConfirmationRequest, openShipment))
                .thenReturn(createRequestedShipmentDetail());
        when(shipServiceClient.modifyOpenShipment(any(RequestedShipmentDetail.class), any(WorkstationDetail.class)))
                .thenReturn(createReply());
        when(mapper.buildResponse(any(CompleteShipmentDetail.class)))
                .thenReturn(createConfirmedShipmentData(createShipmentDetail()));
        when(openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData()))
                .thenReturn(openShipmentData);
        when(mapper.buildPESRequest(any(ShipmentConfirmationRequest.class), any(OpenShipment.class),
                any(ConfirmedShipmentData.class), any(CompleteShipmentDetail.class), any(LocationDataDetail.class),
                any(OpenShipmentData.class))).thenReturn(new PackagePossessionEventDetailRequest());
        when(packageEventServiceClient.postPackagePossessionEvents(any(PackagePossessionEventDetailRequest.class)))
                .thenReturn(true);
        when(mapper.buildShipmentDetails(any(OpenShipment.class), any(ShipmentConfirmationRequest.class),
                any(LocationDataDetail.class), any(CompleteShipmentDetail.class), any(ConfirmedShipmentData.class)))
                        .thenReturn(new ShipmentDetail());
        when(confirmShipmentRepository.save(any(ShipmentDetail.class))).thenReturn(true);
        when(openShipmentServiceMapper.buildUpdateOpenShipmentStatusData(any(ShipmentConfirmationRequest.class)))
                .thenReturn(new UpdateOpenShipmentStatusData());
        ConfirmedShipmentData actual = shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
        verify(eventService, times(1)).sendGroundTransactionRate(any(ShipmentConfirmationRequest.class),
                any(OpenShipmentData.class));
    }

    /**
     * Test case to validate that when request is made with Non account payment and Ground service
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint with shipment gets stored in the db successfully and ground rate notification throws
     * Transient DB exception
     */
    @Test
    public void testConfirmShipmentSaveWithGroundHomeDeliveryServiceWithTransientDBException() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        openShipmentData.getOpenShipment()
                .setServiceType(ServiceType.GROUND_HOME_DELIVERY.name());
        shipmentConfirmationRequest.getPaymentDetail()
                .setPaymentType(PaymentType.NON_ACCOUNT.name());
        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(mapper.createRequestedShipmentDetail(shipmentConfirmationRequest, openShipment))
                .thenReturn(createRequestedShipmentDetail());
        when(shipServiceClient.modifyOpenShipment(any(RequestedShipmentDetail.class), any(WorkstationDetail.class)))
                .thenReturn(createReply());
        when(mapper.buildResponse(any(CompleteShipmentDetail.class)))
                .thenReturn(createConfirmedShipmentData(createShipmentDetail()));
        when(openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData()))
                .thenReturn(openShipmentData);
        when(mapper.buildPESRequest(any(ShipmentConfirmationRequest.class), any(OpenShipment.class),
                any(ConfirmedShipmentData.class), any(CompleteShipmentDetail.class), any(LocationDataDetail.class),
                any(OpenShipmentData.class))).thenReturn(new PackagePossessionEventDetailRequest());
        when(packageEventServiceClient.postPackagePossessionEvents(any(PackagePossessionEventDetailRequest.class)))
                .thenReturn(true);
        when(mapper.buildShipmentDetails(any(OpenShipment.class), any(ShipmentConfirmationRequest.class),
                any(LocationDataDetail.class), any(CompleteShipmentDetail.class), any(ConfirmedShipmentData.class)))
                        .thenReturn(new ShipmentDetail());
        when(confirmShipmentRepository.save(any(ShipmentDetail.class))).thenReturn(true);
        when(openShipmentServiceMapper.buildUpdateOpenShipmentStatusData(any(ShipmentConfirmationRequest.class)))
                .thenReturn(new UpdateOpenShipmentStatusData());
        ConfirmedShipmentData actual = shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
        verify(eventService, times(1)).sendGroundTransactionRate(any(ShipmentConfirmationRequest.class),
                any(OpenShipmentData.class));
    }

    /**
     * Test case to validate that when request is made with Non account payment and Ground service
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint with shipment gets stored in the db successfully and ground rate notification throws
     * Hystrix exception
     */
    @Test
    public void testConfirmShipmentSaveWithGroundHomeDeliveryServiceWithHystrixException() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        OpenShipmentData openShipmentData = createOpenShipmentData();
        openShipmentData.getOpenShipment()
                .setServiceType(ServiceType.GROUND_HOME_DELIVERY.name());
        shipmentConfirmationRequest.getPaymentDetail()
                .setPaymentType(PaymentType.NON_ACCOUNT.name());
        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(mapper.createRequestedShipmentDetail(shipmentConfirmationRequest, openShipment))
                .thenReturn(createRequestedShipmentDetail());
        when(shipServiceClient.modifyOpenShipment(any(RequestedShipmentDetail.class), any(WorkstationDetail.class)))
                .thenReturn(createReply());
        when(mapper.buildResponse(any(CompleteShipmentDetail.class)))
                .thenReturn(createConfirmedShipmentData(createShipmentDetail()));
        when(openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData()))
                .thenReturn(openShipmentData);
        when(mapper.buildPESRequest(any(ShipmentConfirmationRequest.class), any(OpenShipment.class),
                any(ConfirmedShipmentData.class), any(CompleteShipmentDetail.class), any(LocationDataDetail.class),
                any(OpenShipmentData.class))).thenReturn(new PackagePossessionEventDetailRequest());
        when(packageEventServiceClient.postPackagePossessionEvents(any(PackagePossessionEventDetailRequest.class)))
                .thenReturn(true);
        when(mapper.buildShipmentDetails(any(OpenShipment.class), any(ShipmentConfirmationRequest.class),
                any(LocationDataDetail.class), any(CompleteShipmentDetail.class), any(ConfirmedShipmentData.class)))
                        .thenReturn(new ShipmentDetail());
        when(confirmShipmentRepository.save(any(ShipmentDetail.class))).thenReturn(true);
        when(openShipmentServiceMapper.buildUpdateOpenShipmentStatusData(any(ShipmentConfirmationRequest.class)))
                .thenReturn(new UpdateOpenShipmentStatusData());
        ConfirmedShipmentData actual = shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
        verify(eventService, times(1)).sendGroundTransactionRate(any(ShipmentConfirmationRequest.class),
                any(OpenShipmentData.class));
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint and it throws TerminalDbAccessException then it's handled successfully
     */
    @Test
    public void testConfirmShipmentTerminalDbAccessException() {

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId))
                .thenThrow(TerminalDbAccessException.class);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_SEARCH_FAILURE.getMessage()));

        shipmentCreationService.confirmShipment(createShipmentConfirmationRequest());
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint it throws TransientDbAccessException and it's handled successfully
     */
    @Test
    public void testConfirmShipmentTransientDbAccessException() {

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId))
                .thenThrow(TransientDbAccessException.class);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_SEARCH_FAILURE.getMessage()));

        shipmentCreationService.confirmShipment(createShipmentConfirmationRequest());
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint and it throws HystrixRuntimeException then it's handled successfully
     */
    @Test
    public void testConfirmShipmentHystrixRuntimeException() {

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId))
                .thenThrow(HystrixRuntimeException.class);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.SYSTEM_UNAVAILABLE.getMessage()));

        shipmentCreationService.confirmShipment(createShipmentConfirmationRequest());
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint and it throws TerminalDbAccessException on retrieving shipment transaction information
     * then it's handled successfully
     */
    @Test
    public void testConfirmShipmentTerminalDbAccessExceptionOnTransactionInfo() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        openShipment.setOpenShipmentStatusCode(TransactionStatus.CONFIRMED.name());
        CompleteShipmentDetail completeShipmentDetail = createReply();
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId))
                .thenThrow(TerminalDbAccessException.class);
        when(shipServiceClient.confirmOpenShipment(shipmentConfirmationRequest)).thenReturn(completeShipmentDetail);
        when(mapper.buildResponse(completeShipmentDetail)).thenReturn(createResponse(completeShipmentDetail));
        ConfirmedShipmentData actual = shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
        assertThat(actual.getMasterTrackingId(), notNullValue());
        assertThat(actual.getCarrierType(), notNullValue());
        assertThat(actual.getCarrierCode(), notNullValue());
        assertThat(actual.getPackageCount(), notNullValue());
        assertThat(actual.getPackageLineItems(), notNullValue());
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint and it throws TransientDbAccessException on retrieving shipment transaction information
     * then it's handled successfully
     */
    @Test
    public void testConfirmShipmentTransientDbAccessExceptionOnTransactionInfo() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        openShipment.setOpenShipmentStatusCode(TransactionStatus.CONFIRMED.name());
        CompleteShipmentDetail completeShipmentDetail = createReply();
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId))
                .thenThrow(TransientDbAccessException.class);
        when(shipServiceClient.confirmOpenShipment(shipmentConfirmationRequest)).thenReturn(completeShipmentDetail);
        when(mapper.buildResponse(completeShipmentDetail)).thenReturn(createResponse(completeShipmentDetail));
        ConfirmedShipmentData actual = shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
        assertThat(actual.getMasterTrackingId(), notNullValue());
        assertThat(actual.getCarrierType(), notNullValue());
        assertThat(actual.getCarrierCode(), notNullValue());
        assertThat(actual.getPackageCount(), notNullValue());
        assertThat(actual.getPackageLineItems(), notNullValue());
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint and it throws HystrixRuntimeException on retrieving shipment transaction information
     * then it's handled successfully
     */
    @Test
    public void testConfirmShipmentHystrixRuntimeExceptionOnTransactionInfo() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        openShipment.setOpenShipmentStatusCode(TransactionStatus.CONFIRMED.name());
        CompleteShipmentDetail completeShipmentDetail = createReply();
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId))
                .thenThrow(HystrixRuntimeException.class);
        when(shipServiceClient.confirmOpenShipment(shipmentConfirmationRequest)).thenReturn(completeShipmentDetail);
        when(mapper.buildResponse(completeShipmentDetail)).thenReturn(createResponse(completeShipmentDetail));
        ConfirmedShipmentData actual = shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
        assertThat(actual.getMasterTrackingId(), notNullValue());
        assertThat(actual.getCarrierType(), notNullValue());
        assertThat(actual.getCarrierCode(), notNullValue());
        assertThat(actual.getPackageCount(), notNullValue());
        assertThat(actual.getPackageLineItems(), notNullValue());
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint and it throws TransientDbAccessException on CSHP call then it's handled successfully
     */
    @Test
    public void testConfirmShipmentClientTransientExceptionOnCSHPCall() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);

        ClientTransientException clientTransientException =
                new ClientTransientException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);
        when(shipServiceClient.confirmOpenShipment(shipmentConfirmationRequest)).thenThrow(clientTransientException);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getMessage()));

        shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint and it throws HystrixRuntimeException on CSHP call then it's handled successfully
     */
    @Test
    public void testConfirmShipmentClientTerminalExceptionOnCSHPCall() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);

        ClientTerminalException clientTerminalException =
                new ClientTerminalException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);
        when(shipServiceClient.confirmOpenShipment(shipmentConfirmationRequest)).thenThrow(clientTerminalException);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getMessage()));

        shipmentCreationService.confirmShipment(shipmentConfirmationRequest);
    }
    
    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint with shipment already confirmed then it gives notification as Note 
     */
    @Test
    public void testConfirmShipmentAlreadyConfirmedNotification() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        openShipment.setOpenShipmentStatusCode(TransactionStatus.CONFIRMED.name());
        ShipmentDetail shipmentDetail = createShipmentDetail();
        ConfirmedShipmentData confirmedShipmentData = createConfirmedShipmentData(shipmentDetail);
        List<EventNotification> notifications = new ArrayList<>();
        EventNotification notification = new EventNotification("SHIPMENT.ALREADY.CONFIRMED",
                "Shipment Already Confirmed and paymentRefId matches in transaction table.", NotificationType.NOTE,
                null);
        notifications.add(notification);

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId)).thenReturn(shipmentDetail);
        when(mapper.buildConfirmedShipmentResponse(shipmentDetail)).thenReturn(confirmedShipmentData);
        ConfirmedShipmentData actual =
                shipmentCreationService.confirmShipment(createShipmentConfirmationRequest());

        assertThat(actual, notNullValue());
        assertThat(actual.getNotifications()
                .get(0)
                .getCode(),
                equalTo(notifications.get(0)
                        .getCode()));
        assertThat(actual.getNotifications()
                .get(0)
                .getMessage(),
                equalTo(notifications.get(0)
                        .getMessage()));
        assertThat(actual.getNotifications()
                .get(0)
                .getNotificationType().name(),
                equalTo(notifications.get(0)
                        .getNotificationType().name()));
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint with shipment doesn't gets stored in the db.
     */
    @Test
    public void testConfirmShipmentSaveFails() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        shipmentConfirmationRequest.getPaymentDetail()
                .setPaymentType("CASH");

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(mapper.createRequestedShipmentDetail(shipmentConfirmationRequest, openShipment))
                .thenReturn(createRequestedShipmentDetail());
        when(shipServiceClient.modifyOpenShipment(any(RequestedShipmentDetail.class), any(WorkstationDetail.class)))
                .thenReturn(createReply());
        when(mapper.buildResponse(any(CompleteShipmentDetail.class)))
                .thenReturn(createConfirmedShipmentData(createShipmentDetail()));
        when(openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData())).thenReturn(createOpenShipmentData());
        when(mapper.buildPESRequest(any(ShipmentConfirmationRequest.class), any(OpenShipment.class),
                any(ConfirmedShipmentData.class), any(CompleteShipmentDetail.class), any(LocationDataDetail.class),
                any(OpenShipmentData.class))).thenReturn(new PackagePossessionEventDetailRequest());
        when(packageEventServiceClient.postPackagePossessionEvents(any(PackagePossessionEventDetailRequest.class)))
                .thenReturn(true);
        when(mapper.buildShipmentDetails(any(OpenShipment.class), any(ShipmentConfirmationRequest.class),
                any(LocationDataDetail.class), any(CompleteShipmentDetail.class), any(ConfirmedShipmentData.class)))
                        .thenReturn(new ShipmentDetail());
        when(confirmShipmentRepository.save(any(ShipmentDetail.class))).thenReturn(false);

        ConfirmedShipmentData actual =
                shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
        verify(openShipmentRepository, never()).updateStatus(any(UpdateOpenShipmentStatusData.class));
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint with shipment it will throws Terminal Exception.
     */
    @Test
    public void testConfirmShipmentSaveTerminalException() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        shipmentConfirmationRequest.getPaymentDetail()
                .setPaymentType("CASH");

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(mapper.createRequestedShipmentDetail(shipmentConfirmationRequest, openShipment))
                .thenReturn(createRequestedShipmentDetail());
        when(shipServiceClient.modifyOpenShipment(any(RequestedShipmentDetail.class), any(WorkstationDetail.class)))
                .thenReturn(createReply());
        when(mapper.buildResponse(any(CompleteShipmentDetail.class)))
                .thenReturn(createConfirmedShipmentData(createShipmentDetail()));
        when(openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData())).thenReturn(createOpenShipmentData());
        when(mapper.buildPESRequest(any(ShipmentConfirmationRequest.class), any(OpenShipment.class),
                any(ConfirmedShipmentData.class), any(CompleteShipmentDetail.class), any(LocationDataDetail.class),
                any(OpenShipmentData.class))).thenReturn(new PackagePossessionEventDetailRequest());
        when(packageEventServiceClient.postPackagePossessionEvents(any(PackagePossessionEventDetailRequest.class)))
                .thenReturn(true);
        when(mapper.buildShipmentDetails(any(OpenShipment.class), any(ShipmentConfirmationRequest.class),
                any(LocationDataDetail.class), any(CompleteShipmentDetail.class), any(ConfirmedShipmentData.class)))
                        .thenReturn(new ShipmentDetail());
        when(confirmShipmentRepository.save(any(ShipmentDetail.class))).thenThrow(TerminalDbAccessException.class);

        ConfirmedShipmentData actual = shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
        assertThat(actual.getNotifications()
                .get(0)
                .getCode(), equalTo(ServiceErrorCode.DB_INSERTION_FAILURE.getApiErrorCode()));
        assertThat(actual.getNotifications()
                .get(0)
                .getMessage(), equalTo(ServiceErrorCode.DB_INSERTION_FAILURE.getMessage()));
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint with shipment then it will throw Hystrix exception
     */
    @Test
    public void testConfirmShipmentSaveHystrixException() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        shipmentConfirmationRequest.getPaymentDetail()
                .setPaymentType("CASH");

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(mapper.createRequestedShipmentDetail(shipmentConfirmationRequest, openShipment))
                .thenReturn(createRequestedShipmentDetail());
        when(shipServiceClient.modifyOpenShipment(any(RequestedShipmentDetail.class), any(WorkstationDetail.class)))
                .thenReturn(createReply());
        when(mapper.buildResponse(any(CompleteShipmentDetail.class)))
                .thenReturn(createConfirmedShipmentData(createShipmentDetail()));
        when(openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData())).thenReturn(createOpenShipmentData());
        when(mapper.buildPESRequest(any(ShipmentConfirmationRequest.class), any(OpenShipment.class),
                any(ConfirmedShipmentData.class), any(CompleteShipmentDetail.class), any(LocationDataDetail.class),
                any(OpenShipmentData.class))).thenReturn(new PackagePossessionEventDetailRequest());
        when(packageEventServiceClient.postPackagePossessionEvents(any(PackagePossessionEventDetailRequest.class)))
                .thenReturn(true);
        when(mapper.buildShipmentDetails(any(OpenShipment.class), any(ShipmentConfirmationRequest.class),
                any(LocationDataDetail.class), any(CompleteShipmentDetail.class), any(ConfirmedShipmentData.class)))
                        .thenReturn(new ShipmentDetail());
        when(confirmShipmentRepository.save(any(ShipmentDetail.class))).thenThrow(HystrixRuntimeException.class);

        ConfirmedShipmentData actual =
                shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
        assertThat(actual.getNotifications()
                .get(0)
                .getCode(), equalTo(ServiceErrorCode.SYSTEM_UNAVAILABLE.getApiErrorCode()));
        assertThat(actual.getNotifications()
                .get(0)
                .getMessage(), equalTo(ServiceErrorCode.SYSTEM_UNAVAILABLE.getMessage()));
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint with shipment and if PES call fails then it will throw
     * ClientTerminalException
     */
    @Test
    public void testConfirmShipmentSaveClientTransientException() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        shipmentConfirmationRequest.getPaymentDetail()
                .setPaymentType("CASH");

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(mapper.createRequestedShipmentDetail(shipmentConfirmationRequest, openShipment))
                .thenReturn(createRequestedShipmentDetail());
        when(shipServiceClient.modifyOpenShipment(any(RequestedShipmentDetail.class), any(WorkstationDetail.class)))
                .thenReturn(createReply());
        when(mapper.buildResponse(any(CompleteShipmentDetail.class)))
                .thenReturn(createConfirmedShipmentData(createShipmentDetail()));
        

        ConfirmedShipmentData actual = shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
    }
    
    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint with shipment and if PES call fails then it will throw
     * ClientTerminalException
     * 
     * @throws ClientTerminalException
     */
    @Test
    public void testConfirmShipmentClientTerminalException() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        shipmentConfirmationRequest.getPaymentDetail()
                .setPaymentType("CASH");

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(mapper.createRequestedShipmentDetail(shipmentConfirmationRequest, openShipment))
                .thenReturn(createRequestedShipmentDetail());
        when(shipServiceClient.modifyOpenShipment(any(RequestedShipmentDetail.class), any(WorkstationDetail.class)))
                .thenReturn(createReply());
        when(mapper.buildResponse(any(CompleteShipmentDetail.class)))
                .thenReturn(createConfirmedShipmentData(createShipmentDetail()));
        when(openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData())).thenReturn(createOpenShipmentData());
        when(mapper.buildPESRequest(any(ShipmentConfirmationRequest.class), any(OpenShipment.class),
                any(ConfirmedShipmentData.class), any(CompleteShipmentDetail.class), any(LocationDataDetail.class),
                any(OpenShipmentData.class))).thenReturn(new PackagePossessionEventDetailRequest());
        when(packageEventServiceClient.postPackagePossessionEvents(any(PackagePossessionEventDetailRequest.class)))
                .thenThrow(new ClientTerminalException(ServiceErrorCode.PES_SERVICE_UNAVAILABLE));
        try {
            shipmentCreationService.confirmShipment(shipmentConfirmationRequest);
        } catch (ServiceProcessorTerminalException ex) {
            assertThat(ServiceErrorCode.PES_SERVICE_UNAVAILABLE.getMessage(), equalTo(ex.getMessage()));
        }
    }
    
    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint with shipment and if PES call gets fail then it will throw
     * ClientTransientException
     * 
     * @throws ClientTransientException
     */
    @Test
    public void testConfirmShipmentClientTransientException() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        shipmentConfirmationRequest.getPaymentDetail()
                .setPaymentType("CASH");

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(mapper.createRequestedShipmentDetail(shipmentConfirmationRequest, openShipment))
                .thenReturn(createRequestedShipmentDetail());
        when(shipServiceClient.modifyOpenShipment(any(RequestedShipmentDetail.class), any(WorkstationDetail.class)))
                .thenReturn(createReply());
        when(mapper.buildResponse(any(CompleteShipmentDetail.class)))
                .thenReturn(createConfirmedShipmentData(createShipmentDetail()));
        when(openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData())).thenReturn(createOpenShipmentData());
        when(mapper.buildPESRequest(any(ShipmentConfirmationRequest.class), any(OpenShipment.class),
                any(ConfirmedShipmentData.class), any(CompleteShipmentDetail.class), any(LocationDataDetail.class),
                any(OpenShipmentData.class))).thenReturn(new PackagePossessionEventDetailRequest());
        when(packageEventServiceClient.postPackagePossessionEvents(any(PackagePossessionEventDetailRequest.class)))
                .thenThrow(new ClientTransientException(ServiceErrorCode.PES_SERVICE_UNAVAILABLE));
        try {
            shipmentCreationService.confirmShipment(shipmentConfirmationRequest);
        } catch (ServiceProcessorTerminalException ex) {
            assertThat(ServiceErrorCode.PES_SERVICE_UNAVAILABLE.getMessage(), equalTo(ex.getMessage()));
        }
    }
    
    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#stop(DeleteSoldShipmentData)}
     * endpoint and it is executed successfully.
     */
    @Test
    public void testStopSuccess() {

        ShipmentDetail shipmentDetail = createShipmentDetail();
        ConfirmedShipmentData confirmedShipmentData = createConfirmedShipmentData(shipmentDetail);
        PackageCancellationEventDetailRequest packageCancellationEventDetailRequest =
                getPackageCancellationEventDetailRequest();
        confirmedShipmentData.setCarrierType(CARRIER_TYPE);
        DeleteSoldShipmentData deleteSoldShipmentData = createDeleteSoldShipmentData();
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId)).thenReturn(shipmentDetail);
        when(mapper.buildConfirmedShipmentResponse(shipmentDetail)).thenReturn(confirmedShipmentData);
        when(packageEventServiceClient.postPackageCancellationEvents(packageCancellationEventDetailRequest))
                .thenReturn(true);

        when(mapper.buildPESCancelRequest(any(String.class), any(ConfirmedShipmentData.class),
                any(WorkstationDetail.class), any(LocationDataDetail.class), any(PersonDetail.class)))
                        .thenReturn(packageCancellationEventDetailRequest);

        shipmentCreationService.stop(deleteSoldShipmentData);

        verify(confirmShipmentRepository, times(1)).getShipmentDetails(trackingId, referenceId);
        verify(mapper, times(1)).buildConfirmedShipmentResponse(shipmentDetail);
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#stop(DeleteSoldShipmentData)} endpoint when
     * confirmed shipment is null and it is executed successfully.
     */
    @Test
    public void testStopWhenConFirmedShipmentNull() {

        ShipmentDetail shipmentDetail = createShipmentDetail();
        ConfirmedShipmentData confirmedShipmentData = null;
        DeleteSoldShipmentData deleteSoldShipmentData = createDeleteSoldShipmentData();
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId)).thenReturn(shipmentDetail);
        when(mapper.buildConfirmedShipmentResponse(shipmentDetail)).thenReturn(confirmedShipmentData);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.SHIPMENT_ALREADY_DELETED.getMessage()));

        shipmentCreationService.stop(deleteSoldShipmentData);

        verify(confirmShipmentRepository, times(1)).getShipmentDetails(trackingId, referenceId);
        verify(mapper, times(1)).buildConfirmedShipmentResponse(shipmentDetail);
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#stop(DeleteSoldShipmentData)} endpoint when
     * shipment is returned and it is executed successfully.
     */
    @Test
    public void testStopWhenShipmentStatusReturned() {

        ShipmentDetail shipmentDetail = createShipmentDetail();
        ConfirmedShipmentData confirmedShipmentData = createConfirmedShipmentData(shipmentDetail);
        confirmedShipmentData.setShipmentStatus(TransactionStatus.RETURNED);
        DeleteSoldShipmentData deleteSoldShipmentData = createDeleteSoldShipmentData();
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId)).thenReturn(shipmentDetail);
        when(mapper.buildConfirmedShipmentResponse(shipmentDetail)).thenReturn(confirmedShipmentData);
        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.SHIPMENT_ALREADY_DELETED.getMessage()));
        shipmentCreationService.stop(deleteSoldShipmentData);

        verify(confirmShipmentRepository, times(1)).getShipmentDetails(trackingId, referenceId);
        verify(mapper, times(1)).buildConfirmedShipmentResponse(shipmentDetail);
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#stop(DeleteSoldShipmentData)} endpoint when
     * tracking id is null and it is executed successfully.
     */
    @Test
    public void testStopWhenMasterTrackingNull() {

        ShipmentDetail shipmentDetail = createShipmentDetail();
        ConfirmedShipmentData confirmedShipmentData = createConfirmedShipmentData(shipmentDetail);
        confirmedShipmentData.setMasterTrackingId(null);
        DeleteSoldShipmentData deleteSoldShipmentData = createDeleteSoldShipmentData();
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId)).thenReturn(shipmentDetail);
        when(mapper.buildConfirmedShipmentResponse(shipmentDetail)).thenReturn(confirmedShipmentData);
        shipmentCreationService.stop(deleteSoldShipmentData);

        verify(confirmShipmentRepository, times(1)).getShipmentDetails(trackingId, referenceId);
        verify(mapper, times(1)).buildConfirmedShipmentResponse(shipmentDetail);
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#stop(DeleteSoldShipmentData)} endpoint then
     * client transient exception is thrown.
     * 
     * @throws ClientTransientException
     */
    @Test
    public void testStopWhenTransientException() {

        ShipmentDetail shipmentDetail = createShipmentDetail();
        ConfirmedShipmentData confirmedShipmentData = createConfirmedShipmentData(shipmentDetail);
        DeleteSoldShipmentData deleteSoldShipmentData = createDeleteSoldShipmentData();
        ClientTransientException clientTransientException =
                new ClientTransientException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);
        when(shipServiceClient.deleteShipment(any(TrackingIdDetail.class), any(WorkstationDetail.class),
                any(String.class))).thenThrow(clientTransientException);
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId)).thenReturn(shipmentDetail);
        when(mapper.buildConfirmedShipmentResponse(shipmentDetail)).thenReturn(confirmedShipmentData);

        shipmentCreationService.stop(deleteSoldShipmentData);

        verify(confirmShipmentRepository, times(1)).getShipmentDetails(trackingId, referenceId);
        verify(mapper, times(1)).buildConfirmedShipmentResponse(shipmentDetail);
    }
    
    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#stop(shipmentTrackingNumber,retailTransactionId,locationId,requestDateTime,workstationDetail,locationDataDetail,personDetail)}
     * endpoint then client transient exception is thrown.
     * 
     * @throws HystrixException
     */
    @Test
    public void testStopWhenHystrixException() {

        ShipmentDetail shipmentDetail = createShipmentDetail();
        ConfirmedShipmentData confirmedShipmentData = createConfirmedShipmentData(shipmentDetail);
        DeleteSoldShipmentData deleteSoldShipmentData = createDeleteSoldShipmentData();
        when(shipServiceClient.deleteShipment(any(TrackingIdDetail.class), any(WorkstationDetail.class),
                any(String.class))).thenThrow(HystrixRuntimeException.class);
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId)).thenReturn(shipmentDetail);
        when(mapper.buildConfirmedShipmentResponse(shipmentDetail)).thenReturn(confirmedShipmentData);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        shipmentCreationService.stop(deleteSoldShipmentData);

        verify(confirmShipmentRepository, times(1)).getShipmentDetails(trackingId, referenceId);
        verify(mapper, times(1)).buildConfirmedShipmentResponse(shipmentDetail);
    }
    
    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#stop(DeleteSoldShipmentData)} endpoint then
     * client terminal exception is thrown.
     * 
     * @throws ClientTerminalException
     */
    @Test
    public void testStopWhenTerminalException() {

        ShipmentDetail shipmentDetail = createShipmentDetail();
        ConfirmedShipmentData confirmedShipmentData = createConfirmedShipmentData(shipmentDetail);
        DeleteSoldShipmentData deleteSoldShipmentData = createDeleteSoldShipmentData();
        ClientTerminalException clientTerminalException =
                new ClientTerminalException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);
        when(shipServiceClient.deleteShipment(any(TrackingIdDetail.class), any(WorkstationDetail.class),
                any(String.class))).thenThrow(clientTerminalException);
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId)).thenReturn(shipmentDetail);
        when(mapper.buildConfirmedShipmentResponse(shipmentDetail)).thenReturn(confirmedShipmentData);

        shipmentCreationService.stop(deleteSoldShipmentData);

        verify(confirmShipmentRepository, times(1)).getShipmentDetails(trackingId, referenceId);
        verify(mapper, times(1)).buildConfirmedShipmentResponse(shipmentDetail);
    }
    
    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#stop(DeleteSoldShipmentData)} endpoint then
     * client terminal exception is thrown.
     * 
     * @throws ClientTerminalException
     */
    @Test
    public void testStopWhenUpdateStatusTerminalDbAccessException() {

        ShipmentDetail shipmentDetail = createShipmentDetail();
        PackageCancellationEventDetailRequest packageCancellationEventDetailRequest =
                getPackageCancellationEventDetailRequest();
        ConfirmedShipmentData confirmedShipmentData = createConfirmedShipmentData(shipmentDetail);
        confirmedShipmentData.setCarrierType(CARRIER_TYPE);
        DeleteSoldShipmentData deleteSoldShipmentData = createDeleteSoldShipmentData();

        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId)).thenReturn(shipmentDetail);
        when(mapper.buildConfirmedShipmentResponse(shipmentDetail)).thenReturn(confirmedShipmentData);
        when(mapper.buildPESCancelRequest(any(String.class), any(ConfirmedShipmentData.class),
                any(WorkstationDetail.class), any(LocationDataDetail.class), any(PersonDetail.class)))
                        .thenReturn(packageCancellationEventDetailRequest);
        when(packageEventServiceClient.postPackageCancellationEvents(packageCancellationEventDetailRequest))
                .thenReturn(true);
        when(mapper.buildUpdateOpenShipmentStatusData(trackingId, locationId, "RETURNED", null, referenceId))
                .thenReturn(getUpdateOpenShipmentStatusData());
        when(confirmShipmentRepository.updateStatus(any(UpdateOpenShipmentStatusData.class)))
                .thenThrow(TerminalDbAccessException.class);

        shipmentCreationService.stop(deleteSoldShipmentData);

        verify(confirmShipmentRepository, times(1)).getShipmentDetails(trackingId, referenceId);
        verify(mapper, times(1)).buildConfirmedShipmentResponse(shipmentDetail);

    }
    
    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#stop(DeleteSoldShipmentData)} endpoint then
     * hystrix runtime exception is thrown.
     * 
     * @throws HystrixRuntimeException
     */
    @Test
    public void testStopWhenUpdateStatusHystrixException() {

        ShipmentDetail shipmentDetail = createShipmentDetail();
        PackageCancellationEventDetailRequest packageCancellationEventDetailRequest =
                getPackageCancellationEventDetailRequest();
        ConfirmedShipmentData confirmedShipmentData = createConfirmedShipmentData(shipmentDetail);
        confirmedShipmentData.setCarrierType(CARRIER_TYPE);
        DeleteSoldShipmentData deleteSoldShipmentData = createDeleteSoldShipmentData();

        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId)).thenReturn(shipmentDetail);
        when(mapper.buildConfirmedShipmentResponse(shipmentDetail)).thenReturn(confirmedShipmentData);
        when(mapper.buildPESCancelRequest(any(String.class), any(ConfirmedShipmentData.class),
                any(WorkstationDetail.class), any(LocationDataDetail.class), any(PersonDetail.class)))
                        .thenReturn(packageCancellationEventDetailRequest);
        when(packageEventServiceClient.postPackageCancellationEvents(packageCancellationEventDetailRequest))
                .thenReturn(true);
        when(mapper.buildUpdateOpenShipmentStatusData(trackingId, locationId, "RETURNED", null, referenceId))
                .thenReturn(getUpdateOpenShipmentStatusData());
        when(confirmShipmentRepository.updateStatus(any(UpdateOpenShipmentStatusData.class)))
                .thenThrow(HystrixRuntimeException.class);

        shipmentCreationService.stop(deleteSoldShipmentData);

        verify(confirmShipmentRepository, times(1)).getShipmentDetails(trackingId, referenceId);
        verify(mapper, times(1)).buildConfirmedShipmentResponse(shipmentDetail);

    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#stop(DeleteSoldShipmentData)} endpoint then
     * transient db exception is thrown.
     * 
     * @throws TransientDbAccessException
     */
    @Test
    public void testStopWhenTransientDBException() {

        DeleteSoldShipmentData deleteSoldShipmentData = createDeleteSoldShipmentData();
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId))
                .thenThrow(TransientDbAccessException.class);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        shipmentCreationService.stop(deleteSoldShipmentData);

        verify(confirmShipmentRepository, times(1)).getShipmentDetails(trackingId, referenceId);
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#stop(DeleteSoldShipmentData)} endpoint then
     * terminal db access exception is thrown.
     * 
     * @throws TerminalDbAccessException
     */
    @Test
    public void testStopWhenTerminalDBException() {

        DeleteSoldShipmentData deleteSoldShipmentData = createDeleteSoldShipmentData();
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId))
                .thenThrow(TerminalDbAccessException.class);
        expectedEx.expect(ServiceProcessorTerminalException.class);

        shipmentCreationService.stop(deleteSoldShipmentData);

        verify(confirmShipmentRepository, times(1)).getShipmentDetails(trackingId, referenceId);
    }

    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#stop(DeleteSoldShipmentData)} endpoint then
     * hystrix runtime exception is thrown.
     * 
     * @throws HystrixRuntimeException
     */
    @Test
    public void testStopWhenHystrixDBException() {

        DeleteSoldShipmentData deleteSoldShipmentData = createDeleteSoldShipmentData();
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId))
                .thenThrow(HystrixRuntimeException.class);
        expectedEx.expect(ServiceProcessorTerminalException.class);

        shipmentCreationService.stop(deleteSoldShipmentData);

        verify(confirmShipmentRepository, times(1)).getShipmentDetails(trackingId, referenceId);
    }
    
    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#stop(DeleteSoldShipmentData)} endpoint when
     * shipment status is null and it is executed successfully.
     */
    @Test
    public void testStopWhenShipmentStatusNull() {

        ShipmentDetail shipmentDetail = createShipmentDetail();
        ConfirmedShipmentData confirmedShipmentData = createConfirmedShipmentData(shipmentDetail);
        confirmedShipmentData.setShipmentStatus(null);
        DeleteSoldShipmentData deleteSoldShipmentData = createDeleteSoldShipmentData();
        when(confirmShipmentRepository.getShipmentDetails(trackingId, referenceId)).thenReturn(shipmentDetail);
        when(mapper.buildConfirmedShipmentResponse(shipmentDetail)).thenReturn(confirmedShipmentData);
        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.SHIPMENT_ALREADY_DELETED.getMessage()));
        shipmentCreationService.stop(deleteSoldShipmentData);

        verify(confirmShipmentRepository, times(1)).getShipmentDetails(trackingId, referenceId);
        verify(mapper, times(1)).buildConfirmedShipmentResponse(shipmentDetail);
    }

    /**
     * Method to prepare the response object of ConfirmedShipmentData
     *
     * @param reply
     * @return
     */
    private ConfirmedShipmentData createResponse(
            CompleteShipmentDetail reply) {

        ConfirmedShipmentData data = null;
        data = new ConfirmedShipmentData();
        CarrierCodeType code = CarrierCodeType.valueOf(reply.getCarrierCode());
        data.setCarrierCode(code.name());
        data.setCarrierType(CarrierType.GROUND.name());
        data.setMasterTrackingId(new TrackingSequenceDetail(reply.getMasterTrackingId()
                .getTrackingNumber(), null, 1));
        data.setPackageCount(reply.getPackageCount());
        data.setPackageLineItems(createPackageLineItem(reply.getCompletedPackageDetails()
                .get(0), reply));

        return data;
    }

    /**
     * Create packageLine item
     *
     * @param completedPackageData
     * @param details
     * @return
     */
    private List<ConfirmedPackageData> createPackageLineItem(
            CompletedPackageData completedPackageData,
            CompleteShipmentDetail details) {

        List<ConfirmedPackageData> packageDetails = new ArrayList<>();
        ConfirmedPackageData data = new ConfirmedPackageData();
        TrackingSequenceDetail trackingSequence = null;
        data.setTrackingId(trackingSequence);
        data.setMaster(true);
        packageDetails.add(data);

        return packageDetails;
    }

    /**
     * Method to prepare the reply of CompleteShipmentDetail
     *
     * @return
     */
    private CompleteShipmentDetail createReply() {

        CompleteShipmentDetail completeShipmentDetail = new CompleteShipmentDetail();
        TrackingIdDetail masterTrackingId = new TrackingIdDetail();
        List<CompletedPackageData> completedPackageDetails = new ArrayList<>();

        masterTrackingId.setTrackingIdType("794978662786");
        masterTrackingId.setTrackingIdType("FEDEX");
        List<TrackingIdDetail> trackingIds = new ArrayList<>();
        trackingIds.add(masterTrackingId);
        completedPackageDetails
                .add(createCompletePackageDetails(1, trackingIds, 0, "SERVICE_DEFAULT", new PackageOperationData()));
        completeShipmentDetail.setCompletedPackageDetails(completedPackageDetails);
        completeShipmentDetail.setMasterTrackingId(masterTrackingId);
        completeShipmentDetail.setUsDomestic(true);
        completeShipmentDetail.setIndex("12345");
        completeShipmentDetail.setCarrierCode(CarrierCodeType.FDXG.name());

        return completeShipmentDetail;
    }

    /**
     * Create CompletedPackageData object
     *
     * @param sequenceNumber
     * @param trackingIds
     * @param groupNumber
     * @param signatureOption
     * @param operationalDetail
     * @return
     */
    private CompletedPackageData createCompletePackageDetails(
            Integer sequenceNumber,
            List<TrackingIdDetail> trackingIds,
            Integer groupNumber,
            String signatureOption,
            PackageOperationData operationalDetail) {

        CompletedPackageData completedPackageData = new CompletedPackageData();
        completedPackageData.setGroupNumber(groupNumber);
        completedPackageData.setSequenceNumber(sequenceNumber);
        completedPackageData.setTrackingIds(trackingIds);
        completedPackageData.setSignatureOption(signatureOption);
        completedPackageData.setOperationalDetail(operationalDetail);

        return completedPackageData;
    }

    /**
     * Method to create object of ShipmentConfirmationRequest
     * 
     * @return
     */
    private ShipmentConfirmationRequest createShipmentConfirmationRequest() {

        ShipmentConfirmationRequest request = new ShipmentConfirmationRequest();
        request.setWorkstationDetail(createWorkstationDetail());
        request.setLocationDataDetail(createLocationDataDetails());
        request.setReferenceId(referenceId);
        request.setTrackingId(trackingId);
        request.setTransactionLocationId(locationId);
        request.setShipmentTimeStamp(requestedTimeStamp);
        request.setLabelSpecificationDetail(createLabelSpecificationDetail());
        request.setPaymentDetail(createPaymentDetail());

        return request;
    }

    /**
     * Method to create LabelSpecificationDetail object
     * 
     * @return
     */
    private LabelSpecificationDetail createLabelSpecificationDetail() {

        LabelSpecificationDetail labelSpecificationDetail = new LabelSpecificationDetail();

        labelSpecificationDetail.setProcessingOptionsRequested("IGNORE_RESIDENTIAL_DELIVERY_POLICIES");
        labelSpecificationDetail.setFormatType("OPERATIONAL_LABEL");
        labelSpecificationDetail.setStockType("STOCK_4X6");
        labelSpecificationDetail.setImageType("PNG");
        labelSpecificationDetail.setPrintingOrientation("TOP_EDGE_OF_TEXT_FIRST");
        labelSpecificationDetail.setRotation("NA");
        labelSpecificationDetail.setLabelOrigin("ALVA");
        labelSpecificationDetail.setCustomerSpecifiedDetail("BUSINESS");

        return labelSpecificationDetail;
    }


    /**
     * Method to create PaymentDetail object
     * 
     * @return
     */
    private PaymentDetail createPaymentDetail() {

        PaymentDetail paymentDetail = new PaymentDetail();

        Price price = new Price();
        price.setAmount(1.1);
        price.setCurrency("USD");

        PartyDetail payor = new PartyDetail();
        payor.setAccountNumber("038000004");
        payor.setAccountType("FEDEX_EXPRESS");
        payor.setAddress(new AddressDetail());
        payor.setCreditCard(new CreditCardData());

        paymentDetail.setPaymentSource("SPOS");
        paymentDetail.setPaymentType("ACCOUNT");
        paymentDetail.setResponsibleParty("SENDER");
        paymentDetail.setAmount(price);
        paymentDetail.setPayor(payor);

        return paymentDetail;
    }

    /**
     * Method to create object of OpenShipment
     * 
     * @param referenceId
     * @param locationId
     * @param trackingId
     * @return
     */
    private OpenShipment createOpenShipment(
            String referenceId,
            String locationId,
            String trackingId) {

        // Instance of ShipmentData as JSON
        String shipmentData =
                "{\"openShipment\": {\"locationId\": \"TRLA\",\"shipmentIndex\": \"123456789\",\"carrierDetails\": {\"carrierCode\": \"FXO\",\"opco\": \"EXPRESS\"},\"serviceType\": \"PRIORITY_OVERNIGHT\",\"packagingType\": \"YOUR_PACKAGING\",\"packageCount\": 0,\"recipient\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"specialServicesRequested\": {\"specialServices\": [{\"specialServiceType\": \"SIGNATURE_OPTION\",\"specialServiceSubType\": \"\",\"displayText\": \"ASR\",\"description\": \"Adult signature\"}],\"holdAtLocationDetail\": {\"locationId\": \"DREK\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"}}},\"totalInsuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"totalWeight\": {\"units\": \"LB\",\"value\": \"3\",\"measurementType\": \"MANUAL\"},\"packageLineItems\": [{\"dimensions\": {\"length\": \"1\",\"width\": \"1\",\"height\": \"1\",\"units\": \"IN\"},\"insuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"specialHandlingDetail\": \"CUSTOMER\",\"specialServicesDetails\": [{\"specialServiceType\": \"SIGNATURE_OPTION\",\"specialServiceSubType\": \"\",\"displayText\": \"ASR\",\"description\": \"Adult signature required\"}],\"weight\": {\"units\": \"LB\",\"value\": \"2\",\"measurementType\": \"MANUAL\"},\"signatureOption\": \"ADULT\",\"trackingId\": {\"trackingNumber\": \"123456789\",\"barcode\": \"11111111111111111111111111111111\",\"sequence\": 1},\"master\": true}],\"shipper\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"customer\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"rateDetails\": {\"serviceDetails\": {\"serviceType\": \"SIGNATURE_OPTTION\",\"serviceName\": \"\",\"description\": \"ASR\"},\"deliveryDayOfWeek\": \"FRI\",\"hal\": false,\"commitDetails\": {\"commitTimestamp\": \"2020-08-21T23:59:00\",\"dayOfWeek\": \"FRI\"},\"rateDetails\": {\"currency\": \"USD\",\"ratingType\": \"ONE_RATE\",\"totalBaseCharge\": \"1.23\",\"totalRebates\": \"1.23\",\"totalNetCharges\": \"1.23\",\"totalSurcharges\": \"1.23\",\"totalTaxes\": \"1.23\",\"totalFreightDiscounts\": \"1.23\",\"totalNetFreight\": \"1.23\",\"totalNetFedExCharge\": \"1.23\",\"totalDutiesAndTaxes\": \"1.23\",\"totalAncillaryFeesAndTaxes\": \"1.23\",\"totalDutiesTaxesAndFees\": \"1.23\",\"totalNetChargeWithDutiesAndTaxes\": \"1.23\",\"surcharges\": [{\"surchargeType\": \"INSURED_VALUE\",\"description\": \"Declared value\",\"amount\": \"1.23\"}]}},\"paymentDetails\": {\"paymentType\": \"CASH\",\"paymentSource\": \"SPOS\",\"paymentAmount\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"responsibleParty\": \"SENDER\",\"payor\": {\"accountType\": \"FEDEX_EXPRESS\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"accountNumber\": \"1110093\",\"creditCard\": {\"maskedcreditCard\": \"string\",\"type\": \"VISA\",\"expirationDate\": \"string\"}}},\"labelSpecification\": {\"processingOptionsRequested\": \"string\",\"disposition\": [\"string\"],\"formatType\": \"OPERATIONAL_LABEL\",\"imageType\": \"PNG\",\"stockType\": \"STOCK_4X6\",\"printingOrientation\": \"TOP_EDGE_OF_TEXT_FIRST\",\"rotation\": \"string\",\"labelOrder\": \"string\",\"localization\": \"string\",\"labelOrigin\": \"string\",\"customerSpecifiedDetail\": \"string\"}}}";

        OpenShipment openShipment = new OpenShipment();
        openShipment.setOpenShipmentId(referenceId);
        openShipment.setTrackingNumber(trackingId);
        openShipment.setFedexLocationId(locationId);
        openShipment.setCityCenterAccountNumber("033600003");
        openShipment.setDeviceId("SSFE");
        openShipment.setFedexId("3900090");
        openShipment.setInitialAcceptanceTmStmp("2020-09-01T11:55:28.687+05:30");
        openShipment.setMeterNumber("033600003");
        openShipment.setOpCoCode("FXO");
        openShipment.setOpCoLocationId("DREK");
        openShipment.setOpenShipmentStatusCode("ship");
        openShipment.setOpenShipmentStatusTmStmp("2020-07-30T11:55:28.687+05:30");
        openShipment.setPackageAcceptTmStmp("2020-09-01T11:55:28.687+05:30");
        openShipment.setShipmentData(shipmentData);
        openShipment.setSoftwareId("SSFE");

        return openShipment;
    }

    /**
     * Method to prepare the WorkstationDetail request
     * 
     * @return
     */
    private WorkstationDetail createWorkstationDetail() {

        WorkstationDetail workstationDetail = new WorkstationDetail();
        workstationDetail.setAccountNumber("038000004");
        workstationDetail.setMeterNumber("6997006");
        workstationDetail.setSoftwareId("SSFE");
        return workstationDetail;
    }

    /**
     * Method to create object of ShipmentDetail
     * 
     * @return
     */
    private ShipmentDetail createShipmentDetail() {

        ShipmentDetail shipmentDetail = new ShipmentDetail();
        shipmentDetail.setAcceptTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentDetail.setAfterPickupFlag(false);
        shipmentDetail.setApiClientId("1234");
        shipmentDetail.setApiClientTypeName("apiClientType");
        shipmentDetail.setBillToAccountType("bill");
        shipmentDetail.setCityCenterAccountNumber("1234556");
        shipmentDetail.setChargeAmount(1.23);
        shipmentDetail.setCurrency("USD");
        shipmentDetail.setCurrentShipmentStatTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentDetail.setCurrentShipmentStatus("status");
        shipmentDetail.setCustomerAccountNumber("1234556");
        shipmentDetail.setDelCommitDate("2020-07-30T11:55:28.687+05:30");
        shipmentDetail.setDeviceId("SSFE");
        shipmentDetail.setDevicePlatformType("devicePlatformType");
        shipmentDetail.setDimensionUom("IN");
        shipmentDetail.setDiscountAmount(1.23);
        shipmentDetail.setInitialAcceptanceTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentDetail.setPaymentType("ACCOUNT");
        shipmentDetail.setTaxAmount(1.23);
        shipmentDetail.setSoftwareId("SSFE");
        shipmentDetail.setWeightUom("LB");
        shipmentDetail.setShipmentTrackingNumber("12345");
        shipmentDetail.setMeterNumber("033600003");
        shipmentDetail.setOpCoCode("FXO");
        shipmentDetail.setOpCoLocationId("DREK");
        shipmentDetail.setShipmentContactDetail(createShipmentContactDetail());
        shipmentDetail.setShipmentPackageDetails(createShipmentPackageDetails());

        return shipmentDetail;
    }

    private List<ShipmentPackageDetail> createShipmentPackageDetails() {

        List<ShipmentPackageDetail> shipmentPackageDetailsList = new ArrayList<>();
        ShipmentPackageDetail shipmentPackageDetail = new ShipmentPackageDetail();
        shipmentPackageDetail.setAcceptTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentPackageDetail.setAmount(1.2);
        shipmentPackageDetail.setBarcode("12345678");
        shipmentPackageDetail.setTrackingNumber("123456");
        shipmentPackageDetail.setShipmentPackageAdditionalServices(createShipmentPackageAdditionalServices());
        shipmentPackageDetailsList.add(shipmentPackageDetail);
        return shipmentPackageDetailsList;
    }

    private List<ShipmentPackageAdditionalService> createShipmentPackageAdditionalServices() {

        List<ShipmentPackageAdditionalService> shipmentPackageAdditionalServiceList = new ArrayList<>();
        ShipmentPackageAdditionalService shipmentPackageAdditionalService = new ShipmentPackageAdditionalService();
        shipmentPackageAdditionalService.setAcceptTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentPackageAdditionalServiceList.add(shipmentPackageAdditionalService);
        return shipmentPackageAdditionalServiceList;
    }

    /**
     * Method to create object of ShipmentContactDetail
     * 
     * @return
     */
    private ShipmentContactDetail createShipmentContactDetail() {

        ShipmentContactDetail shipmentContactDetail = new ShipmentContactDetail();
        shipmentContactDetail.setAcceptTmStmp("2020-07-30T11:55:28.687+05:30");
        shipmentContactDetail.setCustomerName("customer");
        shipmentContactDetail.setSenderName("sender");
        shipmentContactDetail.setRecipientName("recipient");

        return shipmentContactDetail;
    }

    /**
     * Method to create object of ConfirmedShipmentData
     * 
     * @return
     */
    private ConfirmedShipmentData createConfirmedShipmentData(
            ShipmentDetail shipmentDetail) {

        ConfirmedShipmentData data = new ConfirmedShipmentData();
        data.setCarrierCode(CarrierCodeType.FDXE.name());
        data.setCarrierType(shipmentDetail.getOpCoCode());
        data.setPackageCount(shipmentDetail.getTotalPackageQuantity());
        data.setPackageLineItems(
                createPackageLineItemsAndMasterTrkngId(shipmentDetail.getShipmentPackageDetails(), data));
        data.setShipmentStatus(TransactionStatus.CREATED);
        return data;
    }

    /**
     * Build package line items for confirmed shipment response data
     * 
     * @param shipmentPackageDetails
     * @param data
     * @return
     */
    private List<ConfirmedPackageData> createPackageLineItemsAndMasterTrkngId(
            List<ShipmentPackageDetail> shipmentPackageDetails,
            ConfirmedShipmentData data) {

        List<ConfirmedPackageData> packageDetails = new ArrayList<>();
        ConfirmedPackageData packageData = null;
        TrackingSequenceDetail trackingSequence = null;

        for (ShipmentPackageDetail shipmentPackageDetail : shipmentPackageDetails) {
            packageData = new ConfirmedPackageData();
            trackingSequence = new TrackingSequenceDetail(shipmentPackageDetail.getTrackingNumber(),
                    shipmentPackageDetail.getBarcode(), shipmentPackageDetail.getSeqNumber());
            data.setMasterTrackingId(trackingSequence);
            packageData.setMaster(true);
            packageData.setTrackingId(trackingSequence);
            packageDetails.add(packageData);
        }
        return packageDetails;
    }

    /**
     * Method to prepare the RequestedShipmentDetail request
     * 
     * @return
     */
    private RequestedShipmentDetail createRequestedShipmentDetail() {

        RequestedShipmentDetail requestedShipmentDetail = new RequestedShipmentDetail();
        List<VariationOptionDetail> variationOptionDetails = new ArrayList<>();
        requestedShipmentDetail.setActions(new ArrayList<>());
        requestedShipmentDetail.setVariationOptions(variationOptionDetails);
        requestedShipmentDetail.setShippingChargesPayment(createShipingPaymentDetail());
        requestedShipmentDetail.setShipTimestamp("2020-08-31T16:59:10.273Z");

        return requestedShipmentDetail;
    }
    
    /**
     * Method to prepare the PaymentDetail
     * 
     * @return
     */
    private PaymentDetail createShipingPaymentDetail() {

        PaymentDetail paymentDetail = new PaymentDetail();
        paymentDetail.setPaymentType("ACCOUNT");
        paymentDetail.setResponsibleParty("SENDER");
        paymentDetail.setPayor(new PartyDetail());

        return paymentDetail;
    }

    /*
     * Creates transaction header details
     * 
     * @return transactionHeaderDetail
     */
    private TransactionHeaderDetail getTransactionHeaderDetails() {

        return new TransactionHeaderDetail("", "2020-09-10T10:59:10.273Z", "20172", new DeviceDetail());
    }

    /**
     * Prepare address object.
     * 
     * @return
     */
    public AddressDetail getAddress() {

        AddressDetail address = new AddressDetail(new ArrayList<String>(), "Dallas", "TX", "75024", "US", true);
        return address;
    }
    
    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint and it is executed successfully with Express After Pickup as true
     */
    @Test
    public void testConfirmShipmentWdExpressAfterPickup() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        CompleteShipmentDetail completeShipmentDetail = createReply();
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(shipServiceClient.confirmOpenShipment(shipmentConfirmationRequest)).thenReturn(completeShipmentDetail);
        when(mapper.buildResponse(completeShipmentDetail)).thenReturn(createResponse(completeShipmentDetail));

        ConfirmedShipmentData actual = shipmentCreationService.confirmShipment(shipmentConfirmationRequest);

        assertThat(actual, notNullValue());
        assertThat(actual.getMasterTrackingId(), notNullValue());
        assertThat(actual.getCarrierType(), notNullValue());
        assertThat(actual.getCarrierCode(), notNullValue());
        assertThat(actual.getPackageCount(), notNullValue());
        assertThat(actual.getPackageLineItems(), notNullValue());
        assertThat(shipmentConfirmationRequest.getShipmentTimeStamp(), equalTo("2020-09-10T10:59:10.273Z"));
    }
    
    /**
     * Test case to validate that when request is made to
     * {@link ConfirmShipmentTransactionServiceImpl#confirmShipment(ShipmentConfirmationRequest)}
     * endpoint with shipment gets stored in the db successfully
     * 
     * @throws HystrixException
     */
    @Test
    public void testConfirmShipmentHystrixException() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        shipmentConfirmationRequest.getPaymentDetail()
                .setPaymentType("CASH");

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(mapper.createRequestedShipmentDetail(shipmentConfirmationRequest, openShipment))
                .thenReturn(createRequestedShipmentDetail());
        when(shipServiceClient.modifyOpenShipment(any(RequestedShipmentDetail.class), any(WorkstationDetail.class)))
                .thenReturn(createReply());
        when(mapper.buildResponse(any(CompleteShipmentDetail.class)))
                .thenReturn(createConfirmedShipmentData(createShipmentDetail()));
        when(openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData())).thenReturn(createOpenShipmentData());
        when(mapper.buildPESRequest(any(ShipmentConfirmationRequest.class), any(OpenShipment.class),
                any(ConfirmedShipmentData.class), any(CompleteShipmentDetail.class), any(LocationDataDetail.class),
                any(OpenShipmentData.class))).thenReturn(new PackagePossessionEventDetailRequest());
        when(packageEventServiceClient.postPackagePossessionEvents(any(PackagePossessionEventDetailRequest.class)))
                .thenThrow(HystrixRuntimeException.class);
        try {
            shipmentCreationService.confirmShipment(shipmentConfirmationRequest);
        } catch (ServiceProcessorTerminalException ex) {
            assertThat(ServiceErrorCode.PES_SERVICE_UNAVAILABLE.getMessage(), equalTo(ex.getMessage()));
        }
    }
    
    /**
     * Prepare PackageCancellationEventDetailRequest object.
     * 
     * @return
     */
    public PackageCancellationEventDetailRequest getPackageCancellationEventDetailRequest() {

        PackageCancellationEventDetailRequest packageCancellationEventDetailRequest =
                new PackageCancellationEventDetailRequest();
        packageCancellationEventDetailRequest.setHeaderDetails(getTransactionHeaderDetails());
        packageCancellationEventDetailRequest.setLocationDetails(getLocationDataDetail());
        packageCancellationEventDetailRequest.setPackageLineItems(getPackageLineItemDetailList());

        return packageCancellationEventDetailRequest;
    }
    
    /**
     * Prepare LocationDataDetail object.
     * 
     * @return
     */
    public LocationDataDetail getLocationDataDetail() {

        LocationDataDetail locationDataDetail = new LocationDataDetail();
        locationDataDetail.setCityCenterAccountNumber("12345");
        locationDataDetail.setGroundAccountNumber("123456789");
        locationDataDetail.setExpressAfterPickup(true);
        locationDataDetail.setGroundAfterPickup(true);
        locationDataDetail.setOpCo("FDXE");
        locationDataDetail.setOpCoLocationId("DFWD");
        locationDataDetail.setTransactionLocationInfo(new Location("DFWD", getAddress()));
        locationDataDetail.setServingLocationInfo(new Location("DFWD", getAddress()));

        return locationDataDetail;
    }
    
    /**
     * Prepare List<PackageLineItemDetail> object.
     * 
     * @return
     */
    public List<PackageLineItemDetail> getPackageLineItemDetailList() {

        List<PackageLineItemDetail> packageLineItemDetailList = new ArrayList<>();
        PackageLineItemDetail packageLineItemDetail = new PackageLineItemDetail();
        packageLineItemDetail.setPackageType("YOUR_PACKAGING");
        packageLineItemDetail.setPackageIdentifier(getTrackingSequenceDetail());
        packageLineItemDetail.setCustomerName(new PersonDetail("Mr.", "Customer"));
        packageLineItemDetail.setChildPackages(getTrackingSequenceDetailList());
        packageLineItemDetailList.add(packageLineItemDetail);

        return packageLineItemDetailList;

    }
    
    /**
     * Prepare TrackingSequenceDetail object.
     * 
     * @return
     */
    public TrackingSequenceDetail getTrackingSequenceDetail()
    {
        TrackingSequenceDetail trackingSequenceDetail = new TrackingSequenceDetail();
        trackingSequenceDetail.setTrackingNumber("794973860000");
        trackingSequenceDetail.setBarcode("1234567890");
        trackingSequenceDetail.setSequence(1);
        
        return trackingSequenceDetail;
    }
    
    /**
     * Prepare TrackingSequenceDetail list object.
     * 
     * @return
     */
    public List<TrackingSequenceDetail> getTrackingSequenceDetailList()
    {
        List<TrackingSequenceDetail> trackingSequenceDetailList = new ArrayList<>();
        trackingSequenceDetailList.add(getTrackingSequenceDetail());
        
        return trackingSequenceDetailList;
    }
    
    /**
     * Prepare UpdateOpenShipmentStatusData object.
     * 
     * @return
     */
    public UpdateOpenShipmentStatusData getUpdateOpenShipmentStatusData()
    {
        UpdateOpenShipmentStatusData updateOpenShipmentStatusData = new UpdateOpenShipmentStatusData();
        updateOpenShipmentStatusData.setTrackingId("794973860000");
        updateOpenShipmentStatusData.setReferenceId("794973860000");
        updateOpenShipmentStatusData.setTeamMemberId("123456");
        updateOpenShipmentStatusData.setLocationId("LASA");
        updateOpenShipmentStatusData.setOpenShipmentStatusCode("RETURNED");
        
        return updateOpenShipmentStatusData;
    }

    /**
     * Prepare DeleteSoldShipmentData object.
     * 
     * @return
     */
    private DeleteSoldShipmentData createDeleteSoldShipmentData() {

        return new DeleteSoldShipmentData(trackingId, referenceId, locationId, requestedTimeStamp,
                createWorkstationDetail(), new LocationDataDetail(), new PersonDetail("Mr.", "Customer"));
    }
    
    /**
     * Method to create OpenShipmentData object
     * 
     * @return
     */
    private OpenShipmentData createOpenShipmentData() {

        OpenShipmentData openShpmtData = new OpenShipmentData();
        OpenShipmentDetail openShipmentDetail = new OpenShipmentDetail();
        openShipmentDetail.setLabelSpecification(createLabelSpecificationDetail());
        openShipmentDetail.setSpecialServicesRequested(createShipmentSpecialServiceDetail());
        openShpmtData.setOpenShipment(openShipmentDetail);
        openShpmtData.getOpenShipment().setPaymentDetails(new PaymentDetail(PaymentType.NON_ACCOUNT.name(), null, null, null, null));
        return openShpmtData;
    }
    
    /**
     * Method to create ShipmentSpecialServiceDetail request
     * 
     * @return ShipmentSpecialServiceDetail
     */
    private ShipmentSpecialServiceDetail createShipmentSpecialServiceDetail() {

        ShipmentSpecialServiceDetail shipmentSpecialServiceDetail = new ShipmentSpecialServiceDetail();
        List<SpecialServiceDescriptionDetail> listSpecialServicesRequestedType = new ArrayList<>();
        SpecialServiceDescriptionDetail specialServiceDescriptionDetail = new SpecialServiceDescriptionDetail();
        specialServiceDescriptionDetail.setSpecialServiceType("FEDEX_ONE_RATE");
        listSpecialServicesRequestedType.add(specialServiceDescriptionDetail);
        shipmentSpecialServiceDetail.setSpecialServicesRequestedType(listSpecialServicesRequestedType);
        return shipmentSpecialServiceDetail;
    }

    /**
     * This method creates location data details
     * 
     * @return locationDataDetail
     */
    private LocationDataDetail createLocationDataDetails() {

        LocationDataDetail locationDataDetail = new LocationDataDetail();
        locationDataDetail.setOpCoLocationId("opCoLocationId");
        locationDataDetail.setExpressAfterPickup(true);
        locationDataDetail.setCityCenterAccountNumber("123456");
        locationDataDetail.setGroundAccountNumber("987654");
        locationDataDetail.setOpCo("opCo");

        return locationDataDetail;
    }

}
