package com.fedex.rscs.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixException;
import com.netflix.hystrix.exception.HystrixRuntimeException;

import com.fedex.common.cxs.exception.TerminalDbAccessException;
import com.fedex.common.cxs.exception.TransientDbAccessException;
import com.fedex.rscs.client.cshp.ShipServiceClient;
import com.fedex.rscs.client.rsss.RSSSClient;
import com.fedex.rscs.client.srrs.SRRSClient;
import com.fedex.rscs.dto.InterlineShippingDetail;
import com.fedex.rscs.dto.PaymentType;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.ServiceProcessorTerminalException;
import com.fedex.rscs.exception.client.ClientTerminalException;
import com.fedex.rscs.exception.client.ClientTransientException;
import com.fedex.rscs.model.CommitInfo;
import com.fedex.rscs.model.InterlineShippingData;
import com.fedex.rscs.model.LabelSpecificationDetail;
import com.fedex.rscs.model.OpenShipment;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.PaymentDetail;
import com.fedex.rscs.model.RequestedPackageLineItemDetail;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ServiceDescriptionDetail;
import com.fedex.rscs.model.SurchargeDetail;
import com.fedex.rscs.model.VariationOptionDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.common.CurrencyType;
import com.fedex.rscs.model.common.PackagePreauthorizedRatedDetail;
import com.fedex.rscs.model.common.PreauthorizedRateCommitDetail;
import com.fedex.rscs.model.common.PreauthorizedRateServiceDetail;
import com.fedex.rscs.model.common.Price;
import com.fedex.rscs.model.common.ShipmentPreauthorizedRatedDetail;
import com.fedex.rscs.model.common.ShippingRateSurcharge;
import com.fedex.rscs.model.cshp.CompleteShipmentDetail;
import com.fedex.rscs.model.cshp.CompletedPackageData;
import com.fedex.rscs.model.cshp.DeleteOpenShipmentDetail;
import com.fedex.rscs.model.cshp.OpenShipmentResponse;
import com.fedex.rscs.model.cshp.PackageOperationData;
import com.fedex.rscs.model.cshp.PreauthorizedRateData;
import com.fedex.rscs.model.cshp.ShippingPaymentData;
import com.fedex.rscs.model.cshp.ShippingRateData;
import com.fedex.rscs.model.cshp.TrackingIdDetail;
import com.fedex.rscs.model.cshp.TransactionStatus;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;
import com.fedex.rscs.repository.OpenShipmentRepository;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test cases required to unit test ShipmentCreationServiceImpl class
 * 
 * @author 3798910
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class OpenShipmentTransactionServiceImplTest {

    @InjectMocks
    private OpenShipmentTransactionServiceImpl shipmentCreationService;

    @Mock
    private ShipServiceClient shipServiceClient;

    @Mock
    private OpenShipmentTransactionServiceMapper mapper;

    @Mock
    private OpenShipmentRepository openShipmentRepository;

    @Mock
    private SRRSClient srrsClient;
    
    @Mock
    private RSSSClient rsssClient;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    String referenceId;
    String locationId;
    String trackingId;
    String requestedDateTime;
    String locationCode;
    String index;

    @Before
    public void setup() {

        shipmentCreationService = new OpenShipmentTransactionServiceImpl(shipServiceClient, mapper,
                openShipmentRepository, srrsClient, rsssClient);

        referenceId = "3456";
        locationId = "LASA";
        trackingId = "794973860000";
        requestedDateTime = "23-09-2020";
        locationCode = "DFWD";
        index = "12345";
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceImpl#create(RequestedShipmentDetail,WorkstationDetail)}
     * to set the object fields and then to make a call to createOpenShipment() client method and
     * return a valid response.
     * 
     */
    @Test
    public void testCreate() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        ShippingRateData shippingRateData = createShipRateData();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        PreauthorizedRateData preauthorizedRateData = createPreAuthorizedRateData();
        String accountNumber = "038000004";
        CompleteShipmentDetail completeShipmentDetail = createReply();
        OpenShipment openShipment = new OpenShipment();
        when(mapper.createRequestedShipmentDetail(requestedShipmentDetail, accountNumber, shippingRateData))
                .thenReturn(requestedShipmentDetail);
        when(shipServiceClient.createOpenShipment(requestedShipmentDetail, workstationDetail))
                .thenReturn(completeShipmentDetail);
        when(mapper.createOpenShipmentResponse(completeShipmentDetail, requestedShipmentDetail, shippingRateData,
                preauthorizedRateData)).thenReturn(createOpenShipmentResponse());
        when(srrsClient.getPreauthorizedRates(workstationDetail, requestedShipmentDetail))
                .thenReturn(preauthorizedRateData);
        when(mapper.mapPreAuthorizedRateDataToShippingRateData(preauthorizedRateData)).thenReturn(shippingRateData);
        when(mapper.createOpenShipment(any(OpenShipmentResponse.class), any(WorkstationDetail.class),
                any(RequestedShipmentDetail.class))).thenReturn(openShipment);
        when(openShipmentRepository.save(openShipment)).thenReturn(true);

        OpenShipmentResponse openShipmentResponse =
                shipmentCreationService.create(requestedShipmentDetail, workstationDetail);
        assertThat(openShipmentResponse, notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getCompletedPackageDetails(), notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getMasterTrackingId(), notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getIndex(), notNullValue());

    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link ShipmentCreationServiceImpl#create(RequestedShipmentDetail,WorkstationDetail)} to set
     * the object fields and then to make a call to createOpenShipment() and throw
     * ShipmentProcessorTerminalException.
     * 
     * @throws ClientTransientException
     * 
     */
    @Test
    public void testCreateTransientException() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        ShippingRateData shippingRateData = createShipRateData();
        PreauthorizedRateData preauthorizedRateData = createPreAuthorizedRateData();
        String accountNumber = "038000004";
        ClientTransientException clientTransientException =
                new ClientTransientException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);
        when(mapper.createRequestedShipmentDetail(requestedShipmentDetail, accountNumber, shippingRateData))
                .thenReturn(requestedShipmentDetail);
        when(srrsClient.getPreauthorizedRates(workstationDetail, requestedShipmentDetail))
                .thenReturn(preauthorizedRateData);
        when(mapper.mapPreAuthorizedRateDataToShippingRateData(preauthorizedRateData)).thenReturn(shippingRateData);
        doThrow(clientTransientException).when(shipServiceClient)
                .createOpenShipment(requestedShipmentDetail, workstationDetail);
        try {
            shipmentCreationService.create(requestedShipmentDetail, workstationDetail);
        } catch (ServiceProcessorTerminalException e) {
            assertEquals(clientTransientException.getErrorCode(), e.getErrorCode());
        }
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link ShipmentCreationServiceImpl#create(RequestedShipmentDetail,WorkstationDetail)} to set
     * the object fields and then to make a call to createOpenShipment() and throw
     * ShipmentProcessorTerminalException.
     * 
     * @throws ClientTerminalException
     * 
     */
    @Test
    public void testCreateTerminalException() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        ShippingRateData shippingRateData = createShipRateData();
        PreauthorizedRateData preauthorizedRateData = createPreAuthorizedRateData();
        String accountNumber = "038000004";
        ClientTerminalException clientTerminalException =
                new ClientTerminalException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);
        when(mapper.createRequestedShipmentDetail(requestedShipmentDetail, accountNumber, shippingRateData))
                .thenReturn(requestedShipmentDetail);
        when(srrsClient.getPreauthorizedRates(workstationDetail, requestedShipmentDetail))
                .thenReturn(preauthorizedRateData);
        when(mapper.mapPreAuthorizedRateDataToShippingRateData(preauthorizedRateData)).thenReturn(shippingRateData);
        doThrow(clientTerminalException).when(shipServiceClient)
                .createOpenShipment(requestedShipmentDetail, workstationDetail);
        try {
            shipmentCreationService.create(requestedShipmentDetail, workstationDetail);
        } catch (ServiceProcessorTerminalException e) {
            assertEquals(clientTerminalException.getErrorCode(), e.getErrorCode());
        }
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link ShipmentCreationServiceImpl#create(RequestedShipmentDetail,WorkstationDetail)} to set
     * the object fields and then to make a call to getRateOptions() and throw
     * ShipmentProcessorTerminalException.
     * 
     * @throws ClientTerminalException
     * 
     */
    @Test
    public void testCreateTerminalExceptionForRate() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        ClientTerminalException clientTerminalException =
                new ClientTerminalException(ServiceErrorCode.RATING_SERVICE_UNAVAILABLE);
        doThrow(clientTerminalException).when(srrsClient)
                .getPreauthorizedRates(workstationDetail, requestedShipmentDetail);
        try {
            shipmentCreationService.create(requestedShipmentDetail, workstationDetail);
        } catch (ServiceProcessorTerminalException e) {
            assertEquals(clientTerminalException.getErrorCode(), e.getErrorCode());
        }
    }

    /**
     * Test case to validate that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#retrieveOpenShipment(String, String, String)}
     * endpoint it is executed successfully
     */
    @Test
    public void testRetrieveOpenShipment() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);
        when(mapper.retrieveOpenShipmentResponse(openShipment)).thenReturn(createOpenShipmentResponse());

        OpenShipmentResponse response =
                shipmentCreationService.retrieveOpenShipment(referenceId, locationId, trackingId);

        assertThat(response, notNullValue());
        assertThat(response.getCompleteShipmentDetail(), notNullValue());
        assertThat(response.getShippingPaymentData(), notNullValue());
        assertThat(response.getShippingRateData(), notNullValue());
    }

    /**
     * Test case to validate that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#retrieveOpenShipment(String, String, String)}
     * endpoint it throws TerminalDbAccessException and it's handled successfully
     */
    @Test
    public void testRetrieveOpenShipmentTerminalDbAccessException() {

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId))
                .thenThrow(TerminalDbAccessException.class);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_SEARCH_FAILURE.getMessage()));

        shipmentCreationService.retrieveOpenShipment(referenceId, locationId, trackingId);
    }

    /**
     * Test case to validate that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#retrieveOpenShipment(String, String, String)}
     * endpoint it throws TransientDbAccessException and it's handled successfully
     */
    @Test
    public void testRetrieveOpenShipmentTransientDbAccessException() {

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId))
                .thenThrow(TransientDbAccessException.class);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_SEARCH_FAILURE.getMessage()));

        shipmentCreationService.retrieveOpenShipment(referenceId, locationId, trackingId);
    }

    /**
     * Test case to validate that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#retrieveOpenShipment(String, String, String)}
     * endpoint it throws HystrixRuntimeException and it's handled successfully
     */
    @Test
    public void testRetrieveOpenShipmentHystrixRuntimeException() {

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId))
                .thenThrow(HystrixRuntimeException.class);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.SYSTEM_UNAVAILABLE.getMessage()));

        shipmentCreationService.retrieveOpenShipment(referenceId, locationId, trackingId);
    }

    /**
     * Test case to validate that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#deleteOpenShipment(String, String, String, WorkstationDetail)}
     * endpoint it is executed successfully
     */
    @Test
    public void testDeleteOpenShipmentClient() {

        doNothing().when(shipServiceClient)
                .deleteOpenShipment(createDeleteOpenShipmentDetail());
        when(mapper.deleteOpenShipmentRequest(requestedDateTime, locationCode, index, createWorkstationDetail()))
                .thenReturn(createDeleteOpenShipmentDetail());

        shipmentCreationService.deleteOpenShipmentClient(requestedDateTime, locationCode, index,
                createWorkstationDetail());

        verify(shipServiceClient, times(1)).deleteOpenShipment(createDeleteOpenShipmentDetail());
    }

    /**
     * Test case to validate that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#deleteOpenShipmentClient(String, String, String, WorkstationDetail)}
     * endpoint it throws ClientTerminalException and it's handled successfully
     */
    @Test
    public void testDeleteOpenShipmentClientTerminalException() {

        doThrow(ClientTerminalException.class).when(shipServiceClient)
                .deleteOpenShipment(createDeleteOpenShipmentDetail());
        when(mapper.deleteOpenShipmentRequest(requestedDateTime, locationCode, index, createWorkstationDetail()))
                .thenReturn(createDeleteOpenShipmentDetail());

        shipmentCreationService.deleteOpenShipmentClient(requestedDateTime, locationCode, index,
                createWorkstationDetail());

        verify(shipServiceClient, times(1)).deleteOpenShipment(createDeleteOpenShipmentDetail());
    }

    /**
     * Test case to validate that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#deleteOpenShipmentClient(String, String, String, WorkstationDetail)}
     * endpoint it throws ClientTransientException and it's handled successfully
     */
    @Test
    public void testDeleteOpenShipmentClientTransientException() {

        doThrow(ClientTransientException.class).when(shipServiceClient)
                .deleteOpenShipment(createDeleteOpenShipmentDetail());
        when(mapper.deleteOpenShipmentRequest(requestedDateTime, locationCode, index, createWorkstationDetail()))
                .thenReturn(createDeleteOpenShipmentDetail());

        shipmentCreationService.deleteOpenShipmentClient(requestedDateTime, locationCode, index,
                createWorkstationDetail());

        verify(shipServiceClient, times(1)).deleteOpenShipment(createDeleteOpenShipmentDetail());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceImpl#create(RequestedShipmentDetail,WorkstationDetail)}
     * to set the object fields and then to make a call to createOpenShipment() client method and
     * return a valid response.
     * 
     */
    @Test
    public void testCreateRepositoryCallFails() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        ShippingRateData shippingRateData = createShipRateData();
        PreauthorizedRateData preauthorizedRateData = createPreAuthorizedRateData();
        String accountNumber = "038000004";
        CompleteShipmentDetail completeShipmentDetail = createReply();
        OpenShipment openShipment = new OpenShipment();

        when(srrsClient.getPreauthorizedRates(workstationDetail, requestedShipmentDetail))
                .thenReturn(preauthorizedRateData);
        when(mapper.mapPreAuthorizedRateDataToShippingRateData(preauthorizedRateData)).thenReturn(shippingRateData);
        when(mapper.createRequestedShipmentDetail(requestedShipmentDetail, accountNumber, shippingRateData))
                .thenReturn(requestedShipmentDetail);
        when(shipServiceClient.createOpenShipment(requestedShipmentDetail, workstationDetail))
                .thenReturn(completeShipmentDetail);
        when(mapper.createOpenShipmentResponse(completeShipmentDetail, requestedShipmentDetail, shippingRateData,
                preauthorizedRateData)).thenReturn(createOpenShipmentResponse());
        when(mapper.createOpenShipment(any(OpenShipmentResponse.class), any(WorkstationDetail.class),
                any(RequestedShipmentDetail.class))).thenReturn(openShipment);
        when(openShipmentRepository.save(any(OpenShipment.class))).thenReturn(false);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_INSERTION_FAILURE.getMessage()));

        shipmentCreationService.create(requestedShipmentDetail, workstationDetail);

        verify(srrsClient, times(1)).getRateOptions(workstationDetail, requestedShipmentDetail);
        verify(mapper, times(1)).createRequestedShipmentDetail(requestedShipmentDetail, accountNumber,
                shippingRateData);
        verify(shipServiceClient, times(1)).createOpenShipment(requestedShipmentDetail, workstationDetail);
        verify(mapper, times(1)).createOpenShipmentResponse(completeShipmentDetail, requestedShipmentDetail,
                shippingRateData, preauthorizedRateData);
        verify(mapper, times(1)).createOpenShipment(any(OpenShipmentResponse.class), any(WorkstationDetail.class),
                any(RequestedShipmentDetail.class));
        verify(openShipmentRepository, times(1)).save(openShipment);
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceImpl#create(RequestedShipmentDetail,WorkstationDetail)}
     * to set the object fields and then to make a call to createOpenShipment() client method and
     * then make the call to repository method and throw exception of TransientDbAcessException .
     * 
     */
    @Test
    public void testCreateRepositoryException() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        ShippingRateData shippingRateData = createShipRateData();
        PreauthorizedRateData preauthorizedRateData = createPreAuthorizedRateData();
        String accountNumber = "038000004";
        CompleteShipmentDetail completeShipmentDetail = createReply();
        OpenShipment openShipment = new OpenShipment();

        when(srrsClient.getPreauthorizedRates(workstationDetail, requestedShipmentDetail))
                .thenReturn(preauthorizedRateData);
        when(mapper.mapPreAuthorizedRateDataToShippingRateData(preauthorizedRateData)).thenReturn(shippingRateData);
        when(mapper.createRequestedShipmentDetail(requestedShipmentDetail, accountNumber, shippingRateData))
                .thenReturn(requestedShipmentDetail);
        when(shipServiceClient.createOpenShipment(requestedShipmentDetail, workstationDetail))
                .thenReturn(completeShipmentDetail);
        when(mapper.createOpenShipmentResponse(completeShipmentDetail, requestedShipmentDetail, shippingRateData,
                preauthorizedRateData)).thenReturn(createOpenShipmentResponse());
        when(mapper.createOpenShipment(any(OpenShipmentResponse.class), any(WorkstationDetail.class),
                any(RequestedShipmentDetail.class))).thenReturn(openShipment);
        when(openShipmentRepository.save(openShipment)).thenThrow(TransientDbAccessException.class);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_INSERTION_FAILURE.getMessage()));

        shipmentCreationService.create(requestedShipmentDetail, workstationDetail);

        verify(srrsClient, times(1)).getRateOptions(workstationDetail, requestedShipmentDetail);
        verify(mapper, times(1)).createRequestedShipmentDetail(requestedShipmentDetail, accountNumber,
                shippingRateData);
        verify(shipServiceClient, times(1)).createOpenShipment(requestedShipmentDetail, workstationDetail);
        verify(mapper, times(1)).createOpenShipmentResponse(completeShipmentDetail, requestedShipmentDetail,
                shippingRateData, preauthorizedRateData);
        verify(mapper, times(1)).createOpenShipment(any(OpenShipmentResponse.class), any(WorkstationDetail.class),
                any(RequestedShipmentDetail.class));
        verify(openShipmentRepository, times(1)).save(openShipment);

    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceImpl#create(RequestedShipmentDetail,WorkstationDetail)}
     * to set the object fields and then to make a call to createOpenShipment() client method and
     * then make the call to repository method and throw exception of HystrixRunTimeException .
     * 
     */
    @Test
    public void testCreateRepositoryHystrixException() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        ShippingRateData shippingRateData = createShipRateData();
        PreauthorizedRateData preauthorizedRateData = createPreAuthorizedRateData();
        String accountNumber = "038000004";
        CompleteShipmentDetail completeShipmentDetail = createReply();
        OpenShipment openShipment = new OpenShipment();

        when(srrsClient.getPreauthorizedRates(workstationDetail, requestedShipmentDetail))
                .thenReturn(preauthorizedRateData);
        when(mapper.mapPreAuthorizedRateDataToShippingRateData(preauthorizedRateData)).thenReturn(shippingRateData);
        when(mapper.createRequestedShipmentDetail(requestedShipmentDetail, accountNumber, shippingRateData))
                .thenReturn(requestedShipmentDetail);
        when(shipServiceClient.createOpenShipment(requestedShipmentDetail, workstationDetail))
                .thenReturn(completeShipmentDetail);
        when(mapper.createOpenShipmentResponse(completeShipmentDetail, requestedShipmentDetail, shippingRateData,
                preauthorizedRateData)).thenReturn(createOpenShipmentResponse());
        when(mapper.createOpenShipment(any(OpenShipmentResponse.class), any(WorkstationDetail.class),
                any(RequestedShipmentDetail.class))).thenReturn(openShipment);
        when(openShipmentRepository.save(openShipment)).thenThrow(HystrixRuntimeException.class);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.SYSTEM_UNAVAILABLE.getMessage()));

        shipmentCreationService.create(requestedShipmentDetail, workstationDetail);

        verify(srrsClient, times(1)).getRateOptions(workstationDetail, requestedShipmentDetail);
        verify(mapper, times(1)).createRequestedShipmentDetail(requestedShipmentDetail, accountNumber,
                shippingRateData);
        verify(shipServiceClient, times(1)).createOpenShipment(requestedShipmentDetail, workstationDetail);
        verify(mapper, times(1)).createOpenShipmentResponse(completeShipmentDetail, requestedShipmentDetail,
                shippingRateData, preauthorizedRateData);
        verify(mapper, times(1)).createOpenShipment(any(OpenShipmentResponse.class), any(WorkstationDetail.class),
                any(RequestedShipmentDetail.class));
        verify(openShipmentRepository, times(1)).save(openShipment);
    }

    /**
     * Method to create object of OpenShipment
     * 
     * @param referenceId
     * @param locationId
     * @param trackingId
     * @return
     */
    private OpenShipment createOpenShipment(
            String referenceId,
            String locationId,
            String trackingId) {

        // Instance of ShipmentData as JSON
        String shipmentData =
                "{\"openShipment\": {\"locationId\": \"TRLA\",\"shipmentIndex\": \"123456789\",\"carrierDetails\": {\"carrierCode\": \"FXO\",\"opco\": \"EXPRESS\"},\"serviceType\": \"PRIORITY_OVERNIGHT\",\"packagingType\": \"YOUR_PACKAGING\",\"packageCount\": 0,\"recipient\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"specialServicesRequested\": {\"specialServices\": [{\"specialServiceType\": \"SIGNATURE_OPTION\",\"specialServiceSubType\": \"\",\"displayText\": \"ASR\",\"description\": \"Adult signature\"}],\"holdAtLocationDetail\": {\"locationId\": \"DREK\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"}}},\"totalInsuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"totalWeight\": {\"units\": \"LB\",\"value\": \"3\",\"measurementType\": \"MANUAL\"},\"packageLineItems\": [{\"dimensions\": {\"length\": \"1\",\"width\": \"1\",\"height\": \"1\",\"units\": \"IN\"},\"insuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"specialHandlingDetail\": \"CUSTOMER\",\"specialServicesDetails\": [{\"specialServiceType\": \"SIGNATURE_OPTION\",\"specialServiceSubType\": \"\",\"displayText\": \"ASR\",\"description\": \"Adult signature required\"}],\"weight\": {\"units\": \"LB\",\"value\": \"2\",\"measurementType\": \"MANUAL\"},\"signatureOption\": \"ADULT\",\"trackingId\": {\"trackingNumber\": \"123456789\",\"barcode\": \"11111111111111111111111111111111\",\"sequence\": 1},\"master\": true}],\"shipper\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"customer\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"rateDetails\": {\"serviceDetails\": {\"serviceType\": \"SIGNATURE_OPTTION\",\"serviceName\": \"\",\"description\": \"ASR\"},\"deliveryDayOfWeek\": \"FRI\",\"hal\": false,\"commitDetails\": {\"commitTimestamp\": \"2020-08-21T23:59:00\",\"dayOfWeek\": \"FRI\"},\"rateDetails\": {\"currency\": \"USD\",\"ratingType\": \"ONE_RATE\",\"totalBaseCharge\": \"1.23\",\"totalRebates\": \"1.23\",\"totalNetCharges\": \"1.23\",\"totalSurcharges\": \"1.23\",\"totalTaxes\": \"1.23\",\"totalFreightDiscounts\": \"1.23\",\"totalNetFreight\": \"1.23\",\"totalNetFedExCharge\": \"1.23\",\"totalDutiesAndTaxes\": \"1.23\",\"totalAncillaryFeesAndTaxes\": \"1.23\",\"totalDutiesTaxesAndFees\": \"1.23\",\"totalNetChargeWithDutiesAndTaxes\": \"1.23\",\"surcharges\": [{\"surchargeType\": \"INSURED_VALUE\",\"description\": \"Declared value\",\"amount\": \"1.23\"}]}},\"paymentDetails\": {\"paymentType\": \"CASH\",\"paymentSource\": \"SPOS\",\"paymentAmount\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"responsibleParty\": \"SENDER\",\"payor\": {\"accountType\": \"FEDEX_EXPRESS\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"accountNumber\": \"1110093\",\"creditCard\": {\"maskedcreditCard\": \"string\",\"type\": \"VISA\",\"expirationDate\": \"string\"}}},\"labelSpecification\": {\"processingOptionsRequested\": \"string\",\"disposition\": [\"string\"],\"formatType\": \"OPERATIONAL_LABEL\",\"imageType\": \"PNG\",\"stockType\": \"STOCK_4X6\",\"printingOrientation\": \"TOP_EDGE_OF_TEXT_FIRST\",\"rotation\": \"string\",\"labelOrder\": \"string\",\"localization\": \"string\",\"labelOrigin\": \"string\",\"customerSpecifiedDetail\": \"string\"}}}";

        OpenShipment openShipment = new OpenShipment();
        openShipment.setOpenShipmentId(referenceId);
        openShipment.setTrackingNumber(trackingId);
        openShipment.setFedexLocationId(locationId);
        openShipment.setCityCenterAccountNumber("033600003");
        openShipment.setDeviceId("SSFE");
        openShipment.setFedexId("3900090");
        openShipment.setInitialAcceptanceTmStmp("2020-09-01T11:55:28.687+05:30");
        openShipment.setMeterNumber("033600003");
        openShipment.setOpCoCode("FXO");
        openShipment.setOpCoLocationId("DREK");
        openShipment.setOpenShipmentStatusCode("CREATED");
        openShipment.setOpenShipmentStatusTmStmp("2020-07-30T11:55:28.687+05:30");
        openShipment.setPackageAcceptTmStmp("2020-09-01T11:55:28.687+05:30");
        openShipment.setShipmentData(shipmentData);
        openShipment.setSoftwareId("SSFE");

        return openShipment;
    }

    /**
     * Method to prepare the WorkstationDetail request
     * 
     * @return
     */
    private WorkstationDetail createWorkstationDetail() {

        WorkstationDetail workstationDetail = new WorkstationDetail();
        workstationDetail.setAccountNumber("038000004");
        workstationDetail.setMeterNumber("6997006");
        workstationDetail.setSoftwareId("SSFE");
        return workstationDetail;
    }

    /**
     * Method to prepare the reply of CompleteShipmentDetail
     * 
     * @return
     */
    private CompleteShipmentDetail createReply() {

        CompleteShipmentDetail completeShipmentDetail = new CompleteShipmentDetail();
        TrackingIdDetail masterTrackingId = new TrackingIdDetail();
        List<CompletedPackageData> completedPackageDetails = new ArrayList<>();

        masterTrackingId.setTrackingIdType("794978662786");
        masterTrackingId.setTrackingIdType("FEDEX");
        List<TrackingIdDetail> trackingIds = new ArrayList<>();
        trackingIds.add(masterTrackingId);
        completedPackageDetails
                .add(createCompletePackageDetails(1, trackingIds, 0, "SERVICE_DEFAULT", new PackageOperationData()));
        completeShipmentDetail.setCompletedPackageDetails(completedPackageDetails);
        completeShipmentDetail.setMasterTrackingId(masterTrackingId);
        completeShipmentDetail.setUsDomestic(true);
        completeShipmentDetail.setIndex("12345");

        return completeShipmentDetail;
    }

    /**
     * Method to prepare the response object of CompletedPackageData
     * 
     * @param sequenceNumber
     * @param trackingIds
     * @param groupNumber
     * @param signatureOption
     * @param operationalDetail
     * @return
     */
    private CompletedPackageData createCompletePackageDetails(
            Integer sequenceNumber,
            List<TrackingIdDetail> trackingIds,
            Integer groupNumber,
            String signatureOption,
            PackageOperationData operationalDetail) {

        CompletedPackageData completedPackageData = new CompletedPackageData();
        completedPackageData.setGroupNumber(groupNumber);
        completedPackageData.setSequenceNumber(sequenceNumber);
        completedPackageData.setTrackingIds(trackingIds);
        completedPackageData.setSignatureOption(signatureOption);
        completedPackageData.setOperationalDetail(operationalDetail);
        return completedPackageData;

    }

    /**
     * Method to prepare the RequestedShipmentDetail request
     * 
     * @return
     */
    private RequestedShipmentDetail createRequestedShipmentDetail() {

        RequestedShipmentDetail requestedShipmentDetail = new RequestedShipmentDetail();
        List<VariationOptionDetail> variationOptionDetails = new ArrayList<>();
        List<RequestedPackageLineItemDetail> packageLineItems = new ArrayList<>();
        RequestedPackageLineItemDetail packageLineItem = new RequestedPackageLineItemDetail();
        packageLineItem.setSequenceNumber(1);
        packageLineItems.add(packageLineItem);
        requestedShipmentDetail.setShipTimestamp("23-09-2020");
        requestedShipmentDetail.setTransactionLocationCode("DFWD");
        requestedShipmentDetail.setActions(new ArrayList<>());
        requestedShipmentDetail.setVariationOptions(variationOptionDetails);
        requestedShipmentDetail.setLabelSpecification(new LabelSpecificationDetail());
        requestedShipmentDetail.setPackageLineItems(packageLineItems);
        requestedShipmentDetail.setInterlineShippingData(null);
        requestedShipmentDetail.setShippingChargesPayment(createMockShipingPaymentDetail());;

        return requestedShipmentDetail;
    }

    /**
     * Method to prepare OpenShipmentResponse object
     * 
     * @return
     */
    private OpenShipmentResponse createOpenShipmentResponse() {

        OpenShipmentResponse openShipmentResponse = new OpenShipmentResponse();
        ShippingPaymentData shippingPaymentData = new ShippingPaymentData();
        ShippingRateData shippingRateData = new ShippingRateData();
        CompleteShipmentDetail completeShipmentDetail = createReply();
        openShipmentResponse.setCompleteShipmentDetail(completeShipmentDetail);
        openShipmentResponse.setShippingPaymentData(shippingPaymentData);
        openShipmentResponse.setShippingRateData(shippingRateData);

        return openShipmentResponse;
    }

    /**
     * Method to create DeleteOpenShipmentDetail
     * 
     * @return DeleteOpenShipmentDetail
     */
    private DeleteOpenShipmentDetail createDeleteOpenShipmentDetail() {

        DeleteOpenShipmentDetail deleteOpenShipmentDetail = new DeleteOpenShipmentDetail();
        deleteOpenShipmentDetail.setRequestedDateTime(requestedDateTime);
        deleteOpenShipmentDetail.setLocationCode(locationCode);
        deleteOpenShipmentDetail.setIndex(index);
        deleteOpenShipmentDetail.setWorkstationDetail(createWorkstationDetail());

        return deleteOpenShipmentDetail;
    }

    /**
     * Method to create Shipping Rate Data
     * 
     * @return ShippingRateData
     */
    private ShippingRateData createShipRateData() {

        ShippingRateData shippingRateData = new ShippingRateData();
        shippingRateData.setCommitDetails(createCommitDetails());
        shippingRateData.setCurrency("USD");
        shippingRateData.setDeliveryDayOfWeek("TUE");
        shippingRateData.setHal(false);
        shippingRateData.setRatingType("STANDARD_RATE");
        shippingRateData.setServiceDetails(createServiceDetails());
        shippingRateData.setSurcharges(createSurcharge());
        shippingRateData.setTotalAncillaryFeesAndTaxes(0);
        shippingRateData.setTotalBaseCharges(0);
        shippingRateData.setTotalDutiesAndTaxes(0);
        shippingRateData.setTotalDutiesTaxesAndFees(0);
        shippingRateData.setTotalFreightDiscounts(0.2);
        shippingRateData.setTotalNetCharges(0.8);
        shippingRateData.setTotalNetChargeWithDutiesAndTaxes(0);
        shippingRateData.setTotalNetFedExCharge(0);
        shippingRateData.setTotalNetFreight(0);
        shippingRateData.setTotalRebates(0);
        shippingRateData.setTotalSurcharges(0);
        shippingRateData.setTotalTaxes(0);
        return shippingRateData;
    }

    /**
     * Method to create List<ShippingRateSurcharge>
     * 
     * @return List<ShippingRateSurcharge>
     */
    private List<ShippingRateSurcharge> createShippingRateSurcharge() {

        List<ShippingRateSurcharge> surchargeDetailList = new ArrayList<>();
        ShippingRateSurcharge surchargeDetail = new ShippingRateSurcharge();
        surchargeDetail.setAmount(0.9);
        surchargeDetail.setDescription("surcharge");
        surchargeDetail.setSurchargeType("Insured_value");
        surchargeDetailList.add(surchargeDetail);
        return surchargeDetailList;
    }
    
    /**
     * Method to create List<SSurchargeDetail>
     * 
     * @return List<SurchargeDetail>
     */
    private List<SurchargeDetail> createSurcharge() {

        List<SurchargeDetail> surchargeDetailList = new ArrayList<>();
        SurchargeDetail surchargeDetail = new SurchargeDetail();
        surchargeDetail.setAmount(0.9);
        surchargeDetail.setDescription("surcharge");
        surchargeDetail.setSurchargeType("Insured_value");
        surchargeDetailList.add(surchargeDetail);
        return surchargeDetailList;
    }

    /**
     * Method to create service description detial
     * 
     * @return ServiceDescriptionDetail
     */
    private ServiceDescriptionDetail createServiceDetails() {

        ServiceDescriptionDetail serviceDescriptionDetail = new ServiceDescriptionDetail();
        serviceDescriptionDetail.setDescription("abc");
        return serviceDescriptionDetail;
    }

    /**
     * Method to create Commit Info
     * 
     * @return CommitInfo
     */
    private CommitInfo createCommitDetails() {

        CommitInfo commitInfo = new CommitInfo();
        commitInfo.setCommitTimestamp("18-09-2020");
        commitInfo.setDayOfWeek("TUE");
        return commitInfo;
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#deleteOpenShipmentClient(String, String, String, WorkstationDetail)}
     * then it is executed successfully
     */
    @Test
    public void testDeleteOpenShipment() {

        when(mapper.deleteOpenShipmentRequest(any(String.class), any(String.class), any(String.class),
                any(WorkstationDetail.class))).thenReturn(createDeleteOpenShipmentDetail());

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId))
                .thenReturn(createOpenShipment(referenceId, locationId, trackingId));

        when(openShipmentRepository.updateStatus(any(UpdateOpenShipmentStatusData.class))).thenReturn(true);

        doNothing().when(shipServiceClient)
                .deleteOpenShipment(createDeleteOpenShipmentDetail());

        shipmentCreationService.deleteOpenShipment(referenceId, locationId, trackingId);

        verify(openShipmentRepository, times(1)).retrieve(referenceId, locationId, trackingId);
        verify(openShipmentRepository, times(1)).updateStatus(any(UpdateOpenShipmentStatusData.class));
        verify(shipServiceClient, times(1)).deleteOpenShipment(any(DeleteOpenShipmentDetail.class));
    }

    /**
     * Test case to verify that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#deleteOpenShipmentClient(String, String, String, WorkstationDetail)}
     * then it is executed and no records were updated
     */
    @Test
    public void testDeleteOpenShipmentFalse() {

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId))
                .thenReturn(createOpenShipment(referenceId, locationId, trackingId));

        when(openShipmentRepository.updateStatus(any(UpdateOpenShipmentStatusData.class))).thenReturn(false);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_DELETION_FAILURE.getMessage()));

        shipmentCreationService.deleteOpenShipment(referenceId, locationId, trackingId);

        verify(openShipmentRepository, times(1)).retrieve(referenceId, locationId, trackingId);
        verify(openShipmentRepository, times(1)).updateStatus(any(UpdateOpenShipmentStatusData.class));
        verify(shipServiceClient, times(0)).deleteOpenShipment(any(DeleteOpenShipmentDetail.class));
    }

    /**
     * Test case to verify that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#deleteOpenShipmentClient(String, String, String, WorkstationDetail)}
     * then TerminalDbAccessException is thrown and it's handled successfully
     */
    @Test
    public void testDeleteOpenShipmentTerminalDbAccessException() {


        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId))
                .thenReturn(createOpenShipment(referenceId, locationId, trackingId));

        when(openShipmentRepository.updateStatus(any(UpdateOpenShipmentStatusData.class)))
                .thenThrow(TerminalDbAccessException.class);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_DELETION_FAILURE.getMessage()));

        shipmentCreationService.deleteOpenShipment(referenceId, locationId, trackingId);

        verify(openShipmentRepository, times(1)).retrieve(referenceId, locationId, trackingId);
        verify(openShipmentRepository, times(1)).updateStatus(any(UpdateOpenShipmentStatusData.class));
        verify(shipServiceClient, times(0)).deleteOpenShipment(any(DeleteOpenShipmentDetail.class));
    }

    /**
     * Test case to verify that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#deleteOpenShipmentClient(String, String, String, WorkstationDetail)}
     * then TransientDbAccessException is thrown and it's handled successfully
     */
    @Test
    public void testDeleteOpenShipmentTransientDbAccessException() {

        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId))
                .thenReturn(createOpenShipment(referenceId, locationId, trackingId));

        when(openShipmentRepository.updateStatus(any(UpdateOpenShipmentStatusData.class)))
                .thenThrow(TransientDbAccessException.class);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.DB_DELETION_FAILURE.getMessage()));

        shipmentCreationService.deleteOpenShipment(referenceId, locationId, trackingId);

        verify(openShipmentRepository, times(1)).retrieve(referenceId, locationId, trackingId);
        verify(openShipmentRepository, times(1)).updateStatus(any(UpdateOpenShipmentStatusData.class));
        verify(shipServiceClient, times(0)).deleteOpenShipment(any(DeleteOpenShipmentDetail.class));
    }

    /**
     * Test case to verify that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#deleteOpenShipmentClient(String, String, String, WorkstationDetail)}
     * then HystrixRuntimeException is thrown and it's handled successfully
     */
    @Test
    public void testDeleteOpenShipmentHystrixRuntimeException() {


        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId))
                .thenReturn(createOpenShipment(referenceId, locationId, trackingId));

        when(openShipmentRepository.updateStatus(any(UpdateOpenShipmentStatusData.class)))
                .thenThrow(HystrixRuntimeException.class);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.SYSTEM_UNAVAILABLE.getMessage()));

        shipmentCreationService.deleteOpenShipment(referenceId, locationId, trackingId);

        verify(openShipmentRepository, times(1)).retrieve(referenceId, locationId, trackingId);
        verify(openShipmentRepository, times(1)).updateStatus(any(UpdateOpenShipmentStatusData.class));
        verify(shipServiceClient, times(0)).deleteOpenShipment(any(DeleteOpenShipmentDetail.class));
    }

    /**
     * Test case to validate that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#retrieveOpenShipment(String, String, String)} then
     * ShipmentProcessorTerminalException is thrown and it's handled successfully
     */
    @Test
    public void testRetrieveOpenShipmentStatusConfirmed() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        openShipment.setOpenShipmentStatusCode(TransactionStatus.CONFIRMED.name());
        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.OPEN_SHIPMENT_STATUS_CONFIRMED.getMessage()));

        shipmentCreationService.retrieveOpenShipment(referenceId, locationId, trackingId);

    }

    /**
     * Test case to validate that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#deleteOpenShipment(String, String, String)} then
     * ShipmentProcessorTerminalException is thrown and it's handled successfully
     */
    @Test
    public void testDeleteOpenShipmentStatusConfirmed() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        openShipment.setOpenShipmentStatusCode(TransactionStatus.CONFIRMED.name());
        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.OPEN_SHIPMENT_STATUS_CONFIRMED.getMessage()));

        shipmentCreationService.deleteOpenShipment(referenceId, locationId, trackingId);

    }

    /**
     * Test case to validate that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#retrieveOpenShipment(String, String, String)} then
     * ShipmentProcessorTerminalException is thrown and it's handled successfully
     */
    @Test
    public void testRetrieveOpenShipmentStatusVoid() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        openShipment.setOpenShipmentStatusCode(TransactionStatus.VOID.name());
        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.OPEN_SHIPMENT_STATUS_DELETED.getMessage()));

        shipmentCreationService.retrieveOpenShipment(referenceId, locationId, trackingId);

    }

    /**
     * Test case to validate that when request is made to
     * {@link OpenShipmentTransactionServiceImpl#deleteOpenShipment(String, String, String)} then
     * ShipmentProcessorTerminalException is thrown and it's handled successfully
     */
    @Test
    public void testDeleteOpenShipmentStatusVoid() {

        OpenShipment openShipment = createOpenShipment(referenceId, locationId, trackingId);
        openShipment.setOpenShipmentStatusCode(TransactionStatus.VOID.name());
        when(openShipmentRepository.retrieve(referenceId, locationId, trackingId)).thenReturn(openShipment);

        expectedEx.expect(ServiceProcessorTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.OPEN_SHIPMENT_STATUS_DELETED.getMessage()));

        shipmentCreationService.deleteOpenShipment(referenceId, locationId, trackingId);

    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link ShipmentCreationServiceImpl#create(RequestedShipmentDetail,WorkstationDetail)} to set
     * the object fields and then to make a call to createOpenShipment() and throw
     * ShipmentProcessorTerminalException.
     * 
     * @throws HystrixException
     * 
     */
    @Test
    public void testCreateHystrixException() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        ShippingRateData shippingRateData = createShipRateData();
        PreauthorizedRateData preauthorizedRateData = createPreAuthorizedRateData();
        String accountNumber = "038000004";
        when(mapper.createRequestedShipmentDetail(requestedShipmentDetail, accountNumber, shippingRateData))
                .thenReturn(requestedShipmentDetail);
        when(srrsClient.getPreauthorizedRates(workstationDetail, requestedShipmentDetail))
                .thenReturn(preauthorizedRateData);
        when(mapper.mapPreAuthorizedRateDataToShippingRateData(preauthorizedRateData)).thenReturn(shippingRateData);
        doThrow(new HystrixRuntimeException(HystrixRuntimeException.FailureType.TIMEOUT, null,
                "Unable to process request. Please try again later.", null, null)).when(shipServiceClient)
                        .createOpenShipment(requestedShipmentDetail, workstationDetail);
        try {
            shipmentCreationService.create(requestedShipmentDetail, workstationDetail);
        } catch (ServiceProcessorTerminalException e) {
            assertEquals(ServiceErrorCode.RATING_SERVICE_UNAVAILABLE.getMessage(), e.getMessage());
        }
    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceImpl#create(RequestedShipmentDetail,WorkstationDetail)}
     * with interlineShippingDetail and PaymentType=Non_Account to set the object fields and then to
     * make a call to createOpenShipment() client method and return a valid response.
     * 
     */
    @Test
    public void testCreateWithInterlineShippingDetailAndNonAccountPaymentType() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.setInterlineShippingData(createMockInterlineShippingDetail());
        requestedShipmentDetail.setShippingChargesPayment(createMockShipingPaymentDetail());
        requestedShipmentDetail.getShippingChargesPayment()
                .setPaymentType(PaymentType.NON_ACCOUNT.name());
        ShippingRateData shippingRateData = createShipRateData();
        PreauthorizedRateData preauthorizedRateData = createPreAuthorizedRateData();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        String interlineAccountNumber = "164462765";
        CompleteShipmentDetail completeShipmentDetail = createReply();
        OpenShipment openShipment = new OpenShipment();
        when(mapper.createRequestedShipmentDetail(requestedShipmentDetail, interlineAccountNumber, shippingRateData))
                .thenReturn(requestedShipmentDetail);
        when(shipServiceClient.createOpenShipment(requestedShipmentDetail, workstationDetail))
                .thenReturn(completeShipmentDetail);
        when(mapper.createOpenShipmentResponse(completeShipmentDetail, requestedShipmentDetail, shippingRateData,
                preauthorizedRateData)).thenReturn(createOpenShipmentResponse());
        when(srrsClient.getPreauthorizedRates(workstationDetail, requestedShipmentDetail))
                .thenReturn(preauthorizedRateData);
        when(mapper.mapPreAuthorizedRateDataToShippingRateData(preauthorizedRateData)).thenReturn(shippingRateData);
        when(rsssClient.validateInterlineAccounts(requestedShipmentDetail.getInterlineShippingData()
                .getInterlineId())).thenReturn(interlineAccountNumber);
        when(mapper.createOpenShipment(any(OpenShipmentResponse.class), any(WorkstationDetail.class),
                any(RequestedShipmentDetail.class))).thenReturn(openShipment);
        when(openShipmentRepository.save(openShipment)).thenReturn(true);

        OpenShipmentResponse openShipmentResponse =
                shipmentCreationService.create(requestedShipmentDetail, workstationDetail);
        assertThat(openShipmentResponse, notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getCompletedPackageDetails(), notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getMasterTrackingId(), notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getIndex(), notNullValue());

        verify(rsssClient, times(1)).validateInterlineAccounts(requestedShipmentDetail.getInterlineShippingData()
                .getInterlineId());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceImpl#create(RequestedShipmentDetail,WorkstationDetail)}
     * with interlineShippingDetail and PaymentType= Account to set the object fields and then to
     * make a call to createOpenShipment() client method and return a valid response.
     * 
     */
    @Test
    public void testCreateWithInterlineShippingDetailAndAccountPaymentType() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.setInterlineShippingData(createMockInterlineShippingDetail());
        requestedShipmentDetail.setShippingChargesPayment(createMockShipingPaymentDetail());
        ShippingRateData shippingRateData = createShipRateData();
        PreauthorizedRateData preauthorizedRateData = createPreAuthorizedRateData();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        CompleteShipmentDetail completeShipmentDetail = createReply();
        OpenShipment openShipment = new OpenShipment();
        when(mapper.createRequestedShipmentDetail(requestedShipmentDetail, workstationDetail.getAccountNumber(),
                shippingRateData)).thenReturn(requestedShipmentDetail);
        when(shipServiceClient.createOpenShipment(requestedShipmentDetail, workstationDetail))
                .thenReturn(completeShipmentDetail);
        when(mapper.createOpenShipmentResponse(completeShipmentDetail, requestedShipmentDetail, shippingRateData,
                preauthorizedRateData)).thenReturn(createOpenShipmentResponse());
        when(srrsClient.getPreauthorizedRates(workstationDetail, requestedShipmentDetail))
                .thenReturn(preauthorizedRateData);
        when(mapper.mapPreAuthorizedRateDataToShippingRateData(preauthorizedRateData)).thenReturn(shippingRateData);
        when(mapper.createOpenShipment(any(OpenShipmentResponse.class), any(WorkstationDetail.class),
                any(RequestedShipmentDetail.class))).thenReturn(openShipment);
        when(openShipmentRepository.save(openShipment)).thenReturn(true);

        OpenShipmentResponse openShipmentResponse =
                shipmentCreationService.create(requestedShipmentDetail, workstationDetail);
        assertThat(openShipmentResponse, notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getCompletedPackageDetails(), notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getMasterTrackingId(), notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getIndex(), notNullValue());
        assertThat(requestedShipmentDetail.getInterlineShippingData(), nullValue());

    }

    /**
     * Method to build the mock instance of {@link InterlineShippingDetail} object
     * 
     * @return
     */
    private InterlineShippingData createMockInterlineShippingDetail() {

        InterlineShippingData interlineShippingData = new InterlineShippingData();
        interlineShippingData.setInterlineId("100127");
        interlineShippingData.setInterlineCode("G4");
        interlineShippingData.setInterlineName("ALLEGIANT AIRLINES");
        interlineShippingData.setInterlineNumber("008");
        interlineShippingData.setEmployeeId("3932");

        return interlineShippingData;
    }

    /**
     * Method to build the mock instance of {@link PaymentDetail} object
     * 
     * @return
     */
    private PaymentDetail createMockShipingPaymentDetail() {

        PaymentDetail paymentDetail = new PaymentDetail();
        paymentDetail.setPaymentType(PaymentType.ACCOUNT.name());
        paymentDetail.setResponsibleParty("SENDER");
        paymentDetail.setPayor(new PartyDetail());

        return paymentDetail;
    }
    
    /**
     * Method to create Preauthorized Rate Data
     * 
     * @return PreauthorizedRateData
     */
    private PreauthorizedRateData createPreAuthorizedRateData() {

        PreauthorizedRateData rate = new PreauthorizedRateData();
        rate.setServiceType("PRIORITY_OVERNIGHT");
        rate.setPackagingType("YOUR_PACKAGING");
        rate.setSignatureOption("Direct");
        rate.setServiceDetails(new PreauthorizedRateServiceDetail());
        rate.setCommitDetails(new PreauthorizedRateCommitDetail());
        ShipmentPreauthorizedRatedDetail shipmentPreauthorizedRatedDetail = new ShipmentPreauthorizedRatedDetail();
        shipmentPreauthorizedRatedDetail.setTotalBaseCharge(new Price(CurrencyType.USD, 25));
        shipmentPreauthorizedRatedDetail.setTotalNetCharge(new Price(CurrencyType.USD, 25));
        rate.setShipmentRateDetails(shipmentPreauthorizedRatedDetail);
        List<PackagePreauthorizedRatedDetail> ratedPackages = new ArrayList<>();
        PackagePreauthorizedRatedDetail packagePreauthorizedRatedDetail = new PackagePreauthorizedRatedDetail();
        ratedPackages.add(packagePreauthorizedRatedDetail);
        rate.setRatedPackages(ratedPackages);
        rate.getRatedPackages()
                .get(0)
                .setSurcharges(createShippingRateSurcharge());
        return rate;
    }

}
