package com.fedex.rscs.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.util.CollectionUtils;

import com.fedex.rscs.dto.CreateOpenShipmentRequestData;
import com.fedex.rscs.dto.InterlineShippingDetail;
import com.fedex.rscs.dto.RequestedShipment;
import com.fedex.rscs.dto.WeightSource;
import com.fedex.rscs.model.AddressDetail;
import com.fedex.rscs.model.BatteryClassificationType;
import com.fedex.rscs.model.CommitInfo;
import com.fedex.rscs.model.ContactDetail;
import com.fedex.rscs.model.HoldAtLocDetail;
import com.fedex.rscs.model.InsuredValueDetail;
import com.fedex.rscs.model.InterlineShippingData;
import com.fedex.rscs.model.LabelSpecificationDetail;
import com.fedex.rscs.model.OpenShipment;
import com.fedex.rscs.model.OpenShipmentData;
import com.fedex.rscs.model.OpenShipmentDetail;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.PaymentDetail;
import com.fedex.rscs.model.RequestedPackageLineItemDetail;
import com.fedex.rscs.model.RequestedPackageSpecialServices;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ServiceDescriptionDetail;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.ShipmentSpecialServiceDetail;
import com.fedex.rscs.model.ShippingCustomerReferenceDetails;
import com.fedex.rscs.model.SpecialServiceDescriptionDetail;
import com.fedex.rscs.model.SurchargeDetail;
import com.fedex.rscs.model.VariationOptionDetail;
import com.fedex.rscs.model.WeightDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.common.CurrencyType;
import com.fedex.rscs.model.common.PackagePreauthorizedRatedDetail;
import com.fedex.rscs.model.common.PreauthorizedRateCommitDetail;
import com.fedex.rscs.model.common.PreauthorizedRateServiceDetail;
import com.fedex.rscs.model.common.Price;
import com.fedex.rscs.model.common.ShipmentPreauthorizedRatedDetail;
import com.fedex.rscs.model.common.ShippingRateSurcharge;
import com.fedex.rscs.model.cshp.CompleteShipmentDetail;
import com.fedex.rscs.model.cshp.CompletedPackageData;
import com.fedex.rscs.model.cshp.CreditCardData;
import com.fedex.rscs.model.cshp.DeleteOpenShipmentDetail;
import com.fedex.rscs.model.cshp.EPaymentMode;
import com.fedex.rscs.model.cshp.OpenShipmentResponse;
import com.fedex.rscs.model.cshp.PreauthorizedRateData;
import com.fedex.rscs.model.cshp.ProductDescription;
import com.fedex.rscs.model.cshp.ShippingRateData;
import com.fedex.rscs.model.cshp.TrackingIdDetail;
import com.fedex.rscs.model.cshp.TrackingSequenceDetail;
import com.fedex.rscs.model.cshp.TransactionStatus;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;
import com.fedex.rscs.repository.OpenShipmentRepository;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * It contains test cases required to unit test OpenShipmentTransactionServiceMapper class
 * 
 * @author 3798910
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class OpenShipmentTransactionServiceMapperTest {

    @InjectMocks
    private OpenShipmentTransactionServiceMapper mapper;

    @Mock
    OpenShipmentRepository openShipmentRepository;
    
    @Before
    public void setup() {

        mapper = new OpenShipmentTransactionServiceMapper();
        mapper.openShipmentRepository = openShipmentRepository;
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipmentDetail(RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with all mandatory fields covered.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetail() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        ShippingRateData shippingRateData = createShipRateData();
        String accountNumber = "038000004";
        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(requestedShipment, accountNumber, shippingRateData);
        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getLabelSpecification(), notNullValue());
        assertThat(requestedShipmentDetail.getActions(), notNullValue());
        assertThat(requestedShipmentDetail.getVariationOptions(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipmentDetail(RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with all mandatory fields covered.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailIfPaymentOptionNull() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        requestedShipment.setShippingChargesPayment(null);
        ShippingRateData shippingRateData = createShipRateData();
        String accountNumber = "038000004";
        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(requestedShipment, accountNumber, shippingRateData);
        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getLabelSpecification(), notNullValue());
        assertThat(requestedShipmentDetail.getActions(), notNullValue());
        assertThat(requestedShipmentDetail.getVariationOptions(), notNullValue());
    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipmentDetail(RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with all mandatory fields
     * covered.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailIfPackageLineItemsNull() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        ShippingRateData shippingRateData = createShipRateData();
        requestedShipment.setPackageLineItems(null);
        String accountNumber = "038000004";
        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(requestedShipment, accountNumber, shippingRateData);
        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getLabelSpecification(), notNullValue());
        assertThat(requestedShipmentDetail.getActions(), notNullValue());
        assertThat(requestedShipmentDetail.getVariationOptions(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipmentDetail(RequestedShipmentDetail)}
     * to set the object fields when empty fields are provided as a request.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailEmptyRequestData() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        ShippingRateData shippingRateData = createShipRateData();
        requestedShipment.setLabelSpecification(new LabelSpecificationDetail());
        String accountNumber = "038000004";
        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(requestedShipment, accountNumber, shippingRateData);
        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipmentDetail(RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with all mandatory fields covered
     * with null special service requested.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailWdNullSpclServiceRequested() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        requestedShipment.setSpecialServicesRequested(null);
        ShippingRateData shippingRateData = createShipRateData();
        String accountNumber = "038000004";
        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(requestedShipment, accountNumber, shippingRateData);
        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getLabelSpecification(), notNullValue());
        assertThat(requestedShipmentDetail.getActions(), notNullValue());
        assertThat(requestedShipmentDetail.getVariationOptions(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipmentDetail(RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with all mandatory fields covered
     * with null hold at location details.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailWdNullHALDtl() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        requestedShipment.getSpecialServicesRequested()
                .setHoldAtLocationDetail(null);
        ShippingRateData shippingRateData = createShipRateData();
        String accountNumber = "038000004";
        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(requestedShipment, accountNumber, shippingRateData);
        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getLabelSpecification(), notNullValue());
        assertThat(requestedShipmentDetail.getActions(), notNullValue());
        assertThat(requestedShipmentDetail.getVariationOptions(), notNullValue());
        assertThat(requestedShipmentDetail.getSpecialServicesRequested(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipmentDetail(RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with all mandatory fields covered
     * with null Contact details in hold at location object.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailWdNullContactDetail() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        requestedShipment.getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .setContact(null);
        ShippingRateData shippingRateData = createShipRateData();
        String accountNumber = "038000004";
        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(requestedShipment, accountNumber, shippingRateData);
        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getLabelSpecification(), notNullValue());
        assertThat(requestedShipmentDetail.getActions(), notNullValue());
        assertThat(requestedShipmentDetail.getVariationOptions(), notNullValue());
        assertThat(requestedShipmentDetail.getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(requestedShipmentDetail.getSpecialServicesRequested()
                .getSpecialServicesRequestedType(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipmentDetail(RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with all mandatory fields covered
     * with null phone number in contact details for hpld at location detail object.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailWdNullphoneNumber() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        requestedShipment.getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getContact()
                .setPhoneNumber(null);
        ShippingRateData shippingRateData = createShipRateData();
        String accountNumber = "038000004";
        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(requestedShipment, accountNumber, shippingRateData);
        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getLabelSpecification(), notNullValue());
        assertThat(requestedShipmentDetail.getActions(), notNullValue());
        assertThat(requestedShipmentDetail.getVariationOptions(), notNullValue());
        assertThat(requestedShipmentDetail.getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(requestedShipmentDetail.getSpecialServicesRequested()
                .getSpecialServicesRequestedType(), notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createOpenShipmentResponse(CompleteShipmentDetail,RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with all mandatory fields covered.
     * 
     */
    @Test
    public void testCreateOpenShipmentResponse() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        ShippingRateData shippingRateData = createShipRateData();
        PreauthorizedRateData preauthorizedRateData = createPreAuthorizedRateData();
        List<RequestedPackageLineItemDetail> packageLineItems = new ArrayList<>();
        RequestedPackageLineItemDetail packageLineItem = new RequestedPackageLineItemDetail();
        packageLineItem.setWeight(new WeightDetail("LB", "5"));
        packageLineItem.setInsuredValue(new InsuredValueDetail("USD", "5"));
        packageLineItem.setSequenceNumber(1);
        packageLineItems.add(packageLineItem);
        requestedShipment.setPackageCount("1");
        requestedShipment.setPackageLineItems(packageLineItems);
        CompleteShipmentDetail comShipDet = new CompleteShipmentDetail();
        List<CompletedPackageData> comPackData = new ArrayList<>();
        CompletedPackageData comPackdat = new CompletedPackageData();
        comPackdat.setTrackingId(new TrackingSequenceDetail());
        List<TrackingIdDetail> tracDetails = new ArrayList<>();
        TrackingIdDetail tracDet = new TrackingIdDetail();
        tracDetails.add(tracDet);
        comPackdat.setTrackingIds(tracDetails);
        comPackData.add(comPackdat);
        comPackdat.setSequenceNumber(1);
        comShipDet.setCompletedPackageDetails(comPackData);

        OpenShipmentResponse openShipmentResponse = mapper.createOpenShipmentResponse(comShipDet, requestedShipment,
                shippingRateData, preauthorizedRateData);
        assertThat(openShipmentResponse, notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail(), notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getPackageCount(), equalTo(1));
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getTotalInsuredValue()
                .getAmount(), equalTo("5.0"));
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getTotalWeight()
                .getValue(), equalTo("5.0"));
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createOpenShipmentResponse(CompleteShipmentDetail,RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with empty weight and insured value
     * covered.
     * 
     */
    @Test
    public void testCreateOpenShipmentResponseEmptyWeightAndInsured() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        ShippingRateData shippingRateData = createShipRateData();
        PreauthorizedRateData preauthorizedRateData = createPreAuthorizedRateData();
        List<RequestedPackageLineItemDetail> packageLineItems = new ArrayList<>();
        RequestedPackageLineItemDetail packageLineItem = new RequestedPackageLineItemDetail();
        packageLineItem.setSequenceNumber(1);
        packageLineItems.add(packageLineItem);
        requestedShipment.setPackageCount("1");
        requestedShipment.setPackageLineItems(packageLineItems);
        ShipmentSpecialServiceDetail shipmentDetail = new ShipmentSpecialServiceDetail();
        SpecialServiceDescriptionDetail[] specialServicesType = { new SpecialServiceDescriptionDetail("SIGNATURE_OPTION", "SIGNATURE_OPTION", "SIGNATURE_OPTION", "SIGNATURE_OPTION")};
        shipmentDetail.setSpecialServicesRequestedType(CollectionUtils.arrayToList(specialServicesType));
        requestedShipment.setSpecialServicesRequested(shipmentDetail);
        CompleteShipmentDetail comShipDet = new CompleteShipmentDetail();
        List<CompletedPackageData> comPackData = new ArrayList<>();
        CompletedPackageData comPackdat = new CompletedPackageData();
        comPackdat.setTrackingId(new TrackingSequenceDetail());
        List<TrackingIdDetail> tracDetails = new ArrayList<>();
        TrackingIdDetail tracDet = new TrackingIdDetail();
        tracDetails.add(tracDet);
        comPackdat.setTrackingIds(tracDetails);
        comPackData.add(comPackdat);
        comPackdat.setSequenceNumber(1);
        comShipDet.setCompletedPackageDetails(comPackData);
        OpenShipmentResponse openShipmentResponse = mapper.createOpenShipmentResponse(comShipDet, requestedShipment,
                shippingRateData, preauthorizedRateData);
        assertThat(openShipmentResponse, notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail(), notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getPackageCount(), equalTo(1));
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getTotalInsuredValue()
                .getAmount(), equalTo("0.0"));
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getTotalWeight()
                .getValue(), equalTo("0.0"));
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getCompletedPackageDetails()
                .get(0)
                .getSpecialServicesDetails(), hasSize(1));
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getCompletedPackageDetails()
                .get(0)
                .getSpecialServicesDetails()
                .get(0)
                .getSpecialServiceType(), equalTo("SIGNATURE_OPTION"));
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createOpenShipmentResponse(CompleteShipmentDetail,RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with all mandatory fields
     * covered.
     * 
     */
    @Test
    public void testCreateOpenShipmentResponseIfSpecialServicesRequestedNull() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        requestedShipment.setSpecialServicesRequested(null);
        ShippingRateData shippingRateData = createShipRateData();
        PreauthorizedRateData preauthorizedRateData = createPreAuthorizedRateData();
        List<RequestedPackageLineItemDetail> packageLineItems = new ArrayList<>();
        RequestedPackageLineItemDetail packageLineItem = new RequestedPackageLineItemDetail();
        packageLineItem.setWeight(new WeightDetail("LB", "5"));
        packageLineItem.setInsuredValue(new InsuredValueDetail("USD", "5"));
        packageLineItem.setSequenceNumber(1);
        packageLineItems.add(packageLineItem);
        requestedShipment.setPackageCount("1");
        requestedShipment.setPackageLineItems(packageLineItems);
        CompleteShipmentDetail comShipDet = new CompleteShipmentDetail();
        List<CompletedPackageData> comPackData = new ArrayList<>();
        CompletedPackageData comPackdat = new CompletedPackageData();
        comPackdat.setTrackingId(new TrackingSequenceDetail());
        List<TrackingIdDetail> tracDetails = new ArrayList<>();
        TrackingIdDetail tracDet = new TrackingIdDetail();
        tracDetails.add(tracDet);
        comPackdat.setTrackingIds(tracDetails);
        comPackData.add(comPackdat);
        comPackdat.setSequenceNumber(1);
        comShipDet.setCompletedPackageDetails(comPackData);

        OpenShipmentResponse openShipmentResponse = mapper.createOpenShipmentResponse(comShipDet, requestedShipment,
                shippingRateData, preauthorizedRateData);
        assertThat(openShipmentResponse, notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail(), notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getPackageCount(), equalTo(1));
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getTotalInsuredValue()
                .getAmount(), equalTo("5.0"));
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getTotalWeight()
                .getValue(), equalTo("5.0"));
    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createOpenShipmentResponse(CompleteShipmentDetail,RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with all mandatory fields
     * covered.
     * 
     */
    @Test
    public void testCreateOpenShipmentResponseIfSpecialServicesRequestedTypeEmpty() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        requestedShipment.getSpecialServicesRequested().setSpecialServicesRequestedType(null);
        ShippingRateData shippingRateData = createShipRateData();
        PreauthorizedRateData preauthorizedRateData = createPreAuthorizedRateData();
        List<RequestedPackageLineItemDetail> packageLineItems = new ArrayList<>();
        RequestedPackageLineItemDetail packageLineItem = new RequestedPackageLineItemDetail();
        packageLineItem.setWeight(new WeightDetail("LB", "5"));
        packageLineItem.setInsuredValue(new InsuredValueDetail("USD", "5"));
        packageLineItem.setSequenceNumber(1);
        packageLineItems.add(packageLineItem);
        requestedShipment.setPackageCount("1");
        requestedShipment.setPackageLineItems(packageLineItems);
        CompleteShipmentDetail comShipDet = new CompleteShipmentDetail();
        List<CompletedPackageData> comPackData = new ArrayList<>();
        CompletedPackageData comPackdat = new CompletedPackageData();
        comPackdat.setTrackingId(new TrackingSequenceDetail());
        List<TrackingIdDetail> tracDetails = new ArrayList<>();
        TrackingIdDetail tracDet = new TrackingIdDetail();
        tracDetails.add(tracDet);
        comPackdat.setTrackingIds(tracDetails);
        comPackData.add(comPackdat);
        comPackdat.setSequenceNumber(1);
        comShipDet.setCompletedPackageDetails(comPackData);

        OpenShipmentResponse openShipmentResponse = mapper.createOpenShipmentResponse(comShipDet, requestedShipment,
                shippingRateData, preauthorizedRateData);
        assertThat(openShipmentResponse, notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail(), notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getPackageCount(), equalTo(1));
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getTotalInsuredValue()
                .getAmount(), equalTo("5.0"));
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getTotalWeight()
                .getValue(), equalTo("5.0"));
    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipment(CreateOpenShipmentRequestData)}
     * to set the object fields and return a valid response object with empty RequestedShipment covered.
     * 
     */
    @Test
    public void testCreateRequestedShipmentEmptyRequestedShipment() {

        CreateOpenShipmentRequestData creOpenShipReq = new CreateOpenShipmentRequestData();
        RequestedShipment requestedShipment = mapper.createRequestedShipment(creOpenShipReq);
        assertThat(requestedShipment, nullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipment(CreateOpenShipmentRequestData)}
     * to set the object fields and return a valid response object with RequestedShipment covered.
     * 
     */
    @Test
    public void testCreateRequestedShipment() {

        CreateOpenShipmentRequestData creOpenShipReq = new CreateOpenShipmentRequestData();
        creOpenShipReq.setRequestedShipment(new RequestedShipment());
        RequestedShipment requestedShipment = mapper.createRequestedShipment(creOpenShipReq);
        assertThat(requestedShipment, notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipment(CreateOpenShipmentRequestData)}
     * to set the object fields and return a valid response object with empty CreateOpenShipmentRequest
     * covered.
     * 
     */
    @Test
    public void testCreateRequestedShipmentEmptyCreateOpenShipmentRequest() {

        RequestedShipment requestedShipment = mapper.createRequestedShipment(null);
        assertThat(requestedShipment, nullValue());
    }

    /**
     * Method to prepare the RequestedShipmentDetail request
     * 
     * @return
     */
    private RequestedShipmentDetail createRequestedShipmentDetail() {

        RequestedShipmentDetail requestedShipmentDetail = new RequestedShipmentDetail();
        List<VariationOptionDetail> variationOptionDetails = new ArrayList<>();
        requestedShipmentDetail.setActions(new ArrayList<>());
        requestedShipmentDetail.setVariationOptions(variationOptionDetails);
        requestedShipmentDetail.setShippingChargesPayment(createShipingPaymentDetail());
        requestedShipmentDetail.setShipTimestamp("2020-08-31T16:59:10.273Z");
        requestedShipmentDetail.setPackageLineItems(createPackageLineItems());
        requestedShipmentDetail.setRecipient(createPartyDetail());
        requestedShipmentDetail.setSpecialServicesRequested(createShipmentSpecialService());
        requestedShipmentDetail.setInterlineShippingData(null);
        
        return requestedShipmentDetail;
    }
    
    /**
     * Create PartyDetail
     * 
     * @return
     */
    private PartyDetail createPartyDetail() {

        List<String> streetLines = new ArrayList<>();
        streetLines.add("4071 North Trail");

        AddressDetail addressDetail = new AddressDetail();
        addressDetail.setPostalCode("75023");
        addressDetail.setCountryCode("US");
        addressDetail.setStreetLines(streetLines);
        return new PartyDetail(addressDetail, createContactDetail(), "038000004",
                new CreditCardData("4005554444444460", "VISA", "12/2020"), "FEDEX_EXPRESS");
    }

    /**
     * Create ContactDetail
     * 
     * @return
     */
    private ContactDetail createContactDetail() {

        ContactDetail contactDetail = new ContactDetail();
        contactDetail.setContactId("test123");
        contactDetail.setFullName("Dhruv Kumar Sood");
        contactDetail.setPhoneNumber("32456764");

        return contactDetail;
    }

    /**
     * Create ShipmentSpecialServiceDetail
     * 
     * @return
     */
    private ShipmentSpecialServiceDetail createShipmentSpecialService() {

        ShipmentSpecialServiceDetail shipSpecialService = new ShipmentSpecialServiceDetail();
        shipSpecialService.setSpecialServicesRequestedType(createShipmentSpecialServiceDetail());
        shipSpecialService.setHoldAtLocationDetail(createHoldAtLocationDetail());

        return shipSpecialService;
    }

    /**
     * Create HoldAtLocationDetail
     * 
     * @return
     */
    private HoldAtLocDetail createHoldAtLocationDetail() {

        HoldAtLocDetail holdAtLocDetail = new HoldAtLocDetail();
        holdAtLocDetail.setAddress(new AddressDetail(new ArrayList<String>(), "", "TX", "75024", "US", true));
        holdAtLocDetail.setLocationId("DFWD");
        holdAtLocDetail.setContact(new ContactDetail(null, null, null, null, "1111111111", null));

        return holdAtLocDetail;
    }

    /**
     * Create ShipmentSpecialServiceDetail
     * 
     * @return
     */
    private List<SpecialServiceDescriptionDetail> createShipmentSpecialServiceDetail() {

        List<SpecialServiceDescriptionDetail> speSerList = new ArrayList<>();
        speSerList.add(new SpecialServiceDescriptionDetail("FEDEX_ONE_RATE", null, null, null));

        return speSerList;
    }


    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#retrieveOpenShipmentResponse(OpenShipment)} to set
     * the object fields and return a valid response object with all mandatory fields covered.
     * 
     */
    @Test
    public void testRetrieveOpenShipmentResponse() {

        OpenShipment openShipment = createOpenShipment();
        
        when(openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData()))
                .thenReturn(createOpenShipmentData());
        OpenShipmentResponse openShipmentResponse = mapper.retrieveOpenShipmentResponse(openShipment);
        assertThat(openShipmentResponse, notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail(), notNullValue());
        assertThat(openShipmentResponse.getShippingPaymentData(), notNullValue());
        assertThat(openShipmentResponse.getShippingRateData(), notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getPackageCount(), equalTo(0));
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getTotalInsuredValue()
                .getAmount(), equalTo("1.23"));
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getTotalWeight()
                .getValue(), equalTo("3"));
        assertThat(openShipmentResponse.getShippingPaymentData()
                .getPaymentType(), equalTo("ACCOUNT"));
        assertThat(openShipmentResponse.getShippingRateData()
                .getCurrency(), equalTo("USD"));
        assertThat(openShipmentResponse.getShippingRateData()
                .getRatingType(), equalTo("STANDARD_RATE"));
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getIndex(), equalTo("123456789"));
    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#retrieveOpenShipmentResponse(OpenShipment)} to set
     * the object fields and return a valid response object when null is returned on mapping blob data.
     * 
     */
    @Test
    public void testRetrieveOpenShipmentResponseWithNullOpenShipmentData() {

        OpenShipment openShipment = createOpenShipment();

        when(openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData()))
                .thenReturn(null);
        OpenShipmentResponse openShipmentResponse = mapper.retrieveOpenShipmentResponse(openShipment);
        assertThat(openShipmentResponse, notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail(), notNullValue());
        assertThat(openShipmentResponse.getShippingPaymentData(), nullValue());
        assertThat(openShipmentResponse.getShippingRateData(), nullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#retrieveOpenShipmentResponse(OpenShipment)} to set
     * the object fields and return a valid response object when empty OpenShipmentData is fetched from
     * blob.
     * 
     */
    @Test
    public void testRetrieveOpenShipmentResponseWithEmptyOpenShipmentData() {

        OpenShipment openShipment = createOpenShipment();

        when(openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData()))
                .thenReturn(new OpenShipmentData());
        OpenShipmentResponse openShipmentResponse = mapper.retrieveOpenShipmentResponse(openShipment);
        assertThat(openShipmentResponse, notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail(), notNullValue());
        assertThat(openShipmentResponse.getShippingPaymentData(), nullValue());
        assertThat(openShipmentResponse.getShippingRateData(), nullValue());
    }

    /**
     * Method to prepare the PaymentDetail
     * 
     * @return
     */
    private PaymentDetail createShipingPaymentDetail() {

        PaymentDetail paymentDetail = new PaymentDetail();
        paymentDetail.setPaymentType("ACCOUNT");
        paymentDetail.setResponsibleParty("SENDER");
        paymentDetail.setPayor(new PartyDetail());

        return paymentDetail;
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#retrieveOpenShipmentResponse(OpenShipment)} to set
     * the object fields and return a valid response object with empty shipment data.
     * 
     */
    @Test
    public void testRetrieveOpenShipmentResponseEmptyShipmentData() {

        OpenShipment openShipment = createOpenShipment();
        openShipment.setShipmentData(null);

        OpenShipmentResponse openShipmentResponse = mapper.retrieveOpenShipmentResponse(openShipment);
        assertThat(openShipmentResponse, notNullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail(), notNullValue());
        assertThat(openShipmentResponse.getShippingPaymentData(), nullValue());
        assertThat(openShipmentResponse.getShippingRateData(), nullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getPackageCount(), equalTo(0));
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getTotalInsuredValue(), nullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getTotalWeight(), nullValue());
        assertThat(openShipmentResponse.getCompleteShipmentDetail()
                .getIndex(), nullValue());
    }

    /**
     * Method to create object of OpenShipment
     * 
     * @return
     */
    private OpenShipment createOpenShipment() {

        // Instance of ShipmentData as JSON
        String shipmentData =
                "{\"openShipment\": {\"locationId\": \"TRLA\",\"shipmentIndex\": \"123456789\",\"carrierDetails\": {\"carrierCode\": \"FDXE\",\"opco\": \"EXPRESS\"},\"serviceType\": \"PRIORITY_OVERNIGHT\",\"packagingType\": \"YOUR_PACKAGING\",\"packageCount\": 0,\"recipient\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"specialServicesRequested\": {\"specialServices\": [{\"specialServiceType\": \"SIGNATURE_OPTION\",\"specialServiceSubType\": \"\",\"displayText\": \"ASR\",\"description\": \"Adult signature\"}],\"holdAtLocationDetail\": {\"locationId\": \"DREK\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"}}},\"totalInsuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"totalWeight\": {\"units\": \"LB\",\"value\": \"3\",\"measurementType\": \"MANUAL\"},\"packageLineItems\": [{\"dimensions\": {\"length\": \"1\",\"width\": \"1\",\"height\": \"1\",\"units\": \"IN\"},\"insuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"specialHandlingDetail\": \"CUSTOMER\",\"specialServicesDetails\": [{\"specialServiceType\": \"SIGNATURE_OPTION\",\"specialServiceSubType\": \"\",\"displayText\": \"ASR\",\"description\": \"Adult signature required\"}],\"weight\": {\"units\": \"LB\",\"value\": \"2\",\"measurementType\": \"MANUAL\"},\"signatureOption\": \"ADULT\",\"trackingId\": {\"trackingNumber\": \"123456789\",\"barcode\": \"11111111111111111111111111111111\",\"sequence\": 1},\"master\": true}],\"shipper\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"customer\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"personName\": \"fedexsender\",\"company\": {\"name\": \"infogain\"},\"emailDetails\": {\"emailAddress\": \"info@infogain.com\"},\"phoneNumberDetails\": [{\"phoneNumber\": {\"number\": \"4365798090\",\"extension\": \"+021\"},\"usage\": \"PRIMARY\"}]}},\"rateDetails\": {\"serviceDetails\": {\"serviceType\": \"SIGNATURE_OPTTION\",\"serviceName\": \"\",\"description\": \"ASR\"},\"deliveryDayOfWeek\": \"FRI\",\"hal\": false,\"commitDetails\": {\"commitTimestamp\": \"2020-08-21T23:59:00\",\"dayOfWeek\": \"FRI\"},\"currency\": \"USD\",\"ratingType\": \"STANDARD_RATE\",\"totalBaseCharges\": \"1.23\",\"totalRebates\": \"1.23\",\"totalNetCharges\": \"1.23\",\"totalSurcharges\": \"1.23\",\"totalTaxes\": \"1.23\",\"totalFreightDiscounts\": \"1.23\",\"totalNetFreight\": \"1.23\",\"totalNetFedExCharge\": \"1.23\",\"totalDutiesAndTaxes\": \"1.23\",\"totalAncillaryFeesAndTaxes\": \"1.23\",\"totalDutiesTaxesAndFees\": \"1.23\",\"totalNetChargeWithDutiesAndTaxes\": \"1.23\",\"surcharges\": [{\"surchargeType\": \"INSURED_VALUE\",\"description\": \"Declared value\",\"amount\": \"1.23\"}]},\"paymentDetails\": {\"paymentType\": \"ACCOUNT\",\"paymentSource\": \"SPOS\",\"paymentAmount\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"responsibleParty\": \"SENDER\",\"payor\": {\"accountType\": \"FEDEX_EXPRESS\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"accountNumber\": \"1110093\",\"creditCard\": {\"maskedcreditCard\": \"string\",\"type\": \"VISA\",\"expirationDate\": \"string\"}}},\"labelSpecification\": {\"processingOptionsRequested\": \"string\",\"disposition\": [\"string\"],\"formatType\": \"OPERATIONAL_LABEL\",\"imageType\": \"PNG\",\"stockType\": \"STOCK_4X6\",\"printingOrientation\": \"TOP_EDGE_OF_TEXT_FIRST\",\"rotation\": \"string\",\"labelOrder\": \"string\",\"localization\": \"string\",\"labelOrigin\": \"string\",\"customerSpecifiedDetail\": \"string\"}}}";

        OpenShipment openShipment = new OpenShipment();
        openShipment.setOpenShipmentId("12345");
        openShipment.setTrackingNumber("123456");
        openShipment.setFedexLocationId("1234567");
        openShipment.setCityCenterAccountNumber("033600003");
        openShipment.setDeviceId("SSFE");
        openShipment.setFedexId("3900090");
        openShipment.setInitialAcceptanceTmStmp("2020-07-30T11:55:28.687+05:30");
        openShipment.setMeterNumber("033600003");
        openShipment.setOpCoCode("FXO");
        openShipment.setOpCoLocationId("DREK");
        openShipment.setOpenShipmentStatusCode("ship");
        openShipment.setOpenShipmentStatusTmStmp("2020-07-30T11:55:28.687+05:30");
        openShipment.setPackageAcceptTmStmp("2020-07-30T11:55:28.687+05:30");
        openShipment.setShipmentData(shipmentData);
        openShipment.setSoftwareId("SSFE");

        return openShipment;
    }

    /**
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipmentDetail(RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with shipping charges payment
     * covered.
     * 
     */
    @Test
    public void testShipmentChargesPaymentNonAccountCreateOpenShipmentRequest() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        ShippingRateData shippingRateData = createShipRateData();
        String accountNumber = "038000004";
        requestedShipment.getShippingChargesPayment()
                .setPaymentType("NON_ACCOUNT");
        requestedShipment.getShippingChargesPayment()
                .getPayor()
                .setAccountNumber(accountNumber);
        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(requestedShipment, accountNumber, shippingRateData);
        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getLabelSpecification(), notNullValue());
        assertThat(requestedShipmentDetail.getActions(), notNullValue());
        assertThat(requestedShipmentDetail.getVariationOptions(), notNullValue());
        assertThat(requestedShipmentDetail.getShippingChargesPayment(), notNullValue());
        assertThat(requestedShipmentDetail.getShippingChargesPayment()
                .getEpaymentMode(), equalTo(EPaymentMode.CASH));
        
    }

    /**
     * Method to create Shipping Rate Data
     * 
     * @return ShippingRateData
     */
    private ShippingRateData createShipRateData() {

        ShippingRateData shippingRateData = new ShippingRateData();
        shippingRateData.setCommitDetails(createCommitDetails());
        shippingRateData.setCurrency("USD");
        shippingRateData.setDeliveryDayOfWeek("TUE");
        shippingRateData.setHal(false);
        shippingRateData.setRatingType("STANDARD_RATE");
        shippingRateData.setServiceDetails(createServiceDetails());
        shippingRateData.setSurcharges(createSurcharge());
        shippingRateData.setTotalAncillaryFeesAndTaxes(0);
        shippingRateData.setTotalBaseCharges(0);
        shippingRateData.setTotalDutiesAndTaxes(0);
        shippingRateData.setTotalDutiesTaxesAndFees(0);
        shippingRateData.setTotalFreightDiscounts(0.2);
        shippingRateData.setTotalNetCharges(0.8);
        shippingRateData.setTotalNetChargeWithDutiesAndTaxes(0);
        shippingRateData.setTotalNetFedExCharge(0);
        shippingRateData.setTotalNetFreight(0);
        shippingRateData.setTotalRebates(0);
        shippingRateData.setTotalSurcharges(0);
        shippingRateData.setTotalTaxes(0);
        return shippingRateData;
    }

    /**
     * Method to create List<SurchargeDetail>
     * 
     * @return List<SurchargeDetail>
     */
    private List<SurchargeDetail> createSurcharge() {

        List<SurchargeDetail> surchargeDetailList = new ArrayList<>();
        SurchargeDetail surchargeDetail = new SurchargeDetail();
        surchargeDetail.setAmount(0.9);
        surchargeDetail.setDescription("surcharge");
        surchargeDetail.setSurchargeType("Insured_value");
        surchargeDetailList.add(surchargeDetail);
        return surchargeDetailList;
    }

    /**
     * Method to create service description detial
     * 
     * @return ServiceDescriptionDetail
     */
    private ServiceDescriptionDetail createServiceDetails() {

        ServiceDescriptionDetail serviceDescriptionDetail = new ServiceDescriptionDetail();
        serviceDescriptionDetail.setDescription("abc");
        return serviceDescriptionDetail;
    }

    /**
     * Method to create Commit Info
     * 
     * @return CommitInfo
     */
    private CommitInfo createCommitDetails() {

        CommitInfo commitInfo = new CommitInfo();
        commitInfo.setCommitTimestamp("18-09-2020");
        commitInfo.setDayOfWeek("TUE");
        return commitInfo;
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createOpenShipment(openShipmentResponse, workstationDetail, requestShipmentDetail)}
     * to set the object fields and return a valid response OpenShipment object
     * 
     */
    @Test
    public void testCreateOpenShipment() {

        OpenShipmentResponse openShipmentResponse = createOpenShipmentResponse();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestShipmentDetail = createRequestedShipmentDetail();

        OpenShipment openShipment =
                mapper.createOpenShipment(openShipmentResponse, workstationDetail, requestShipmentDetail);
        assertThat(openShipment, notNullValue());
        assertThat(openShipment.getCityCenterAccountNumber(), equalTo("038000004"));
        assertThat(openShipment.getPackageAcceptTmStmp(), equalTo("2020-08-31T16:59:10.273Z"));
        assertThat(openShipment.getOpCoLocationId(), equalTo("LASA"));
        assertThat(openShipment.getShipmentData(), notNullValue());
        assertTrue(openShipment.getShipmentData()
                .contains("2120"));
        assertThat(openShipment.getShipmentData(), is(not(containsString("100127"))));
        assertThat(openShipment.getShipmentData(), is(not(containsString("ALLEGIANT AIRLINES"))));
    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createOpenShipment(openShipmentResponse, workstationDetail, requestShipmentDetail)}
     * to set the object fields and return a valid response OpenShipment object
     * 
     */
    @Test
    public void testCreateOpenShipmentIfCustomerReferencesNull() {

        OpenShipmentResponse openShipmentResponse = createOpenShipmentResponse();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestShipmentDetail = createRequestedShipmentDetail();
        requestShipmentDetail.getPackageLineItems()
                .get(0)
                .setCustomerReferences(null);
        OpenShipment openShipment =
                mapper.createOpenShipment(openShipmentResponse, workstationDetail, requestShipmentDetail);
        assertThat(openShipment, notNullValue());
        assertThat(openShipment.getCityCenterAccountNumber(), equalTo("038000004"));
        assertThat(openShipment.getPackageAcceptTmStmp(), equalTo("2020-08-31T16:59:10.273Z"));
        assertThat(openShipment.getOpCoLocationId(), equalTo("LASA"));
        assertThat(openShipment.getShipmentData(), notNullValue());
        assertTrue(openShipment.getShipmentData()
                .contains("2120"));
    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createOpenShipment(openShipmentResponse, workstationDetail, requestShipmentDetail)}
     * to set the object fields and return a valid response OpenShipment object
     * 
     */
    @Test
    public void testCreateOpenShipmentIfPackageSpecialServicesNull() {

        OpenShipmentResponse openShipmentResponse = createOpenShipmentResponse();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestShipmentDetail = createRequestedShipmentDetail();
        requestShipmentDetail.getPackageLineItems()
                .get(0)
                .setPackageSpecialServicesRequested(null);

        OpenShipment openShipment =
                mapper.createOpenShipment(openShipmentResponse, workstationDetail, requestShipmentDetail);
        assertThat(openShipment, notNullValue());
        assertThat(openShipment.getCityCenterAccountNumber(), equalTo("038000004"));
        assertThat(openShipment.getPackageAcceptTmStmp(), equalTo("2020-08-31T16:59:10.273Z"));
        assertThat(openShipment.getOpCoLocationId(), equalTo("LASA"));
        assertThat(openShipment.getShipmentData(), notNullValue());
        assertTrue(openShipment.getShipmentData()
                .contains("2120"));
    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createOpenShipment(openShipmentResponse, workstationDetail, requestShipmentDetail)}
     * to set the object fields and return a valid response OpenShipment object
     * 
     */
    @Test
    public void testCreateOpenShipmentIfSpecialServicesRequestedNull() {

        OpenShipmentResponse openShipmentResponse = createOpenShipmentResponse();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestShipmentDetail = createRequestedShipmentDetail();
        requestShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .setSpecialServicesRequested(null);

        OpenShipment openShipment =
                mapper.createOpenShipment(openShipmentResponse, workstationDetail, requestShipmentDetail);
        assertThat(openShipment, notNullValue());
        assertThat(openShipment.getCityCenterAccountNumber(), equalTo("038000004"));
        assertThat(openShipment.getPackageAcceptTmStmp(), equalTo("2020-08-31T16:59:10.273Z"));
        assertThat(openShipment.getOpCoLocationId(), equalTo("LASA"));
        assertThat(openShipment.getShipmentData(), notNullValue());
        assertTrue(openShipment.getShipmentData()
                .contains("2120"));
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createOpenShipment(openShipmentResponse, workstationDetail, requestShipmentDetail)}
     * to not set the some object fields and return a valid response OpenShipment object without those
     * fields.
     * 
     */
    @Test
    public void testCreateOpenShipmentWithEmptyParam() {

        OpenShipmentResponse openShipmentResponse = createOpenShipmentResponse();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestShipmentDetail = createRequestedShipmentDetail();
        openShipmentResponse.getCompleteShipmentDetail()
                .setServiceDescription(null);
        openShipmentResponse.getCompleteShipmentDetail()
                .setPackagingDescription(null);
        openShipmentResponse.getCompleteShipmentDetail()
                .setCarrierCode("FDXG");

        OpenShipment openShipment =
                mapper.createOpenShipment(openShipmentResponse, workstationDetail, requestShipmentDetail);
        assertThat(openShipment, notNullValue());
        assertThat(openShipment.getShipmentData(), notNullValue());
        assertThat(openShipment.getShipmentData(), containsString("GROUND"));

    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createOpenShipment(openShipmentResponse, workstationDetail, requestShipmentDetail)}
     * to set the object fields and return a valid response OpenShipment object
     * 
     */
    @Test
    public void testDeleteOpenShipment() {

        WorkstationDetail workstationDetail = createWorkstationDetail();
        DeleteOpenShipmentDetail deleteOpenShipmentDetail =
                mapper.deleteOpenShipmentRequest("23-09-2020", "DFWD", "12345", workstationDetail);

        assertThat(deleteOpenShipmentDetail, notNullValue());
        assertThat(deleteOpenShipmentDetail.getRequestedDateTime(), equalTo("23-09-2020"));
        assertThat(deleteOpenShipmentDetail.getLocationCode(), equalTo("DFWD"));
        assertThat(deleteOpenShipmentDetail.getIndex(), equalTo("12345"));
        assertThat(deleteOpenShipmentDetail.getWorkstationDetail(), notNullValue());
    }

    /**
     * Method will create OpenShipmentResponse object
     * 
     * @return
     */
    private OpenShipmentResponse createOpenShipmentResponse() {

        OpenShipmentResponse openShipmentResponse = new OpenShipmentResponse();
        CompleteShipmentDetail completeShipmentDetail = new CompleteShipmentDetail();
        TrackingIdDetail masterTrackingId = new TrackingIdDetail();
        ProductDescription productDescription = new ProductDescription();
        productDescription.setType("FEDEX_TYPE");

        masterTrackingId.setTrackingNumber("794978662786");
        masterTrackingId.setTrackingIdType("FEDEX");
        completeShipmentDetail.setMasterTrackingId(masterTrackingId);
        completeShipmentDetail.setUsDomestic(true);
        completeShipmentDetail.setIndex("12345");
        completeShipmentDetail.setCarrierCode("FDXE");
        completeShipmentDetail.setLocationId("LASA");
        completeShipmentDetail.setServiceDescription(productDescription);
        completeShipmentDetail.setPackagingDescription(productDescription);
        
        List<CompletedPackageData> completedPackageDetails = new ArrayList<>();
        CompletedPackageData completedPackageDetail = new CompletedPackageData();
        completedPackageDetail.setGroupNumber(1);
        completedPackageDetail.setMaster(true);
        completedPackageDetails.add(completedPackageDetail);
        
        completeShipmentDetail.setCompletedPackageDetails(completedPackageDetails);
        
        openShipmentResponse.setShippingRateData(new ShippingRateData());
        openShipmentResponse.setCompleteShipmentDetail(completeShipmentDetail);
        return openShipmentResponse;
    }

    /**
     * Method to prepare the WorkstationDetail request
     * 
     * @return
     */
    private WorkstationDetail createWorkstationDetail() {

        WorkstationDetail workstationDetail = new WorkstationDetail();
        workstationDetail.setAccountNumber("038000004");
        workstationDetail.setAppVersionId("2120");
        workstationDetail.setMeterNumber("6997006");
        workstationDetail.setSoftwareId("SSFE");
        workstationDetail.setTeamMemberId("981840");
        workstationDetail.setDeviceId("FUSE");

        return workstationDetail;
    }

    /**
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipmentDetail(RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with shipping charges payment
     * covered.
     * 
     */
    @Test
    public void testShipmentChargesPaymentAccountCreateOpenShipmentRequest() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        ShippingRateData shippingRateData = createShipRateData();
        String accountNumber = "038000004";
        requestedShipment.getShippingChargesPayment()
                .setPaymentType("ACCOUNT");
        requestedShipment.getShippingChargesPayment()
                .setResponsibleParty("RECIPIENT");
        requestedShipment.getShippingChargesPayment()
                .getPayor()
                .setAccountNumber(accountNumber);
        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(requestedShipment, accountNumber, shippingRateData);
        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getLabelSpecification(), notNullValue());
        assertThat(requestedShipmentDetail.getActions(), notNullValue());
        assertThat(requestedShipmentDetail.getVariationOptions(), notNullValue());
        assertThat(requestedShipmentDetail.getShippingChargesPayment()
                .getResponsibleParty(), equalTo("RECIPIENT"));
        assertThat(requestedShipmentDetail.getShippingChargesPayment()
                .getResponsibleParty(), notNullValue());
    }

    /**
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipmentDetail(RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with Responsible Party Null covered.
     * 
     */
    @Test
    public void testShipmentChargesPaymentAccountResponsiblePartyBlank() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        ShippingRateData shippingRateData = createShipRateData();
        String accountNumber = "038000004";
        requestedShipment.getShippingChargesPayment()
                .setPaymentType("ACCOUNT");
        requestedShipment.getShippingChargesPayment()
                .setResponsibleParty(null);
        requestedShipment.getShippingChargesPayment()
                .getPayor()
                .setAccountNumber(accountNumber);
        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(requestedShipment, accountNumber, shippingRateData);
        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getLabelSpecification(), notNullValue());
        assertThat(requestedShipmentDetail.getActions(), notNullValue());
        assertThat(requestedShipmentDetail.getVariationOptions(), notNullValue());
        assertThat(requestedShipmentDetail.getShippingChargesPayment()
                .getResponsibleParty(), equalTo("SENDER"));
        assertThat(requestedShipmentDetail.getShippingChargesPayment()
                .getResponsibleParty(), notNullValue());
    }

    /**
     * Method to prepare the List<RequestedPackageLineItemDetail> object
     * 
     * @return
     */
    private List<RequestedPackageLineItemDetail> createPackageLineItems() {

        List<RequestedPackageLineItemDetail> pkgLineItemDetailList = new ArrayList<>();
        RequestedPackageLineItemDetail reqPckgLineItemDtl = new RequestedPackageLineItemDetail();

        List<ShippingCustomerReferenceDetails> customerReferenceList = new ArrayList<>();
        ShippingCustomerReferenceDetails customerReferenceDtl = new ShippingCustomerReferenceDetails();
        customerReferenceDtl.setReferenceType("CUSTOMER_REFERENCE");
        customerReferenceDtl.setValue("abc");
        customerReferenceList.add(customerReferenceDtl);

        RequestedPackageSpecialServices pkgSpclSrvcRequested = new RequestedPackageSpecialServices();
        WeightDetail dryIceWeightDtl = new WeightDetail();
        dryIceWeightDtl.setUnit("KG");
        dryIceWeightDtl.setValue("1");
        pkgSpclSrvcRequested.setDryIceWeightDetails(dryIceWeightDtl);
        List<BatteryClassificationType> bateryClassification = new ArrayList<>();
        bateryClassification.add(BatteryClassificationType.LITHIUM_METAL_CONTAINED_IN_EQUIPMENT);
        pkgSpclSrvcRequested.setBatteryClassifications(bateryClassification);
        List<SpecialServiceDescriptionDetail> specialServicesRequested = new ArrayList<>();
        SpecialServiceDescriptionDetail specialServiceDescriptionDetail = new SpecialServiceDescriptionDetail();
        specialServiceDescriptionDetail.setSpecialServiceType("DRY_ICE");
        specialServicesRequested.add(specialServiceDescriptionDetail);
        pkgSpclSrvcRequested.setSpecialServicesRequested(specialServicesRequested);
        
        reqPckgLineItemDtl.setCustomerReferences(customerReferenceList);
        reqPckgLineItemDtl.setPackageSpecialServicesRequested(pkgSpclSrvcRequested);
        WeightDetail weight = new WeightDetail();
        weight.setUnit("KG");
        weight.setValue("1");
        reqPckgLineItemDtl.setWeight(weight);
        pkgLineItemDetailList.add(reqPckgLineItemDtl);

        return pkgLineItemDetailList;
    }

    /**
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipmentDetail(RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with Responsible Party Null covered.
     * 
     */
    @Test
    public void testBuildUpdateOpenShipmentStatusData() {

        ShipmentConfirmationRequest shipmentConfirmationRequest = createShipmentConfirmationRequest();
        UpdateOpenShipmentStatusData updateOpenShipmentStatusData =
                mapper.buildUpdateOpenShipmentStatusData(shipmentConfirmationRequest);
        assertThat(updateOpenShipmentStatusData, notNullValue());
        assertThat(updateOpenShipmentStatusData.getLocationId(), equalTo("LASA"));
        assertThat(updateOpenShipmentStatusData.getOpenShipmentStatusCode(),
                equalTo(TransactionStatus.CONFIRMED.name()));
        assertThat(updateOpenShipmentStatusData.getReferenceId(), equalTo("3456"));
        assertThat(updateOpenShipmentStatusData.getTeamMemberId(), equalTo("981840"));
        assertThat(updateOpenShipmentStatusData.getTrackingId(), equalTo("7898981234"));
    }


    /**
     * Method to create object of ShipmentConfirmationRequest
     * 
     * @return
     */
    private ShipmentConfirmationRequest createShipmentConfirmationRequest() {

        ShipmentConfirmationRequest request = new ShipmentConfirmationRequest();
        request.setWorkstationDetail(createWorkstationDetail());
        request.setReferenceId("3456");
        request.setTrackingId("7898981234");
        request.setTransactionLocationId("LASA");
        request.setPaymentDetail(new PaymentDetail());
        return request;
    }

    /**
     * Method to create object of OpenShipmentData
     * 
     * @return
     */
    private OpenShipmentData createOpenShipmentData() {

        OpenShipmentData openShipmentData = new OpenShipmentData();
        openShipmentData.setOpenShipment(createOpenShipmentDetail());

        return openShipmentData;
    }

    /**
     * Method to create object of OpenShipmentDetail
     * 
     * @return
     */
    private OpenShipmentDetail createOpenShipmentDetail() {

        OpenShipmentDetail openShipmentDetail = new OpenShipmentDetail();
        openShipmentDetail.setAppVersionId("2120");
        openShipmentDetail.setShipmentIndex("123456789");
        openShipmentDetail.setPaymentDetails(createShipingPaymentDetail());
        openShipmentDetail.setRateDetails(createShipRateData());
        openShipmentDetail.setTotalInsuredValue(new InsuredValueDetail("USD", "1.23"));
        openShipmentDetail.setTotalWeight(new WeightDetail("LB", "3"));
        return openShipmentDetail;
    }
    
    /**
     * {@link OpenShipmentTransactionServiceMapper#GetAppVersionIdFromCreateOpenShipment(OpenShipmentData)}
     * returns valid appVersionId for valid OpenShipmentData.
     * 
     */
    @Test
    public void testGetAppVersionIdFromCreateOpenShipment() {

        OpenShipmentData openShipmentData = createOpenShipmentData();
        String appVersionId = mapper.getAppVersionIdFromCreateOpenShipment(openShipmentData);
        assertThat(appVersionId, notNullValue());
        assertThat(appVersionId, equalTo("2120"));
    }

    /**
     * {@link OpenShipmentTransactionServiceMapper#GetAppVersionIdFromCreateOpenShipment(OpenShipmentData)}
     * returns null appVersionId for null OpenShipmentData is covered.
     * 
     */
    @Test
    public void testGetAppVersionIdFromCreateOpenShipmentForNullOpenShipmentData() {

        OpenShipmentData openShipmentData = null;
        String appVersionId = mapper.getAppVersionIdFromCreateOpenShipment(openShipmentData);
        assertThat(appVersionId, nullValue());
    }
     
    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createOpenShipment(openShipmentResponse, workstationDetail, requestShipmentDetail)}
     * with InterlineShippingDetail to set the object fields and return a valid response
     * OpenShipment object
     * 
     */
    @Test
    public void testCreateOpenShipmentWithInterlineShippingDetail() {

        OpenShipmentResponse openShipmentResponse = createOpenShipmentResponse();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.setInterlineShippingData(createMockInterlineShippingDetail());
        requestedShipmentDetail.getShippingChargesPayment()
                .setInterlineId(requestedShipmentDetail.getInterlineShippingData()
                        .getInterlineId());

        OpenShipment openShipment =
                mapper.createOpenShipment(openShipmentResponse, workstationDetail, requestedShipmentDetail);
        assertThat(openShipment, notNullValue());
        assertThat(openShipment.getCityCenterAccountNumber(), equalTo("038000004"));
        assertThat(openShipment.getShipmentData(), notNullValue());
        assertThat(openShipment.getShipmentData(), containsString("100127"));
        assertThat(openShipment.getShipmentData(), containsString("ALLEGIANT AIRLINES"));
    }

    /**
     * Method to build the mock instance of {@link InterlineShippingDetail} object
     * 
     * @return
     */
    private InterlineShippingData createMockInterlineShippingDetail() {

        InterlineShippingData interlineShippingData = new InterlineShippingData();
        interlineShippingData.setInterlineId("100127");
        interlineShippingData.setInterlineCode("G4");
        interlineShippingData.setInterlineName("ALLEGIANT AIRLINES");
        interlineShippingData.setInterlineNumber("008");
        interlineShippingData.setEmployeeId("3932");

        return interlineShippingData;
    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipmentDetail(RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with all mandatory fields covered
     * with null measurementType in weight details and dry ice weight details.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailWdMeasurementTypeNull() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        ShippingRateData shippingRateData = createShipRateData();
        List<RequestedPackageLineItemDetail> packageLineItems = createPackageLineItems();
        RequestedPackageLineItemDetail packageLineItem = packageLineItems.get(0);
        packageLineItem.getWeight()
                .setMeasurementType(null);
        packageLineItem.getPackageSpecialServicesRequested()
                .setDryIceWeightDetails(null);
        packageLineItems.add(packageLineItem);
        requestedShipment.setPackageLineItems(packageLineItems);
        String accountNumber = "038000004";
        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(requestedShipment, accountNumber, shippingRateData);
        assertThat(requestedShipmentDetail, notNullValue());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipmentDetail(RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with all mandatory fields covered
     * with null measurementType in dry iceweight details.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailWdDryIceMeasurementTypeNull() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        ShippingRateData shippingRateData = createShipRateData();
        requestedShipment.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getDryIceWeightDetails()
                .setMeasurementType(null);
        String accountNumber = "038000004";
        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(requestedShipment, accountNumber, shippingRateData);
        assertThat(requestedShipmentDetail, notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getDryIceWeightDetails(), notNullValue());
        assertThat(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getDryIceWeightDetails()
                .getMeasurementType(), notNullValue());
        assertEquals(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .getDryIceWeightDetails()
                .getMeasurementType(), WeightSource.MANUAL.toString());
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link OpenShipmentTransactionServiceMapper#createRequestedShipmentDetail(RequestedShipmentDetail)}
     * to set the object fields and return a valid response object with all mandatory fields covered
     * with measurementType in weight details.
     * 
     */
    @Test
    public void testCreateRequestedShipmentDetailWdMeasurementTypeData() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        ShippingRateData shippingRateData = createShipRateData();
        List<RequestedPackageLineItemDetail> packageLineItems = createPackageLineItems();
        RequestedPackageLineItemDetail packageLineItem = packageLineItems.get(0);
        packageLineItem.getWeight()
                .setMeasurementType(WeightSource.MANUAL.toString());
        packageLineItem.getPackageSpecialServicesRequested()
                .getDryIceWeightDetails()
                .setMeasurementType(WeightSource.MANUAL.toString());
        packageLineItems.add(packageLineItem);
        requestedShipment.setPackageLineItems(packageLineItems);
        String accountNumber = "038000004";
        RequestedShipmentDetail requestedShipmentDetail =
                mapper.createRequestedShipmentDetail(requestedShipment, accountNumber, shippingRateData);
        assertThat(requestedShipmentDetail, notNullValue());
    }
    
    /**
     * Method to create Preauthorized Rate Data
     * 
     * @return PreauthorizedRateData
     */
    private PreauthorizedRateData createPreAuthorizedRateData() {

        PreauthorizedRateData rate = new PreauthorizedRateData();
        rate.setServiceType("PRIORITY_OVERNIGHT");
        rate.setPackagingType("YOUR_PACKAGING");
        rate.setSignatureOption("Direct");
        rate.setServiceDetails(new PreauthorizedRateServiceDetail());
        rate.setCommitDetails(new PreauthorizedRateCommitDetail());
        ShipmentPreauthorizedRatedDetail shipmentPreauthorizedRatedDetail = new ShipmentPreauthorizedRatedDetail();
        shipmentPreauthorizedRatedDetail.setTotalBaseCharge(new Price(CurrencyType.USD, 25));
        shipmentPreauthorizedRatedDetail.setTotalNetCharge(new Price(CurrencyType.USD, 25));
        rate.setShipmentRateDetails(shipmentPreauthorizedRatedDetail);
        List<PackagePreauthorizedRatedDetail> ratedPackages = new ArrayList<>();
        PackagePreauthorizedRatedDetail packagePreauthorizedRatedDetail = new PackagePreauthorizedRatedDetail();
        ratedPackages.add(packagePreauthorizedRatedDetail);
        rate.setRatedPackages(ratedPackages);
        rate.getRatedPackages()
                .get(0)
                .setSurcharges(createShippingRateSurcharge());
        return rate;
    }

    /**
     * Method to create List<ShippingRateSurcharge>
     * 
     * @return List<ShippingRateSurcharge>
     */
    private List<ShippingRateSurcharge> createShippingRateSurcharge() {

        List<ShippingRateSurcharge> surchargeDetailList = new ArrayList<>();
        ShippingRateSurcharge surchargeDetail = new ShippingRateSurcharge();
        surchargeDetail.setAmount(0.9);
        surchargeDetail.setDescription("surcharge");
        surchargeDetail.setSurchargeType("Insured_value");
        surchargeDetailList.add(surchargeDetail);
        return surchargeDetailList;
    }
}
