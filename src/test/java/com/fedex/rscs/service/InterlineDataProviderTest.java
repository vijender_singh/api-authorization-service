package com.fedex.rscs.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.netflix.hystrix.exception.HystrixRuntimeException;

import com.fedex.rscs.client.rsss.RSSSClient;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.ServiceProcessorTerminalException;
import com.fedex.rscs.exception.client.ClientTerminalException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test cases to valid the functionality of {@link InterlineDataProvider} component.
 * 
 * @authorMohit.kumar
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class InterlineDataProviderTest {

    @Mock
    private RSSSClient rsssClient;

    private InterlineDataProvider interlineDataProvider;

    private static final String INTERLINE_ID = "US_CODE";
    private static final String ACCOUNT_NUMBER = "123456789";

    @Before
    public void setup() {

        interlineDataProvider = new InterlineDataProvider(rsssClient);
    }

    /**
     * Test case to validate the scenario if the
     * {@link InterlineDataProvider#getInterlineAccountInfo(String)} is called and RSSS client
     * responded with the interline details, then same information should be returned from this
     * method.
     * 
     */
    @Test
    public void testGetInterlineAccountInfoWhenValidateInterlineAccountInfoSuccess() {

        when(rsssClient.validateInterlineAccounts(INTERLINE_ID)).thenReturn(ACCOUNT_NUMBER);
        String actual = interlineDataProvider.getInterlineAccountInfo(INTERLINE_ID);
        verify(rsssClient, times(1)).validateInterlineAccounts(INTERLINE_ID);
        assertThat(actual, notNullValue());

    }

    /**
     * Test case to validate the scenario if the
     * {@link InterlineDataProvider#getInterlineAccountInfo(String)} is called and RSSS client
     * responded with exception, then same exception is thrown from this method.
     * 
     */
    @Test(expected = ServiceProcessorTerminalException.class)
    public void testGetInterlineAccountInfoWhenValidateInterlineAccountInfoFailure() {

        doThrow(new ClientTerminalException(ServiceErrorCode.INTERLINE_ACCOUNT_SERVICE_UNAVAILABLE)).when(rsssClient)
                .validateInterlineAccounts(INTERLINE_ID);
        interlineDataProvider.getInterlineAccountInfo(INTERLINE_ID);
    }

    /**
     * Test case to validate the scenario if the
     * {@link InterlineDataProvider#getInterlineAccountInfo(String)} is called and RSSS client
     * responded with HystrixRuntimeException, then ShipmentRatingProcessorTerminalException is
     * thrown from this method.
     * 
     */
    @Test(expected = ServiceProcessorTerminalException.class)
    public void testGetInterlineAccountInfoWhenValidateInterlineAccountInfoTimeout() {

        doThrow(new HystrixRuntimeException(HystrixRuntimeException.FailureType.TIMEOUT, null,
                "Unable to process request. Please try again later.", null, null)).when(rsssClient)
                        .validateInterlineAccounts(INTERLINE_ID);
        interlineDataProvider.getInterlineAccountInfo(INTERLINE_ID);
    }
}
