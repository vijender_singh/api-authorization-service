package com.fedex.rscs.client.srrs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.BaseOAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.test.web.client.MockRestServiceServer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fedex.common.cxs.dto.CXSEnvelope;
import com.fedex.common.cxs.dto.CXSError;
import com.fedex.common.cxs.exception.TerminalDbAccessException;
import com.fedex.rscs.client.srrs.dto.CommitDetail;
import com.fedex.rscs.client.srrs.dto.Currency;
import com.fedex.rscs.client.srrs.dto.PreauthorizedRateDetail;
import com.fedex.rscs.client.srrs.dto.PreauthorizedRateResource;
import com.fedex.rscs.client.srrs.dto.PreauthorizedRateResponse;
import com.fedex.rscs.client.srrs.dto.RateOption;
import com.fedex.rscs.client.srrs.dto.ServiceDescription;
import com.fedex.rscs.client.srrs.dto.ShipmentRateQuotesResource;
import com.fedex.rscs.client.srrs.dto.ShipmentRateQuotesResponse;
import com.fedex.rscs.client.srrs.dto.ShippingRateDetail;
import com.fedex.rscs.client.srrs.dto.ShippingRateOption;
import com.fedex.rscs.client.util.RetailShipmentCreationServiceClientUtil;
import com.fedex.rscs.config.AppConfig;
import com.fedex.rscs.exception.client.ClientTerminalException;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ShipmentSpecialServiceDetail;
import com.fedex.rscs.model.SpecialServiceDescriptionDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.common.CurrencyType;
import com.fedex.rscs.model.common.PreauthorizedRateCommitDetail;
import com.fedex.rscs.model.common.PreauthorizedRateServiceDetail;
import com.fedex.rscs.model.common.Price;
import com.fedex.rscs.model.common.ShipmentPreauthorizedRatedDetail;
import com.fedex.rscs.model.common.ShippingRateSurcharge;
import com.fedex.rscs.model.cshp.PreauthorizedRateData;
import com.fedex.rscs.model.cshp.ShippingRateData;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

/**
 * This covers all the test cases required to unit test the Rate service client operations.
 * 
 * @author 3900094
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SRRSClientImplTest {

    private static final String RATING_SERVICE_URI = "/retailshipping/fedexoffice/v1/ratequotes";
    private static final String PRE_AUTHORIZED_RATE_URI = "/retailshipping/fedexoffice/v1/preauthorizedrates";
    private static final String SERVICE_TYPE = "FEDEX_GROUND";

    @Mock
    private OAuth2RestTemplate restTemplate;

    @Mock
    private AppConfig appConfig;

    @InjectMocks
    private SRRSClientImpl service;

    private MockRestServiceServer mockServer;
    private RetailShipmentCreationServiceClientUtil clientUtil;

    private ObjectMapper mapper = new ObjectMapper();

    @Mock
    private ObjectMapper objectMapper;

    @InjectMocks
    private SRRSResponseMapper responseMapper;

    @Mock
    private SRRSRequestBuilder requestBuilder;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void initialize() {

        BaseOAuth2ProtectedResourceDetails resource = new BaseOAuth2ProtectedResourceDetails();
        resource.setTokenName("bearer");
        restTemplate = new OAuth2RestTemplate(resource);
        mockServer = MockRestServiceServer.createServer(restTemplate);
        restTemplate.getOAuth2ClientContext()
                .setAccessToken(new DefaultOAuth2AccessToken("accesstoken"));
        clientUtil = new RetailShipmentCreationServiceClientUtil();
        when(appConfig.getRateServiceUri()).thenReturn("");
        service = new SRRSClientImpl(restTemplate, appConfig, responseMapper, requestBuilder, clientUtil);
    }

    /**
     * Test case to validate the scenario if the rate service is called and get the required request
     * details and if the service returns the success response, then {@link SRRSClient} returns
     * required rate details.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testGetRateOptionsSuccess()
            throws JsonProcessingException {

        final CXSEnvelope<ShipmentRateQuotesResponse> mockResponse = createRateResponse();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();

        mockServer.expect(once(), requestTo(RATING_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        ShippingRateData actual = service.getRateOptions(workstationDetail, requestedShipmentDetail);

        mockServer.verify();
        assertThat(actual, notNullValue());
        assertThat(actual.getCommitDetails(), notNullValue());
        assertThat(actual.getTotalBaseCharges(), notNullValue());
    }

    /**
     * Test case to validate the scenario if the rate service is called and not able to return
     * required rate details and if the service returns the success response, then
     * {@link SRRSClient} throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testGetRateOptionsSuccessAndStatusIsCreated()
            throws JsonProcessingException {

        final CXSEnvelope<ShipmentRateQuotesResponse> mockResponse = createRateResponse();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();

        mockServer.expect(once(), requestTo(RATING_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        ShippingRateData actual = service.getRateOptions(workstationDetail, requestedShipmentDetail);
        assertThat(actual, nullValue());

    }

    /**
     * Test case to validate the scenario if the rate service is called and not able to return
     * required rate details and if the service returns the success response, then
     * {@link SRRSClient} throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testGetRateOptionsSuccessWithOutputNull()
            throws JsonProcessingException {

        final CXSEnvelope<ShipmentRateQuotesResponse> mockResponse = CXSEnvelope.success("");
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();

        mockServer.expect(once(), requestTo(RATING_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        ShippingRateData actual = service.getRateOptions(workstationDetail, requestedShipmentDetail);

        assertThat(actual, nullValue());

    }
    
    /**
     * Test case to validate the scenario if the rate service is called and not able to return
     * required rate details and if the service returns the success response, then
     * {@link SRRSClient} throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testGetRateOptionsSuccessWithNullMockResponse()
            throws JsonProcessingException {

        final CXSEnvelope<ShipmentRateQuotesResponse> mockResponse = null;
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();

        mockServer.expect(once(), requestTo(RATING_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        ShippingRateData actual = service.getRateOptions(workstationDetail, requestedShipmentDetail);

        assertThat(actual, nullValue());

    }
    
    /**
     * Test case to validate the scenario if the rate service is called and not able to return
     * required rate details and if the service returns the success response, then
     * {@link SRRSClient} throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testGetRateOptionsSuccessAndNullRateQuote()
            throws JsonProcessingException {

        final CXSEnvelope<ShipmentRateQuotesResponse> mockResponse = createRateResponse();
        mockResponse.getOutput()
                .setRateQuote(null);
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        mockServer.expect(once(), requestTo(RATING_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        ShippingRateData actual = service.getRateOptions(workstationDetail, requestedShipmentDetail);
        assertThat(actual, nullValue());

    }

    /**
     * Test case to validate the scenario if the rate service is called and not able to return
     * required rate details and if the service returns the success response, then
     * {@link SRRSClient} throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testGetRateOptionsSuccessAndNullRateOption()
            throws JsonProcessingException {

        final CXSEnvelope<ShipmentRateQuotesResponse> mockResponse = createRateResponse();
        mockResponse.getOutput()
                .getRateQuote()
                .setRateOptions(null);
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        mockServer.expect(once(), requestTo(RATING_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        ShippingRateData actual = service.getRateOptions(workstationDetail, requestedShipmentDetail);
        assertThat(actual, nullValue());

    }

    /**
     * Test case to validate the scenario if the rate service is called to get the rate details and
     * if the service returns the error response along with {@link CXSError} errors and exception
     * occurs while the response, then {@link SRRSClient} response should throw {@link SRRSClient}
     * with the default error code with http message error message as received in error response.
     * 
     * @throws IOException
     */
    @Test
    public void testGetRateOptionsWithNullResponse()
            throws IOException {

        final CXSEnvelope<ShipmentRateQuotesResponse> mockResponse = createRateResponse();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();

        mockServer.expect(once(), requestTo(RATING_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.BAD_REQUEST).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));

        expectedEx.expect(ClientTerminalException.class);
        service.getRateOptions(workstationDetail, requestedShipmentDetail);

        mockServer.verify();
    }

    /**
     * Test case to validate the scenario if the rate service is called to get the rate details and
     * if the service returns the error response along with {@link CXSError} errors and exception
     * occurs while the response, then {@link SRRSClient} response should throw {@link SRRSClient}
     * with the default error code with http message error message as received in error response.
     * 
     * @throws IOException
     */
    @Test
    public void testGetRateOptionsWithServiceUnavailable()
            throws IOException {

        final CXSEnvelope mockResponse = buildMockErrorResponse("RATING.SERVICE.UNAVAILABLE",
                "Rating service is not available.Please try again later.");

        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();

        mockServer.expect(once(), requestTo(RATING_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.SERVICE_UNAVAILABLE).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));

        expectedEx.expect(ClientTerminalException.class);
        service.getRateOptions(workstationDetail, requestedShipmentDetail);

        mockServer.verify();
    }

    /**
     * Test case to validate the scenario if the rate service is called to get the rate details and
     * if the service returns the error response along with {@link CXSError} errors and exception
     * occurs while the response, then {@link SRRSClient} response should throw {@link SRRSClient}
     * with the default error code with http message error message as received in error response.
     * 
     * @throws IOException
     */
    @Test
    public void testGetRateOptionsWithInternalServer()
            throws IOException {

        final CXSEnvelope mockResponse = buildMockErrorResponse("INTERNAL.SERVER.ERROR", "");

        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();

        mockServer.expect(once(), requestTo(RATING_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));

        expectedEx.expect(ClientTerminalException.class);
        service.getRateOptions(workstationDetail, requestedShipmentDetail);

        mockServer.verify();
    }

    /**
     * Method to create Rate Response Detail.
     * 
     * @return RateResponse
     */
    private CXSEnvelope<ShipmentRateQuotesResponse> createRateResponse() {

        ShipmentRateQuotesResponse response = new ShipmentRateQuotesResponse();
        ShipmentRateQuotesResource rateResp = new ShipmentRateQuotesResource();
        rateResp.setQuoteDate("2020-07-29T10:30:00.812Z");
        ShippingRateOption shippingRateOption = new ShippingRateOption();

        ServiceDescription serviceDescription = createServiceDescriptionDetail();

        CommitDetail commitInfo = new CommitDetail();
        commitInfo.setCommitTimestamp("2020-07-29T10:30:00.812Z");
        commitInfo.setDayOfWeek("SAT");

        shippingRateOption.setServiceDetails(serviceDescription);
        shippingRateOption.setPackagingType("YOUR_PACKAGING");
        shippingRateOption.setSignatureOption("SERVICE_DEFAULT");
        shippingRateOption.setHal(true);
        shippingRateOption.setCommitDetails(commitInfo);
        shippingRateOption.setRateDetails(createRateDetails());

        List<ShippingRateOption> rateDetails = new ArrayList<>();
        rateDetails.add(shippingRateOption);

        rateResp.setRateOptions(rateDetails);
        response.setRateQuote(rateResp);
        return CXSEnvelope.success(response);
    }

    /**
     * Method to create Rate Details
     * 
     * @return ShippingRateDetail
     */
    private ShippingRateDetail createRateDetails() {

        ShippingRateDetail shippingRateDetail = new ShippingRateDetail();
        shippingRateDetail.setCurrency(Currency.valueOf("USD"));
        shippingRateDetail.setRatingType(RateOption.valueOf("ONE_RATE"));
        shippingRateDetail.setTotalBaseCharge(10.6);
        shippingRateDetail.setTotalRebates(5.0);
        shippingRateDetail.setTotalNetCharges(15.6);
        shippingRateDetail.setTotalSurcharges(0.21);
        shippingRateDetail.setTotalTaxes(5.00);
        shippingRateDetail.setTotalFreightDiscounts(1.21);
        shippingRateDetail.setTotalNetFreight(0.32);
        shippingRateDetail.setTotalNetFedExCharge(2.32);
        shippingRateDetail.setTotalDutiesAndTaxes(0.87);
        shippingRateDetail.setTotalAncillaryFeesAndTaxes(0.56);
        shippingRateDetail.setTotalDutiesTaxesAndFees(2.88);
        shippingRateDetail.setTotalNetChargeWithDutiesAndTaxes(3.9);
        List<ShippingRateSurcharge> surcharges = new ArrayList<>();
        ShippingRateSurcharge surcharge = new ShippingRateSurcharge();
        surcharge.setAmount(45.00);
        surcharge.setDescription("Declared Value");
        surcharge.setSurchargeType("INSURED_VALUE");
        surcharges.add(surcharge);
        shippingRateDetail.setSurcharges(surcharges);
        return shippingRateDetail;
    }

    /**
     * Method to create Service Description Detail.
     * 
     * @return ServiceDescription
     */
    private ServiceDescription createServiceDescriptionDetail() {

        ServiceDescription servcDetail = new ServiceDescription();
        servcDetail.setServiceType(SERVICE_TYPE);
        servcDetail.setDescription(SERVICE_TYPE);
        servcDetail.setServiceName(SERVICE_TYPE);

        return servcDetail;
    }

    /**
     * Method to create RequestedShipmentDetail
     * 
     * @return RequestedShipmentDetail
     */
    private RequestedShipmentDetail createRequestedShipmentDetail() {

        RequestedShipmentDetail requestedShipmentDetail = new RequestedShipmentDetail();

        ShipmentSpecialServiceDetail shipmentSpecialServiceDetail = new ShipmentSpecialServiceDetail();
        List<SpecialServiceDescriptionDetail> speSerList = new ArrayList<>();
        speSerList.add(new SpecialServiceDescriptionDetail("FEDEX_ONE_RATE", null, null, null));
        shipmentSpecialServiceDetail.setSpecialServicesRequestedType(speSerList);
        requestedShipmentDetail.setSpecialServicesRequested(shipmentSpecialServiceDetail);
        return requestedShipmentDetail;
    }

    /**
     * Method to create WorkstationDetails
     * 
     * @return
     */
    private WorkstationDetail createWorkstationDetail() {

        WorkstationDetail workstationDetail = new WorkstationDetail();
        workstationDetail.setAccountNumber("375200007");
        workstationDetail.setMeterNumber("006990060");
        workstationDetail.setSoftwareId("SSFO");
        workstationDetail.setAppVersionId("2021");
        return workstationDetail;
    }

    /**
     * Method to build the mock {@link CXSEnvelope} error response envelop for the provided error
     * code and message.
     */
    private CXSEnvelope buildMockErrorResponse(
            String errorCode,
            String errorMessage) {

        return CXSEnvelope.error(new CXSError(errorCode, errorMessage));
    }
    
    /**
     * Test case to validate the scenario if the rate service is called and get the required request
     * details and if the service returns the success response, then {@link SRRSClient} returns required
     * rate details.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testGetPreauthorizedRatesSuccess()
            throws JsonProcessingException {

        final CXSEnvelope<PreauthorizedRateResponse> mockResponse = createPreAuthRateResponse();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();

        mockServer.expect(once(), requestTo(PRE_AUTHORIZED_RATE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        PreauthorizedRateData actual = service.getPreauthorizedRates(workstationDetail, requestedShipmentDetail);

        mockServer.verify();
        assertThat(actual, notNullValue());
        assertThat(actual.getCommitDetails(), notNullValue());
        assertThat(actual.getShipmentRateDetails()
                .getTotalBaseCharge(), notNullValue());
    }

    /**
     * Test case to validate the scenario if the rate service is called and not able to return required
     * rate details and if the service returns the success response, then {@link SRRSClient} throws
     * Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testGetPreauthorizedRatesSuccessAndStatusIsCreated()
            throws JsonProcessingException {

        final CXSEnvelope<PreauthorizedRateResponse> mockResponse = createPreAuthRateResponse();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();

        mockServer.expect(once(), requestTo(PRE_AUTHORIZED_RATE_URI))
                .andRespond(withStatus(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        PreauthorizedRateData actual = service.getPreauthorizedRates(workstationDetail, requestedShipmentDetail);
        assertThat(actual, nullValue());

    }

    /**
     * Test case to validate the scenario if the rate service is called and not able to return required
     * rate details and if the service returns the success response, then {@link SRRSClient} throws
     * Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testGetPreauthorizedRatesSuccessWithOutputNull()
            throws JsonProcessingException {

        final CXSEnvelope<ShipmentRateQuotesResponse> mockResponse = CXSEnvelope.success("");
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();

        mockServer.expect(once(), requestTo(PRE_AUTHORIZED_RATE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        PreauthorizedRateData actual = service.getPreauthorizedRates(workstationDetail, requestedShipmentDetail);

        assertThat(actual, nullValue());

    }

    /**
     * Test case to validate the scenario if the rate service is called and not able to return required
     * rate details and if the service returns the success response, then {@link SRRSClient} throws
     * Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testGetPreauthorizedRatesSuccessWithNullMockResponse()
            throws JsonProcessingException {

        final CXSEnvelope<ShipmentRateQuotesResponse> mockResponse = null;
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();

        mockServer.expect(once(), requestTo(PRE_AUTHORIZED_RATE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        PreauthorizedRateData actual = service.getPreauthorizedRates(workstationDetail, requestedShipmentDetail);

        assertThat(actual, nullValue());

    }

    /**
     * Test case to validate the scenario if the rate service is called and not able to return required
     * rate details and if the service returns the success response, then {@link SRRSClient} throws
     * Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testGetPreauthorizedRatesSuccessAndNullPreauthorizedRate()
            throws JsonProcessingException {

        final CXSEnvelope<PreauthorizedRateResponse> mockResponse = createPreAuthRateResponse();
        mockResponse.getOutput()
                .setPreauthorizedRate(null);
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        mockServer.expect(once(), requestTo(PRE_AUTHORIZED_RATE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        PreauthorizedRateData actual = service.getPreauthorizedRates(workstationDetail, requestedShipmentDetail);
        assertThat(actual, nullValue());

    }

    /**
     * Test case to validate the scenario if the rate service is called and not able to return required
     * rate details and if the service returns the success response, then {@link SRRSClient} throws
     * Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testGetPreauthorizedRatesSuccessAndNullPreauthorizedRateDetail()
            throws JsonProcessingException {

        final CXSEnvelope<PreauthorizedRateResponse> mockResponse = createPreAuthRateResponse();
        mockResponse.getOutput()
                .getPreauthorizedRate()
                .setPreauthorizedRateDetail(null);
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        mockServer.expect(once(), requestTo(PRE_AUTHORIZED_RATE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        PreauthorizedRateData actual = service.getPreauthorizedRates(workstationDetail, requestedShipmentDetail);
        assertThat(actual, nullValue());

    }

    /**
     * Test case to validate the scenario if the rate service is called to get the rate details and if
     * the service returns the error response along with {@link CXSError} errors and exception occurs
     * while the response, then {@link SRRSClient} response should throw {@link SRRSClient} with the
     * default error code with http message error message as received in error response.
     * 
     * @throws IOException
     */
    @Test
    public void testGetPreauthorizedRatesWithNullResponse()
            throws IOException {

        final CXSEnvelope<PreauthorizedRateResponse> mockResponse = createPreAuthRateResponse();
        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();

        mockServer.expect(once(), requestTo(PRE_AUTHORIZED_RATE_URI))
                .andRespond(withStatus(HttpStatus.BAD_REQUEST).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));

        expectedEx.expect(ClientTerminalException.class);
        service.getPreauthorizedRates(workstationDetail, requestedShipmentDetail);

        mockServer.verify();
    }

    /**
     * Test case to validate the scenario if the rate service is called to get the rate details and if
     * the service returns the error response along with {@link CXSError} errors and exception occurs
     * while the response, then {@link SRRSClient} response should throw {@link SRRSClient} with the
     * default error code with http message error message as received in error response.
     * 
     * @throws IOException
     */
    @Test
    public void testGetPreauthorizedRatesWithServiceUnavailable()
            throws IOException {

        final CXSEnvelope mockResponse = buildMockErrorResponse("RATING.SERVICE.UNAVAILABLE",
                "Rating service is not available.Please try again later.");

        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();

        mockServer.expect(once(), requestTo(PRE_AUTHORIZED_RATE_URI))
                .andRespond(withStatus(HttpStatus.SERVICE_UNAVAILABLE).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));

        expectedEx.expect(ClientTerminalException.class);
        service.getPreauthorizedRates(workstationDetail, requestedShipmentDetail);

        mockServer.verify();
    }

    /**
     * Test case to validate the scenario if the rate service is called to get the rate details and if
     * the service returns the error response along with {@link CXSError} errors and exception occurs
     * while the response, then {@link SRRSClient} response should throw {@link SRRSClient} with the
     * default error code with http message error message as received in error response.
     * 
     * @throws IOException
     */
    @Test
    public void testGetPreauthorizedRatesWithInternalServer()
            throws IOException {

        final CXSEnvelope mockResponse = buildMockErrorResponse("INTERNAL.SERVER.ERROR", "");

        WorkstationDetail workstationDetail = createWorkstationDetail();
        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();

        mockServer.expect(once(), requestTo(PRE_AUTHORIZED_RATE_URI))
                .andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));

        expectedEx.expect(ClientTerminalException.class);
        service.getPreauthorizedRates(workstationDetail, requestedShipmentDetail);

        mockServer.verify();
    }

    /**
     * Method to create Rate Response Detail.
     * 
     * @return RateResponse
     */
    private CXSEnvelope<PreauthorizedRateResponse> createPreAuthRateResponse() {

        PreauthorizedRateResponse response = new PreauthorizedRateResponse();
        PreauthorizedRateResource rateResp = new PreauthorizedRateResource();
        PreauthorizedRateDetail rateDetail = new PreauthorizedRateDetail();
        rateDetail.setDeliveryTimestamp("2020-07-29T10:30:00.812Z");

        rateDetail.setServiceType("PRIORITY_OVERNIGHT");
        rateDetail.setPackagingType("YOUR_PACKAGING");
        rateDetail.setSignatureOption("Direct");
        rateDetail.setServiceDetails(new PreauthorizedRateServiceDetail());
        rateDetail.setCommitDetails(new PreauthorizedRateCommitDetail());

        ShipmentPreauthorizedRatedDetail shipmentPreauthorizedRatedDetail = new ShipmentPreauthorizedRatedDetail();
        shipmentPreauthorizedRatedDetail.setTotalBaseCharge(new Price(CurrencyType.USD, 25));
        rateDetail.setShipmentRateDetails(shipmentPreauthorizedRatedDetail);

        rateResp.setPreauthorizedRateDetail(rateDetail);
        response.setPreauthorizedRate(rateResp);
        return CXSEnvelope.success(response);
    }

}
