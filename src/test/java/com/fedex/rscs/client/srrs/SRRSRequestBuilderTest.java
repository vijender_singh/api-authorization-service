package com.fedex.rscs.client.srrs;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.fedex.rscs.client.srrs.dto.Address;
import com.fedex.rscs.client.srrs.dto.AddressClassification;
import com.fedex.rscs.client.srrs.dto.ShipmentRateQuotesRequest;
import com.fedex.rscs.dto.HomeDeliveryType;
import com.fedex.rscs.dto.LocationType;
import com.fedex.rscs.dto.PhoneNumber;
import com.fedex.rscs.model.AddressDetail;
import com.fedex.rscs.model.AssociatedAccountDetail;
import com.fedex.rscs.model.BatteryClassificationType;
import com.fedex.rscs.model.ContactDetail;
import com.fedex.rscs.model.DimensionDetail;
import com.fedex.rscs.model.HoldAtLocDetail;
import com.fedex.rscs.model.HomeDelvDetail;
import com.fedex.rscs.model.InsuredValueDetail;
import com.fedex.rscs.model.InterlineShippingData;
import com.fedex.rscs.model.LabelSpecificationDetail;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.PaymentDetail;
import com.fedex.rscs.model.RequestedPackageLineItemDetail;
import com.fedex.rscs.model.RequestedPackageSpecialServices;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ShipmentSpecialServiceDetail;
import com.fedex.rscs.model.SpecialServiceDescriptionDetail;
import com.fedex.rscs.model.SpecialServiceDetail;
import com.fedex.rscs.model.VariationOptionDetail;
import com.fedex.rscs.model.WeightDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.cshp.CreditCardData;
import com.fedex.rscs.model.cshp.Price;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertTrue;
import static org.hamcrest.Matchers.equalTo;

/**
 * Test class for unit testing of SRRSRequestBuilde class
 * 
 * @author 3900094
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SRRSRequestBuilderTest {

    @InjectMocks
    private SRRSRequestBuilder requestBuilder;

    private static final String LOCATION_CODE = "DFWD";

    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequest() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.setExpressAfterPickup(true);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationId(), notNullValue());
        assertTrue(actualRequest.getShipmentRateQuoteRequest()
                .getLocationDetails()
                .isExpressAfterPickup());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getDisplayName(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationType(), notNullValue());

    }
    
    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a valid
     * request.
     */
    @Test
    public void testBuildRateRequestWithHomeDelivery() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetailWithHomeDelivery();
        requestedShipmentDetail.setExpressAfterPickup(true);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryDetail(), notNullValue());
        assertTrue(actualRequest.getShipmentRateQuoteRequest()
                .getLocationDetails()
                .isExpressAfterPickup());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryDetail()
                .getHomeDeliveryType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryDetail()
                .getPhoneNumber(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryDetail()
                .getDeliveryDate(), notNullValue());

    }

    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a valid
     * request.
     */
    @Test
    public void testBuildRateRequestWithHomeDeliveryWithNullPhoneNumber() {

        RequestedShipmentDetail requestedShipmentDetail =
                createRequestedShipmentDetailWithHomeDeliveryWithNullPhoneNumber();
        requestedShipmentDetail.setExpressAfterPickup(true);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryDetail(), notNullValue());
        assertTrue(actualRequest.getShipmentRateQuoteRequest()
                .getLocationDetails()
                .isExpressAfterPickup());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryDetail()
                .getHomeDeliveryType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryDetail()
                .getPhoneNumber(), nullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryDetail()
                .getDeliveryDate(), notNullValue());

    }

    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequestIfContactNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .setContact(null);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationId(), notNullValue());

    }

    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequestIfSignatureNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .setSignatureOption(null);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationId(), notNullValue());

    }

    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequestIfPartyContactNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getRecipient()
                .setContact(null);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationId(), notNullValue());

    }

    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequestIfOriginNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.setOrigin(null);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationId(), notNullValue());

    }

    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequestIfAccountAndResponsiblePartyNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getShippingChargesPayment()
                .setResponsibleParty(null);
        requestedShipmentDetail.getShippingChargesPayment()
                .setPayor(null);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationId(), notNullValue());

    }

    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequestIfPaymentOptionNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.setShippingChargesPayment(null);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationId(), notNullValue());

    }

    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequestIfPhoneUsageAndUnitsNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getRecipient()
                .getContact()
                .setPhoneUsage(null);
        requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getDimensions()
                .setUnits(null);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationId(), notNullValue());

    }

    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequestWhenServiceRequestedNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getSpecialServicesRequested()
                .setSpecialServicesRequestedType(null);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceType(), notNullValue());

    }


    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequestWhenHoldLocationAddressNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .setAddress(null);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceType(), notNullValue());

    }
    
    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequestWhenHoldLocationNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getSpecialServicesRequested()
                .setHoldAtLocationDetail(null);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceType(), notNullValue());

    }

    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequestWhenPackageLineItemsDimensionsNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .setDimensions(null);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());

    }

    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequestWhenPackageLineItemsDryIceWeightNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getDimensions()
                .setUnits(null);
        requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .setDryIceWeightDetails(null);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());

    }

    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequestIfInterlineCodeNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getInterlineShippingData()
                .setInterlineId(null);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationId(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getPaymentOption()
                .getInterlineId(), nullValue());

    }

    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequestIfInterlineShippingDataNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.setInterlineShippingData(null);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getWeight(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRequestedPackageLineItems()
                .get(0)
                .getSpecialServicesRequested()
                .get(0)
                .getSpecialServiceType(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationId(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getPaymentOption()
                .getInterlineId(), nullValue());

    }

    /**
     * Create RequestedShipmentDetail
     * 
     * @return
     */
    private RequestedShipmentDetail createRequestedShipmentDetail() {

        RequestedShipmentDetail requestedShipmentDetail = new RequestedShipmentDetail();
        requestedShipmentDetail.setServiceType("PRIORITY_OVERNIGHT");
        requestedShipmentDetail.setPackagingType("FEDEX_PAK");
        requestedShipmentDetail.setTransactionLocationCode(LOCATION_CODE);
        requestedShipmentDetail.setSender(createShipperPartyDetail());
        requestedShipmentDetail.setOrigin(createShipperPartyDetail());
        requestedShipmentDetail.setRecipient(createShipperPartyDetail());
        requestedShipmentDetail.setVariationOptions(createVariationOptionDetail());
        requestedShipmentDetail.setSpecialServicesRequested(createShipmentSpecialService());
        requestedShipmentDetail.setPackageLineItems(createRequestedPackageLineItems());
        requestedShipmentDetail.setShippingChargesPayment(createPayment());
        requestedShipmentDetail.setLabelSpecification(new LabelSpecificationDetail());
        requestedShipmentDetail.setPackageCount("3");
        List<String> actions = new ArrayList<>();
        actions.add("STRONG_VALIDATION");
        requestedShipmentDetail.setActions(actions);
        requestedShipmentDetail.setInterlineShippingData(
                new InterlineShippingData("100127", "100037", "008", "ALLEGIANT AIRLINES", "1234", "164462765"));
        return requestedShipmentDetail;
    }

    /**
     * Create WorkstationDetails
     * 
     * @return
     */
    private WorkstationDetail createWorkstationDetails() {

        WorkstationDetail workstationDetail = new WorkstationDetail();
        workstationDetail.setAccountNumber("375200007");
        workstationDetail.setAppName("FUSE");
        workstationDetail.setAppVersionId("fuseid");
        workstationDetail.setDeviceId("FUSE");
        workstationDetail.setGroundAccountNumber("375200007");
        workstationDetail.setSoftwareId("2020");
        return workstationDetail;

    }

    /**
     * Create ShipperPartyDetail
     * 
     * @return
     */
    private PartyDetail createShipperPartyDetail() {

        List<String> streetLines = new ArrayList<>();
        streetLines.add("4071 North Trail");

        AddressDetail addressDetail = new AddressDetail();
        addressDetail.setPostalCode("75023");
        addressDetail.setCountryCode("US");
        addressDetail.setStreetLines(streetLines);
        return new PartyDetail(addressDetail, createContactDetail(), "038000004",
                new CreditCardData("4005554444444460", "VISA", "12/2020"), "FEDEX_EXPRESS");
    }

    /**
     * Create ContactDetail
     * 
     * @return
     */
    private ContactDetail createContactDetail() {

        ContactDetail contactDetail = new ContactDetail();
        contactDetail.setContactId("test123");
        contactDetail.setFullName("Dhruv Kumar Sood");
        contactDetail.setPhoneNumber("32456764");
        contactDetail.setPhoneUsage("PRIMARY");
        return contactDetail;
    }

    /**
     * Create List<VariationOptionDetails>
     * 
     * @return
     */
    private List<VariationOptionDetail> createVariationOptionDetail() {

        List<VariationOptionDetail> variationOptionDetails = new ArrayList<>();
        List<String> listOfVariationValue = new ArrayList<>();
        listOfVariationValue.add("SUPPORTED");
        VariationOptionDetail variationOptionDetail = new VariationOptionDetail("ISS", listOfVariationValue);
        variationOptionDetail.setId("ISS");

        variationOptionDetail.setValues(listOfVariationValue);
        variationOptionDetails.add(variationOptionDetail);

        return variationOptionDetails;
    }

    /**
     * Create ShipmentSpecialServiceDetail
     * 
     * @return
     */
    private ShipmentSpecialServiceDetail createShipmentSpecialService() {

        ShipmentSpecialServiceDetail shipSpecialService = new ShipmentSpecialServiceDetail();
        shipSpecialService.setSpecialServicesRequestedType(createShipmentSpecialServiceDetail());
        shipSpecialService.setHoldAtLocationDetail(createHoldAtLocationDetail());

        return shipSpecialService;
    }

    /**
     * Create HoldAtLocationDetail
     * 
     * @return
     */
    private HoldAtLocDetail createHoldAtLocationDetail() {

        HoldAtLocDetail holdAtLocDetail = new HoldAtLocDetail();
        holdAtLocDetail.setAddress(new AddressDetail(new ArrayList<String>(), "", "TX", "75024", "US", true));
        holdAtLocDetail.setContact(createContactDetail());
        holdAtLocDetail.setLocationId(LOCATION_CODE);
        holdAtLocDetail.setDisplayName("FedEx OnSite");
        holdAtLocDetail.setLocationType(LocationType.FEDEX_ONSITE.name());

        return holdAtLocDetail;
    }

    /**
     * Create ShipmentSpecialServiceDetail
     * 
     * @return
     */
    private List<SpecialServiceDescriptionDetail> createShipmentSpecialServiceDetail() {
        List<SpecialServiceDescriptionDetail> speSerList = new ArrayList<>();
        speSerList.add(new SpecialServiceDescriptionDetail("FEDEX_ONE_RATE", "FEDEX_ONE_RATE", "FEDEX_ONE_RATE", "FEDEX_ONE_RATE"));

        return speSerList;
    }

    /**
     * Create List<RequestedPackageLineItems>
     * 
     * @return
     */
    private List<RequestedPackageLineItemDetail> createRequestedPackageLineItems() {

        List<RequestedPackageLineItemDetail> requestedPackageLineItems = new ArrayList<>();
        RequestedPackageLineItemDetail requestedPackageLineItem = new RequestedPackageLineItemDetail();
        requestedPackageLineItem.setDimensions(new DimensionDetail(1, 5, 6, "IN"));
        requestedPackageLineItem.setInsuredValue(new InsuredValueDetail("USD", "100"));
        requestedPackageLineItem.setWeight(new WeightDetail("LB", "5"));
        requestedPackageLineItem.setSequenceNumber(1);
        requestedPackageLineItem.setGroupNumber(2);
        requestedPackageLineItem.setGroupPackageCount(3);
        requestedPackageLineItem.setPackageSpecialServicesRequested(createPackageSpecialSerivce());
        requestedPackageLineItem.setSpecialHandlingDetail("INSPECTED");
        SpecialServiceDetail signatureOption = new SpecialServiceDetail();
        signatureOption.setSpecialServiceSubType("SERVICE_DEFAULT");
        requestedPackageLineItem.setSignatureOption(signatureOption);
        requestedPackageLineItems.add(requestedPackageLineItem);

        return requestedPackageLineItems;
    }

    /**
     * Create PaymentDetail
     * 
     * @return
     */
    private PaymentDetail createPayment() {

        List<AssociatedAccountDetail> accountDetailList = new ArrayList<>();
        accountDetailList.add(new AssociatedAccountDetail());

        return new PaymentDetail("NON_ACCOUNT", createShipperPartyDetail(), "THIRD_PARTY", accountDetailList,
                new Price("USD", 100.0));

    }

    /**
     * Create createPackageSpecialSerivce
     * 
     * @return
     */
    private RequestedPackageSpecialServices createPackageSpecialSerivce() {

        RequestedPackageSpecialServices packSpecService = new RequestedPackageSpecialServices();

        List<SpecialServiceDescriptionDetail> specSerReqDet = new ArrayList<>();
        specSerReqDet.add(new SpecialServiceDescriptionDetail("SERVICE_DEFAULT", "SERVICE_DEFAULT", "SERVICE_DEFAULT", "SERVICE_DEFAULT"));

        packSpecService.setSpecialServicesRequested(specSerReqDet);
        packSpecService.setDryIceWeightDetails(new WeightDetail("LB", "5"));
        
        List<BatteryClassificationType> batteryClassifications = new ArrayList<>();
        batteryClassifications.add(BatteryClassificationType.LITHIUM_ION_CONTAINED_IN_EQUIPMENT);
        
        packSpecService.setBatteryClassifications(batteryClassifications);
        return packSpecService;
    }

    /**
     * Test case to validate if recipient address is residential, then recipient address should have
     * address classification as HOME, else it should be BUSINESS.
     */
    @Test
    public void testBuildRateRequestRecipientAddressDetails() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);
        AddressDetail expectedAddress = requestedShipmentDetail.getRecipient()
                .getAddress();
        Address actualAddress = actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress();
        assertThat(actualAddress.getCountryCode(), equalTo(expectedAddress.getCountryCode()));
        assertThat(actualAddress.getPostalCode(), equalTo(expectedAddress.getPostalCode()));
        assertThat(actualAddress.getStateOrProvinceCode(), equalTo(expectedAddress.getStateOrProvinceCode()));
        assertThat(actualAddress.getStreetLines(), equalTo(expectedAddress.getStreetLines()));
        assertThat(actualAddress.getAddressClassification(), equalTo(AddressClassification.BUSINESS));

        // set the residential flag true to verify if the adress classification is returned HOME.
        requestedShipmentDetail.getRecipient()
                .getAddress()
                .setResidential(true);
        actualRequest = requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);
        actualAddress = actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress();
        assertThat(actualAddress.getCountryCode(), equalTo(expectedAddress.getCountryCode()));
        assertThat(actualAddress.getPostalCode(), equalTo(expectedAddress.getPostalCode()));
        assertThat(actualAddress.getStateOrProvinceCode(), equalTo(expectedAddress.getStateOrProvinceCode()));
        assertThat(actualAddress.getStreetLines(), equalTo(expectedAddress.getStreetLines()));
        assertThat(actualAddress.getAddressClassification(), equalTo(AddressClassification.HOME));
    }

    /**
     * Test case to validate if sender address is residential, then shipper address should have
     * address classification as HOME, else it should be BUSINESS.
     */
    @Test
    public void testBuildRateRequestShipperAddressDetails() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);
        AddressDetail expectedAddress = requestedShipmentDetail.getSender()
                .getAddress();
        Address actualAddress = actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress();
        assertThat(actualAddress.getCountryCode(), equalTo(expectedAddress.getCountryCode()));
        assertThat(actualAddress.getPostalCode(), equalTo(expectedAddress.getPostalCode()));
        assertThat(actualAddress.getStateOrProvinceCode(), equalTo(expectedAddress.getStateOrProvinceCode()));
        assertThat(actualAddress.getStreetLines(), equalTo(expectedAddress.getStreetLines()));
        assertThat(actualAddress.getAddressClassification(), equalTo(AddressClassification.BUSINESS));

        // set the residential flag true to verify if the adress classification is returned HOME.
        requestedShipmentDetail.getSender()
                .getAddress()
                .setResidential(true);
        actualRequest = requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);
        actualAddress = actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress();
        assertThat(actualAddress.getCountryCode(), equalTo(expectedAddress.getCountryCode()));
        assertThat(actualAddress.getPostalCode(), equalTo(expectedAddress.getPostalCode()));
        assertThat(actualAddress.getStateOrProvinceCode(), equalTo(expectedAddress.getStateOrProvinceCode()));
        assertThat(actualAddress.getStreetLines(), equalTo(expectedAddress.getStreetLines()));
        assertThat(actualAddress.getAddressClassification(), equalTo(AddressClassification.HOME));
    }
    
    /**
     * Test case to validate the scenario if the {@link buildRateRequest} is called then we get a
     * valid request.
     */
    @Test
    public void testBuildRateRequestWithNullLocationType() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .setLocationType(null);
        ShipmentRateQuotesRequest actualRequest =
                requestBuilder.buildRateRequest(createWorkstationDetails(), requestedShipmentDetail);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getShipmentRateQuoteRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationType(), nullValue());

    }

    /**
     * Create RequestedShipmentDetail
     * 
     * @return
     */
    private RequestedShipmentDetail createRequestedShipmentDetailWithHomeDelivery() {

        RequestedShipmentDetail requestedShipmentDetail = new RequestedShipmentDetail();
        requestedShipmentDetail.setServiceType("GROUND_HOME_DELIVERY");
        requestedShipmentDetail.setPackagingType("FEDEX_PAK");
        requestedShipmentDetail.setTransactionLocationCode(LOCATION_CODE);
        requestedShipmentDetail.setSender(createShipperPartyDetail());
        requestedShipmentDetail.setOrigin(createShipperPartyDetail());
        requestedShipmentDetail.setRecipient(createShipperPartyDetail());
        requestedShipmentDetail.setVariationOptions(createVariationOptionDetail());
        requestedShipmentDetail.setSpecialServicesRequested(createShipmentSpecialServiceWithHomeDeliveryDetails());
        requestedShipmentDetail.setPackageLineItems(createRequestedPackageLineItems());
        requestedShipmentDetail.setShippingChargesPayment(createPayment());
        requestedShipmentDetail.setLabelSpecification(new LabelSpecificationDetail());
        requestedShipmentDetail.setPackageCount("3");
        List<String> actions = new ArrayList<>();
        actions.add("STRONG_VALIDATION");
        requestedShipmentDetail.setActions(actions);
        return requestedShipmentDetail;
    }
    
    /**
     * Create ShipmentSpecialServiceDetail
     * 
     * @return shipSpecialService
     */
    private ShipmentSpecialServiceDetail createShipmentSpecialServiceWithHomeDeliveryDetails() {

        ShipmentSpecialServiceDetail shipSpecialService = new ShipmentSpecialServiceDetail();
        shipSpecialService.setSpecialServicesRequestedType(createShipmentSpecialServiceDetailWithHomeDelivery());
        shipSpecialService.setHomeDeliveryDetail(createHomeDeliveryDetail());

        return shipSpecialService;
    }

    /**
     * Method to set special service with home delivery
     * 
     * @return speSerList
     */
    private List<SpecialServiceDescriptionDetail> createShipmentSpecialServiceDetailWithHomeDelivery() {

        List<SpecialServiceDescriptionDetail> speSerList = new ArrayList<>();
        speSerList.add(new SpecialServiceDescriptionDetail("HOME_DELIVERY_PREMIUM", "", "", ""));

        return speSerList;
    }
    
    /**
     * Create Home Delivery details
     * 
     * @return
     */
    private HomeDelvDetail createHomeDeliveryDetail() {

        HomeDelvDetail homeDelvDetail = new HomeDelvDetail();
        homeDelvDetail.setDeliveryDate("2021-01-11");
        homeDelvDetail.setHomeDeliveryType(HomeDeliveryType.DATE_CERTAIN);
        homeDelvDetail.setPhoneNumber(createPhoneNumber());

        return homeDelvDetail;
    }

    /**
     * This method return Phone number
     * 
     * @return phoneNumber
     */
    private PhoneNumber createPhoneNumber() {

        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setExtension("54321");
        phoneNumber.setNumber("4699318155");
        return phoneNumber;
    }

    /**
     * Create RequestedShipmentDetail
     * 
     * @return
     */
    private RequestedShipmentDetail createRequestedShipmentDetailWithHomeDeliveryWithNullPhoneNumber() {

        RequestedShipmentDetail requestedShipmentDetail = new RequestedShipmentDetail();
        requestedShipmentDetail.setServiceType("GROUND_HOME_DELIVERY");
        requestedShipmentDetail.setPackagingType("FEDEX_PAK");
        requestedShipmentDetail.setTransactionLocationCode(LOCATION_CODE);
        requestedShipmentDetail.setSender(createShipperPartyDetail());
        requestedShipmentDetail.setOrigin(createShipperPartyDetail());
        requestedShipmentDetail.setRecipient(createShipperPartyDetail());
        requestedShipmentDetail.setVariationOptions(createVariationOptionDetail());
        requestedShipmentDetail.setSpecialServicesRequested(createShipmentSpecialServiceWithHomeDeliveryDetailsWithNullPhoneNumber());
        requestedShipmentDetail.setPackageLineItems(createRequestedPackageLineItems());
        requestedShipmentDetail.setShippingChargesPayment(createPayment());
        requestedShipmentDetail.setLabelSpecification(new LabelSpecificationDetail());
        requestedShipmentDetail.setPackageCount("3");
        List<String> actions = new ArrayList<>();
        actions.add("STRONG_VALIDATION");
        requestedShipmentDetail.setActions(actions);
        return requestedShipmentDetail;
    }
    
    /**
     * Create ShipmentSpecialServiceDetail
     * 
     * @return
     */
    private ShipmentSpecialServiceDetail createShipmentSpecialServiceWithHomeDeliveryDetailsWithNullPhoneNumber() {

        ShipmentSpecialServiceDetail shipSpecialService = new ShipmentSpecialServiceDetail();
        shipSpecialService.setSpecialServicesRequestedType(createShipmentSpecialServiceDetailWithHomeDelivery());
        shipSpecialService.setHomeDeliveryDetail(createHomeDeliveryDetailWithNullPhoneNumber());

        return shipSpecialService;
    }
    
    /**
     * Create Home Delivery details
     * 
     * @return
     */
    private HomeDelvDetail createHomeDeliveryDetailWithNullPhoneNumber() {

        HomeDelvDetail homeDelvDetail = new HomeDelvDetail();
        homeDelvDetail.setDeliveryDate("2021-01-11");
        homeDelvDetail.setHomeDeliveryType(HomeDeliveryType.DATE_CERTAIN);
        homeDelvDetail.setPhoneNumber(null);

        return homeDelvDetail;
    }
}
