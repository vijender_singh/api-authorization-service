package com.fedex.rscs.client.srrs;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.fedex.rscs.client.srrs.dto.CommitDetail;
import com.fedex.rscs.client.srrs.dto.Currency;
import com.fedex.rscs.client.srrs.dto.PreauthorizedRateDetail;
import com.fedex.rscs.client.srrs.dto.PreauthorizedRateResource;
import com.fedex.rscs.client.srrs.dto.RateOption;
import com.fedex.rscs.client.srrs.dto.ServiceDescription;
import com.fedex.rscs.client.srrs.dto.ShipmentRateQuotesResource;
import com.fedex.rscs.client.srrs.dto.ShippingRateDetail;
import com.fedex.rscs.client.srrs.dto.ShippingRateOption;
import com.fedex.rscs.model.common.CurrencyType;
import com.fedex.rscs.model.common.PreauthorizedRateCommitDetail;
import com.fedex.rscs.model.common.PreauthorizedRateServiceDetail;
import com.fedex.rscs.model.common.Price;
import com.fedex.rscs.model.common.ShipmentPreauthorizedRatedDetail;
import com.fedex.rscs.model.common.ShippingRateSurcharge;
import com.fedex.rscs.model.cshp.PreauthorizedRateData;
import com.fedex.rscs.model.cshp.ShippingRateData;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * This class contains test cases for unit testing of SRRSResponseMapper class.
 * 
 * @author 3900094
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SRRSResponseMapperTest {

    @InjectMocks
    private SRRSResponseMapper mapper;

    private static final String SERVICE = "FEDEX_GROUND";

    @Before
    public void setup() {

        mapper = new SRRSResponseMapper();
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link SRRSResponseMapper#transformRateResponse(ShipmentRateQuotesResource)} to set the
     * object fields null and return a valid response object with all mandatory fields covered.
     * 
     */
    @Test
    public void testTransformRateResponseWhenRateOptionNull() {

        ShipmentRateQuotesResource dto = mock(ShipmentRateQuotesResource.class);

        ShippingRateData response = mapper.transformRateResponse(dto);

        assertThat(response, nullValue());

    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link SRRSResponseMapper#transformRateResponse(ShipmentRateQuotesResource)} to set the
     * object fields and return a valid response object with all mandatory fields covered.
     * 
     */
    @Test
    public void testTransformRateResponse() {

        ShipmentRateQuotesResource dto = createRateResponse();

        ShippingRateData response = mapper.transformRateResponse(dto);

        assertThat(response, notNullValue());
        assertThat(response.getTotalBaseCharges(), notNullValue());
        assertThat(response.getTotalNetCharges(), notNullValue());
        assertThat(response.getTotalSurcharges(), notNullValue());
        assertThat(response.getTotalTaxes(), notNullValue());

    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link SRRSResponseMapper#transformRateResponse(ShipmentRateQuotesResource)} to set the object
     * fields null and return a valid response object with all mandatory fields covered.
     * 
     */
    @Test
    public void testTransformPreAuthRateResponseWhenRateOptionNull() {

        PreauthorizedRateResource dto = mock(PreauthorizedRateResource.class);

        PreauthorizedRateData response = mapper.transformPreauthorizedRateResponse(dto);

        assertThat(response, nullValue());

    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link SRRSResponseMapper#transformRateResponse(ShipmentRateQuotesResource)} to set the object
     * fields and return a valid response object with all mandatory fields covered.
     * 
     */
    @Test
    public void testTransformPreAuthRateResponse() {

        PreauthorizedRateResource dto = createPreAuthRateResponse();

        PreauthorizedRateData response = mapper.transformPreauthorizedRateResponse(dto);

        assertThat(response, notNullValue());
        assertThat(response.getShipmentRateDetails()
                .getTotalBaseCharge(), notNullValue());
        assertThat(response.getShipmentRateDetails()
                .getTotalNetCharge(), notNullValue());
        assertThat(response.getShipmentRateDetails()
                .getTotalSurcharges(), notNullValue());
        assertThat(response.getShipmentRateDetails()
                .getTotalTaxes(), notNullValue());

    }

    /**
     * Method to create Rate Response Detail.
     * 
     * @return RateResponse
     */
    private PreauthorizedRateResource createPreAuthRateResponse() {

        PreauthorizedRateResource rateResp = new PreauthorizedRateResource();
        PreauthorizedRateDetail rateDetail = new PreauthorizedRateDetail();
        rateDetail.setDeliveryTimestamp("2020-07-29T10:30:00.812Z");

        rateDetail.setServiceType("PRIORITY_OVERNIGHT");
        rateDetail.setPackagingType("YOUR_PACKAGING");
        rateDetail.setSignatureOption("Direct");
        rateDetail.setServiceDetails(new PreauthorizedRateServiceDetail());
        rateDetail.setCommitDetails(new PreauthorizedRateCommitDetail());

        ShipmentPreauthorizedRatedDetail shipmentPreauthorizedRatedDetail = new ShipmentPreauthorizedRatedDetail();
        shipmentPreauthorizedRatedDetail.setTotalBaseCharge(new Price(CurrencyType.USD, 25));
        shipmentPreauthorizedRatedDetail.setTotalNetCharge(new Price(CurrencyType.USD, 32));
        shipmentPreauthorizedRatedDetail.setTotalSurcharges(new Price(CurrencyType.USD, 5));
        shipmentPreauthorizedRatedDetail.setTotalTaxes(new Price(CurrencyType.USD, 2));
        rateDetail.setShipmentRateDetails(shipmentPreauthorizedRatedDetail);

        rateResp.setPreauthorizedRateDetail(rateDetail);
        return rateResp;
    }

    /**
     * Method to create Rate Response Detail.
     * 
     * @return RateResponse
     */
    private ShipmentRateQuotesResource createRateResponse() {

        ShipmentRateQuotesResource rateResp = new ShipmentRateQuotesResource();
        rateResp.setQuoteDate("2020-07-29T10:30:00.812Z");
        ShippingRateOption shippingRateOption = new ShippingRateOption();

        ServiceDescription serviceDescription = createServiceDescriptionDetail();

        CommitDetail commitInfo = new CommitDetail();
        commitInfo.setCommitTimestamp("2020-07-29T10:30:00.812Z");
        commitInfo.setDayOfWeek("SAT");

        List<ShippingRateSurcharge> surcharges = new ArrayList<>();
        ShippingRateSurcharge surcharge = new ShippingRateSurcharge();
        surcharge.setAmount(45.00);
        surcharge.setDescription("Declared Value");
        surcharge.setSurchargeType("INSURED_VALUE");
        surcharges.add(surcharge);

        shippingRateOption.setServiceDetails(serviceDescription);
        shippingRateOption.setPackagingType("YOUR_PACKAGING");
        shippingRateOption.setSignatureOption("SERVICE_DEFAULT");
        shippingRateOption.setHal(true);
        shippingRateOption.setCommitDetails(commitInfo);
        shippingRateOption.setRateDetails(createRateDetails());

        List<ShippingRateOption> rateDetails = new ArrayList<>();
        rateDetails.add(shippingRateOption);

        rateResp.setRateOptions(rateDetails);

        return rateResp;
    }

    /**
     * Method to create Rate Details
     * 
     * @return ShippingRateDetail
     */
    private ShippingRateDetail createRateDetails() {

        ShippingRateDetail shippingRateDetail = new ShippingRateDetail();
        shippingRateDetail.setCurrency(Currency.valueOf("USD"));
        shippingRateDetail.setRatingType(RateOption.valueOf("ONE_RATE"));
        shippingRateDetail.setTotalBaseCharge(10.6);
        shippingRateDetail.setTotalRebates(5.0);
        shippingRateDetail.setTotalNetCharges(15.6);
        shippingRateDetail.setTotalSurcharges(0.21);
        shippingRateDetail.setTotalTaxes(5.00);
        shippingRateDetail.setTotalFreightDiscounts(1.21);
        shippingRateDetail.setTotalNetFreight(0.32);
        shippingRateDetail.setTotalNetFedExCharge(2.32);
        shippingRateDetail.setTotalDutiesAndTaxes(0.87);
        shippingRateDetail.setTotalAncillaryFeesAndTaxes(0.56);
        shippingRateDetail.setTotalDutiesTaxesAndFees(2.88);
        shippingRateDetail.setTotalNetChargeWithDutiesAndTaxes(3.9);
        List<ShippingRateSurcharge> surcharges = new ArrayList<>();
        ShippingRateSurcharge surcharge = new ShippingRateSurcharge();
        surcharge.setAmount(45.00);
        surcharge.setDescription("Declared Value");
        surcharge.setSurchargeType("INSURED_VALUE");
        surcharges.add(surcharge);
        shippingRateDetail.setSurcharges(surcharges);
        return shippingRateDetail;
    }

    /**
     * Method to create Service Description Detail.
     * 
     * @return ServiceDescription
     */
    private ServiceDescription createServiceDescriptionDetail() {

        ServiceDescription servcDetail = new ServiceDescription();
        servcDetail.setServiceType(SERVICE);
        servcDetail.setDescription(SERVICE);
        servcDetail.setServiceName(SERVICE);

        return servcDetail;
    }

}
