package com.fedex.rscs.client.rtls;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.BaseOAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.test.web.client.MockRestServiceServer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fedex.common.cxs.dto.CXSEnvelope;
import com.fedex.common.cxs.exception.TerminalDbAccessException;
import com.fedex.rscs.client.rtls.dto.LocationData;
import com.fedex.rscs.client.rtls.dto.LocationDataResource;
import com.fedex.rscs.config.AppConfig;
import com.fedex.rscs.model.LocationContextDetail;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

/**
 * This covers all the test cases required to unit test the Retail location service client
 * operations.
 * 
 * @author shashank.jain
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class RetailLocationServiceClientImplTest {

    private static final String RETAIL_LOCATION_SERVICE_URI = "/location/fedexoffice/v2/retaillocations/";
    private static final String LOCATION_ID = "GVTK";

    @Mock
    private OAuth2RestTemplate restTemplate;

    @Mock
    private AppConfig appConfig;

    @InjectMocks
    private RetailLocationServiceClientImpl service;

    private MockRestServiceServer mockServer;

    private ObjectMapper mapper = new ObjectMapper();

    @Mock
    private ObjectMapper objectMapper;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void initialize() {

        BaseOAuth2ProtectedResourceDetails resource = new BaseOAuth2ProtectedResourceDetails();
        resource.setTokenName("bearer");
        restTemplate = new OAuth2RestTemplate(resource);
        mockServer = MockRestServiceServer.createServer(restTemplate);
        restTemplate.getOAuth2ClientContext()
                .setAccessToken(new DefaultOAuth2AccessToken("accesstoken"));

        when(appConfig.getRtlServiceUri()).thenReturn("");
        service = new RetailLocationServiceClientImpl(restTemplate, appConfig);
    }

    /**
     * Method to build the mock {@link CXSEnvelope<LocationContextResource>} response.
     * 
     * @return
     */
    private CXSEnvelope<LocationDataResource> buildRetailLocationServiceResponse() {

        LocationDataResource mockResponse = new LocationDataResource();

        LocationData retailLocation = new LocationData("033600003", "4341903", "6988194", null, "FXE", "NQAA");
        mockResponse.setRetailLocation(retailLocation);

        return (CXSEnvelope.success(mockResponse));
    }


    /**
     * Test case to validate the scenario if the retail location service is called and get the
     * required location context details and if the service returns the success response, then
     * {@link RetailLocationServiceClient} returns required location context details.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testLocationContextDetailsSuccess()
            throws JsonProcessingException {

        final CXSEnvelope<LocationDataResource> mockResponse = buildRetailLocationServiceResponse();
        mockServer.expect(once(), requestTo(RETAIL_LOCATION_SERVICE_URI + LOCATION_ID))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        LocationContextDetail actual = service.getLocationContextDetails(LOCATION_ID);

        mockServer.verify();
        assertThat(actual, notNullValue());
        assertThat(actual.getCityCenterAccountNumber(), notNullValue());
        assertThat(actual.getGroundAccountNumber(), notNullValue());
    }


    /**
     * Test case to validate the scenario if the retail location service is called and not able to
     * return required location context details and if the service returns the success response,
     * then {@link RetailLocationServiceClient} throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testLocationDetailsSuccesAndStutasIsCreated()
            throws JsonProcessingException {

        final CXSEnvelope<LocationDataResource> mockResponse = buildRetailLocationServiceResponse();

        mockServer.expect(once(), requestTo(RETAIL_LOCATION_SERVICE_URI + LOCATION_ID))
                .andRespond(withStatus(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        LocationContextDetail actual = service.getLocationContextDetails(LOCATION_ID);
        assertThat(actual, nullValue());

    }

    /**
     * Test case to validate the scenario if the retail location service is called and not able to
     * return required location context details and if the service returns the success response,
     * then {@link RetailLocationServiceClient} throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testLocationDetailsSuccesAndNullContext()
            throws JsonProcessingException {

        final CXSEnvelope<LocationDataResource> mockResponse = buildRetailLocationServiceResponse();
        mockResponse.getOutput()
                .setRetailLocation(null);
        mockServer.expect(once(), requestTo(RETAIL_LOCATION_SERVICE_URI + LOCATION_ID))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        LocationContextDetail actual = service.getLocationContextDetails(LOCATION_ID);
        assertThat(actual, nullValue());

    }

}
