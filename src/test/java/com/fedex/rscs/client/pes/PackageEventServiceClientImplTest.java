package com.fedex.rscs.client.pes;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.BaseOAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.test.web.client.MockRestServiceServer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fedex.common.cxs.dto.CXSEnvelope;
import com.fedex.common.cxs.dto.CXSError;
import com.fedex.common.cxs.exception.TerminalDbAccessException;
import com.fedex.rscs.client.pes.dto.Notification;
import com.fedex.rscs.client.pes.dto.PackageCancellationEventResource;
import com.fedex.rscs.client.pes.dto.PackageCancellationEventResponse;
import com.fedex.rscs.client.pes.dto.PackagePossessionEventResource;
import com.fedex.rscs.client.pes.dto.PossessionEvent;
import com.fedex.rscs.client.util.RetailShipmentCreationServiceClientUtil;
import com.fedex.rscs.config.AppConfig;
import com.fedex.rscs.model.common.PackageCancellationEventDetailRequest;
import com.fedex.rscs.model.common.PackagePossessionEventDetailRequest;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

/**
 * Test cases required to unit test PackageEventServiceClientImpl class
 * 
 * @author shakti.saurabh
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class PackageEventServiceClientImplTest {

    private static final String PES_SERVICE_URI = "/retailshipping/fedexoffice/v1/packagepossessionevents";
    private static final String PES_SERVICE_URI_CANCELLATION = "/retailshipping/fedexoffice/v1/packagecancellationevents";

    @Mock
    private OAuth2RestTemplate restTemplate;

    @Mock
    private AppConfig appConfig;

    @InjectMocks
    private PackageEventServiceClientImpl service;

    private MockRestServiceServer mockServer;
    private RetailShipmentCreationServiceClientUtil clientUtil;

    private ObjectMapper mapper = new ObjectMapper();


    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void initialize() {

        BaseOAuth2ProtectedResourceDetails resource = new BaseOAuth2ProtectedResourceDetails();
        resource.setTokenName("bearer");
        restTemplate = new OAuth2RestTemplate(resource);
        mockServer = MockRestServiceServer.createServer(restTemplate);
        restTemplate.getOAuth2ClientContext()
                .setAccessToken(new DefaultOAuth2AccessToken("accesstoken"));
        clientUtil = new RetailShipmentCreationServiceClientUtil();
        when(appConfig.getPesServiceUri()).thenReturn("");
        service = new PackageEventServiceClientImpl(restTemplate, appConfig,clientUtil);
    }

    /**
     * Test case to validate the scenario when call is made to
     * {@link PackageEventServiceClientImpl#postPackagePossessionEvents(PackagePossessionEventDetailRequest)}
     * to post in pes client
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackagePossessionEvents()
            throws JsonProcessingException {

        final CXSEnvelope<PackagePossessionEventResource> mockResponse =
                CXSEnvelope.success(createPackageEventResource());

        mockServer.expect(once(), requestTo(PES_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        boolean actual = service.postPackagePossessionEvents(new PackagePossessionEventDetailRequest());

        mockServer.verify();
        assertThat(actual, notNullValue());

    }

    /**
     * This method creates PackageEventResource
     * 
     * @return
     */
    private PackagePossessionEventResource createPackageEventResource() {

        PackagePossessionEventResource pper = new PackagePossessionEventResource();
        List<PossessionEvent> peList = new ArrayList<>();
        PossessionEvent pe = new PossessionEvent();
        pe.setAccepted(true);
        peList.add(pe);
        pper.setLineItems(peList);
        return pper;
    }
    
    /**
     * Test case to validate the scenario if the pes service is called and not able to return
     * required details and if the service returns the success response, then {@link PESClient}
     * throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackagePossessionEventsWithOutputNull()
            throws JsonProcessingException {

        final CXSEnvelope<PackagePossessionEventResource> mockResponse = null;

        mockServer.expect(once(), requestTo(PES_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackagePossessionEvents(new PackagePossessionEventDetailRequest());

    }

    /**
     * Test case to validate the scenario if the pes service is called and not able to return with
     * other HttpStatus details and if the service returns the success response, then
     * {@link PESClient} throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackagePossessionEventsWithOtherHttpStatus()
            throws JsonProcessingException {

        final CXSEnvelope<PackagePossessionEventResource> mockResponse =
                CXSEnvelope.success(new PackagePossessionEventResource());

        mockServer.expect(once(), requestTo(PES_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackagePossessionEvents(new PackagePossessionEventDetailRequest());

    }

    /**
     * Test case to validate the scenario if the pes service is called and not able to return empty
     * output body details and if the service returns the success response, then {@link PESClient}
     * throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackagePossessionEventsWithEmptyOutputBody()
            throws JsonProcessingException {

        final CXSEnvelope<PackagePossessionEventResource> mockResponse = CXSEnvelope.success();

        mockServer.expect(once(), requestTo(PES_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackagePossessionEvents(new PackagePossessionEventDetailRequest());

    }
    
    /**
     * Test case to validate the scenario if the pes service is called and not able to return empty
     * output body details and if the service returns the success response, then {@link PESClient}
     * throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackagePossessionEventsWithFalseBody()
            throws JsonProcessingException {

        PackagePossessionEventResource ppes = createPackageEventResource();
        ppes.getLineItems()
                .get(0)
                .setAccepted(false);
        Notification notification = new Notification();
        List<Notification> notifications = new ArrayList<>();
        notifications.add(notification);
        ppes.getLineItems()
                .get(0)
                .setEventNotification(notifications);
        final CXSEnvelope<PackagePossessionEventResource> mockResponse = CXSEnvelope.success(ppes);

        mockServer.expect(once(), requestTo(PES_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackagePossessionEvents(new PackagePossessionEventDetailRequest());

    }

    /**
     * Test case to validate the scenario if the pes service is called and not able to return empty
     * output body details and if the service returns the success response, then {@link PESClient}
     * throws Exception with service unavailable.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackagePossessionEventsWithCXSError()
            throws JsonProcessingException {

        final CXSEnvelope mockErrorResponse =
                buildMockErrorResponse("SPST.SYSTEM.UNAVAILABLE", "System is down. Please try again later.");

        mockServer.expect(once(), requestTo(PES_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.SERVICE_UNAVAILABLE).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockErrorResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackagePossessionEvents(new PackagePossessionEventDetailRequest());

    }
    
    /**
     * Test case to validate the scenario if the pes service is called and not able to return empty
     * output body details and if the service returns the success response, then {@link PESClient}
     * throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackagePossessionEventsWithCXSInternalError()
            throws JsonProcessingException {

        final CXSEnvelope mockErrorResponse =
                buildMockErrorResponse("INTERNAL.SERVER.ERROR", "System is down.");

        mockServer.expect(once(), requestTo(PES_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockErrorResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackagePossessionEvents(new PackagePossessionEventDetailRequest());

    }
    
    /**
     * Test case to validate the scenario if the pes service is called and not able to return empty
     * output body details and if the service returns the success response, then {@link PESClient}
     * throws Exception with Bad Request error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackagePossessionEventsWithCXSBadReqError()
            throws JsonProcessingException {

        final CXSEnvelope mockErrorResponse =
                buildMockErrorResponse("DUPLICATE.TRACKINGID", "Duplicate tracking number or barcode.");

        mockServer.expect(once(), requestTo(PES_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.BAD_REQUEST).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockErrorResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackagePossessionEvents(new PackagePossessionEventDetailRequest());

    }

    /**
     * Test case to validate the scenario if the pes service is called and not able to return empty
     * output body details and if the service returns the success response, then {@link PESClient}
     * throws Exception with No Implementation.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackagePossessionEventsWithNOImplErrorCode()
            throws JsonProcessingException {

        mockServer.expect(once(), requestTo(PES_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.NOT_IMPLEMENTED).contentType(MediaType.APPLICATION_JSON)) ;
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackagePossessionEvents(new PackagePossessionEventDetailRequest());

    }
    
    /**
     * Test case to validate the scenario when call is made to
     * {@link PackageEventServiceClientImpl#postPackageCancellationEvents(PackageCancellationEventDetailRequest)}
     * to post in pes client
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackageCancellationEvents()
            throws JsonProcessingException {

        final CXSEnvelope<PackageCancellationEventResponse> mockResponse =
                CXSEnvelope.success(createPackagePossessionEventResource());

        mockServer.expect(once(), requestTo(PES_SERVICE_URI_CANCELLATION))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        boolean actual = service.postPackageCancellationEvents(new PackageCancellationEventDetailRequest());

        mockServer.verify();
        assertThat(actual, notNullValue());

    }

    /**
     * This method creates PackageCancellationEventResponse
     * 
     * @return
     */
    private PackageCancellationEventResponse createPackagePossessionEventResource() {

        PackageCancellationEventResponse pceRes = new PackageCancellationEventResponse();
        List<PackageCancellationEventResource> pcerList = new ArrayList<>();
        PackageCancellationEventResource pcer = new PackageCancellationEventResource();
        pcer.setCancelled(true);
        pcerList.add(pcer);
        pceRes.setPackageCancellationEvents(pcerList);
        return pceRes;
    }


    /**
     * Test case to validate the scenario if the pes service is called and not able to return
     * required details and if the service returns the success response, then {@link PESClient}
     * throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackageCancellationEventsWithOutputNull()
            throws JsonProcessingException {

        final CXSEnvelope<PackageCancellationEventResponse> mockResponse = null;

        mockServer.expect(once(), requestTo(PES_SERVICE_URI_CANCELLATION))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackageCancellationEvents(new PackageCancellationEventDetailRequest());
    }

    /**
     * Test case to validate the scenario if the pes service is called and not able to return with
     * other HttpStatus details and if the service returns the success response, then
     * {@link PESClient} throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackageCancellationEventsWithOtherHttpStatus()
            throws JsonProcessingException {

        final CXSEnvelope<PackageCancellationEventResponse> mockResponse =
                CXSEnvelope.success(createPackagePossessionEventResource());

        mockServer.expect(once(), requestTo(PES_SERVICE_URI_CANCELLATION))
                .andRespond(withStatus(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackageCancellationEvents(new PackageCancellationEventDetailRequest());

    }

    /**
     * Test case to validate the scenario if the pes service is called and not able to return empty
     * output body details and if the service returns the success response, then {@link PESClient}
     * throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackageCancellationEventsWithEmptyOutputBody()
            throws JsonProcessingException {

        final CXSEnvelope<PackageCancellationEventResponse> mockResponse = CXSEnvelope.success();

        mockServer.expect(once(), requestTo(PES_SERVICE_URI_CANCELLATION))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackageCancellationEvents(new PackageCancellationEventDetailRequest());

    }

    /**
     * Test case to validate the scenario if the pes service is called and not able to return empty
     * output body details and if the service returns the success response with false cancellation,
     * then {@link PESClient} throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackageCancellationEventsWithEmptyIsCancelledFalse()
            throws JsonProcessingException {

        PackageCancellationEventResponse res = createPackagePossessionEventResource();
        res.getPackageCancellationEvents()
                .get(0)
                .setCancelled(false);
        res.getPackageCancellationEvents()
                .get(0)
                .setEventNotification(new Notification());
        final CXSEnvelope<PackageCancellationEventResponse> mockResponse = CXSEnvelope.success(res);

        mockServer.expect(once(), requestTo(PES_SERVICE_URI_CANCELLATION))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackageCancellationEvents(new PackageCancellationEventDetailRequest());

    }

    /**
     * Test case to validate the scenario if the pes service is called and not able to return empty
     * output body details and if the service returns the success response, then {@link PESClient}
     * throws Exception with service unavailable.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackageCancellationEventsWithCXSError()
            throws JsonProcessingException {

        final CXSEnvelope mockErrorResponse =
                buildMockErrorResponse("SPST.SYSTEM.UNAVAILABLE", "System is down. Please try again later.");

        mockServer.expect(once(), requestTo(PES_SERVICE_URI_CANCELLATION))

                .andRespond(withStatus(HttpStatus.SERVICE_UNAVAILABLE).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockErrorResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackageCancellationEvents(new PackageCancellationEventDetailRequest());

    }

    /**
     * Test case to validate the scenario if the pes service is called and not able to return empty
     * output body details and if the service returns the success response, then {@link PESClient}
     * throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackageCancellationEventsWithCXSInternalError()
            throws JsonProcessingException {

        final CXSEnvelope mockErrorResponse = buildMockErrorResponse("INTERNAL.SERVER.ERROR", "System is down.");

        mockServer.expect(once(), requestTo(PES_SERVICE_URI_CANCELLATION))

                .andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockErrorResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackageCancellationEvents(new PackageCancellationEventDetailRequest());

    }

    /**
     * Test case to validate the scenario if the pes service is called and not able to return empty
     * output body details and if the service returns the success response, then {@link PESClient}
     * throws Exception with Bad Request error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackageCancellationEventsWithCXSBadReqError()
            throws JsonProcessingException {

        final CXSEnvelope mockErrorResponse =
                buildMockErrorResponse("DUPLICATE.TRACKINGID", "Duplicate tracking number or barcode.");

        mockServer.expect(once(), requestTo(PES_SERVICE_URI_CANCELLATION))

                .andRespond(withStatus(HttpStatus.BAD_REQUEST).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockErrorResponse)));
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackageCancellationEvents(new PackageCancellationEventDetailRequest());

    }

    /**
     * Test case to validate the scenario if the pes service is called and not able to return empty
     * output body details and if the service returns the success response, then {@link PESClient}
     * throws Exception with No Implementation.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testPostPackageCancellationEventsWithNOImplErrorCode()
            throws JsonProcessingException {


        mockServer.expect(once(), requestTo(PES_SERVICE_URI_CANCELLATION))

                .andRespond(withStatus(HttpStatus.NOT_IMPLEMENTED).contentType(MediaType.APPLICATION_JSON));
        expectedEx.expect(TerminalDbAccessException.class);

        service.postPackageCancellationEvents(new PackageCancellationEventDetailRequest());

    }

    /**
     * Method to build the mock {@link CXSEnvelope} error response envelop for the provided error
     * code and message.
     * 
     * @param errorCode
     * @param errorMessage
     * @return
     */
    private CXSEnvelope buildMockErrorResponse(
            String errorCode,
            String errorMessage) {

        return CXSEnvelope.error(new CXSError(errorCode, errorMessage));
    }
}
