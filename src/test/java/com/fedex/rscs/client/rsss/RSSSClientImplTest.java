package com.fedex.rscs.client.rsss;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.BaseOAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.test.web.client.MockRestServiceServer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fedex.common.cxs.dto.CXSEnvelope;
import com.fedex.common.cxs.dto.CXSError;
import com.fedex.rscs.client.rsss.dto.InterlineAccountResource;
import com.fedex.rscs.client.rsss.dto.InterlineAccountResponse;
import com.fedex.rscs.client.rsss.dto.ShippingEnablementDetail;
import com.fedex.rscs.client.util.RetailShipmentCreationServiceClientUtil;
import com.fedex.rscs.config.AppConfig;
import com.fedex.rscs.dto.CarrierType;
import com.fedex.rscs.exception.client.ClientTerminalException;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

/**
 * Test class for RSSSClientImpl Class
 * 
 * @author 3932968
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class RSSSClientImplTest {

    private static final String INTERLINE_ACCOUNT_SERVICE_URI =
            "/retailshipping/fedexoffice/v1/interlineaccounts/1234";
    private static final String INTERLINE_ID = "1234";
    private static final String SUPPORT_SERVICE_URI = "http://localhost:9090";

    @Mock
    private OAuth2RestTemplate restTemplate;

    @Mock
    private AppConfig appConfig;

    private RetailShipmentCreationServiceClientUtil clientUtil;

    @InjectMocks
    private RSSSClientImpl service;

    private MockRestServiceServer mockServer;

    private ObjectMapper mapper = new ObjectMapper();

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void initialize() {

        BaseOAuth2ProtectedResourceDetails resource = new BaseOAuth2ProtectedResourceDetails();
        resource.setTokenName("bearer");
        restTemplate = new OAuth2RestTemplate(resource);
        mockServer = MockRestServiceServer.createServer(restTemplate);
        restTemplate.getOAuth2ClientContext()
                .setAccessToken(new DefaultOAuth2AccessToken("accesstoken"));

        when(appConfig.getSupportServiceUri()).thenReturn("");
        clientUtil = new RetailShipmentCreationServiceClientUtil();
        service = new RSSSClientImpl(restTemplate, appConfig, clientUtil);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RSSSClientImpl#validateInterlineAccounts(interlineId))} then it is executed
     * successfully
     * 
     * throws JsonProcessingException
     */
    @Test
    public void testValidateInterlineAccountInfo()
            throws JsonProcessingException {

        final CXSEnvelope<InterlineAccountResponse> mockResponse =
                CXSEnvelope.success(createInterlineAccountInfoResponse());

        when(appConfig.getSupportServiceUri()).thenReturn(SUPPORT_SERVICE_URI);

        mockServer.expect(once(), requestTo(appConfig.getSupportServiceUri() + INTERLINE_ACCOUNT_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        String actual = service.validateInterlineAccounts(INTERLINE_ID);

        mockServer.verify();
        assertThat(actual, notNullValue());

    }

    /**
     * Test case to verify that when request is made to
     * {@link RSSSClientImpl#validateInterlineAccounts(interlineId))} and not able to return
     * required details and if the service returns the success response, then {@link RSSSClientImpl}
     * throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testValidateInterlineAccountInfoWithOutputNull()
            throws JsonProcessingException {

        final CXSEnvelope<InterlineAccountResponse> mockResponse = CXSEnvelope.success("");

        when(appConfig.getSupportServiceUri()).thenReturn(SUPPORT_SERVICE_URI);

        mockServer.expect(once(), requestTo(appConfig.getSupportServiceUri() + INTERLINE_ACCOUNT_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));

        expectedEx.expect(ClientTerminalException.class);

        service.validateInterlineAccounts(INTERLINE_ID);

    }

    /**
     * Test case to verify that when request is made to
     * {@link RSSSClientImpl#validateInterlineAccounts(interlineId))} and not able to return
     * with other HttpStatus details and if the service returns the success response, then
     * {@link RSSSClientImpl} throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testValidateInterlineAccountInfoWithOtherHttpStatus()
            throws JsonProcessingException {

        final CXSEnvelope<InterlineAccountResponse> mockResponse =
                CXSEnvelope.success(createInterlineAccountInfoResponse());

        when(appConfig.getSupportServiceUri()).thenReturn(SUPPORT_SERVICE_URI);

        mockServer.expect(once(), requestTo(appConfig.getSupportServiceUri() + INTERLINE_ACCOUNT_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockResponse)));
        expectedEx.expect(ClientTerminalException.class);

        service.validateInterlineAccounts(INTERLINE_ID);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RSSSClientImpl#validateInterlineAccounts(interlineId))} and not able to return
     * empty output body details and if the service returns the success response, then
     * {@link RSSSClientImpl} throws Exception with service unavailable.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testValidateInterlineAccountInfoWithCXSError()
            throws JsonProcessingException {

        final CXSEnvelope mockErrorResponse = buildMockErrorResponse("INTERLINE.ACCOUNT.SERVICE.UNAVAILABLE",
                "Interline Account service is not available. Please try again later.");

        when(appConfig.getSupportServiceUri()).thenReturn(SUPPORT_SERVICE_URI);

        mockServer.expect(once(), requestTo(appConfig.getSupportServiceUri() + INTERLINE_ACCOUNT_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.SERVICE_UNAVAILABLE).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockErrorResponse)));
        expectedEx.expect(ClientTerminalException.class);

        service.validateInterlineAccounts(INTERLINE_ID);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RSSSClientImpl#validateInterlineAccounts(interlineId))} and not able to return
     * empty output body details and if the service returns the success response, then
     * {@link RSSSClientImpl} throws Exception with Internal Server error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testValidateInterlineAccountInfoCXSInternalError()
            throws JsonProcessingException {

        final CXSEnvelope mockErrorResponse = buildMockErrorResponse("INTERNAL.SERVER.ERROR", "Service Unavailable.");

        when(appConfig.getSupportServiceUri()).thenReturn(SUPPORT_SERVICE_URI);

        mockServer.expect(once(), requestTo(appConfig.getSupportServiceUri() + INTERLINE_ACCOUNT_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockErrorResponse)));
        expectedEx.expect(ClientTerminalException.class);

        service.validateInterlineAccounts(INTERLINE_ID);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RSSSClientImpl#validateInterlineAccounts(interlineId))} and not able to return
     * empty output body details and if the service returns the success response, then
     * {@link RSSSClientImpl} throws Exception with Bad Request error.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testValidateInterlineAccountInfoWithCXSBadReqError()
            throws JsonProcessingException {

        final CXSEnvelope mockErrorResponse =
                buildMockErrorResponse("INVALID.INTERLINE.ACCOUNT.NUMBER", "Interline account number not found.");

        when(appConfig.getSupportServiceUri()).thenReturn(SUPPORT_SERVICE_URI);

        mockServer.expect(once(), requestTo(appConfig.getSupportServiceUri() + INTERLINE_ACCOUNT_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.BAD_REQUEST).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(mockErrorResponse)));
        expectedEx.expect(ClientTerminalException.class);

        service.validateInterlineAccounts(INTERLINE_ID);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RSSSClientImpl#validateInterlineAccounts(interlineId))} and not able to return
     * empty output body details and if the service returns the success response, then
     * {@link RSSSClientImpl} throws Exception with No Implementation.
     * 
     * @throws JsonProcessingException
     * 
     */
    @Test
    public void testValidateInterlineAccountInfoWithNOImplErrorCode()
            throws JsonProcessingException {

        when(appConfig.getSupportServiceUri()).thenReturn(SUPPORT_SERVICE_URI);

        mockServer.expect(once(), requestTo(appConfig.getSupportServiceUri() + INTERLINE_ACCOUNT_SERVICE_URI))
                .andRespond(withStatus(HttpStatus.NOT_IMPLEMENTED).contentType(MediaType.APPLICATION_JSON));
        expectedEx.expect(ClientTerminalException.class);

        service.validateInterlineAccounts(INTERLINE_ID);
    }

    /**
     * Method to build the {@link ShippingEnablementDetail} object instance with test data.
     * 
     * @return
     */
    private ShippingEnablementDetail buildShippingEnablementDetail() {

        ShippingEnablementDetail shippingEnablementDetail = new ShippingEnablementDetail();
        List<CarrierType> carrierTypeList = new ArrayList<>();
        carrierTypeList.add(CarrierType.EXPRESS);

        shippingEnablementDetail.setAllowedOneRate(true);
        shippingEnablementDetail.setAllowedCarriers(carrierTypeList);

        return shippingEnablementDetail;
    }

    /**
     * Method to build the mock {@link CXSEnvelope} error response envelop for the provided error
     * code and message.
     * 
     * @param errorCode
     * @param errorMessage
     * @return
     */
    private CXSEnvelope buildMockErrorResponse(
            String errorCode,
            String errorMessage) {

        return CXSEnvelope.error(new CXSError(errorCode, errorMessage));
    }

    /**
     * Method to create InterlineAccountInfoResponse object.
     * 
     * @return InterlineAccountInfoResponse
     */
    private InterlineAccountResponse createInterlineAccountInfoResponse() {

        InterlineAccountResponse interlineAccountInfoResponse = new InterlineAccountResponse();

        InterlineAccountResource interlineAccountInfoResource = new InterlineAccountResource();
        interlineAccountInfoResource.setAccountNumber("164462765");
        interlineAccountInfoResource.setInterlineCode(INTERLINE_ID);
        interlineAccountInfoResource.setInterlineNumber("008");
        interlineAccountInfoResource.setInterlineName("ALLEGIANT AIRLINES");
        interlineAccountInfoResource.setInterlineCode("G4");
        interlineAccountInfoResource.setShippingEnablementDetail(buildShippingEnablementDetail());

        interlineAccountInfoResponse.setInterlineAccount(interlineAccountInfoResource);
        return interlineAccountInfoResponse;
    }

}

