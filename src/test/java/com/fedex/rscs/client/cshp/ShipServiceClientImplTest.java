package com.fedex.rscs.client.cshp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fedex.nxgen.ship.v17.ientities.ConfirmOpenShipmentReply;
import com.fedex.nxgen.ship.v17.ientities.ConfirmOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.CreateOpenShipmentReply;
import com.fedex.nxgen.ship.v17.ientities.CreateOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.DeleteOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.DeleteShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.ModifyOpenShipmentReply;
import com.fedex.nxgen.ship.v17.ientities.ModifyOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.ShipInterface;
import com.fedex.nxgen.ship.v17.ientities.ShipmentReply;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.cshp.CompleteShipmentDetail;
import com.fedex.rscs.model.cshp.DeleteOpenShipmentDetail;
import com.fedex.rscs.model.cshp.TrackingIdDetail;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test class for code coverage.
 * 
 * @author 5034922
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ShipServiceClientImplTest {

    @InjectMocks
    private ShipServiceClientImpl shipServiceClientImpl;

    @Mock
    private ShipServiceRequestBuilder shipServiceRequestBuilder;

    @Mock
    private ShipServiceResponseMapper shipServiceResponseMapper;

    @Mock
    private ShipInterface shipInterface;

    /**
     * Test case for create shipment
     */
    @Test
    public void testCreateShipment() {

        RequestedShipmentDetail requestedShipmentDetail = mock(RequestedShipmentDetail.class);
        WorkstationDetail workstationDetail = mock(WorkstationDetail.class);
        CreateOpenShipmentRequest createOpenShipmentRequest = mock(CreateOpenShipmentRequest.class);
        CreateOpenShipmentReply createOpenShipmentReply = mock(CreateOpenShipmentReply.class);
        CompleteShipmentDetail completeShipmentDetail = mock(CompleteShipmentDetail.class);
        when(shipServiceRequestBuilder.buildRequest(requestedShipmentDetail, workstationDetail))
                .thenReturn(createOpenShipmentRequest);
        when(shipInterface.createOpenShipment(createOpenShipmentRequest)).thenReturn(createOpenShipmentReply);
        when(shipServiceResponseMapper.mapToShipmentDetail(createOpenShipmentReply)).thenReturn(completeShipmentDetail);

        shipServiceClientImpl.createOpenShipment(requestedShipmentDetail, workstationDetail);

        verify(shipServiceRequestBuilder, times(1)).buildRequest(requestedShipmentDetail, workstationDetail);
        verify(shipInterface, times(1)).createOpenShipment(createOpenShipmentRequest);
        verify(shipServiceResponseMapper, times(1)).mapToShipmentDetail(createOpenShipmentReply);

    }


    /**
     * Test case for delete open shipment
     */
    @Test
    public void testDeleteOpenShipment() {

        DeleteOpenShipmentDetail deleteOpenShipmentDetail = mock(DeleteOpenShipmentDetail.class);
        DeleteOpenShipmentRequest deleteOpenShipmentRequest = mock(DeleteOpenShipmentRequest.class);


        when(shipServiceRequestBuilder.buildDeleteOpenShipmentRequest(deleteOpenShipmentDetail))
                .thenReturn(deleteOpenShipmentRequest);

        shipServiceClientImpl.deleteOpenShipment(deleteOpenShipmentDetail);

        verify(shipServiceRequestBuilder, times(1)).buildDeleteOpenShipmentRequest(deleteOpenShipmentDetail);
        verify(shipInterface, times(1)).deleteOpenShipment(deleteOpenShipmentRequest);
    }

    @Test
    public void testDeleteShipment() {

        ShipmentReply reply = new ShipmentReply();
        reply.setHighestSeverity("SUCCESS");

        TrackingIdDetail trackingDetail = new TrackingIdDetail("FEDEX", "0201", null, "794979039803");
        DeleteShipmentRequest request = mock(DeleteShipmentRequest.class);

        when(shipServiceRequestBuilder.buildDeleteShipmentRequest(trackingDetail, createWorkstationDetails(),"DFWD"))
                .thenReturn(request);
        when(shipInterface.deleteShipment(request)).thenReturn(reply);
        when(shipServiceResponseMapper.buildDeleteShipmentResponse(reply)).thenReturn(reply.getHighestSeverity());

        String result = shipServiceClientImpl.deleteShipment(trackingDetail, createWorkstationDetails(),"DFWD");

        assertThat(result, notNullValue());
        assertThat(result, equalTo("SUCCESS"));
    }

    /**
     * Test case for modify open shipment
     */
    @Test
    public void testModifyOpenShipment() {

        RequestedShipmentDetail requestedShipment = mock(RequestedShipmentDetail.class);
        ModifyOpenShipmentRequest modReq = mock(ModifyOpenShipmentRequest.class);
        ModifyOpenShipmentReply modRep = mock(ModifyOpenShipmentReply.class);

        CompleteShipmentDetail comShipDet = mock(CompleteShipmentDetail.class);
        when(shipServiceRequestBuilder.buildModifyRequest(requestedShipment, createWorkstationDetails()))
                .thenReturn(modReq);
        when(shipInterface.modifyOpenShipment(modReq)).thenReturn(modRep);
        when(shipServiceResponseMapper.buildModifyOpenShipmentResponse(modRep)).thenReturn(comShipDet);
        shipServiceClientImpl.modifyOpenShipment(requestedShipment, createWorkstationDetails());
        verify(shipServiceRequestBuilder, times(1)).buildModifyRequest(requestedShipment, createWorkstationDetails());
        verify(shipServiceResponseMapper, times(1)).buildModifyOpenShipmentResponse(modRep);
        verify(shipInterface, times(1)).modifyOpenShipment(modReq);
    }

    /**
     * Create WorkstationDetails
     * 
     * @return
     */
    private WorkstationDetail createWorkstationDetails() {

        WorkstationDetail workstationDetail = new WorkstationDetail();
        workstationDetail.setAccountNumber("375200007");
        workstationDetail.setMeterNumber("006990060");
        workstationDetail.setSoftwareId("SSFO");
        workstationDetail.setAppVersionId("2021");
        return workstationDetail;
    }
    
    /**
     * Test case for confirm open shipment
     */
    @Test
    public void testConfirmOpenShipment() {

        ShipmentConfirmationRequest confirmShipmentRequested = mock(ShipmentConfirmationRequest.class);
        ConfirmOpenShipmentRequest confirmReq = mock(ConfirmOpenShipmentRequest.class);
        ConfirmOpenShipmentReply confirmReply = mock(ConfirmOpenShipmentReply.class);

        CompleteShipmentDetail comShipDet = mock(CompleteShipmentDetail.class);
        when(shipServiceRequestBuilder.buildConfirmRequest(confirmShipmentRequested)).thenReturn(confirmReq);
        when(shipInterface.confirmOpenShipment(confirmReq)).thenReturn(confirmReply);
        when(shipServiceResponseMapper.buildConfirmOpenShipmentResponse(confirmReply)).thenReturn(comShipDet);
        shipServiceClientImpl.confirmOpenShipment(confirmShipmentRequested);
        verify(shipServiceRequestBuilder, times(1)).buildConfirmRequest(confirmShipmentRequested);
        verify(shipServiceResponseMapper, times(1)).buildConfirmOpenShipmentResponse(confirmReply);
        verify(shipInterface, times(1)).confirmOpenShipment(confirmReq);
    }

}
