package com.fedex.rscs.client.cshp;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fedex.nxgen.ship.v17.ientities.ConfirmOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.CreateOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.DeleteOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.DeleteShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.ModifyOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.PackageSpecialServicesRequested;
import com.fedex.rscs.client.srrs.dto.PackageSpecialHandlingType;
import com.fedex.rscs.config.cshp.ShipServiceConfiguration;
import com.fedex.rscs.dto.HomeDeliveryType;
import com.fedex.rscs.dto.PhoneNumber;
import com.fedex.rscs.model.AddressDetail;
import com.fedex.rscs.model.AssociatedAccountDetail;
import com.fedex.rscs.model.BatteryClassificationType;
import com.fedex.rscs.model.ContactDetail;
import com.fedex.rscs.model.DimensionDetail;
import com.fedex.rscs.model.HoldAtLocDetail;
import com.fedex.rscs.model.HomeDelvDetail;
import com.fedex.rscs.model.InsuredValueDetail;
import com.fedex.rscs.model.LabelSpecificationDetail;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.PaymentDetail;
import com.fedex.rscs.model.RequestedPackageLineItemDetail;
import com.fedex.rscs.model.RequestedPackageSpecialServices;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.ShipmentNotificationRoleType;
import com.fedex.rscs.model.ShipmentSpecialServiceDetail;
import com.fedex.rscs.model.ShippingCustomerReferenceDetails;
import com.fedex.rscs.model.SpecialServiceDescriptionDetail;
import com.fedex.rscs.model.SpecialServiceDetail;
import com.fedex.rscs.model.VariationOptionDetail;
import com.fedex.rscs.model.WeightDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.cshp.CreditCardData;
import com.fedex.rscs.model.cshp.DeleteOpenShipmentDetail;
import com.fedex.rscs.model.cshp.EPaymentMode;
import com.fedex.rscs.model.cshp.Price;
import com.fedex.rscs.model.cshp.TrackingIdDetail;
import com.fedex.rscs.util.RetailShipmentCreationServiceUtil;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Test class for code coverage.
 * 
 * @author 5034922
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ShipServiceRequestBuilderTest {

    @InjectMocks
    private ShipServiceRequestBuilder shipServiceRequestBuilder;

    @Mock
    private RetailShipmentCreationServiceUtil serviceUtil;

    @Mock
    private ShipServiceConfiguration serviceConfig;

    private static final String LOCATION_CODE = "DFWD";

    @Before
    public void setup() {

        when(serviceConfig.getClientApplicationId()).thenReturn("rscs");
        when(serviceUtil.createPackageSpecialService(any(), any())).thenReturn(createPackageSpecialServicesRequested());
    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created
     */
    @Test
    public void testBuildRequest() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationContactAndAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getPhoneNumber(), notNullValue());

    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created
     */
    @Test
    public void testBuildRequestWithHomeDeliveryService() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetailWithHomeDelivery();
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryPremiumDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryPremiumDetail()
                .getDate(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryPremiumDetail()
                .getPhoneNumber(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryPremiumDetail()
                .getHomeDeliveryPremiumType(), notNullValue());

    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created
     */
    @Test
    public void testBuildRequestWithHomeDeliveryServiceWithNullPhoneNumber() {

        RequestedShipmentDetail specialServicesValidRequest =
                createRequestedShipmentDetailWithHomeDeliveryWithNullPhoneNumber();
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryPremiumDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryPremiumDetail()
                .getDate(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryPremiumDetail()
                .getPhoneNumber(), nullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHomeDeliveryPremiumDetail()
                .getHomeDeliveryPremiumType(), notNullValue());

    }

    /**
     * Test case to verify ModifyOpenShipmentRequest is created
     */
    @Test
    public void testModifyOpenBuildRequest() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        ModifyOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildModifyRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());

    }

    /**
     * Test case to verify DeleteOpenShipmentRequest is created
     */
    @Test
    public void testDeleteOpenShipment() {

        when(serviceUtil.getCustomerTransactionId(any(String.class), any(String.class), any(String.class)))
                .thenCallRealMethod();

        DeleteOpenShipmentDetail deleteOpenShipmentDetail = new DeleteOpenShipmentDetail();
        deleteOpenShipmentDetail.setIndex("794824603854");
        deleteOpenShipmentDetail.setLocationCode("DFWD");
        deleteOpenShipmentDetail.setRequestedDateTime("2020-07-30T11:51:40.767-05:00");
        deleteOpenShipmentDetail.setWorkstationDetail(createWorkstationDetails());
        DeleteOpenShipmentRequest request =
                shipServiceRequestBuilder.buildDeleteOpenShipmentRequest(deleteOpenShipmentDetail);
        assertThat(request, notNullValue());
        assertThat(request.getIndex(), equalTo("794824603854"));
        assertThat(request.getClientDetail()
                .getAccountNumber(), equalTo("375200007"));
    }

    @Test
    public void testDeletShipmentRequest() {

        TrackingIdDetail trackingDetail = new TrackingIdDetail("FEDEX", "0201", null, "794979039803");
        DeleteShipmentRequest request = shipServiceRequestBuilder.buildDeleteShipmentRequest(trackingDetail,
                createWorkstationDetails(), "DFWD");

        assertThat(request, notNullValue());
        assertThat(request.getClientDetail(), notNullValue());
        assertThat(request.getTrackingId(), notNullValue());
        assertThat(request.getTrackingId()
                .getTrackingIdType(), equalTo("FEDEX"));
        assertThat(request.getTrackingId()
                .getFormId(), equalTo("0201"));
        assertThat(request.getTrackingId()
                .getTrackingNumber(), equalTo("794979039803"));

    }

    /**
     * Create RequestedShipmentDetail
     * 
     * @return
     */
    private RequestedShipmentDetail createRequestedShipmentDetail() {

        RequestedShipmentDetail requestedShipmentDetail = new RequestedShipmentDetail();
        requestedShipmentDetail.setServiceType("PRIORITY_OVERNIGHT");
        requestedShipmentDetail.setPackagingType("FEDEX_PAK");
        requestedShipmentDetail.setTransactionLocationCode(LOCATION_CODE);
        requestedShipmentDetail.setSender(createShipperPartyDetail());
        requestedShipmentDetail.setOrigin(createShipperPartyDetail());
        requestedShipmentDetail.setRecipient(createShipperPartyDetail());
        requestedShipmentDetail.setVariationOptions(createVariationOptionDetail());
        requestedShipmentDetail.setSpecialServicesRequested(createShipmentSpecialService());
        requestedShipmentDetail.setPackageLineItems(createRequestedPackageLineItems());
        requestedShipmentDetail.setShippingChargesPayment(createPayment());
        requestedShipmentDetail.setLabelSpecification(new LabelSpecificationDetail());
        requestedShipmentDetail.setPackageCount("3");
        List<String> actions = new ArrayList<>();
        actions.add("STRONG_VALIDATION");
        requestedShipmentDetail.setActions(actions);
        return requestedShipmentDetail;
    }

    /**
     * Create List<RequestedPackageLineItems>
     * 
     * @return
     */
    private List<RequestedPackageLineItemDetail> createRequestedPackageLineItems() {

        List<RequestedPackageLineItemDetail> requestedPackageLineItems = new ArrayList<>();
        RequestedPackageLineItemDetail requestedPackageLineItem = new RequestedPackageLineItemDetail();
        requestedPackageLineItem.setDimensions(new DimensionDetail(1, 5, 6, ""));
        requestedPackageLineItem.setInsuredValue(new InsuredValueDetail("USD", "100"));
        requestedPackageLineItem.setWeight(new WeightDetail("LB", "5"));
        requestedPackageLineItem.setSequenceNumber(1);
        requestedPackageLineItem.setGroupNumber(2);
        requestedPackageLineItem.setGroupPackageCount(3);
        requestedPackageLineItem.setPackageSpecialServicesRequested(createPackageSpecialSerivce());
        requestedPackageLineItem.setSpecialHandlingDetail("INSPECTED");
        SpecialServiceDetail signatureOption = new SpecialServiceDetail();
        signatureOption.setSpecialServiceSubType("SERVICE_DEFAULT");
        requestedPackageLineItem.setSignatureOption(signatureOption);
        requestedPackageLineItem.setCustomerReferences(createShippingCustomerReferenceDetails());
        requestedPackageLineItems.add(requestedPackageLineItem);

        return requestedPackageLineItems;
    }

    /**
     * Create createPackageSpecialSerivce
     * 
     * @return
     */
    private RequestedPackageSpecialServices createPackageSpecialSerivce() {

        RequestedPackageSpecialServices packSpecService = new RequestedPackageSpecialServices();

        List<SpecialServiceDescriptionDetail> specSerReqDet = new ArrayList<>();
        specSerReqDet.add(new SpecialServiceDescriptionDetail("SERVICE_DEFAULT", null, null, null));
        
        packSpecService.setSpecialServicesRequested(specSerReqDet);
        packSpecService.setDryIceWeightDetails(new WeightDetail("LB", "5"));
        List<BatteryClassificationType> batteryClassificationTypes = new ArrayList<>(1);
        batteryClassificationTypes.add(BatteryClassificationType.LITHIUM_ION_PACKED_WITH_EQUIPMENT);
        packSpecService.setBatteryClassifications(batteryClassificationTypes);

        return packSpecService;
    }

    /**
     * Create PaymentDetail
     * 
     * @return
     */
    private PaymentDetail createPayment() {

        List<AssociatedAccountDetail> accountDetailList = new ArrayList<>();
        accountDetailList.add(new AssociatedAccountDetail());

        return new PaymentDetail("CASH", createShipperPartyDetail(), "CASH", accountDetailList,
                new Price("USD", 100.0));

    }

    /**
     * Create ShipmentSpecialServiceDetail
     * 
     * @return
     */
    private ShipmentSpecialServiceDetail createShipmentSpecialService() {

        ShipmentSpecialServiceDetail shipSpecialService = new ShipmentSpecialServiceDetail();
        shipSpecialService.setSpecialServicesRequestedType(createShipmentSpecialServiceDetail());
        shipSpecialService.setHoldAtLocationDetail(createHoldAtLocationDetail());

        return shipSpecialService;
    }

    /**
     * Create HoldAtLocationDetail
     * 
     * @return
     */
    private HoldAtLocDetail createHoldAtLocationDetail() {

        HoldAtLocDetail holdAtLocDetail = new HoldAtLocDetail();
        holdAtLocDetail.setAddress(new AddressDetail(new ArrayList<String>(), "", "TX", "75024", "US", true));
        holdAtLocDetail.setLocationId(LOCATION_CODE);
        holdAtLocDetail.setContact(new ContactDetail(null, null, null, null, "1111111111", null));

        return holdAtLocDetail;
    }

    /**
     * Create ShipmentSpecialServiceDetail
     * 
     * @return
     */
    private List<SpecialServiceDescriptionDetail> createShipmentSpecialServiceDetail() {

        List<SpecialServiceDescriptionDetail> speSerList = new ArrayList<>();
        speSerList.add(new SpecialServiceDescriptionDetail("FEDEX_ONE_RATE", null, null, null));

        return speSerList;
    }

    /**
     * Create WorkstationDetails
     * 
     * @return
     */
    private WorkstationDetail createWorkstationDetails() {

        WorkstationDetail workstationDetail = new WorkstationDetail();
        workstationDetail.setAccountNumber("375200007");
        workstationDetail.setAppName("FUSE");
        workstationDetail.setAppVersionId("fuseid");
        workstationDetail.setDeviceId("FUSE");
        workstationDetail.setGroundAccountNumber("375200007");
        workstationDetail.setSoftwareId("2020");
        return workstationDetail;
    }

    /**
     * Create ShipperPartyDetail
     * 
     * @return
     */
    private PartyDetail createShipperPartyDetail() {

        List<String> streetLines = new ArrayList<>();
        streetLines.add("4071 North Trail");

        AddressDetail addressDetail = new AddressDetail();
        addressDetail.setPostalCode("75023");
        addressDetail.setCountryCode("US");
        addressDetail.setStreetLines(streetLines);
        return new PartyDetail(addressDetail, createContactDetail(), "038000004",
                new CreditCardData("4005554444444460", "VISA", "12/2020"), "FEDEX_EXPRESS");
    }

    /**
     * Create ContactDetail
     * 
     * @return
     */
    private ContactDetail createContactDetail() {

        ContactDetail contactDetail = new ContactDetail();
        contactDetail.setContactId("test123");
        contactDetail.setFullName("Dhruv Kumar Sood");
        contactDetail.setPhoneNumber("32456764");

        return contactDetail;
    }

    /**
     * Create List<VariationOptionDetails>
     * 
     * @return
     */
    private List<VariationOptionDetail> createVariationOptionDetail() {

        List<VariationOptionDetail> variationOptionDetails = new ArrayList<>();
        List<String> listOfVariationValue = new ArrayList<>();
        listOfVariationValue.add("SUPPORTED");
        VariationOptionDetail variationOptionDetail = new VariationOptionDetail("ISS", listOfVariationValue);
        variationOptionDetail.setId("ISS");

        variationOptionDetail.setValues(listOfVariationValue);
        variationOptionDetails.add(variationOptionDetail);

        return variationOptionDetails;
    }

    /**
     * Test case to verify create Open Shipments without shipment special service requested is created
     */
    @Test
    public void testBuildRequestWithSpecialServicesRequestedNull() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.setSpecialServicesRequested(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
    }
    
    /**
     * Test case to verify create Open Shipments without shipment special service requested type is created
     */
    @Test
    public void testBuildRequestWithSpecialServicesRequestedTypeNull() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getSpecialServicesRequested()
                .setSpecialServicesRequestedType(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
    }
    
    /**
     * Test case to verify create Open Shipments without shipment special service requested is created
     */
    @Test
    public void testBuildRequestWithSpecialServicesRequestedIsSignatureOption() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();

        List<SpecialServiceDescriptionDetail> specSerReqDet = new ArrayList<>();
        specSerReqDet.add(new SpecialServiceDescriptionDetail("SIGNATURE_OPTION", "NO_SIGNATURE_REQUIRED", null, null));

        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .setSpecialServicesRequested(specSerReqDet);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
    }
    
    /**
     * Test case to verify create Open Shipments without origin is created
     */
    @Test
    public void testBuildRequestWithOriginNull() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.setOrigin(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
    }
    
    /**
     * Test case to verify create Open Shipments without dimensions weight and insuredValue is created
     */
    @Test
    public void testBuildRequestWithDimensionsWeightAndInsuredValueNull() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .setDimensions(null);

        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .setWeight(null);
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .setInsuredValue(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
    }
    
    /**
     * Test case to verify create Open Shipments without HAL is created
     */
    @Test
    public void testBuildRequestWithHALNull() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getSpecialServicesRequested()
                .setHoldAtLocationDetail(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
    }
    
    /**
     * Test case to verify create Open Shipments without HAL contact is created
     */
    @Test
    public void testBuildRequestWithHALContactAndVariationOptionsNull() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .setContact(null);
        specialServicesValidRequest.setVariationOptions(null);
        specialServicesValidRequest.getSender()
                .setAddress(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created with package count null
     */
    @Test
    public void testBuildRequestWhenPackageCountNull() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getShippingChargesPayment()
                .setPayor(null);
        specialServicesValidRequest.setPackageCount(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationContactAndAddress(), notNullValue());
    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created with requested package line items null
     */
    @Test
    public void testBuildRequestWhenReqestedPkgLineItmNull() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.setPackageLineItems(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationContactAndAddress(), notNullValue());
    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created with group package count null
     */
    @Test
    public void testBuildRequestWhenGroupPackageCountNull() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .setGroupPackageCount(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationContactAndAddress(), notNullValue());
    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created with package special service is null
     */
    @Test
    public void testBuildRequestWhenPackageSpclSrvcNull() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .setPackageSpecialServicesRequested(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationContactAndAddress(), notNullValue());
    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created with special service type requested null
     */
    @Test
    public void testBuildRequestWhenReqSpclSrvcTypeNull() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .setSpecialServicesRequested(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationContactAndAddress(), notNullValue());
    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created with dry ice weight details null
     */
    @Test
    public void testBuildRequestWhenDryICeWeightDetailNull() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .setDryIceWeightDetails(null);
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .getPackageSpecialServicesRequested()
                .setBatteryClassifications(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationContactAndAddress(), notNullValue());
    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created with signature Option details null
     */
    @Test
    public void testBuildRequestWhenSignatureOptnDetailNull() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .setSignatureOption(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationContactAndAddress(), notNullValue());
    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created with payment Option null
     */
    @Test
    public void testBuildRequestWhenPaymentOptionNull() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.setShippingChargesPayment(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationContactAndAddress(), notNullValue());
    }

    /**
     * Test case to verify ConfirmOpenShipmentRequest is created
     */
    @Test
    public void testBuildConfirmRequest() {

        ShipmentConfirmationRequest confirmShipmentRequested = createConfirmShipmentRequested();
        ConfirmOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildConfirmRequest(confirmShipmentRequested);

        assertThat(actualRequest, notNullValue());

        assertThat(actualRequest.getApplicationId(), equalTo(serviceConfig.getClientApplicationId()));
        assertThat(actualRequest.getIndex(), equalTo("375200007"));
        assertThat(actualRequest.getClientDetail()
                .getAccountNumber(), equalTo("375200007"));
        assertThat(actualRequest.getAsynchronousProcessingOptions(), notNullValue());

    }

    /**
     * Test case for specialhandling customer
     */
    @Test
    public void testCreateShipmentWithCustomer() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .setSpecialHandlingDetail("CUSTOMER");
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail()
                        .getPackedBy(),
                equalTo("CUSTOMER"));

    }

    /**
     * Test case for specialhandling customer
     */
    @Test
    public void testCreateShipmentWithFedExOffice() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .setSpecialHandlingDetail(PackageSpecialHandlingType.FEDEX_OFFICE.name());
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail()
                        .getPackedBy(),
                equalTo("FEDEX_OFFICE"));

    }

    /**
     * Test case for specialhandling customer refused inspection
     */
    @Test
    public void testCreateShipmentWithRefusedInspection() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .setSpecialHandlingDetail("CUSTOMER_REFUSED_INSPECTION");
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail()
                        .getInspectionStatus(),
                equalTo("CUSTOMER_REFUSED_INSPECTION"));

    }

    /**
     * Test case for specialhandling No inspected
     */
    @Test
    public void testCreateShipmentWithNoInspection() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .setSpecialHandlingDetail("FEDEX_DAMAGE_KNOWN");
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail()
                        .getInspectionStatus(),
                equalTo("NOT_INSPECTED"));

    }

    /**
     * Test case for specialhandling inspected damage known
     */
    @Test
    public void testCreateShipmentWithInspectedDamageKnown() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .setSpecialHandlingDetail("INSPECTED_DAMAGE_KNOWN");
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail()
                        .getDamageKnown(),
                equalTo("DAMAGE_KNOWN"));

    }

    /**
     * Test case for specialhandling refused inspection damage known
     */
    @Test
    public void testCreateShipmentWithRefusedInspectionDamageKnown() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .setSpecialHandlingDetail("REFUSED_INSPECTION_DAMAGE_KNOWN");
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail()
                        .getDamageKnown(),
                equalTo("DAMAGE_KNOWN"));

    }

    /**
     * Test case for specialhandling Damage known
     */
    @Test
    public void testCreateShipmentWithDamageKnown() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .setSpecialHandlingDetail("FEDEX_DAMAGE_KNOWN");
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail()
                        .getDamageKnown(),
                equalTo("DAMAGE_KNOWN"));

    }

    /**
     * Test case for specialhandling No Damage known
     */
    @Test
    public void testCreateShipmentWithNoDamageKnown() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.getPackageLineItems()
                .get(0)
                .setSpecialHandlingDetail("FEDEX_OFFICE");
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialHandlingDetail()
                        .getDamageKnown(),
                equalTo("NO_KNOWN_DAMAGE"));

    }

    /**
     * Create ConfirmShipmentRequested
     * 
     * @return
     */
    private ShipmentConfirmationRequest createConfirmShipmentRequested() {

        ShipmentConfirmationRequest confirmShipmentRequested = new ShipmentConfirmationRequest();
        confirmShipmentRequested.setWorkstationDetail(createWorkstationDetails());
        confirmShipmentRequested.setLabelSpecificationDetail(new LabelSpecificationDetail());
        confirmShipmentRequested.setReferenceId("375200007");
        confirmShipmentRequested.setShipmentTimeStamp("2020-08-05T11:57:40.951+05:30");

        return confirmShipmentRequested;
    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created with Action details null
     */
    @Test
    public void testBuildRequestForActionNull() {

        RequestedShipmentDetail specialServicesValidRequest = createRequestedShipmentDetail();
        specialServicesValidRequest.setActions(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(specialServicesValidRequest, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationContactAndAddress(), notNullValue());

    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created with Associate Account null
     */
    @Test
    public void testBuildRequestForAssociateAccountNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getShippingChargesPayment()
                .setAssociatedAccounts(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(requestedShipmentDetail, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationContactAndAddress(), notNullValue());
    }

    /**
     * Create ShippingCustomerReferenceDetails
     * 
     * @return
     */
    private List<ShippingCustomerReferenceDetails> createShippingCustomerReferenceDetails() {

        List<ShippingCustomerReferenceDetails> shippingCustomerReferenceDetailsList = new ArrayList<>();

        ShippingCustomerReferenceDetails shippingCustomerReferenceDetails = new ShippingCustomerReferenceDetails();
        shippingCustomerReferenceDetails.setReferenceType("CUSTOMER_REFERENCE");
        shippingCustomerReferenceDetails.setValue("111");
        shippingCustomerReferenceDetailsList.add(shippingCustomerReferenceDetails);

        return shippingCustomerReferenceDetailsList;
    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created with BatteryClassification and
     * CustomerReferences are null
     */
    @Test
    public void testBuildGetAllSpecialServicesRequestCustomerReferencesNull() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .setCustomerReferences(null);

        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(requestedShipmentDetail, createWorkstationDetails());

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
    }

    /**
     * Test Email Notification for sender
     */
    @Test
    public void testSetEmailNotificationForSender() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        requestedShipment.getSender()
                .getContact()
                .setEmailId("test@test.com");
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(requestedShipment, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail()
                .getEventNotifications(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail()
                .getEventNotifications().length, equalTo(1));
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail()
                .getEventNotifications()[0].getRole(), equalTo(ShipmentNotificationRoleType.SHIPPER.name()));
    }

    /**
     * Test Email Notification for Recipient
     */
    @Test
    public void testSetEmailNotificationForRecipient() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        requestedShipment.getRecipient()
                .getContact()
                .setEmailId("test@test.com");
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(requestedShipment, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail()
                .getEventNotifications(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail()
                .getEventNotifications().length, equalTo(1));
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail()
                .getEventNotifications()[0].getRole(), equalTo(ShipmentNotificationRoleType.RECIPIENT.name()));
    }

    /**
     * Test Email Notification for Customer
     */
    @Test
    public void testSetEmailNotificationForOther() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        requestedShipment.setCustomer(createShipperPartyDetail());
        requestedShipment.getCustomer()
                .getContact()
                .setEmailId("primary111@test.com");
        requestedShipment.getCustomer()
                .getContact()
                .setAlternateEmailId("secondary222@test.com");
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(requestedShipment, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail()
                .getEventNotifications(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail()
                .getEventNotifications().length, equalTo(2));
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail()
                .getEventNotifications()[0].getRole(), equalTo(ShipmentNotificationRoleType.OTHER.name()));
    }

    /**
     * Test Email Notification for Customer with null primary and alternate Email.
     */
    @Test
    public void testSetEmailNotificationForOtherWdNullEmail() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();
        requestedShipment.setCustomer(createShipperPartyDetail());
        requestedShipment.getCustomer()
                .getContact()
                .setEmailId(null);
        requestedShipment.getCustomer()
                .getContact()
                .setAlternateEmailId(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(requestedShipment, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail(), nullValue());
    }

    /**
     * Test Email Notification for Sender
     */
    @Test
    public void testEmailNotificationNotSetForSnderWhenEmptyEmail() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();

        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(requestedShipment, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail(), nullValue());

    }

    /**
     * Test Email Notification for Recipient
     */
    @Test
    public void testEmailNotificationNotSetForRecipientWhenEmptyEmail() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();

        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(requestedShipment, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail(), nullValue());

    }

    /**
     * Test Email Notification for Customer
     */
    @Test
    public void testEmailNotificationNotSetForCustomerWhenEmptyEmail() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();

        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(requestedShipment, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail(), nullValue());

    }



    /**
     * Test Email Notification not set for Customer when customer is null
     */
    @Test
    public void testEmailNotificationNotSetWhenCustomerNull() {

        RequestedShipmentDetail requestedShipment = createRequestedShipmentDetail();

        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(requestedShipment, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail(), nullValue());

    }

    /**
     * Test case to verify if {@link CreateOpenShipmentRequest} built using builder has required
     * attributes when payment type is provided with epaymentMode.
     */
    @Test
    public void testBuildRequestWhenPaymentTypeEpayment() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getShippingChargesPayment()
                .setEpaymentMode(EPaymentMode.CASH);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(requestedShipmentDetail, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(), null,
                LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationContactAndAddress(), notNullValue());

        assertThat(actualRequest.getRequestedShipment()
                .getShippingChargesPayment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getShippingChargesPayment()
                .getPaymentType(), equalTo(ShipServiceRequestBuilder.PAYMENT_TYPE_EPAYMENT));
        assertThat(actualRequest.getRequestedShipment()
                .getShippingChargesPayment()
                .getEPaymentDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getShippingChargesPayment()
                .getEPaymentDetail()
                .getEPaymentMode(),
                equalTo(requestedShipmentDetail.getShippingChargesPayment()
                        .getEpaymentMode()
                        .name()));

    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created with express after pickup
     */
    @Test
    public void testBuildRequestWithExpressAfterPickup() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.setShipTimestamp("2020-10-15T13:57:37.039-05:00");
        requestedShipmentDetail.setExpressAfterPickup(true);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(requestedShipmentDetail, createWorkstationDetails());

        verify(serviceUtil, times(1)).getCustomerTransactionId(serviceConfig.getClientApplicationId(),
                "2020-10-16T13:57:37.039-05:00", LOCATION_CODE);

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getLocationContactAndAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getHoldAtLocationDetail()
                .getPhoneNumber(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getShipTimestamp(), equalTo("2020-10-16T13:57:37.039-05:00"));

    }

    /**
     * Test case to verify CreateOpenShipmentRequest is created without any user requested special
     * service
     */
    @Test
    public void testBuildRequestMissingShipmentSpecialServiceType() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.setSpecialServicesRequested(createShipmentSpecialServiceWithHomeDelivery());
        requestedShipmentDetail.getSpecialServicesRequested()
                .setSpecialServicesRequestedType(null);
        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(requestedShipmentDetail, createWorkstationDetails());

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested(), nullValue());

    }
    
    /**
     * Test case to verify CreateOpenShipmentRequest is created with EVENT_NOTIFICATION
     */
    @Test
    public void testBuildRequestWithShipperEventNotification() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getSender()
                .getContact()
                .setEmailId("abc@gmai.com");
        requestedShipmentDetail.getSpecialServicesRequested()
                .setSpecialServicesRequestedType(null);

        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(requestedShipmentDetail, createWorkstationDetails());

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail(), notNullValue());

    }
    
    /**
     * Test case to verify CreateOpenShipmentRequest is created with EVENT_NOTIFICATION
     */
    @Test
    public void testBuildRequestWithRecipientEventNotification() {

        RequestedShipmentDetail requestedShipmentDetail = createRequestedShipmentDetail();
        requestedShipmentDetail.getRecipient()
                .getContact()
                .setEmailId("abc@gmai.com");

        CreateOpenShipmentRequest actualRequest =
                shipServiceRequestBuilder.buildRequest(requestedShipmentDetail, createWorkstationDetails());

        assertThat(actualRequest, notNullValue());
        assertThat(actualRequest.getRequestedShipment(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getPackagingType(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getOrigin()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getCountryCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRecipient()
                .getAddress()
                .getPostalCode(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0], notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getWeight(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getValue(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getDryIceWeight()
                        .getUnits(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getRequestedPackageLineItems()[0].getSpecialServicesRequested()
                        .getSpecialServiceTypes(),
                notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getSpecialServiceTypes(), notNullValue());
        assertThat(actualRequest.getRequestedShipment()
                .getSpecialServicesRequested()
                .getEventNotificationDetail(), notNullValue());

    }
    
    /**
     * Create RequestedShipmentDetail with Home Delivery
     * 
     * @return
     */
    private RequestedShipmentDetail createRequestedShipmentDetailWithHomeDelivery() {

        RequestedShipmentDetail requestedShipmentDetail = new RequestedShipmentDetail();
        requestedShipmentDetail.setServiceType("PRIORITY_OVERNIGHT");
        requestedShipmentDetail.setPackagingType("FEDEX_PAK");
        requestedShipmentDetail.setTransactionLocationCode(LOCATION_CODE);
        requestedShipmentDetail.setSender(createShipperPartyDetail());
        requestedShipmentDetail.setOrigin(createShipperPartyDetail());
        requestedShipmentDetail.setRecipient(createShipperPartyDetail());
        requestedShipmentDetail.setVariationOptions(createVariationOptionDetail());
        requestedShipmentDetail.setSpecialServicesRequested(createShipmentSpecialServiceWithHomeDelivery());
        requestedShipmentDetail.setPackageLineItems(createRequestedPackageLineItems());
        requestedShipmentDetail.setShippingChargesPayment(createPayment());
        requestedShipmentDetail.setLabelSpecification(new LabelSpecificationDetail());
        requestedShipmentDetail.setPackageCount("3");
        List<String> actions = new ArrayList<>();
        actions.add("STRONG_VALIDATION");
        requestedShipmentDetail.setActions(actions);
        return requestedShipmentDetail;
    }

    /**
     * Create ShipmentSpecialServiceDetail with Home Delivery
     * 
     * @return
     */
    private ShipmentSpecialServiceDetail createShipmentSpecialServiceWithHomeDelivery() {

        ShipmentSpecialServiceDetail shipSpecialService = new ShipmentSpecialServiceDetail();
        shipSpecialService.setSpecialServicesRequestedType(createShipmentSpecialServiceDetailWithHomeDelivery());
        shipSpecialService.setHomeDeliveryDetail(createHomeDeliveryDetail());

        return shipSpecialService;
    }

    /**
     * Create Home Delivery details
     * 
     * @return
     */
    private HomeDelvDetail createHomeDeliveryDetail() {

        HomeDelvDetail homeDelvDetail = new HomeDelvDetail();
        homeDelvDetail.setDeliveryDate("2021-01-11");
        homeDelvDetail.setHomeDeliveryType(HomeDeliveryType.DATE_CERTAIN);
        homeDelvDetail.setPhoneNumber(createPhoneNumber());

        return homeDelvDetail;
    }

    /**
     * This method return Phone number
     * 
     * @return phoneNumber
     */
    private PhoneNumber createPhoneNumber() {

        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setExtension("54321");
        phoneNumber.setNumber("4699318155");
        return phoneNumber;
    }

    /**
     * Create ShipmentSpecialServiceDetail
     * 
     * @return
     */
    private List<SpecialServiceDescriptionDetail> createShipmentSpecialServiceDetailWithHomeDelivery() {

        List<SpecialServiceDescriptionDetail> speSerList = new ArrayList<>();
        speSerList.add(new SpecialServiceDescriptionDetail("HOME_DELIVERY_PREMIUM", null, null, null));

        return speSerList;
    }

    /**
     * Create RequestedShipmentDetail with Home Delivery
     * 
     * @return
     */
    private RequestedShipmentDetail createRequestedShipmentDetailWithHomeDeliveryWithNullPhoneNumber() {

        RequestedShipmentDetail requestedShipmentDetail = new RequestedShipmentDetail();
        requestedShipmentDetail.setServiceType("PRIORITY_OVERNIGHT");
        requestedShipmentDetail.setPackagingType("FEDEX_PAK");
        requestedShipmentDetail.setTransactionLocationCode(LOCATION_CODE);
        requestedShipmentDetail.setSender(createShipperPartyDetail());
        requestedShipmentDetail.setOrigin(createShipperPartyDetail());
        requestedShipmentDetail.setRecipient(createShipperPartyDetail());
        requestedShipmentDetail.setVariationOptions(createVariationOptionDetail());
        requestedShipmentDetail
                .setSpecialServicesRequested(createShipmentSpecialServiceWithHomeDeliveryWithNullPhoneNumber());
        requestedShipmentDetail.setPackageLineItems(createRequestedPackageLineItems());
        requestedShipmentDetail.setShippingChargesPayment(createPayment());
        requestedShipmentDetail.setLabelSpecification(new LabelSpecificationDetail());
        requestedShipmentDetail.setPackageCount("3");
        List<String> actions = new ArrayList<>();
        actions.add("STRONG_VALIDATION");
        requestedShipmentDetail.setActions(actions);
        return requestedShipmentDetail;
    }

    /**
     * Create ShipmentSpecialServiceDetail with Home Delivery
     * 
     * @return
     */
    private ShipmentSpecialServiceDetail createShipmentSpecialServiceWithHomeDeliveryWithNullPhoneNumber() {

        ShipmentSpecialServiceDetail shipSpecialService = new ShipmentSpecialServiceDetail();
        shipSpecialService.setSpecialServicesRequestedType(createShipmentSpecialServiceDetailWithHomeDelivery());
        shipSpecialService.setHomeDeliveryDetail(createHomeDeliveryDetailWithNullPhoneNumber());

        return shipSpecialService;
    }

    /**
     * Create Home Delivery details
     * 
     * @return
     */
    private HomeDelvDetail createHomeDeliveryDetailWithNullPhoneNumber() {

        HomeDelvDetail homeDelvDetail = new HomeDelvDetail();
        homeDelvDetail.setDeliveryDate("2021-01-11");
        homeDelvDetail.setHomeDeliveryType(HomeDeliveryType.DATE_CERTAIN);
        homeDelvDetail.setPhoneNumber(null);

        return homeDelvDetail;
    }
    
    /**
     * method to create the PackageSpecialServicesRequested.
     */
    private PackageSpecialServicesRequested createPackageSpecialServicesRequested() {

        PackageSpecialServicesRequested  details = new PackageSpecialServicesRequested();
        details.setSpecialServiceTypes(new String[] {});

        return details;
    }
}


