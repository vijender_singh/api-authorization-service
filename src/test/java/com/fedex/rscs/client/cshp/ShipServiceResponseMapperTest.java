package com.fedex.rscs.client.cshp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.nxgen.ship.v17.ientities.AssociatedShipmentDetail;
import com.fedex.nxgen.ship.v17.ientities.CompletedPackageDetail;
import com.fedex.nxgen.ship.v17.ientities.CompletedShipmentDetail;
import com.fedex.nxgen.ship.v17.ientities.ConfirmOpenShipmentReply;
import com.fedex.nxgen.ship.v17.ientities.CreateOpenShipmentReply;
import com.fedex.nxgen.ship.v17.ientities.ModifyOpenShipmentReply;
import com.fedex.nxgen.ship.v17.ientities.Notification;
import com.fedex.nxgen.ship.v17.ientities.OfferingIdentifierDetail;
import com.fedex.nxgen.ship.v17.ientities.PackagingDescription;
import com.fedex.nxgen.ship.v17.ientities.ProductName;
import com.fedex.nxgen.ship.v17.ientities.ServiceDescription;
import com.fedex.nxgen.ship.v17.ientities.ShipmentOperationalDetail;
import com.fedex.nxgen.ship.v17.ientities.ShipmentReply;
import com.fedex.nxgen.ship.v17.ientities.ShippingDocument;
import com.fedex.nxgen.ship.v17.ientities.SpecialServiceDescription;
import com.fedex.nxgen.ship.v17.ientities.TrackingId;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.client.ClientTerminalException;
import com.fedex.rscs.exception.client.ClientTransientException;
import com.fedex.rscs.model.Severity;
import com.fedex.rscs.model.ShippingDocumentType;
import com.fedex.rscs.model.cshp.CompleteShipmentDetail;
import com.fedex.rscs.util.RetailShipmentCreationServiceUtil;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * Test class for ShipServiceResponseMapper
 * 
 * @author 5034922
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ShipServiceResponseMapperTest {

    @Mock
    private ShipServiceClient shipServiceClient;
    @InjectMocks
    private ShipServiceResponseMapper mapper;
    @Mock
    private RetailShipmentCreationServiceUtil util;
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();


    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#mapToShipmentDetail(CreateOpenShipmentReply))} and empty reply
     * is received and it's handled
     */
    @Test
    public void testBuildCreateOpenShipmentReplyEmpty() {

        CreateOpenShipmentReply createOpenShipmentReply = null;

        expectedEx.expect(ClientTransientException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getMessage()));
        mapper.mapToShipmentDetail(createOpenShipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#mapToShipmentDetail(CreateOpenShipmentReply))} and null severity
     * is returned and it's handled
     */
    @Test
    public void testBuildCreateOpenShipmentReplyNullSeverity() {

        CreateOpenShipmentReply createOpenShipmentReply = new CreateOpenShipmentReply();
        createOpenShipmentReply.setHighestSeverity(null);

        expectedEx.expect(ClientTransientException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getMessage()));
        mapper.mapToShipmentDetail(createOpenShipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#mapToShipmentDetail(CreateOpenShipmentReply))} and failure
     * severity is returned and it's handled
     */
    @Test
    public void testBuildCreateOpenShipmentReplyFailureSeverity() {

        CreateOpenShipmentReply createOpenShipmentReply = new CreateOpenShipmentReply();
        createOpenShipmentReply.setHighestSeverity(Severity.FAILURE.name());

        expectedEx.expect(ClientTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getMessage()));
        mapper.mapToShipmentDetail(createOpenShipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#mapToShipmentDetail(CreateOpenShipmentReply))} and error
     * severity is returned and it's handled
     */
    @Test
    public void testBuildCreateOpenShipmentReplyErrorSeverity() {

        String errorMessage = "Application ID is null or empty.";

        CreateOpenShipmentReply createOpenShipmentReply = new CreateOpenShipmentReply();
        createOpenShipmentReply.setHighestSeverity(Severity.ERROR.name());

        Notification notification = new Notification();
        notification.setCode("512");
        notification.setMessage(errorMessage);

        createOpenShipmentReply.setNotifications(new Notification[] {notification});

        expectedEx.expect(ClientTerminalException.class);
        expectedEx.expectMessage(equalTo(errorMessage));
        mapper.mapToShipmentDetail(createOpenShipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#mapToShipmentDetail(CreateOpenShipmentReply))} and error
     * severity is returned and it's handled
     */
    @Test
    public void testBuildCreateOpenShipmentReplyErrorSeverityAndEmptyNotification() {

        CreateOpenShipmentReply createOpenShipmentReply = new CreateOpenShipmentReply();
        createOpenShipmentReply.setHighestSeverity(Severity.ERROR.name());
        CompleteShipmentDetail actualResponse = mapper.mapToShipmentDetail(createOpenShipmentReply);

        assertThat(actualResponse, nullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildModifyOpenShipmentResponse(ModifyOpenShipmentReply)} and
     * empty reply is received and it's handled
     */
    @Test
    public void testBuildModifyShipmentReplyEmpty() {

        ModifyOpenShipmentReply modifyOpenShipmentReply = null;

        expectedEx.expect(ClientTransientException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getMessage()));
        mapper.buildModifyOpenShipmentResponse(modifyOpenShipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildModifyOpenShipmentResponse(ModifyOpenShipmentReply))} and
     * null severity is returned and it's handled
     */
    @Test
    public void testBuildModifyShipmentReplyNullSeverity() {

        ModifyOpenShipmentReply modifyOpenShipmentReply = new ModifyOpenShipmentReply();
        modifyOpenShipmentReply.setHighestSeverity(null);

        expectedEx.expect(ClientTransientException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getMessage()));
        mapper.buildModifyOpenShipmentResponse(modifyOpenShipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildModifyOpenShipmentResponse(ModifyOpenShipmentReply))} and
     * failure severity is returned and it's handled
     */
    @Test
    public void testBuildModifyShipmentReplyFailureSeverity() {

        ModifyOpenShipmentReply modifyOpenShipmentReply = new ModifyOpenShipmentReply();
        modifyOpenShipmentReply.setHighestSeverity(Severity.FAILURE.name());

        expectedEx.expect(ClientTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getMessage()));
        mapper.buildModifyOpenShipmentResponse(modifyOpenShipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildModifyOpenShipmentResponse(ModifyOpenShipmentReply))} and
     * error severity is returned and it's handled
     */
    @Test
    public void testBuildModifyShipmentReplyErrorSeverity() {

        String errorMessage = "Application ID is null or empty.";

        ModifyOpenShipmentReply modifyOpenShipmentReply = new ModifyOpenShipmentReply();
        modifyOpenShipmentReply.setHighestSeverity(Severity.ERROR.name());

        Notification notification = new Notification();
        notification.setCode("512");
        notification.setMessage(errorMessage);

        modifyOpenShipmentReply.setNotifications(new Notification[] {notification});

        expectedEx.expect(ClientTerminalException.class);
        expectedEx.expectMessage(equalTo(errorMessage));
        mapper.buildModifyOpenShipmentResponse(modifyOpenShipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildModifyOpenShipmentResponse(ModifyOpenShipmentReply))} and
     * error severity is returned and it's handled
     */
    @Test
    public void testBuildModifyShipmentReplyErrorSeverityAndEmptyNotification() {

        ModifyOpenShipmentReply modifyOpenShipmentReply = new ModifyOpenShipmentReply();
        modifyOpenShipmentReply.setHighestSeverity(Severity.ERROR.name());
        CompleteShipmentDetail actualResponse = mapper.buildModifyOpenShipmentResponse(modifyOpenShipmentReply);

        assertThat(actualResponse, nullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#mapToShipmentDetail(CreateOpenShipmentReply))} and empty reply
     * is received.
     */
    @Test
    public void testMapToShipmentDetailIfCompletedShipmentDetailEmpty() {

        CreateOpenShipmentReply createOpenShipmentReply = new CreateOpenShipmentReply();
        createOpenShipmentReply.setHighestSeverity(Severity.SUCCESS.name());
        createOpenShipmentReply.setCompletedShipmentDetail(null);
        CompleteShipmentDetail shipmentDetail = mapper.mapToShipmentDetail(createOpenShipmentReply);

        assertThat(shipmentDetail, nullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#mapToShipmentDetail(CreateOpenShipmentReply))} and non empty
     * reply is received.
     */
    @Test
    public void testMapToShipmentDetailIfCompletedShipmentDetailNotEmpty() {

        CreateOpenShipmentReply createOpenShipmentReply = new CreateOpenShipmentReply();
        createOpenShipmentReply.setHighestSeverity(Severity.SUCCESS.name());
        CompletedShipmentDetail completedShipmentDetail = new CompletedShipmentDetail();
        completedShipmentDetail.setCompletedPackageDetails(null);
        completedShipmentDetail.setServiceDescription(null);
        completedShipmentDetail.setSpecialServiceDescriptions(null);
        completedShipmentDetail.setPackagingDescription(null);
        createOpenShipmentReply.setCompletedShipmentDetail(completedShipmentDetail);
        CompleteShipmentDetail shipmentDetail = mapper.mapToShipmentDetail(createOpenShipmentReply);

        assertThat(shipmentDetail, notNullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#mapToShipmentDetail(CreateOpenShipmentReply))} and
     * CompleteShipmentDetail object is received.
     */
    @Test
    public void testMapToShipmentDetailSuccessRespone() {

        CreateOpenShipmentReply reply = new CreateOpenShipmentReply();
        reply.setHighestSeverity(Severity.SUCCESS.name());
        CompletedShipmentDetail completedShipmentDetail = new CompletedShipmentDetail();
        completedShipmentDetail.setUsDomestic(true);
        completedShipmentDetail.setCarrierCode("FDXG");
        completedShipmentDetail.setMasterTrackingId(getMasterTracking());
        completedShipmentDetail.setServiceDescription(getServiceDescription());
        completedShipmentDetail.setPackagingDescription(getPackagingDescription());
        completedShipmentDetail.setSpecialServiceDescriptions(getSpecialServiceDescription().stream()
                .toArray(SpecialServiceDescription[]::new));
        completedShipmentDetail.setOperationalDetail(getOperationalDetail());
        completedShipmentDetail.setCompletedPackageDetails(getCompletedPackageDetail().stream()
                .toArray(CompletedPackageDetail[]::new));
        reply.setCompletedShipmentDetail(completedShipmentDetail);
        CompleteShipmentDetail shipmentDetail = mapper.mapToShipmentDetail(reply);

        assertThat(shipmentDetail, notNullValue());
        assertThat(shipmentDetail.getMasterTrackingId(), notNullValue());
        assertThat(shipmentDetail.getCarrierCode(), notNullValue());
        assertThat(shipmentDetail.getCompletedPackageDetails(), notNullValue());
        assertThat(shipmentDetail.getOperationalDetail(), notNullValue());
        assertThat(shipmentDetail.getPackagingDescription(), notNullValue());
        assertThat(shipmentDetail.getServiceDescription(), notNullValue());
        assertThat(shipmentDetail.getServiceDescription(), notNullValue());
        assertThat(shipmentDetail.getSpecialServiceDescriptions(), notNullValue());
    }

    /**
     * Create Mock for CompletedPackageDetail.
     * 
     * @return
     */
    private List<CompletedPackageDetail> getCompletedPackageDetail() {

        List<CompletedPackageDetail> listCompletedPackageDetail = new ArrayList<>();
        CompletedPackageDetail completedPackageDetail = new CompletedPackageDetail();
        completedPackageDetail.setSequenceNumber(1);
        completedPackageDetail.setGroupNumber(0);
        Stream<TrackingId> stream = Stream.of(getMasterTracking());
        completedPackageDetail.setTrackingIds(stream.toArray(TrackingId[]::new));
        completedPackageDetail.setSignatureOption("SERVICE_DEFAULT");
        listCompletedPackageDetail.add(completedPackageDetail);
        return listCompletedPackageDetail;
    }

    /**
     * Create mock for ShipmentOperationalDetail.
     * 
     * @return
     */
    private ShipmentOperationalDetail getOperationalDetail() {

        ShipmentOperationalDetail shipmentOperationalDetail = new ShipmentOperationalDetail();
        shipmentOperationalDetail.setOriginLocationNumber(380);
        shipmentOperationalDetail.setDestinationLocationNumber(752);
        shipmentOperationalDetail.setDeliveryDate("2020-07-29");
        shipmentOperationalDetail.setDeliveryDay("WED");
        shipmentOperationalDetail.setTransitTime("TWO_DAYS");
        shipmentOperationalDetail.setIneligibleForMoneyBackGuarantee(false);
        String[] stringArray = {"SATURDAY_DELIVERY"};
        shipmentOperationalDetail.setDeliveryEligibilities(stringArray);
        shipmentOperationalDetail.setServiceCode("92");
        shipmentOperationalDetail.setPackagingCode("01");
        return shipmentOperationalDetail;
    }

    /**
     * Create Mock for SpecialServiceDescription.
     * 
     * @return
     */
    private List<SpecialServiceDescription> getSpecialServiceDescription() {

        List<SpecialServiceDescription> listSpecialServiceDescription = new ArrayList<>();
        SpecialServiceDescription specialServiceDescription = new SpecialServiceDescription();
        specialServiceDescription.setIdentifier(getIdentifiee());
        specialServiceDescription.setNames(createNames().stream()
                .toArray(ProductName[]::new));
        listSpecialServiceDescription.add(specialServiceDescription);
        return listSpecialServiceDescription;
    }

    /**
     * Create Mock for OfferingIdentifierDetail
     * 
     * @return
     */
    private OfferingIdentifierDetail getIdentifiee() {

        OfferingIdentifierDetail identifier = new OfferingIdentifierDetail();
        identifier.setCode("02");
        identifier.setId("EP1000000060");
        identifier.setType("DELIVER_WEEKDAY");
        return identifier;
    }

    /**
     * Create Mock for PackagingDescription.
     * 
     * @return
     */
    private PackagingDescription getPackagingDescription() {

        PackagingDescription packagingDescription = new PackagingDescription();
        packagingDescription.setCode("01");
        packagingDescription.setPackagingType("YOUR_PACKAGING");
        packagingDescription.setAstraDescription("CST PKG");
        packagingDescription.setDescription("Customer Packaging");
        packagingDescription.setNames(createNames().stream()
                .toArray(ProductName[]::new));
        return packagingDescription;
    }

    /**
     * Create mock for ServiceDescription.
     * 
     * @return
     */
    private ServiceDescription getServiceDescription() {

        ServiceDescription serviceDescription = new ServiceDescription();
        serviceDescription.setCode("92");
        serviceDescription.setServiceType("FEDEX_GROUND");
        serviceDescription.setAstraDescription("FXG");
        serviceDescription.setDescription("FedEx Ground");
        serviceDescription.setNames(createNames().stream()
                .toArray(ProductName[]::new));
        return serviceDescription;
    }

    /**
     * Create mock for ProductName.
     * 
     * @return
     */
    private List<ProductName> createNames() {

        List<ProductName> listProductName = new ArrayList<>();
        ProductName productName = new ProductName();
        productName.setEncoding("ascii");
        productName.setType("short");
        productName.setValue("FG");
        listProductName.add(productName);
        return listProductName;
    }

    /**
     * Mock for TrackingId.
     * 
     * @return
     */
    private TrackingId getMasterTracking() {

        TrackingId trackingId = new TrackingId();
        trackingId.setTrackingIdType("FEDEX");
        trackingId.setTrackingNumber("794978662786");
        return trackingId;
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildModifyOpenShipmentResponse(ModifyOpenShipmentReply))} and
     * empty reply is received.
     */
    @Test
    public void testBuildModifyOpenShipmentResponseIfModifyOpenShipmentReplyEmpty() {

        ModifyOpenShipmentReply modifyOpenShipmentReply = new ModifyOpenShipmentReply();
        modifyOpenShipmentReply.setHighestSeverity(Severity.SUCCESS.name());
        modifyOpenShipmentReply.setCompletedShipmentDetail(null);
        CompleteShipmentDetail shipmentDetail = mapper.buildModifyOpenShipmentResponse(modifyOpenShipmentReply);

        assertThat(shipmentDetail, nullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildModifyOpenShipmentResponse(ModifyOpenShipmentReply))} and
     * non empty reply is received.
     */
    @Test
    public void testBuildModifyOpenShipmentResponseIfModifyOpenShipmentReplyNotEmpty() {

        ModifyOpenShipmentReply modifyOpenShipmentReply = new ModifyOpenShipmentReply();
        modifyOpenShipmentReply.setHighestSeverity(Severity.SUCCESS.name());
        CompletedShipmentDetail completedShipmentDetail = new CompletedShipmentDetail();
        completedShipmentDetail.setCompletedPackageDetails(null);
        completedShipmentDetail.setServiceDescription(null);
        completedShipmentDetail.setSpecialServiceDescriptions(null);
        completedShipmentDetail.setPackagingDescription(null);
        modifyOpenShipmentReply.setCompletedShipmentDetail(completedShipmentDetail);
        CompleteShipmentDetail shipmentDetail = mapper.buildModifyOpenShipmentResponse(modifyOpenShipmentReply);

        assertThat(shipmentDetail, notNullValue());
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildModifyOpenShipmentResponse(ModifyOpenShipmentReply))} and
     * completedPackageDetails is empty.
     */
    @Test
    public void testBuildModifyOpenShipmentResponseIfCompletedPackageDetailsEmpty() {

        ModifyOpenShipmentReply modifyOpenShipmentReply = new ModifyOpenShipmentReply();
        modifyOpenShipmentReply.setHighestSeverity(Severity.SUCCESS.name());
        CompletedShipmentDetail completedShipmentDetail = new CompletedShipmentDetail();
        completedShipmentDetail.setCompletedPackageDetails(null);
        modifyOpenShipmentReply.setCompletedShipmentDetail(completedShipmentDetail);
        CompleteShipmentDetail shipmentDetail = mapper.buildModifyOpenShipmentResponse(modifyOpenShipmentReply);

        assertThat(shipmentDetail, notNullValue());
        assertThat(shipmentDetail.getCompletedPackageDetails(), nullValue());
        assertThat(shipmentDetail.getPackageCount(), equalTo(0));
        
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildModifyOpenShipmentResponse(ModifyOpenShipmentReply))} and
     * CompleteShipmentDetail object is received.
     * 
     * @throws IOException
     */
    @Test
    public void testBuildModifyOpenShipmentResponseSuccessResponse()
            throws IOException {

        CompleteShipmentDetail actualResponse =
                mapper.buildModifyOpenShipmentResponse(createSuccessModifyOpenShipmentReply());

        assertThat(actualResponse, notNullValue());
        assertThat(actualResponse.getMasterTrackingId(), notNullValue());
        assertThat(actualResponse.getCarrierCode(), notNullValue());
        assertThat(actualResponse.getCompletedPackageDetails(), notNullValue());
        assertThat(actualResponse.getPackageCount(), equalTo(actualResponse.getCompletedPackageDetails().size()));
        assertThat(actualResponse.getOperationalDetail(), notNullValue());
        assertThat(actualResponse.getPackagingDescription(), notNullValue());
        assertThat(actualResponse.getServiceDescription(), notNullValue());
        assertThat(actualResponse.getSpecialServiceDescriptions(), notNullValue());
        assertThat(actualResponse.getShipmentDocuments(), notNullValue());
    }

    /**
     * Method to provide mock success response of CSHP modifyOpenShipment.
     * 
     * @return ModifyOpenShipmentReply
     * @throws IOException
     */
    private ModifyOpenShipmentReply createSuccessModifyOpenShipmentReply()
            throws IOException {

        String responseJson =
                "{\"highestSeverity\": \"SUCCESS\",\"notifications\": [{\"severity\": \"SUCCESS\",\"source\": \"ship\",\"code\": \"0000\",\"message\": \"Success\",\"localizedMessage\": \"Success\"}],\"transactionDetail\": {\"customerTransactionId\": \"SSFE-GVTKK-12345671596090460000\",\"localization\": {\"languageCode\": \"en\"}},\"version\": {\"serviceId\": \"ship\",\"major\": \"17\",\"intermediate\": \"0\",\"minor\": \"0\"},\"jobId\": \"65s01692j25ade28713299599\",\"asynchronousProcessingResults\": {\"Options\": \"SYNCHRONOUSLY_PROCESSED\"},\"completedShipmentDetail\": {\"usDomestic\": \"true\",\"carrierCode\": \"FDXE\",\"masterTrackingId\": {\"trackingIdType\": \"FEDEX\",\"formId\": \"0201\",\"trackingNumber\": \"794978918115\"},\"serviceDescription\": {\"serviceType\": \"PRIORITY_OVERNIGHT\",\"code\": \"01\",\"names\": [{\"type\": \"long\",\"encoding\": \"utf-8\",\"value\": \"FedEx Priority OvernightÂ®\"},{\"type\": \"long\",\"encoding\": \"ascii\",\"value\": \"FedEx Priority Overnight\"},{\"type\": \"medium\",\"encoding\": \"utf-8\",\"value\": \"FedEx Priority OvernightÂ®\"},{\"type\": \"medium\",\"encoding\": \"ascii\",\"value\": \"FedEx Priority Overnight\"},{\"type\": \"short\",\"encoding\": \"utf-8\",\"value\": \"P-1\"},{\"type\": \"short\",\"encoding\": \"ascii\",\"value\": \"P-1\"},{\"type\": \"abbrv\",\"encoding\": \"ascii\",\"value\": \"PO\"}],\"description\": \"Priority Overnight\",\"astraDescription\": \"P1\"},\"packagingDescription\": {\"packagingType\": \"YOUR_PACKAGING\",\"code\": \"01\",\"names\": [{\"type\": \"long\",\"encoding\": \"utf-8\",\"value\": \"Your Packaging\"},{\"type\": \"long\",\"encoding\": \"ascii\",\"value\": \"Your Packaging\"},{\"type\": \"medium\",\"encoding\": \"utf-8\",\"value\": \"Your Packaging\"},{\"type\": \"medium\",\"encoding\": \"ascii\",\"value\": \"Your Packaging\"},{\"type\": \"small\",\"encoding\": \"utf-8\",\"value\": \"Your Pkg\"},{\"type\": \"small\",\"encoding\": \"ascii\",\"value\": \"Your Pkg\"},{\"type\": \"short\",\"encoding\": \"utf-8\",\"value\": \"Your\"},{\"type\": \"short\",\"encoding\": \"ascii\",\"value\": \"Your\"},{\"type\": \"abbrv\",\"encoding\": \"ascii\",\"value\": \"YP\"}],\"description\": \"Customer Packaging\",\"astraDescription\": \"CST PKG\"},\"specialServiceDescriptions\": [{\"identifier\": {\"id\": \"EP1000000060\",\"type\": \"DELIVER_WEEKDAY\",\"code\": \"02\"},\"names\": [{\"type\": \"long\",\"encoding\": \"utf-8\",\"value\": \"Deliver Weekday\"},{\"type\": \"long\",\"encoding\": \"ascii\",\"value\": \"Deliver Weekday\"},{\"type\": \"medium\",\"encoding\": \"utf-8\",\"value\": \"Deliver Weekday\"},{\"type\": \"medium\",\"encoding\": \"ascii\",\"value\": \"Deliver Weekday\"},{\"type\": \"short\",\"encoding\": \"utf-8\",\"value\": \"WDY\"},{\"type\": \"short\",\"encoding\": \"ascii\",\"value\": \"WDY\"}]}],\"operationalDetail\": {\"ursaPrefixCode\": \"NA\",\"ursaSuffixCode\": \"ODMA\",\"originLocationId\": \"TRLA\",\"originLocationNumber\": \"0\",\"originServiceArea\": \"A2\",\"destinationLocationId\": \"ODMA\",\"destinationLocationNumber\": \"0\",\"destinationServiceArea\": \"A1\",\"destinationLocationStateOrProvinceCode\": \"MD\",\"deliveryDate\": \"2020-07-31\",\"deliveryDay\": \"FRI\",\"commitDate\": \"2020-07-31\",\"commitDay\": \"FRI\",\"ineligibleForMoneyBackGuarantee\": \"false\",\"astraPlannedServiceLevel\": \"FRI - 31 JUL 10:30A\",\"astraDescription\": \"PRIORITY OVERNIGHT\",\"postalCode\": \"21212\",\"stateOrProvinceCode\": \"MD\",\"countryCode\": \"US\",\"airportId\": \"BWI\",\"serviceCode\": \"01\",\"packagingCode\": \"01\"},\"completedPackageDetails\": [{\"sequenceNumber\": \"1\",\"trackingIds\": [{\"trackingIdType\": \"FEDEX\",\"formId\": \"0201\",\"trackingNumber\": \"794978918115\"}],\"groupNumber\": \"0\",\"operationalDetail\": {\"operationalInstructions\": [{\"number\": 1,\"content\": \"TRK#\"},{\"number\": 1,\"content\": \"0201\"},{\"number\": 1,\"content\": \"NA ODMA\"},{\"number\": 1,\"content\": \"1001891724510002121200794978918115\"},{\"number\": 1,\"content\": \"56BJ2/8DAF/B766\"},{\"number\": 1,\"content\": \"7949 7891 8115\"},{\"number\": 1,\"content\": \"FRI - 31 JUL 10:30A\"},{\"number\": 1,\"content\": \"PRIORITY OVERNIGHT\"},{\"number\": 1,\"content\": \"21212\"},{\"number\": 1,\"content\": \"MD-US\"},{\"number\": 1,\"content\": \"BWI\"}],\"barcodes\": {\"binaryBarcodes\": [{\"type\": \"COMMON_2D\",\"value\": \"Wyk+HjAxHTAyMjEyMTIdODQwHTAxHTc5NDk3ODkxODExNTAyMDEdRkRFHTAzMzYwMDAwMx0yMTIdHTEvMR0xMC4wMExCHU4dbm9pZGEdQmFsdGltb3JlHU1EHWluZm9nYWluHjA2HTEwWkVEMDA4HTEyWjY3NjU0NTQzNzYdMTVaMDA2OTkwMTAwHTIwWhwxMDAdMzFaMTAwMTg5MTcyNDUxMDAwMjEyMTIwMDc5NDk3ODkxODExNR0zMlowMh0zNFowMR0zOVpUUkxBHR4wOR1GRFgdeh04HRgtGi8CAn9AHgQ=\"}],\"stringBarcodes\": [{\"type\": \"FEDEX_1D\",\"value\": \"1001891724510002121200794978918115\"}]}},\"label\": {\"type\": \"OUTBOUND_LABEL\",\"shippingDocumentDisposition\": \"RETURNED\",\"imageType\": \"PDF\",\"resolution\": \"200\",\"copiesToPrint\": \"1\",\"parts\": [{\"documentPartSequenceNumber\": \"1\",\"image\": \"JVBERi0xLjQKMSAwIG9iago8PAovVHlwZSAvQ2F0YWxvZwovUGFnZXMgMyAwIFIKPj4KZW5kb2JqCjIgMCBvYmoKPDwKL1R5cGUgL091dGxpbmVzCi9Db3VudCAwCj4+CmVuZG9iagozIDAgb2JqCjw8Ci9UeXBlIC9QYWdlcwovQ291bnQgMQovS2lkcyBbMTggMCBSXQo+PgplbmRvYmoKNCAwIG9iagpbL1BERiAvVGV4dCAvSW1hZ2VCIC9JbWFnZUMgL0ltYWdlSV0KZW5kb2JqCjUgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUxCi9CYXNlRm9udCAvSGVsdmV0aWNhCi9FbmNvZGluZyAvTWFjUm9tYW5FbmNvZGluZwo+PgplbmRvYmoKNiAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9IZWx2ZXRpY2EtQm9sZAovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjcgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUxCi9CYXNlRm9udCAvSGVsdmV0aWNhLU9ibGlxdWUKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iago4IDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0hlbHZldGljYS1Cb2xkT2JsaXF1ZQovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjkgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUxCi9CYXNlRm9udCAvQ291cmllcgovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjEwIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0NvdXJpZXItQm9sZAovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjExIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0NvdXJpZXItT2JsaXF1ZQovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjEyIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0NvdXJpZXItQm9sZE9ibGlxdWUKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iagoxMyAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9UaW1lcy1Sb21hbgovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjE0IDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL1RpbWVzLUJvbGQKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iagoxNSAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9UaW1lcy1JdGFsaWMKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iagoxNiAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9UaW1lcy1Cb2xkSXRhbGljCi9FbmNvZGluZyAvTWFjUm9tYW5FbmNvZGluZwo+PgplbmRvYmoKMTcgMCBvYmogCjw8Ci9DcmVhdGlvbkRhdGUgKEQ6MjAwMykKL1Byb2R1Y2VyIChGZWRFeCBTZXJ2aWNlcykKL1RpdGxlIChGZWRFeCBTaGlwcGluZyBMYWJlbCkNL0NyZWF0b3IgKEZlZEV4IEN1c3RvbWVyIEF1dG9tYXRpb24pDS9BdXRob3IgKENMUyBWZXJzaW9uIDUxMjAxMzUpCj4+CmVuZG9iagoxOCAwIG9iago8PAovVHlwZSAvUGFnZQ0vUGFyZW50IDMgMCBSCi9SZXNvdXJjZXMgPDwgL1Byb2NTZXQgNCAwIFIgCiAvRm9udCA8PCAvRjEgNSAwIFIgCi9GMiA2IDAgUiAKL0YzIDcgMCBSIAovRjQgOCAwIFIgCi9GNSA5IDAgUiAKL0Y2IDEwIDAgUiAKL0Y3IDExIDAgUiAKL0Y4IDEyIDAgUiAKL0Y5IDEzIDAgUiAKL0YxMCAxNCAwIFIgCi9GMTEgMTUgMCBSIAovRjEyIDE2IDAgUiAKID4+Ci9YT2JqZWN0IDw8IC9GZWRFeEV4cHJlc3MgMjAgMCBSCi9FeHByZXNzRSAyMSAwIFIKL2JhcmNvZGUwIDIyIDAgUgo+Pgo+PgovTWVkaWFCb3ggWzAgMCA3OTIgNjEyXQovVHJpbUJveFswIDAgNzkyIDYxMl0KL0NvbnRlbnRzIDE5IDAgUgovUm90YXRlIDkwPj4KZW5kb2JqCjE5IDAgb2JqCjw8IC9MZW5ndGggMjk4NwovRmlsdGVyIFsvQVNDSUk4NURlY29kZSAvRmxhdGVEZWNvZGVdIAo+PgpzdHJlYW0KR2F0VTY5b2c6JyZcU15KczNPImAxKF5pYSg5Pk8xZ1RHMGkqXyJkX0ciNl1Xa2BYVEArYywmRTdNNmxaNSonNyMlIiRXSTtmV2cyS1JVaGoKPFE0ZVtRaSdEImkzQytVPUdkMkIpOXEpaypET0lmJzdwWzByRiMsPHEiVDtaWl86dWotXyZgTyhdW1ojQ2tCLG8vWz85YWowOy4uIVhFYGUKP10nQl86VjwzSEdCO0QhXlU2RUslWSo0OSpZLzUpJSFLRmluOiwoVVFIO1tuJkE9LGUwVzExayYiRFBCXChCQjghTFA/aTJVPEhlRGcsO2sKXixjKz5aVEFrKVtuLGdaQDg1MVQtXGk1T0UrRCpvcjhEakAkPl1KdGo8Um1LcUBIL15BNVckZFEiPXBHXEBnXVFDJmM/UGE3Jl8/TEArWUIKRztZSi1xPWBAby1OL2xpOCZqazUlbVJdZD5YPVkmUixRVD9mQz0sTG5ubi0pKzJSWFdyc2VrOzNFb1RuWlRdLV8wQyVKZ2lXbnBKcDNQQkkKPzRIM0dTanBoK0xBYlRTLF49Qm5CUmZkVGY1ZUFHYS4pckkkNi9xUyk9Jjg0WzVHcEhwWj9FZFpLVC9DRj9bPEQ3IStQOCdNZmQvKHBGWkAKZFAhaSc2IVQzSFQnZlA4UlBBRDMsMj5wXlItU0YoKEJwRGlldC5CMj11YFVDLkNeOGNeJWY0Y2RKXidSXlNcUl8zQD8mdEUhMj4jXURoY0gKcmwra1NmUGRyJyYkTix0JHUkbGEsWUs+dEdsaFIlcD9qQVFwdFtsUlY5Ym1gTlxGV0ptTW5bKV1DIyNeTzFXJGttYnQ0NnBeOGpxX1hfZEMKaDsuUC9mM2NTV0laayZVYTVvODNZN0NIXXBDRm1uJiROSHBaZC1Sb0BXWCEsSXNcWGhtJ2FnbCFLJDhiR2wsIUcjQUoyZSkudTVgXyR0NSwKRmw8NkNNcGhfMGdKS15uakBEZ2EuRWJrRGM3LTZzMDxYTDZtMGVgL3ErTnM5LFhCWlJtIlR1ajlER1Z1WVx1R3VcRUMiRmRoUEApNk5UbG4KZT5gSyg7W0dITjRbOE5QakkjXDFMNHVfMTIoVjgyZ2g0Y3RjY28tb0dgKyRZMkQ1dFxUKExMY0I4TTwiKC4kQjhhYD5OWzpkXCFvYCxiSXQKNGNlQGBnWkxMXWpNSzlWK0hWNyMlazsmcEE8akg8JzQiRzxrVixoOiw0cURYZ2g4QE1rLmFcTTQ7Q284UGdebHFaRkVPNm5BP0deRXRcO2gKUEtpLU9OUC5fYytxdWhmciMsY19MNFxBP2k8ISRuZFQiW0hnK2I4JWE8OzVmQk04OmhOWSIrUmliUDNVREhVRHFkV2NXNFdeW0xQNTE7QzkKXSIzSTRPRy0uKTZRcURScS0nX0wlT1RMRzckP2FrWz9XLE4jJFAoZGcrZDFPMXJAbT9oM2tGIjNwPEJQTC9aJyFJOl10b2tvZUdwW2JFWkQKIjBMJCRqaUJuVllUZ0UsMUwjS1JPTkgiQzZeb0pJPDRnak4yO0xeWCQvJ3M1WihvTEU7SUczWEQ3LURQZlhkRE9fbzNGbilGYTY0PnRyc1oKbFMzIjJOZylgZzlYTFVFYGNoYGtTJ047M2pqLG4mNFBNXmhTYlpIajZUdG5AOlBPI0tIJ2hYQy0+XW1yJFkkTTpPdXBaRkVFWnE9ImZ0KCYKYSg8dElfMyFVJlJdc1ZTI1whSFk3MT4sbDFmPXVGPUdJP0I3W2xLcVhJJ0dzTGIpcFMyQydgQ0pWclNFSigsZl1URlFcJ2guPS1SND0xc1wKZzU8KVFIM2pLNVpPZjxWb2UrZXNbVEYmSyo0JFlWNFIvO1hvcDJzZSM5aCJPYWBUJTlvdGpWSThXMzZROjJMIlwxNVcmVXBfKWc2JzpXMkEKSy9PJFc+QCNyZFc1RG1ELUk2SD0icFhJPF10Wi9NLEA7a2ZkSipsM05OTiIvVG9TbVJZMmcwT0IsSVtKRGc6RFtZbXVIaUpmb0tfI0czPU8KWHBzI20qPUFcUzAvI1pqcEknLl1CcVByaSxFOC11MVpXL3RTcEViLVhAJ109TWNbLC06Pyw7QkdDYWp0V0BSLTlOLzckX1JdOFNTXmo2OV8KJ1BBW04wa2NcYl4uVy1mR09iOlFJJ1EuW09YZmc4VSIncHApKEdJJ2w1XUBKZjUtZE1PKVIvIm9HKSciMFgvJ1VtTGlcXipJXnVtRGg0TkwKVUtmKzIiZnQnVGJHMzc9aS9xXE9QXFMmPj5QRFxbKGFVaEU5U2BbYCIxRDxlYjBIJmpvMWkwczJIWWFXQTRqZiMkTllcPkNsO04hTENmM2QKO1A7OEJJUSJfZ0AtXUY5VGVtTUstMl9wNWheLWNcNWE6Vl88I1VRSDFLMUBhZk9AIXNJYWZXJEZ0Ym1RWG5hVEMlZUtLbV1IVCcrOmxdRT8KP1w8SjRaNUkuMSciVTFcb2xTXE1sYXQhSVM0ZVVYMWBMYFtVZG02bm1YVztGOnA8W10uUDkzYERXOGZUKWotMTBiYFo8OEZfaVlQb2sxL1kKKEAlUCNbXlxcOE8jYDI3Ok11ZzhsTko9c3JlUDVLYDMsOzwiKk42S1JhT2hzNyQ9Z11cN2hORkFQb0c1ZCMuO01XU09fLkhESyNxSUhsMyUKVzc8Y10tLkg/YjwsLC5yJ24yXFhUU2ZuJzNnPFZzTSpvXFJiUW1BSldwMChPPXUnX084amRIZWhaNGkmazMicD06LktCNyYsWT49ISZvQiYKcD0zUXEncSpgQEMtTyhYTiRJTlRrJGBjPllXciYyXEdxJFBFNDo+ZWg0XWBfcVFjYCZmaWhCSjJfTSNfS2FTKWZAWG8rK1dJZU1xVWJtI1oKbCw3cD46TkohOWR0LV1xPEhiI1gnVUE2bjYpVHEnaVV1b3RyITQ/K0A/RHAyOG5sKCpkaCModTQ1S2g9VC0mY01uJTRJUmRmSyFwb2wuWm8KSUNdYV1mNElRcDxGLHQ4ZGJ1SXA3P0YvXk5GKSx0L1tKJChQV3NXNGZBSFY4cW0/N21jXUBPRXEuODRYXV9RJ3MsK2M9ODU+SmFdZD9OY1QKRFs3PyY3JjphIjsrV1RDOWU8bWk6JFRob2svaGhnVzQmNEEuLVZGblx1QzQ/UFhQKitHcWgkYVQiazlkaixaRylvXEw6UmFbRWRocUo9L2wKPSwvZTQsSiYuXWhaZ2JmXClvOyNLSjo2V3MiZGcjUT5tS2AlOztDczZxQC9AWkAlbiY6Ki49bChzRChtcj09Zl8zZ2M+cGNjajNeOkYldWwKbVc3OyNROGhvXSVDL1RMbG1cI1Q9W2Z1cGZiVTY8REU2N0M0LDNbJ1h0QHRBL1FDK0wpVT9DSFMjXD5MJGBzY1ZbWXQ+Kz1ALSFbZl5CTDIKJyIyTzsiQFtTZFIkXDFzWkVGTk1ZXVU4X0ZHcSVsJ1tAXDYhRic2XkhxOi0yN21eV1AtczljNFt0KFVHLyUtRyM/c1xbPDxEa2ojIWpudTAKcSEhVWYpUGhfO1EkPFxgUl5QYTMzTzskXC5vLEVEPUZhSCpSYCJiUj43N1E6MCJCPlhnMnVzcyxsSW5iT190MidWclFlOEJHb2A4XyxXTCcKIUV1Qzo6P1RyRi9bVC0kMFYpQkxAM201KidqRCdKJDVdVT0kUUVQOTh1RzprIl9hSVA6T2RuZ2BjM2k8ZzUsdUhTU2dJalotJyR0NlNFZWAKQ1M8VzdUaV1HcWJUU09qOTpfXmtUSlxYKCZwRUw+S0pjYFZnKjNTVEMrJytDN0tcbzBTYFFFcT4lakhaOj9tJ1coMUMxcUQjbWhoMXUvRHUKRD9hVUJUUFhMX0RAJ05BOiYtdGRJZWNlUFtiLlVvZXE/cDM7Iy9VVlRORGw5ZlhmKU8qc2VGUDtYcS5zYFY0anBycCRvfj4KZW5kc3RyZWFtCmVuZG9iagoyMCAwIG9iago8PCAvVHlwZSAvWE9iamVjdAovU3VidHlwZSAvSW1hZ2UKL1dpZHRoIDExOAovSGVpZ2h0IDQ5Ci9Db2xvclNwYWNlIC9EZXZpY2VHcmF5Ci9CaXRzUGVyQ29tcG9uZW50IDgKL0xlbmd0aCA0NjEKL0ZpbHRlciBbL0FTQ0lJODVEZWNvZGUgL0ZsYXRlRGVjb2RlXQo+PnN0cmVhbQpHYiIvZUpJXVI/I1huWGtUNkE+WEtuRGJXQiJNWkVhalBwZDRdJyheQFJsdCJrQiJTMCRmJT89bT00Qzs7LiROKGpSY1lSJXNkJmw5JE90XgopJjc9TCg3LWVxcWVALigvU0M5ODQtZkxYZUxaNXFjWjVsQUVCNTU+M0AwPWsiO1JoMmhCY1FMMyZKPzVvW2NIMGJTRk5ePDs7QDQxU1k5bQpjIlpJPVlsI2M0KWBfIllsbU4zJS5BLmxOU2BWZS9ZPCU/LjcoKCkvLmRCWztrIUpbJSclREpcST8rYTc6NW9Oa1lccUVTPjM8STViWm0oSQolZDxAQik4RGJ0M3BLR2NsYlFrMSdpWlZzN05NMFxvJlIqcVU+Z0h1LUZyQ2toPGtIRkcqXCRNPV9zVi8tJFxKRjplLz8+ZlhBK0NDbXBEZQpkU1A2R2N1LFxYJ0FMPWE1SkNbNDZHLmhwMj1VNGVeJGVvXFEjYiptN0M4JC1KQShjWj9ZYCdDOzZKcEhlUGxcJEhFQyF1XipRYSs2Z0VNawpbZkVWKmIuIz5wcj4tV1NmIz5oJlc/dWtrVHNoWWA9L1guNjZBKjtiaXMvZGdAUVhYc3I4bTt+PgplbmRzdHJlYW0KZW5kb2JqCjIxIDAgb2JqCjw8IC9UeXBlIC9YT2JqZWN0Ci9TdWJ0eXBlIC9JbWFnZQovV2lkdGggNTQKL0hlaWdodCA1NAovQ29sb3JTcGFjZSAvRGV2aWNlR3JheQovQml0c1BlckNvbXBvbmVudCA4Ci9MZW5ndGggNzcKL0ZpbHRlciBbL0FTQ0lJODVEZWNvZGUgL0ZsYXRlRGVjb2RlXQo+PnN0cmVhbQpHYiIwSmQwVGRxJGo0b0ZeVSwiSFRzOUVJRTswQVQsX0UqTFolb0A3Smw1VjtIJ0NzPVRycURhSC40QmYjYzRPVlQ7KGQjZjxHRTl+PgplbmRzdHJlYW0KZW5kb2JqCjIyIDAgb2JqCjw8IC9UeXBlIC9YT2JqZWN0Ci9TdWJ0eXBlIC9JbWFnZQovV2lkdGggMjA5Ci9IZWlnaHQgNjYKL0NvbG9yU3BhY2UgL0RldmljZUdyYXkKL0JpdHNQZXJDb21wb25lbnQgOAovTGVuZ3RoIDEyNjEKL0ZpbHRlciBbL0FTQ0lJODVEZWNvZGUgL0ZsYXRlRGVjb2RlXQo+PnN0cmVhbQpHYiIvY2dRRUxBI1hlRmZULWZoR2xVXEIxPFIqdSZZY0xkbk5yJmtqRi5pRW5vQ01lKFQvclRuclZPOUkqa2ldQU06XyVFRikrXV0/VFtCLApBJDkzKTMmYExeTHAtR1dDSUxAYmI5c0Nua3BeKHUrb1BBKjAtLDBNVkdIV1sjYCFIYFgsTDc0PkZnIllVO1AlRVM3dWNKVSEkV2xXPy5LaAplMjwtIS0tTiZNLiFOOG8ib0w0WjBuLCEiPW5XY15EWVs7VS1SVXJiRT9CVTFwVHA5Uk4+dG07KCFJP2oqZ1BKcWE9aGkyM0kzJCtAdV5PLQpEOzNOOUNSIlVMTVhubUE6ZGpUXmRoJVs2PVFMT11QTUAxKmVLJlgtJHQ2SlkzcmQkKGFYXlwpQl11Uig0KE49YUdWYjBGMzw2RzQsS29wNApOSXVbNjdtYyQoNlRQPmcsYGRdWmUhMD5BaU8yI2pfTiRhMT84T2ZVJCI6RlQlMlJhYjklPzpXTi9jKyReaDYuR3JUQWhVVDRTKSNiRCQsMgpdblRALzNkTzNRLFdXYnRhJSIkOnBPMms1Q1AuVixtQ1pvaDwxbTNzZD8qN2wlMmdvLU1aWUxFXjkwS2NkLD4pXlFtODBySTNULSNAcXVQTAomKklCJS9pNkNBJGQnNWZGLU8lUThaMkE9YkhrO1IjX3VaI1ciQmFSaDZObkpuQytzZW91Ilg2NlloKDY9MDQ/YjtgLUBVOERlKCRiYjlMZApPcyFtczNdMC4/VGE0JDA5alcwOzRPJ1ZKJzhtPUlOWV9dWWMrQ2tmZGJFRVkvUGFFMFZnbzNQWVJrK0NEISt1OjgyKG80SzlJbDlgNlE/bQpcXkJcUS8kMS8wL0hcRVFiPDtpJGFpXUtLOSs/a1dMbSREQzxBSilDM0ZhYi0sbWtnKmAxSUQ7WzJtUF5UXUdEIkw+OEJrNiZWNGo7V2BjNgpOUlpLJ24vKiojREVyJiVXZi1SI19RR2EhJystUiIxNFYsWyFLSmtvQjoxaShbdCZNckohL29YTUU/Lm9qbml1WytEYlwiYWJbcWAzNCZzcwpQNk9DL09NOkphNVpEQikoJUNbPGthTkgpMjdyWlVWJVkyUkQ5Sy9wZUVsUF9sNjNqQjJScTYsKiFhKGIjciFMLlVmYHRoYCg5NFFAQEI0OwpGVyxqTT9eT1tkOHFpWU9MOkJvLEYyXUpuQC1jXSddTiZGR1RvKk4qa111ZElTSGIqcSx0TGNSPiRmRm0kal45RmVRKkR0KSZGIXRGRU9pMwpWPz1dTmQsUTVmUmF0Mj5oN2RgJmoxP2E0JUMpL0A7aFtQLzRjcmJgaV5RIjxCc0opRURPLkxIQ2AndSJUUW46WSgnbTEvZCpDMU5nUlgncQo7TW8/U1UpQE1YPWIvJEVBM2FeXkMpI1drTzp0KmpYaTBGdUdJbyNtVHNmP2ZEL1EwL0JBOD5QbD9lT0o3b1FhRUV1aFBnU2VGKGZCXiErXwomMHEqLl1LOCRUXFMnKkpjQyM7VTFqJS8sOE0hUENkO0NXJXFbQy5FSVE3YSQ4WEhqakRTXzxkTDBocDk1LEMjWjw3Rz1NRTRzaUxtLS9VOgptVHM2dWVcUnBzNFo7JlohVEEuSVJBSWcsR1Vrak1pM3NSIkF0TkYpSWhISn4+CmVuZHN0cmVhbQplbmRvYmoKeHJlZgowIDIzCjAwMDAwMDAwMDAgNjU1MzUgZiAKMDAwMDAwMDAwOSAwMDAwMCBuIAowMDAwMDAwMDU4IDAwMDAwIG4gCjAwMDAwMDAxMDQgMDAwMDAgbiAKMDAwMDAwMDE2MiAwMDAwMCBuIAowMDAwMDAwMjE0IDAwMDAwIG4gCjAwMDAwMDAzMTIgMDAwMDAgbiAKMDAwMDAwMDQxNSAwMDAwMCBuIAowMDAwMDAwNTIxIDAwMDAwIG4gCjAwMDAwMDA2MzEgMDAwMDAgbiAKMDAwMDAwMDcyNyAwMDAwMCBuIAowMDAwMDAwODI5IDAwMDAwIG4gCjAwMDAwMDA5MzQgMDAwMDAgbiAKMDAwMDAwMTA0MyAwMDAwMCBuIAowMDAwMDAxMTQ0IDAwMDAwIG4gCjAwMDAwMDEyNDQgMDAwMDAgbiAKMDAwMDAwMTM0NiAwMDAwMCBuIAowMDAwMDAxNDUyIDAwMDAwIG4gCjAwMDAwMDE2MjIgMDAwMDAgbiAKMDAwMDAwMjAwMiAwMDAwMCBuIAowMDAwMDA1MDgxIDAwMDAwIG4gCjAwMDAwMDU3MjggMDAwMDAgbiAKMDAwMDAwNTk4OSAwMDAwMCBuIAp0cmFpbGVyCjw8Ci9JbmZvIDE3IDAgUgovU2l6ZSAyMwovUm9vdCAxIDAgUgo+PgpzdGFydHhyZWYKNzQzNwolJUVPRgo=\"}]},\"signatureOption\": \"SERVICE_DEFAULT\"}]}"
                        + "}";

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper.readValue(responseJson, ModifyOpenShipmentReply.class);
    }


    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildModifyOpenShipmentResponse(ModifyOpenShipmentReply))} and
     * CompleteShipmentDetail object is received but it fails to decode the image buffer
     * 
     * @throws IOException
     */
    @Test
    public void testBuildModifyOpenShipmentResponseImageDecodeFailure()
            throws IOException {

        when(util.decodePackageDocument(any(ShippingDocument.class), eq(ShippingDocumentType.OUTBOUND_LABEL)))
                .thenThrow(new ClientTransientException(ServiceErrorCode.INTERNAL_SERVER_ERROR));
        expectedEx.expect(ClientTransientException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.INTERNAL_SERVER_ERROR.getMessage()));
        mapper.buildModifyOpenShipmentResponse(createSuccessModifyOpenShipmentReply());
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildConfirmOpenShipmentResponse(ConfirmOpenShipmentReply)} and
     * empty reply is received and it's handled
     */
    @Test
    public void testBuildConfirmShipmentReplyEmpty() {

        ConfirmOpenShipmentReply confirmOpenShipmentReply = null;

        expectedEx.expect(ClientTransientException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getMessage()));
        mapper.buildConfirmOpenShipmentResponse(confirmOpenShipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildConfirmOpenShipmentResponse(ConfirmOpenShipmentReply))} and
     * null severity is returned and it's handled
     */
    @Test
    public void testBuildConfirmShipmentReplyNullSeverity() {

        ConfirmOpenShipmentReply confirmOpenShipmentReply = new ConfirmOpenShipmentReply();
        confirmOpenShipmentReply.setHighestSeverity(null);

        expectedEx.expect(ClientTransientException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getMessage()));
        mapper.buildConfirmOpenShipmentResponse(confirmOpenShipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildConfirmOpenShipmentResponse(ConfirmOpenShipmentReply))} and
     * failure severity is returned and it's handled
     */
    @Test
    public void testBuildConfirmShipmentReplyFailureSeverity() {

        ConfirmOpenShipmentReply confirmOpenShipmentReply = new ConfirmOpenShipmentReply();
        confirmOpenShipmentReply.setHighestSeverity(Severity.FAILURE.name());

        expectedEx.expect(ClientTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getMessage()));
        mapper.buildConfirmOpenShipmentResponse(confirmOpenShipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildConfirmOpenShipmentResponse(ConfirmOpenShipmentReply))} and
     * error severity is returned and it's handled
     */
    @Test
    public void testBuildConfirmShipmentReplyErrorSeverity() {

        String errorMessage = "Application ID is null or empty.";

        ConfirmOpenShipmentReply confirmOpenShipmentReply = new ConfirmOpenShipmentReply();
        confirmOpenShipmentReply.setHighestSeverity(Severity.ERROR.name());

        Notification notification = new Notification();
        notification.setCode("512");
        notification.setMessage(errorMessage);

        confirmOpenShipmentReply.setNotifications(new Notification[] {notification});

        expectedEx.expect(ClientTerminalException.class);
        expectedEx.expectMessage(equalTo(errorMessage));
        mapper.buildConfirmOpenShipmentResponse(confirmOpenShipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildConfirmOpenShipmentResponse(ConfirmOpenShipmentReply))} and
     * error severity is returned and it's handled
     */
    @Test
    public void testBuildConfirmShipmentReplyErrorSeverityAndEmptyNotification() {

        ConfirmOpenShipmentReply confirmOpenShipmentReply = new ConfirmOpenShipmentReply();
        confirmOpenShipmentReply.setHighestSeverity(Severity.ERROR.name());
        CompleteShipmentDetail actualResponse = mapper.buildConfirmOpenShipmentResponse(confirmOpenShipmentReply);

        assertThat(actualResponse, nullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildConfirmOpenShipmentResponse(ConfirmOpenShipmentReply))} and
     * empty reply is received.
     */
    @Test
    public void testBuildConfirmOpenShipmentResponseIfConfirmOpenShipmentReplyEmpty() {

        ConfirmOpenShipmentReply confirmOpenShipmentReply = new ConfirmOpenShipmentReply();
        confirmOpenShipmentReply.setHighestSeverity(Severity.SUCCESS.name());
        confirmOpenShipmentReply.setCompletedShipmentDetail(null);
        CompleteShipmentDetail shipmentDetail = mapper.buildConfirmOpenShipmentResponse(confirmOpenShipmentReply);

        assertThat(shipmentDetail, nullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildConfirmOpenShipmentResponse(ConfirmOpenShipmentReply))} and
     * non empty reply is received.
     */
    @Test
    public void testBuildConfirmOpenShipmentResponseIfConfirmOpenShipmentReplyNotEmpty() {

        ConfirmOpenShipmentReply confirmOpenShipmentReply = new ConfirmOpenShipmentReply();
        confirmOpenShipmentReply.setHighestSeverity(Severity.SUCCESS.name());
        CompletedShipmentDetail completedShipmentDetail = new CompletedShipmentDetail();
        completedShipmentDetail.setCompletedPackageDetails(null);
        completedShipmentDetail.setServiceDescription(null);
        completedShipmentDetail.setSpecialServiceDescriptions(null);
        completedShipmentDetail.setPackagingDescription(null);
        confirmOpenShipmentReply.setCompletedShipmentDetail(completedShipmentDetail);
        CompleteShipmentDetail shipmentDetail = mapper.buildConfirmOpenShipmentResponse(confirmOpenShipmentReply);

        assertThat(shipmentDetail, notNullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildConfirmOpenShipmentResponse(ConfirmOpenShipmentReply))} and
     * CompleteShipmentDetail object is received.
     * 
     * @throws IOException
     */
    @Test
    public void testBuildConfirmOpenShipmentResponseSuccessResponse()
            throws IOException {

        CompleteShipmentDetail actualResponse =
                mapper.buildConfirmOpenShipmentResponse(createSuccessConfirmOpenShipmentReply());

        assertThat(actualResponse, notNullValue());
        assertThat(actualResponse.getMasterTrackingId(), notNullValue());
        assertThat(actualResponse.getCarrierCode(), notNullValue());
        assertThat(actualResponse.getCompletedPackageDetails(), notNullValue());
        assertThat(actualResponse.getOperationalDetail(), notNullValue());
        assertThat(actualResponse.getPackagingDescription(), notNullValue());
        assertThat(actualResponse.getServiceDescription(), notNullValue());
        assertThat(actualResponse.getSpecialServiceDescriptions(), notNullValue());
        assertThat(actualResponse.getShipmentDocuments(), notNullValue());
        assertThat(actualResponse.getPackageCount(), equalTo(1));
    }

    /**
     * Method to provide mock success response of CSHP confirmOpenShipment.
     * 
     * @return ConfirmOpenShipmentReply
     * @throws IOException
     */
    private ConfirmOpenShipmentReply createSuccessConfirmOpenShipmentReply()
            throws IOException {

        String responseJson =
                "{\"highestSeverity\": \"SUCCESS\",\"notifications\": [{\"severity\": \"SUCCESS\",\"source\": \"ship\",\"code\": \"0000\",\"message\": \"Success\",\"localizedMessage\": \"Success\"}],\"transactionDetail\": {\"customerTransactionId\": \"SSFE-GVTKK-12345671596090460000\",\"localization\": {\"languageCode\": \"en\"}},\"version\": {\"serviceId\": \"ship\",\"major\": \"17\",\"intermediate\": \"0\",\"minor\": \"0\"},\"jobId\": \"65s01692j25ade28713299599\",\"asynchronousProcessingResults\": {\"Options\": \"SYNCHRONOUSLY_PROCESSED\"},\"completedShipmentDetail\": {\"usDomestic\": \"true\",\"carrierCode\": \"FDXE\",\"masterTrackingId\": {\"trackingIdType\": \"FEDEX\",\"formId\": \"0201\",\"trackingNumber\": \"794978918115\"},\"serviceDescription\": {\"serviceType\": \"PRIORITY_OVERNIGHT\",\"code\": \"01\",\"names\": [{\"type\": \"long\",\"encoding\": \"utf-8\",\"value\": \"FedEx Priority OvernightÂ®\"},{\"type\": \"long\",\"encoding\": \"ascii\",\"value\": \"FedEx Priority Overnight\"},{\"type\": \"medium\",\"encoding\": \"utf-8\",\"value\": \"FedEx Priority OvernightÂ®\"},{\"type\": \"medium\",\"encoding\": \"ascii\",\"value\": \"FedEx Priority Overnight\"},{\"type\": \"short\",\"encoding\": \"utf-8\",\"value\": \"P-1\"},{\"type\": \"short\",\"encoding\": \"ascii\",\"value\": \"P-1\"},{\"type\": \"abbrv\",\"encoding\": \"ascii\",\"value\": \"PO\"}],\"description\": \"Priority Overnight\",\"astraDescription\": \"P1\"},\"packagingDescription\": {\"packagingType\": \"YOUR_PACKAGING\",\"code\": \"01\",\"names\": [{\"type\": \"long\",\"encoding\": \"utf-8\",\"value\": \"Your Packaging\"},{\"type\": \"long\",\"encoding\": \"ascii\",\"value\": \"Your Packaging\"},{\"type\": \"medium\",\"encoding\": \"utf-8\",\"value\": \"Your Packaging\"},{\"type\": \"medium\",\"encoding\": \"ascii\",\"value\": \"Your Packaging\"},{\"type\": \"small\",\"encoding\": \"utf-8\",\"value\": \"Your Pkg\"},{\"type\": \"small\",\"encoding\": \"ascii\",\"value\": \"Your Pkg\"},{\"type\": \"short\",\"encoding\": \"utf-8\",\"value\": \"Your\"},{\"type\": \"short\",\"encoding\": \"ascii\",\"value\": \"Your\"},{\"type\": \"abbrv\",\"encoding\": \"ascii\",\"value\": \"YP\"}],\"description\": \"Customer Packaging\",\"astraDescription\": \"CST PKG\"},\"specialServiceDescriptions\": [{\"identifier\": {\"id\": \"EP1000000060\",\"type\": \"DELIVER_WEEKDAY\",\"code\": \"02\"},\"names\": [{\"type\": \"long\",\"encoding\": \"utf-8\",\"value\": \"Deliver Weekday\"},{\"type\": \"long\",\"encoding\": \"ascii\",\"value\": \"Deliver Weekday\"},{\"type\": \"medium\",\"encoding\": \"utf-8\",\"value\": \"Deliver Weekday\"},{\"type\": \"medium\",\"encoding\": \"ascii\",\"value\": \"Deliver Weekday\"},{\"type\": \"short\",\"encoding\": \"utf-8\",\"value\": \"WDY\"},{\"type\": \"short\",\"encoding\": \"ascii\",\"value\": \"WDY\"}]}],\"operationalDetail\": {\"ursaPrefixCode\": \"NA\",\"ursaSuffixCode\": \"ODMA\",\"originLocationId\": \"TRLA\",\"originLocationNumber\": \"0\",\"originServiceArea\": \"A2\",\"destinationLocationId\": \"ODMA\",\"destinationLocationNumber\": \"0\",\"destinationServiceArea\": \"A1\",\"destinationLocationStateOrProvinceCode\": \"MD\",\"deliveryDate\": \"2020-07-31\",\"deliveryDay\": \"FRI\",\"commitDate\": \"2020-07-31\",\"commitDay\": \"FRI\",\"ineligibleForMoneyBackGuarantee\": \"false\",\"astraPlannedServiceLevel\": \"FRI - 31 JUL 10:30A\",\"astraDescription\": \"PRIORITY OVERNIGHT\",\"postalCode\": \"21212\",\"stateOrProvinceCode\": \"MD\",\"countryCode\": \"US\",\"airportId\": \"BWI\",\"serviceCode\": \"01\",\"packagingCode\": \"01\"},\"completedPackageDetails\": [{\"sequenceNumber\": \"1\",\"trackingIds\": [{\"trackingIdType\": \"FEDEX\",\"formId\": \"0201\",\"trackingNumber\": \"794978918115\"}],\"groupNumber\": \"0\",\"operationalDetail\": {\"operationalInstructions\": [{\"number\": 1,\"content\": \"TRK#\"},{\"number\": 1,\"content\": \"0201\"},{\"number\": 1,\"content\": \"NA ODMA\"},{\"number\": 1,\"content\": \"1001891724510002121200794978918115\"},{\"number\": 1,\"content\": \"56BJ2/8DAF/B766\"},{\"number\": 1,\"content\": \"7949 7891 8115\"},{\"number\": 1,\"content\": \"FRI - 31 JUL 10:30A\"},{\"number\": 1,\"content\": \"PRIORITY OVERNIGHT\"},{\"number\": 1,\"content\": \"21212\"},{\"number\": 1,\"content\": \"MD-US\"},{\"number\": 1,\"content\": \"BWI\"}],\"barcodes\": {\"binaryBarcodes\": [{\"type\": \"COMMON_2D\",\"value\": \"Wyk+HjAxHTAyMjEyMTIdODQwHTAxHTc5NDk3ODkxODExNTAyMDEdRkRFHTAzMzYwMDAwMx0yMTIdHTEvMR0xMC4wMExCHU4dbm9pZGEdQmFsdGltb3JlHU1EHWluZm9nYWluHjA2HTEwWkVEMDA4HTEyWjY3NjU0NTQzNzYdMTVaMDA2OTkwMTAwHTIwWhwxMDAdMzFaMTAwMTg5MTcyNDUxMDAwMjEyMTIwMDc5NDk3ODkxODExNR0zMlowMh0zNFowMR0zOVpUUkxBHR4wOR1GRFgdeh04HRgtGi8CAn9AHgQ=\"}],\"stringBarcodes\": [{\"type\": \"FEDEX_1D\",\"value\": \"1001891724510002121200794978918115\"}]}},\"label\": {\"type\": \"OUTBOUND_LABEL\",\"shippingDocumentDisposition\": \"RETURNED\",\"imageType\": \"PDF\",\"resolution\": \"200\",\"copiesToPrint\": \"1\",\"parts\": [{\"documentPartSequenceNumber\": \"1\",\"image\": \"JVBERi0xLjQKMSAwIG9iago8PAovVHlwZSAvQ2F0YWxvZwovUGFnZXMgMyAwIFIKPj4KZW5kb2JqCjIgMCBvYmoKPDwKL1R5cGUgL091dGxpbmVzCi9Db3VudCAwCj4+CmVuZG9iagozIDAgb2JqCjw8Ci9UeXBlIC9QYWdlcwovQ291bnQgMQovS2lkcyBbMTggMCBSXQo+PgplbmRvYmoKNCAwIG9iagpbL1BERiAvVGV4dCAvSW1hZ2VCIC9JbWFnZUMgL0ltYWdlSV0KZW5kb2JqCjUgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUxCi9CYXNlRm9udCAvSGVsdmV0aWNhCi9FbmNvZGluZyAvTWFjUm9tYW5FbmNvZGluZwo+PgplbmRvYmoKNiAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9IZWx2ZXRpY2EtQm9sZAovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjcgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUxCi9CYXNlRm9udCAvSGVsdmV0aWNhLU9ibGlxdWUKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iago4IDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0hlbHZldGljYS1Cb2xkT2JsaXF1ZQovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjkgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUxCi9CYXNlRm9udCAvQ291cmllcgovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjEwIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0NvdXJpZXItQm9sZAovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjExIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0NvdXJpZXItT2JsaXF1ZQovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjEyIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL0NvdXJpZXItQm9sZE9ibGlxdWUKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iagoxMyAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9UaW1lcy1Sb21hbgovRW5jb2RpbmcgL01hY1JvbWFuRW5jb2RpbmcKPj4KZW5kb2JqCjE0IDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMQovQmFzZUZvbnQgL1RpbWVzLUJvbGQKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iagoxNSAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9UaW1lcy1JdGFsaWMKL0VuY29kaW5nIC9NYWNSb21hbkVuY29kaW5nCj4+CmVuZG9iagoxNiAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTEKL0Jhc2VGb250IC9UaW1lcy1Cb2xkSXRhbGljCi9FbmNvZGluZyAvTWFjUm9tYW5FbmNvZGluZwo+PgplbmRvYmoKMTcgMCBvYmogCjw8Ci9DcmVhdGlvbkRhdGUgKEQ6MjAwMykKL1Byb2R1Y2VyIChGZWRFeCBTZXJ2aWNlcykKL1RpdGxlIChGZWRFeCBTaGlwcGluZyBMYWJlbCkNL0NyZWF0b3IgKEZlZEV4IEN1c3RvbWVyIEF1dG9tYXRpb24pDS9BdXRob3IgKENMUyBWZXJzaW9uIDUxMjAxMzUpCj4+CmVuZG9iagoxOCAwIG9iago8PAovVHlwZSAvUGFnZQ0vUGFyZW50IDMgMCBSCi9SZXNvdXJjZXMgPDwgL1Byb2NTZXQgNCAwIFIgCiAvRm9udCA8PCAvRjEgNSAwIFIgCi9GMiA2IDAgUiAKL0YzIDcgMCBSIAovRjQgOCAwIFIgCi9GNSA5IDAgUiAKL0Y2IDEwIDAgUiAKL0Y3IDExIDAgUiAKL0Y4IDEyIDAgUiAKL0Y5IDEzIDAgUiAKL0YxMCAxNCAwIFIgCi9GMTEgMTUgMCBSIAovRjEyIDE2IDAgUiAKID4+Ci9YT2JqZWN0IDw8IC9GZWRFeEV4cHJlc3MgMjAgMCBSCi9FeHByZXNzRSAyMSAwIFIKL2JhcmNvZGUwIDIyIDAgUgo+Pgo+PgovTWVkaWFCb3ggWzAgMCA3OTIgNjEyXQovVHJpbUJveFswIDAgNzkyIDYxMl0KL0NvbnRlbnRzIDE5IDAgUgovUm90YXRlIDkwPj4KZW5kb2JqCjE5IDAgb2JqCjw8IC9MZW5ndGggMjk4NwovRmlsdGVyIFsvQVNDSUk4NURlY29kZSAvRmxhdGVEZWNvZGVdIAo+PgpzdHJlYW0KR2F0VTY5b2c6JyZcU15KczNPImAxKF5pYSg5Pk8xZ1RHMGkqXyJkX0ciNl1Xa2BYVEArYywmRTdNNmxaNSonNyMlIiRXSTtmV2cyS1JVaGoKPFE0ZVtRaSdEImkzQytVPUdkMkIpOXEpaypET0lmJzdwWzByRiMsPHEiVDtaWl86dWotXyZgTyhdW1ojQ2tCLG8vWz85YWowOy4uIVhFYGUKP10nQl86VjwzSEdCO0QhXlU2RUslWSo0OSpZLzUpJSFLRmluOiwoVVFIO1tuJkE9LGUwVzExayYiRFBCXChCQjghTFA/aTJVPEhlRGcsO2sKXixjKz5aVEFrKVtuLGdaQDg1MVQtXGk1T0UrRCpvcjhEakAkPl1KdGo8Um1LcUBIL15BNVckZFEiPXBHXEBnXVFDJmM/UGE3Jl8/TEArWUIKRztZSi1xPWBAby1OL2xpOCZqazUlbVJdZD5YPVkmUixRVD9mQz0sTG5ubi0pKzJSWFdyc2VrOzNFb1RuWlRdLV8wQyVKZ2lXbnBKcDNQQkkKPzRIM0dTanBoK0xBYlRTLF49Qm5CUmZkVGY1ZUFHYS4pckkkNi9xUyk9Jjg0WzVHcEhwWj9FZFpLVC9DRj9bPEQ3IStQOCdNZmQvKHBGWkAKZFAhaSc2IVQzSFQnZlA4UlBBRDMsMj5wXlItU0YoKEJwRGlldC5CMj11YFVDLkNeOGNeJWY0Y2RKXidSXlNcUl8zQD8mdEUhMj4jXURoY0gKcmwra1NmUGRyJyYkTix0JHUkbGEsWUs+dEdsaFIlcD9qQVFwdFtsUlY5Ym1gTlxGV0ptTW5bKV1DIyNeTzFXJGttYnQ0NnBeOGpxX1hfZEMKaDsuUC9mM2NTV0laayZVYTVvODNZN0NIXXBDRm1uJiROSHBaZC1Sb0BXWCEsSXNcWGhtJ2FnbCFLJDhiR2wsIUcjQUoyZSkudTVgXyR0NSwKRmw8NkNNcGhfMGdKS15uakBEZ2EuRWJrRGM3LTZzMDxYTDZtMGVgL3ErTnM5LFhCWlJtIlR1ajlER1Z1WVx1R3VcRUMiRmRoUEApNk5UbG4KZT5gSyg7W0dITjRbOE5QakkjXDFMNHVfMTIoVjgyZ2g0Y3RjY28tb0dgKyRZMkQ1dFxUKExMY0I4TTwiKC4kQjhhYD5OWzpkXCFvYCxiSXQKNGNlQGBnWkxMXWpNSzlWK0hWNyMlazsmcEE8akg8JzQiRzxrVixoOiw0cURYZ2g4QE1rLmFcTTQ7Q284UGdebHFaRkVPNm5BP0deRXRcO2gKUEtpLU9OUC5fYytxdWhmciMsY19MNFxBP2k8ISRuZFQiW0hnK2I4JWE8OzVmQk04OmhOWSIrUmliUDNVREhVRHFkV2NXNFdeW0xQNTE7QzkKXSIzSTRPRy0uKTZRcURScS0nX0wlT1RMRzckP2FrWz9XLE4jJFAoZGcrZDFPMXJAbT9oM2tGIjNwPEJQTC9aJyFJOl10b2tvZUdwW2JFWkQKIjBMJCRqaUJuVllUZ0UsMUwjS1JPTkgiQzZeb0pJPDRnak4yO0xeWCQvJ3M1WihvTEU7SUczWEQ3LURQZlhkRE9fbzNGbilGYTY0PnRyc1oKbFMzIjJOZylgZzlYTFVFYGNoYGtTJ047M2pqLG4mNFBNXmhTYlpIajZUdG5AOlBPI0tIJ2hYQy0+XW1yJFkkTTpPdXBaRkVFWnE9ImZ0KCYKYSg8dElfMyFVJlJdc1ZTI1whSFk3MT4sbDFmPXVGPUdJP0I3W2xLcVhJJ0dzTGIpcFMyQydgQ0pWclNFSigsZl1URlFcJ2guPS1SND0xc1wKZzU8KVFIM2pLNVpPZjxWb2UrZXNbVEYmSyo0JFlWNFIvO1hvcDJzZSM5aCJPYWBUJTlvdGpWSThXMzZROjJMIlwxNVcmVXBfKWc2JzpXMkEKSy9PJFc+QCNyZFc1RG1ELUk2SD0icFhJPF10Wi9NLEA7a2ZkSipsM05OTiIvVG9TbVJZMmcwT0IsSVtKRGc6RFtZbXVIaUpmb0tfI0czPU8KWHBzI20qPUFcUzAvI1pqcEknLl1CcVByaSxFOC11MVpXL3RTcEViLVhAJ109TWNbLC06Pyw7QkdDYWp0V0BSLTlOLzckX1JdOFNTXmo2OV8KJ1BBW04wa2NcYl4uVy1mR09iOlFJJ1EuW09YZmc4VSIncHApKEdJJ2w1XUBKZjUtZE1PKVIvIm9HKSciMFgvJ1VtTGlcXipJXnVtRGg0TkwKVUtmKzIiZnQnVGJHMzc9aS9xXE9QXFMmPj5QRFxbKGFVaEU5U2BbYCIxRDxlYjBIJmpvMWkwczJIWWFXQTRqZiMkTllcPkNsO04hTENmM2QKO1A7OEJJUSJfZ0AtXUY5VGVtTUstMl9wNWheLWNcNWE6Vl88I1VRSDFLMUBhZk9AIXNJYWZXJEZ0Ym1RWG5hVEMlZUtLbV1IVCcrOmxdRT8KP1w8SjRaNUkuMSciVTFcb2xTXE1sYXQhSVM0ZVVYMWBMYFtVZG02bm1YVztGOnA8W10uUDkzYERXOGZUKWotMTBiYFo8OEZfaVlQb2sxL1kKKEAlUCNbXlxcOE8jYDI3Ok11ZzhsTko9c3JlUDVLYDMsOzwiKk42S1JhT2hzNyQ9Z11cN2hORkFQb0c1ZCMuO01XU09fLkhESyNxSUhsMyUKVzc8Y10tLkg/YjwsLC5yJ24yXFhUU2ZuJzNnPFZzTSpvXFJiUW1BSldwMChPPXUnX084amRIZWhaNGkmazMicD06LktCNyYsWT49ISZvQiYKcD0zUXEncSpgQEMtTyhYTiRJTlRrJGBjPllXciYyXEdxJFBFNDo+ZWg0XWBfcVFjYCZmaWhCSjJfTSNfS2FTKWZAWG8rK1dJZU1xVWJtI1oKbCw3cD46TkohOWR0LV1xPEhiI1gnVUE2bjYpVHEnaVV1b3RyITQ/K0A/RHAyOG5sKCpkaCModTQ1S2g9VC0mY01uJTRJUmRmSyFwb2wuWm8KSUNdYV1mNElRcDxGLHQ4ZGJ1SXA3P0YvXk5GKSx0L1tKJChQV3NXNGZBSFY4cW0/N21jXUBPRXEuODRYXV9RJ3MsK2M9ODU+SmFdZD9OY1QKRFs3PyY3JjphIjsrV1RDOWU8bWk6JFRob2svaGhnVzQmNEEuLVZGblx1QzQ/UFhQKitHcWgkYVQiazlkaixaRylvXEw6UmFbRWRocUo9L2wKPSwvZTQsSiYuXWhaZ2JmXClvOyNLSjo2V3MiZGcjUT5tS2AlOztDczZxQC9AWkAlbiY6Ki49bChzRChtcj09Zl8zZ2M+cGNjajNeOkYldWwKbVc3OyNROGhvXSVDL1RMbG1cI1Q9W2Z1cGZiVTY8REU2N0M0LDNbJ1h0QHRBL1FDK0wpVT9DSFMjXD5MJGBzY1ZbWXQ+Kz1ALSFbZl5CTDIKJyIyTzsiQFtTZFIkXDFzWkVGTk1ZXVU4X0ZHcSVsJ1tAXDYhRic2XkhxOi0yN21eV1AtczljNFt0KFVHLyUtRyM/c1xbPDxEa2ojIWpudTAKcSEhVWYpUGhfO1EkPFxgUl5QYTMzTzskXC5vLEVEPUZhSCpSYCJiUj43N1E6MCJCPlhnMnVzcyxsSW5iT190MidWclFlOEJHb2A4XyxXTCcKIUV1Qzo6P1RyRi9bVC0kMFYpQkxAM201KidqRCdKJDVdVT0kUUVQOTh1RzprIl9hSVA6T2RuZ2BjM2k8ZzUsdUhTU2dJalotJyR0NlNFZWAKQ1M8VzdUaV1HcWJUU09qOTpfXmtUSlxYKCZwRUw+S0pjYFZnKjNTVEMrJytDN0tcbzBTYFFFcT4lakhaOj9tJ1coMUMxcUQjbWhoMXUvRHUKRD9hVUJUUFhMX0RAJ05BOiYtdGRJZWNlUFtiLlVvZXE/cDM7Iy9VVlRORGw5ZlhmKU8qc2VGUDtYcS5zYFY0anBycCRvfj4KZW5kc3RyZWFtCmVuZG9iagoyMCAwIG9iago8PCAvVHlwZSAvWE9iamVjdAovU3VidHlwZSAvSW1hZ2UKL1dpZHRoIDExOAovSGVpZ2h0IDQ5Ci9Db2xvclNwYWNlIC9EZXZpY2VHcmF5Ci9CaXRzUGVyQ29tcG9uZW50IDgKL0xlbmd0aCA0NjEKL0ZpbHRlciBbL0FTQ0lJODVEZWNvZGUgL0ZsYXRlRGVjb2RlXQo+PnN0cmVhbQpHYiIvZUpJXVI/I1huWGtUNkE+WEtuRGJXQiJNWkVhalBwZDRdJyheQFJsdCJrQiJTMCRmJT89bT00Qzs7LiROKGpSY1lSJXNkJmw5JE90XgopJjc9TCg3LWVxcWVALigvU0M5ODQtZkxYZUxaNXFjWjVsQUVCNTU+M0AwPWsiO1JoMmhCY1FMMyZKPzVvW2NIMGJTRk5ePDs7QDQxU1k5bQpjIlpJPVlsI2M0KWBfIllsbU4zJS5BLmxOU2BWZS9ZPCU/LjcoKCkvLmRCWztrIUpbJSclREpcST8rYTc6NW9Oa1lccUVTPjM8STViWm0oSQolZDxAQik4RGJ0M3BLR2NsYlFrMSdpWlZzN05NMFxvJlIqcVU+Z0h1LUZyQ2toPGtIRkcqXCRNPV9zVi8tJFxKRjplLz8+ZlhBK0NDbXBEZQpkU1A2R2N1LFxYJ0FMPWE1SkNbNDZHLmhwMj1VNGVeJGVvXFEjYiptN0M4JC1KQShjWj9ZYCdDOzZKcEhlUGxcJEhFQyF1XipRYSs2Z0VNawpbZkVWKmIuIz5wcj4tV1NmIz5oJlc/dWtrVHNoWWA9L1guNjZBKjtiaXMvZGdAUVhYc3I4bTt+PgplbmRzdHJlYW0KZW5kb2JqCjIxIDAgb2JqCjw8IC9UeXBlIC9YT2JqZWN0Ci9TdWJ0eXBlIC9JbWFnZQovV2lkdGggNTQKL0hlaWdodCA1NAovQ29sb3JTcGFjZSAvRGV2aWNlR3JheQovQml0c1BlckNvbXBvbmVudCA4Ci9MZW5ndGggNzcKL0ZpbHRlciBbL0FTQ0lJODVEZWNvZGUgL0ZsYXRlRGVjb2RlXQo+PnN0cmVhbQpHYiIwSmQwVGRxJGo0b0ZeVSwiSFRzOUVJRTswQVQsX0UqTFolb0A3Smw1VjtIJ0NzPVRycURhSC40QmYjYzRPVlQ7KGQjZjxHRTl+PgplbmRzdHJlYW0KZW5kb2JqCjIyIDAgb2JqCjw8IC9UeXBlIC9YT2JqZWN0Ci9TdWJ0eXBlIC9JbWFnZQovV2lkdGggMjA5Ci9IZWlnaHQgNjYKL0NvbG9yU3BhY2UgL0RldmljZUdyYXkKL0JpdHNQZXJDb21wb25lbnQgOAovTGVuZ3RoIDEyNjEKL0ZpbHRlciBbL0FTQ0lJODVEZWNvZGUgL0ZsYXRlRGVjb2RlXQo+PnN0cmVhbQpHYiIvY2dRRUxBI1hlRmZULWZoR2xVXEIxPFIqdSZZY0xkbk5yJmtqRi5pRW5vQ01lKFQvclRuclZPOUkqa2ldQU06XyVFRikrXV0/VFtCLApBJDkzKTMmYExeTHAtR1dDSUxAYmI5c0Nua3BeKHUrb1BBKjAtLDBNVkdIV1sjYCFIYFgsTDc0PkZnIllVO1AlRVM3dWNKVSEkV2xXPy5LaAplMjwtIS0tTiZNLiFOOG8ib0w0WjBuLCEiPW5XY15EWVs7VS1SVXJiRT9CVTFwVHA5Uk4+dG07KCFJP2oqZ1BKcWE9aGkyM0kzJCtAdV5PLQpEOzNOOUNSIlVMTVhubUE6ZGpUXmRoJVs2PVFMT11QTUAxKmVLJlgtJHQ2SlkzcmQkKGFYXlwpQl11Uig0KE49YUdWYjBGMzw2RzQsS29wNApOSXVbNjdtYyQoNlRQPmcsYGRdWmUhMD5BaU8yI2pfTiRhMT84T2ZVJCI6RlQlMlJhYjklPzpXTi9jKyReaDYuR3JUQWhVVDRTKSNiRCQsMgpdblRALzNkTzNRLFdXYnRhJSIkOnBPMms1Q1AuVixtQ1pvaDwxbTNzZD8qN2wlMmdvLU1aWUxFXjkwS2NkLD4pXlFtODBySTNULSNAcXVQTAomKklCJS9pNkNBJGQnNWZGLU8lUThaMkE9YkhrO1IjX3VaI1ciQmFSaDZObkpuQytzZW91Ilg2NlloKDY9MDQ/YjtgLUBVOERlKCRiYjlMZApPcyFtczNdMC4/VGE0JDA5alcwOzRPJ1ZKJzhtPUlOWV9dWWMrQ2tmZGJFRVkvUGFFMFZnbzNQWVJrK0NEISt1OjgyKG80SzlJbDlgNlE/bQpcXkJcUS8kMS8wL0hcRVFiPDtpJGFpXUtLOSs/a1dMbSREQzxBSilDM0ZhYi0sbWtnKmAxSUQ7WzJtUF5UXUdEIkw+OEJrNiZWNGo7V2BjNgpOUlpLJ24vKiojREVyJiVXZi1SI19RR2EhJystUiIxNFYsWyFLSmtvQjoxaShbdCZNckohL29YTUU/Lm9qbml1WytEYlwiYWJbcWAzNCZzcwpQNk9DL09NOkphNVpEQikoJUNbPGthTkgpMjdyWlVWJVkyUkQ5Sy9wZUVsUF9sNjNqQjJScTYsKiFhKGIjciFMLlVmYHRoYCg5NFFAQEI0OwpGVyxqTT9eT1tkOHFpWU9MOkJvLEYyXUpuQC1jXSddTiZGR1RvKk4qa111ZElTSGIqcSx0TGNSPiRmRm0kal45RmVRKkR0KSZGIXRGRU9pMwpWPz1dTmQsUTVmUmF0Mj5oN2RgJmoxP2E0JUMpL0A7aFtQLzRjcmJgaV5RIjxCc0opRURPLkxIQ2AndSJUUW46WSgnbTEvZCpDMU5nUlgncQo7TW8/U1UpQE1YPWIvJEVBM2FeXkMpI1drTzp0KmpYaTBGdUdJbyNtVHNmP2ZEL1EwL0JBOD5QbD9lT0o3b1FhRUV1aFBnU2VGKGZCXiErXwomMHEqLl1LOCRUXFMnKkpjQyM7VTFqJS8sOE0hUENkO0NXJXFbQy5FSVE3YSQ4WEhqakRTXzxkTDBocDk1LEMjWjw3Rz1NRTRzaUxtLS9VOgptVHM2dWVcUnBzNFo7JlohVEEuSVJBSWcsR1Vrak1pM3NSIkF0TkYpSWhISn4+CmVuZHN0cmVhbQplbmRvYmoKeHJlZgowIDIzCjAwMDAwMDAwMDAgNjU1MzUgZiAKMDAwMDAwMDAwOSAwMDAwMCBuIAowMDAwMDAwMDU4IDAwMDAwIG4gCjAwMDAwMDAxMDQgMDAwMDAgbiAKMDAwMDAwMDE2MiAwMDAwMCBuIAowMDAwMDAwMjE0IDAwMDAwIG4gCjAwMDAwMDAzMTIgMDAwMDAgbiAKMDAwMDAwMDQxNSAwMDAwMCBuIAowMDAwMDAwNTIxIDAwMDAwIG4gCjAwMDAwMDA2MzEgMDAwMDAgbiAKMDAwMDAwMDcyNyAwMDAwMCBuIAowMDAwMDAwODI5IDAwMDAwIG4gCjAwMDAwMDA5MzQgMDAwMDAgbiAKMDAwMDAwMTA0MyAwMDAwMCBuIAowMDAwMDAxMTQ0IDAwMDAwIG4gCjAwMDAwMDEyNDQgMDAwMDAgbiAKMDAwMDAwMTM0NiAwMDAwMCBuIAowMDAwMDAxNDUyIDAwMDAwIG4gCjAwMDAwMDE2MjIgMDAwMDAgbiAKMDAwMDAwMjAwMiAwMDAwMCBuIAowMDAwMDA1MDgxIDAwMDAwIG4gCjAwMDAwMDU3MjggMDAwMDAgbiAKMDAwMDAwNTk4OSAwMDAwMCBuIAp0cmFpbGVyCjw8Ci9JbmZvIDE3IDAgUgovU2l6ZSAyMwovUm9vdCAxIDAgUgo+PgpzdGFydHhyZWYKNzQzNwolJUVPRgo=\"}]},\"signatureOption\": \"SERVICE_DEFAULT\"}]}"
                        + "}";

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper.readValue(responseJson, ConfirmOpenShipmentReply.class);
    }


    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildConfirmOpenShipmentResponse(ConfirmOpenShipmentReply))} and
     * CompleteShipmentDetail object is received but it fails to decode the image buffer
     * 
     * @throws IOException
     */
    @Test
    public void testBuildConfirmOpenShipmentResponseImageDecodeFailure()
            throws IOException {

        when(util.decodePackageDocument(any(ShippingDocument.class), eq(ShippingDocumentType.OUTBOUND_LABEL)))
                .thenThrow(new ClientTransientException(ServiceErrorCode.INTERNAL_SERVER_ERROR));
        expectedEx.expect(ClientTransientException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.INTERNAL_SERVER_ERROR.getMessage()));
        mapper.buildConfirmOpenShipmentResponse(createSuccessConfirmOpenShipmentReply());
    }
    
    /**
     * test the package count when service returns empty list of CompletedPackageDetail in response.
     * @throws IOException
     */
    @Test
    public void testPackageCountWhenEmptyPackageDetails() throws IOException {
		ConfirmOpenShipmentReply reply = createSuccessConfirmOpenShipmentReply();
		CompletedPackageDetail[] packageDetails = new CompletedPackageDetail[0];
		reply.getCompletedShipmentDetail().setCompletedPackageDetails(packageDetails);
		ShipmentOperationalDetail shipmentOperationalDetail = new ShipmentOperationalDetail();
		shipmentOperationalDetail.setAstraPlannedServiceLevel("Astra Level");
		AssociatedShipmentDetail associatedShipmentDetail = new AssociatedShipmentDetail();
		associatedShipmentDetail.setShipmentOperationalDetail(shipmentOperationalDetail);
		AssociatedShipmentDetail[] associatedShipment = { associatedShipmentDetail };
		reply.getCompletedShipmentDetail().setAssociatedShipments(associatedShipment);
		CompleteShipmentDetail actualResponse = mapper.buildConfirmOpenShipmentResponse(reply);

		assertThat(actualResponse, notNullValue());
		assertThat(actualResponse.getPackageCount(), equalTo(0));
    }
    
    /**
     * test the package count when service returns null CompletedPackageDetail in response.
     * @throws IOException
     */
    @Test
    public void testPackageCountWhenNullPackageDetails() throws IOException {
        ConfirmOpenShipmentReply reply = createSuccessConfirmOpenShipmentReply();
        reply.getCompletedShipmentDetail().setCompletedPackageDetails(null);
        CompleteShipmentDetail actualResponse =
                mapper.buildConfirmOpenShipmentResponse(reply);

        assertThat(actualResponse, notNullValue());
        assertThat(actualResponse.getPackageCount(), equalTo(0));
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildDeleteShipmentResponse(ShipmentReply)} and empty reply
     * is received and it's handled
     */
    @Test
    public void testBuildDeleteShipmentResponseNull() {

        ShipmentReply shipmentReply = null;

        expectedEx.expect(ClientTransientException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getMessage()));
        mapper.buildDeleteShipmentResponse(shipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildDeleteShipmentResponse(ShipmentReply))} and null
     * severity is returned and it's handled
     */
    @Test
    public void testBuildDeleteShipmentResponseReplyNullSeverity() {

        ShipmentReply shipmentReply = new ShipmentReply();
        shipmentReply.setHighestSeverity(null);

        expectedEx.expect(ClientTransientException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getMessage()));
        mapper.buildDeleteShipmentResponse(shipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildDeleteShipmentResponse(ShipmentReply))} and failure
     * severity is returned and it's handled
     */
    @Test
    public void testBuildDeleteShipmentResponseFailureSeverity() {

        ShipmentReply shipmentReply = new ShipmentReply();
        shipmentReply.setHighestSeverity(Severity.FAILURE.name());
        Notification notification = new Notification();
        notification.setCode("702");
        notification.setMessage("Unable to process request. Please try again later.");

        shipmentReply.setNotifications(new Notification[] {notification});

        expectedEx.expect(ClientTerminalException.class);
        expectedEx.expectMessage(equalTo(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getMessage()));
        mapper.buildDeleteShipmentResponse(shipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildDeleteShipmentResponse(ShipmentReply))} and failure
     * severity is returned and it's handled
     */
    @Test
    public void testBuildDeleteShipmentResponseFailureSeverityAndEmptyNotification() {

        ShipmentReply shipmentReply = new ShipmentReply();
        shipmentReply.setHighestSeverity(Severity.FAILURE.name());

        String actual = mapper.buildDeleteShipmentResponse(shipmentReply);
        assertThat(actual, nullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildDeleteShipmentResponse(ShipmentReply))} and error
     * severity is returned and it's handled
     */
    @Test
    public void testbuildDeleteShipmentResponseErrorSeverity() {

        String errorMessage = "Application ID is null or empty.";

        ShipmentReply shipmentReply = new ShipmentReply();
        shipmentReply.setHighestSeverity(Severity.ERROR.name());

        Notification notification = new Notification();
        notification.setCode("512");
        notification.setMessage(errorMessage);

        shipmentReply.setNotifications(new Notification[] {notification});

        expectedEx.expect(ClientTerminalException.class);
        expectedEx.expectMessage(equalTo(errorMessage));
        mapper.buildDeleteShipmentResponse(shipmentReply);
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildDeleteShipmentResponse(ShipmentReply))} and error
     * severity is returned and it's handled
     */
    @Test
    public void testbuildDeleteShipmentResponseErrorSeverityAndEmptyNotification() {

        ShipmentReply shipmentReply = new ShipmentReply();
        shipmentReply.setHighestSeverity(Severity.ERROR.name());
        String actualResponse = mapper.buildDeleteShipmentResponse(shipmentReply);

        assertThat(actualResponse, nullValue());
    }

    /**
     * Test case to verify that when request is made to
     * {@link ShipServiceResponseMapper#buildDeleteShipmentResponse(ShipmentReply))} and success
     * reply is received.
     */
    @Test
    public void testbuildDeleteShipmentResponseSuccess() {

        ShipmentReply shipmentReply = new ShipmentReply();
        shipmentReply.setHighestSeverity(Severity.SUCCESS.name());
        String shipmentDetail = mapper.buildDeleteShipmentResponse(shipmentReply);

        assertThat(shipmentDetail, notNullValue());
    }

}
