package com.fedex.rscs.validator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.fedex.rscs.dto.Address;
import com.fedex.rscs.dto.AddressClassification;
import com.fedex.rscs.dto.HomeDeliveryType;
import com.fedex.rscs.dto.HomeDeliveryDetail;
import com.fedex.rscs.dto.Party;
import com.fedex.rscs.dto.PhoneNumber;
import com.fedex.rscs.dto.PhoneNumberDetail;
import com.fedex.rscs.dto.RequestedShipment;
import com.fedex.rscs.dto.ShipmentSpecialServiceDescription;
import com.fedex.rscs.dto.ShippingContact;
import com.fedex.rscs.dto.SpecialServiceDescription;
import com.fedex.rscs.exception.ServiceInputValidationException;
import com.fedex.rscs.model.ServiceConstant;

import static org.hamcrest.CoreMatchers.equalTo;

/**
 * This class contains test cases for RetailShipmentCreationInputValidator
 * 
 * @author 3932968
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class RetailShippingDataValidatorTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    private String fieldName;
    private String sectionName = ServiceConstant.ADDRESS;

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationInputValidator.checkBlank(String value, String fieldName, String
     * sectionName)} and value is not provided then error is occurred and handled accordingly
     */
    @Test
    public void testCheckBlankValueNull() {

        fieldName = ServiceConstant.CITY;
        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo("The " + fieldName + " is missing."));

        RetailShippingDataValidator.checkBlank(null, fieldName, sectionName);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationInputValidator.checkInvalidLength(String value, String
     * fieldName, String sectionName, int length)} and value is exceeding maximum length then error
     * is occurred and handled accordingly
     */
    @Test
    public void testValueInvalidLength() {

        String value = "House number 1234, Gill Colony, Baltana Zirakpur";
        fieldName = ServiceConstant.STREETLINES;
        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo("The length of " + fieldName + " is invalid."));

        RetailShippingDataValidator.checkInvalidLength(value, fieldName, sectionName, 35);
    }

    /**
     * Test case to verify that when request is made to {@link RetailShipmentCreationInputValidator.
     * checkEmpty(List<T> value, String fieldName, String sectionName)} and value is an empty list
     * then error is occurred and handled accordingly
     */
    @Test
    public void testValueCheckEmpty() {

        List<String> value = new ArrayList<>();
        fieldName = ServiceConstant.STREETLINES;
        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo("The " + fieldName + " is missing."));

        RetailShippingDataValidator.checkEmpty(value, fieldName, sectionName);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationInputValidator.checkBlank(String value, String fieldName, String
     * sectionName)} and value is provided then successful response is handled accordingly
     */
    @Test
    public void testCheckBlankValueSuccess() {

        String value = "Dallas";
        fieldName = ServiceConstant.CITY;

        RetailShippingDataValidator.checkBlank(value, fieldName, sectionName);
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationInputValidator.checkInvalidLength(String value, String
     * fieldName, String sectionName, int length)} and value is not exceeding maximum length then
     * successful response handled accordingly
     */
    @Test
    public void testValueInvalidLengthSuccess() {

        String value = "House number 1234, Gill Colony";
        fieldName = ServiceConstant.STREETLINES;

        RetailShippingDataValidator.checkInvalidLength(value, fieldName, sectionName, 35);
    }

    /**
     * Test case to verify that when request is made to {@link RetailShipmentCreationInputValidator.
     * checkEmpty(List<T> value, String fieldName, String sectionName)} and value is not empty list
     * then successful response handled accordingly
     */
    @Test
    public void testValueCheckEmptySuccess() {

        List<String> value = new ArrayList<>();
        value.add("House number 1234");
        fieldName = ServiceConstant.STREETLINES;

        RetailShippingDataValidator.checkEmpty(value, fieldName, sectionName);
    }

    /**
     * Test case to verify that when request is made to {@link RetailShipmentCreationInputValidator.
     * checkObjectEmpty(Object value, String fieldName, String sectionName)} and value is not empty
     * list then successful response handled accordingly
     */
    @Test
    public void testCheckObjectEmptySuccess() {

        ShipmentSpecialServiceDescription spclSrvcRequested = createShipmentSpecialSrvcDesc();
        fieldName = ServiceConstant.HOME_DELIVERY_DETAIL;
        sectionName = ServiceConstant.SPECIAL_SRVC_REQ;

        RetailShippingDataValidator.checkObjectEmpty(spclSrvcRequested, fieldName, sectionName);
    }

    /**
     * Test case to verify that when request is made to {@link RetailShipmentCreationInputValidator.
     * checkObjectEmpty(Object value, String fieldName, String sectionName)} and value is an empty
     * list then error is occurred and handled accordingly
     */
    @Test
    public void testCheckObjectEmpty() {

        ShipmentSpecialServiceDescription spclSrvcRequested = null;
        fieldName = ServiceConstant.HOME_DELIVERY_DETAIL;
        sectionName = ServiceConstant.SPECIAL_SRVC_REQ;

        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo("The " + fieldName + " is missing."));

        RetailShippingDataValidator.checkObjectEmpty(spclSrvcRequested, fieldName, sectionName);
    }

    /**
     * Test case to verify that when request is made to {@link RetailShipmentCreationInputValidator.
     * validateDeliveryDate(String value, String fieldName, String sectionName, String dateFormat)}
     * and value is not empty list then successful response handled accordingly
     */
    @Test
    public void testValidateDeliveryDateSuccess() {

        ShipmentSpecialServiceDescription spclSrvcRequested = createShipmentSpecialSrvcDesc();
        fieldName = ServiceConstant.DELIVERY_DATE;
        sectionName = ServiceConstant.HOME_DELIVERY_DETAIL;

        RetailShippingDataValidator.validateDeliveryDate(spclSrvcRequested.getHomeDeliveryDetail()
                .getDeliveryDate(), fieldName, sectionName, ServiceConstant.DELIVERY_DATE_FORMAT);
    }

    /**
     * Test case to verify that when request is made to {@link RetailShipmentCreationInputValidator.
     * validateDeliveryDate(String value, String fieldName, String sectionName, String dateFormat)}
     * and value is an empty list then error is occurred and handled accordingly
     */
    @Test
    public void testValidateDeliveryDateFailure() {

        ShipmentSpecialServiceDescription spclSrvcRequested = createShipmentSpecialSrvcDesc();
        spclSrvcRequested.getHomeDeliveryDetail()
                .setDeliveryDate("2021-21-34");
        fieldName = ServiceConstant.DELIVERY_DATE;
        sectionName = ServiceConstant.HOME_DELIVERY_DETAIL;

        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo("The " + fieldName + " is invalid."));

        RetailShippingDataValidator.validateDeliveryDate(spclSrvcRequested.getHomeDeliveryDetail()
                .getDeliveryDate(), fieldName, sectionName, ServiceConstant.DELIVERY_DATE_FORMAT);
    }

    /**
     * Test case to verify that when request is made to {@link RetailShipmentCreationInputValidator.
     * validateHomeDeliveryPremSpecialService(requestedShipment)} and special service requested is
     * null then it's executed successfully
     */
    @Test
    public void testValidateHDSpclSrvcWdSpclSrvcReqstdNull() {

        RequestedShipment requestedShipment = createRequestedShipment();
        requestedShipment.setSpecialServicesRequested(null);

        RetailShippingDataValidator.validateSpecialService(requestedShipment.getSpecialServicesRequested());
    }

    /**
     * Test case to verify that when request is made to {@link RetailShipmentCreationInputValidator.
     * validateHomeDeliveryPremSpecialService(requestedShipment)} and special services is null then
     * it's executed successfully
     */
    @Test
    public void testValidateHDSpclSrvcWdSpclSrvcsNull() {

        RequestedShipment requestedShipment = createRequestedShipment();
        requestedShipment.getSpecialServicesRequested()
                .setSpecialServices(null);

        RetailShippingDataValidator.validateSpecialService(requestedShipment.getSpecialServicesRequested());

    }

    /**
     * Test case to verify that when request is made to {@link RetailShipmentCreationInputValidator.
     * validateHomeDeliveryPremSpecialService(requestedShipment)} and special services is null then
     * it's executed successfully
     */
    @Test
    public void testValidateHDSpclSrvcWdDfrntSpclSrvc() {

        RequestedShipment requestedShipment = createRequestedShipment();
        requestedShipment.getSpecialServicesRequested()
                .getSpecialServices()
                .get(0)
                .setSpecialServiceType("HOLD_AT_LOCATION");

        RetailShippingDataValidator.validateSpecialService(requestedShipment.getSpecialServicesRequested());

    }

    /**
     * Test case to verify that when request is made to {@link RetailShipmentCreationInputValidator.
     * validateHomeDeliveryPremSpecialService(requestedShipment)} and home delivery type provided is
     * EVENING then executed successfully
     */
    @Test
    public void testValidateHDSpclSrvcWdHomeDeliveryType() {

        RequestedShipment requestedShipment = createRequestedShipment();
        requestedShipment.getSpecialServicesRequested()
                .getHomeDeliveryDetail()
                .setHomeDeliveryType(HomeDeliveryType.APPOINTMENT);
        requestedShipment.getSpecialServicesRequested()
                .getHomeDeliveryDetail()
                .setPhoneNumber(null);

        RetailShippingDataValidator.validateSpecialService(requestedShipment.getSpecialServicesRequested());
    }

    /**
     * Test case to verify that when request is made to {@link RetailShipmentCreationInputValidator.
     * validateHomeDeliveryPremSpecialService(requestedShipment)} and delivery date value is not
     * provided then error is occurred and handled accordingly
     */
    @Test
    public void testValidateHDSpclSrvcWdDeliveryDateNull() {

        fieldName = ServiceConstant.DELIVERY_DATE;

        RequestedShipment requestedShipment = createRequestedShipment();
        requestedShipment.getSpecialServicesRequested()
                .getHomeDeliveryDetail()
                .setDeliveryDate(null);

        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo("The " + fieldName + " is missing."));

        RetailShippingDataValidator.validateSpecialService(requestedShipment.getSpecialServicesRequested());
    }

    /**
     * Test case to verify that when request is made to {@link RetailShipmentCreationInputValidator.
     * validateHomeDeliveryPremSpecialService(requestedShipment)} and delivery date value is invalid
     * then error is occurred and handled accordingly
     */
    @Test
    public void testValidateHDSpclSrvcWdInvalidDeliveryDate() {

        fieldName = ServiceConstant.DELIVERY_DATE;

        RequestedShipment requestedShipment = createRequestedShipment();
        requestedShipment.getSpecialServicesRequested()
                .getHomeDeliveryDetail()
                .setDeliveryDate("2021-31-20");

        expectedEx.expect(ServiceInputValidationException.class);
        expectedEx.expectMessage(equalTo("The " + fieldName + " is invalid."));

        RetailShippingDataValidator.validateSpecialService(requestedShipment.getSpecialServicesRequested());
    }

    /**
     * Create RequestedShipment
     */
    private RequestedShipment createRequestedShipment() {

        RequestedShipment requestedShipment = new RequestedShipment();
        requestedShipment.setSpecialServicesRequested(createShipmentSpecialSrvcDesc());
        requestedShipment.setRecipient(createMockParty("Mr. Recipient"));

        return requestedShipment;
    }

    /**
     * Create ShipmentSpecialServiceDescription
     */
    private ShipmentSpecialServiceDescription createShipmentSpecialSrvcDesc() {

        ShipmentSpecialServiceDescription shipmentSpecialServiceDesc = new ShipmentSpecialServiceDescription();
        List<SpecialServiceDescription> specialServiceDescriptionList = new ArrayList<>();
        specialServiceDescriptionList
                .add(new SpecialServiceDescription(ServiceConstant.SPECIAL_SRVC_TYPE_HD, null, null, null));
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("9948587320");
        phoneNumber.setExtension("01202");

        HomeDeliveryDetail homeDeliveryDetail = new HomeDeliveryDetail();
        homeDeliveryDetail.setHomeDeliveryType(HomeDeliveryType.DATE_CERTAIN);
        homeDeliveryDetail.setDeliveryDate("2021-10-11");
        homeDeliveryDetail.setPhoneNumber(phoneNumber);

        shipmentSpecialServiceDesc.setHomeDeliveryDetail(homeDeliveryDetail);
        shipmentSpecialServiceDesc.setSpecialServices(specialServiceDescriptionList);

        return shipmentSpecialServiceDesc;
    }

    /**
     * Prepare mock Party object.
     * 
     * @return
     */
    private Party createMockParty(
            String personName) {

        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("3112556789");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);
        ShippingContact shippingContact = new ShippingContact();
        shippingContact.setPhoneNumberDetails(phoneNumberDetails);
        shippingContact.setPersonName(personName);

        return new Party(createAddress(), shippingContact);
    }

    /**
     * Create Address
     * 
     * @return
     */
    private Address createAddress() {

        List<String> streetLines = new ArrayList<>();
        streetLines.add("9400 WADE BLVD");

        return new Address(streetLines, "Dallas", "TX", "75024", "US", AddressClassification.HOME);

    }

}
