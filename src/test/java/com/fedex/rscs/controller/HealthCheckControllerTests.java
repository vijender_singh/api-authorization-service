package com.fedex.rscs.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fedex.rscs.RetailShipmentCreationApplication;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.head;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test cases for healthCheck controller end points.
 * 
 * @author 3802280
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailShipmentCreationApplication.class)
@WebAppConfiguration
@ActiveProfiles("mock")
public class HealthCheckControllerTests {

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    /**
     * This method does the initial setup for Unit Test Cases to run by creating MockMvc bean.
     */
    @Before
    public void setup() {

        mvc = MockMvcBuilders.webAppContextSetup(context)
                .alwaysDo(MockMvcResultHandlers.print())
                .build();
    }

    /**
     * Test method for {@link com.fedex.rscs.controller.HealthCheckController#respondUp()}. This
     * test method validates health check functionality for head method request type.
     *
     * @throws Exception
     */
    @Test
    public void testRespondUpHeadMethod()
            throws Exception {

        mvc.perform(head("/health").header("Origin", "hi")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    /**
     * Test method for {@link com.fedex.rscs.controller.HealthCheckController#respondUp()}. This
     * test method validates health check functionality for get method request type.
     *
     * @throws Exception
     */
    @Test
    public void testRespondUpGetMethod()
            throws Exception {

        mvc.perform(get("/health").header("Origin", "hi")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value("UP"));
    }

}
