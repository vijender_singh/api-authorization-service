package com.fedex.rscs.controller;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.fedex.auth.token.test.WithMockOAuth2Scope;
import com.fedex.rscs.RetailShipmentCreationApplication;
import com.fedex.rscs.dto.AccountType;
import com.fedex.rscs.dto.Address;
import com.fedex.rscs.dto.AddressClassification;
import com.fedex.rscs.dto.AppName;
import com.fedex.rscs.dto.Company;
import com.fedex.rscs.dto.ConfirmShipmentRequestData;
import com.fedex.rscs.dto.ConfirmedShipmentRequest;
import com.fedex.rscs.dto.ConfirmedShipmentResponse;
import com.fedex.rscs.dto.CreateOpenShipmentRequest;
import com.fedex.rscs.dto.CreateOpenShipmentRequestData;
import com.fedex.rscs.dto.CreateOpenShipmentResponse;
import com.fedex.rscs.dto.CreditCard;
import com.fedex.rscs.dto.Currency;
import com.fedex.rscs.dto.CustomerContact;
import com.fedex.rscs.dto.CustomerEmailDetail;
import com.fedex.rscs.dto.DeleteOpenShipmentResponse;
import com.fedex.rscs.dto.DeleteSoldShipmentRequest;
import com.fedex.rscs.dto.DeleteSoldShipmentRequestData;
import com.fedex.rscs.dto.DeletedSoldShipmentResponse;
import com.fedex.rscs.dto.HomeDeliveryType;
import com.fedex.rscs.dto.Dimensions;
import com.fedex.rscs.dto.HomeDeliveryDetail;
import com.fedex.rscs.dto.ImageType;
import com.fedex.rscs.dto.InterlineShippingDetail;
import com.fedex.rscs.dto.LabelSpecification;
import com.fedex.rscs.dto.LabelType;
import com.fedex.rscs.dto.LocationDetail;
import com.fedex.rscs.dto.LocationInfo;
import com.fedex.rscs.dto.OpenShipmentIdentifier;
import com.fedex.rscs.dto.PackageSpecialHandlingType;
import com.fedex.rscs.dto.Party;
import com.fedex.rscs.dto.PaymentSource;
import com.fedex.rscs.dto.PaymentType;
import com.fedex.rscs.dto.Payor;
import com.fedex.rscs.dto.Person;
import com.fedex.rscs.dto.PersonName;
import com.fedex.rscs.dto.PhoneNumber;
import com.fedex.rscs.dto.PhoneNumberDetail;
import com.fedex.rscs.dto.Price;
import com.fedex.rscs.dto.PrintingOrientation;
import com.fedex.rscs.dto.RequestHeaderDetail;
import com.fedex.rscs.dto.RequestedPackageLineItem;
import com.fedex.rscs.dto.RequestedPaymentType;
import com.fedex.rscs.dto.RequestedShipment;
import com.fedex.rscs.dto.ResponsibleParty;
import com.fedex.rscs.dto.RetrieveOpenShipmentResponse;
import com.fedex.rscs.dto.ShipmentCheckoutRequest;
import com.fedex.rscs.dto.ShipmentCheckoutRequestData;
import com.fedex.rscs.dto.ShipmentCheckoutResponse;
import com.fedex.rscs.dto.ShipmentPaymentOption;
import com.fedex.rscs.dto.ShipmentQueryType;
import com.fedex.rscs.dto.ShipmentSpecialServiceDescription;
import com.fedex.rscs.dto.ShippingContact;
import com.fedex.rscs.dto.ShippingPaymentDetail;
import com.fedex.rscs.dto.StockType;
import com.fedex.rscs.dto.TrackingSequence;
import com.fedex.rscs.dto.Weight;
import com.fedex.rscs.dto.WeightSource;
import com.fedex.rscs.dto.WeightUnit;
import com.fedex.rscs.dto.WorkstationDetails;
import com.fedex.rscs.processor.RetailShipmentCreationProcessor;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * This class contains test cases for RetailShipmentCreationController
 * 
 * @author Dhruv.Sood
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailShipmentCreationApplication.class)
@WebAppConfiguration
@ActiveProfiles("mock")
public class RetailShipmentCreationControllerTest {

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper mapper;

    @Before
    public void setup() {

        mvc = MockMvcBuilders.webAppContextSetup(context)
                .alwaysDo(MockMvcResultHandlers.print())
                .apply(springSecurity())
                .build();
    }

    @MockBean
    private RetailShipmentCreationProcessor retailShipmentCreationProcessor;

    private static final String CREATE_OPEN_SHIPMENTS_ENDPOINT_URI = "/retailshipping/fedexoffice/v1/openshipments";
    private static final String RETRIEVE_OPEN_SHIPMENTS_ENDPOINT_URI = "/retailshipping/fedexoffice/v1/openshipments/";
    private static final String CONFIRM_SHIPMENTS_ENDPOINT_URI = "/retailshipping/fedexoffice/v1/confirmedshipments";
    private static final String SHIPMENT_CHECKOUTS_ENDPOINT_URI = "/retailshipping/fedexoffice/v1/shipmentcheckouts";
    private static final String DELETE_OPEN_SHIPMENTS_ENDPOINT_URI = "/retailshipping/fedexoffice/v1/openshipments/";
    private static final String RETRIEVE_SOLD_SHIPMENTS_ENDPOINT_URI = "/retailshipping/fedexoffice/v1/locations/";
    private static final String RETRIEVE_SOLD_SHIPMENT_SUMMARIES_ENDPOINT_URI =
            "/retailshipping/fedexoffice/v1/locations/";
    private static final String DELETE_SOLD_SHIPMENTS_ENDPOINT_URI =
            "/retailshipping/fedexoffice/v1/deletesoldshipments";

    private static final String ERROR_CODE = "errors[*].code";
    private static final String ERRORS = "errors";
    private static final String TXN_ID = "transactionId";
    private static final String FEDEX_2020 = "fedex-2020";
    private static final String X_TXN_ID = "x-transaction-id";

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequestData)} then
     * successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentsSuccess()
            throws Exception {

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI)
                .content(mapper.writeValueAsString(createMockCreateOpenShipmentRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequestData)} and
     * delivery type is passed as null then error response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentsWhenDeliveryTypeNull()
            throws Exception {

        final String expectedDeliveryTypeMissingErrorCode = "HOMEDELIVERYDETAIL.HOMEDELIVERYTYPE.MISSING.PROPERTY";
        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());
        CreateOpenShipmentRequest request = createMockCreateOpenShipmentRequest();
        request.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .setSpecialServicesRequested(createSpecialServiceWithHomeDeliveryTypeNull());
        mvc.perform(post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedDeliveryTypeMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(1)));
    }

    /**
     * Method to create mock create open shipment request
     * 
     * @return
     */
    private CreateOpenShipmentRequest createMockCreateOpenShipmentRequest() {

        CreateOpenShipmentRequest createOpenShipmentRequest = new CreateOpenShipmentRequest();
        createOpenShipmentRequest.setCreateOpenShipmentRequest(createMockCreateOpenShipmentRequestData());

        return createOpenShipmentRequest;

    }

    /**
     * Method to create mock create open shipment request data
     * 
     * @return
     */
    private CreateOpenShipmentRequestData createMockCreateOpenShipmentRequestData() {

        CreateOpenShipmentRequestData createOpenShipmentRequestData = new CreateOpenShipmentRequestData();
        createOpenShipmentRequestData.setHeaderDetails(mockRequestHeaderDetail());
        createOpenShipmentRequestData.setLocationDetails(mockLocationDetail());
        createOpenShipmentRequestData.setRequestedShipment(createMockRequestedShipment());
        createOpenShipmentRequestData.setPaymentOption(buildShipmentPaymentOptions());
        createOpenShipmentRequestData.setCustomer(createMockPerson("Mr. Customer"));
        createOpenShipmentRequestData.setLabelSpecification(createMockLabelSpecification());
        createOpenShipmentRequestData.setInterlineShippingDetails(createMockInterlineShippingDetail());

        return createOpenShipmentRequestData;
    }

    /**
     * Method to build the mock instance of {@link ShipmentPaymentOption} object
     * 
     * @return
     */
    private ShipmentPaymentOption buildShipmentPaymentOptions() {

        ShipmentPaymentOption shipmentPaymentOption = new ShipmentPaymentOption();
        shipmentPaymentOption.setPaymentSource(PaymentSource.SPOS);
        shipmentPaymentOption.setPaymentType(RequestedPaymentType.ACCOUNT);
        shipmentPaymentOption.setResponsibleParty(ResponsibleParty.SENDER);
        shipmentPaymentOption.setAccountNumber("038000004");

        return shipmentPaymentOption;
    }

    /**
     * Method to build the mock instance of {@link ShippingPaymentDetail} object
     * 
     * @return
     */
    private ShippingPaymentDetail buildShipmentPaymentDetails() {

        ShippingPaymentDetail shippingPaymentDetail = new ShippingPaymentDetail();
        shippingPaymentDetail.setPaymentSource(PaymentSource.SPOS);
        shippingPaymentDetail.setPaymentType(PaymentType.CASH);
        shippingPaymentDetail.setResponsibleParty(ResponsibleParty.SENDER);

        Price price = new Price();
        price.setAmount(1.1);
        price.setCurrency(Currency.USD);

        Payor payor = new Payor();
        payor.setAccountNumber("038000004");
        payor.setAccountType(AccountType.FEDEX_EXPRESS);
        payor.setAddress(mockAddress());
        payor.setCreditCard(new CreditCard());

        shippingPaymentDetail.setAmount(price);
        shippingPaymentDetail.setPayor(payor);

        return shippingPaymentDetail;
    }

    /**
     * Method to build the mock instance of {@link RequestHeaderDetail} object .
     * 
     * @return
     */
    private RequestHeaderDetail mockRequestHeaderDetail() {

        return new RequestHeaderDetail("548796554", "8745896532", ZonedDateTime.now()
                .toString(), mockWorkstationDetails());
    }

    /**
     * Method to build the mock instance of {@link WorkstationDetails} object.
     * 
     * @return
     */
    private WorkstationDetails mockWorkstationDetails() {

        return new WorkstationDetails("BTC-50", "ROSA-01", "123456", AppName.FASTLANE, "SSFE");
    }

    /**
     * Method to build the mock instance of {@link LocationDetail} object.
     * 
     * @return
     */
    private LocationDetail mockLocationDetail() {

        LocationInfo transactionLocationInfo = new LocationInfo("DFWS", mockAddress());
        LocationInfo servingLocationInfo = new LocationInfo("DALA", mockAddress());

        return new LocationDetail("123456", "123456789", transactionLocationInfo, true, true, servingLocationInfo);
    }

    /**
     * Prepare mock address object.
     * 
     * @return
     */
    public Address mockAddress() {

        List<String> streetlines = new ArrayList<String>();
        streetlines.add("H.No 31, XYZ sector");
        return new Address(streetlines, "Dallas", "TX", "75024", "US", AddressClassification.HOME);

    }

    /**
     * Prepare mock RequestedShipment object.
     * 
     * @return
     */
    private RequestedShipment createMockRequestedShipment() {

        RequestedShipment requestedShipment = new RequestedShipment();
        requestedShipment.setServiceType("GROUND_HOME_DELIVERY");
        requestedShipment.setPackagingType("YOUR_PACKAGING");
        requestedShipment.setShipper(createMockParty("Mr. Shipper"));
        requestedShipment.setRecipient(createMockParty("Mr. Recipient"));
        requestedShipment.setSpecialServicesRequested(new ShipmentSpecialServiceDescription());
        requestedShipment.setRequestedPackageLineItems(createMockRequestedPackageLineItems());
        return requestedShipment;
    }

    /**
     * Prepare mock Party object.
     * 
     * @return
     */
    private Party createMockParty(
            String personName) {

        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("3112556789");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);
        ShippingContact shippingContact = new ShippingContact();
        shippingContact.setPhoneNumberDetails(phoneNumberDetails);
        shippingContact.setPersonName(personName);

        return new Party(mockAddress(), shippingContact);
    }

    /**
     * Prepare mock Person object.
     * 
     * @return
     */
    private Person createMockPerson(
            String personName) {

        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("3112556789");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);
        CustomerEmailDetail custEmailDtls = new CustomerEmailDetail();
        custEmailDtls.setEmailAddress("mainEmail123@testmail.com");
        custEmailDtls.setAlternateEmailAddress("alternateEmail123@testmail.com");
        CustomerContact customerContact = new CustomerContact();
        customerContact.setPhoneNumberDetails(phoneNumberDetails);
        customerContact.setPersonName(personName);
        customerContact.setEmailDetails(custEmailDtls);

        return new Person(mockAddress(), customerContact);
    }

    /**
     * Prepare mock List<RequestedPackageLineItems> object.
     * 
     * @return
     */
    private List<RequestedPackageLineItem> createMockRequestedPackageLineItems() {

        Weight weight = new Weight();
        weight.setValue("20");
        weight.setUnits(WeightUnit.KG);
        weight.setMeasurementType(WeightSource.MANUAL);

        List<RequestedPackageLineItem> listOfRequestedPackageLineItems = new ArrayList<>();

        RequestedPackageLineItem requestedPackageLineItems = new RequestedPackageLineItem();
        requestedPackageLineItems.setDimensions(null);
        requestedPackageLineItems.setInsuredValue(null);
        requestedPackageLineItems.setSpecialHandlingDetail(PackageSpecialHandlingType.CUSTOMER);
        requestedPackageLineItems.setSpecialServicesRequested(new ArrayList<>());
        requestedPackageLineItems.setWeight(weight);

        listOfRequestedPackageLineItems.add(requestedPackageLineItems);

        return listOfRequestedPackageLineItems;
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequestData)} with
     * HeaderDetails and requestedShipment as null and then error response is returned and handled
     * successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentsBadRequest()
            throws Exception {

        final String expectedHeaderDetailsMissingErrorCode = "CREATEOPENSHIPMENTREQUEST.HEADERDETAILS.MISSING.PROPERTY";
        final String expectedLocationDetailsMissingErrorCode =
                "CREATEOPENSHIPMENTREQUEST.LOCATIONDETAILS.MISSING.PROPERTY";
        final String expectedRequestedShipmentMissingErrorCode =
                "CREATEOPENSHIPMENTREQUEST.REQUESTEDSHIPMENT.MISSING.PROPERTY";
        final String expectedPaymentDetailsMissingErrorCode =
                "CREATEOPENSHIPMENTREQUEST.PAYMENTOPTION.MISSING.PROPERTY";
        final String expectedCustomerMissingErrorCode = "CREATEOPENSHIPMENTREQUEST.CUSTOMER.MISSING.PROPERTY";
        final String expectedLabelSpecificationMissingErrorCode =
                "CREATEOPENSHIPMENTREQUEST.LABELSPECIFICATION.MISSING.PROPERTY";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .setHeaderDetails(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .setLocationDetails(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .setRequestedShipment(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .setPaymentOption(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .setCustomer(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .setLabelSpecification(null);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedHeaderDetailsMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedLocationDetailsMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedRequestedShipmentMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedPaymentDetailsMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedCustomerMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedLabelSpecificationMissingErrorCode)))
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));

    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequestData)} with
     * labelSpecification > formatType, imageType, stockType and printingOrientation as null and then
     * error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentsWDLabelSpecificationNull()
            throws Exception {

        final String expectedFormatTypeMissingErrorCode = "LABELSPECIFICATION.FORMATTYPE.MISSING.PROPERTY";
        final String expectedImageTypeMissingErrorCode = "LABELSPECIFICATION.IMAGETYPE.MISSING.PROPERTY";
        final String expectedStockTypeMissingErrorCode = "LABELSPECIFICATION.STOCKTYPE.MISSING.PROPERTY";
        final String expectedPrintingOrientationMissingErrorCode =
                "LABELSPECIFICATION.PRINTINGORIENTATION.MISSING.PROPERTY";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getLabelSpecification()
                .setFormatType(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getLabelSpecification()
                .setImageType(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getLabelSpecification()
                .setStockType(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getLabelSpecification()
                .setPrintingOrientation(null);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedFormatTypeMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedImageTypeMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedStockTypeMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedPrintingOrientationMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(4)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequestData)} with
     * HeaderDetails > requestDateTime and WorkStationDetails as null and then error response is
     * returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentsWDRequestDateTimeTranscInfoNull()
            throws Exception {

        final String expectedRequestDateTimeMissingErrorCode = "HEADERDETAILS.REQUESTDATETIME.MISSING.PROPERTY";
        final String expectedWorkStationDetailsMissingErrorCode = "HEADERDETAILS.WORKSTATIONDETAILS.MISSING.PROPERTY";
        final String expectedTransactionLocationInfoMissingErrorCode =
                "LOCATIONDETAILS.TRANSACTIONLOCATIONINFO.MISSING.PROPERTY";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getHeaderDetails()
                .setRequestDateTime(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getHeaderDetails()
                .setWorkstationDetails(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getLocationDetails()
                .setTransactionLocationInfo(null);
        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedRequestDateTimeMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedWorkStationDetailsMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedTransactionLocationInfoMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(3)));

    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequestData)} with
     * RequestedShipment > serviceType, packagingType, shipper, recipient and requestedPackageLineItems
     * as null and then error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentsRSElementsNull()
            throws Exception {

        final String expectedServiceTypeMissingErrorCode = "REQUESTEDSHIPMENT.SERVICETYPE.MISSING.PROPERTY";
        final String expectedPackagingTypeMissingErrorCode = "REQUESTEDSHIPMENT.PACKAGINGTYPE.MISSING.PROPERTY";
        final String expectedRecipientMissingErrorCode = "REQUESTEDSHIPMENT.RECIPIENT.MISSING.PROPERTY";
        final String expectedRequestedPackageLineItemsMissingErrorCode =
                "REQUESTEDSHIPMENT.REQUESTEDPACKAGELINEITEMS.MISSING.PROPERTY";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .setServiceType(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .setPackagingType(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .setShipper(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .setRecipient(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .setRequestedPackageLineItems(null);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedServiceTypeMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedPackagingTypeMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedRecipientMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedRequestedPackageLineItemsMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(4)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequestData)} with
     * RequestedShipment > shipper and recipient address as null and then error response is returned and
     * handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentsRSPartyAddressElementsNull()
            throws Exception {

        final String expectedRecipientAddressMissingErrorCode = "RECIPIENT.ADDRESS.MISSING.PROPERTY";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .setAddress(null);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedRecipientAddressMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(1)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequestData)} with
     * RequestedShipment > shipper and recipient countryCode and phone number as null and then error
     * response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentsRSPartyAddressCCNull()
            throws Exception {

        final String expectedAddressMissingErrorCode = "ADDRESS.COUNTRYCODE.MISSING.PROPERTY";
        final String expectedPhoneNumberMissingErrorCode = "PHONENUMBER.NUMBER.MISSING.PROPERTY";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .setCountryCode(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getContact()
                .getPhoneNumberDetails()
                .get(0)
                .getPhoneNumber()
                .setNumber(null);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedPhoneNumberMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(2)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequestData)} with
     * RequestedShipment > shipper and recipient dimensions, weight, price as null and then error
     * response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentsRSLineItemsNull()
            throws Exception {

        final String expectedAmountMissingErrorCode = "INSUREDVALUE.AMOUNT.MISSING.PROPERTY";
        final String expectedCurrencyMissingErrorCode = "INSUREDVALUE.CURRENCY.MISSING.PROPERTY";
        final String expectedWidthMissingErrorCode = "DIMENSIONS.WIDTH.MISSING.PROPERTY";
        final String expectedLengthMissingErrorCode = "DIMENSIONS.LENGTH.MISSING.PROPERTY";
        final String expectedHeightMissingErrorCode = "DIMENSIONS.HEIGHT.MISSING.PROPERTY";
        final String expectedUnitsMissingErrorCode = "DIMENSIONS.UNITS.MISSING.PROPERTY";
        final String expectedWeightMissingErrorCode = "REQUESTEDPACKAGELINEITEMS[0].WEIGHT.MISSING.PROPERTY";

        Weight weight = new Weight();
        weight.setValue("20");
        weight.setUnits(WeightUnit.KG);

        List<RequestedPackageLineItem> listOfRequestedPackageLineItems = new ArrayList<>();

        RequestedPackageLineItem requestedPackageLineItems = new RequestedPackageLineItem();
        requestedPackageLineItems.setDimensions(new Dimensions());
        requestedPackageLineItems.setInsuredValue(new Price());
        requestedPackageLineItems.setSpecialHandlingDetail(PackageSpecialHandlingType.CUSTOMER);
        requestedPackageLineItems.setSpecialServicesRequested(new ArrayList<>());
        requestedPackageLineItems.setWeight(null);

        listOfRequestedPackageLineItems.add(requestedPackageLineItems);


        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .setRequestedPackageLineItems(listOfRequestedPackageLineItems);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAmountMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedCurrencyMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedWidthMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedLengthMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedHeightMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedUnitsMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedWeightMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(7)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequestData)} with
     * RequestedShipment > weight > Units and Value as null and then error response is returned and
     * handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentsRSLineItemsWeightNull()
            throws Exception {

        final String expectedWeightValueMissingErrorCode = "WEIGHT.VALUE.MISSING.PROPERTY";
        final String expectedWeightUnitsMissingErrorCode = "WEIGHT.UNITS.MISSING.PROPERTY";

        List<RequestedPackageLineItem> listOfRequestedPackageLineItems = new ArrayList<>();

        RequestedPackageLineItem requestedPackageLineItems = new RequestedPackageLineItem();
        requestedPackageLineItems.setDimensions(null);
        requestedPackageLineItems.setInsuredValue(null);
        requestedPackageLineItems.setSpecialHandlingDetail(PackageSpecialHandlingType.CUSTOMER);
        requestedPackageLineItems.setSpecialServicesRequested(new ArrayList<>());
        requestedPackageLineItems.setWeight(new Weight());

        listOfRequestedPackageLineItems.add(requestedPackageLineItems);

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .setRequestedPackageLineItems(listOfRequestedPackageLineItems);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedWeightValueMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedWeightUnitsMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(2)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequestData)} with
     * ShipmentPaymentOption > paymentType, paymentSource and responsibleParty as null and then error
     * response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentsSPDElementsNull()
            throws Exception {

        final String expectedPaymentTypeMissingErrorCode = "PAYMENTOPTION.PAYMENTTYPE.MISSING.PROPERTY";
        final String expectedPaymentSourceMissingErrorCode = "PAYMENTOPTION.PAYMENTSOURCE.MISSING.PROPERTY";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getPaymentOption()
                .setPaymentType(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getPaymentOption()
                .setPaymentSource(null);

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedPaymentTypeMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedPaymentSourceMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(2)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#retrieveOpenShipment(String, String, String) } then
     * successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"retrieveOpenShipment.get"})
    public void testRetrieveOpenShipment()
            throws Exception {

        String locationId = "LASA";
        String trackingId = "794973860000";
        String referenceId = "3456";

        when(retailShipmentCreationProcessor.retrieveOpenShipment(anyString(), anyString(), anyString()))
                .thenReturn(new RetrieveOpenShipmentResponse());

        String uri = RETRIEVE_OPEN_SHIPMENTS_ENDPOINT_URI + "?referenceId=" + referenceId + "&locationId=" + locationId
                + "&trackingId=" + trackingId;
        mvc.perform(get(uri).accept(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#deleteOpenShipment(String, String, String) } then
     * successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"deleteOpenShipment.delete"})
    public void testDeleteOpenShipment()
            throws Exception {

        String locationId = "LASA";
        String trackingId = "794973860000";
        String referenceId = "3456";

        when(retailShipmentCreationProcessor.deleteOpenShipment(anyString(), anyString(), anyString()))
                .thenReturn(new DeleteOpenShipmentResponse());

        String uri = DELETE_OPEN_SHIPMENTS_ENDPOINT_URI + "?referenceId=" + referenceId + "&locationId=" + locationId
                + "&trackingId=" + trackingId;
        mvc.perform(delete(uri).accept(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#retrieveSoldShipments(String, String, ShipmentQueryType) }
     * then successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"retrieveSoldShipments.get"})
    public void testRetrieveSoldShipments()
            throws Exception {

        String locationId = "LASA";
        String id = "794973860000";
        String type = "TRACK";

        String uri = RETRIEVE_SOLD_SHIPMENTS_ENDPOINT_URI + locationId + "/soldshipments?id=" + id + "&type=" + type;

        mvc.perform(get(uri).accept(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#retrieveSoldShipmentSummaries(String, String) } then
     * successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"retrieveSoldShipmentSummaries.get"})
    public void testRetrieveSoldShipmentSummaries()
            throws Exception {

        String locationId = "LASA";
        String operationaldate = "2020-10-09";

        String uri = RETRIEVE_SOLD_SHIPMENT_SUMMARIES_ENDPOINT_URI + locationId
                + "/soldshipmentsummaries?operationaldate=" + operationaldate;

        mvc.perform(get(uri).accept(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Method to create mock confirmed shipments request
     * 
     * @return
     */
    private ConfirmedShipmentRequest createMockConfirmedShipmentRequest() {

        ConfirmedShipmentRequest confirmedShipmentRequest = new ConfirmedShipmentRequest();
        confirmedShipmentRequest.setConfirmedShipmentRequest(createMockConfirmShipmentRequestData());

        return confirmedShipmentRequest;
    }

    /**
     * Method to create mock confirm shipments request data
     * 
     * @return
     */
    private ConfirmShipmentRequestData createMockConfirmShipmentRequestData() {

        ConfirmShipmentRequestData confirmShipmentRequestData = new ConfirmShipmentRequestData();
        confirmShipmentRequestData.setHeaderDetails(mockRequestHeaderDetail());
        confirmShipmentRequestData.setLocationDetails(mockLocationDetail());
        confirmShipmentRequestData.setRequestedShipment(createMockRequestedShipmentConfirmShpmts());
        confirmShipmentRequestData.setPaymentDetails(buildShipmentPaymentDetails());
        confirmShipmentRequestData.setLabelSpecification(createMockLabelSpecification());

        return confirmShipmentRequestData;
    }

    /**
     * Prepare mock OpenShipmentIdentifier object.
     * 
     * @return
     */
    private OpenShipmentIdentifier createMockRequestedShipmentConfirmShpmts() {

        OpenShipmentIdentifier requestedOpenShipment = new OpenShipmentIdentifier();
        requestedOpenShipment.setReferenceId("ALSAKA5544331");
        requestedOpenShipment.setMasterTrackingId(createMockMasterTrackingId());
        return requestedOpenShipment;
    }

    /**
     * Prepare mock TrackingSequence object.
     * 
     * @return
     */
    private TrackingSequence createMockMasterTrackingId() {

        TrackingSequence trackingSequence = new TrackingSequence();
        trackingSequence.setBarcode("1009997351190390058700794960774655");
        trackingSequence.setTrackingNumber("794960774655");
        trackingSequence.setSequence(1);
        return trackingSequence;
    }

    /**
     * Prepare mock LabelSpecification object.
     * 
     * @return
     */
    private LabelSpecification createMockLabelSpecification() {

        LabelSpecification labelSpecification = new LabelSpecification();
        labelSpecification.setProcessingOptionsRequested("IGNORE_RESIDENTIAL_DELIVERY_POLICIES");
        labelSpecification.setFormatType(LabelType.OPERATIONAL_LABEL);
        labelSpecification.setStockType(StockType.STOCK_4X6);
        labelSpecification.setImageType(ImageType.PNG);
        labelSpecification.setPrintingOrientation(PrintingOrientation.TOP_EDGE_OF_TEXT_FIRST);
        labelSpecification.setRotation("NA");
        labelSpecification.setLabelOrigin("ALVA");
        labelSpecification.setCustomerSpecifiedDetail("BUSINESS");
        return labelSpecification;
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipments(ConfirmShipmentRequestData)} then
     * successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"confirmedshipments.post"})
    public void testConfirmShipment()
            throws Exception {

        when(retailShipmentCreationProcessor.confirmShipments(any(ConfirmedShipmentRequest.class)))
                .thenReturn(new ConfirmedShipmentResponse());

        mvc.perform(post(CONFIRM_SHIPMENTS_ENDPOINT_URI)
                .content(mapper.writeValueAsString(createMockConfirmedShipmentRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));

    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipments(ConfirmShipmentRequestData)} with
     * HeaderDetails, requestedShipment and locationDetails as null and then error response is returned
     * and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"confirmedshipments.post"})
    public void testConfirmShipmentBadRequest()
            throws Exception {

        final String expectedHeaderDetailsMissingErrorCode = "CONFIRMEDSHIPMENTREQUEST.HEADERDETAILS.MISSING.PROPERTY";
        final String expectedRequestedShipmentMissingErrorCode =
                "CONFIRMEDSHIPMENTREQUEST.REQUESTEDSHIPMENT.MISSING.PROPERTY";
        final String expectedLocationDetailMissingErrorCode =
                "CONFIRMEDSHIPMENTREQUEST.LOCATIONDETAILS.MISSING.PROPERTY";

        ConfirmedShipmentRequest confirmedShipmentRequest = createMockConfirmedShipmentRequest();
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .setHeaderDetails(null);
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .setRequestedShipment(null);
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .setLocationDetails(null);

        when(retailShipmentCreationProcessor.confirmShipments(any(ConfirmedShipmentRequest.class)))
                .thenReturn(new ConfirmedShipmentResponse());

        mvc.perform(post(CONFIRM_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(confirmedShipmentRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedHeaderDetailsMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedRequestedShipmentMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedLocationDetailMissingErrorCode)))
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));

    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipments(ConfirmShipmentRequestData)} with
     * HeaderDetails > requestDateTime and WorkStationDetails and LocationDetail >
     * transactionLocationInfo as null and then error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"confirmedshipments.post"})
    public void testconfirmShipmentsWDRequestDateTimeTransLocInfoNull()
            throws Exception {

        final String expectedRequestDateTimeMissingErrorCode = "HEADERDETAILS.REQUESTDATETIME.MISSING.PROPERTY";
        final String expectedWorkStationDetailsMissingErrorCode = "HEADERDETAILS.WORKSTATIONDETAILS.MISSING.PROPERTY";
        final String expectedTransactionLocationInfoMissingErrorCode =
                "LOCATIONDETAILS.TRANSACTIONLOCATIONINFO.MISSING.PROPERTY";

        ConfirmedShipmentRequest confirmedShipmentRequest = createMockConfirmedShipmentRequest();
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getHeaderDetails()
                .setRequestDateTime(null);
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getHeaderDetails()
                .setWorkstationDetails(null);
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getLocationDetails()
                .setTransactionLocationInfo(null);

        when(retailShipmentCreationProcessor.confirmShipments(any(ConfirmedShipmentRequest.class)))
                .thenReturn(new ConfirmedShipmentResponse());

        mvc.perform(post(CONFIRM_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(confirmedShipmentRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedRequestDateTimeMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedWorkStationDetailsMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedTransactionLocationInfoMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(3)));

    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipments(ConfirmShipmentRequestData)} with
     * transactionLocationInfo > code, address, OpenShipmentIdentifier > referrencsId and
     * masterTrackingId as null and then error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"confirmedshipments.post"})
    public void testconfirmShipmentsLocInfoOpenShpmntsElementsNull()
            throws Exception {

        final String expectedTransLocInfoCodeMissingErrorCode = "TRANSACTIONLOCATIONINFO.CODE.MISSING.PROPERTY";
        final String expectedTransLocInfoAddressMissingErrorCode = "TRANSACTIONLOCATIONINFO.ADDRESS.MISSING.PROPERTY";
        final String expectedReferenceIdMissingErrorCode = "REQUESTEDSHIPMENT.REFERENCEID.MISSING.PROPERTY";
        final String expectedMasterTrackingIdErrorCode = "REQUESTEDSHIPMENT.MASTERTRACKINGID.MISSING.PROPERTY";

        ConfirmedShipmentRequest confirmedShipmentRequest = createMockConfirmedShipmentRequest();
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .setCode(null);
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .setAddress(null);
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getRequestedShipment()
                .setMasterTrackingId(null);
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getRequestedShipment()
                .setReferenceId(null);

        when(retailShipmentCreationProcessor.confirmShipments(any(ConfirmedShipmentRequest.class)))
                .thenReturn(new ConfirmedShipmentResponse());

        mvc.perform(post(CONFIRM_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(confirmedShipmentRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedTransLocInfoCodeMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedTransLocInfoAddressMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedReferenceIdMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedMasterTrackingIdErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(4)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipments(ConfirmShipmentRequestData)} with
     * ShippingPaymentDetail > paymentType, paymentSource and responsibleParty and TrackingSequence >
     * trackingNumber as null and then error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"confirmedshipments.post"})
    public void testconfirmShipmentsOpnShpmtsElementsNull()
            throws Exception {

        final String expectedTrackngNbrMissingErrorCode = "MASTERTRACKINGID.TRACKINGNUMBER.MISSING.PROPERTY";
        final String expectedPaymentTypeMissingErrorCode = "PAYMENTDETAILS.PAYMENTTYPE.MISSING.PROPERTY";
        final String expectedPaymentSourceErrorCode = "PAYMENTDETAILS.PAYMENTSOURCE.MISSING.PROPERTY";

        ConfirmedShipmentRequest confirmedShipmentRequest = createMockConfirmedShipmentRequest();
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getRequestedShipment()
                .getMasterTrackingId()
                .setTrackingNumber(null);
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getPaymentDetails()
                .setPaymentType(null);
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getPaymentDetails()
                .setPaymentSource(null);
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getPaymentDetails()
                .getPayor()
                .setAccountNumber(null);

        when(retailShipmentCreationProcessor.confirmShipments(any(ConfirmedShipmentRequest.class)))
                .thenReturn(new ConfirmedShipmentResponse());

        mvc.perform(post(CONFIRM_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(confirmedShipmentRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedTrackngNbrMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedPaymentTypeMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedPaymentSourceErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(3)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#shipmentCheckouts(ShipmentCheckoutRequestData)} with
     * HeaderDetails and requestedShipment as null and then error response is returned and handled
     * successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"shipmentcheckouts.post"})
    public void testConfirmShipmentsCheckoutBadRequest()
            throws Exception {

        final String expectedHeaderDetailsMissingErrorCode = "SHIPMENTCHECKOUTREQUEST.HEADERDETAILS.MISSING.PROPERTY";
        final String expectedRequestedShipmentMissingErrorCode =
                "SHIPMENTCHECKOUTREQUEST.REQUESTEDSHIPMENT.MISSING.PROPERTY";
        final String expectedLocationDetailsMissingErrorCode =
                "SHIPMENTCHECKOUTREQUEST.LOCATIONDETAILS.MISSING.PROPERTY";

        ShipmentCheckoutRequest shipmentCheckoutRequest = createMockShipmentCheckoutRequest();
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .setHeaderDetails(null);
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .setRequestedShipment(null);
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .setLocationDetails(null);

        mvc.perform(post(SHIPMENT_CHECKOUTS_ENDPOINT_URI).content(mapper.writeValueAsString(shipmentCheckoutRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedHeaderDetailsMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedRequestedShipmentMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedLocationDetailsMissingErrorCode)))
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));

    }

    /**
     * Method to create mock shipment checkout request
     * 
     * @return
     */
    private ShipmentCheckoutRequest createMockShipmentCheckoutRequest() {

        ShipmentCheckoutRequest shipmentCheckoutRequest = new ShipmentCheckoutRequest();
        shipmentCheckoutRequest.setShipmentCheckoutRequest(createMockShipmentCheckoutRequestData());

        return shipmentCheckoutRequest;
    }

    /**
     * Method to create mock shipment checkout request data
     * 
     * @return
     */
    private ShipmentCheckoutRequestData createMockShipmentCheckoutRequestData() {

        ShipmentCheckoutRequestData shipmentCheckoutRequestData = new ShipmentCheckoutRequestData();
        shipmentCheckoutRequestData.setHeaderDetails(mockRequestHeaderDetail());
        shipmentCheckoutRequestData.setLocationDetails(mockLocationDetail());
        List<OpenShipmentIdentifier> requestedShipments = new ArrayList<>();
        requestedShipments.add(createMockRequestedShipmentConfirmShpmts());
        requestedShipments.add(createMockRequestedShipmentConfirmShpmts());
        shipmentCheckoutRequestData.setRequestedShipment(requestedShipments);
        shipmentCheckoutRequestData.setLabelSpecification(createMockLabelSpecification());
        shipmentCheckoutRequestData.setPaymentDetails(buildShipmentPaymentDetails());

        return shipmentCheckoutRequestData;
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipmentsCheckout(ShipmentCheckoutRequestData)}
     * then successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"shipmentcheckouts.post"})
    public void testConfirmShipmentsCheckout()
            throws Exception {

        when(retailShipmentCreationProcessor.checkoutShipments(any(ShipmentCheckoutRequest.class)))
                .thenReturn(new ShipmentCheckoutResponse());

        mvc.perform(post(SHIPMENT_CHECKOUTS_ENDPOINT_URI)
                .content(mapper.writeValueAsString(createMockShipmentCheckoutRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));

    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * CreateOpenShipmentRequest is null and then error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentsElementNull()
            throws Exception {

        final String expectedCreateOpenShipmenteMissingErrorCode = "CREATEOPENSHIPMENTREQUEST.MISSING.PROPERTY";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        createOpenShipmentRequest.setCreateOpenShipmentRequest(null);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedCreateOpenShipmenteMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(1)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipments(ConfirmedShipmentRequest)} with
     * ConfirmedShipmentRequest as null and then error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"confirmedshipments.post"})
    public void testConfirmedShipmentElementNull()
            throws Exception {

        final String expectedConfirmedShipmentRequestMissingErrorCode = "CONFIRMEDSHIPMENTREQUEST.MISSING.PROPERTY";

        ConfirmedShipmentRequest confirmedShipmentRequest = createMockConfirmedShipmentRequest();
        confirmedShipmentRequest.setConfirmedShipmentRequest(null);

        when(retailShipmentCreationProcessor.confirmShipments(any(ConfirmedShipmentRequest.class)))
                .thenReturn(new ConfirmedShipmentResponse());

        mvc.perform(post(CONFIRM_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(confirmedShipmentRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedConfirmedShipmentRequestMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(1)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#shipmentCheckouts(ShipmentCheckoutRequest)} with
     * ShipmentCheckoutRequest as null and then error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"shipmentcheckouts.post"})
    public void testConfirmShipmentsCheckoutElementNull()
            throws Exception {

        final String expectedShipmentCheckoutRequestMissingErrorCode = "SHIPMENTCHECKOUTREQUEST.MISSING.PROPERTY";

        ShipmentCheckoutRequest shipmentCheckoutRequest = createMockShipmentCheckoutRequest();
        shipmentCheckoutRequest.setShipmentCheckoutRequest(null);

        when(retailShipmentCreationProcessor.checkoutShipments(any(ShipmentCheckoutRequest.class)))
                .thenReturn(new ShipmentCheckoutResponse());

        mvc.perform(post(SHIPMENT_CHECKOUTS_ENDPOINT_URI).content(mapper.writeValueAsString(shipmentCheckoutRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShipmentCheckoutRequestMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(1)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequestData)} with
     * Customer address as null and then error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentCustomerPartyAddressElementsNull()
            throws Exception {

        final String expectedCustomerAddressMissingErrorCode = "CUSTOMER.ADDRESS.MISSING.PROPERTY";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .setAddress(null);


        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedCustomerAddressMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(1)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequestData)} with
     * Customer countryCode and phone number as null and then error response is returned and handled
     * successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentCustomerPartyAddressCCNull()
            throws Exception {

        final String expectedAddressMissingErrorCode = "ADDRESS.COUNTRYCODE.MISSING.PROPERTY";
        final String expectedPhoneNumberMissingErrorCode = "PHONENUMBER.NUMBER.MISSING.PROPERTY";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress()
                .setCountryCode(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getContact()
                .getPhoneNumberDetails()
                .get(0)
                .getPhoneNumber()
                .setNumber(null);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedPhoneNumberMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(2)));
    }

    /**
     * Method to create mock delete sold shipment request
     * 
     * @return
     */
    private DeleteSoldShipmentRequest createMockDeleteSoldShipmentRequest() {

        DeleteSoldShipmentRequest deleteSoldShipmentRequest = new DeleteSoldShipmentRequest();
        deleteSoldShipmentRequest.setDeleteSoldShipmentsRequest(createMockDeleteSoldShipmentRequestData());

        return deleteSoldShipmentRequest;
    }

    /**
     * Method to create mock delete sold shipment request data
     * 
     * @return
     */
    private DeleteSoldShipmentRequestData createMockDeleteSoldShipmentRequestData() {

        DeleteSoldShipmentRequestData deleteSoldShipmentRequestData = new DeleteSoldShipmentRequestData();
        deleteSoldShipmentRequestData.setHeaderDetails(mockRequestHeaderDetail());
        deleteSoldShipmentRequestData.setLocationDetails(new LocationInfo("DFWD", mockAddress()));
        deleteSoldShipmentRequestData.setTrackingId("794984201540");
        deleteSoldShipmentRequestData.setRetailTransactionId("794984201540");
        deleteSoldShipmentRequestData.setCustomerName(new PersonName("Mr.", "Customer"));

        return deleteSoldShipmentRequestData;
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#deleteSoldShipments(DeleteSoldShipmentRequestData)} then
     * successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"deletesoldshipments.post"})
    public void testDeleteSoldShipmentsSuccess()
            throws Exception {

        when(retailShipmentCreationProcessor.deleteSoldShipments(any(DeleteSoldShipmentRequest.class)))
                .thenReturn(new DeletedSoldShipmentResponse());

        mvc.perform(post(DELETE_SOLD_SHIPMENTS_ENDPOINT_URI)
                .content(mapper.writeValueAsString(createMockDeleteSoldShipmentRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#deleteSoldShipments(deleteSoldShipmentRequestData)} with
     * HeaderDetails, locationDetails, trackingId, retailTransactionId and customerName as null and then
     * error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"deletesoldshipments.post"})
    public void testDeleteSoldShipmentsBadRequest()
            throws Exception {

        final String expectedHeaderDetailsMissingErrorCode =
                "DELETESOLDSHIPMENTSREQUEST.HEADERDETAILS.MISSING.PROPERTY";
        final String expectedLocationDetailsMissingErrorCode =
                "DELETESOLDSHIPMENTSREQUEST.LOCATIONDETAILS.MISSING.PROPERTY";
        final String expectedTrackingIdMissingErrorCode = "DELETESOLDSHIPMENTSREQUEST.TRACKINGID.MISSING.PROPERTY";
        final String expectedRetailTransactionIdMissingErrorCode =
                "DELETESOLDSHIPMENTSREQUEST.RETAILTRANSACTIONID.MISSING.PROPERTY";
        final String expectedCustomerNameErrorCode = "DELETESOLDSHIPMENTSREQUEST.CUSTOMERNAME.MISSING.PROPERTY";

        DeleteSoldShipmentRequest deleteSoldShipmentRequest = createMockDeleteSoldShipmentRequest();
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .setHeaderDetails(null);
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .setLocationDetails(null);
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .setTrackingId(null);
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .setRetailTransactionId(null);
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .setCustomerName(null);

        when(retailShipmentCreationProcessor.deleteSoldShipments(any(DeleteSoldShipmentRequest.class)))
                .thenReturn(new DeletedSoldShipmentResponse());

        mvc.perform(
                post(DELETE_SOLD_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(deleteSoldShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedHeaderDetailsMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedLocationDetailsMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedTrackingIdMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedRetailTransactionIdMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedCustomerNameErrorCode)))
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));

    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#deleteSoldShipments(deleteSoldShipmentRequestData)} with
     * HeaderDetails > requestDateTime and WorkStationDetails and Address > countryCode as null and then
     * error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"deletesoldshipments.post"})
    public void testDeleteSoldShipmentsWDRequestDateTimeCountryCodeNull()
            throws Exception {

        final String expectedRequestDateTimeMissingErrorCode = "HEADERDETAILS.REQUESTDATETIME.MISSING.PROPERTY";
        final String expectedWorkStationDetailsMissingErrorCode = "HEADERDETAILS.WORKSTATIONDETAILS.MISSING.PROPERTY";
        final String expectedAddressCountryCodeMissingErrorCode = "ADDRESS.COUNTRYCODE.MISSING.PROPERTY";

        DeleteSoldShipmentRequest deleteSoldShipmentRequest = createMockDeleteSoldShipmentRequest();
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .getHeaderDetails()
                .setRequestDateTime(null);
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .getHeaderDetails()
                .setWorkstationDetails(null);
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .getLocationDetails()
                .getAddress()
                .setCountryCode(null);

        when(retailShipmentCreationProcessor.deleteSoldShipments(any(DeleteSoldShipmentRequest.class)))
                .thenReturn(new DeletedSoldShipmentResponse());

        mvc.perform(
                post(DELETE_SOLD_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(deleteSoldShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedRequestDateTimeMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedWorkStationDetailsMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCountryCodeMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(3)));

    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#deleteSoldShipments(DeleteSoldShipmentRequestData)} with
     * locationDetails > code, address as null and then error response is returned and handled
     * successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"deletesoldshipments.post"})
    public void testDeleteSoldShipmentsLocDetailsElementsNull()
            throws Exception {

        final String expectedLocDetailsCodeMissingErrorCode = "LOCATIONDETAILS.CODE.MISSING.PROPERTY";
        final String expectedLocDetailsAddressMissingErrorCode = "LOCATIONDETAILS.ADDRESS.MISSING.PROPERTY";

        DeleteSoldShipmentRequest deleteSoldShipmentRequest = createMockDeleteSoldShipmentRequest();
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .getLocationDetails()
                .setCode(null);
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .getLocationDetails()
                .setAddress(null);

        when(retailShipmentCreationProcessor.deleteSoldShipments(any(DeleteSoldShipmentRequest.class)))
                .thenReturn(new DeletedSoldShipmentResponse());

        mvc.perform(
                post(DELETE_SOLD_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(deleteSoldShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedLocDetailsCodeMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedLocDetailsAddressMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(2)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * Recipient elements as exceeding maximum length and then error response is returned and
     * handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentRecipientPartyElementExceedMaxSize()
            throws Exception {

        final String expectedAddressStreetlinesExceedErrorCode = "ADDRESS.STREETLINES.INVALID.LENGTH";
        final String expectedAddressCityExceedErrorCode = "ADDRESS.CITY.INVALID.LENGTH";
        final String expectedAddressStateOrProvinceCodeExceedErrorCode = "ADDRESS.STATEORPROVINCECODE.INVALID.LENGTH";
        final String expectedAddressPostalCodeExceedErrorCode = "ADDRESS.POSTALCODE.INVALID.LENGTH";
        final String expectedAddressCountryCodeExceedErrorCode = "ADDRESS.COUNTRYCODE.INVALID.LENGTH";
        final String expectedShippingContactPersonNameExceedErrorCode = "CONTACT.PERSONNAME.INVALID.LENGTH";
        final String expectedShippingContactCompanyExceedErrorCode = "COMPANY.NAME.INVALID.LENGTH";
        final String expectedShippingContactPhoneNumberExceedErrorCode = "PHONENUMBER.NUMBER.INVALID.LENGTH";
        final String expectedShippingContactPhoneExtensionExceedErrorCode = "PHONENUMBER.EXTENSION.INVALID.LENGTH";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");
        streetlines.add("H.NO 4");
        Company name = new Company();
        name.setName(
                "company name exceed 80 characters- Infogain private limited, A-16, Block A, Sector 60 Noida Utter Pradesh 201301, Noida");
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("311255678912");
        phoneNumber.setExtension("1234567");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);

        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .setStreetLines(streetlines);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallasDallas");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .setPostalCode("12345678901");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .setStateOrProvinceCode("TXV");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .setCountryCode("USAUSAUSAUSA");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getContact()
                .setPersonName("Hubert Blaine Wolfeschelegelsteinhausenbergerdorff");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getContact()
                .setCompany(name);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getContact()
                .setPhoneNumberDetails(phoneNumberDetails);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStreetlinesExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCityExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStateOrProvinceCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressPostalCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCountryCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactPersonNameExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactCompanyExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactPhoneNumberExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactPhoneExtensionExceedErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(9)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * Recipient elements as less than minimum length and then error response is returned and
     * handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentRecipientPartyElementLessThanMinSize()
            throws Exception {

        final String expectedShippingContactPhoneNumberErrorCode = "PHONENUMBER.NUMBER.INVALID.LENGTH";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("31125567");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);

        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getContact()
                .setPhoneNumberDetails(phoneNumberDetails);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactPhoneNumberErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(1)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * Recipient elements as boundary maximum length and then successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentRecipientPartyElementBoundryMaxSize()
            throws Exception {

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");
        Company name = new Company();
        name.setName("company name exact 80 characters- Infogain private limited Sector 60 Noida UP...");
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("3112556789");
        phoneNumber.setExtension("12345");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);

        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .setStreetLines(streetlines);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallas..");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .setPostalCode("1234567890");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .setStateOrProvinceCode("TX");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress()
                .setCountryCode("USAUSAUSA.");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getContact()
                .setPersonName("name exact 35 characters - Nancy...");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getContact()
                .setCompany(name);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getContact()
                .setPhoneNumberDetails(phoneNumberDetails);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * Recipient elements as boundary minimum length and then successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentRecipientPartyElementBoundryMinSize()
            throws Exception {

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("3112556789");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);

        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getContact()
                .setPhoneNumberDetails(phoneNumberDetails);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * Shipper elements as exceeding maximum length and then error response is returned and handled
     * successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentShipperPartyElementExceedMaxSize()
            throws Exception {

        final String expectedAddressStreetlinesExceedErrorCode = "ADDRESS.STREETLINES.INVALID.LENGTH";
        final String expectedAddressCityExceedErrorCode = "ADDRESS.CITY.INVALID.LENGTH";
        final String expectedAddressStateOrProvinceCodeExceedErrorCode = "ADDRESS.STATEORPROVINCECODE.INVALID.LENGTH";
        final String expectedAddressPostalCodeExceedErrorCode = "ADDRESS.POSTALCODE.INVALID.LENGTH";
        final String expectedAddressCountryCodeExceedErrorCode = "ADDRESS.COUNTRYCODE.INVALID.LENGTH";
        final String expectedShippingContactPersonNameExceedErrorCode = "CONTACT.PERSONNAME.INVALID.LENGTH";
        final String expectedShippingContactCompanyExceedErrorCode = "COMPANY.NAME.INVALID.LENGTH";
        final String expectedShippingContactPhoneNumberExceedErrorCode = "PHONENUMBER.NUMBER.INVALID.LENGTH";
        final String expectedShippingContactPhoneExtensionExceedErrorCode = "PHONENUMBER.EXTENSION.INVALID.LENGTH";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");
        streetlines.add("H.NO 4");
        Company name = new Company();
        name.setName(
                "company name exceed 80 characters- Infogain private limited, A-16, Block A, Sector 60 Noida Utter Pradesh 201301, Noida");
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("311255678912");
        phoneNumber.setExtension("1234567");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);

        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .setStreetLines(streetlines);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallasDallas");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .setPostalCode("12345678901");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .setStateOrProvinceCode("TXV");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .setCountryCode("USAUSAUSAUSA");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getContact()
                .setPersonName("Hubert Blaine Wolfeschelegelsteinhausenbergerdorff");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getContact()
                .setCompany(name);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getContact()
                .setPhoneNumberDetails(phoneNumberDetails);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStreetlinesExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCityExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStateOrProvinceCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressPostalCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCountryCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactPersonNameExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactCompanyExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactPhoneNumberExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactPhoneExtensionExceedErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(9)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * Shipper elements as less than minimum length and then error response is returned and handled
     * successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentShipperPartyElementLessThanMinSize()
            throws Exception {

        final String expectedShippingContactPhoneNumberErrorCode = "PHONENUMBER.NUMBER.INVALID.LENGTH";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("31125567");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);

        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getContact()
                .setPhoneNumberDetails(phoneNumberDetails);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactPhoneNumberErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(1)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * Shipper elements as boundary maximum length and then successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentShipperPartyElementBoundryMaxSize()
            throws Exception {

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");
        Company name = new Company();
        name.setName("company name exact 80 characters- Infogain private limited Sector 60 Noida UP...");
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("3112556789");
        phoneNumber.setExtension("12345");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);

        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .setStreetLines(streetlines);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallas..");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .setPostalCode("1234567890");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .setStateOrProvinceCode("TX");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getAddress()
                .setCountryCode("USAUSAUSA.");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getContact()
                .setPersonName("name exact 35 characters - Nancy...");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getContact()
                .setCompany(name);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getContact()
                .setPhoneNumberDetails(phoneNumberDetails);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * Shipper elements as boundary minimum length and then successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentShipperPartyElementBoundryMinSize()
            throws Exception {

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("3112556789");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);

        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getContact()
                .setPhoneNumberDetails(phoneNumberDetails);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipments(ConfirmShipmentRequest)} with
     * streetLines,city,stateOrProvinceCode,postalCode and countryCode of TransactionLocationInfo as
     * exceeding maximum length and then error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"confirmedshipments.post"})
    public void testConfirmShipmentTransactionLocationInfoAddressExceedMaxSize()
            throws Exception {

        final String expectedAddressStreetlinesExceedErrorCode = "ADDRESS.STREETLINES.INVALID.LENGTH";
        final String expectedAddressCityExceedErrorCode = "ADDRESS.CITY.INVALID.LENGTH";
        final String expectedAddressStateOrProvinceCodeExceedErrorCode = "ADDRESS.STATEORPROVINCECODE.INVALID.LENGTH";
        final String expectedAddressPostalCodeExceedErrorCode = "ADDRESS.POSTALCODE.INVALID.LENGTH";
        final String expectedAddressCountryCodeExceedErrorCode = "ADDRESS.COUNTRYCODE.INVALID.LENGTH";

        ConfirmedShipmentRequest confirmedShipmentRequest = createMockConfirmedShipmentRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");
        streetlines.add("H.NO 4");

        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setStreetLines(streetlines);
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallasDallas");
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setPostalCode("12345678901");
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setStateOrProvinceCode("TXV");
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setCountryCode("USAUSAUSAUSA");

        when(retailShipmentCreationProcessor.confirmShipments(any(ConfirmedShipmentRequest.class)))
                .thenReturn(new ConfirmedShipmentResponse());

        mvc.perform(post(CONFIRM_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(confirmedShipmentRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStreetlinesExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCityExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStateOrProvinceCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressPostalCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCountryCodeExceedErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(5)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipments(ConfirmShipmentRequest)} with
     * streetLines,city,stateOrProvinceCode,postalCode and countryCode of TransactionLocationInfo as
     * boundary maximum length and then successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"confirmedshipments.post"})
    public void testConfirmShipmentTransactionLocationInfoAddressBoundryMaxSize()
            throws Exception {

        ConfirmedShipmentRequest confirmedShipmentRequest = createMockConfirmedShipmentRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");

        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setStreetLines(streetlines);
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallas..");
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setPostalCode("1234567890");
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setStateOrProvinceCode("TX");
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setCountryCode("USAUSAUSA.");

        when(retailShipmentCreationProcessor.confirmShipments(any(ConfirmedShipmentRequest.class)))
                .thenReturn(new ConfirmedShipmentResponse());

        mvc.perform(post(CONFIRM_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(confirmedShipmentRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    public void testShipmentCheckoutTransactionLocationInfoAddressExceedMaxSize()
            throws Exception {

        final String expectedAddressStreetlinesExceedErrorCode = "ADDRESS.STREETLINES.INVALID.LENGTH";
        final String expectedAddressCityExceedErrorCode = "ADDRESS.CITY.INVALID.LENGTH";
        final String expectedAddressStateOrProvinceCodeExceedErrorCode = "ADDRESS.STATEORPROVINCECODE.INVALID.LENGTH";
        final String expectedAddressPostalCodeExceedErrorCode = "ADDRESS.POSTALCODE.INVALID.LENGTH";
        final String expectedAddressCountryCodeExceedErrorCode = "ADDRESS.COUNTRYCODE.INVALID.LENGTH";

        ShipmentCheckoutRequest shipmentCheckoutRequest = createMockShipmentCheckoutRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");
        streetlines.add("H.NO 4");

        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setStreetLines(streetlines);
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallasDallas");
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setPostalCode("12345678901");
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setStateOrProvinceCode("TXV");
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setCountryCode("USAUSAUSAUSA");

        when(retailShipmentCreationProcessor.checkoutShipments(any(ShipmentCheckoutRequest.class)))
                .thenReturn(new ShipmentCheckoutResponse());

        mvc.perform(post(SHIPMENT_CHECKOUTS_ENDPOINT_URI).content(mapper.writeValueAsString(shipmentCheckoutRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStreetlinesExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCityExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStateOrProvinceCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressPostalCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCountryCodeExceedErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(5)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipmentsCheckout(ShipmentCheckoutRequest)}
     * with streetLines,city,stateOrProvinceCode,postalCode and countryCode of
     * TransactionLocationInfo as boundary maximum length and then successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"shipmentcheckouts.post"})
    public void testShipmentCheckoutTransactionLocationInfoAddressBoundryMaxSize()
            throws Exception {

        ShipmentCheckoutRequest shipmentCheckoutRequest = createMockShipmentCheckoutRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");

        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setStreetLines(streetlines);
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallas..");
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setPostalCode("1234567890");
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setStateOrProvinceCode("TX");
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setCountryCode("USAUSAUSA.");

        when(retailShipmentCreationProcessor.checkoutShipments(any(ShipmentCheckoutRequest.class)))
                .thenReturn(new ShipmentCheckoutResponse());

        mvc.perform(post(SHIPMENT_CHECKOUTS_ENDPOINT_URI).content(mapper.writeValueAsString(shipmentCheckoutRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * streetLines,city,stateOrProvinceCode,postalCode and countryCode of TransactionLocationInfo as
     * exceeding maximum length and then error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentTransactionLocationInfoAddressExceedMaxSize()
            throws Exception {

        final String expectedAddressStreetlinesExceedErrorCode = "ADDRESS.STREETLINES.INVALID.LENGTH";
        final String expectedAddressCityExceedErrorCode = "ADDRESS.CITY.INVALID.LENGTH";
        final String expectedAddressStateOrProvinceCodeExceedErrorCode = "ADDRESS.STATEORPROVINCECODE.INVALID.LENGTH";
        final String expectedAddressPostalCodeExceedErrorCode = "ADDRESS.POSTALCODE.INVALID.LENGTH";
        final String expectedAddressCountryCodeExceedErrorCode = "ADDRESS.COUNTRYCODE.INVALID.LENGTH";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");
        streetlines.add("H.NO 4");
        Company name = new Company();
        name.setName(
                "company name exceed 80 characters- Infogain private limited, A-16, Block A, Sector 60 Noida Utter Pradesh 201301, Noida");
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("311255678912");
        phoneNumber.setExtension("1234567");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);

        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setStreetLines(streetlines);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallasDallas");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setPostalCode("12345678901");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setStateOrProvinceCode("TXV");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setCountryCode("USAUSAUSAUSA");

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStreetlinesExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCityExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStateOrProvinceCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressPostalCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCountryCodeExceedErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(5)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * streetLines,city,stateOrProvinceCode,postalCode and countryCode of TransactionLocationInfo as
     * boundary maximum length and then successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentTransactionLocationInfoAddressBoundryMaxSize()
            throws Exception {

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");

        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setStreetLines(streetlines);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallas..");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setPostalCode("1234567890");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setStateOrProvinceCode("TX");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getLocationDetails()
                .getTransactionLocationInfo()
                .getAddress()
                .setCountryCode("USAUSAUSA.");

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * Customer elements as exceeding maximum length and then error response is returned and handled
     * successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentCustomerPartyElementExceedMaxSize()
            throws Exception {

        final String expectedAddressStreetlinesExceedErrorCode = "ADDRESS.STREETLINES.INVALID.LENGTH";
        final String expectedAddressCityExceedErrorCode = "ADDRESS.CITY.INVALID.LENGTH";
        final String expectedAddressStateOrProvinceCodeExceedErrorCode = "ADDRESS.STATEORPROVINCECODE.INVALID.LENGTH";
        final String expectedAddressPostalCodeExceedErrorCode = "ADDRESS.POSTALCODE.INVALID.LENGTH";
        final String expectedAddressCountryCodeExceedErrorCode = "ADDRESS.COUNTRYCODE.INVALID.LENGTH";
        final String expectedShippingContactPersonNameExceedErrorCode = "CONTACT.PERSONNAME.INVALID.LENGTH";
        final String expectedShippingContactCompanyExceedErrorCode = "COMPANY.NAME.INVALID.LENGTH";
        final String expectedShippingContactPhoneNumberExceedErrorCode = "PHONENUMBER.NUMBER.INVALID.LENGTH";
        final String expectedShippingContactPhoneExtensionExceedErrorCode = "PHONENUMBER.EXTENSION.INVALID.LENGTH";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");
        streetlines.add("H.NO 4");
        Company name = new Company();
        name.setName(
                "company name exceed 80 characters- Infogain private limited, A-16, Block A, Sector 60 Noida Utter Pradesh 201301, Noida");
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("311255678912");
        phoneNumber.setExtension("1234567");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);

        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress()
                .setStreetLines(streetlines);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallasDallas");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress()
                .setPostalCode("12345678901");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress()
                .setStateOrProvinceCode("TXV");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress()
                .setCountryCode("USAUSAUSAUSA");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getContact()
                .setPersonName("Hubert Blaine Wolfeschelegelsteinhausenbergerdorff");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getContact()
                .setCompany(name);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getContact()
                .setPhoneNumberDetails(phoneNumberDetails);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStreetlinesExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCityExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStateOrProvinceCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressPostalCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCountryCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactPersonNameExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactCompanyExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactPhoneNumberExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactPhoneExtensionExceedErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(9)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * Customer elements as less than minimum length and then error response is returned and handled
     * successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentCustomerPartyElementLessThanMinSize()
            throws Exception {

        final String expectedShippingContactPhoneNumberErrorCode = "PHONENUMBER.NUMBER.INVALID.LENGTH";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("31125567");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);

        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getContact()
                .setPhoneNumberDetails(phoneNumberDetails);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactPhoneNumberErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(1)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * Customer elements as boundary maximum length and then successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentCustomerPartyElementBoundryMaxSize()
            throws Exception {

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");
        Company name = new Company();
        name.setName("company name exact 80 characters- Infogain private limited Sector 60 Noida UP...");
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("3112556789");
        phoneNumber.setExtension("12345");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);

        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress()
                .setStreetLines(streetlines);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallas..");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress()
                .setPostalCode("1234567890");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress()
                .setStateOrProvinceCode("TX");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress()
                .setCountryCode("USAUSAUSA.");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getContact()
                .setPersonName("name exact 35 characters - Nancy...");
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getContact()
                .setCompany(name);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getContact()
                .setPhoneNumberDetails(phoneNumberDetails);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * Customer elements as boundary minimum length and then successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentCustomerPartyElementBoundryMinSize()
            throws Exception {

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("3112556789");
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);

        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getContact()
                .setPhoneNumberDetails(phoneNumberDetails);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipments(ConfirmShipmentRequest)} with
     * streetLines,city,stateOrProvinceCode,postalCode and countryCode of Payor as exceeding maximum
     * length and then error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"confirmedshipments.post"})
    public void testConfirmShipmentPayorAddressExceedMaxSize()
            throws Exception {

        final String expectedAddressStreetlinesExceedErrorCode = "ADDRESS.STREETLINES.INVALID.LENGTH";
        final String expectedAddressCityExceedErrorCode = "ADDRESS.CITY.INVALID.LENGTH";
        final String expectedAddressStateOrProvinceCodeExceedErrorCode = "ADDRESS.STATEORPROVINCECODE.INVALID.LENGTH";
        final String expectedAddressPostalCodeExceedErrorCode = "ADDRESS.POSTALCODE.INVALID.LENGTH";
        final String expectedAddressCountryCodeExceedErrorCode = "ADDRESS.COUNTRYCODE.INVALID.LENGTH";

        ConfirmedShipmentRequest confirmedShipmentRequest = createMockConfirmedShipmentRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");
        streetlines.add("H.NO 4");

        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setStreetLines(streetlines);
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallasDallas");
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setPostalCode("12345678901");
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setStateOrProvinceCode("TXV");
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setCountryCode("USAUSAUSAUSA");

        when(retailShipmentCreationProcessor.confirmShipments(any(ConfirmedShipmentRequest.class)))
                .thenReturn(new ConfirmedShipmentResponse());

        mvc.perform(post(CONFIRM_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(confirmedShipmentRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStreetlinesExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCityExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStateOrProvinceCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressPostalCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCountryCodeExceedErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(5)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipments(ConfirmShipmentRequest)} with
     * streetLines,city,stateOrProvinceCode,postalCode and countryCode of Payor as boundary maximum
     * length and then successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"confirmedshipments.post"})
    public void testConfirmShipmentPayorAddressBoundryMaxSize()
            throws Exception {

        ConfirmedShipmentRequest confirmedShipmentRequest = createMockConfirmedShipmentRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");

        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setStreetLines(streetlines);
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallas..");
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setPostalCode("1234567890");
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setStateOrProvinceCode("TX");
        confirmedShipmentRequest.getConfirmedShipmentRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setCountryCode("USAUSAUSA.");

        when(retailShipmentCreationProcessor.confirmShipments(any(ConfirmedShipmentRequest.class)))
                .thenReturn(new ConfirmedShipmentResponse());

        mvc.perform(post(CONFIRM_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(confirmedShipmentRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipmentsCheckout(ShipmentCheckoutRequest)}
     * with streetLines,city,stateOrProvinceCode,postalCode and countryCode of Payor as exceeding
     * maximum length and then error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"shipmentcheckouts.post"})
    public void testShipmentCheckoutPayorAddressExceedMaxSize()
            throws Exception {

        final String expectedAddressStreetlinesExceedErrorCode = "ADDRESS.STREETLINES.INVALID.LENGTH";
        final String expectedAddressCityExceedErrorCode = "ADDRESS.CITY.INVALID.LENGTH";
        final String expectedAddressStateOrProvinceCodeExceedErrorCode = "ADDRESS.STATEORPROVINCECODE.INVALID.LENGTH";
        final String expectedAddressPostalCodeExceedErrorCode = "ADDRESS.POSTALCODE.INVALID.LENGTH";
        final String expectedAddressCountryCodeExceedErrorCode = "ADDRESS.COUNTRYCODE.INVALID.LENGTH";

        ShipmentCheckoutRequest shipmentCheckoutRequest = createMockShipmentCheckoutRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");
        streetlines.add("H.NO 4");

        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setStreetLines(streetlines);
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallasDallas");
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setPostalCode("12345678901");
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setStateOrProvinceCode("TXV");
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setCountryCode("USAUSAUSAUSA");

        when(retailShipmentCreationProcessor.checkoutShipments(any(ShipmentCheckoutRequest.class)))
                .thenReturn(new ShipmentCheckoutResponse());

        mvc.perform(post(SHIPMENT_CHECKOUTS_ENDPOINT_URI).content(mapper.writeValueAsString(shipmentCheckoutRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStreetlinesExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCityExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStateOrProvinceCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressPostalCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCountryCodeExceedErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(5)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#confirmShipmentsCheckout(ShipmentCheckoutRequest)}
     * with streetLines,city,stateOrProvinceCode,postalCode and countryCode of Payor as boundary
     * maximum length and then successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"shipmentcheckouts.post"})
    public void testShipmentCheckoutPayorAddressBoundryMaxSize()
            throws Exception {

        ShipmentCheckoutRequest shipmentCheckoutRequest = createMockShipmentCheckoutRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");

        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setStreetLines(streetlines);
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallas..");
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setPostalCode("1234567890");
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setStateOrProvinceCode("TX");
        shipmentCheckoutRequest.getShipmentCheckoutRequest()
                .getPaymentDetails()
                .getPayor()
                .getAddress()
                .setCountryCode("USAUSAUSA.");

        when(retailShipmentCreationProcessor.checkoutShipments(any(ShipmentCheckoutRequest.class)))
                .thenReturn(new ShipmentCheckoutResponse());

        mvc.perform(post(SHIPMENT_CHECKOUTS_ENDPOINT_URI).content(mapper.writeValueAsString(shipmentCheckoutRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#deleteSoldShipments(DeleteSoldShipmentRequest)} with
     * streetLines,city,stateOrProvinceCode,postalCode and countryCode of LocationInfo as exceeding
     * maximum length and then error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"deletesoldshipments.post"})
    public void testDeleteSoldShipmentsTransactionLocationInfoAddressExceedMaxSize()
            throws Exception {

        final String expectedAddressStreetlinesExceedErrorCode = "ADDRESS.STREETLINES.INVALID.LENGTH";
        final String expectedAddressCityExceedErrorCode = "ADDRESS.CITY.INVALID.LENGTH";
        final String expectedAddressStateOrProvinceCodeExceedErrorCode = "ADDRESS.STATEORPROVINCECODE.INVALID.LENGTH";
        final String expectedAddressPostalCodeExceedErrorCode = "ADDRESS.POSTALCODE.INVALID.LENGTH";
        final String expectedAddressCountryCodeExceedErrorCode = "ADDRESS.COUNTRYCODE.INVALID.LENGTH";

        DeleteSoldShipmentRequest deleteSoldShipmentRequest = createMockDeleteSoldShipmentRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");
        streetlines.add("H.NO 4");

        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .getLocationDetails()
                .getAddress()
                .setStreetLines(streetlines);
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .getLocationDetails()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallasDallas");
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .getLocationDetails()
                .getAddress()
                .setPostalCode("12345678901");
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .getLocationDetails()
                .getAddress()
                .setStateOrProvinceCode("TXV");
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .getLocationDetails()
                .getAddress()
                .setCountryCode("USAUSAUSAUSA");

        when(retailShipmentCreationProcessor.deleteSoldShipments(any(DeleteSoldShipmentRequest.class)))
                .thenReturn(new DeletedSoldShipmentResponse());

        mvc.perform(
                post(DELETE_SOLD_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(deleteSoldShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStreetlinesExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCityExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressStateOrProvinceCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressPostalCodeExceedErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedAddressCountryCodeExceedErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(5)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#deleteSoldShipments(DeleteSoldShipmentRequest)} with
     * streetLines,city,stateOrProvinceCode,postalCode and countryCode of LocationInfo as boundary
     * maximum length and then successful response is returned
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"deletesoldshipments.post"})
    public void testDeleteSoldShipmentsTransactionLocationInfoAddressBoundryMaxSize()
            throws Exception {

        DeleteSoldShipmentRequest deleteSoldShipmentRequest = createMockDeleteSoldShipmentRequest();
        List<String> streetlines = new ArrayList<>();
        streetlines.add("H.NO 1");
        streetlines.add("H.NO 2");
        streetlines.add("H.NO 3");

        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .getLocationDetails()
                .getAddress()
                .setStreetLines(streetlines);
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .getLocationDetails()
                .getAddress()
                .setCity("DallasDallasDallasDallasDallasDallasDallasDallas..");
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .getLocationDetails()
                .getAddress()
                .setPostalCode("1234567890");
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .getLocationDetails()
                .getAddress()
                .setStateOrProvinceCode("TX");
        deleteSoldShipmentRequest.getDeleteSoldShipmentsRequest()
                .getLocationDetails()
                .getAddress()
                .setCountryCode("USAUSAUSA.");

        when(retailShipmentCreationProcessor.deleteSoldShipments(any(DeleteSoldShipmentRequest.class)))
                .thenReturn(new DeletedSoldShipmentResponse());

        mvc.perform(
                post(DELETE_SOLD_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(deleteSoldShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * Customer, Shipper, homeDeliveryDetail elements as invalid value of phone number and then
     * error response is returned and handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentPhoneNumberElementInvalidValue()
            throws Exception {

        final String expectedShippingContactPhoneNumberErrorCode = "PHONENUMBER.NUMBER.INVALID.VALUE";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();

        ShipmentSpecialServiceDescription shipmentSpecialServiceDescription = new ShipmentSpecialServiceDescription();
        HomeDeliveryDetail homeDeliveryDetail = new HomeDeliveryDetail();
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("995-5454a1");
        homeDeliveryDetail.setHomeDeliveryType(HomeDeliveryType.APPOINTMENT);
        homeDeliveryDetail.setPhoneNumber(phoneNumber);
        shipmentSpecialServiceDescription.setHomeDeliveryDetail(homeDeliveryDetail);

        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        phoneNumberDetails.add(phoneNumberDetail);

        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getContact()
                .setPhoneNumberDetails(phoneNumberDetails);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper()
                .getContact()
                .setPhoneNumberDetails(phoneNumberDetails);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .setSpecialServicesRequested(shipmentSpecialServiceDescription);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactPhoneNumberErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactPhoneNumberErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedShippingContactPhoneNumberErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(3)));
    }

    /**
     * Method sets the home delivery details
     * 
     * @return shipmentSpecialServiceDescription
     */
    private ShipmentSpecialServiceDescription createSpecialServiceWithHomeDeliveryTypeNull() {

        ShipmentSpecialServiceDescription shipmentSpecialServiceDescription = new ShipmentSpecialServiceDescription();
        HomeDeliveryDetail homeDeliveryDetail = new HomeDeliveryDetail();
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("4699877966");
        homeDeliveryDetail.setHomeDeliveryType(null);
        homeDeliveryDetail.setPhoneNumber(phoneNumber);
        shipmentSpecialServiceDescription.setHomeDeliveryDetail(homeDeliveryDetail);
        return shipmentSpecialServiceDescription;
    }
    
    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * InterlineShipmentDetails is null and then success response is handled successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentsWhenInterlineShipmentDetailNull()
            throws Exception {

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .setInterlineShippingDetails(null);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isOk())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)));
    }

    /**
     * Test case to verify that when request is made to
     * {@link RetailShipmentCreationController#createOpenShipments(CreateOpenShipmentRequest)} with
     * interlineId and employeeId are null and then error response is returned and handled
     * successfully
     */
    @Test
    @WithMockOAuth2Scope(authorities = {"openshipments.post"})
    public void testCreateOpenShipmentsWhenInterlineShippingDetailElementsNull()
            throws Exception {

        final String expectedInterlineIdMissingErrorCode = "INTERLINESHIPPINGDETAILS.INTERLINEID.MISSING.PROPERTY";
        final String expectedEmployeeIdMissingErrorCode = "INTERLINESHIPPINGDETAILS.EMPLOYEEID.MISSING.PROPERTY";

        CreateOpenShipmentRequest createOpenShipmentRequest = createMockCreateOpenShipmentRequest();
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getInterlineShippingDetails()
                .setInterlineId(null);
        createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getInterlineShippingDetails()
                .setEmployeeId(null);

        when(retailShipmentCreationProcessor.createOpenShipment(any(CreateOpenShipmentRequest.class)))
                .thenReturn(new CreateOpenShipmentResponse());

        mvc.perform(
                post(CREATE_OPEN_SHIPMENTS_ENDPOINT_URI).content(mapper.writeValueAsString(createOpenShipmentRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_TXN_ID, FEDEX_2020))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(TXN_ID).value(equalTo(FEDEX_2020)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedInterlineIdMissingErrorCode)))
                .andExpect(jsonPath(ERROR_CODE, hasItem(expectedEmployeeIdMissingErrorCode)))
                .andExpect(jsonPath(ERRORS, hasSize(2)));
    }

    /**
     * Method to build the mock instance of {@link InterlineShippingDetail} object
     * 
     * @return
     */
    private InterlineShippingDetail createMockInterlineShippingDetail() {

        InterlineShippingDetail interlineShippingDetail = new InterlineShippingDetail();
        interlineShippingDetail.setInterlineId("100127");
        interlineShippingDetail.setInterlineCode("G4");
        interlineShippingDetail.setInterlineName("ALLEGIANT AIRLINES");
        interlineShippingDetail.setInterlineNumber("008");
        interlineShippingDetail.setEmployeeId("3932");

        return interlineShippingDetail;
    }
}
