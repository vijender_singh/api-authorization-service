package com.fedex.rscs.model;

import com.fedex.rscs.model.common.LocationDataDetail;

/**
 * ConfirmShipmentRequest model specification
 * 
 * @author 5034922
 *
 */
public class ShipmentConfirmationRequest {

    private String referenceId;
    private String shipmentTimeStamp;
    private WorkstationDetail workstationDetail;
    private String transactionLocationId;
    private String trackingId;
    private LabelSpecificationDetail labelSpecificationDetail;
    private PaymentDetail paymentDetail;
    private LocationDataDetail locationDataDetail;

    public ShipmentConfirmationRequest() {}

    /**
     * @param referenceId
     * @param date
     * @param workstationDetail
     * @param labelSpecificationDetail
     */
    public ShipmentConfirmationRequest(String referenceId, String shipmentTimeStamp,
            WorkstationDetail workstationDetail, LabelSpecificationDetail labelSpecificationDetail,
            PaymentDetail paymentDetail, LocationDataDetail locationDataDetail) {

        this.referenceId = referenceId;
        this.shipmentTimeStamp = shipmentTimeStamp;
        this.workstationDetail = workstationDetail;
        this.labelSpecificationDetail = labelSpecificationDetail;
        this.paymentDetail = paymentDetail;
        this.locationDataDetail = locationDataDetail;
    }

    /**
     * @return the referenceId
     */
    public String getReferenceId() {

        return referenceId;
    }

    /**
     * @param referenceId the referenceId to set
     */
    public void setReferenceId(
            String referenceId) {

        this.referenceId = referenceId;
    }


    /**
     * @return the workstationDetail
     */
    public WorkstationDetail getWorkstationDetail() {

        return workstationDetail;
    }

    /**
     * @param workstationDetail the workstationDetail to set
     */
    public void setWorkstationDetail(
            WorkstationDetail workstationDetail) {

        this.workstationDetail = workstationDetail;
    }

    /**
     * @return the labelSpecificationDetail
     */
    public LabelSpecificationDetail getLabelSpecificationDetail() {

        return labelSpecificationDetail;
    }

    /**
     * @param labelSpecificationDetail the labelSpecificationDetail to set
     */
    public void setLabelSpecificationDetail(
            LabelSpecificationDetail labelSpecificationDetail) {

        this.labelSpecificationDetail = labelSpecificationDetail;
    }


    /**
     * @return the shipmentTimeStamp
     */
    public String getShipmentTimeStamp() {

        return shipmentTimeStamp;
    }


    /**
     * @param shipmentTimeStamp the shipmentTimeStamp to set
     */
    public void setShipmentTimeStamp(
            String shipmentTimeStamp) {

        this.shipmentTimeStamp = shipmentTimeStamp;
    }


    /**
     * @return the transactionLocationId
     */
    public String getTransactionLocationId() {

        return transactionLocationId;
    }


    /**
     * @param transactionLocationId the transactionLocationId to set
     */
    public void setTransactionLocationId(
            String transactionLocationId) {

        this.transactionLocationId = transactionLocationId;
    }

    /**
     * @return the trackingId
     */
    public String getTrackingId() {

        return trackingId;
    }

    /**
     * @param trackingId the trackingId to set
     */
    public void setTrackingId(
            String trackingId) {

        this.trackingId = trackingId;
    }

    /**
     * @return the paymentDetal
     */
    public PaymentDetail getPaymentDetail() {

        return paymentDetail;
    }


    /**
     * @param paymentDetal the paymentDetal to set
     */
    public void setPaymentDetail(
            PaymentDetail paymentDetail) {

        this.paymentDetail = paymentDetail;
    }

    /**
     * gets location data details
     * 
     * @return locationDataDetail
     */
    public LocationDataDetail getLocationDataDetail() {

        return locationDataDetail;
    }


    /**
     * sets location data details
     * 
     * @param locationDataDetail
     */
    public void setLocationDataDetail(
            LocationDataDetail locationDataDetail) {
    
        this.locationDataDetail = locationDataDetail;
    }
    @Override
    public String toString() {

        return "ShipmentConfirmationRequest [referenceId=" + referenceId + ", shipmentTimeStamp=" + shipmentTimeStamp
                + ", workstationDetail=" + workstationDetail + ", transactionLocationId=" + transactionLocationId
                + ", trackingId=" + trackingId + ", labelSpecificationDetail=" + labelSpecificationDetail
                + ", paymentDetail=" + paymentDetail + ", locationDataDetail=" + locationDataDetail + "]";
    }

}
