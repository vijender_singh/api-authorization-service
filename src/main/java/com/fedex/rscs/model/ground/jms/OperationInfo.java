package com.fedex.rscs.model.ground.jms;

/**
 * Operation info that needs to be send to Ground system
 * 
 * @author Vijender.Singh
 *
 */
public class OperationInfo {

    private String trackID;
    private String shipDate;
    private String postalCode;
    private String rateZone;
    private String operation;

    /**
     * @return the trackID
     */
    public String getTrackID() {

        return trackID;
    }

    /**
     * @param trackID the trackID to set
     */
    public void setTrackID(
            String trackID) {

        this.trackID = trackID;
    }

    /**
     * @return the shipDate
     */
    public String getShipDate() {

        return shipDate;
    }

    /**
     * @param shipDate the shipDate to set
     */
    public void setShipDate(
            String shipDate) {

        this.shipDate = shipDate;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {

        return postalCode;
    }

    /**
     * @param postalCode the postalCode to set
     */
    public void setPostalCode(
            String postalCode) {

        this.postalCode = postalCode;
    }

    /**
     * @return the rateZone
     */
    public String getRateZone() {

        return rateZone;
    }

    /**
     * @param rateZone the rateZone to set
     */
    public void setRateZone(
            String rateZone) {

        this.rateZone = rateZone;
    }

    /**
     * @return the operation
     */
    public String getOperation() {

        return operation;
    }

    /**
     * @param operation the operation to set
     */
    public void setOperation(
            String operation) {

        this.operation = operation;
    }

    @Override
    public String toString() {

        return "OperationInfo [trackID=" + trackID + ", shipDate=" + shipDate + ", postalCode=" + postalCode
                + ", rateZone=" + rateZone + ", operation=" + operation + "]";
    }

}
