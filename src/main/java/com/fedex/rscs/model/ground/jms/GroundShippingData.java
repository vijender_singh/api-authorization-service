package com.fedex.rscs.model.ground.jms;

import com.fedex.rscs.model.common.PackagePreauthorizedRatedDetail;

/**
 * Model class to hold the ground shipping data
 * 
 * @author Vijender.Singh
 *
 */
public class GroundShippingData {

    private PrePaymentInfo prePaymentInfo;
    private OperationInfo operationInfo;
    private PackagePreauthorizedRatedDetail ratedPackages;

    public GroundShippingData() {}

    public GroundShippingData(PrePaymentInfo prePaymentInfo, OperationInfo operationInfo,
            PackagePreauthorizedRatedDetail ratedPackages) {

        this.prePaymentInfo = prePaymentInfo;
        this.operationInfo = operationInfo;
        this.ratedPackages = ratedPackages;
    }

    /**
     * @return the prePaymentInfo
     */
    public PrePaymentInfo getPrePaymentInfo() {

        return prePaymentInfo;
    }

    /**
     * @param prePaymentInfo the prePaymentInfo to set
     */
    public void setPrePaymentInfo(
            PrePaymentInfo prePaymentInfo) {

        this.prePaymentInfo = prePaymentInfo;
    }

    /**
     * @return the operationInfo
     */
    public OperationInfo getOperationInfo() {

        return operationInfo;
    }

    /**
     * @param operationInfo the operationInfo to set
     */
    public void setOperationInfo(
            OperationInfo operationInfo) {

        this.operationInfo = operationInfo;
    }


    /**
     * @return the ratedPackages
     */
    public PackagePreauthorizedRatedDetail getRatedPackages() {

        return ratedPackages;
    }


    /**
     * @param ratedPackages the ratedPackages to set
     */
    public void setRatedPackages(
            PackagePreauthorizedRatedDetail ratedPackages) {

        this.ratedPackages = ratedPackages;
    }

    @Override
    public String toString() {

        return "GroundShippingData [prePaymentInfo=" + prePaymentInfo + ", operationInfo=" + operationInfo
                + ", ratedPackages=" + ratedPackages + "]";
    }

}
