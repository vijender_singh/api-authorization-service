package com.fedex.rscs.model.ground.jms;

/**
 * Class to hold payment data for ground shipping
 * 
 * @author Vijender.Singh
 *
 */
public class PrePaymentInfo {

    private String mode;
    private String payMthdCode;
    private String shipperNumber;
    private String source;
    private String refid;
    private String maskedCCNbr;
    private String ccExpirationDate;
    private String ccType;

    /**
     * @return the mode
     */
    public String getMode() {

        return mode;
    }

    /**
     * @param mode the mode to set
     */
    public void setMode(
            String mode) {

        this.mode = mode;
    }

    /**
     * @return the payMthdCode
     */
    public String getPayMthdCode() {

        return payMthdCode;
    }

    /**
     * @param payMthdCode the payMthdCode to set
     */
    public void setPayMthdCode(
            String payMthdCode) {

        this.payMthdCode = payMthdCode;
    }

    /**
     * @return the shipperNumber
     */
    public String getShipperNumber() {

        return shipperNumber;
    }

    /**
     * @param shipperNumber the shipperNumber to set
     */
    public void setShipperNumber(
            String shipperNumber) {

        this.shipperNumber = shipperNumber;
    }

    /**
     * @return the source
     */
    public String getSource() {

        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(
            String source) {

        this.source = source;
    }

    /**
     * @return the refid
     */
    public String getRefid() {

        return refid;
    }

    /**
     * @param refid the refid to set
     */
    public void setRefid(
            String refid) {

        this.refid = refid;
    }

    /**
     * @return the maskedCCNbr
     */
    public String getMaskedCCNbr() {

        return maskedCCNbr;
    }

    /**
     * @param maskedCCNbr the maskedCCNbr to set
     */
    public void setMaskedCCNbr(
            String maskedCCNbr) {

        this.maskedCCNbr = maskedCCNbr;
    }

    /**
     * @return the ccExpirationDate
     */
    public String getCcExpirationDate() {

        return ccExpirationDate;
    }

    /**
     * @param ccExpirationDate the ccExpirationDate to set
     */
    public void setCcExpirationDate(
            String ccExpirationDate) {

        this.ccExpirationDate = ccExpirationDate;
    }

    /**
     * @return the ccType
     */
    public String getCcType() {

        return ccType;
    }

    /**
     * @param ccType the ccType to set
     */
    public void setCcType(
            String ccType) {

        this.ccType = ccType;
    }

    @Override
    public String toString() {

        return "PrePaymentInfo [mode=" + mode + ", payMthdCode=" + payMthdCode + ", shipperNumber=" + shipperNumber
                + ", source=" + source + ", refid=" + refid + ", maskedCCNbr=" + maskedCCNbr + ", ccExpirationDate="
                + ccExpirationDate + ", ccType=" + ccType + "]";
    }
}
