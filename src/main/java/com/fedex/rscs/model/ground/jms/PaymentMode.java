package com.fedex.rscs.model.ground.jms;

/**
 * Payment mode required for Ground shipping data
 * 
 * @author Vijender.Singh
 *
 */
public enum PaymentMode {

    CREDIT_CARD("CCA"), CASH("CSH"), CHECK("CHK");

    private String mode;

    private PaymentMode(String mode) {

        this.mode = mode;
    }

    /**
     * @return the mode
     */
    public String getMode() {

        return mode;
    }

}
