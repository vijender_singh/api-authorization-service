package com.fedex.rscs.model;

import com.fedex.rscs.model.common.LocationDataDetail;
import com.fedex.rscs.model.common.PersonDetail;

/**
 * This model class will be used for delete sold shipment request.
 * 
 * @author 3900094
 *
 */
public class DeleteSoldShipmentData {

    String shipmentTrackingNumber;
    String retailTransactionId;
    String locationId;
    String requestDateTime;
    WorkstationDetail workstationDetail;
    LocationDataDetail locationDetail;
    PersonDetail personDetail;

    public DeleteSoldShipmentData() {}

    /**
     * @param shipmentTrackingNumber
     * @param retailTransactionId
     * @param locationId
     * @param requestDateTime
     * @param workstationDetail
     * @param locationDetail
     * @param personDetail
     */
    public DeleteSoldShipmentData(String shipmentTrackingNumber, String retailTransactionId, String locationId,
            String requestDateTime, WorkstationDetail workstationDetail, LocationDataDetail locationDetail,
            PersonDetail personDetail) {

        this.shipmentTrackingNumber = shipmentTrackingNumber;
        this.retailTransactionId = retailTransactionId;
        this.locationId = locationId;
        this.requestDateTime = requestDateTime;
        this.workstationDetail = workstationDetail;
        this.locationDetail = locationDetail;
        this.personDetail = personDetail;
    }

    /**
     * @return the shipmentTrackingNumber
     */
    public String getShipmentTrackingNumber() {

        return shipmentTrackingNumber;
    }

    /**
     * @param shipmentTrackingNumber to set the shipmentTrackingNumber
     */
    public void setShipmentTrackingNumber(
            String shipmentTrackingNumber) {

        this.shipmentTrackingNumber = shipmentTrackingNumber;
    }

    /**
     * @return the retailTransactionId
     */
    public String getRetailTransactionId() {

        return retailTransactionId;
    }

    /**
     * @param retailTransactionId to set the retailTransactionId
     */
    public void setRetailTransactionId(
            String retailTransactionId) {

        this.retailTransactionId = retailTransactionId;
    }

    /**
     * @return the locationId
     */
    public String getLocationId() {

        return locationId;
    }

    /**
     * @param locationId to set the locationId
     */
    public void setLocationId(
            String locationId) {

        this.locationId = locationId;
    }

    /**
     * @return the requestDateTime
     */
    public String getRequestDateTime() {

        return requestDateTime;
    }

    /**
     * @param requestDateTime to set the requestDateTime
     */
    public void setRequestDateTime(
            String requestDateTime) {

        this.requestDateTime = requestDateTime;
    }

    /**
     * @return the workstationDetail
     */
    public WorkstationDetail getWorkstationDetail() {

        return workstationDetail;
    }

    /**
     * @param workstationDetail to set the workstationDetail
     */
    public void setWorkstationDetail(
            WorkstationDetail workstationDetail) {

        this.workstationDetail = workstationDetail;
    }

    /**
     * @return the locationDetail
     */
    public LocationDataDetail getLocationDetail() {

        return locationDetail;
    }

    /**
     * @param locationDetail to set the locationDetail
     */
    public void setLocationDetail(
            LocationDataDetail locationDetail) {

        this.locationDetail = locationDetail;
    }

    /**
     * @return the personDetail
     */
    public PersonDetail getPersonDetail() {

        return personDetail;
    }

    /**
     * @param personDetail to set the personDetail
     */
    public void setPersonDetail(
            PersonDetail personDetail) {

        this.personDetail = personDetail;
    }

    @Override
    public String toString() {

        return "DeleteSoldShipmentData [shipmentTrackingNumber=" + shipmentTrackingNumber + ", retailTransactionId="
                + retailTransactionId + ", locationId=" + locationId + ", requestDateTime=" + requestDateTime
                + ", workstationDetail=" + workstationDetail + ", locationDetail=" + locationDetail + ", personDetail="
                + personDetail + "]";
    }

}
