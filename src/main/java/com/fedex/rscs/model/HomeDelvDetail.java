package com.fedex.rscs.model;

import com.fedex.rscs.dto.HomeDeliveryType;
import com.fedex.rscs.dto.PhoneNumber;

/**
 * This is a model class to save the home delivery details
 * 
 * @author Vijender.Singh
 *
 */
public class HomeDelvDetail {

    private HomeDeliveryType homeDeliveryType;
    private String deliveryDate;
    private PhoneNumber phoneNumber;

    public HomeDelvDetail() {}

    /**
     * Constructor to set the fields
     * 
     * @param homeDeliveryType
     * @param deliveryDate
     * @param phoneNumber
     */
    public HomeDelvDetail(HomeDeliveryType homeDeliveryType, String deliveryDate, PhoneNumber phoneNumber) {

        this.homeDeliveryType = homeDeliveryType;
        this.deliveryDate = deliveryDate;
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the homeDeliveryType
     */
    public HomeDeliveryType getHomeDeliveryType() {

        return homeDeliveryType;
    }

    /**
     * @param homeDeliveryType the homeDeliveryType to set
     */
    public void setHomeDeliveryType(
            HomeDeliveryType homeDeliveryType) {

        this.homeDeliveryType = homeDeliveryType;
    }

    /**
     * @return the deliveryDate
     */
    public String getDeliveryDate() {

        return deliveryDate;
    }

    /**
     * @param deliveryDate the deliveryDate to set
     */
    public void setDeliveryDate(
            String deliveryDate) {

        this.deliveryDate = deliveryDate;
    }

    /**
     * @return the phoneNumber
     */
    public PhoneNumber getPhoneNumber() {

        return phoneNumber;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(
            PhoneNumber phoneNumber) {

        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {

        return "HomeDelvDetail [homeDeliveryType=" + homeDeliveryType + ", deliveryDate=" + deliveryDate + ", phoneNumber="
                + phoneNumber + "]";
    }
}
