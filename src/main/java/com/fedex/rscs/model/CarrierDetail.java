package com.fedex.rscs.model;

/**
 * Carrier Detail model for open shipment
 * 
 * @author Surbhi.Gupta
 *
 */
public class CarrierDetail {

    String carrierCode;
    String opco;

    public CarrierDetail() {}

    /**
     * @param carrierCode
     * @param opco
     */
    public CarrierDetail(String carrierCode, String opco) {

        this.carrierCode = carrierCode;
        this.opco = opco;
    }

    /**
     * @return the carrierCode
     */
    public String getCarrierCode() {

        return carrierCode;
    }

    /**
     * @return the opco
     */
    public String getOpco() {

        return opco;
    }

    /**
     * @param carrierCode the carrierCode to set
     */
    public void setCarrierCode(
            String carrierCode) {

        this.carrierCode = carrierCode;
    }

    /**
     * @param opco the opco to set
     */
    public void setOpco(
            String opco) {

        this.opco = opco;
    }

}
