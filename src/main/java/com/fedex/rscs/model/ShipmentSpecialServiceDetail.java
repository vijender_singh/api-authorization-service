package com.fedex.rscs.model;

import java.util.List;

/**
 * Define the Shipment Special Service Detail model.
 * 
 * @author 3900109
 *
 */
public class ShipmentSpecialServiceDetail {

    private List<SpecialServiceDescriptionDetail> specialServicesRequestedType;
    private HoldAtLocDetail holdAtLocationDetail;
    private HomeDelvDetail homeDeliveryDetail;

    public ShipmentSpecialServiceDetail() {}

    /**
     * @param specialServicesRequestedType
     * @param holdAtLocationDetail
     */
    public ShipmentSpecialServiceDetail(List<SpecialServiceDescriptionDetail> specialServicesRequestedType,
            HoldAtLocDetail holdAtLocationDetail, HomeDelvDetail homeDeliveryDetail) {

        this.specialServicesRequestedType = specialServicesRequestedType;
        this.holdAtLocationDetail = holdAtLocationDetail;
        this.homeDeliveryDetail = homeDeliveryDetail;
    }

    /**
     * @return the specialServicesRequestedType
     */
    public List<SpecialServiceDescriptionDetail> getSpecialServicesRequestedType() {

        return specialServicesRequestedType;
    }

    /**
     * @param specialServicesRequestedType the specialServicesRequestedType to set
     */
    public void setSpecialServicesRequestedType(
            List<SpecialServiceDescriptionDetail> specialServicesRequestedType) {

        this.specialServicesRequestedType = specialServicesRequestedType;
    }

    /**
     * @return the holdAtLocationDetail
     */
    public HoldAtLocDetail getHoldAtLocationDetail() {

        return holdAtLocationDetail;
    }

    /**
     * @param holdAtLocationDetail the holdAtLocationDetail to set
     */
    public void setHoldAtLocationDetail(
            HoldAtLocDetail holdAtLocationDetail) {

        this.holdAtLocationDetail = holdAtLocationDetail;
    }

    /**
     * @return the homeDeliveryDetail
     */
    public HomeDelvDetail getHomeDeliveryDetail() {

        return homeDeliveryDetail;
    }

    /**
     * @param homeDeliveryDetail the homeDeliveryDetail to set
     */
    public void setHomeDeliveryDetail(
            HomeDelvDetail homeDeliveryDetail) {

        this.homeDeliveryDetail = homeDeliveryDetail;
    }

    @Override
    public String toString() {

        return "ShipmentSpecialServiceDetail [specialServicesRequestedType=" + specialServicesRequestedType
                + ", holdAtLocationDetail=" + holdAtLocationDetail + ", homeDeliveryDetail=" + homeDeliveryDetail + "]";
    }
}
