package com.fedex.rscs.model;

import java.util.List;

/**
 * Define VariationOptions model specifications.
 * 
 * @author 5034922
 *
 */
public class VariationOptionDetail {

    private String id;
    private List<String> values;

    public VariationOptionDetail() {

    }

    /**
     * @param id
     * @param values
     */
    public VariationOptionDetail(String id, List<String> values) {

        this.id = id;
        this.values = values;
    }


    /**
     * @return the id
     */
    public String getId() {

        return id;
    }


    /**
     * @param id the id to set
     */
    public void setId(
            String id) {

        this.id = id;
    }


    /**
     * @return the values
     */
    public List<String> getValues() {

        return values;
    }


    /**
     * @param values the values to set
     */
    public void setValues(
           List<String> values) {

        this.values = values;
    }


    @Override
    public String toString() {

        return "VariationOptions [id=" + id + ", values=" + values + "]";
    }

}
