package com.fedex.rscs.model;

/**
 * Deleted sold shipment detail model
 * 
 * @author 3932956
 *
 */
public class DeleteSoldShipmentDetail {

    private String trackingNumber;
    private String carrierType;

    /**
     * @param trackingNumber
     * @param carrierType
     */
    public DeleteSoldShipmentDetail(String trackingNumber, String carrierType) {
        
        this.trackingNumber = trackingNumber;
        this.carrierType = carrierType;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {

        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {

        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the carrierType
     */
    public String getCarrierType() {

        return carrierType;
    }

    /**
     * @param carrierType the carrierType to set
     */
    public void setCarrierType(
            String carrierType) {

        this.carrierType = carrierType;
    }

    @Override
    public String toString() {

        return "DeleteSoldShipmentDetail [trackingNumber=" + trackingNumber + ", carrierType=" + carrierType + "]";
    }

}
