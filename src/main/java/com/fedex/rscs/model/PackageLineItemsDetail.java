package com.fedex.rscs.model;

import com.fedex.rscs.model.cshp.TrackingSequenceDetail;

/**
 * Package Line items model
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackageLineItemsDetail {

    private TrackingSequenceDetail trackingId;
    private boolean master;

    public PackageLineItemsDetail() {

        // default constructor
    }
    
    /**
     * @return the trackingId
     */
    public TrackingSequenceDetail getTrackingId() {
    
        return trackingId;
    }
    
    /**
     * @param trackingId the trackingId to set
     */
    public void setTrackingId(
            TrackingSequenceDetail trackingId) {
    
        this.trackingId = trackingId;
    }
    
    /**
     * @return the master
     */
    public boolean isMaster() {
    
        return master;
    }
    
    /**
     * @param master the master to set
     */
    public void setMaster(
            boolean master) {
    
        this.master = master;
    }
    
    @Override
    public String toString() {

        return "PackageLineItemsDetail [trackingId=" + trackingId + ", master=" + master + "]";
    }
    
}
