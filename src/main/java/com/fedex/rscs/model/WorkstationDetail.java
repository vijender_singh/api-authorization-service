package com.fedex.rscs.model;

/**
 * Represents shipment workstation details.
 * 
 * @author 3666723
 *
 */
public class WorkstationDetail {
    
    private String deviceId;
    private String accountNumber;
    private String meterNumber;
    private String softwareId;
    private String appName;
    private String appVersionId;
    private String guid;
    private String groundAccountNumber;
    private String teamMemberId;

    public WorkstationDetail() {}

    /**
     * @return the deviceId
     */
    public String getDeviceId() {

        return deviceId;
    }


    /**
     * @param deviceId the deviceId to set
     */
    public void setDeviceId(
            String deviceId) {

        this.deviceId = deviceId;
    }
        
    
    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
    
        return accountNumber;
    }

    
    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(
            String accountNumber) {
    
        this.accountNumber = accountNumber;
    }

    /**
     * @return the meterNumber
     */
    public String getMeterNumber() {

        return meterNumber;
    }


    /**
     * @param meterNumber the meterNumber to set
     */
    public void setMeterNumber(
            String meterNumber) {

        this.meterNumber = meterNumber;
    }


    /**
     * @return the softwareId
     */
    public String getSoftwareId() {

        return softwareId;
    }


    /**
     * @param softwareId the softwareId to set
     */
    public void setSoftwareId(
            String softwareId) {

        this.softwareId = softwareId;
    }


    /**
     * @return the appName
     */
    public String getAppName() {

        return appName;
    }


    /**
     * @param appName the appName to set
     */
    public void setAppName(
            String appName) {

        this.appName = appName;
    }


    /**
     * @return the appVersionId
     */
    public String getAppVersionId() {

        return appVersionId;
    }


    /**
     * @param appVersionId the appVersionId to set
     */
    public void setAppVersionId(
            String appVersionId) {

        this.appVersionId = appVersionId;
    }
    
    /**
     * @return the guid
     */
    public String getGuid() {
    
        return guid;
    }

    /**
     * @param guid the guid to set
     */
    public void setGuid(
            String guid) {
    
        this.guid = guid;
    }
    
    /**
     * @return the groundAccountNumber
     */
    public String getGroundAccountNumber() {
    
        return groundAccountNumber;
    }

    /**
     * @param groundAccountNumber the groundAccountNumber to set
     */
    public void setGroundAccountNumber(
            String groundAccountNumber) {
    
        this.groundAccountNumber = groundAccountNumber;
    }
    
    /**
     * @return the teamMemberId
     */
    public String getTeamMemberId() {
    
        return teamMemberId;
    }

    /**
     * @param teamMemberId the teamMemberId to set
     */
    public void setTeamMemberId(
            String teamMemberId) {
    
        this.teamMemberId = teamMemberId;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + ((accountNumber == null) ? 0 : accountNumber.hashCode());
        result = prime * result + ((appName == null) ? 0 : appName.hashCode());
        result = prime * result + ((appVersionId == null) ? 0 : appVersionId.hashCode());
        result = prime * result + ((deviceId == null) ? 0 : deviceId.hashCode());
        result = prime * result + ((groundAccountNumber == null) ? 0 : groundAccountNumber.hashCode());
        result = prime * result + ((guid == null) ? 0 : guid.hashCode());
        result = prime * result + ((meterNumber == null) ? 0 : meterNumber.hashCode());
        result = prime * result + ((softwareId == null) ? 0 : softwareId.hashCode());
        result = prime * result + ((teamMemberId == null) ? 0 : teamMemberId.hashCode());
        return result;
    }

    @Override
    public boolean equals(
            Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WorkstationDetail other = (WorkstationDetail) obj;
        if (accountNumber == null) {
            if (other.accountNumber != null)
                return false;
        } else if (!accountNumber.equals(other.accountNumber))
            return false;
        if (appName == null) {
            if (other.appName != null)
                return false;
        } else if (!appName.equals(other.appName))
            return false;
        if (appVersionId == null) {
            if (other.appVersionId != null)
                return false;
        } else if (!appVersionId.equals(other.appVersionId))
            return false;
        if (deviceId == null) {
            if (other.deviceId != null)
                return false;
        } else if (!deviceId.equals(other.deviceId))
            return false;
        if (groundAccountNumber == null) {
            if (other.groundAccountNumber != null)
                return false;
        } else if (!groundAccountNumber.equals(other.groundAccountNumber))
            return false;
        if (guid == null) {
            if (other.guid != null)
                return false;
        } else if (!guid.equals(other.guid))
            return false;
        if (meterNumber == null) {
            if (other.meterNumber != null)
                return false;
        } else if (!meterNumber.equals(other.meterNumber))
            return false;
        if (softwareId == null) {
            if (other.softwareId != null)
                return false;
        } else if (!softwareId.equals(other.softwareId))
            return false;
        if (teamMemberId == null) {
            if (other.teamMemberId != null)
                return false;
        } else if (!teamMemberId.equals(other.teamMemberId))
            return false;
        return true;
    }

    @Override
    public String toString() {

        return "WorkstationDetail [deviceId=" + deviceId + ", accountNumber=" + accountNumber + ", meterNumber="
                + meterNumber + ", softwareId=" + softwareId + ", appName=" + appName + ", appVersionId=" + appVersionId
                + ", guid=" + guid + ", groundAccountNumber=" + groundAccountNumber + ", teamMemberId=" + teamMemberId
                + "]";
    }
    
}
