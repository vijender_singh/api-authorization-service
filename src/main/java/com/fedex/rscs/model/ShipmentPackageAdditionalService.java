package com.fedex.rscs.model;

/**
 * Define the Shipment Package Additional Service model used in repository layer for mapping table
 * SHIPMENT_PACKAGE_ADDL_SVC.
 * 
 * @author shakti.saurabh
 *
 */
public class ShipmentPackageAdditionalService {

    private String shipmentTrackingNumber;
    private String acceptTmStmp;
    private String trackingNumber;
    private String specialSerivceCode;
    private String specialServiceDescription;
    
    /**
     * @return the shipmentTrackingNumber
     */
    public String getShipmentTrackingNumber() {
    
        return shipmentTrackingNumber;
    }
    
    /**
     * @param shipmentTrackingNumber the shipmentTrackingNumber to set
     */
    public void setShipmentTrackingNumber(
            String shipmentTrackingNumber) {
    
        this.shipmentTrackingNumber = shipmentTrackingNumber;
    }
    
    /**
     * @return the acceptTmStmp
     */
    public String getAcceptTmStmp() {
    
        return acceptTmStmp;
    }

    
    /**
     * @param acceptTmStmp the acceptTmStmp to set
     */
    public void setAcceptTmStmp(
            String acceptTmStmp) {
    
        this.acceptTmStmp = acceptTmStmp;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
    
        return trackingNumber;
    }
    
    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {
    
        this.trackingNumber = trackingNumber;
    }
    
    /**
     * @return the specialSerivceCode
     */
    public String getSpecialSerivceCode() {
    
        return specialSerivceCode;
    }
    
    /**
     * @param specialSerivceCode the specialSerivceCode to set
     */
    public void setSpecialSerivceCode(
            String specialSerivceCode) {
    
        this.specialSerivceCode = specialSerivceCode;
    }
    
    /**
     * @return the specialServiceDescription
     */
    public String getSpecialServiceDescription() {
    
        return specialServiceDescription;
    }
    
    /**
     * @param specialServiceDescription the specialServiceDescription to set
     */
    public void setSpecialServiceDescription(
            String specialServiceDescription) {
    
        this.specialServiceDescription = specialServiceDescription;
    }

    @Override
    public String toString() {

        return "ShipmentPackageAdditionalService [shipmentTrackingNumber=" + shipmentTrackingNumber + ", acceptTmStmp="
                + acceptTmStmp + ", trackingNumber=" + trackingNumber + ", specialSerivceCode=" + specialSerivceCode
                + ", specialServiceDescription=" + specialServiceDescription + "]";
    }

}
