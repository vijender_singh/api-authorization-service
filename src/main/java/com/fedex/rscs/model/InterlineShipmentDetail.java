package com.fedex.rscs.model;

/**
 * Define the Interline Shipment model used in repository layer for mapping table INTLN_SHPMT
 * 
 * @author 3932956
 *
 */
public class InterlineShipmentDetail {

    private String shipmentTrackingNumber;
    private String acceptTmStmp;
    private String interlineId;
    private String employeeId;
    private String shipmentWeight;

    public InterlineShipmentDetail() {
        
    }

    /**
     * @return the shipmentTrackingNumber
     */
    public String getShipmentTrackingNumber() {

        return shipmentTrackingNumber;
    }

    /**
     * @param shipmentTrackingNumber the shipmentTrackingNumber to set
     */
    public void setShipmentTrackingNumber(
            String shipmentTrackingNumber) {

        this.shipmentTrackingNumber = shipmentTrackingNumber;
    }

    /**
     * @return the acceptTmStmp
     */
    public String getAcceptTmStmp() {

        return acceptTmStmp;
    }

    /**
     * @param acceptTmStmp the acceptTmStmp to set
     */
    public void setAcceptTmStmp(
            String acceptTmStmp) {

        this.acceptTmStmp = acceptTmStmp;
    }

    /**
     * @return the interlineId
     */
    public String getInterlineId() {

        return interlineId;
    }

    /**
     * @param interlineId the interlineId to set
     */
    public void setInterlineId(
            String interlineId) {

        this.interlineId = interlineId;
    }

    /**
     * @return the employeeId
     */
    public String getEmployeeId() {

        return employeeId;
    }

    /**
     * @param employeeId the employeeId to set
     */
    public void setEmployeeId(
            String employeeId) {

        this.employeeId = employeeId;
    }

    /**
     * @return the shipmentWeight
     */
    public String getShipmentWeight() {

        return shipmentWeight;
    }

    /**
     * @param shipmentWeight the shipmentWeight to set
     */
    public void setShipmentWeight(
            String shipmentWeight) {

        this.shipmentWeight = shipmentWeight;
    }

    @Override
    public String toString() {

        return "InterlineShipmentDetail [shipmentTrackingNumber=" + shipmentTrackingNumber + ", acceptTmStmp="
                + acceptTmStmp + ", interlineId=" + interlineId + ", employeeId=" + employeeId + ", shipmentWeight="
                + shipmentWeight + "]";
    }

}
