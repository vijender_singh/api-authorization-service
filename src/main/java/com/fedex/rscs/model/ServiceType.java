package com.fedex.rscs.model;

/**
 * Represent the FedEx service types.
 * 
 * @author 5034922
 *
 */
public enum ServiceType {

    FIRST_OVERNIGHT, PRIORITY_OVERNIGHT, STANDARD_OVERNIGHT, FEDEX_2_DAY, FEDEX_2_DAY_AM, FEDEX_GROUND, GROUND_HOME_DELIVERY;
}
