package com.fedex.rscs.model;

import java.util.List;

import com.fedex.rscs.model.cshp.CompletedPackageData;
import com.fedex.rscs.model.cshp.PreauthorizedRateData;
import com.fedex.rscs.model.cshp.ShippingRateData;

/**
 * Blob object to save in database for open shipment flow
 * 
 * @author Surbhi.Gupta
 *
 */
public class OpenShipmentDetail {

    private String locationId;
    private String shipmentIndex;
    private String serviceType;
    private String packagingType;
    private int packageCount;
    private CarrierDetail carrierDetails;
    private PartyDetail customer;
    private PartyDetail shipper;
    private PartyDetail recipient;
    private ShipmentSpecialServiceDetail specialServicesRequested;
    private InsuredValueDetail totalInsuredValue;
    private WeightDetail totalWeight;
    private ShippingRateData rateDetails;
    private PreauthorizedRateData preauthorizedRateDetails;
    private PaymentDetail paymentDetails;
    private LabelSpecificationDetail labelSpecification;
    private List<CompletedPackageData> packageLineItems;
    private String appVersionId;
    private String physicalPackagingType;
    private boolean isSenderCustomer;
    private InterlineShippingData interlineShippingData;
     
    public OpenShipmentDetail() {}

    /**
     * @return the locationId
     */
    public String getLocationId() {

        return locationId;
    }

    /**
     * @param locationId the locationId to set
     */
    public void setLocationId(
            String locationId) {

        this.locationId = locationId;
    }

    /**
     * @return the shipmentIndex
     */
    public String getShipmentIndex() {

        return shipmentIndex;
    }

    /**
     * @param shipmentIndex the shipmentIndex to set
     */
    public void setShipmentIndex(
            String shipmentIndex) {

        this.shipmentIndex = shipmentIndex;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {

        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(
            String serviceType) {

        this.serviceType = serviceType;
    }

    /**
     * @return the packagingType
     */
    public String getPackagingType() {

        return packagingType;
    }

    /**
     * @param packagingType the packagingType to set
     */
    public void setPackagingType(
            String packagingType) {

        this.packagingType = packagingType;
    }

    /**
     * @return the packageCount
     */
    public int getPackageCount() {

        return packageCount;
    }

    /**
     * @param packageCount the packageCount to set
     */
    public void setPackageCount(
            int packageCount) {

        this.packageCount = packageCount;
    }

    /**
     * @return the carrierDetails
     */
    public CarrierDetail getCarrierDetails() {

        return carrierDetails;
    }

    /**
     * @param carrierDetails the carrierDetails to set
     */
    public void setCarrierDetails(
            CarrierDetail carrierDetails) {

        this.carrierDetails = carrierDetails;
    }

    /**
     * @return the customer
     */
    public PartyDetail getCustomer() {

        return customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(
            PartyDetail customer) {

        this.customer = customer;
    }

    /**
     * @return the shipper
     */
    public PartyDetail getShipper() {

        return shipper;
    }

    /**
     * @param shipper the shipper to set
     */
    public void setShipper(
            PartyDetail shipper) {

        this.shipper = shipper;
    }

    /**
     * @return the recipient
     */
    public PartyDetail getRecipient() {

        return recipient;
    }

    /**
     * @param recipient the recipient to set
     */
    public void setRecipient(
            PartyDetail recipient) {

        this.recipient = recipient;
    }

    /**
     * @return the specialServicesRequested
     */
    public ShipmentSpecialServiceDetail getSpecialServicesRequested() {

        return specialServicesRequested;
    }

    /**
     * @param specialServicesRequested the specialServicesRequested to set
     */
    public void setSpecialServicesRequested(
            ShipmentSpecialServiceDetail specialServicesRequested) {

        this.specialServicesRequested = specialServicesRequested;
    }

    /**
     * @return the totalInsuredValue
     */
    public InsuredValueDetail getTotalInsuredValue() {

        return totalInsuredValue;
    }

    /**
     * @param totalInsuredValue the totalInsuredValue to set
     */
    public void setTotalInsuredValue(
            InsuredValueDetail totalInsuredValue) {

        this.totalInsuredValue = totalInsuredValue;
    }

    /**
     * @return the totalWeight
     */
    public WeightDetail getTotalWeight() {

        return totalWeight;
    }

    /**
     * @param totalWeight the totalWeight to set
     */
    public void setTotalWeight(
            WeightDetail totalWeight) {

        this.totalWeight = totalWeight;
    }

    /**
     * @return the rateDetails
     */
    public ShippingRateData getRateDetails() {

        return rateDetails;
    }

    /**
     * @param rateDetails the rateDetails to set
     */
    public void setRateDetails(
            ShippingRateData rateDetails) {

        this.rateDetails = rateDetails;
    }

    /**
     * @return the preauthorizedRateDetails
     */
    public PreauthorizedRateData getPreauthorizedRateDetails() {

        return preauthorizedRateDetails;
    }

    /**
     * @param preauthorizedRateDetails the preauthorizedRateDetails to set
     */
    public void setPreauthorizedRateDetails(
            PreauthorizedRateData preauthorizedRateDetails) {

        this.preauthorizedRateDetails = preauthorizedRateDetails;
    }

    /**
     * @return the paymentDetails
     */
    public PaymentDetail getPaymentDetails() {

        return paymentDetails;
    }

    /**
     * @param paymentDetails the paymentDetails to set
     */
    public void setPaymentDetails(
            PaymentDetail paymentDetails) {

        this.paymentDetails = paymentDetails;
    }

    /**
     * @return the labelSpecification
     */
    public LabelSpecificationDetail getLabelSpecification() {

        return labelSpecification;
    }

    /**
     * @param labelSpecification the labelSpecification to set
     */
    public void setLabelSpecification(
            LabelSpecificationDetail labelSpecification) {

        this.labelSpecification = labelSpecification;
    }

    /**
     * @return the packageLineItems
     */
    public List<CompletedPackageData> getPackageLineItems() {

        return packageLineItems;
    }

    /**
     * @param packageLineItems the packageLineItems to set
     */
    public void setPackageLineItems(
            List<CompletedPackageData> packageLineItems) {

        this.packageLineItems = packageLineItems;
    }

    /**
     * @return the appVersionId
     */
    public String getAppVersionId() {

        return appVersionId;
    }

    /**
     * @param appVersionId the appVersionId to set
     */
    public void setAppVersionId(
            String appVersionId) {

        this.appVersionId = appVersionId;
    }

    /**
     * @return the physicalpackagingType
     */
    public String getPhysicalPackagingType() {
    
        return physicalPackagingType;
    }
    
    /**
     * @param physicalPackagingType the physicalpackagingType to set
     */
    public void setPhysicalPackagingType(
            String physicalPackagingType) {
    
        this.physicalPackagingType = physicalPackagingType;
    }
    
    /**
     * @return the isSenderCustomer
     */
    public boolean isSenderCustomer() {
    
        return isSenderCustomer;
    }

    /**
     * @param isSenderCustomer the isSenderCustomer to set
     */
    public void setSenderCustomer(
            boolean isSenderCustomer) {
    
        this.isSenderCustomer = isSenderCustomer;
    }

    /**
     * @return the interlineShippingData
     */
    public InterlineShippingData getInterlineShippingData() {

        return interlineShippingData;
    }

    /**
     * @param interlineShippingData the interlineShippingData to set
     */
    public void setInterlineShippingData(
            InterlineShippingData interlineShippingData) {

        this.interlineShippingData = interlineShippingData;
    }

    @Override
    public String toString() {

        return "OpenShipmentDetail [locationId=" + locationId + ", shipmentIndex=" + shipmentIndex + ", serviceType="
                + serviceType + ", packagingType=" + packagingType + ", packageCount=" + packageCount
                + ", carrierDetails=" + carrierDetails + ", customer=" + customer + ", shipper=" + shipper
                + ", recipient=" + recipient + ", specialServicesRequested=" + specialServicesRequested
                + ", totalInsuredValue=" + totalInsuredValue + ", totalWeight=" + totalWeight + ", rateDetails="
                + rateDetails + ", preauthorizedRateDetails=" + preauthorizedRateDetails + ", paymentDetails="
                + paymentDetails + ", labelSpecification=" + labelSpecification + ", packageLineItems="
                + packageLineItems + ", appVersionId=" + appVersionId + ", physicalPackagingType="
                + physicalPackagingType + ", isSenderCustomer=" + isSenderCustomer + ", interlineShippingData="
                + interlineShippingData + "]";
    }

}
