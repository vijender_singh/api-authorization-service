package com.fedex.rscs.model;

/**
 * Define the Shipment Contact model used in repository layer for mapping table SHIPMENT_CONTACT.
 * 
 * @author shakti.saurabh
 *
 */
public class ShipmentContactDetail {

    private String shipmentTrackingNumber;
    private String acceptTmStmp;
    private String recipientName;
    private String recipientCityName;
    private String recipientStateProvinceCode;
    private String recipientPostalCode;
    private String recipientCountryName;
    private String recipientCompanyName;
    private String recipientAddress1Text;
    private String recipientAddress2Text;
    private String recipientAddress3Text;
    private boolean residence;
    private String senderName;
    private String senderCityName;
    private String senderStateProvinceCode;
    private String senderPostalCode;
    private String senderCountryName;
    private String senderCompanyName;
    private String senderAddress1Text;
    private String senderAddress2Text;
    private String senderAddress3Text;
    private boolean isSenderCustomer;
    private String customerName;
    private String customerAddress1Text;
    private String customerAddress2Text;
    private String customerAddress3Text;
    private String customerCityName;
    private String customerStateProvinceCode;
    private String customerPostalCode;
    private String customerCountryName;
    private String customerCompanyName;
    private String customerPhoneNumber;
    private String customerPhoneExtensionNumber;

    /**
     * @return the shipmentTrackingNumber
     */
    public String getShipmentTrackingNumber() {

        return shipmentTrackingNumber;
    }

    /**
     * @param shipmentTrackingNumber the shipmentTrackingNumber to set
     */
    public void setShipmentTrackingNumber(
            String shipmentTrackingNumber) {

        this.shipmentTrackingNumber = shipmentTrackingNumber;
    }

    /**
     * @return the acceptTmStmp
     */
    public String getAcceptTmStmp() {

        return acceptTmStmp;
    }

    /**
     * @param acceptTmStmp the acceptTmStmp to set
     */
    public void setAcceptTmStmp(
            String acceptTmStmp) {

        this.acceptTmStmp = acceptTmStmp;
    }

    /**
     * @return the recipientName
     */
    public String getRecipientName() {

        return recipientName;
    }

    /**
     * @param recipientName the recipientName to set
     */
    public void setRecipientName(
            String recipientName) {

        this.recipientName = recipientName;
    }

    /**
     * @return the recipientCityName
     */
    public String getRecipientCityName() {

        return recipientCityName;
    }

    /**
     * @param recipientCityName the recipientCityName to set
     */
    public void setRecipientCityName(
            String recipientCityName) {

        this.recipientCityName = recipientCityName;
    }

    /**
     * @return the recipientStateProvinceCode
     */
    public String getRecipientStateProvinceCode() {

        return recipientStateProvinceCode;
    }

    /**
     * @param recipientStateProvinceCode the recipientStateProvinceCode to set
     */
    public void setRecipientStateProvinceCode(
            String recipientStateProvinceCode) {

        this.recipientStateProvinceCode = recipientStateProvinceCode;
    }

    /**
     * @return the recipientPostalCode
     */
    public String getRecipientPostalCode() {

        return recipientPostalCode;
    }

    /**
     * @param recipientPostalCode the recipientPostalCode to set
     */
    public void setRecipientPostalCode(
            String recipientPostalCode) {

        this.recipientPostalCode = recipientPostalCode;
    }

    /**
     * @return the recipientCountryName
     */
    public String getRecipientCountryName() {

        return recipientCountryName;
    }

    /**
     * @param recipientCountryName the recipientCountryName to set
     */
    public void setRecipientCountryName(
            String recipientCountryName) {

        this.recipientCountryName = recipientCountryName;
    }

    /**
     * @return the recipientCompanyName
     */
    public String getRecipientCompanyName() {

        return recipientCompanyName;
    }

    /**
     * @param recipientCompanyName the recipientCompanyName to set
     */
    public void setRecipientCompanyName(
            String recipientCompanyName) {

        this.recipientCompanyName = recipientCompanyName;
    }

    /**
     * @return the recipientAddress1Text
     */
    public String getRecipientAddress1Text() {

        return recipientAddress1Text;
    }

    /**
     * @param recipientAddress1Text the recipientAddress1Text to set
     */
    public void setRecipientAddress1Text(
            String recipientAddress1Text) {

        this.recipientAddress1Text = recipientAddress1Text;
    }

    /**
     * @return the recipientAddress2Text
     */
    public String getRecipientAddress2Text() {

        return recipientAddress2Text;
    }

    /**
     * @param recipientAddress2Text the recipientAddress2Text to set
     */
    public void setRecipientAddress2Text(
            String recipientAddress2Text) {

        this.recipientAddress2Text = recipientAddress2Text;
    }

    /**
     * @return the recipientAddress3Text
     */
    public String getRecipientAddress3Text() {

        return recipientAddress3Text;
    }

    /**
     * @param recipientAddress3Text the recipientAddress3Text to set
     */
    public void setRecipientAddress3Text(
            String recipientAddress3Text) {

        this.recipientAddress3Text = recipientAddress3Text;
    }

    /**
     * @return the residence
     */
    public boolean isResidence() {

        return residence;
    }

    /**
     * @param residence the residence to set
     */
    public void setResidence(
            boolean residence) {

        this.residence = residence;
    }

    /**
     * @return the senderName
     */
    public String getSenderName() {

        return senderName;
    }

    /**
     * @param senderName the senderName to set
     */
    public void setSenderName(
            String senderName) {

        this.senderName = senderName;
    }

    /**
     * @return the senderCityName
     */
    public String getSenderCityName() {

        return senderCityName;
    }

    /**
     * @param senderCityName the senderCityName to set
     */
    public void setSenderCityName(
            String senderCityName) {

        this.senderCityName = senderCityName;
    }

    /**
     * @return the senderStateProvinceCode
     */
    public String getSenderStateProvinceCode() {

        return senderStateProvinceCode;
    }

    /**
     * @param senderStateProvinceCode the senderStateProvinceCode to set
     */
    public void setSenderStateProvinceCode(
            String senderStateProvinceCode) {

        this.senderStateProvinceCode = senderStateProvinceCode;
    }

    /**
     * @return the senderPostalCode
     */
    public String getSenderPostalCode() {

        return senderPostalCode;
    }

    /**
     * @param senderPostalCode the senderPostalCode to set
     */
    public void setSenderPostalCode(
            String senderPostalCode) {

        this.senderPostalCode = senderPostalCode;
    }

    /**
     * @return the senderCountryName
     */
    public String getSenderCountryName() {

        return senderCountryName;
    }

    /**
     * @param senderCountryName the senderCountryName to set
     */
    public void setSenderCountryName(
            String senderCountryName) {

        this.senderCountryName = senderCountryName;
    }

    /**
     * @return the senderCompanyName
     */
    public String getSenderCompanyName() {

        return senderCompanyName;
    }

    /**
     * @param senderCompanyName the senderCompanyName to set
     */
    public void setSenderCompanyName(
            String senderCompanyName) {

        this.senderCompanyName = senderCompanyName;
    }

    /**
     * @return the senderAddress1Text
     */
    public String getSenderAddress1Text() {

        return senderAddress1Text;
    }

    /**
     * @param senderAddress1Text the senderAddress1Text to set
     */
    public void setSenderAddress1Text(
            String senderAddress1Text) {

        this.senderAddress1Text = senderAddress1Text;
    }

    /**
     * @return the senderAddress2Text
     */
    public String getSenderAddress2Text() {

        return senderAddress2Text;
    }

    /**
     * @param senderAddress2Text the senderAddress2Text to set
     */
    public void setSenderAddress2Text(
            String senderAddress2Text) {

        this.senderAddress2Text = senderAddress2Text;
    }

    /**
     * @return the senderAddress3Text
     */
    public String getSenderAddress3Text() {

        return senderAddress3Text;
    }

    /**
     * @param senderAddress3Text the senderAddress3Text to set
     */
    public void setSenderAddress3Text(
            String senderAddress3Text) {

        this.senderAddress3Text = senderAddress3Text;
    }

    /**
     * @return the isSenderCustomer
     */
    public boolean isSenderCustomer() {

        return isSenderCustomer;
    }

    /**
     * @param isSenderCustomer the isSenderCustomer to set
     */
    public void setSenderCustomer(
            boolean isSenderCustomer) {

        this.isSenderCustomer = isSenderCustomer;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {

        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(
            String customerName) {

        this.customerName = customerName;
    }

    /**
     * @return the customerAddress1Text
     */
    public String getCustomerAddress1Text() {

        return customerAddress1Text;
    }

    /**
     * @param customerAddress1Text the customerAddress1Text to set
     */
    public void setCustomerAddress1Text(
            String customerAddress1Text) {

        this.customerAddress1Text = customerAddress1Text;
    }

    /**
     * @return the customerAddress2Text
     */
    public String getCustomerAddress2Text() {

        return customerAddress2Text;
    }

    /**
     * @param customerAddress2Text the customerAddress2Text to set
     */
    public void setCustomerAddress2Text(
            String customerAddress2Text) {

        this.customerAddress2Text = customerAddress2Text;
    }

    /**
     * @return the customerAddress3Text
     */
    public String getCustomerAddress3Text() {

        return customerAddress3Text;
    }

    /**
     * @param customerAddress3Text the customerAddress3Text to set
     */
    public void setCustomerAddress3Text(
            String customerAddress3Text) {

        this.customerAddress3Text = customerAddress3Text;
    }

    /**
     * @return the customerCityName
     */
    public String getCustomerCityName() {

        return customerCityName;
    }

    /**
     * @param customerCityName the customerCityName to set
     */
    public void setCustomerCityName(
            String customerCityName) {

        this.customerCityName = customerCityName;
    }

    /**
     * @return the customerStateProvinceCode
     */
    public String getCustomerStateProvinceCode() {

        return customerStateProvinceCode;
    }

    /**
     * @param customerStateProvinceCode the customerStateProvinceCode to set
     */
    public void setCustomerStateProvinceCode(
            String customerStateProvinceCode) {

        this.customerStateProvinceCode = customerStateProvinceCode;
    }

    /**
     * @return the customerPostalCode
     */
    public String getCustomerPostalCode() {

        return customerPostalCode;
    }

    /**
     * @param customerPostalCode the customerPostalCode to set
     */
    public void setCustomerPostalCode(
            String customerPostalCode) {

        this.customerPostalCode = customerPostalCode;
    }

    /**
     * @return the customerCountryName
     */
    public String getCustomerCountryName() {

        return customerCountryName;
    }

    /**
     * @param customerCountryName the customerCountryName to set
     */
    public void setCustomerCountryName(
            String customerCountryName) {

        this.customerCountryName = customerCountryName;
    }

    /**
     * @return the customerCompanyName
     */
    public String getCustomerCompanyName() {

        return customerCompanyName;
    }

    /**
     * @param customerCompanyName the customerCompanyName to set
     */
    public void setCustomerCompanyName(
            String customerCompanyName) {

        this.customerCompanyName = customerCompanyName;
    }

    /**
     * @return the customerPhoneNumber
     */
    public String getCustomerPhoneNumber() {

        return customerPhoneNumber;
    }

    /**
     * @param customerPhoneNumber the customerPhoneNumber to set
     */
    public void setCustomerPhoneNumber(
            String customerPhoneNumber) {

        this.customerPhoneNumber = customerPhoneNumber;
    }

    /**
     * @return the customerPhoneExtensionNumber
     */
    public String getCustomerPhoneExtensionNumber() {

        return customerPhoneExtensionNumber;
    }

    /**
     * @param customerPhoneExtensionNumber the customerPhoneExtensionNumber to set
     */
    public void setCustomerPhoneExtensionNumber(
            String customerPhoneExtensionNumber) {

        this.customerPhoneExtensionNumber = customerPhoneExtensionNumber;
    }

    @Override
    public String toString() {

        return "ShipmentContactDetail [shipmentTrackingNumber=" + shipmentTrackingNumber + ", acceptTmStmp="
                + acceptTmStmp + ", recipientName=" + recipientName + ", recipientCityName=" + recipientCityName
                + ", recipientStateProvinceCode=" + recipientStateProvinceCode + ", recipientPostalCode="
                + recipientPostalCode + ", recipientCountryName=" + recipientCountryName + ", recipientCompanyName="
                + recipientCompanyName + ", recipientAddress1Text=" + recipientAddress1Text + ", recipientAddress2Text="
                + recipientAddress2Text + ", recipientAddress3Text=" + recipientAddress3Text + ", residence="
                + residence + ", senderName=" + senderName + ", senderCityName=" + senderCityName
                + ", senderStateProvinceCode=" + senderStateProvinceCode + ", senderPostalCode=" + senderPostalCode
                + ", senderCountryName=" + senderCountryName + ", senderCompanyName=" + senderCompanyName
                + ", senderAddress1Text=" + senderAddress1Text + ", senderAddress2Text=" + senderAddress2Text
                + ", senderAddress3Text=" + senderAddress3Text + ", isSenderCustomer=" + isSenderCustomer
                + ", customerName=" + customerName + ", customerAddress1Text=" + customerAddress1Text
                + ", customerAddress2Text=" + customerAddress2Text + ", customerAddress3Text=" + customerAddress3Text
                + ", customerCityName=" + customerCityName + ", customerStateProvinceCode=" + customerStateProvinceCode
                + ", customerPostalCode=" + customerPostalCode + ", customerCountryName=" + customerCountryName
                + ", customerCompanyName=" + customerCompanyName + ", customerPhoneNumber=" + customerPhoneNumber
                + ", customerPhoneExtensionNumber=" + customerPhoneExtensionNumber + "]";
    }

}
