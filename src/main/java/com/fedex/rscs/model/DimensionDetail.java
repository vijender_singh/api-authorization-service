package com.fedex.rscs.model;


/**
 * Model class to hold shipment dimension info
 * 
 * @author 5034922
 *
 */
public class DimensionDetail {

    private Integer length;
    private Integer width;
    private Integer height;
    private String units;

    public DimensionDetail() {}

    /**
     * @param length
     * @param width
     * @param height
     * @param units
     */
    public DimensionDetail(Integer length, Integer width, Integer height, String units) {
        super();
        this.length = length;
        this.width = width;
        this.height = height;
        this.units = units;
    }

    /**
     * @return the length
     */
    public Integer getLength() {

        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(
            Integer length) {

        this.length = length;
    }

    /**
     * @return the width
     */
    public Integer getWidth() {

        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(
            Integer width) {

        this.width = width;
    }

    /**
     * @return the height
     */
    public Integer getHeight() {

        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(
            Integer height) {

        this.height = height;
    }

    /**
     * @return the units
     */
    public String getUnits() {
    
        return units;
    }
    
    /**
     * @param units the units to set
     */
    public void setUnits(
            String units) {
    
        this.units = units;
    }

    @Override
    public String toString() {

        return "DimensionDetail [length=" + length + ", width=" + width + ", height=" + height + ", units=" + units
                + "]";
    }

}
