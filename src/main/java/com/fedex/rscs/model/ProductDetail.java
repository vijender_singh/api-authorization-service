package com.fedex.rscs.model;

/**
 * Model for ProductDetail
 * 
 * @author 3900094
 *
 */
public class ProductDetail {

    private String type;
    private String encoding;
    private String value;

    public ProductDetail() {}

    /**
     * @return the type
     */
    public String getType() {

        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(
            String type) {

        this.type = type;
    }

    /**
     * @return the encoding
     */
    public String getEncoding() {

        return encoding;
    }

    /**
     * @param encoding the encoding to set
     */
    public void setEncoding(
            String encoding) {

        this.encoding = encoding;
    }

    /**
     * @return the value
     */
    public String getValue() {

        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(
            String value) {

        this.value = value;
    }

    @Override
    public String toString() {

        return "ProductDetail [type=" + type + ", encoding=" + encoding + ", value=" + value + "]";
    }

}
