package com.fedex.rscs.model;

/**
 * Define the Hold At Location Detail model.
 * 
 * @author 3932968
 *
 */
public class HoldAtLocDetail {

    private String locationId;
    private AddressDetail address;
    private ContactDetail contact;
    private String locationType;
    private String displayName;

    public HoldAtLocDetail() {}

    /**
     * @param locationId
     * @param address
     * @param contact
     * @param locationType
     * @param displayName
     */
    public HoldAtLocDetail(String locationId, AddressDetail address, ContactDetail contact, String locationType,
            String displayName) {

        this.locationId = locationId;
        this.address = address;
        this.contact = contact;
        this.locationType = locationType;
        this.displayName = displayName;
    }

    /**
     * @return the locationId
     */
    public String getLocationId() {

        return locationId;
    }

    /**
     * @param locationId the locationId to set
     */
    public void setLocationId(
            String locationId) {

        this.locationId = locationId;
    }

    /**
     * @return the address
     */
    public AddressDetail getAddress() {

        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(
            AddressDetail address) {

        this.address = address;
    }

    public ContactDetail getContact() {

        return contact;
    }

    public void setContact(
            ContactDetail contact) {

        this.contact = contact;
    }

    /**
     * @return the locationType
     */
    public String getLocationType() {
    
        return locationType;
    }

    /**
     * @param locationType the locationType to set
     */
    public void setLocationType(
            String locationType) {
    
        this.locationType = locationType;
    }
    
    /**
     * @return the displayName
     */
    public String getDisplayName() {
    
        return displayName;
    }
    
    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(
            String displayName) {
    
        this.displayName = displayName;
    }

    @Override
    public String toString() {

        return "HoldAtLocDetail [locationId=" + locationId + ", address=" + address + ", contact=" + contact
                + ", locationType=" + locationType + ", displayName=" + displayName + "]";
    }

}
