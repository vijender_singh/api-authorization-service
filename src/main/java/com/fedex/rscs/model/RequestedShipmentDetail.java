package com.fedex.rscs.model;

import java.util.List;

/**
 * Model to contain requested shipment details.
 * 
 * @author 3932968
 *
 */
public class RequestedShipmentDetail {

    private String shipTimestamp;
    private String dropOffType;
    private String serviceType;
    private List<VariationOptionDetail> variationOptions;
    private String packagingType;
    private PartyDetail origin;
    private PartyDetail sender;
    private PartyDetail recipient;
    private PartyDetail customer;
    private String packageCount;
    private String transactionLocationCode;
    private String index;
    private PaymentDetail shippingChargesPayment;
    private LabelSpecificationDetail labelSpecification;
    private List<String> actions;
    private boolean isSenderCustomer;
    private List<RequestedPackageLineItemDetail> packageLineItems;
    private ShipmentSpecialServiceDetail specialServicesRequested;
    private boolean expressAfterPickup;
    private boolean groundAfterPickup;
    private InterlineShippingData interlineShippingData;

    public RequestedShipmentDetail() {

    }

    /**
     * @return the shipTimestamp
     */
    public String getShipTimestamp() {

        return shipTimestamp;
    }

    /**
     * @param shipTimestamp the shipTimestamp to set
     */
    public void setShipTimestamp(
            String shipTimestamp) {

        this.shipTimestamp = shipTimestamp;
    }

    /**
     * @return the dropOffType
     */
    public String getDropOffType() {

        return dropOffType;
    }

    /**
     * @param dropOffType the dropOffType to set
     */
    public void setDropOffType(
            String dropOffType) {

        this.dropOffType = dropOffType;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {

        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(
            String serviceType) {

        this.serviceType = serviceType;
    }

    /**
     * @return the variationOptions
     */
    public List<VariationOptionDetail> getVariationOptions() {

        return variationOptions;
    }

    /**
     * @param variationOptions the variationOptions to set
     */
    public void setVariationOptions(
            List<VariationOptionDetail> variationOptions) {

        this.variationOptions = variationOptions;
    }

    /**
     * @return the packagingType
     */
    public String getPackagingType() {

        return packagingType;
    }

    /**
     * @param packagingType the packagingType to set
     */
    public void setPackagingType(
            String packagingType) {

        this.packagingType = packagingType;
    }

    /**
     * @return the origin
     */
    public PartyDetail getOrigin() {

        return origin;
    }

    /**
     * @param origin the origin to set
     */
    public void setOrigin(
            PartyDetail origin) {

        this.origin = origin;
    }

    /**
     * @return the sender
     */
    public PartyDetail getSender() {

        return sender;
    }

    /**
     * @param sender the sender to set
     */
    public void setSender(
            PartyDetail sender) {

        this.sender = sender;
    }

    /**
     * @return the recipient
     */
    public PartyDetail getRecipient() {

        return recipient;
    }

    /**
     * @param recipient the recipient to set
     */
    public void setRecipient(
            PartyDetail recipient) {

        this.recipient = recipient;
    }

    /**
     * @return the customer
     */
    public PartyDetail getCustomer() {

        return customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(
            PartyDetail customer) {

        this.customer = customer;
    }

    /**
     * @return the packageCount
     */
    public String getPackageCount() {

        return packageCount;
    }

    /**
     * @param packageCount the packageCount to set
     */
    public void setPackageCount(
            String packageCount) {

        this.packageCount = packageCount;
    }

    /**
     * @return the shippingChargesPayment
     */
    public PaymentDetail getShippingChargesPayment() {

        return shippingChargesPayment;
    }

    /**
     * @param shippingChargesPayment the shippingChargesPayment to set
     */
    public void setShippingChargesPayment(
            PaymentDetail shippingChargesPayment) {

        this.shippingChargesPayment = shippingChargesPayment;
    }

    /**
     * @return the labelSpecification
     */
    public LabelSpecificationDetail getLabelSpecification() {

        return labelSpecification;
    }

    /**
     * @param labelSpecification the labelSpecification to set
     */
    public void setLabelSpecification(
            LabelSpecificationDetail labelSpecification) {

        this.labelSpecification = labelSpecification;
    }

    /**
     * @return the packageLineItems
     */
    public List<RequestedPackageLineItemDetail> getPackageLineItems() {

        return packageLineItems;
    }

    /**
     * @param packageLineItems the packageLineItems to set
     */
    public void setPackageLineItems(
            List<RequestedPackageLineItemDetail> packageLineItems) {

        this.packageLineItems = packageLineItems;
    }

    /**
     * @return the specialServicesRequested
     */
    public ShipmentSpecialServiceDetail getSpecialServicesRequested() {

        return specialServicesRequested;
    }

    /**
     * @param specialServicesRequested the specialServicesRequested to set
     */
    public void setSpecialServicesRequested(
            ShipmentSpecialServiceDetail specialServicesRequested) {

        this.specialServicesRequested = specialServicesRequested;
    }

    /**
     * @return the transactionLocationCode
     */
    public String getTransactionLocationCode() {

        return transactionLocationCode;
    }

    /**
     * @param transactionLocationCode the transactionLocationCode to set
     */
    public void setTransactionLocationCode(
            String transactionLocationCode) {

        this.transactionLocationCode = transactionLocationCode;
    }

    /**
     * @return the index
     */
    public String getIndex() {

        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(
            String index) {

        this.index = index;
    }

    /**
     * @return the actions
     */
    public List<String> getActions() {

        return actions;
    }

    /**
     * @param actions the actions to set
     */
    public void setActions(
            List<String> actions) {

        this.actions = actions;
    }

    /**
     * @return the isSenderCustomer
     */
    public boolean isSenderCustomer() {

        return isSenderCustomer;
    }

    /**
     * @param isSenderCustomer the isSenderCustomer to set
     */
    public void setSenderCustomer(
            boolean isSenderCustomer) {

        this.isSenderCustomer = isSenderCustomer;
    }

    /**
     * @return the expressAfterPickup
     */
    public boolean isExpressAfterPickup() {

        return expressAfterPickup;
    }

    /**
     * @param expressAfterPickup the expressAfterPickup to set
     */
    public void setExpressAfterPickup(
            boolean expressAfterPickup) {

        this.expressAfterPickup = expressAfterPickup;
    }
    
    /**
     * @return the interlineShippingData
     */
    public InterlineShippingData getInterlineShippingData() {

        return interlineShippingData;
    }

    /**
     * @param interlineShippingData the interlineShippingData to set
     */
    public void setInterlineShippingData(
            InterlineShippingData interlineShippingData) {

        this.interlineShippingData = interlineShippingData;
    }

       
    /**
     * @return the groundAfterPickup
     */
    public boolean isGroundAfterPickup() {
    
        return groundAfterPickup;
    }

    
    /**
     * @param groundAfterPickup the groundAfterPickup to set
     */
    public void setGroundAfterPickup(
            boolean groundAfterPickup) {
    
        this.groundAfterPickup = groundAfterPickup;
    }

    @Override
    public String toString() {

        return "RequestedShipmentDetail [shipTimestamp=" + shipTimestamp + ", dropOffType=" + dropOffType
                + ", serviceType=" + serviceType + ", variationOptions=" + variationOptions + ", packagingType="
                + packagingType + ", origin=" + origin + ", sender=" + sender + ", recipient=" + recipient
                + ", customer=" + customer + ", packageCount=" + packageCount + ", transactionLocationCode="
                + transactionLocationCode + ", index=" + index + ", shippingChargesPayment=" + shippingChargesPayment
                + ", labelSpecification=" + labelSpecification + ", actions=" + actions + ", isSenderCustomer="
                + isSenderCustomer + ", packageLineItems=" + packageLineItems + ", specialServicesRequested="
                + specialServicesRequested + ", expressAfterPickup=" + expressAfterPickup + ", groundAfterPickup="
                + groundAfterPickup + ", interlineShippingData=" + interlineShippingData + "]";
    }

}
