package com.fedex.rscs.model;


/**
 * This model class represents the contact details.
 * 
 * @author 5034922
 */
public class ContactDetail {

    private String contactId;
    private String firstName;
    private String lastName;
    private String fullName;
    private String companyName;
    private String emailId;
    private String phoneNumber;
    private String phoneExtension;
    private String phoneUsage;
    private String alternateEmailId;

    public ContactDetail() {}

    /**
     * @param firstName
     * @param lastName
     * @param companyName
     * @param emailId
     * @param phoneNumber
     * @param phoneExtension
     */
    public ContactDetail(String firstName, String lastName, String companyName,
            String emailId, String phoneNumber, String phoneExtension) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.companyName = companyName;
        this.emailId = emailId;
        this.phoneNumber = phoneNumber;
        this.phoneExtension = phoneExtension;
    }

    /**
     * @return the contactId
     */
    public String getContactId() {

        return contactId;
    }

    /**
     * @param contactId the contactId to set
     */
    public void setContactId(
            String contactId) {

        this.contactId = contactId;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {

        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(
            String firstName) {

        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {

        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(
            String lastName) {

        this.lastName = lastName;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {

        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(
            String fullName) {

        this.fullName = fullName;
    }

    /**
     * @return the companyName
     */
    public String getCompanyName() {

        return companyName;
    }

    /**
     * @param companyName the companyName to set
     */
    public void setCompanyName(
            String companyName) {

        this.companyName = companyName;
    }

    /**
     * @return the emailId
     */
    public String getEmailId() {

        return emailId;
    }

    /**
     * @param emailId the emailId to set
     */
    public void setEmailId(
            String emailId) {

        this.emailId = emailId;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {

        return phoneNumber;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(
            String phoneNumber) {

        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the phoneExtension
     */
    public String getPhoneExtension() {

        return phoneExtension;
    }

    /**
     * @param phoneExtension the phoneExtension to set
     */
    public void setPhoneExtension(
            String phoneExtension) {

        this.phoneExtension = phoneExtension;
    }

    /**
     * @return the phoneUsage
     */
    public String getPhoneUsage() {

        return phoneUsage;
    }

    /**
     * @param phoneUsage the phoneUsage to set
     */
    public void setPhoneUsage(
            String phoneUsage) {

        this.phoneUsage = phoneUsage;
    }
    
    /**
     * @return the alternateEmailId
     */
    public String getAlternateEmailId() {
    
        return alternateEmailId;
    }
    
    /**
     * @param alternateEmailId the alternateEmailId to set
     */
    public void setAlternateEmailId(
            String alternateEmailId) {
    
        this.alternateEmailId = alternateEmailId;
    }

    @Override
    public String toString() {

        return "ContactDetail [contactId=" + contactId + ", firstName=" + firstName + ", lastName=" + lastName
                + ", fullName=" + fullName + ", companyName=" + companyName + ", emailId=" + emailId + ", phoneNumber="
                + phoneNumber + ", phoneExtension=" + phoneExtension + ", phoneUsage=" + phoneUsage
                + ", alternateEmailId=" + alternateEmailId + "]";
    }
    
}
