package com.fedex.rscs.model;

import java.util.List;

import com.fedex.rscs.model.cshp.EPaymentMode;
import com.fedex.rscs.model.cshp.Price;

/**
 * PaymentDetail model specification.
 * 
 * @author 5034922
 *
 */
public class PaymentDetail {

    private String paymentType;
    private PartyDetail payor;
    private String responsibleParty;
    private List<AssociatedAccountDetail> associatedAccounts;
    private Price amount;
    private String paymentSource;
    private EPaymentMode epaymentMode;
    private String paymentRefId;
    private String interlineId;

    public PaymentDetail() {}

    /**
     * @param paymentType
     * @param payor
     * @param responsibleParty
     * @param associatedAccounts
     * @param amount
     */
    public PaymentDetail(String paymentType, PartyDetail payor, String responsibleParty,
            List<AssociatedAccountDetail> associatedAccounts, Price amount) {

        this.paymentType = paymentType;
        this.payor = payor;
        this.responsibleParty = responsibleParty;
        this.associatedAccounts = associatedAccounts;
        this.amount = amount;
    }

    /**
     * @return the paymentType
     */
    public String getPaymentType() {

        return paymentType;
    }

    /**
     * @param paymentType the paymentType to set
     */
    public void setPaymentType(
            String paymentType) {

        this.paymentType = paymentType;
    }

    /**
     * @return the associatedAccount
     */
    public List<AssociatedAccountDetail> getAssociatedAccounts() {

        return associatedAccounts;
    }

    /**
     * @param associatedAccount the associatedAccount to set
     */
    public void setAssociatedAccounts(
            List<AssociatedAccountDetail> associatedAccounts) {

        this.associatedAccounts = associatedAccounts;
    }

    /**
     * @return the amount
     */
    public Price getAmount() {

        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(
            Price amount) {

        this.amount = amount;
    }

    /**
     * @return the payor
     */
    public PartyDetail getPayor() {

        return payor;
    }

    /**
     * @param payor the payor to set
     */
    public void setPayor(
            PartyDetail payor) {

        this.payor = payor;
    }

    /**
     * @return the responsibleParty
     */
    public String getResponsibleParty() {

        return responsibleParty;
    }

    /**
     * @param responsibleParty the responsibleParty to set
     */
    public void setResponsibleParty(
            String responsibleParty) {

        this.responsibleParty = responsibleParty;
    }

    /**
     * @return the paymentSource
     */
    public String getPaymentSource() {

        return paymentSource;
    }

    /**
     * @param paymentSource the paymentSource to set
     */
    public void setPaymentSource(
            String paymentSource) {

        this.paymentSource = paymentSource;
    }

    /**
     * get the {@link EPaymentMode} mode.
     * 
     * @return
     */
    public EPaymentMode getEpaymentMode() {

        return epaymentMode;
    }

    /**
     * Set the {@link EPaymentMode} mode.
     * 
     * @param epaymentMode
     */
    public void setEpaymentMode(
            EPaymentMode epaymentMode) {

        this.epaymentMode = epaymentMode;
    }

    /**
     * @return
     */
    public String getPaymentRefId() {

        return paymentRefId;
    }

    /**
     * @param paymentRefId
     */
    public void setPaymentRefId(
            String paymentRefId) {

        this.paymentRefId = paymentRefId;
    }

    /**
     * @return the interlineId
     */
    public String getInterlineId() {

        return interlineId;
    }

    /**
     * @param interlineId the interlineId to set
     */
    public void setInterlineId(
            String interlineId) {

        this.interlineId = interlineId;
    }

    @Override
    public String toString() {

        return "PaymentDetail [paymentType=" + paymentType + ", payor=" + payor + ", responsibleParty="
                + responsibleParty + ", associatedAccounts=" + associatedAccounts + ", amount=" + amount
                + ", paymentSource=" + paymentSource + ", epaymentMode=" + epaymentMode + ", paymentRefId="
                + paymentRefId + ", interlineId=" + interlineId + "]";
    }

}
