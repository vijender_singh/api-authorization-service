package com.fedex.rscs.model;

/**
 * Represents version detail
 * 
 * @author 3666723
 *
 */
public class VersionDetail {
    
    private String serviceId;
    private int major;
    private int intermediate;
    private int minor;
    
    /**
     * @return the serviceId
     */
    public String getServiceId() {
    
        return serviceId;
    }
    
    /**
     * @param serviceId the serviceId to set
     */
    public void setServiceId(
            String serviceId) {
    
        this.serviceId = serviceId;
    }
    
    /**
     * @return the major
     */
    public int getMajor() {
    
        return major;
    }
    
    /**
     * @param major the major to set
     */
    public void setMajor(
            int major) {
    
        this.major = major;
    }
    
    /**
     * @return the intermediate
     */
    public int getIntermediate() {
    
        return intermediate;
    }
    
    /**
     * @param intermediate the intermediate to set
     */
    public void setIntermediate(
            int intermediate) {
    
        this.intermediate = intermediate;
    }
    
    /**
     * @return the minor
     */
    public int getMinor() {
    
        return minor;
    }
    
    /**
     * @param minor the minor to set
     */
    public void setMinor(
            int minor) {
    
        this.minor = minor;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {

        return "VersionDetail [serviceId=" + serviceId + ", major=" + major + ", intermediate=" + intermediate
                + ", minor=" + minor + "]";
    }
}
