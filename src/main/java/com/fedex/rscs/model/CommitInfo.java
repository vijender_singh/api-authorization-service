package com.fedex.rscs.model;

/**
 * Model for CommitInfo
 * 
 * @author 3900094
 *
 */
public class CommitInfo {

    private String commitTimestamp;
    private String dayOfWeek;

    public CommitInfo() {}

    /**
     * @return the commitTimestamp
     */
    public String getCommitTimestamp() {

        return commitTimestamp;
    }

    /**
     * @param commitTimestamp the commitTimestamp to set
     */
    public void setCommitTimestamp(
            String commitTimestamp) {

        this.commitTimestamp = commitTimestamp;
    }

    /**
     * @return the dayOfWeek
     */
    public String getDayOfWeek() {

        return dayOfWeek;
    }

    /**
     * @param dayOfWeek the dayOfWeek to set
     */
    public void setDayOfWeek(
            String dayOfWeek) {

        this.dayOfWeek = dayOfWeek;
    }

    @Override
    public String toString() {

        return "CommitInfo [commitTimestamp=" + commitTimestamp + ", dayOfWeek=" + dayOfWeek + "]";
    }

}

