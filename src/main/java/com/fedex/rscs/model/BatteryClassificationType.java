package com.fedex.rscs.model;

/**
 * Represents Enum Model for Battery Classification Type.
 * 
 * @author 3932968
 *
 */
public enum BatteryClassificationType {
    
    LITHIUM_ION_PACKED_WITH_EQUIPMENT("LITHIUM_ION", "PACKED_WITH_EQUIPMENT","IATA_SECTION_II"),
    LITHIUM_ION_CONTAINED_IN_EQUIPMENT("LITHIUM_ION", "CONTAINED_IN_EQUIPMENT","IATA_SECTION_II"),
    LITHIUM_METAL_PACKED_WITH_EQUIPMENT("LITHIUM_METAL","PACKED_WITH_EQUIPMENT","IATA_SECTION_II"),
    LITHIUM_METAL_CONTAINED_IN_EQUIPMENT("LITHIUM_METAL","CONTAINED_IN_EQUIPMENT","IATA_SECTION_II");

    private String material;
    private String packing;
    private String regulatorySubType;

    BatteryClassificationType(String material, String packing, String regulatorySubType) {
        this.material = material;
        this.packing = packing;
        this.regulatorySubType = regulatorySubType;
    }

    public String getMaterial() {

        return material;
    }

    public String getPacking() {

        return packing;
    }

    public String getRegulatorySubType() {

        return regulatorySubType;
    }
}
