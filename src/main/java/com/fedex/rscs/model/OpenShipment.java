package com.fedex.rscs.model;

/**
 * Model class to save data in OPEN_SHIPMENT under FXORSCS_SCHEMA
 * 
 * @author Surbhi.Gupta
 *
 */
public class OpenShipment {

    String openShipmentId;
    String trackingNumber;
    String packageAcceptTmStmp;
    String initialAcceptanceTmStmp;
    String fedexLocationId;
    String opCoLocationId;
    String opCoCode;
    String meterNumber;
    String cityCenterAccountNumber;
    String fedexId;
    String shipmentData;
    String softwareId;
    String deviceId;
    String openShipmentStatusCode;
    String openShipmentStatusTmStmp;
   

    public OpenShipment() {

        // default constructor
    }

    /**
     * @return the openShipmentId
     */
    public String getOpenShipmentId() {

        return openShipmentId;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {

        return trackingNumber;
    }

    /**
     * @return the packageAcceptTmStmp
     */
    public String getPackageAcceptTmStmp() {

        return packageAcceptTmStmp;
    }

    /**
     * @return the initialAcceptanceTmStmp
     */
    public String getInitialAcceptanceTmStmp() {

        return initialAcceptanceTmStmp;
    }

    /**
     * @return the fedexLocationId
     */
    public String getFedexLocationId() {

        return fedexLocationId;
    }

    /**
     * @return the opCoLocationId
     */
    public String getOpCoLocationId() {

        return opCoLocationId;
    }

    /**
     * @return the opCoCode
     */
    public String getOpCoCode() {

        return opCoCode;
    }

    /**
     * @return the meterNumber
     */
    public String getMeterNumber() {

        return meterNumber;
    }

    /**
     * @return the cityCenterAccountNumber
     */
    public String getCityCenterAccountNumber() {

        return cityCenterAccountNumber;
    }

    /**
     * @return the fedexId
     */
    public String getFedexId() {

        return fedexId;
    }

    /**
     * @return the shipmentData
     */
    public String getShipmentData() {

        return shipmentData;
    }

    /**
     * @return the softwareId
     */
    public String getSoftwareId() {

        return softwareId;
    }

    /**
     * @return the deviceId
     */
    public String getDeviceId() {

        return deviceId;
    }

    /**
     * @return the openShipmentStatusCode
     */
    public String getOpenShipmentStatusCode() {

        return openShipmentStatusCode;
    }

    /**
     * @return the openShipmentStatusTmStmp
     */
    public String getOpenShipmentStatusTmStmp() {

        return openShipmentStatusTmStmp;
    }

    /**
     * @param openShipmentId the openShipmentId to set
     */
    public void setOpenShipmentId(
            String openShipmentId) {

        this.openShipmentId = openShipmentId;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {

        this.trackingNumber = trackingNumber;
    }

    /**
     * @param packageAcceptTmStmp the packageAcceptTmStmp to set
     */
    public void setPackageAcceptTmStmp(
            String packageAcceptTmStmp) {

        this.packageAcceptTmStmp = packageAcceptTmStmp;
    }

    /**
     * @param initialAcceptanceTmStmp the initialAcceptanceTmStmp to set
     */
    public void setInitialAcceptanceTmStmp(
            String initialAcceptanceTmStmp) {

        this.initialAcceptanceTmStmp = initialAcceptanceTmStmp;
    }

    /**
     * @param fedexLocationId the fedexLocationId to set
     */
    public void setFedexLocationId(
            String fedexLocationId) {

        this.fedexLocationId = fedexLocationId;
    }

    /**
     * @param opCoLocationId the opCoLocationId to set
     */
    public void setOpCoLocationId(
            String opCoLocationId) {

        this.opCoLocationId = opCoLocationId;
    }

    /**
     * @param opCoCode the opCoCode to set
     */
    public void setOpCoCode(
            String opCoCode) {

        this.opCoCode = opCoCode;
    }

    /**
     * @param meterNumber the meterNumber to set
     */
    public void setMeterNumber(
            String meterNumber) {

        this.meterNumber = meterNumber;
    }

    /**
     * @param cityCenterAccountNumber the cityCenterAccountNumber to set
     */
    public void setCityCenterAccountNumber(
            String cityCenterAccountNumber) {

        this.cityCenterAccountNumber = cityCenterAccountNumber;
    }

    /**
     * @param fedexId the fedexId to set
     */
    public void setFedexId(
            String fedexId) {

        this.fedexId = fedexId;
    }

    /**
     * @param shipmentData the shipmentData to set
     */
    public void setShipmentData(
            String shipmentData) {

        this.shipmentData = shipmentData;
    }

    /**
     * @param softwareId the softwareId to set
     */
    public void setSoftwareId(
            String softwareId) {

        this.softwareId = softwareId;
    }

    /**
     * @param deviceId the deviceId to set
     */
    public void setDeviceId(
            String deviceId) {

        this.deviceId = deviceId;
    }

    /**
     * @param openShipmentStatusCode the openShipmentStatusCode to set
     */
    public void setOpenShipmentStatusCode(
            String openShipmentStatusCode) {

        this.openShipmentStatusCode = openShipmentStatusCode;
    }

    /**
     * @param openShipmentStatusTmStmp the openShipmentStatusTmStmp to set
     */
    public void setOpenShipmentStatusTmStmp(
            String openShipmentStatusTmStmp) {

        this.openShipmentStatusTmStmp = openShipmentStatusTmStmp;
    }

    @Override
    public String toString() {

        return "OpenShipment [openShipmentId=" + openShipmentId + ", trackingNumber=" + trackingNumber
                + ", packageAcceptTmStmp=" + packageAcceptTmStmp + ", initialAcceptanceTmStmp="
                + initialAcceptanceTmStmp + ", fedexLocationId=" + fedexLocationId + ", opCoLocationId="
                + opCoLocationId + ", opCoCode=" + opCoCode + ", meterNumber=" + meterNumber
                + ", cityCenterAccountNumber=" + cityCenterAccountNumber + ", fedexId=" + fedexId + ", shipmentData="
                + shipmentData + ", softwareId=" + softwareId + ", deviceId=" + deviceId + ", openShipmentStatusCode="
                + openShipmentStatusCode + ", openShipmentStatusTmStmp=" + openShipmentStatusTmStmp + "]";
    }

}
