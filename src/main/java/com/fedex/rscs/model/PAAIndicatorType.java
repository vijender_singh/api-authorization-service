package com.fedex.rscs.model;

/**
 * Represents Enum for PAAIndicatorType.
 * 
 * @author 3798910
 *
 */
public enum PAAIndicatorType {

    CUSTOMER("NO_KNOWN_DAMAGE", "NOT_INSPECTED", "CUSTOMER"), FEDEX_OFFICE("NO_KNOWN_DAMAGE", "INSPECTED",
            "FEDEX_OFFICE"), FEDEX_DAMAGE_KNOWN("DAMAGE_KNOWN", "INSPECTED", "FEDEX_OFFICE"), INSPECTED(
                    "NO_KNOWN_DAMAGE", "INSPECTED", "CUSTOMER"), INSPECTED_DAMAGE_KNOWN("DAMAGE_KNOWN", "INSPECTED",
                            "CUSTOMER"), CUSTOMER_REFUSED_INSPECTION("NO_KNOWN_DAMAGE", "CUSTOMER_REFUSED_INSPECTION",
                                    "CUSTOMER"), REFUSED_INSPECTION_DAMAGE_KNOWN("DAMAGE_KNOWN",
                                            "CUSTOMER_REFUSED_INSPECTION", "CUSTOMER");

    private String damageStatus;
    private String inspectionStatus;
    private String packedBy;

    PAAIndicatorType(String damageStatus, String inspectionStatus, String packedBy) {
        this.damageStatus = damageStatus;
        this.inspectionStatus = inspectionStatus;
        this.packedBy = packedBy;
    }

    public String getDamageStatusType() {

        return damageStatus;
    }

    public String getPackedByType() {

        return packedBy;
    }

    public String getInspectionStatusType() {

        return inspectionStatus;
    }

}
