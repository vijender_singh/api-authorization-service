package com.fedex.rscs.model;

/**
 * Deleted sold shipment detail model
 * 
 * @author 3932956
 *
 */
public class DeletedSoldShipmentDetail {

    private String trackingNumber;
    private String carrierType;
    private boolean isUpdationError;
    
    public DeletedSoldShipmentDetail() {}
    
    /**
     * @param trackingNumber
     * @param carrierType
     */
    public DeletedSoldShipmentDetail(String trackingNumber, String carrierType) {
        
        this.trackingNumber = trackingNumber;
        this.carrierType = carrierType;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {

        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {

        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the carrierType
     */
    public String getCarrierType() {

        return carrierType;
    }

    /**
     * @param carrierType the carrierType to set
     */
    public void setCarrierType(
            String carrierType) {

        this.carrierType = carrierType;
    }

    /**
     * @return isUpdationError
     */
    public boolean isUpdationError() {
    
        return isUpdationError;
    }

    /**
     * @param isUpdationError to set the isUpdationError
     */
    public void setUpdationError(
            boolean isUpdationError) {
    
        this.isUpdationError = isUpdationError;
    }

    @Override
    public String toString() {

        return "DeletedSoldShipmentDetail [trackingNumber=" + trackingNumber + ", carrierType=" + carrierType
                + ", isUpdationError=" + isUpdationError + "]";
    }

}
