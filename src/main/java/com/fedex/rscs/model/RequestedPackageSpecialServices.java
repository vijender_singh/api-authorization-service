package com.fedex.rscs.model;

import java.util.List;

/**
 * Define the Requested Package Special Services model.
 * 
 * @author 3900109
 *
 */
public class RequestedPackageSpecialServices {

    private List<SpecialServiceDescriptionDetail> specialServicesRequested;
    private WeightDetail dryIceWeightDetails;
    private List<BatteryClassificationType> batteryClassifications;

    public RequestedPackageSpecialServices() {}

        
    /**
     * @param specialServicesRequested
     * @param dryIceWeightDetails
     * @param batteryClassifications
     */
    public RequestedPackageSpecialServices(List<SpecialServiceDescriptionDetail> specialServicesRequested, WeightDetail dryIceWeightDetails,
            List<BatteryClassificationType> batteryClassifications) {

        this.specialServicesRequested = specialServicesRequested;
        this.dryIceWeightDetails = dryIceWeightDetails;
        this.batteryClassifications = batteryClassifications;
    }



    /**
     * @return the specialServicesRequested
     */
    public List<SpecialServiceDescriptionDetail> getSpecialServicesRequested() {
    
        return specialServicesRequested;
    }

    
    /**
     * @param specialServicesRequested the specialServicesRequested to set
     */
    public void setSpecialServicesRequested(
            List<SpecialServiceDescriptionDetail> specialServicesRequested) {
    
        this.specialServicesRequested = specialServicesRequested;
    }

    
    /**
     * @return the dryIceWeightDetails
     */
    public WeightDetail getDryIceWeightDetails() {
    
        return dryIceWeightDetails;
    }

    
    /**
     * @param dryIceWeightDetails the dryIceWeightDetails to set
     */
    public void setDryIceWeightDetails(
            WeightDetail dryIceWeightDetails) {
    
        this.dryIceWeightDetails = dryIceWeightDetails;
    }

    
    /**
     * @return the batteryClassifications
     */
    public List<BatteryClassificationType> getBatteryClassifications() {
    
        return batteryClassifications;
    }

    
    /**
     * @param batteryClassifications the batteryClassifications to set
     */
    public void setBatteryClassifications(
            List<BatteryClassificationType> batteryClassifications) {
    
        this.batteryClassifications = batteryClassifications;
    }


    @Override
    public String toString() {

        return "RequestedPackageSpecialServices [specialServicesRequested=" + specialServicesRequested
                + ", dryIceWeightDetails=" + dryIceWeightDetails + ", batteryClassifications=" + batteryClassifications
                + "]";
    }

}
