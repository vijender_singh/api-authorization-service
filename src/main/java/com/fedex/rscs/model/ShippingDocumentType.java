package com.fedex.rscs.model;


/**
 * Represents the shipping document types.
 * 
 * @author 3811225
 *
 */
public enum ShippingDocumentType {

    OUTBOUND_LABEL("LABEL"), COMMERCIAL_INVOICE("COMMERCIAL_INVOICE"), CERTIFICATE_OF_ORIGIN(
            "CERTIFICATE_OF_ORIGIN"), PRO_FORMA_INVOICE("PRO_FORMA_INVOICE");


    private String value;

    /**
     * @param value
     */
    ShippingDocumentType(String value) {

        this.value = value;
    }

    /**
     * @return the value
     */
    public String getValue() {

        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(
            String value) {

        this.value = value;
    }

}
