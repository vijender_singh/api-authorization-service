package com.fedex.rscs.model.common;

import java.util.List;

/**
 * Define PackagePreauthorizedRatedDetail specification.
 * 
 * @author 5034922
 *
 */
public class PackagePreauthorizedRatedDetail {

    private int groupNumber;
    private String rateType;
    private String ratedWeightMethod;
    private String minimumChargeType;
    private Weight billingWeight;
    private Weight dimWeight;
    private Weight oversizeWeight;
    private String ratingType;
    private Price baseCharge;
    private Price totalFreightDiscounts;
    private Price netFreight;
    private Price totalSurcharges;
    private Price netFedExCharge;
    private Price totalTaxes;
    private Price netCharge;
    private Price totalRebates;
    private List<ShippingRateDiscount> freightDiscounts;
    private List<ShippingRateDiscount> rebates;
    private List<ShippingRateSurcharge> surcharges;
    private List<ShippingRateTax> taxes;

    public PackagePreauthorizedRatedDetail() {

    }

    /**
     * @return the groupNumber
     */
    public int getGroupNumber() {

        return groupNumber;
    }

    /**
     * @param groupNumber the groupNumber to set
     */
    public void setGroupNumber(
            int groupNumber) {

        this.groupNumber = groupNumber;
    }

    /**
     * @return the rateType
     */
    public String getRateType() {

        return rateType;
    }

    /**
     * @param rateType the rateType to set
     */
    public void setRateType(
            String rateType) {

        this.rateType = rateType;
    }

    /**
     * @return the ratedWeightMethod
     */
    public String getRatedWeightMethod() {

        return ratedWeightMethod;
    }

    /**
     * @param ratedWeightMethod the ratedWeightMethod to set
     */
    public void setRatedWeightMethod(
            String ratedWeightMethod) {

        this.ratedWeightMethod = ratedWeightMethod;
    }

    /**
     * @return the minimumChargeType
     */
    public String getMinimumChargeType() {

        return minimumChargeType;
    }

    /**
     * @param minimumChargeType the minimumChargeType to set
     */
    public void setMinimumChargeType(
            String minimumChargeType) {

        this.minimumChargeType = minimumChargeType;
    }

    /**
     * @return the billingWeight
     */
    public Weight getBillingWeight() {

        return billingWeight;
    }

    /**
     * @param billingWeight the billingWeight to set
     */
    public void setBillingWeight(
            Weight billingWeight) {

        this.billingWeight = billingWeight;
    }

    /**
     * @return the dimWeight
     */
    public Weight getDimWeight() {

        return dimWeight;
    }

    /**
     * @param dimWeight the dimWeight to set
     */
    public void setDimWeight(
            Weight dimWeight) {

        this.dimWeight = dimWeight;
    }

    /**
     * @return the oversizeWeight
     */
    public Weight getOversizeWeight() {

        return oversizeWeight;
    }

    /**
     * @param oversizeWeight the oversizeWeight to set
     */
    public void setOversizeWeight(
            Weight oversizeWeight) {

        this.oversizeWeight = oversizeWeight;
    }

    /**
     * @return the ratingType
     */
    public String getRatingType() {

        return ratingType;
    }

    /**
     * @param ratingType the ratingType to set
     */
    public void setRatingType(
            String ratingType) {

        this.ratingType = ratingType;
    }

    /**
     * @return the baseCharge
     */
    public Price getBaseCharge() {

        return baseCharge;
    }

    /**
     * @param baseCharge the baseCharge to set
     */
    public void setBaseCharge(
            Price baseCharge) {

        this.baseCharge = baseCharge;
    }

    /**
     * @return the totalFreightDiscounts
     */
    public Price getTotalFreightDiscounts() {

        return totalFreightDiscounts;
    }

    /**
     * @param totalFreightDiscounts the totalFreightDiscounts to set
     */
    public void setTotalFreightDiscounts(
            Price totalFreightDiscounts) {

        this.totalFreightDiscounts = totalFreightDiscounts;
    }

    /**
     * @return the netFreight
     */
    public Price getNetFreight() {

        return netFreight;
    }

    /**
     * @param netFreight the netFreight to set
     */
    public void setNetFreight(
            Price netFreight) {

        this.netFreight = netFreight;
    }

    /**
     * @return the totalSurcharges
     */
    public Price getTotalSurcharges() {

        return totalSurcharges;
    }

    /**
     * @param totalSurcharges the totalSurcharges to set
     */
    public void setTotalSurcharges(
            Price totalSurcharges) {

        this.totalSurcharges = totalSurcharges;
    }

    /**
     * @return the netFedExCharge
     */
    public Price getNetFedExCharge() {

        return netFedExCharge;
    }

    /**
     * @param netFedExCharge the netFedExCharge to set
     */
    public void setNetFedExCharge(
            Price netFedExCharge) {

        this.netFedExCharge = netFedExCharge;
    }

    /**
     * @return the totalTaxes
     */
    public Price getTotalTaxes() {

        return totalTaxes;
    }

    /**
     * @param totalTaxes the totalTaxes to set
     */
    public void setTotalTaxes(
            Price totalTaxes) {

        this.totalTaxes = totalTaxes;
    }

    /**
     * @return the netCharge
     */
    public Price getNetCharge() {

        return netCharge;
    }

    /**
     * @param netCharge the netCharge to set
     */
    public void setNetCharge(
            Price netCharge) {

        this.netCharge = netCharge;
    }

    /**
     * @return the totalRebates
     */
    public Price getTotalRebates() {

        return totalRebates;
    }

    /**
     * @param totalRebates the totalRebates to set
     */
    public void setTotalRebates(
            Price totalRebates) {

        this.totalRebates = totalRebates;
    }

    /**
     * @return the freightDiscounts
     */
    public List<ShippingRateDiscount> getFreightDiscounts() {

        return freightDiscounts;
    }

    /**
     * @param freightDiscounts the freightDiscounts to set
     */
    public void setFreightDiscounts(
            List<ShippingRateDiscount> freightDiscounts) {

        this.freightDiscounts = freightDiscounts;
    }

    /**
     * @return the rebates
     */
    public List<ShippingRateDiscount> getRebates() {

        return rebates;
    }

    /**
     * @param rebates the rebates to set
     */
    public void setRebates(
            List<ShippingRateDiscount> rebates) {

        this.rebates = rebates;
    }

    /**
     * @return the surcharges
     */
    public List<ShippingRateSurcharge> getSurcharges() {

        return surcharges;
    }

    /**
     * @param surcharges the surcharges to set
     */
    public void setSurcharges(
            List<ShippingRateSurcharge> surcharges) {

        this.surcharges = surcharges;
    }

    /**
     * @return the taxes
     */
    public List<ShippingRateTax> getTaxes() {

        return taxes;
    }

    /**
     * @param taxes the taxes to set
     */
    public void setTaxes(
            List<ShippingRateTax> taxes) {

        this.taxes = taxes;
    }

    @Override
    public String toString() {

        return "PackagePreauthorizedRatedDetail [groupNumber=" + groupNumber + ", rateType=" + rateType
                + ", ratedWeightMethod=" + ratedWeightMethod + ", minimumChargeType=" + minimumChargeType
                + ", billingWeight=" + billingWeight + ", dimWeight=" + dimWeight + ", oversizeWeight=" + oversizeWeight
                + ", ratingType=" + ratingType + ", baseCharge=" + baseCharge + ", totalFreightDiscounts="
                + totalFreightDiscounts + ", netFreight=" + netFreight + ", totalSurcharges=" + totalSurcharges
                + ", netFedExCharge=" + netFedExCharge + ", totalTaxes=" + totalTaxes + ", netCharge=" + netCharge
                + ", totalRebates=" + totalRebates + ", freightDiscounts=" + freightDiscounts + ", rebates=" + rebates
                + ", surcharges=" + surcharges + ", taxes=" + taxes + "]";
    }

}
