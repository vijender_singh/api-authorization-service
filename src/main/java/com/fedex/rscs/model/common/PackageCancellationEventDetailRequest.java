package com.fedex.rscs.model.common;

import java.util.List;

/**
 * Represents Possesion Package Event Cancellation Request Detail Model class. It is mapped to DTO's
 * {@link com.fedex.rscs.client.pes.dto.PackageCancellationEventRequestData} object.
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackageCancellationEventDetailRequest {

    private TransactionHeaderDetail headerDetails;
    private LocationDataDetail locationDetails;
    private List<PackageLineItemDetail> packageLineItems;

    /**
     * @return the headerDetails
     */
    public TransactionHeaderDetail getHeaderDetails() {

        return headerDetails;
    }

    /**
     * @param headerDetails the headerDetails to set
     */
    public void setHeaderDetails(
            TransactionHeaderDetail headerDetails) {

        this.headerDetails = headerDetails;
    }

    /**
     * @return the locationDetails
     */
    public LocationDataDetail getLocationDetails() {

        return locationDetails;
    }

    /**
     * @param locationDetails the locationDetails to set
     */
    public void setLocationDetails(
            LocationDataDetail locationDetails) {

        this.locationDetails = locationDetails;
    }

    /**
     * @return the packageLineItems
     */
    public List<PackageLineItemDetail> getPackageLineItems() {

        return packageLineItems;
    }

    /**
     * @param packageLineItems the packageLineItems to set
     */
    public void setPackageLineItems(
            List<PackageLineItemDetail> packageLineItems) {

        this.packageLineItems = packageLineItems;
    }

    @Override
    public String toString() {

        return "PackagePossessionEventDetailRequest [headerDetails=" + headerDetails + ", locationDetails="
                + locationDetails + ", packageLineItems=" + packageLineItems + "]";
    }

}
