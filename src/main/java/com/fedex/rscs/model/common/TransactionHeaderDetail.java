package com.fedex.rscs.model.common;

/**
 * Represents the header details model.
 * 
 * @author Shakti.Saurabh
 *
 */
public class TransactionHeaderDetail {

    private String guid;
    private String requestDateTime;
    private String teamMemberId;
    private DeviceDetail workstationDetails;

    public TransactionHeaderDetail() {}

    /**
     * @param guid
     * @param requestDateTime
     * @param teamMemberId
     * @param workstationDetails
     */
    public TransactionHeaderDetail(String guid, String requestDateTime, String teamMemberId,
            DeviceDetail workstationDetails) {

        this.guid = guid;
        this.requestDateTime = requestDateTime;
        this.teamMemberId = teamMemberId;
        this.workstationDetails = workstationDetails;
    }

    /**
     * @return the guid
     */
    public String getGuid() {

        return guid;
    }

    /**
     * @param guid the guid to set
     */
    public void setGuid(
            String guid) {

        this.guid = guid;
    }

    /**
     * @return the requestDateTime
     */
    public String getRequestDateTime() {

        return requestDateTime;
    }

    /**
     * @param requestDateTime the requestDateTime to set
     */
    public void setRequestDateTime(
            String requestDateTime) {

        this.requestDateTime = requestDateTime;
    }

    /**
     * @return the teamMemberId
     */
    public String getTeamMemberId() {

        return teamMemberId;
    }

    /**
     * @param teamMemberId the teamMemberId to set
     */
    public void setTeamMemberId(
            String teamMemberId) {

        this.teamMemberId = teamMemberId;
    }

    /**
     * @return the workstationDetails
     */
    public DeviceDetail getWorkstationDetails() {

        return workstationDetails;
    }

    /**
     * @param workstationDetails the workstationDetails to set
     */
    public void setWorkstationDetails(
            DeviceDetail workstationDetails) {

        this.workstationDetails = workstationDetails;
    }

    public String toString() {

        return "HeaderDetail [guid=" + guid + ", requestDateTime=" + requestDateTime + ", teamMemberId=" + teamMemberId
                + ", workstationDetails=" + workstationDetails + "]";
    }

}
