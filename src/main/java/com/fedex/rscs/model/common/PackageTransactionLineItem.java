package com.fedex.rscs.model.common;

import java.util.ArrayList;
import java.util.List;


/**
 * Represents the premetered shipment transaction details. It is mapped to DTO's
 * {@link com.fedex.rscs.PossessionEvent.dto.PremeteredTransactionDetail} object.
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackageTransactionLineItem  {


    private String trackingNumber;
    private boolean accepted;
    private List<EventNotification> eventNotification = new ArrayList<>();
    
    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
    
        return trackingNumber;
    }
    
    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {
    
        this.trackingNumber = trackingNumber;
    }
    
    /**
     * @return the accepted
     */
    public boolean isAccepted() {
    
        return accepted;
    }
    
    /**
     * @param accepted the accepted to set
     */
    public void setAccepted(
            boolean accepted) {
    
        this.accepted = accepted;
    }
    
    /**
     * @return the eventNotification
     */
    public List<EventNotification> getEventNotification() {
    
        return eventNotification;
    }
    
    /**
     * @param eventNotification the eventNotification to set
     */
    public void setEventNotification(
            List<EventNotification> eventNotification) {
    
        this.eventNotification = eventNotification;
    }

    @Override
    public String toString() {

        return "PackageTransactionLineItem [trackingNumber=" + trackingNumber + ", accepted=" + accepted
                + ", eventNotification=" + eventNotification + "]";
    }

}
