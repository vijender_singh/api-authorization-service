package com.fedex.rscs.model.common;

/**
 * Shipping Rate Surcharge
 * 
 * @author 3932956
 *
 */
public class ShippingRateSurcharge {

    private String surchargeType;
    private String description;
    private double amount;
    private SurchargeClassification surchargeClassification;

    public ShippingRateSurcharge() {

    }

    /**
     * @return the surchargeType
     */
    public String getSurchargeType() {

        return surchargeType;
    }

    /**
     * @param surchargeType the surchargeType to set
     */
    public void setSurchargeType(
            String surchargeType) {

        this.surchargeType = surchargeType;
    }

    /**
     * @return the description
     */
    public String getDescription() {

        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(
            String description) {

        this.description = description;
    }

    /**
     * @return the amount
     */
    public double getAmount() {

        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(
            double amount) {

        this.amount = amount;
    }
    
    /**
     * @return the surchargeClassification
     */
    public SurchargeClassification getSurchargeClassification() {
    
        return surchargeClassification;
    }

    /**
     * @param surchargeClassification the surchargeClassification to set
     */
    public void setSurchargeClassification(
            SurchargeClassification surchargeClassification) {
    
        this.surchargeClassification = surchargeClassification;
    }

    @Override
    public String toString() {

        return "ShippingRateSurcharge [surchargeType=" + surchargeType + ", description=" + description + ", amount="
                + amount + ", surchargeClassification=" + surchargeClassification + "]";
    } 
}
