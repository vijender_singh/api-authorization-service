package com.fedex.rscs.model.common;

/**
 * Enum to hold units
 * 
 * @author 3932968
 *
 */
public enum Units {
    LB, KG;
}
