package com.fedex.rscs.model.common;

/**
 * Represents model class of LocationDetail It is mapped to DTO's
 * {@link com.fedex.rscs.dto.LocationDetail} object.
 * 
 * @author Shakti.Saurabh
 *
 */
public class LocationDataDetail {

    private String cityCenterAccountNumber;
    private String groundAccountNumber;
    private Location transactionLocationInfo;
    private boolean groundAfterPickup;
    private boolean expressAfterPickup;
    private Location servingLocationInfo;
    private String opCo;
    private String opCoLocationId;

    public LocationDataDetail() {}
    
    /**
     * @param cityCenterAccountNumber
     * @param groundAccountNumber
     * @param transactionLocationInfo
     * @param groundAfterPickup
     * @param expressAfterPickup
     * @param servingLocationInfo
     * @param opCo
     * @param opCoLocationId
     */
    public LocationDataDetail(String cityCenterAccountNumber, String groundAccountNumber,
            Location transactionLocationInfo, boolean groundAfterPickup, boolean expressAfterPickup,
            Location servingLocationInfo, String opCo, String opCoLocationId) {

        this.cityCenterAccountNumber = cityCenterAccountNumber;
        this.groundAccountNumber = groundAccountNumber;
        this.transactionLocationInfo = transactionLocationInfo;
        this.groundAfterPickup = groundAfterPickup;
        this.expressAfterPickup = expressAfterPickup;
        this.servingLocationInfo = servingLocationInfo;
        this.opCo = opCo;
        this.opCoLocationId = opCoLocationId;
    }

    /**
     * @return the cityCenterAccountNumber
     */
    public String getCityCenterAccountNumber() {

        return cityCenterAccountNumber;
    }

    /**
     * @param cityCenterAccountNumber the cityCenterAccountNumber to set
     */
    public void setCityCenterAccountNumber(
            String cityCenterAccountNumber) {

        this.cityCenterAccountNumber = cityCenterAccountNumber;
    }

    /**
     * @return the groundAccountNumber
     */
    public String getGroundAccountNumber() {

        return groundAccountNumber;
    }

    /**
     * @param groundAccountNumber the groundAccountNumber to set
     */
    public void setGroundAccountNumber(
            String groundAccountNumber) {

        this.groundAccountNumber = groundAccountNumber;
    }

    /**
     * @return the transactionLocationInfo
     */
    public Location getTransactionLocationInfo() {

        return transactionLocationInfo;
    }

    /**
     * @param transactionLocationInfo the transactionLocationInfo to set
     */
    public void setTransactionLocationInfo(
            Location transactionLocationInfo) {

        this.transactionLocationInfo = transactionLocationInfo;
    }

    /**
     * @return the groundAfterPickup
     */
    public boolean isGroundAfterPickup() {

        return groundAfterPickup;
    }

    /**
     * @param groundAfterPickup the groundAfterPickup to set
     */
    public void setGroundAfterPickup(
            boolean groundAfterPickup) {

        this.groundAfterPickup = groundAfterPickup;
    }

    /**
     * @return the expressAfterPickup
     */
    public boolean isExpressAfterPickup() {

        return expressAfterPickup;
    }

    /**
     * @param expressAfterPickup the expressAfterPickup to set
     */
    public void setExpressAfterPickup(
            boolean expressAfterPickup) {

        this.expressAfterPickup = expressAfterPickup;
    }

    /**
     * @return the servingLocationInfo
     */
    public Location getServingLocationInfo() {

        return servingLocationInfo;
    }

    /**
     * @param servingLocationInfo the servingLocationInfo to set
     */
    public void setServingLocationInfo(
            Location servingLocationInfo) {

        this.servingLocationInfo = servingLocationInfo;
    }
    
    /**
     * @return the opCo
     */
    public String getOpCo() {
    
        return opCo;
    }
    
    /**
     * @param opCo the opCo to set
     */
    public void setOpCo(
            String opCo) {
    
        this.opCo = opCo;
    }
    
    /**
     * @return the opCoLocationId
     */
    public String getOpCoLocationId() {
    
        return opCoLocationId;
    }

    /**
     * @param opCoLocationId the opCoLocationId to set
     */
    public void setOpCoLocationId(
            String opCoLocationId) {
    
        this.opCoLocationId = opCoLocationId;
    }

    @Override
    public String toString() {

        return "LocationDataDetail [cityCenterAccountNumber=" + cityCenterAccountNumber + ", groundAccountNumber="
                + groundAccountNumber + ", transactionLocationInfo=" + transactionLocationInfo + ", groundAfterPickup="
                + groundAfterPickup + ", expressAfterPickup=" + expressAfterPickup + ", servingLocationInfo="
                + servingLocationInfo + ", opCo=" + opCo + ", opCoLocationId=" + opCoLocationId + "]";
    }

}
   