package com.fedex.rscs.model.common;

import java.util.List;

/**
 * Define the PreauthorizedRateServiceDetail specification
 * 
 * @author 5034922
 *
 */
public class PreauthorizedRateServiceDetail {

    private String serviceType;
    private String serviceName;
    private String description;
    private String serviceId;
    private String code;
    private List<String> operatingOrgCodes;
    private String serviceCategory;
    private String astraDescription;

    public PreauthorizedRateServiceDetail() {

    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {

        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(
            String serviceType) {

        this.serviceType = serviceType;
    }

    /**
     * @return the serviceName
     */
    public String getServiceName() {

        return serviceName;
    }

    /**
     * @param serviceName the serviceName to set
     */
    public void setServiceName(
            String serviceName) {

        this.serviceName = serviceName;
    }

    /**
     * @return the description
     */
    public String getDescription() {

        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(
            String description) {

        this.description = description;
    }

    /**
     * @return the serviceId
     */
    public String getServiceId() {

        return serviceId;
    }

    /**
     * @param serviceId the serviceId to set
     */
    public void setServiceId(
            String serviceId) {

        this.serviceId = serviceId;
    }

    /**
     * @return the code
     */
    public String getCode() {

        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(
            String code) {

        this.code = code;
    }

    /**
     * @return the operatingOrgCodes
     */
    public List<String> getOperatingOrgCodes() {

        return operatingOrgCodes;
    }

    /**
     * @param operatingOrgCodes the operatingOrgCodes to set
     */
    public void setOperatingOrgCodes(
            List<String> operatingOrgCodes) {

        this.operatingOrgCodes = operatingOrgCodes;
    }

    /**
     * @return the serviceCategory
     */
    public String getServiceCategory() {

        return serviceCategory;
    }

    /**
     * @param serviceCategory the serviceCategory to set
     */
    public void setServiceCategory(
            String serviceCategory) {

        this.serviceCategory = serviceCategory;
    }

    /**
     * @return the astraDescription
     */
    public String getAstraDescription() {

        return astraDescription;
    }

    /**
     * @param astraDescription the astraDescription to set
     */
    public void setAstraDescription(
            String astraDescription) {

        this.astraDescription = astraDescription;
    }

    @Override
    public String toString() {

        return "PreauthorizedRateServiceDetail [serviceType=" + serviceType + ", serviceName=" + serviceName
                + ", description=" + description + ", serviceId=" + serviceId + ", code=" + code
                + ", operatingOrgCodes=" + operatingOrgCodes + ", serviceCategory=" + serviceCategory
                + ", astraDescription=" + astraDescription + "]";
    }

}

