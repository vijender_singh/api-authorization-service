package com.fedex.rscs.model.common;

import java.math.BigDecimal;

/**
 * Define CurrencyExchangeRate specification.
 * 
 * @author 5034922
 *
 */
public class CurrencyExchangeRate {

    private CurrencyType fromCurrency;
    private CurrencyType intoCurrency;
    private BigDecimal rate;

    public CurrencyExchangeRate() {

    }


    /**
     * @return the fromCurrency
     */
    public CurrencyType getFromCurrency() {

        return fromCurrency;
    }


    /**
     * @param fromCurrency the fromCurrency to set
     */
    public void setFromCurrency(
            CurrencyType fromCurrency) {

        this.fromCurrency = fromCurrency;
    }


    /**
     * @return the intoCurrency
     */
    public CurrencyType getIntoCurrency() {

        return intoCurrency;
    }


    /**
     * @param intoCurrency the intoCurrency to set
     */
    public void setIntoCurrency(
            CurrencyType intoCurrency) {

        this.intoCurrency = intoCurrency;
    }


    /**
     * @return the rate
     */
    public BigDecimal getRate() {

        return rate;
    }


    /**
     * @param rate the rate to set
     */
    public void setRate(
            BigDecimal rate) {

        this.rate = rate;
    }


    @Override
    public String toString() {

        return "CurrencyExchangeRate [fromCurrency=" + fromCurrency + ", intoCurrency=" + intoCurrency + ", rate="
                + rate + "]";
    }

}
