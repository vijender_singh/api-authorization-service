package com.fedex.rscs.model.common;

/**
 * Enum to hold currency
 * 
 * @author 3932968
 *
 */
public enum CurrencyType {
    USD;
}
