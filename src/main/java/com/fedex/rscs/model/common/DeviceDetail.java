package com.fedex.rscs.model.common;

/**
 * Represents shipment workstation details.
 * 
 * @author shakti.saurabh
 *
 */
public class DeviceDetail {

    private String deviceId;
    private String meterNumber;
    private String softwareId;
    private String appName;
    private String appVersionId;

    public DeviceDetail() {}

    /**
     * @param deviceId
     * @param meterNumber
     * @param softwareId
     * @param appName
     * @param appVersionId
     */
    public DeviceDetail(String deviceId, String meterNumber, String softwareId, String appName, String appVersionId) {

        this.deviceId = deviceId;
        this.meterNumber = meterNumber;
        this.softwareId = softwareId;
        this.appName = appName;
        this.appVersionId = appVersionId;
    }


    /**
     * @return the deviceId
     */
    public String getDeviceId() {

        return deviceId;
    }


    /**
     * @param deviceId the deviceId to set
     */
    public void setDeviceId(
            String deviceId) {

        this.deviceId = deviceId;
    }


    /**
     * @return the meterNumber
     */
    public String getMeterNumber() {

        return meterNumber;
    }


    /**
     * @param meterNumber the meterNumber to set
     */
    public void setMeterNumber(
            String meterNumber) {

        this.meterNumber = meterNumber;
    }


    /**
     * @return the softwareId
     */
    public String getSoftwareId() {

        return softwareId;
    }


    /**
     * @param softwareId the softwareId to set
     */
    public void setSoftwareId(
            String softwareId) {

        this.softwareId = softwareId;
    }


    /**
     * @return the appName
     */
    public String getAppName() {

        return appName;
    }


    /**
     * @param appName the appName to set
     */
    public void setAppName(
            String appName) {

        this.appName = appName;
    }


    /**
     * @return the appVersionId
     */
    public String getAppVersionId() {

        return appVersionId;
    }


    /**
     * @param appVersionId the appVersionId to set
     */
    public void setAppVersionId(
            String appVersionId) {

        this.appVersionId = appVersionId;
    }

    @Override
    public boolean equals(
            Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DeviceDetail other = (DeviceDetail) obj;
        if (appName == null) {
            if (other.appName != null)
                return false;
        } else if (!appName.equals(other.appName))
            return false;
        if (appVersionId == null) {
            if (other.appVersionId != null)
                return false;
        } else if (!appVersionId.equals(other.appVersionId))
            return false;
        if (deviceId == null) {
            if (other.deviceId != null)
                return false;
        } else if (!deviceId.equals(other.deviceId))
            return false;
        if (meterNumber == null) {
            if (other.meterNumber != null)
                return false;
        } else if (!meterNumber.equals(other.meterNumber))
            return false;
        if (softwareId == null) {
            if (other.softwareId != null)
                return false;
        } else if (!softwareId.equals(other.softwareId))
            return false;
        return true;
    }

    @Override
    public String toString() {

        return "WorkstationDetail [deviceId=" + deviceId + ", meterNumber=" + meterNumber + ", softwareId=" + softwareId
                + ", appName=" + appName + ", appVersionId=" + appVersionId + "]";
    }
}
