package com.fedex.rscs.model.common;

import java.util.List;

import com.fedex.rscs.model.PaymentDetail;

/**
 * Represents Possesion Package Event Request Detail Model class. It is mapped to DTO's
 * {@link com.fedex.rscs.client.pes.dto.PackagePossessionEventRequest} object.
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackagePossessionEventDetailRequest {

    private TransactionHeaderDetail headerDetails;
    private LocationDataDetail locationDetails;
    private PaymentDetail paymentDetails;
    private List<PackagePossessionEventDetail> packageLineItems;

    /**
     * @return the headerDetails
     */
    public TransactionHeaderDetail getHeaderDetails() {

        return headerDetails;
    }

    /**
     * @param headerDetails the headerDetails to set
     */
    public void setHeaderDetails(
            TransactionHeaderDetail headerDetails) {

        this.headerDetails = headerDetails;
    }

    /**
     * @return the locationDetails
     */
    public LocationDataDetail getLocationDetails() {

        return locationDetails;
    }

    /**
     * @param locationDetails the locationDetails to set
     */
    public void setLocationDetails(
            LocationDataDetail locationDetails) {

        this.locationDetails = locationDetails;
    }

    /**
     * @return the paymentDetails
     */
    public PaymentDetail getPaymentDetails() {
    
        return paymentDetails;
    }

    /**
     * @param paymentDetails the paymentDetails to set
     */
    public void setPaymentDetails(
            PaymentDetail paymentDetails) {
    
        this.paymentDetails = paymentDetails;
    }

    /**
     * @return the packageLineItems
     */
    public List<PackagePossessionEventDetail> getPackageLineItems() {
    
        return packageLineItems;
    }

    /**
     * @param packageLineItems the packageLineItems to set
     */
    public void setPackageLineItems(
            List<PackagePossessionEventDetail> packageLineItems) {
    
        this.packageLineItems = packageLineItems;
    }

    @Override
    public String toString() {

        return "PackagePossessionEventDetailRequest [headerDetails=" + headerDetails + ", locationDetails="
                + locationDetails + ", paymentDetails=" + paymentDetails + ", packageLineItems=" + packageLineItems
                + "]";
    }

}
