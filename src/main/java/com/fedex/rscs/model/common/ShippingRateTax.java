package com.fedex.rscs.model.common;

/**
 * Shipping rate tax dto for taxes
 * 
 * @author 3883424
 *
 */
public class ShippingRateTax {

    private CurrencyType currency;
    private String taxType;
    private String description;
    private double amount;

    public ShippingRateTax() {

    }

    /**
     * @return the currency
     */
    public CurrencyType getCurrency() {

        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(
            CurrencyType currency) {

        this.currency = currency;
    }

    /**
     * @return the taxType
     */
    public String getTaxType() {

        return taxType;
    }

    /**
     * @param taxType the taxType to set
     */
    public void setTaxType(
            String taxType) {

        this.taxType = taxType;
    }

    /**
     * @return the description
     */
    public String getDescription() {

        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(
            String description) {

        this.description = description;
    }

    /**
     * @return the amount
     */
    public double getAmount() {

        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(
            double amount) {

        this.amount = amount;
    }

    @Override
    public String toString() {

        return "ShippingRateTax [currency=" + currency + ", taxType=" + taxType + ", description=" + description
                + ", amount=" + amount + "]";
    }

}
