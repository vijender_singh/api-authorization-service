package com.fedex.rscs.model.common;

import java.util.List;

/**
 * Define the ShipmentPreauthorizedRatedDetail specification.
 * 
 * @author 5034922
 *
 */
public class ShipmentPreauthorizedRatedDetail {

    private String rateType;
    private String rateScale;
    private String rateZone;
    private String pricingCode;
    private String ratedWeightMethod;
    private String minimumChargeType;
    private CurrencyExchangeRate currencyExchangeRate;
    private List<String> specialRatingApplied;
    private int dimDivisor;
    private String dimDivisorType;
    private String fuelSurchargePercent;
    private Weight totalBillingWeight;
    private Weight totalDimWeight;
    private Price totalBaseCharge;
    private Price totalFreightDiscounts;
    private Price totalNetFreight;
    private Price totalSurcharges;
    private Price totalNetFedExCharge;
    private Price totalTaxes;
    private Price totalNetCharge;
    private Price totalRebates;
    private Price totalDutiesAndTaxes;
    private Price totalAncillaryFeesAndTaxes;
    private Price totalDutiesTaxesAndFees;
    private Price totalNetChargeWithDutiesAndTaxes;
    private List<ShippingRateDiscount> freightDiscounts;
    private List<RebatesDetails> rebates;
    private List<ShippingRateSurcharge> surcharges;
    private List<ShippingRateTax> taxes;
    private List<ShippingRateTax> ancillaryFeesAndTaxes;

    public ShipmentPreauthorizedRatedDetail() {

    }

    /**
     * @return the rateType
     */
    public String getRateType() {

        return rateType;
    }

    /**
     * @param rateType the rateType to set
     */
    public void setRateType(
            String rateType) {

        this.rateType = rateType;
    }

    /**
     * @return the rateScale
     */
    public String getRateScale() {

        return rateScale;
    }

    /**
     * @param rateScale the rateScale to set
     */
    public void setRateScale(
            String rateScale) {

        this.rateScale = rateScale;
    }

    /**
     * @return the rateZone
     */
    public String getRateZone() {

        return rateZone;
    }

    /**
     * @param rateZone the rateZone to set
     */
    public void setRateZone(
            String rateZone) {

        this.rateZone = rateZone;
    }

    /**
     * @return the pricingCode
     */
    public String getPricingCode() {

        return pricingCode;
    }

    /**
     * @param pricingCode the pricingCode to set
     */
    public void setPricingCode(
            String pricingCode) {

        this.pricingCode = pricingCode;
    }

    /**
     * @return the ratedWeightMethod
     */
    public String getRatedWeightMethod() {

        return ratedWeightMethod;
    }

    /**
     * @param ratedWeightMethod the ratedWeightMethod to set
     */
    public void setRatedWeightMethod(
            String ratedWeightMethod) {

        this.ratedWeightMethod = ratedWeightMethod;
    }

    /**
     * @return the minimumChargeType
     */
    public String getMinimumChargeType() {

        return minimumChargeType;
    }

    /**
     * @param minimumChargeType the minimumChargeType to set
     */
    public void setMinimumChargeType(
            String minimumChargeType) {

        this.minimumChargeType = minimumChargeType;
    }

    /**
     * @return the currencyExchangeRate
     */
    public CurrencyExchangeRate getCurrencyExchangeRate() {

        return currencyExchangeRate;
    }

    /**
     * @param currencyExchangeRate the currencyExchangeRate to set
     */
    public void setCurrencyExchangeRate(
            CurrencyExchangeRate currencyExchangeRate) {

        this.currencyExchangeRate = currencyExchangeRate;
    }

    /**
     * @return the specialRatingApplied
     */
    public List<String> getSpecialRatingApplied() {

        return specialRatingApplied;
    }

    /**
     * @param specialRatingApplied the specialRatingApplied to set
     */
    public void setSpecialRatingApplied(
            List<String> specialRatingApplied) {

        this.specialRatingApplied = specialRatingApplied;
    }

    /**
     * @return the dimDivisor
     */
    public int getDimDivisor() {

        return dimDivisor;
    }

    /**
     * @param dimDivisor the dimDivisor to set
     */
    public void setDimDivisor(
            int dimDivisor) {

        this.dimDivisor = dimDivisor;
    }

    /**
     * @return the dimDivisorType
     */
    public String getDimDivisorType() {

        return dimDivisorType;
    }

    /**
     * @param dimDivisorType the dimDivisorType to set
     */
    public void setDimDivisorType(
            String dimDivisorType) {

        this.dimDivisorType = dimDivisorType;
    }

    /**
     * @return the fuelSurchargePercent
     */
    public String getFuelSurchargePercent() {

        return fuelSurchargePercent;
    }

    /**
     * @param fuelSurchargePercent the fuelSurchargePercent to set
     */
    public void setFuelSurchargePercent(
            String fuelSurchargePercent) {

        this.fuelSurchargePercent = fuelSurchargePercent;
    }

    /**
     * @return the totalBillingWeight
     */
    public Weight getTotalBillingWeight() {

        return totalBillingWeight;
    }

    /**
     * @param totalBillingWeight the totalBillingWeight to set
     */
    public void setTotalBillingWeight(
            Weight totalBillingWeight) {

        this.totalBillingWeight = totalBillingWeight;
    }

    /**
     * @return the totalDimWeight
     */
    public Weight getTotalDimWeight() {

        return totalDimWeight;
    }

    /**
     * @param totalDimWeight the totalDimWeight to set
     */
    public void setTotalDimWeight(
            Weight totalDimWeight) {

        this.totalDimWeight = totalDimWeight;
    }

    /**
     * @return the totalBaseCharge
     */
    public Price getTotalBaseCharge() {

        return totalBaseCharge;
    }

    /**
     * @param totalBaseCharge the totalBaseCharge to set
     */
    public void setTotalBaseCharge(
            Price totalBaseCharge) {

        this.totalBaseCharge = totalBaseCharge;
    }

    /**
     * @return the totalFreightDiscounts
     */
    public Price getTotalFreightDiscounts() {

        return totalFreightDiscounts;
    }

    /**
     * @param totalFreightDiscounts the totalFreightDiscounts to set
     */
    public void setTotalFreightDiscounts(
            Price totalFreightDiscounts) {

        this.totalFreightDiscounts = totalFreightDiscounts;
    }

    /**
     * @return the totalNetFreight
     */
    public Price getTotalNetFreight() {

        return totalNetFreight;
    }

    /**
     * @param totalNetFreight the totalNetFreight to set
     */
    public void setTotalNetFreight(
            Price totalNetFreight) {

        this.totalNetFreight = totalNetFreight;
    }

    /**
     * @return the totalSurcharges
     */
    public Price getTotalSurcharges() {

        return totalSurcharges;
    }

    /**
     * @param totalSurcharges the totalSurcharges to set
     */
    public void setTotalSurcharges(
            Price totalSurcharges) {

        this.totalSurcharges = totalSurcharges;
    }

    /**
     * @return the totalNetFedExCharge
     */
    public Price getTotalNetFedExCharge() {

        return totalNetFedExCharge;
    }

    /**
     * @param totalNetFedExCharge the totalNetFedExCharge to set
     */
    public void setTotalNetFedExCharge(
            Price totalNetFedExCharge) {

        this.totalNetFedExCharge = totalNetFedExCharge;
    }

    /**
     * @return the totalTaxes
     */
    public Price getTotalTaxes() {

        return totalTaxes;
    }

    /**
     * @param totalTaxes the totalTaxes to set
     */
    public void setTotalTaxes(
            Price totalTaxes) {

        this.totalTaxes = totalTaxes;
    }

    /**
     * @return the totalNetCharge
     */
    public Price getTotalNetCharge() {

        return totalNetCharge;
    }

    /**
     * @param totalNetCharge the totalNetCharge to set
     */
    public void setTotalNetCharge(
            Price totalNetCharge) {

        this.totalNetCharge = totalNetCharge;
    }

    /**
     * @return the totalRebates
     */
    public Price getTotalRebates() {

        return totalRebates;
    }

    /**
     * @param totalRebates the totalRebates to set
     */
    public void setTotalRebates(
            Price totalRebates) {

        this.totalRebates = totalRebates;
    }

    /**
     * @return the totalDutiesAndTaxes
     */
    public Price getTotalDutiesAndTaxes() {

        return totalDutiesAndTaxes;
    }

    /**
     * @param totalDutiesAndTaxes the totalDutiesAndTaxes to set
     */
    public void setTotalDutiesAndTaxes(
            Price totalDutiesAndTaxes) {

        this.totalDutiesAndTaxes = totalDutiesAndTaxes;
    }

    /**
     * @return the totalAncillaryFeesAndTaxes
     */
    public Price getTotalAncillaryFeesAndTaxes() {

        return totalAncillaryFeesAndTaxes;
    }

    /**
     * @param totalAncillaryFeesAndTaxes the totalAncillaryFeesAndTaxes to set
     */
    public void setTotalAncillaryFeesAndTaxes(
            Price totalAncillaryFeesAndTaxes) {

        this.totalAncillaryFeesAndTaxes = totalAncillaryFeesAndTaxes;
    }

    /**
     * @return the totalDutiesTaxesAndFees
     */
    public Price getTotalDutiesTaxesAndFees() {

        return totalDutiesTaxesAndFees;
    }

    /**
     * @param totalDutiesTaxesAndFees the totalDutiesTaxesAndFees to set
     */
    public void setTotalDutiesTaxesAndFees(
            Price totalDutiesTaxesAndFees) {

        this.totalDutiesTaxesAndFees = totalDutiesTaxesAndFees;
    }

    /**
     * @return the totalNetChargeWithDutiesAndTaxes
     */
    public Price getTotalNetChargeWithDutiesAndTaxes() {

        return totalNetChargeWithDutiesAndTaxes;
    }

    /**
     * @param totalNetChargeWithDutiesAndTaxes the totalNetChargeWithDutiesAndTaxes to set
     */
    public void setTotalNetChargeWithDutiesAndTaxes(
            Price totalNetChargeWithDutiesAndTaxes) {

        this.totalNetChargeWithDutiesAndTaxes = totalNetChargeWithDutiesAndTaxes;
    }

    /**
     * @return the freightDiscounts
     */
    public List<ShippingRateDiscount> getFreightDiscounts() {

        return freightDiscounts;
    }

    /**
     * @param freightDiscounts the freightDiscounts to set
     */
    public void setFreightDiscounts(
            List<ShippingRateDiscount> freightDiscounts) {

        this.freightDiscounts = freightDiscounts;
    }

    /**
     * @return the rebates
     */
    public List<RebatesDetails> getRebates() {

        return rebates;
    }

    /**
     * @param rebates the rebates to set
     */
    public void setRebates(
            List<RebatesDetails> rebates) {

        this.rebates = rebates;
    }

    /**
     * @return the surcharges
     */
    public List<ShippingRateSurcharge> getSurcharges() {

        return surcharges;
    }

    /**
     * @param surcharges the surcharges to set
     */
    public void setSurcharges(
            List<ShippingRateSurcharge> surcharges) {

        this.surcharges = surcharges;
    }

    /**
     * @return the taxes
     */
    public List<ShippingRateTax> getTaxes() {

        return taxes;
    }

    /**
     * @param taxes the taxes to set
     */
    public void setTaxes(
            List<ShippingRateTax> taxes) {

        this.taxes = taxes;
    }

    /**
     * @return the ancillaryFeesAndTaxes
     */
    public List<ShippingRateTax> getAncillaryFeesAndTaxes() {

        return ancillaryFeesAndTaxes;
    }

    /**
     * @param ancillaryFeesAndTaxes the ancillaryFeesAndTaxes to set
     */
    public void setAncillaryFeesAndTaxes(
            List<ShippingRateTax> ancillaryFeesAndTaxes) {

        this.ancillaryFeesAndTaxes = ancillaryFeesAndTaxes;
    }

    @Override
    public String toString() {

        return "ShipmentPreauthorizedRatedDetail [rateType=" + rateType + ", rateScale=" + rateScale + ", rateZone="
                + rateZone + ", pricingCode=" + pricingCode + ", ratedWeightMethod=" + ratedWeightMethod
                + ", minimumChargeType=" + minimumChargeType + ", currencyExchangeRate=" + currencyExchangeRate
                + ", specialRatingApplied=" + specialRatingApplied + ", dimDivisor=" + dimDivisor + ", dimDivisorType="
                + dimDivisorType + ", fuelSurchargePercent=" + fuelSurchargePercent + ", totalBillingWeight="
                + totalBillingWeight + ", totalDimWeight=" + totalDimWeight + ", totalBaseCharge=" + totalBaseCharge
                + ", totalFreightDiscounts=" + totalFreightDiscounts + ", totalNetFreight=" + totalNetFreight
                + ", totalSurcharges=" + totalSurcharges + ", totalNetFedExCharge=" + totalNetFedExCharge
                + ", totalTaxes=" + totalTaxes + ", totalNetCharge=" + totalNetCharge + ", totalRebates=" + totalRebates
                + ", totalDutiesAndTaxes=" + totalDutiesAndTaxes + ", totalAncillaryFeesAndTaxes="
                + totalAncillaryFeesAndTaxes + ", totalDutiesTaxesAndFees=" + totalDutiesTaxesAndFees
                + ", totalNetChargeWithDutiesAndTaxes=" + totalNetChargeWithDutiesAndTaxes + ", freightDiscounts="
                + freightDiscounts + ", rebates=" + rebates + ", surcharges=" + surcharges + ", taxes=" + taxes
                + ", ancillaryFeesAndTaxes=" + ancillaryFeesAndTaxes + "]";
    }
}
