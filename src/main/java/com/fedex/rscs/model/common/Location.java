package com.fedex.rscs.model.common;

import com.fedex.rscs.model.AddressDetail;

/**
 * Location information specification.
 * 
 * @author Shakti.Saurabh
 *
 */
public class Location {

    private String code;
    private AddressDetail address;

    public Location() {}

    public Location(final String code, final AddressDetail address) {

        this.code = code;
        this.address = address;
    }

    /**
     * 
     * @return code
     */
    public String getCode() {

        return code;
    }

    /**
     * 
     * @param code to set code
     */
    public void setCode(
            String code) {

        this.code = code;
    }

    /**
     * 
     * @return address
     */
    public AddressDetail getAddress() {

        return address;
    }

    /**
     * 
     * @param address to set address
     */
    public void setAddress(
            AddressDetail address) {

        this.address = address;
    }

    @Override
    public String toString() {

        return "Location [code=" + code + ", address=" + address + "]";
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + (address == null ? 0 : address.hashCode());
        result = prime * result + (code == null ? 0 : code.hashCode());
        return result;
    }

    @Override
    public boolean equals(
            Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Location other = (Location) obj;
        if (code == null) {
            if (other.code != null) {
                return false;
            }
        } else if (!code.equals(other.code)) {
            return false;
        }
        if (address == null) {
            if (other.address != null) {
                return false;
            }
        } else if (!address.equals(other.address)) {
            return false;
        }
        return true;
    }


}
