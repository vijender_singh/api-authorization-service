package com.fedex.rscs.model.common;

import javax.validation.constraints.NotNull;

/**
 * Define the Price DTO
 * 
 * @author 3932968
 *
 */
public class Price {

    @NotNull
    private CurrencyType currency;
    @NotNull
    private double amount;

    public Price() {}

    /**
     * @param currency
     * @param amount
     */
    public Price(CurrencyType currency, double amount) {

        this.currency = currency;
        this.amount = amount;
    }

    /**
     * 
     * @return currency
     */
    public CurrencyType getCurrency() {

        return currency;
    }

    /**
     * 
     * @param currency to set currency
     */
    public void setCurrency(
            CurrencyType currency) {

        this.currency = currency;
    }

    /**
     * @return the amount
     */
    public double getAmount() {

        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(
            double amount) {

        this.amount = amount;
    }

    @Override
    public String toString() {

        return "Price [currency=" + currency + ", amount=" + amount + "]";
    }

}
