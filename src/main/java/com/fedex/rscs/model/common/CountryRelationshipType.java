package com.fedex.rscs.model.common;

/**
 * Enum for Country Relationship type
 * 
 * @author Shakti.Saurabh
 *
 */
public enum CountryRelationshipType {

    DOMESTIC("DOM"), INTERNATIONAL("INTL");

    private String countryRelationshipCode;

    CountryRelationshipType(String countryRelationshipCode) {
        this.countryRelationshipCode = countryRelationshipCode;
    }

    public String getCountryRelationshipCode() {

        return countryRelationshipCode;
    }
}
