package com.fedex.rscs.model.common;

import java.util.List;

import com.fedex.rscs.client.pes.dto.PAAIndicatorType;
import com.fedex.rscs.model.AddressDetail;
import com.fedex.rscs.model.InterlineShippingData;
import com.fedex.rscs.model.cshp.Price;

/**
 * Represents Package Detail Model class. It is mapped to DTO's
 * {@link com.fedex.rscs.client.pes.dto.PackagePossessionEventDetail} object.
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackagePossessionEventDetail {

    private String barcode;
    private String trackingNumber;
    private String formId;
    private String operatingCompany;
    private String packageType;
    PAAIndicatorType paaIndicator;
    private boolean reprintAstra;
    private Price shipmentChargeAmount;
    private OfferingDetail service;
    private List<OfferingDetail> serviceOptions;
    private String routingId;
    private AddressDetail destinationAddress;
    private String serviceAreaCode;
    private String groundAccountNumber;
    private CountryRelationshipType countryRelationshipType;
    private InterlineShippingData interlineShippingDetails;
    private boolean manualBarcodeEntry;

    public PackagePossessionEventDetail() {}

    /**
     * @return the barcode
     */
    public String getBarcode() {

        return barcode;
    }

    /**
     * @param barcode the barcode to set
     */
    public void setBarcode(
            String barcode) {

        this.barcode = barcode;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {

        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {

        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the formId
     */
    public String getFormId() {

        return formId;
    }

    /**
     * @param formId the formId to set
     */
    public void setFormId(
            String formId) {

        this.formId = formId;
    }

    /**
     * @return the operatingCompany
     */
    public String getOperatingCompany() {

        return operatingCompany;
    }

    /**
     * @param operatingCompany the operatingCompany to set
     */
    public void setOperatingCompany(
            String operatingCompany) {

        this.operatingCompany = operatingCompany;
    }

    /**
     * @return the paaIndicator
     */
    public PAAIndicatorType getPaaIndicator() {

        return paaIndicator;
    }

    /**
     * @param paaIndicator the paaIndicator to set
     */
    public void setPaaIndicator(
            PAAIndicatorType paaIndicator) {

        this.paaIndicator = paaIndicator;
    }

    /**
     * @return the reprintAstra
     */
    public boolean isReprintAstra() {

        return reprintAstra;
    }

    /**
     * @param reprintAstra the reprintAstra to set
     */
    public void setReprintAstra(
            boolean reprintAstra) {

        this.reprintAstra = reprintAstra;
    }

    /**
     * @return the service
     */
    public OfferingDetail getService() {

        return service;
    }

    /**
     * @param service the service to set
     */
    public void setService(
            OfferingDetail service) {

        this.service = service;
    }

    /**
     * @return the serviceOptions
     */
    public List<OfferingDetail> getServiceOptions() {

        return serviceOptions;
    }

    /**
     * @param serviceOptions the serviceOptions to set
     */
    public void setServiceOptions(
            List<OfferingDetail> serviceOptions) {

        this.serviceOptions = serviceOptions;
    }

    /**
     * @return the routingId
     */
    public String getRoutingId() {

        return routingId;
    }

    /**
     * @param routingId the routingId to set
     */
    public void setRoutingId(
            String routingId) {

        this.routingId = routingId;
    }

    /**
     * @return the destinationAddress
     */
    public AddressDetail getDestinationAddress() {

        return destinationAddress;
    }

    /**
     * @param destinationAddress the destinationAddress to set
     */
    public void setDestinationAddress(
            AddressDetail destinationAddress) {

        this.destinationAddress = destinationAddress;
    }

    /**
     * @return the countryRelationshipType
     */
    public CountryRelationshipType getCountryRelationshipType() {

        return countryRelationshipType;
    }

    /**
     * @param countryRelationshipType the countryRelationshipType to set
     */
    public void setCountryRelationshipType(
            CountryRelationshipType countryRelationshipType) {

        this.countryRelationshipType = countryRelationshipType;
    }

    /**
     * @return the packageType
     */
    public String getPackageType() {
    
        return packageType;
    }
    
    /**
     * @param packageType the packageType to set
     */
    public void setPackageType(
            String packageType) {
    
        this.packageType = packageType;
    }

    /**
     * @return the shipmentChargeAmount
     */
    public Price getShipmentChargeAmount() {
    
        return shipmentChargeAmount;
    }

    /**
     * @param shipmentChargeAmount the shipmentChargeAmount to set
     */
    public void setShipmentChargeAmount(
            Price shipmentChargeAmount) {
    
        this.shipmentChargeAmount = shipmentChargeAmount;
    }

    /**
     * @return the serviceAreaCode
     */
    public String getServiceAreaCode() {
    
        return serviceAreaCode;
    }

    /**
     * @param serviceAreaCode the serviceAreaCode to set
     */
    public void setServiceAreaCode(
            String serviceAreaCode) {
    
        this.serviceAreaCode = serviceAreaCode;
    }

    /**
     * @return the groundAccountNumber
     */
    public String getGroundAccountNumber() {
    
        return groundAccountNumber;
    }

    /**
     * @param groundAccountNumber the groundAccountNumber to set
     */
    public void setGroundAccountNumber(
            String groundAccountNumber) {
    
        this.groundAccountNumber = groundAccountNumber;
    }
    
    /**
     * @return the interlineShippingDetails
     */
    public InterlineShippingData getInterlineShippingDetails() {

        return interlineShippingDetails;
    }

    /**
     * @param interlineShippingDetails the interlineShippingDetails to set
     */
    public void setInterlineShippingDetails(
            InterlineShippingData interlineShippingDetails) {

        this.interlineShippingDetails = interlineShippingDetails;
    }
    
    /**
     * @return the manualBarcodeEntry
     */
    public boolean isManualBarcodeEntry() {

        return manualBarcodeEntry;
    }

    /**
     * @param manualBarcodeEntry the manualBarcodeEntry to set
     */
    public void setManualBarcodeEntry(
            boolean manualBarcodeEntry) {

        this.manualBarcodeEntry = manualBarcodeEntry;
    }

    @Override
    public String toString() {

        return "PackagePossessionEventDetail [barcode=" + barcode + ", trackingNumber=" + trackingNumber + ", formId="
                + formId + ", operatingCompany=" + operatingCompany + ", packageType=" + packageType + ", paaIndicator="
                + paaIndicator + ", reprintAstra=" + reprintAstra + ", shipmentChargeAmount=" + shipmentChargeAmount
                + ", service=" + service + ", serviceOptions=" + serviceOptions + ", routingId=" + routingId
                + ", destinationAddress=" + destinationAddress + ", serviceAreaCode=" + serviceAreaCode
                + ", groundAccountNumber=" + groundAccountNumber + ", countryRelationshipType="
                + countryRelationshipType + ", interlineShippingDetails=" + interlineShippingDetails
                + ", manualBarcodeEntry=" + manualBarcodeEntry + "]";
    }

}
