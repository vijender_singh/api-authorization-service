package com.fedex.rscs.model.common;


public enum SurchargeClassification {
    STANDARD, REQUESTED_SPECIAL_SERVICES
}
