package com.fedex.rscs.model.common;

import java.util.List;

/**
 * Represents Possession Package Event Detail Resource Model class. It is mapped to DTO's
 * {@link com.fedex.rscs.client.pes.dto.PackagePossessionEventResource} object.
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackagePossessionEventDetailResource {

    private List<PackageTransactionLineItem> lineItems;

    /**
     * @return the lineItems
     */
    public List<PackageTransactionLineItem> getLineItems() {
    
        return lineItems;
    }

    /**
     * @param lineItems the lineItems to set
     */
    public void setLineItems(
            List<PackageTransactionLineItem> lineItems) {
    
        this.lineItems = lineItems;
    }

    @Override
    public String toString() {

        return "PackagePossessionEventDetailResource [lineItems=" + lineItems + "]";
    }

}
