package com.fedex.rscs.model.common;

import java.util.List;

/**
 * Define the PreauthorizedRateCommitDetail specification
 * 
 * @author 5034922
 *
 */
public class PreauthorizedRateCommitDetail {

    private String commitTimestamp;
    private String dayOfWeek;
    private String commodityName;
    private String serviceType;
    private List<String> operatingOrgCodes;
    private String serviceCategory;
    private PreauthorizedRateServiceDetail serviceDescription;
    private ShipmentServiceAddressDetail derivedOriginDetail;
    private ShipmentServiceAddressDetail derivedDestinationDetail;
    private String destinationServiceArea;
    private String brokerToDestinationDays;
    private String documentContent;

    public PreauthorizedRateCommitDetail() {

    }
    
    /**
     * @return the commitTimestamp
     */
    public String getCommitTimestamp() {

        return commitTimestamp;
    }

    /**
     * @param commitTimestamp the commitTimestamp to set
     */
    public void setCommitTimestamp(
            String commitTimestamp) {

        this.commitTimestamp = commitTimestamp;
    }

    /**
     * @return the dayOfWeek
     */
    public String getDayOfWeek() {

        return dayOfWeek;
    }

    /**
     * @param dayOfWeek the dayOfWeek to set
     */
    public void setDayOfWeek(
            String dayOfWeek) {

        this.dayOfWeek = dayOfWeek;
    }

    /**
     * @return the commodityName
     */
    public String getCommodityName() {

        return commodityName;
    }

    /**
     * @param commodityName the commodityName to set
     */
    public void setCommodityName(
            String commodityName) {

        this.commodityName = commodityName;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {

        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(
            String serviceType) {

        this.serviceType = serviceType;
    }

    /**
     * @return the operatingOrgCodes
     */
    public List<String> getOperatingOrgCodes() {

        return operatingOrgCodes;
    }

    /**
     * @param operatingOrgCodes the operatingOrgCodes to set
     */
    public void setOperatingOrgCodes(
            List<String> operatingOrgCodes) {

        this.operatingOrgCodes = operatingOrgCodes;
    }

    /**
     * @return the serviceCategory
     */
    public String getServiceCategory() {

        return serviceCategory;
    }

    /**
     * @param serviceCategory the serviceCategory to set
     */
    public void setServiceCategory(
            String serviceCategory) {

        this.serviceCategory = serviceCategory;
    }

    /**
     * @return the serviceDescription
     */
    public PreauthorizedRateServiceDetail getServiceDescription() {

        return serviceDescription;
    }

    /**
     * @param serviceDescription the serviceDescription to set
     */
    public void setServiceDescription(
            PreauthorizedRateServiceDetail serviceDescription) {

        this.serviceDescription = serviceDescription;
    }

    /**
     * @return the derivedOriginDetail
     */
    public ShipmentServiceAddressDetail getDerivedOriginDetail() {

        return derivedOriginDetail;
    }

    /**
     * @param derivedOriginDetail the derivedOriginDetail to set
     */
    public void setDerivedOriginDetail(
            ShipmentServiceAddressDetail derivedOriginDetail) {

        this.derivedOriginDetail = derivedOriginDetail;
    }

    /**
     * @return the derivedDestinationDetail
     */
    public ShipmentServiceAddressDetail getDerivedDestinationDetail() {

        return derivedDestinationDetail;
    }

    /**
     * @param derivedDestinationDetail the derivedDestinationDetail to set
     */
    public void setDerivedDestinationDetail(
            ShipmentServiceAddressDetail derivedDestinationDetail) {

        this.derivedDestinationDetail = derivedDestinationDetail;
    }

    /**
     * @return the destinationServiceArea
     */
    public String getDestinationServiceArea() {

        return destinationServiceArea;
    }

    /**
     * @param destinationServiceArea the destinationServiceArea to set
     */
    public void setDestinationServiceArea(
            String destinationServiceArea) {

        this.destinationServiceArea = destinationServiceArea;
    }

    /**
     * @return the brokerToDestinationDays
     */
    public String getBrokerToDestinationDays() {

        return brokerToDestinationDays;
    }

    /**
     * @param brokerToDestinationDays the brokerToDestinationDays to set
     */
    public void setBrokerToDestinationDays(
            String brokerToDestinationDays) {

        this.brokerToDestinationDays = brokerToDestinationDays;
    }

    /**
     * @return the documentContent
     */
    public String getDocumentContent() {

        return documentContent;
    }

    /**
     * @param documentContent the documentContent to set
     */
    public void setDocumentContent(
            String documentContent) {

        this.documentContent = documentContent;
    }

    @Override
    public String toString() {

        return "PreauthorizedRateCommitDetail [commitTimestamp=" + commitTimestamp + ", dayOfWeek=" + dayOfWeek
                + ", commodityName=" + commodityName + ", serviceType=" + serviceType + ", operatingOrgCodes="
                + operatingOrgCodes + ", serviceCategory=" + serviceCategory + ", serviceDescription="
                + serviceDescription + ", derivedOriginDetail=" + derivedOriginDetail + ", derivedDestinationDetail="
                + derivedDestinationDetail + ", destinationServiceArea=" + destinationServiceArea
                + ", brokerToDestinationDays=" + brokerToDestinationDays + ", documentContent=" + documentContent + "]";
    }

}
