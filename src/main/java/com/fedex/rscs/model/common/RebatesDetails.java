package com.fedex.rscs.model.common;

/**
 * RebatesDetails specification dto.
 * 
 * @author 3883424
 *
 */
public class RebatesDetails {

    private CurrencyType currency;
    private double amount;
    private String rebateType;
    private String description;
    private double percent;

    public RebatesDetails() {

    }

    /**
     * @return the currency
     */
    public CurrencyType getCurrency() {

        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(
            CurrencyType currency) {

        this.currency = currency;
    }

    /**
     * @return the amount
     */
    public double getAmount() {

        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(
            double amount) {

        this.amount = amount;
    }

    /**
     * @return the rebateType
     */
    public String getRebateType() {

        return rebateType;
    }

    /**
     * @param rebateType the rebateType to set
     */
    public void setRebateType(
            String rebateType) {

        this.rebateType = rebateType;
    }

    /**
     * @return the description
     */
    public String getDescription() {

        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(
            String description) {

        this.description = description;
    }

    /**
     * @return the percent
     */
    public double getPercent() {

        return percent;
    }

    /**
     * @param percent the percent to set
     */
    public void setPercent(
            double percent) {

        this.percent = percent;
    }

    @Override
    public String toString() {

        return "RebatesDetails [currency=" + currency + ", amount=" + amount + ", rebateType=" + rebateType
                + ", description=" + description + ", percent=" + percent + "]";
    }
}
