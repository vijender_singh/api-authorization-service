package com.fedex.rscs.model.common;

/**
 * Represents Possession Package Event Detail Resource Model class. It is mapped to DTO's
 * {@link com.fedex.rscs.client.pes.dto.PersonName} object.
 * 
 * @author Shakti.Saurabh
 *
 */
public class PersonDetail {

    private String firstName;
    private String lastName;

    public PersonDetail() {}

    /**
     * @param firstName
     * @param lastName
     */
    public PersonDetail(String firstName, String lastName) {

        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {

        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(
            String firstName) {

        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {

        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(
            String lastName) {

        this.lastName = lastName;
    }

    @Override
    public String toString() {

        return "PersonName [firstName=" + firstName + ", lastName=" + lastName + "]";
    }

}
