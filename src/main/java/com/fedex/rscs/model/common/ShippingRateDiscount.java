package com.fedex.rscs.model.common;

/**
 * Shipping rate discount dto for freightDiscounts
 * 
 * @author 3883424
 *
 */
public class ShippingRateDiscount {

    private String discountType;
    private String description;
    private double amount;
    private double percent;
    private CurrencyType currency;

    public ShippingRateDiscount() {

    }

    /**
     * @return the discountType
     */
    public String getDiscountType() {

        return discountType;
    }

    /**
     * @param discountType the discountType to set
     */
    public void setDiscountType(
            String discountType) {

        this.discountType = discountType;
    }

    /**
     * @return the description
     */
    public String getDescription() {

        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(
            String description) {

        this.description = description;
    }

    /**
     * @return the amount
     */
    public double getAmount() {

        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(
            double amount) {

        this.amount = amount;
    }

    /**
     * @return the percent
     */
    public double getPercent() {

        return percent;
    }

    /**
     * @param percent the percent to set
     */
    public void setPercent(
            double percent) {

        this.percent = percent;
    }

    @Override
    public String toString() {

        return "ShippingRateDiscount [discountType=" + discountType + ", description=" + description + ", amount="
                + amount + ", percent=" + percent + ", currency=" + currency + "]";
    }

}
