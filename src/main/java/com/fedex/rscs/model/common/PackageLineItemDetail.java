package com.fedex.rscs.model.common;

import java.util.List;

import com.fedex.rscs.model.cshp.TrackingSequenceDetail;

/**
 * Represents Possession Package Event Detail Resource Model class. It is mapped to DTO's
 * {@link com.fedex.rscs.client.pes.dto.PackageLineItem} object.
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackageLineItemDetail {

    private String packageType;
    private TrackingSequenceDetail packageIdentifier;
    private PersonDetail customerName;
    private List<TrackingSequenceDetail> childPackages;

    public PackageLineItemDetail() {}

    /**
     * @param packageType
     * @param packageIdentifier
     * @param customerName
     * @param childPackages
     */
    public PackageLineItemDetail(String packageType, TrackingSequenceDetail packageIdentifier,
            PersonDetail customerName, List<TrackingSequenceDetail> childPackages) {

        this.packageType = packageType;
        this.packageIdentifier = packageIdentifier;
        this.customerName = customerName;
        this.childPackages = childPackages;
    }

    /**
     * @return the packageType
     */
    public String getPackageType() {

        return packageType;
    }

    /**
     * @param packageType the packageType to set
     */
    public void setPackageType(
            String packageType) {

        this.packageType = packageType;
    }

    /**
     * @return the packageIdentifier
     */
    public TrackingSequenceDetail getPackageIdentifier() {

        return packageIdentifier;
    }

    /**
     * @param packageIdentifier the packageIdentifier to set
     */
    public void setPackageIdentifier(
            TrackingSequenceDetail packageIdentifier) {

        this.packageIdentifier = packageIdentifier;
    }

    /**
     * @return the customerName
     */
    public PersonDetail getCustomerName() {

        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(
            PersonDetail customerName) {

        this.customerName = customerName;
    }

    /**
     * @return the childPackages
     */
    public List<TrackingSequenceDetail> getChildPackages() {

        return childPackages;
    }

    /**
     * @param childPackages the childPackages to set
     */
    public void setChildPackages(
            List<TrackingSequenceDetail> childPackages) {

        this.childPackages = childPackages;
    }

    @Override
    public String toString() {

        return "PackageLineItemDetail [packageType=" + packageType + ", packageIdentifier=" + packageIdentifier
                + ", customerName=" + customerName + ", childPackages=" + childPackages + "]";
    }


}
