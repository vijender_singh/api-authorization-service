package com.fedex.rscs.model.common;

/**
 * Represents model class of OfferingDetail It is mapped to DTO's
 * {@link com.fedex.rscs.pes.dto.OfferingIdentifierDetail} object.
 * 
 * @author Shakti.Saurabh
 *
 */
public class OfferingDetail {

    private String id;
    private String type;
    private String code;

    public OfferingDetail() {}

    /**
     * @param id
     * @param type
     * @param code
     */
    public OfferingDetail(String id, String type, String code) {
        
        this.id = id;
        this.type = type;
        this.code = code;
    }

    /**
     * @return the id
     */
    public String getId() {

        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(
            String id) {

        this.id = id;
    }

    /**
     * @return the type
     */
    public String getType() {

        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(
            String type) {

        this.type = type;
    }

    /**
     * @return the code
     */
    public String getCode() {

        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(
            String code) {

        this.code = code;
    }

    @Override
    public boolean equals(
            Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        OfferingDetail other = (OfferingDetail) obj;
        if (code == null) {
            if (other.code != null)
                return false;
        } else if (!code.equals(other.code))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

    @Override
    public String toString() {

        return "OfferingDetail [id=" + id + ", type=" + type + ", code=" + code + "]";
    }
}
