package com.fedex.rscs.model;

/**
 * ShipmentNotificationRoleType model specifications
 * 
 * @author 5034922
 *
 */
public enum ShipmentNotificationRoleType {
    SHIPPER, RECIPIENT, OTHER
}