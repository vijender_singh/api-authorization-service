package com.fedex.rscs.model;

/**
 * Represents severity levels used in FAST services
 * 
 * @author 3802280
 *
 */
public enum Severity {

    FAILURE, ERROR, NOTE, WARNING, SUCCESS;

    public boolean isAtOrMoreSevereThan(
            Severity severity) {

        if (this.ordinal() >= severity.ordinal()) {
            return true;
        } else {
            return false;
        }
    }
}
