/**
 * All domain objects belong in this package.  A domain object
 * represents the canonical definition of an object internal to this application
 */
package com.fedex.rscs.model;