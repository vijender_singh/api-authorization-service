package com.fedex.rscs.model;

import java.util.Arrays;
import java.util.List;

/**
 * Class with constants used across the RSCS service.
 * 
 * @author 3798910
 *
 */
public class ServiceConstant {

    public static final String SHIPMENT_CONTROL_VARIABLE_OPTION_ID = "ISS";
    public static final List<String> SHIPMENT_CONTROL_VARIABLE_OPTION_VALUE = Arrays.asList("SUPPORTED");
    public static final String DEVICE_PLATFORM_TYPE = "Browser";
    public static final String API_VERSION_ID = "V1";
    public static final String CITY = "city";
    public static final String ADDRESS = "address";
    public static final String STREETLINES = "streetLines";
    public static final String STATEORPROVICECODE = "stateOrProvinceCode";
    public static final String SPECIAL_SRVC_TYPE_HD = "HOME_DELIVERY_PREMIUM";
    public static final String HOME_DELIVERY_DETAIL = "homeDeliveryDetail";
    public static final String SPECIAL_SRVC_REQ = "specialServicesRequested";
    public static final String DELIVERY_DATE = "deliveryDate";
    public static final String DELIVERY_DATE_FORMAT = "uuuu-M-d";
    public static final String LOCATION_ID = "locationId";
    public static final String ID = "id";
    public static final String TYPE = "type";
    public static final String SOLDSHIPMENT = "SOLDSHIPMENT";
    public static final String OPERATIONALDATE = "operationaldate";
    public static final String REFERENCE_ID = "referenceId";
    public static final String TRACKING_ID = "trackingId";
    public static final String OPENSHIPMENT = "OPENSHIPMENT";

}
