package com.fedex.rscs.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Model to represent Address.
 * 
 * @author 5034922
 *
 */
public class AddressDetail {

    private List<String> streetLines;
    private String city;
    private String stateOrProvinceCode;
    private String postalCode;
    private String countryCode;
    private boolean isResidential;

    public AddressDetail() {}

    /**
     * @param streetLines
     * @param city
     * @param stateOrProvinceCode
     * @param postalCode
     * @param countryCode
     * @param isResidential
     */
    public AddressDetail(List<String> streetLines, String city, String stateOrProvinceCode, String postalCode,
            String countryCode, boolean isResidential) {

        this.streetLines = streetLines;
        this.city = city;
        this.stateOrProvinceCode = stateOrProvinceCode;
        this.postalCode = postalCode;
        this.countryCode = countryCode;
        this.isResidential = isResidential;
    }



    /**
     * 
     * @return streetLines
     */
    public List<String> getStreetLines() {

        if (streetLines == null) {
            streetLines = new ArrayList<String>();
        }
        return streetLines;
    }

    /**
     * 
     * @param streetLines to set streetLines
     */
    public void setStreetLines(
            List<String> streetLines) {

        this.streetLines = streetLines;
    }

    /**
     * 
     * @return city
     */
    public String getCity() {

        return city;
    }

    /**
     * 
     * @param city to set city
     */
    public void setCity(
            String city) {

        this.city = city;
    }

    /**
     * 
     * @return stateOrProvinceCode
     */
    public String getStateOrProvinceCode() {

        return stateOrProvinceCode;
    }

    /**
     * 
     * @param stateOrProvinceCode to set stateOrProvinceCode
     */
    public void setStateOrProvinceCode(
            String stateOrProvinceCode) {

        this.stateOrProvinceCode = stateOrProvinceCode;
    }

    /**
     * 
     * @return postalCode
     */
    public String getPostalCode() {

        return postalCode;
    }

    /**
     * 
     * @param postalCode to set postalCode
     */
    public void setPostalCode(
            String postalCode) {

        this.postalCode = postalCode;
    }

    /**
     * 
     * @return countryCode
     */
    public String getCountryCode() {

        return countryCode;
    }

    /**
     * 
     * @param countryCode to set countryCode
     */
    public void setCountryCode(
            String countryCode) {

        this.countryCode = countryCode;
    }


    /**
     * @return the isResidential
     */
    public boolean isResidential() {

        return isResidential;
    }


    /**
     * @param isResidential the isResidential to set
     */
    public void setResidential(
            boolean isResidential) {

        this.isResidential = isResidential;
    }

    @Override
    public String toString() {

        return "AddressDetail [streetLines=" + streetLines + ", city=" + city + ", stateOrProvinceCode="
                + stateOrProvinceCode + ", postalCode=" + postalCode + ", countryCode=" + countryCode
                + ", isResidential=" + isResidential + "]";
    }

}

