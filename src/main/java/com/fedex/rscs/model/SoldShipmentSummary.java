package com.fedex.rscs.model;

import java.util.List;

import com.fedex.rscs.model.cshp.TrackingSequenceDetail;

/**
 * Sold shipment summary model
 * 
 * @author 3900109
 *
 */
public class SoldShipmentSummary {

    private String guid;
    private CarrierDetail carrierDetails;
    private TrackingSequenceDetail masterTrackingId;
    private String serviceType;
    private String transactionDateTime;
    private String status;
    private String retailTransactionId;
    private String trackingNumber;
    private List<PackageLineItemsDetail> packageLineItems;

    public SoldShipmentSummary() {}

    /**
     * @return the guid
     */
    public String getGuid() {
    
        return guid;
    }
    
    /**
     * @param guid the guid to set
     */
    public void setGuid(
            String guid) {
    
        this.guid = guid;
    }

    /**
     * @return the carrierDetails
     */
    public CarrierDetail getCarrierDetails() {
    
        return carrierDetails;
    }
    
    /**
     * @param carrierDetails the carrierDetails to set
     */
    public void setCarrierDetails(
            CarrierDetail carrierDetails) {
    
        this.carrierDetails = carrierDetails;
    }

    /**
     * @return the masterTrackingId
     */
    public TrackingSequenceDetail getMasterTrackingId() {
    
        return masterTrackingId;
    }

    /**
     * @param masterTrackingId the masterTrackingId to set
     */
    public void setMasterTrackingId(
            TrackingSequenceDetail masterTrackingId) {
    
        this.masterTrackingId = masterTrackingId;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {
    
        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(
            String serviceType) {
    
        this.serviceType = serviceType;
    }
    
    /**
     * @return the transactionDateTime
     */
    public String getTransactionDateTime() {
    
        return transactionDateTime;
    }

    /**
     * @param transactionDateTime the transactionDateTime to set
     */
    public void setTransactionDateTime(
            String transactionDateTime) {
    
        this.transactionDateTime = transactionDateTime;
    }
    
    /**
     * @return the status
     */
    public String getStatus() {
    
        return status;
    }
    
    /**
     * @param status the status to set
     */
    public void setStatus(
            String status) {
    
        this.status = status;
    }
    
    /**
     * @return the retailTransactionId
     */
    public String getRetailTransactionId() {
    
        return retailTransactionId;
    }
    
    /**
     * @param retailTransactionId the retailTransactionId to set
     */
    public void setRetailTransactionId(
            String retailTransactionId) {
    
        this.retailTransactionId = retailTransactionId;
    }

    /**
     * @return the packageLineItems
     */
    public List<PackageLineItemsDetail> getPackageLineItems() {
    
        return packageLineItems;
    }

    /**
     * @param packageLineItems the packageLineItems to set
     */
    public void setPackageLineItems(
            List<PackageLineItemsDetail> packageLineItems) {
    
        this.packageLineItems = packageLineItems;
    }
    
    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {

        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {

        this.trackingNumber = trackingNumber;
    }

    @Override
    public String toString() {

        return "SoldShipmentSummary [guid=" + guid + ", carrierDetails=" + carrierDetails + ", masterTrackingId="
                + masterTrackingId + ", serviceType=" + serviceType + ", transactionDateTime=" + transactionDateTime
                + ", status=" + status + ", retailTransactionId=" + retailTransactionId + ", trackingNumber="
                + trackingNumber + ", packageLineItems=" + packageLineItems + "]";
    }

}
