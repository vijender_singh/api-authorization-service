package com.fedex.rscs.model;

/**
 * Model class to contains InterlineShippingDetail DTO.
 * 
 * @author 3900094
 *
 */
public class InterlineShippingData {

    private String interlineId;
    private String interlineCode;
    private String interlineNumber;
    private String interlineName;
    private String employeeId;
    private String accountNumber;

    public InterlineShippingData() {}

    /**
     * @param interlineId
     * @param interlineCode
     * @param interlineNumber
     * @param interlineName
     * @param employeeId
     * @param accountNumber
     */
    public InterlineShippingData(String interlineId, String interlineCode, String interlineNumber, String interlineName,
            String employeeId, String accountNumber) {

        this.interlineId = interlineId;
        this.interlineCode = interlineCode;
        this.interlineNumber = interlineNumber;
        this.interlineName = interlineName;
        this.employeeId = employeeId;
        this.accountNumber = accountNumber;
    }

    /**
     * @return the interlineId
     */
    public String getInterlineId() {

        return interlineId;
    }

    /**
     * @param interlineId the interlineId to set
     */
    public void setInterlineId(
            String interlineId) {

        this.interlineId = interlineId;
    }

    /**
     * @return the interlineCode
     */
    public String getInterlineCode() {

        return interlineCode;
    }

    /**
     * @param interlineCode the interlineCode to set
     */
    public void setInterlineCode(
            String interlineCode) {

        this.interlineCode = interlineCode;
    }

    /**
     * @return the interlineNumber
     */
    public String getInterlineNumber() {

        return interlineNumber;
    }

    /**
     * @param interlineNumber the interlineNumber to set
     */
    public void setInterlineNumber(
            String interlineNumber) {

        this.interlineNumber = interlineNumber;
    }

    /**
     * @return the interlineName
     */
    public String getInterlineName() {

        return interlineName;
    }

    /**
     * @param interlineName the interlineName to set
     */
    public void setInterlineName(
            String interlineName) {

        this.interlineName = interlineName;
    }

    /**
     * @return the employeeId
     */
    public String getEmployeeId() {

        return employeeId;
    }

    /**
     * @param employeeId the employeeId to set
     */
    public void setEmployeeId(
            String employeeId) {

        this.employeeId = employeeId;
    }
      
    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
    
        return accountNumber;
    }
    
    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(
            String accountNumber) {
    
        this.accountNumber = accountNumber;
    }

    @Override
    public String toString() {

        return "InterlineShippingData [interlineId=" + interlineId + ", interlineCode=" + interlineCode
                + ", interlineNumber=" + interlineNumber + ", interlineName=" + interlineName + ", employeeId="
                + employeeId + ", accountNumber=" + accountNumber + "]";
    }

}
