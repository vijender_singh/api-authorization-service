package com.fedex.rscs.model;

import java.util.List;

/**
 * Define the Requested Package Line Items specification model.
 * 
 * @author 5034922
 *
 */
public class RequestedPackageLineItemDetail {

    private Integer sequenceNumber;
    private Integer groupNumber;
    private Integer groupPackageCount;
    private InsuredValueDetail insuredValue;
    private DimensionDetail dimensions;
    private WeightDetail weight;
    private String packedBy;
    private String physicalPackagingType;
    private RequestedPackageSpecialServices packageSpecialServicesRequested;
    private SpecialServiceDetail signatureOption;
    private String specialHandlingDetail;
    private List<ShippingCustomerReferenceDetails> customerReferences;

    public RequestedPackageLineItemDetail() {

    }

    /**
     * @return the sequenceNumber
     */
    public Integer getSequenceNumber() {

        return sequenceNumber;
    }

    /**
     * @param sequenceNumber the sequenceNumber to set
     */
    public void setSequenceNumber(
            Integer sequenceNumber) {

        this.sequenceNumber = sequenceNumber;
    }

    /**
     * @return the groupNumber
     */
    public Integer getGroupNumber() {

        return groupNumber;
    }

    /**
     * @param groupNumber the groupNumber to set
     */
    public void setGroupNumber(
            Integer groupNumber) {

        this.groupNumber = groupNumber;
    }

    /**
     * @return the groupPackageCount
     */
    public Integer getGroupPackageCount() {

        return groupPackageCount;
    }

    /**
     * @param groupPackageCount the groupPackageCount to set
     */
    public void setGroupPackageCount(
            Integer groupPackageCount) {

        this.groupPackageCount = groupPackageCount;
    }

    /**
     * @return the insuredValue
     */
    public InsuredValueDetail getInsuredValue() {

        return insuredValue;
    }

    /**
     * @param insuredValue the insuredValue to set
     */
    public void setInsuredValue(
            InsuredValueDetail insuredValue) {

        this.insuredValue = insuredValue;
    }

    /**
     * @return the dimensions
     */
    public DimensionDetail getDimensions() {

        return dimensions;
    }

    /**
     * @param dimensions the dimensions to set
     */
    public void setDimensions(
            DimensionDetail dimensions) {

        this.dimensions = dimensions;
    }

    /**
     * @return the weight
     */
    public WeightDetail getWeight() {

        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(
            WeightDetail weight) {

        this.weight = weight;
    }

    /**
     * @return the packedBy
     */
    public String getPackedBy() {

        return packedBy;
    }

    /**
     * @param packedBy the packedBy to set
     */
    public void setPackedBy(
            String packedBy) {

        this.packedBy = packedBy;
    }

    /**
     * @return the packageSpecialServicesRequested
     */
    public RequestedPackageSpecialServices getPackageSpecialServicesRequested() {

        return packageSpecialServicesRequested;
    }

    /**
     * @param packageSpecialServicesRequested the packageSpecialServicesRequested to set
     */
    public void setPackageSpecialServicesRequested(
            RequestedPackageSpecialServices packageSpecialServicesRequested) {

        this.packageSpecialServicesRequested = packageSpecialServicesRequested;
    }

    /**
     * @return the signatureOption
     */
    public SpecialServiceDetail getSignatureOption() {
    
        return signatureOption;
    }
    
    /**
     * @param signatureOption the signatureOption to set
     */
    public void setSignatureOption(
            SpecialServiceDetail signatureOption) {
    
        this.signatureOption = signatureOption;
    }

    /**
     * @param specialHandlingDetail the specialHandlingDetail to set
     */
    public void setSpecialHandlingDetail(
            String specialHandlingDetail) {
    
        this.specialHandlingDetail = specialHandlingDetail;
    }

    /**
     * @return the customerReferences
     */
    public List<ShippingCustomerReferenceDetails> getCustomerReferences() {

        return customerReferences;
    }

    /**
     * @param customerReferences the customerReferences to set
     */
    public void setCustomerReferences(
            List<ShippingCustomerReferenceDetails> customerReferences) {

        this.customerReferences = customerReferences;
    }

    /**
     * @return the specialHandlingDetail
     */
    public String getSpecialHandlingDetail() {

        return specialHandlingDetail;
    }

    /**
     * @return the physicalPackagingType
     */
    public String getPhysicalPackagingType() {

        return physicalPackagingType;
    }

    /**
     * @param physicalPackagingType the physicalPackagingType to set
     */
    public void setPhysicalPackagingType(
            String physicalPackagingType) {

        this.physicalPackagingType = physicalPackagingType;
    }

    @Override
    public String toString() {

        return "RequestedPackageLineItemDetail [sequenceNumber=" + sequenceNumber + ", groupNumber=" + groupNumber
                + ", groupPackageCount=" + groupPackageCount + ", insuredValue=" + insuredValue + ", dimensions="
                + dimensions + ", weight=" + weight + ", packedBy=" + packedBy + ", physicalPackagingType="
                + physicalPackagingType + ", packageSpecialServicesRequested=" + packageSpecialServicesRequested
                + ", signatureOption=" + signatureOption + ", specialHandlingDetail=" + specialHandlingDetail
                + ", customerReferences=" + customerReferences + "]";
    }

}

