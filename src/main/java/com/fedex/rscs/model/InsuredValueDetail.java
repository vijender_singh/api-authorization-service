package com.fedex.rscs.model;


/**
 * Define the InsuredValue model.
 * 
 * @author 5034922
 *
 */
public class InsuredValueDetail {

    private String currency;
    private String amount;

    public InsuredValueDetail() {}

    /**
     * @param currency
     * @param amount
     */
    public InsuredValueDetail(String currency, String amount) {

        this.currency = currency;
        this.amount = amount;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {

        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(
            String currency) {

        this.currency = currency;
    }

    /**
     * @return the amount
     */
    public String getAmount() {

        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(
            String amount) {

        this.amount = amount;
    }

    @Override
    public String toString() {

        return "InsuredValue [currency=" + currency + ", amount=" + amount + "]";
    }

}
