package com.fedex.rscs.model;

/**
 * AssociatedAccount model specification.
 * 
 * @author 5034922
 *
 */
public class AssociatedAccountDetail {

    private String type;
    private String accountNumber;
    private String meterNumber;
    private String nickName;
    protected boolean isIpasCustomer;

    public AssociatedAccountDetail(){
        
    }
    
    /**
     * @param type
     * @param accountNumber
     * @param meterNumber
     * @param nickName
     * @param isIpasCustomer
     */
    public AssociatedAccountDetail(String type, String accountNumber, String meterNumber, String nickName,
            boolean isIpasCustomer) {

        this.type = type;
        this.accountNumber = accountNumber;
        this.meterNumber = meterNumber;
        this.nickName = nickName;
        this.isIpasCustomer = isIpasCustomer;
    }

    /**
     * @return the type
     */
    public String getType() {

        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(
            String type) {

        this.type = type;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {

        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(
            String accountNumber) {

        this.accountNumber = accountNumber;
    }

    /**
     * @return the meterNumber
     */
    public String getMeterNumber() {

        return meterNumber;
    }

    /**
     * @param meterNumber the meterNumber to set
     */
    public void setMeterNumber(
            String meterNumber) {

        this.meterNumber = meterNumber;
    }

    /**
     * @return the nickName
     */
    public String getNickName() {

        return nickName;
    }

    /**
     * @param nickName the nickName to set
     */
    public void setNickName(
            String nickName) {

        this.nickName = nickName;
    }

    /**
     * @return the isIpasCustomer
     */
    public boolean isIpasCustomer() {

        return isIpasCustomer;
    }

    /**
     * @param isIpasCustomer the isIpasCustomer to set
     */
    public void setIpasCustomer(
            boolean isIpasCustomer) {

        this.isIpasCustomer = isIpasCustomer;
    }

    @Override
    public String toString() {

        return "AssociatedAccount [type=" + type + ", accountNumber=" + accountNumber + ", meterNumber=" + meterNumber
                + ", nickName=" + nickName + ", isIpasCustomer=" + isIpasCustomer + "]";
    }

}
