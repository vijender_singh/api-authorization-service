package com.fedex.rscs.model;

/**
 * Represents the Operating company Enum
 * 
 * @author Shakti.Saurabh
 *
 */
public enum OperatingCompany {

    FEDEX_EXPRESS("Express"), FEDEX_GROUND("Ground");

    private String shipmentType;

    OperatingCompany(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public String getShipmentType() {

        return shipmentType;
    }
}
