package com.fedex.rscs.model;

import java.util.List;

/**
 * Define the Shipment Package Detail model used in repository layer for mapping table
 * SHIPMENT_PACKAGE.
 * 
 * @author shakti.saurabh
 *
 */
public class ShipmentPackageDetail {

    private String shipmentTrackingNumber;
    private String acceptTmStmp;
    private String trackingNumber;
    private String barcode;
    private String formId;
    private boolean masterFlag;
    private int seqNumber;
    private long lengthQuantity;
    private long heightQuantity;
    private long widthQuantity;
    private double amount;
    private double weight;
    private boolean manualWeightFlag;
    private String paaDamageStatText;
    private String paaPackByText;
    private String paaInspectionStatText;
    private double dryIceWeight;
    private List<ShipmentPackageAdditionalService> shipmentPackageAdditionalServices;

    /**
     * @return the shipmentTrackingNumber
     */
    public String getShipmentTrackingNumber() {

        return shipmentTrackingNumber;
    }

    /**
     * @param shipmentTrackingNumber the shipmentTrackingNumber to set
     */
    public void setShipmentTrackingNumber(
            String shipmentTrackingNumber) {

        this.shipmentTrackingNumber = shipmentTrackingNumber;
    }

    /**
     * @return the acceptTmStmp
     */
    public String getAcceptTmStmp() {

        return acceptTmStmp;
    }

    /**
     * @param acceptTmStmp the acceptTmStmp to set
     */
    public void setAcceptTmStmp(
            String acceptTmStmp) {

        this.acceptTmStmp = acceptTmStmp;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {

        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {

        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the barcode
     */
    public String getBarcode() {

        return barcode;
    }

    /**
     * @param barcode the barcode to set
     */
    public void setBarcode(
            String barcode) {

        this.barcode = barcode;
    }

    /**
     * @return the formId
     */
    public String getFormId() {

        return formId;
    }

    /**
     * @param formId the formId to set
     */
    public void setFormId(
            String formId) {

        this.formId = formId;
    }

    /**
     * @return the masterFlag
     */
    public boolean isMasterFlag() {

        return masterFlag;
    }

    /**
     * @param masterFlag the masterFlag to set
     */
    public void setMasterFlag(
            boolean masterFlag) {

        this.masterFlag = masterFlag;
    }

    /**
     * @return the seqNumber
     */
    public int getSeqNumber() {

        return seqNumber;
    }

    /**
     * @param seqNumber the seqNumber to set
     */
    public void setSeqNumber(
            int seqNumber) {

        this.seqNumber = seqNumber;
    }

    /**
     * @return the lengthQuantity
     */
    public long getLengthQuantity() {

        return lengthQuantity;
    }

    /**
     * @param lengthQuantity the lengthQuantity to set
     */
    public void setLengthQuantity(
            long lengthQuantity) {

        this.lengthQuantity = lengthQuantity;
    }

    /**
     * @return the heightQuantity
     */
    public long getHeightQuantity() {

        return heightQuantity;
    }

    /**
     * @param heightQuantity the heightQuantity to set
     */
    public void setHeightQuantity(
            long heightQuantity) {

        this.heightQuantity = heightQuantity;
    }

    /**
     * @return the widthQuantity
     */
    public long getWidthQuantity() {

        return widthQuantity;
    }

    /**
     * @param widthQuantity the widthQuantity to set
     */
    public void setWidthQuantity(
            long widthQuantity) {

        this.widthQuantity = widthQuantity;
    }

    
    /**
     * @return the amount
     */
    public double getAmount() {
    
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(
            double amount) {
    
        this.amount = amount;
    }

    /**
     * @return the weight
     */
    public double getWeight() {

        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(
            double weight) {

        this.weight = weight;
    }

    /**
     * @return the manualWeightFlag
     */
    public boolean isManualWeightFlag() {

        return manualWeightFlag;
    }

    /**
     * @param manualWeightFlag the manualWeightFlag to set
     */
    public void setManualWeightFlag(
            boolean manualWeightFlag) {

        this.manualWeightFlag = manualWeightFlag;
    }

    /**
     * @return the paaDamageStatText
     */
    public String getPaaDamageStatText() {

        return paaDamageStatText;
    }

    /**
     * @param paaDamageStatText the paaDamageStatText to set
     */
    public void setPaaDamageStatText(
            String paaDamageStatText) {

        this.paaDamageStatText = paaDamageStatText;
    }

    /**
     * @return the paaPackByText
     */
    public String getPaaPackByText() {

        return paaPackByText;
    }

    /**
     * @param paaPackByText the paaPackByText to set
     */
    public void setPaaPackByText(
            String paaPackByText) {

        this.paaPackByText = paaPackByText;
    }

    /**
     * @return the paaInspectionStatText
     */
    public String getPaaInspectionStatText() {

        return paaInspectionStatText;
    }

    /**
     * @param paaInspectionStatText the paaInspectionStatText to set
     */
    public void setPaaInspectionStatText(
            String paaInspectionStatText) {

        this.paaInspectionStatText = paaInspectionStatText;
    }

    /**
     * @return the dryIceWeight
     */
    public double getDryIceWeight() {

        return dryIceWeight;
    }

    /**
     * @param dryIceWeight the dryIceWeight to set
     */
    public void setDryIceWeight(
            double dryIceWeight) {

        this.dryIceWeight = dryIceWeight;
    }

    /**
     * @return the shipmentPackageAdditionalServices
     */
    public List<ShipmentPackageAdditionalService> getShipmentPackageAdditionalServices() {

        return shipmentPackageAdditionalServices;
    }

    /**
     * @param shipmentPackageAdditionalServices the shipmentPackageAdditionalServices to set
     */
    public void setShipmentPackageAdditionalServices(
            List<ShipmentPackageAdditionalService> shipmentPackageAdditionalServices) {

        this.shipmentPackageAdditionalServices = shipmentPackageAdditionalServices;
    }

    @Override
    public String toString() {

        return "ShipmentPackageDetail [shipmentTrackingNumber=" + shipmentTrackingNumber + ", acceptTmStmp="
                + acceptTmStmp + ", trackingNumber=" + trackingNumber + ", barcode=" + barcode + ", formId=" + formId
                + ", masterFlag=" + masterFlag + ", seqNumber=" + seqNumber + ", lengthQuantity=" + lengthQuantity
                + ", heightQuantity=" + heightQuantity + ", widthQuantity=" + widthQuantity + ", amount="
                + amount + ", weight=" + weight + ", manualWeightFlag=" + manualWeightFlag
                + ", paaDamageStatText=" + paaDamageStatText + ", paaPackByText=" + paaPackByText
                + ", paaInspectionStatText=" + paaInspectionStatText + ", dryIceWeight=" + dryIceWeight
                + ", shipmentPackageAdditionalServices=" + shipmentPackageAdditionalServices + "]";
    }

}
