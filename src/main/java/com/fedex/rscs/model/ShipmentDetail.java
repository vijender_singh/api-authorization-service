package com.fedex.rscs.model;

import java.util.List;

/**
 * Define the Shipment model used in repository layer for mapping table SHIPMENT.
 * 
 * @author shakti.saurabh
 *
 */
public class ShipmentDetail {

    private String shipmentTrackingNumber;
    private String acceptTmStmp;
    private String initialAcceptanceTmStmp;
    private String fedexId;
    private String fedexLocationId;
    private String opCoLocationId;
    private String opCoCode;
    private String deviceId;
    private String devicePlatformType;
    private String apiClientTypeName;
    private String apiVersionId;
    private String apiClientId;
    private String originCountryCode;
    private boolean afterPickupFlag;
    private String groundAccountNumber;
    private String cityCenterAccountNumber;
    private String meterNumber;
    private String softwareId;
    private String paymentType;
    private String customerAccountNumber;
    private String billToAccountType;
    private String fedexPackageType;
    private String nonFedexPackageType;
    private boolean standardPackageFlag;
    private int totalPackageQuantity;
    private String currency;
    private String weightUom;
    private String dimensionUom;
    private String ratingType;
    private String shippingServiceName;
    private String halFedexLocationId;
    private double chargeAmount;
    private double discountAmount;
    private double surchargeAmount;
    private double taxAmount;
    private double netChargeAmount;
    private String transactionShipmentRefId;
    private String posTransactionRefId;
    private String delCommitDate;
    private String currentShipmentStatus;
    private String currentShipmentStatTmStmp;
    private ShipmentContactDetail shipmentContactDetail;
    private List<ShipmentPackageDetail> shipmentPackageDetails;
    private String opcoShippingServiceName;
    private String shippingServiceDesc;
    private InterlineShipmentDetail interlineShipmentDetail;

    /**
     * @return the shipmentTrackingNumber
     */
    public String getShipmentTrackingNumber() {

        return shipmentTrackingNumber;
    }

    /**
     * @param shipmentTrackingNumber the shipmentTrackingNumber to set
     */
    public void setShipmentTrackingNumber(
            String shipmentTrackingNumber) {

        this.shipmentTrackingNumber = shipmentTrackingNumber;
    }


    /**
     * @return the acceptTmStmp
     */
    public String getAcceptTmStmp() {

        return acceptTmStmp;
    }

    /**
     * @param acceptTmStmp the acceptTmStmp to set
     */
    public void setAcceptTmStmp(
            String acceptTmStmp) {

        this.acceptTmStmp = acceptTmStmp;
    }

    /**
     * @return the initialAcceptanceTmStmp
     */
    public String getInitialAcceptanceTmStmp() {

        return initialAcceptanceTmStmp;
    }

    /**
     * @param initialAcceptanceTmStmp the initialAcceptanceTmStmp to set
     */
    public void setInitialAcceptanceTmStmp(
            String initialAcceptanceTmStmp) {

        this.initialAcceptanceTmStmp = initialAcceptanceTmStmp;
    }

    /**
     * @return the fedexId
     */
    public String getFedexId() {

        return fedexId;
    }

    /**
     * @param fedexId the fedexId to set
     */
    public void setFedexId(
            String fedexId) {

        this.fedexId = fedexId;
    }

    /**
     * @return the fedexLocationId
     */
    public String getFedexLocationId() {

        return fedexLocationId;
    }

    /**
     * @param fedexLocationId the fedexLocationId to set
     */
    public void setFedexLocationId(
            String fedexLocationId) {

        this.fedexLocationId = fedexLocationId;
    }

    /**
     * @return the opCoLocationId
     */
    public String getOpCoLocationId() {

        return opCoLocationId;
    }

    /**
     * @param opCoLocationId the opCoLocationId to set
     */
    public void setOpCoLocationId(
            String opCoLocationId) {

        this.opCoLocationId = opCoLocationId;
    }

    /**
     * @return the opCoCode
     */
    public String getOpCoCode() {

        return opCoCode;
    }

    /**
     * @param opCoCode the opCoCode to set
     */
    public void setOpCoCode(
            String opCoCode) {

        this.opCoCode = opCoCode;
    }

    /**
     * @return the deviceId
     */
    public String getDeviceId() {

        return deviceId;
    }

    /**
     * @param deviceId the deviceId to set
     */
    public void setDeviceId(
            String deviceId) {

        this.deviceId = deviceId;
    }

    /**
     * @return the devicePlatformType
     */
    public String getDevicePlatformType() {

        return devicePlatformType;
    }

    /**
     * @param devicePlatformType the devicePlatformType to set
     */
    public void setDevicePlatformType(
            String devicePlatformType) {

        this.devicePlatformType = devicePlatformType;
    }

    /**
     * @return the apiClientTypeName
     */
    public String getApiClientTypeName() {

        return apiClientTypeName;
    }

    /**
     * @param apiClientTypeName the apiClientTypeName to set
     */
    public void setApiClientTypeName(
            String apiClientTypeName) {

        this.apiClientTypeName = apiClientTypeName;
    }

    /**
     * @return the apiVersionId
     */
    public String getApiVersionId() {

        return apiVersionId;
    }

    /**
     * @param apiVersionId the apiVersionId to set
     */
    public void setApiVersionId(
            String apiVersionId) {

        this.apiVersionId = apiVersionId;
    }

    /**
     * @return the apiClientId
     */
    public String getApiClientId() {

        return apiClientId;
    }

    /**
     * @param apiClientId the apiClientId to set
     */
    public void setApiClientId(
            String apiClientId) {

        this.apiClientId = apiClientId;
    }

    /**
     * @return the originCountryCode
     */
    public String getOriginCountryCode() {

        return originCountryCode;
    }

    /**
     * @param originCountryCode the originCountryCode to set
     */
    public void setOriginCountryCode(
            String originCountryCode) {

        this.originCountryCode = originCountryCode;
    }

    /**
     * @return the afterPickupFlag
     */
    public boolean isAfterPickupFlag() {

        return afterPickupFlag;
    }

    /**
     * @param afterPickupFlag the afterPickupFlag to set
     */
    public void setAfterPickupFlag(
            boolean afterPickupFlag) {

        this.afterPickupFlag = afterPickupFlag;
    }

    /**
     * @return the groundAccountNumber
     */
    public String getGroundAccountNumber() {

        return groundAccountNumber;
    }

    /**
     * @param groundAccountNumber the groundAccountNumber to set
     */
    public void setGroundAccountNumber(
            String groundAccountNumber) {

        this.groundAccountNumber = groundAccountNumber;
    }

    /**
     * @return the cityCenterAccountNumber
     */
    public String getCityCenterAccountNumber() {

        return cityCenterAccountNumber;
    }

    /**
     * @param cityCenterAccountNumber the cityCenterAccountNumber to set
     */
    public void setCityCenterAccountNumber(
            String cityCenterAccountNumber) {

        this.cityCenterAccountNumber = cityCenterAccountNumber;
    }

    /**
     * @return the meterNumber
     */
    public String getMeterNumber() {

        return meterNumber;
    }

    /**
     * @param meterNumber the meterNumber to set
     */
    public void setMeterNumber(
            String meterNumber) {

        this.meterNumber = meterNumber;
    }

    /**
     * @return the softwareId
     */
    public String getSoftwareId() {

        return softwareId;
    }

    /**
     * @param softwareId the softwareId to set
     */
    public void setSoftwareId(
            String softwareId) {

        this.softwareId = softwareId;
    }

    /**
     * @return the paymentType
     */
    public String getPaymentType() {

        return paymentType;
    }

    /**
     * @param paymentType the paymentType to set
     */
    public void setPaymentType(
            String paymentType) {

        this.paymentType = paymentType;
    }

    /**
     * @return the customerAccountNumber
     */
    public String getCustomerAccountNumber() {

        return customerAccountNumber;
    }

    /**
     * @param customerAccountNumber the customerAccountNumber to set
     */
    public void setCustomerAccountNumber(
            String customerAccountNumber) {

        this.customerAccountNumber = customerAccountNumber;
    }

    /**
     * @return the billToAccountType
     */
    public String getBillToAccountType() {

        return billToAccountType;
    }

    /**
     * @param billToAccountType the billToAccountType to set
     */
    public void setBillToAccountType(
            String billToAccountType) {

        this.billToAccountType = billToAccountType;
    }

    /**
     * @return the fedexPackageType
     */
    public String getFedexPackageType() {

        return fedexPackageType;
    }

    /**
     * @param fedexPackageType the fedexPackageType to set
     */
    public void setFedexPackageType(
            String fedexPackageType) {

        this.fedexPackageType = fedexPackageType;
    }

    /**
     * @return the nonFedexPackageType
     */
    public String getNonFedexPackageType() {

        return nonFedexPackageType;
    }

    /**
     * @param nonFedexPackageType the nonFedexPackageType to set
     */
    public void setNonFedexPackageType(
            String nonFedexPackageType) {

        this.nonFedexPackageType = nonFedexPackageType;
    }

    /**
     * @return the standardPackageFlag
     */
    public boolean isStandardPackageFlag() {

        return standardPackageFlag;
    }

    /**
     * @param standardPackageFlag the standardPackageFlag to set
     */
    public void setStandardPackageFlag(
            boolean standardPackageFlag) {

        this.standardPackageFlag = standardPackageFlag;
    }

    /**
     * @return the totalPackageQuantity
     */
    public int getTotalPackageQuantity() {

        return totalPackageQuantity;
    }

    /**
     * @param totalPackageQuantity the totalPackageQuantity to set
     */
    public void setTotalPackageQuantity(
            int totalPackageQuantity) {

        this.totalPackageQuantity = totalPackageQuantity;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {

        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(
            String currency) {

        this.currency = currency;
    }

    /**
     * @return the weightUom
     */
    public String getWeightUom() {

        return weightUom;
    }

    /**
     * @param weightUom the weightUom to set
     */
    public void setWeightUom(
            String weightUom) {

        this.weightUom = weightUom;
    }

    /**
     * @return the dimensionUom
     */
    public String getDimensionUom() {

        return dimensionUom;
    }

    /**
     * @param dimensionUom the dimensionUom to set
     */
    public void setDimensionUom(
            String dimensionUom) {

        this.dimensionUom = dimensionUom;
    }

    /**
     * @return the ratingType
     */
    public String getRatingType() {

        return ratingType;
    }

    /**
     * @param ratingType the ratingType to set
     */
    public void setRatingType(
            String ratingType) {

        this.ratingType = ratingType;
    }

    /**
     * @return the shippingServiceName
     */
    public String getShippingServiceName() {

        return shippingServiceName;
    }

    /**
     * @param shippingServiceName the shippingServiceName to set
     */
    public void setShippingServiceName(
            String shippingServiceName) {

        this.shippingServiceName = shippingServiceName;
    }

    /**
     * @return the halFedexLocationId
     */
    public String getHalFedexLocationId() {

        return halFedexLocationId;
    }

    /**
     * @param halFedexLocationId the halFedexLocationId to set
     */
    public void setHalFedexLocationId(
            String halFedexLocationId) {

        this.halFedexLocationId = halFedexLocationId;
    }

    /**
     * @return the chargeAmount
     */
    public double getChargeAmount() {

        return chargeAmount;
    }

    /**
     * @param chargeAmount the chargeAmount to set
     */
    public void setChargeAmount(
            double chargeAmount) {

        this.chargeAmount = chargeAmount;
    }

    /**
     * @return the discountAmount
     */
    public double getDiscountAmount() {

        return discountAmount;
    }

    /**
     * @param discountAmount the discountAmount to set
     */
    public void setDiscountAmount(
            double discountAmount) {

        this.discountAmount = discountAmount;
    }

    /**
     * @return the surchargeAmount
     */
    public double getSurchargeAmount() {

        return surchargeAmount;
    }

    /**
     * @param surchargeAmount the surchargeAmount to set
     */
    public void setSurchargeAmount(
            double surchargeAmount) {

        this.surchargeAmount = surchargeAmount;
    }

    /**
     * @return the taxAmount
     */
    public double getTaxAmount() {

        return taxAmount;
    }

    /**
     * @param taxAmount the taxAmount to set
     */
    public void setTaxAmount(
            double taxAmount) {

        this.taxAmount = taxAmount;
    }

    /**
     * @return the netChargeAmount
     */
    public double getNetChargeAmount() {

        return netChargeAmount;
    }

    /**
     * @param netChargeAmount the netChargeAmount to set
     */
    public void setNetChargeAmount(
            double netChargeAmount) {

        this.netChargeAmount = netChargeAmount;
    }

    /**
     * @return the transactionShipmentRefId
     */
    public String getTransactionShipmentRefId() {

        return transactionShipmentRefId;
    }

    /**
     * @param transactionShipmentRefId the transactionShipmentRefId to set
     */
    public void setTransactionShipmentRefId(
            String transactionShipmentRefId) {

        this.transactionShipmentRefId = transactionShipmentRefId;
    }

    /**
     * @return the posTransactionRefId
     */
    public String getPosTransactionRefId() {

        return posTransactionRefId;
    }

    /**
     * @param posTransactionRefId the posTransactionRefId to set
     */
    public void setPosTransactionRefId(
            String posTransactionRefId) {

        this.posTransactionRefId = posTransactionRefId;
    }

    /**
     * @return the delCommitDate
     */
    public String getDelCommitDate() {

        return delCommitDate;
    }

    /**
     * @param delCommitDate the delCommitDate to set
     */
    public void setDelCommitDate(
            String delCommitDate) {

        this.delCommitDate = delCommitDate;
    }

    /**
     * @return the currentShipmentStatus
     */
    public String getCurrentShipmentStatus() {

        return currentShipmentStatus;
    }

    /**
     * @param currentShipmentStatus the currentShipmentStatus to set
     */
    public void setCurrentShipmentStatus(
            String currentShipmentStatus) {

        this.currentShipmentStatus = currentShipmentStatus;
    }

    /**
     * @return the currentShipmentStatTmStmp
     */
    public String getCurrentShipmentStatTmStmp() {

        return currentShipmentStatTmStmp;
    }

    /**
     * @param currentShipmentStatTmStmp the currentShipmentStatTmStmp to set
     */
    public void setCurrentShipmentStatTmStmp(
            String currentShipmentStatTmStmp) {

        this.currentShipmentStatTmStmp = currentShipmentStatTmStmp;
    }

    /**
     * @return the shipmentContactDetail
     */
    public ShipmentContactDetail getShipmentContactDetail() {

        return shipmentContactDetail;
    }

    /**
     * @param shipmentContactDetail the shipmentContactDetail to set
     */
    public void setShipmentContactDetail(
            ShipmentContactDetail shipmentContactDetail) {

        this.shipmentContactDetail = shipmentContactDetail;
    }

    /**
     * @return the shipmentPackageDetails
     */
    public List<ShipmentPackageDetail> getShipmentPackageDetails() {

        return shipmentPackageDetails;
    }

    /**
     * @param shipmentPackageDetails the shipmentPackageDetails to set
     */
    public void setShipmentPackageDetails(
            List<ShipmentPackageDetail> shipmentPackageDetails) {

        this.shipmentPackageDetails = shipmentPackageDetails;
    }
    
    /**
     * @return the opcoShippingServiceName
     */
    public String getOpcoShippingServiceName() {

        return opcoShippingServiceName;
    }

    /**
     * @param opcoShippingServiceName the opcoShippingServiceName to set
     */
    public void setOpcoShippingServiceName(
            String opcoShippingServiceName) {

        this.opcoShippingServiceName = opcoShippingServiceName;
    }
    
    /**
     * @return the shippingServiceDesc
     */
    public String getShippingServiceDesc() {

        return shippingServiceDesc;
    }

    /**
     * @param shippingServiceDesc the shippingServiceDesc to set
     */
    public void setShippingServiceDesc(
            String shippingServiceDesc) {

        this.shippingServiceDesc = shippingServiceDesc;
    }
    
    /**
     * @return the interlineShipmentDetail
     */
    public InterlineShipmentDetail getInterlineShipmentDetail() {

        return interlineShipmentDetail;
    }

    /**
     * @param interlineShipmentDetail the interlineShipmentDetail to set
     */
    public void setInterlineShipmentDetail(
            InterlineShipmentDetail interlineShipmentDetail) {

        this.interlineShipmentDetail = interlineShipmentDetail;
    }

    @Override
    public String toString() {

        return "ShipmentDetail [shipmentTrackingNumber=" + shipmentTrackingNumber + ", acceptTmStmp=" + acceptTmStmp
                + ", initialAcceptanceTmStmp=" + initialAcceptanceTmStmp + ", fedexId=" + fedexId + ", fedexLocationId="
                + fedexLocationId + ", opCoLocationId=" + opCoLocationId + ", opCoCode=" + opCoCode + ", deviceId="
                + deviceId + ", devicePlatformType=" + devicePlatformType + ", apiClientTypeName=" + apiClientTypeName
                + ", apiVersionId=" + apiVersionId + ", apiClientId=" + apiClientId + ", originCountryCode="
                + originCountryCode + ", afterPickupFlag=" + afterPickupFlag + ", groundAccountNumber="
                + groundAccountNumber + ", cityCenterAccountNumber=" + cityCenterAccountNumber + ", meterNumber="
                + meterNumber + ", softwareId=" + softwareId + ", paymentType=" + paymentType
                + ", customerAccountNumber=" + customerAccountNumber + ", billToAccountType=" + billToAccountType
                + ", fedexPackageType=" + fedexPackageType + ", nonFedexPackageType=" + nonFedexPackageType
                + ", standardPackageFlag=" + standardPackageFlag + ", totalPackageQuantity=" + totalPackageQuantity
                + ", currency=" + currency + ", weightUom=" + weightUom + ", dimensionUom=" + dimensionUom
                + ", ratingType=" + ratingType + ", shippingServiceName=" + shippingServiceName
                + ", halFedexLocationId=" + halFedexLocationId + ", chargeAmount=" + chargeAmount + ", discountAmount="
                + discountAmount + ", surchargeAmount=" + surchargeAmount + ", taxAmount=" + taxAmount
                + ", netChargeAmount=" + netChargeAmount + ", transactionShipmentRefId=" + transactionShipmentRefId
                + ", posTransactionRefId=" + posTransactionRefId + ", delCommitDate=" + delCommitDate
                + ", currentShipmentStatus=" + currentShipmentStatus + ", currentShipmentStatTmStmp="
                + currentShipmentStatTmStmp + ", shipmentContactDetail=" + shipmentContactDetail
                + ", shipmentPackageDetails=" + shipmentPackageDetails + ", opcoShippingServiceName="
                + opcoShippingServiceName + ", shippingServiceDesc=" + shippingServiceDesc
                + ", interlineShipmentDetail=" + interlineShipmentDetail + "]";
    }

}
