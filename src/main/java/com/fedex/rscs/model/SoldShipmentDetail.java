package com.fedex.rscs.model;

import java.util.List;

import com.fedex.rscs.model.cshp.Price;
import com.fedex.rscs.model.cshp.TrackingSequenceDetail;

/**
 * Sold shipment resource model
 * 
 * @author Shakti.Saurabh
 *
 */
public class SoldShipmentDetail {

    private String guid;
    private String trackingNumber;
    private CarrierDetail carrierDetails;
    private TrackingSequenceDetail masterTrackingId;
    private String serviceType;
    private String transactionDateTime;
    private String status;
    private String retailTransactionId;
    private List<PackageLineItemsDetail> packageLineItems;
    private PartyDetail shipper;
    private PartyDetail recipient;
    private PartyDetail customer;
    private String packagingType;
    private String packageCount;
    private String paymentType;
    private Price totalAmount;
    private String location;
    private boolean validLocation;

    public SoldShipmentDetail() {

        // default constructor
    }

    /**
     * @return the guid
     */
    public String getGuid() {
    
        return guid;
    }
    
    /**
     * @param guid the guid to set
     */
    public void setGuid(
            String guid) {
    
        this.guid = guid;
    }
    
    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
    
        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {
    
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the carrierDetails
     */
    public CarrierDetail getCarrierDetails() {
    
        return carrierDetails;
    }
    
    /**
     * @param carrierDetails the carrierDetails to set
     */
    public void setCarrierDetails(
            CarrierDetail carrierDetails) {
    
        this.carrierDetails = carrierDetails;
    }

    /**
     * @return the masterTrackingId
     */
    public TrackingSequenceDetail getMasterTrackingId() {
    
        return masterTrackingId;
    }

    /**
     * @param masterTrackingId the masterTrackingId to set
     */
    public void setMasterTrackingId(
            TrackingSequenceDetail masterTrackingId) {
    
        this.masterTrackingId = masterTrackingId;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {
    
        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(
            String serviceType) {
    
        this.serviceType = serviceType;
    }
    
    /**
     * @return the transactionDateTime
     */
    public String getTransactionDateTime() {
    
        return transactionDateTime;
    }

    /**
     * @param transactionDateTime the transactionDateTime to set
     */
    public void setTransactionDateTime(
            String transactionDateTime) {
    
        this.transactionDateTime = transactionDateTime;
    }
    
    /**
     * @return the status
     */
    public String getStatus() {
    
        return status;
    }
    
    /**
     * @param status the status to set
     */
    public void setStatus(
            String status) {
    
        this.status = status;
    }
    
    /**
     * @return the retailTransactionId
     */
    public String getRetailTransactionId() {
    
        return retailTransactionId;
    }
    
    /**
     * @param retailTransactionId the retailTransactionId to set
     */
    public void setRetailTransactionId(
            String retailTransactionId) {
    
        this.retailTransactionId = retailTransactionId;
    }

    /**
     * @return the packageLineItems
     */
    public List<PackageLineItemsDetail> getPackageLineItems() {
    
        return packageLineItems;
    }

    /**
     * @param packageLineItems the packageLineItems to set
     */
    public void setPackageLineItems(
            List<PackageLineItemsDetail> packageLineItems) {
    
        this.packageLineItems = packageLineItems;
    }
    
    /**
     * @return the shipper
     */
    public PartyDetail getShipper() {
    
        return shipper;
    }
    
    /**
     * @param shipper the shipper to set
     */
    public void setShipper(
            PartyDetail shipper) {
    
        this.shipper = shipper;
    }

    /**
     * @return the recipient
     */
    public PartyDetail getRecipient() {
    
        return recipient;
    }

    /**
     * @param recipient the recipient to set
     */
    public void setRecipient(
            PartyDetail recipient) {
    
        this.recipient = recipient;
    }

    /**
     * @return the customer
     */
    public PartyDetail getCustomer() {
    
        return customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(
            PartyDetail customer) {
    
        this.customer = customer;
    }

    /**
     * @return the packagingType
     */
    public String getPackagingType() {
    
        return packagingType;
    }

    /**
     * @param packagingType the packagingType to set
     */
    public void setPackagingType(
            String packagingType) {
    
        this.packagingType = packagingType;
    }

    /**
     * @return the packageCount
     */
    public String getPackageCount() {
    
        return packageCount;
    }
    
    /**
     * @param packageCount the packageCount to set
     */
    public void setPackageCount(
            String packageCount) {
    
        this.packageCount = packageCount;
    }

    /**
     * @return the paymentType
     */
    public String getPaymentType() {
    
        return paymentType;
    }

    /**
     * @param paymentType the paymentType to set
     */
    public void setPaymentType(
            String paymentType) {
    
        this.paymentType = paymentType;
    }

    /**
     * @return the totalAmount
     */
    public Price getTotalAmount() {
    
        return totalAmount;
    }
    
    /**
     * @param totalAmount the totalAmount to set
     */
    public void setTotalAmount(
            Price totalAmount) {
    
        this.totalAmount = totalAmount;
    }

    /**
     * @return the location
     */
    public String getLocation() {

        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(
            String location) {

        this.location = location;
    }

    /**
     * @return the validLocation
     */
    public boolean isValidLocation() {
    
        return validLocation;
    }
    
    /**
     * @param validLocation the validLocation to set
     */
    public void setValidLocation(
            boolean validLocation) {
    
        this.validLocation = validLocation;
    }

    @Override
    public String toString() {

        return "SoldShipmentDetail [guid=" + guid + ", trackingNumber=" + trackingNumber + ", carrierDetails="
                + carrierDetails + ", masterTrackingId=" + masterTrackingId + ", serviceType=" + serviceType
                + ", transactionDateTime=" + transactionDateTime + ", status=" + status + ", retailTransactionId="
                + retailTransactionId + ", packageLineItems=" + packageLineItems + ", shipper=" + shipper
                + ", recipient=" + recipient + ", customer=" + customer + ", packagingType=" + packagingType
                + ", packageCount=" + packageCount + ", paymentType=" + paymentType + ", totalAmount=" + totalAmount
                + ", location=" + location + ", validLocation=" + validLocation + "]";
    }

}
