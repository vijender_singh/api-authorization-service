package com.fedex.rscs.model;

/**
 * Model for SurchargeDetail
 * 
 * @author 3900094
 *
 */
public class SurchargeDetail {

    private String surchargeType;
    private String description;
    private double amount;

    public SurchargeDetail() {}

    /**
     * @return the surchargeType
     */
    public String getSurchargeType() {

        return surchargeType;
    }

    /**
     * @param surchargeType the surchargeType to set
     */
    public void setSurchargeType(
            String surchargeType) {

        this.surchargeType = surchargeType;
    }

    /**
     * @return the description
     */
    public String getDescription() {

        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(
            String description) {

        this.description = description;
    }

    /**
     * @return the amount
     */
    public double getAmount() {

        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(
            double amount) {

        this.amount = amount;
    }

    @Override
    public String toString() {

        return "SurchargeDetail [surchargeType=" + surchargeType + ", description=" + description + ", amount=" + amount
                + "]";
    }

}
