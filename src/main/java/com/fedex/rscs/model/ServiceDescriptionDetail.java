package com.fedex.rscs.model;

import java.util.List;

/**
 * Model for ServiceDescriptionDetail
 * 
 * @author 3900094
 *
 */
public class ServiceDescriptionDetail {

    private String serviceType;
    private List<ProductDetail> names;
    private String description;
    private String serviceName;

    public ServiceDescriptionDetail() {}

    /**
     * @return the serviceType
     */
    public String getServiceType() {

        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(
            String serviceType) {

        this.serviceType = serviceType;
    }

    /**
     * @return the names
     */
    public List<ProductDetail> getNames() {

        return names;
    }

    /**
     * @param names the names to set
     */
    public void setNames(
            List<ProductDetail> names) {

        this.names = names;
    }

    /**
     * @return the description
     */
    public String getDescription() {

        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(
            String description) {

        this.description = description;
    }

    /**
     * @return the serviceName
     */
    public String getServiceName() {

        return serviceName;
    }

    /**
     * @param serviceName the serviceName to set
     */
    public void setServiceName(
            String serviceName) {

        this.serviceName = serviceName;
    }

    @Override
    public String toString() {

        return "ServiceDescriptionDetail [serviceType=" + serviceType + ", names=" + names + ", description="
                + description + ", serviceName=" + serviceName + "]";
    }

}
