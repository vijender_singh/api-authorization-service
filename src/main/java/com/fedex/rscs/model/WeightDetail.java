package com.fedex.rscs.model;

/**
 * Model class to hold weight details
 * 
 * @author 5034922
 *
 */
public class WeightDetail {

    private String unit;
    private String value;
    private String measurementType;

    public WeightDetail() {}

    /**
     * @param unit
     * @param value
     */
    public WeightDetail(String unit, String value) {

        this.unit = unit;
        this.value = value;
    }

    /**
     * @return the unit
     */
    public String getUnit() {

        return unit;
    }

    /**
     * @param unit the unit to set
     */
    public void setUnit(
            String unit) {

        this.unit = unit;
    }

    /**
     * @return the value
     */
    public String getValue() {

        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(
            String value) {

        this.value = value;
    }
    
    /**
     * @return the measurementType
     */
    public String getMeasurementType() {

        return measurementType;
    }

    /**
     * @param measurementType the measurementType to set
     */
    public void setMeasurementType(
            String measurementType) {

        this.measurementType = measurementType;
    }

    @Override
    public String toString() {

        return "WeightDetail [unit=" + unit + ", value=" + value + ", measurementType=" + measurementType + "]";
    }

}
