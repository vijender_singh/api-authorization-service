package com.fedex.rscs.model;

/**
 * Define the Shipping Customer reference detail DTO
 * 
 * @author 3932968
 *
 */
public class ShippingCustomerReferenceDetails {

    private String referenceType;
    private String value;

    public ShippingCustomerReferenceDetails() {};

    /**
     * @param referenceType
     * @param value
     */
    public ShippingCustomerReferenceDetails(String referenceType, String value) {

        this.referenceType = referenceType;
        this.value = value;
    }

    /**
     * @return the referenceType
     */
    public String getReferenceType() {

        return referenceType;
    }

    /**
     * @param referenceType the referenceType to set
     */
    public void setReferenceType(
            String referenceType) {

        this.referenceType = referenceType;
    }

    /**
     * @return the value
     */
    public String getValue() {

        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(
            String value) {

        this.value = value;
    }

    @Override
    public String toString() {

        return "ShippingCustomerReferenceDetail [referenceType=" + referenceType + ", value=" + value + "]";
    }

}
