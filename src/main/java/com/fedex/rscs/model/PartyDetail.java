package com.fedex.rscs.model;

import com.fedex.rscs.model.cshp.CreditCardData;

/**
 * customer Detail model specification.
 * 
 * @author 5034922
 *
 */
public class PartyDetail {

    private AddressDetail address;
    private ContactDetail contact;
    private String accountNumber;
    private CreditCardData creditCard;
    private String accountType;

    public PartyDetail() {
		
	}

    /**
     * @param address
     * @param contact
     * @param accountNumber
     * @param creditCard
     * @param accountType
     */
    public PartyDetail(AddressDetail address, ContactDetail contact, String accountNumber, CreditCardData creditCard,
            String accountType) {

        this.address = address;
        this.contact = contact;
        this.accountNumber = accountNumber;
        this.creditCard = creditCard;
        this.accountType = accountType;
    }

    /**
     * @return the address
     */
    public AddressDetail getAddress() {

        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(
            AddressDetail address) {

        this.address = address;
    }

    /**
     * @return the contact
     */
    public ContactDetail getContact() {

        return contact;
    }

    /**
     * @param contact the contact to set
     */
    public void setContact(
            ContactDetail contact) {

        this.contact = contact;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {

        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(
            String accountNumber) {

        this.accountNumber = accountNumber;
    }

    /**
     * @return the creditCard
     */
    public CreditCardData getCreditCard() {

        return creditCard;
    }

    /**
     * @param creditCard the creditCard to set
     */
    public void setCreditCard(
            CreditCardData creditCard) {

        this.creditCard = creditCard;
    }

    /**
     * @return the accountType
     */
    public String getAccountType() {

        return accountType;
    }

    /**
     * @param accountType the accountType to set
     */
    public void setAccountType(
            String accountType) {

        this.accountType = accountType;
    }

    @Override
    public String toString() {

        return "PartyDetail [address=" + address + ", contact=" + contact + ", accountNumber=" + accountNumber
                + ", creditCard=" + creditCard + ", accountType=" + accountType + "]";
    }

}
