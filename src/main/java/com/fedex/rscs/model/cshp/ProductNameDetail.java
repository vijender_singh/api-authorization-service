package com.fedex.rscs.model.cshp;

/**
 * ProductName model specification.
 * 
 * @author 5034922
 *
 */
public class ProductNameDetail {

    private String type;
    private String encoding;
    private String value;

    
    public ProductNameDetail() {}
    
    /**
     * @param type
     * @param encoding
     * @param value
     */
    public ProductNameDetail(String type, String encoding, String value) {

        this.type = type;
        this.encoding = encoding;
        this.value = value;
    }

    /**
     * @return the type
     */
    public String getType() {

        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(
            String type) {

        this.type = type;
    }

    /**
     * @return the encoding
     */
    public String getEncoding() {

        return encoding;
    }

    /**
     * @param encoding the encoding to set
     */
    public void setEncoding(
            String encoding) {

        this.encoding = encoding;
    }

    /**
     * @return the value
     */
    public String getValue() {

        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(
            String value) {

        this.value = value;
    }

    @Override
    public String toString() {

        return "ProductName [type=" + type + ", encoding=" + encoding + ", value=" + value + "]";
    }

}
