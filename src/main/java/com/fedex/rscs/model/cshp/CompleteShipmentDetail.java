package com.fedex.rscs.model.cshp;

import java.util.List;

import com.fedex.rscs.model.InsuredValueDetail;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.ShipmentSpecialServiceDetail;
import com.fedex.rscs.model.WeightDetail;

/**
 * Model specification for CompleteShipmentDetail.
 * 
 * @author 5034922
 *
 */
public class CompleteShipmentDetail {

    private boolean isUsDomestic;
    private String locationId;
    private String carrierCode;
    private String plannedServiceLevel;
    private TrackingIdDetail masterTrackingId;
    private ProductDescription packagingDescription;
    private ProductDescription serviceDescription;
    private List<SpecialServiceDescriptionDetail> specialServiceDescriptions;
    private ShipmentOperationData operationalDetail;
    private List<CompletedPackageData> completedPackageDetails;
    private List<ShippingDocument> shipmentDocuments;
    private String index;
    private PartyDetail recipient;
    private InsuredValueDetail totalInsuredValue;
    private WeightDetail totalWeight;
    private int packageCount;
    private ShipmentSpecialServiceDetail specialServicesRequested;

    public CompleteShipmentDetail() {}

    /**
     * @return the isUsDomestic
     */
    public boolean isUsDomestic() {

        return isUsDomestic;
    }

    /**
     * @param isUsDomestic the isUsDomestic to set
     */
    public void setUsDomestic(
            boolean isUsDomestic) {

        this.isUsDomestic = isUsDomestic;
    }

    /**
     * @return the locationId
     */
    public String getLocationId() {

        return locationId;
    }

    /**
     * @param locationId the locationId to set
     */
    public void setLocationId(
            String locationId) {

        this.locationId = locationId;
    }

    /**
     * @return the carrierCode
     */
    public String getCarrierCode() {

        return carrierCode;
    }

    /**
     * @param carrierCode the carrierCode to set
     */
    public void setCarrierCode(
            String carrierCode) {

        this.carrierCode = carrierCode;
	}

	/**
	 * @return the plannedServiceLevel
	 */
	public String getPlannedServiceLevel() {
		return plannedServiceLevel;
	}

	/**
	 * @param plannedServiceLevel the plannedServiceLevel to set
	 */
	public void setPlannedServiceLevel(String plannedServiceLevel) {
		this.plannedServiceLevel = plannedServiceLevel;
	}

	/**
	 * @return the masterTrackingId
	 */
	public TrackingIdDetail getMasterTrackingId() {

		return masterTrackingId;
	}

    /**
     * @param masterTrackingId the masterTrackingId to set
     */
    public void setMasterTrackingId(
            TrackingIdDetail masterTrackingId) {

        this.masterTrackingId = masterTrackingId;
    }

    /**
     * @return the packagingDescription
     */
    public ProductDescription getPackagingDescription() {

        return packagingDescription;
    }

    /**
     * @param packagingDescription the packagingDescription to set
     */
    public void setPackagingDescription(
            ProductDescription packagingDescription) {

        this.packagingDescription = packagingDescription;
    }

    /**
     * @return the serviceDescription
     */
    public ProductDescription getServiceDescription() {

        return serviceDescription;
    }

    /**
     * @param serviceDescription the serviceDescription to set
     */
    public void setServiceDescription(
            ProductDescription serviceDescription) {

        this.serviceDescription = serviceDescription;
    }

    /**
     * @return the specialServiceDescriptions
     */
    public List<SpecialServiceDescriptionDetail> getSpecialServiceDescriptions() {

        return specialServiceDescriptions;
    }

    /**
     * @param specialServiceDescriptions the specialServiceDescriptions to set
     */
    public void setSpecialServiceDescriptions(
            List<SpecialServiceDescriptionDetail> specialServiceDescriptions) {

        this.specialServiceDescriptions = specialServiceDescriptions;
    }

    /**
     * @return the operationalDetail
     */
    public ShipmentOperationData getOperationalDetail() {

        return operationalDetail;
    }

    /**
     * @param operationalDetail the operationalDetail to set
     */
    public void setOperationalDetail(
            ShipmentOperationData operationalDetail) {

        this.operationalDetail = operationalDetail;
    }

    /**
     * @return the completedPackageDetails
     */
    public List<CompletedPackageData> getCompletedPackageDetails() {

        return completedPackageDetails;
    }

    /**
     * @param completedPackageDetails the completedPackageDetails to set
     */
    public void setCompletedPackageDetails(
            List<CompletedPackageData> completedPackageDetails) {

        this.completedPackageDetails = completedPackageDetails;
    }

    /**
     * @return the shipmentDocuments
     */
    public List<ShippingDocument> getShipmentDocuments() {

        return shipmentDocuments;
    }

    /**
     * @param shipmentDocuments the shipmentDocuments to set
     */
    public void setShipmentDocuments(
            List<ShippingDocument> shipmentDocuments) {

        this.shipmentDocuments = shipmentDocuments;
    }

    /**
     * @return the index
     */
    public String getIndex() {

        return index;
    }

    /**
     * @param index to set the index
     */
    public void setIndex(
            String index) {

        this.index = index;
    }


    /**
     * @return the recipient
     */
    public PartyDetail getRecipient() {

        return recipient;
    }

    /**
     * @param recipient the recipient to set
     */
    public void setRecipient(
            PartyDetail recipient) {

        this.recipient = recipient;
    }

    /**
     * @return the specialServicesRequested
     */
    public ShipmentSpecialServiceDetail getSpecialServicesRequested() {

        return specialServicesRequested;
    }
    
    /**
     * @return the totalInsuredValue
     */
    public InsuredValueDetail getTotalInsuredValue() {
    
        return totalInsuredValue;
    }
    
    /**
     * @param totalInsuredValue the totalInsuredValue to set
     */
    public void setTotalInsuredValue(
            InsuredValueDetail totalInsuredValue) {
    
        this.totalInsuredValue = totalInsuredValue;
    }
    
    /**
     * @return the totalWeight
     */
    public WeightDetail getTotalWeight() {
    
        return totalWeight;
    }
    
    /**
     * @param totalWeight the totalWeight to set
     */
    public void setTotalWeight(
            WeightDetail totalWeight) {
    
        this.totalWeight = totalWeight;
    }

    /**
     * @param specialServicesRequested the specialServicesRequested to set
     */
    public void setSpecialServicesRequested(
            ShipmentSpecialServiceDetail specialServicesRequested) {

        this.specialServicesRequested = specialServicesRequested;
    }

    /**
     * @return the packageCount
     */
    public int getPackageCount() {

        return packageCount;
    }

    /**
     * @param packageCount the packageCount to set
     */
    public void setPackageCount(
            int packageCount) {

        this.packageCount = packageCount;
    }

    @Override
    public String toString() {

        return "CompleteShipmentDetail [isUsDomestic=" + isUsDomestic + ", locationId=" + locationId + ", carrierCode="
                + carrierCode + ", masterTrackingId=" + masterTrackingId + ", packagingDescription="
                + packagingDescription + ", serviceDescription=" + serviceDescription + ", specialServiceDescriptions="
                + specialServiceDescriptions + ", operationalDetail=" + operationalDetail + ", completedPackageDetails="
                + completedPackageDetails + ", shipmentDocuments=" + shipmentDocuments + ", index=" + index
                + ", recipient=" + recipient + ", totalInsuredValue=" + totalInsuredValue + ", totalWeight="
                + totalWeight + ", packageCount=" + packageCount + ", specialServicesRequested="
                + specialServicesRequested + "]";
    }

}
