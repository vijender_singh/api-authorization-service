package com.fedex.rscs.model.cshp;

import java.util.Arrays;

/**
 * BinaryBarcode model specification.
 * 
 * @author 5034922
 *
 */
public class BinaryBarcodeDetail {

    private String type;
    private byte[] value;

    /**
     * @param type
     * @param value
     */
    public BinaryBarcodeDetail(String type, byte[] value) {

        this.type = type;
        this.value = value;
    }

    public BinaryBarcodeDetail() {

    }

    /**
     * @return the type
     */
    public String getType() {

        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(
            String type) {

        this.type = type;
    }

    /**
     * @return the value
     */
    public byte[] getValue() {

        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(
            byte[] value) {

        this.value = value;
    }

    @Override
    public String toString() {

        return "BinaryBarcode [type=" + type + ", value=" + Arrays.toString(value) + "]";
    }

}
