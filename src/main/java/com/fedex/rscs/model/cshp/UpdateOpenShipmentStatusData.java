package com.fedex.rscs.model.cshp;

/**
 * Model class for update open shipments status.
 * 
 * @author 3900094
 *
 */
public class UpdateOpenShipmentStatusData {

    private String referenceId;
    private String locationId;
    private String trackingId;
    private String openShipmentStatusCode;
    private String teamMemberId;

    public UpdateOpenShipmentStatusData() {}

    /**
     * @param referenceId
     * @param locationId
     * @param trackingId
     * @param openShipmentStatusCode
     * @param teamMemberId
     */
    public UpdateOpenShipmentStatusData(String referenceId, String locationId, String trackingId,
            String openShipmentStatusCode, String teamMemberId) {

        this.referenceId = referenceId;
        this.locationId = locationId;
        this.trackingId = trackingId;
        this.openShipmentStatusCode = openShipmentStatusCode;
        this.teamMemberId = teamMemberId;
    }

    /**
     * @return the referenceId
     */
    public String getReferenceId() {

        return referenceId;
    }

    /**
     * @param referenceId to set the referenceId
     */
    public void setReferenceId(
            String referenceId) {

        this.referenceId = referenceId;
    }

    /**
     * @return the locationId
     */
    public String getLocationId() {

        return locationId;
    }

    /**
     * @param locationId to set the locationId
     */
    public void setLocationId(
            String locationId) {

        this.locationId = locationId;
    }

    /**
     * @return the trackingId
     */
    public String getTrackingId() {

        return trackingId;
    }

    /**
     * @param trackingId to set the trackingId
     */
    public void setTrackingId(
            String trackingId) {

        this.trackingId = trackingId;
    }

    /**
     * @return the openShipmentStatusCode
     */
    public String getOpenShipmentStatusCode() {

        return openShipmentStatusCode;
    }

    /**
     * @param openShipmentStatusCode to set the openShipmentStatusCode
     */
    public void setOpenShipmentStatusCode(
            String openShipmentStatusCode) {

        this.openShipmentStatusCode = openShipmentStatusCode;
    }

    /**
     * @return the teamMemberId
     */
    public String getTeamMemberId() {

        return teamMemberId;
    }

    /**
     * @param teamMemberId to set the teamMemberId
     */
    public void setTeamMemberId(
            String teamMemberId) {

        this.teamMemberId = teamMemberId;
    }

    @Override
    public String toString() {

        return "UpdateOpenShipmentStatus [referenceId=" + referenceId + ", locationId=" + locationId + ", trackingId="
                + trackingId + ", openShipmentStatusCode=" + openShipmentStatusCode + ", teamMemberId=" + teamMemberId
                + "]";
    }

}
