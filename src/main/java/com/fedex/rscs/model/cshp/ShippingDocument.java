package com.fedex.rscs.model.cshp;

/**
 * ShippingDocument model specifications.
 * 
 * @author 5034922
 *
 */
public class ShippingDocument {

    private String type;
    private String shippingDocumentDisposition;
    private String imageType;
    private Integer resolution;
    private Integer copiesToPrint;
    private String image;

    public ShippingDocument() {

    }

    /**
     * @param type
     * @param shippingDocumentDisposition
     * @param imageType
     * @param resolution
     * @param copiesToPrint
     * @param image
     */
    public ShippingDocument(String type, String shippingDocumentDisposition, String imageType, Integer resolution,
            Integer copiesToPrint, String image) {

        this.type = type;
        this.shippingDocumentDisposition = shippingDocumentDisposition;
        this.imageType = imageType;
        this.resolution = resolution;
        this.copiesToPrint = copiesToPrint;
        this.image = image;
    }

    /**
     * @return the type
     */
    public String getType() {

        return type;
    }


    /**
     * @param type the type to set
     */
    public void setType(
            String type) {

        this.type = type;
    }


    /**
     * @return the shippingDocumentDisposition
     */
    public String getShippingDocumentDisposition() {

        return shippingDocumentDisposition;
    }


    /**
     * @param shippingDocumentDisposition the shippingDocumentDisposition to set
     */
    public void setShippingDocumentDisposition(
            String shippingDocumentDisposition) {

        this.shippingDocumentDisposition = shippingDocumentDisposition;
    }


    /**
     * @return the imageType
     */
    public String getImageType() {

        return imageType;
    }


    /**
     * @param imageType the imageType to set
     */
    public void setImageType(
            String imageType) {

        this.imageType = imageType;
    }


    /**
     * @return the resolution
     */
    public Integer getResolution() {

        return resolution;
    }


    /**
     * @param resolution the resolution to set
     */
    public void setResolution(
            Integer resolution) {

        this.resolution = resolution;
    }


    /**
     * @return the copiesToPrint
     */
    public Integer getCopiesToPrint() {

        return copiesToPrint;
    }


    /**
     * @param copiesToPrint the copiesToPrint to set
     */
    public void setCopiesToPrint(
            Integer copiesToPrint) {

        this.copiesToPrint = copiesToPrint;
    }

    /**
     * @return the image
     */
    public String getImage() {
    
        return image;
    }

    
    /**
     * @param image the image to set
     */
    public void setImage(
            String image) {
    
        this.image = image;
    }

    @Override
    public String toString() {

        return "ShippingDocument [type=" + type + ", shippingDocumentDisposition=" + shippingDocumentDisposition
                + ", imageType=" + imageType + ", resolution=" + resolution + ", copiesToPrint=" + copiesToPrint
                + ", image=" + image + "]";
    }

}
