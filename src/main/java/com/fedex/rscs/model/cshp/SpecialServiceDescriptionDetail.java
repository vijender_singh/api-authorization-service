package com.fedex.rscs.model.cshp;

import java.util.List;

/**
 * SpecialServiceDescription model specification.
 * 
 * @author 5034922
 *
 */
public class SpecialServiceDescriptionDetail {

    private OfferingIdentifierData identifier;
    private List<ProductNameDetail> names;

    public SpecialServiceDescriptionDetail() {

    }

    /**
     * @param identifier
     * @param names
     */
    public SpecialServiceDescriptionDetail(OfferingIdentifierData identifier, List<ProductNameDetail> names) {

        this.identifier = identifier;
        this.names = names;
    }


    /**
     * @return the identifier
     */
    public OfferingIdentifierData getIdentifier() {

        return identifier;
    }


    /**
     * @param identifier the identifier to set
     */
    public void setIdentifier(
            OfferingIdentifierData identifier) {

        this.identifier = identifier;
    }


    /**
     * @return the names
     */
    public List<ProductNameDetail> getNames() {

        return names;
    }


    /**
     * @param names the names to set
     */
    public void setNames(
            List<ProductNameDetail> names) {

        this.names = names;
    }

    @Override
    public String toString() {

        return "SpecialServiceDescription [identifier=" + identifier + ", names=" + names + "]";
    }


}
