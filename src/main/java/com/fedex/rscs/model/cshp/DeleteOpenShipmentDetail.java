package com.fedex.rscs.model.cshp;

import com.fedex.rscs.model.WorkstationDetail;

public class DeleteOpenShipmentDetail {
    
    private String requestedDateTime;
    private String locationCode;
    private String index;
    private WorkstationDetail workstationDetail;
    
    public DeleteOpenShipmentDetail() {}
    
    /**
     * @param requestedDateTime
     * @param locationCode
     * @param index
     * @param workstationDetail
     * @param versionDetail
     */
    public DeleteOpenShipmentDetail(String requestedDateTime, String locationCode, String index,
            WorkstationDetail workstationDetail) {
        
        this.requestedDateTime = requestedDateTime;
        this.locationCode = locationCode;
        this.index = index;
        this.workstationDetail = workstationDetail;
    }

    /**
     * @return the requestedDateTime
     */
    public String getRequestedDateTime() {
    
        return requestedDateTime;
    }
    
    /**
     * @param requestedDateTime the requestedDateTime to set
     */
    public void setRequestedDateTime(
            String requestedDateTime) {
    
        this.requestedDateTime = requestedDateTime;
    }
    
    /**
     * @return the locationCode
     */
    public String getLocationCode() {
    
        return locationCode;
    }
    
    /**
     * @param locationCode the locationCode to set
     */
    public void setLocationCode(
            String locationCode) {
    
        this.locationCode = locationCode;
    }
    
    /**
     * @return the index
     */
    public String getIndex() {
    
        return index;
    }
    
    /**
     * @param index the index to set
     */
    public void setIndex(
            String index) {
    
        this.index = index;
    }
    
    /**
     * @return the workstationDetail
     */
    public WorkstationDetail getWorkstationDetail() {
    
        return workstationDetail;
    }
    
    /**
     * @param workstationDetail the workstationDetail to set
     */
    public void setWorkstationDetail(
            WorkstationDetail workstationDetail) {
    
        this.workstationDetail = workstationDetail;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + ((index == null) ? 0 : index.hashCode());
        result = prime * result + ((locationCode == null) ? 0 : locationCode.hashCode());
        result = prime * result + ((requestedDateTime == null) ? 0 : requestedDateTime.hashCode());
        result = prime * result + ((workstationDetail == null) ? 0 : workstationDetail.hashCode());
        return result;
    }

    @Override
    public boolean equals(
            Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DeleteOpenShipmentDetail other = (DeleteOpenShipmentDetail) obj;
        if (index == null) {
            if (other.index != null)
                return false;
        } else if (!index.equals(other.index))
            return false;
        if (locationCode == null) {
            if (other.locationCode != null)
                return false;
        } else if (!locationCode.equals(other.locationCode))
            return false;
        if (requestedDateTime == null) {
            if (other.requestedDateTime != null)
                return false;
        } else if (!requestedDateTime.equals(other.requestedDateTime))
            return false;
        if (workstationDetail == null) {
            if (other.workstationDetail != null)
                return false;
        } else if (!workstationDetail.equals(other.workstationDetail))
            return false;
        return true;
    }
    
    @Override
    public String toString() {

        return "DeleteOpenShipmentDetail [requestedDateTime=" + requestedDateTime + ", locationCode=" + locationCode
                + ", index=" + index + ", workstationDetail=" + workstationDetail + "]";
    }
}
