package com.fedex.rscs.model.cshp;
/**
 * This enum is use to define the format type. Based on format type, client will take appropriate
 * actions.
 * 
 * @author 3798910
 *
 */
public enum LabelFormatType {
    OPERATIONAL_LABEL, COMMON2D;
}