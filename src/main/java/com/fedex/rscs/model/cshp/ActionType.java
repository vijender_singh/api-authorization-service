package com.fedex.rscs.model.cshp;


/**
 * This enum is use to define the Action type. Based on action type, client will take appropriate
 * actions.
 * 
 * @author 3798910
 *
 */
public enum ActionType {
    STRONG_VALIDATION, CONFIRM
}
