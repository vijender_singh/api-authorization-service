package com.fedex.rscs.model.cshp;

import java.util.List;

import com.fedex.rscs.model.BatteryClassificationType;
import com.fedex.rscs.model.DimensionDetail;
import com.fedex.rscs.model.InsuredValueDetail;
import com.fedex.rscs.model.ShippingCustomerReferenceDetails;
import com.fedex.rscs.model.SpecialServiceDetail;
import com.fedex.rscs.model.WeightDetail;

/**
 * CompletedPackageDetail model specification.
 * 
 * @author 5034922
 *
 */
public class CompletedPackageData {

    private Integer sequenceNumber;
    private List<TrackingIdDetail> trackingIds;
    private Integer groupNumber;
    private String signatureOption;
    private PackageOperationData operationalDetail;
    private DimensionDetail dimensions;
    private InsuredValueDetail insuredValue;
    private String specialHandlingDetail;
    private List<SpecialServiceDetail> specialServicesList;
    private WeightDetail weight;
    private TrackingSequenceDetail trackingId;
    private boolean master;
    private WeightDetail dryIceWeightDetails;
    private List<BatteryClassificationType> batteryClassifications;
    private List<ShippingCustomerReferenceDetails> customerReferences;


    public CompletedPackageData() {}

    /**
     * @return the sequenceNumber
     */
    public Integer getSequenceNumber() {

        return sequenceNumber;
    }

    /**
     * @param sequenceNumber the sequenceNumber to set
     */
    public void setSequenceNumber(
            Integer sequenceNumber) {

        this.sequenceNumber = sequenceNumber;
    }

    /**
     * @return the trackingIds
     */
    public List<TrackingIdDetail> getTrackingIds() {

        return trackingIds;
    }

    /**
     * @param trackingIds the trackingIds to set
     */
    public void setTrackingIds(
            List<TrackingIdDetail> trackingIds) {

        this.trackingIds = trackingIds;
    }

    /**
     * @return the groupNumber
     */
    public Integer getGroupNumber() {

        return groupNumber;
    }

    /**
     * @param groupNumber the groupNumber to set
     */
    public void setGroupNumber(
            Integer groupNumber) {

        this.groupNumber = groupNumber;
    }

    /**
     * @return the signatureOption
     */
    public String getSignatureOption() {

        return signatureOption;
    }

    /**
     * @param signatureOption the signatureOption to set
     */
    public void setSignatureOption(
            String signatureOption) {

        this.signatureOption = signatureOption;
    }

    /**
     * @return the operationalDetail
     */
    public PackageOperationData getOperationalDetail() {

        return operationalDetail;
    }

    /**
     * @param operationalDetail the operationalDetail to set
     */
    public void setOperationalDetail(
            PackageOperationData operationalDetail) {

        this.operationalDetail = operationalDetail;
    }

    /**
     * @return the dimensions
     */
    public DimensionDetail getDimensions() {

        return dimensions;
    }

    /**
     * @param dimensions the dimensions to set
     */
    public void setDimensions(
            DimensionDetail dimensions) {

        this.dimensions = dimensions;
    }

    /**
     * @return the insuredValue
     */
    public InsuredValueDetail getInsuredValue() {

        return insuredValue;
    }

    /**
     * @param insuredValue the insuredValue to set
     */
    public void setInsuredValue(
            InsuredValueDetail insuredValue) {

        this.insuredValue = insuredValue;
    }

    /**
     * @return the specialHandlingDetail
     */
    public String getSpecialHandlingDetail() {

        return specialHandlingDetail;
    }

    /**
     * @param specialHandlingDetail the specialHandlingDetail to set
     */
    public void setSpecialHandlingDetail(
            String specialHandlingDetail) {

        this.specialHandlingDetail = specialHandlingDetail;
    }

    /**
     * @return the specialServicesDetails
     */
    public List<SpecialServiceDetail> getSpecialServicesDetails() {

        return specialServicesList;
    }

    /**
     * @param specialServicesDetails the specialServicesDetails to set
     */
    public void setSpecialServicesDetails(
            List<SpecialServiceDetail> specialServicesDetails) {

        this.specialServicesList = specialServicesDetails;
    }

    /**
     * @return the weight
     */
    public WeightDetail getWeight() {

        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(
            WeightDetail weight) {

        this.weight = weight;
    }

    /**
     * @return the trackingId
     */
    public TrackingSequenceDetail getTrackingId() {

        return trackingId;
    }

    /**
     * @param trackingId the trackingId to set
     */
    public void setTrackingId(
            TrackingSequenceDetail trackingId) {

        this.trackingId = trackingId;
    }

    /**
     * @return the master
     */
    public boolean isMaster() {

        return master;
    }

    /**
     * @param master the master to set
     */
    public void setMaster(
            boolean master) {

        this.master = master;
    }

    /**
     * @return the dryIceWeightDetails
     */
    public WeightDetail getDryIceWeightDetails() {

        return dryIceWeightDetails;
    }

    /**
     * @param dryIceWeightDetails the dryIceWeightDetails to set
     */
    public void setDryIceWeightDetails(
            WeightDetail dryIceWeightDetails) {

        this.dryIceWeightDetails = dryIceWeightDetails;
    }



    /**
     * @return the customerReferences
     */
    public List<ShippingCustomerReferenceDetails> getCustomerReferences() {

        return customerReferences;
    }

    /**
     * @param customerReferences the customerReferences to set
     */
    public void setCustomerReferences(
            List<ShippingCustomerReferenceDetails> customerReferences) {

        this.customerReferences = customerReferences;
    }


    /**
     * @return the specialServicesList
     */
    public List<SpecialServiceDetail> getSpecialServicesList() {

        return specialServicesList;
    }


    /**
     * @param specialServicesList the specialServicesList to set
     */
    public void setSpecialServicesList(
            List<SpecialServiceDetail> specialServicesList) {

        this.specialServicesList = specialServicesList;
    }


    /**
     * @return the batteryClassifications
     */
    public List<BatteryClassificationType> getBatteryClassifications() {

        return batteryClassifications;
    }


    /**
     * @param batteryClassifications the batteryClassifications to set
     */
    public void setBatteryClassifications(
            List<BatteryClassificationType> batteryClassifications) {

        this.batteryClassifications = batteryClassifications;
    }

    @Override
    public String toString() {

        return "CompletedPackageData [sequenceNumber=" + sequenceNumber + ", trackingIds=" + trackingIds
                + ", groupNumber=" + groupNumber + ", signatureOption=" + signatureOption + ", operationalDetail="
                + operationalDetail + ", dimensions=" + dimensions + ", insuredValue=" + insuredValue
                + ", specialHandlingDetail=" + specialHandlingDetail + ", specialServicesList=" + specialServicesList
                + ", weight=" + weight + ", trackingId=" + trackingId + ", master=" + master + ", dryIceWeightDetails="
                + dryIceWeightDetails + ", batteryClassifications=" + batteryClassifications + ", customerReferences="
                + customerReferences + "]";
    }

}
