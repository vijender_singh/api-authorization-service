package com.fedex.rscs.model.cshp;

import com.fedex.rscs.model.AddressDetail;

/**
 * Represents the Shipping Payment Detail Model
 * 
 * @author 3798910
 *
 */
public class ShippingPaymentData {

    private String paymentType;
    private Price amount;
    private String responsibleParty;
    private String accountType;
    private String accountNumber;
    private String maskedCreditCard;
    private String type;
    private String expirationDate;
    private AddressDetail payorAddress;

    public ShippingPaymentData() {}

    /**
     * @param paymentType
     * @param amount
     * @param responsibleParty
     * @param accountType
     * @param accountNumber
     * @param maskedCreditCard
     * @param type
     * @param expirationDate
     * @param payorAddress
     */
    public ShippingPaymentData(String paymentType, Price amount, String responsibleParty, String accountType,
            String accountNumber, String maskedCreditCard, String type, String expirationDate,
            AddressDetail payorAddress) {

        this.paymentType = paymentType;
        this.amount = amount;
        this.responsibleParty = responsibleParty;
        this.accountType = accountType;
        this.accountNumber = accountNumber;
        this.maskedCreditCard = maskedCreditCard;
        this.type = type;
        this.expirationDate = expirationDate;
        this.payorAddress = payorAddress;
    }

    /**
     * @return the paymentType
     */
    public String getPaymentType() {

        return paymentType;
    }

    /**
     * @param paymentType the paymentType to set
     */
    public void setPaymentType(
            String paymentType) {

        this.paymentType = paymentType;
    }

    /**
     * @return the amount
     */
    public Price getAmount() {

        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(
            Price amount) {

        this.amount = amount;
    }

    /**
     * @return the responsibleParty
     */
    public String getResponsibleParty() {

        return responsibleParty;
    }

    /**
     * @param responsibleParty the responsibleParty to set
     */
    public void setResponsibleParty(
            String responsibleParty) {

        this.responsibleParty = responsibleParty;
    }

    /**
     * @return the accountType
     */
    public String getAccountType() {

        return accountType;
    }

    /**
     * @param accountType the accountType to set
     */
    public void setAccountType(
            String accountType) {

        this.accountType = accountType;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {

        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(
            String accountNumber) {

        this.accountNumber = accountNumber;
    }

    /**
     * @return the maskedCreditCard
     */
    public String getMaskedCreditCard() {

        return maskedCreditCard;
    }

    /**
     * @param maskedCreditCard the maskedCreditCard to set
     */
    public void setMaskedCreditCard(
            String maskedCreditCard) {

        this.maskedCreditCard = maskedCreditCard;
    }

    /**
     * @return the type
     */
    public String getType() {

        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(
            String type) {

        this.type = type;
    }

    /**
     * @return the expirationDate
     */
    public String getExpirationDate() {

        return expirationDate;
    }

    /**
     * @return the payorAddress
     */
    public AddressDetail getPayorAddress() {

        return payorAddress;
    }

    /**
     * @param payorAddress the payorAddress to set
     */
    public void setPayorAddress(
            AddressDetail payorAddress) {

        this.payorAddress = payorAddress;
    }

    /**
     * @param expirationDate the expirationDate to set
     */
    public void setExpirationDate(
            String expirationDate) {

        this.expirationDate = expirationDate;
    }

}
