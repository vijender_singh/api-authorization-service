package com.fedex.rscs.model.cshp;

/**
 * The enum is use to define the printing orientation. Based on printing orientation, client will
 * take appropriate actions.
 * 
 * @author 3798910
 *
 */
public enum PrintingOrientation {
    BOTTOM_EDGE_OF_TEXT_FIRST, 
    TOP_EDGE_OF_TEXT_FIRST
}
