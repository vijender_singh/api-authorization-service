package com.fedex.rscs.model.cshp;

import java.util.List;

import com.fedex.rscs.model.CommitInfo;
import com.fedex.rscs.model.ServiceDescriptionDetail;
import com.fedex.rscs.model.SurchargeDetail;

/**
 * Represents the Shipping Rate Data Model
 * 
 * @author 3798910
 *
 */
public class ShippingRateData {

    private ServiceDescriptionDetail serviceDetails;
    private String deliveryDayOfWeek;
    private boolean hal;
    private CommitInfo commitDetails;
    private String currency;
    private String ratingType;
    private double totalBaseCharges;
    private double totalSurcharges;
    private double totalTaxes;
    private double totalNetCharges;
    private double totalRebates;
    private double totalFreightDiscounts;
    private double totalNetFreight;
    private double totalNetFedExCharge;
    private double totalDutiesAndTaxes;
    private double totalAncillaryFeesAndTaxes;
    private double totalDutiesTaxesAndFees;
    private double totalNetChargeWithDutiesAndTaxes;
    private List<SurchargeDetail> surcharges;

    public ShippingRateData() {}

    /**
     * @param currency
     * @param ratingType
     * @param totalBaseCharges
     * @param totalSurcharges
     * @param totalTaxes
     * @param totalNetCharges
     */
    public ShippingRateData(String currency, String ratingType, double totalBaseCharges, double totalSurcharges,
            double totalTaxes, double totalNetCharges) {

        this.currency = currency;
        this.ratingType = ratingType;
        this.totalBaseCharges = totalBaseCharges;
        this.totalSurcharges = totalSurcharges;
        this.totalTaxes = totalTaxes;
        this.totalNetCharges = totalNetCharges;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {

        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(
            String currency) {

        this.currency = currency;
    }

    /**
     * @return the ratingType
     */
    public String getRatingType() {

        return ratingType;
    }

    /**
     * @param ratingType the ratingType to set
     */
    public void setRatingType(
            String ratingType) {

        this.ratingType = ratingType;
    }

    /**
     * @return the totalBaseCharges
     */
    public double getTotalBaseCharges() {

        return totalBaseCharges;
    }

    /**
     * @param totalBaseCharges the totalBaseCharges to set
     */
    public void setTotalBaseCharges(
            double totalBaseCharges) {

        this.totalBaseCharges = totalBaseCharges;
    }

    /**
     * @return the totalSurcharges
     */
    public double getTotalSurcharges() {

        return totalSurcharges;
    }

    /**
     * @param totalSurcharges the totalSurcharges to set
     */
    public void setTotalSurcharges(
            double totalSurcharges) {

        this.totalSurcharges = totalSurcharges;
    }

    /**
     * @return the totalTaxes
     */
    public double getTotalTaxes() {

        return totalTaxes;
    }

    /**
     * @param totalTaxes the totalTaxes to set
     */
    public void setTotalTaxes(
            double totalTaxes) {

        this.totalTaxes = totalTaxes;
    }

    /**
     * @return the totalNetCharges
     */
    public double getTotalNetCharges() {

        return totalNetCharges;
    }

    /**
     * @param totalNetCharges the totalNetCharges to set
     */
    public void setTotalNetCharges(
            double totalNetCharges) {

        this.totalNetCharges = totalNetCharges;
    }

    /**
     * @return the serviceDetails
     */
    public ServiceDescriptionDetail getServiceDetails() {

        return serviceDetails;
    }

    /**
     * @param serviceDetails to set the serviceDetails
     */
    public void setServiceDetails(
            ServiceDescriptionDetail serviceDetails) {

        this.serviceDetails = serviceDetails;
    }

    /**
     * @return the deliveryDayOfWeek
     */
    public String getDeliveryDayOfWeek() {

        return deliveryDayOfWeek;
    }

    /**
     * @param deliveryDayOfWeek to set the deliveryDayOfWeek
     */
    public void setDeliveryDayOfWeek(
            String deliveryDayOfWeek) {

        this.deliveryDayOfWeek = deliveryDayOfWeek;
    }

    /**
     * @return the hal
     */
    public boolean isHal() {

        return hal;
    }

    /**
     * @param hal to set the hal
     */
    public void setHal(
            boolean hal) {

        this.hal = hal;
    }

    /**
     * @return the commitDetails
     */
    public CommitInfo getCommitDetails() {

        return commitDetails;
    }

    /**
     * @param commitDetails to set the commitDetails
     */
    public void setCommitDetails(
            CommitInfo commitDetails) {

        this.commitDetails = commitDetails;
    }

    /**
     * @return the totalRebates
     */
    public double getTotalRebates() {

        return totalRebates;
    }

    /**
     * @param totalRebates to set the totalRebates
     */
    public void setTotalRebates(
            double totalRebates) {

        this.totalRebates = totalRebates;
    }

    /**
     * @return the totalFreightDiscounts
     */
    public double getTotalFreightDiscounts() {

        return totalFreightDiscounts;
    }

    /**
     * @param totalFreightDiscounts to set the totalFreightDiscounts
     */
    public void setTotalFreightDiscounts(
            double totalFreightDiscounts) {

        this.totalFreightDiscounts = totalFreightDiscounts;
    }

    /**
     * @return the totalNetFreight
     */
    public double getTotalNetFreight() {

        return totalNetFreight;
    }

    /**
     * @param totalNetFreight to set the totalNetFreight
     */
    public void setTotalNetFreight(
            double totalNetFreight) {

        this.totalNetFreight = totalNetFreight;
    }

    /**
     * @return the totalNetFedExCharge
     */
    public double getTotalNetFedExCharge() {

        return totalNetFedExCharge;
    }

    /**
     * @param totalNetFedExCharge to set the totalNetFedExCharge
     */
    public void setTotalNetFedExCharge(
            double totalNetFedExCharge) {

        this.totalNetFedExCharge = totalNetFedExCharge;
    }

    /**
     * @return the totalDutiesAndTaxes
     */
    public double getTotalDutiesAndTaxes() {

        return totalDutiesAndTaxes;
    }

    /**
     * @param totalDutiesAndTaxes to set the totalDutiesAndTaxes
     */
    public void setTotalDutiesAndTaxes(
            double totalDutiesAndTaxes) {

        this.totalDutiesAndTaxes = totalDutiesAndTaxes;
    }

    /**
     * @return the totalAncillaryFeesAndTaxes
     */
    public double getTotalAncillaryFeesAndTaxes() {

        return totalAncillaryFeesAndTaxes;
    }

    /**
     * @param totalAncillaryFeesAndTaxes to set the totalAncillaryFeesAndTaxes
     */
    public void setTotalAncillaryFeesAndTaxes(
            double totalAncillaryFeesAndTaxes) {

        this.totalAncillaryFeesAndTaxes = totalAncillaryFeesAndTaxes;
    }

    /**
     * @return the totalDutiesTaxesAndFees
     */
    public double getTotalDutiesTaxesAndFees() {

        return totalDutiesTaxesAndFees;
    }

    /**
     * @param totalDutiesTaxesAndFees to set the totalDutiesTaxesAndFees
     */
    public void setTotalDutiesTaxesAndFees(
            double totalDutiesTaxesAndFees) {

        this.totalDutiesTaxesAndFees = totalDutiesTaxesAndFees;
    }

    /**
     * @return the totalNetChargeWithDutiesAndTaxes
     */
    public double getTotalNetChargeWithDutiesAndTaxes() {

        return totalNetChargeWithDutiesAndTaxes;
    }

    /**
     * @param totalNetChargeWithDutiesAndTaxes to set the totalNetChargeWithDutiesAndTaxes
     */
    public void setTotalNetChargeWithDutiesAndTaxes(
            double totalNetChargeWithDutiesAndTaxes) {

        this.totalNetChargeWithDutiesAndTaxes = totalNetChargeWithDutiesAndTaxes;
    }

    /**
     * @return the surcharges
     */
    public List<SurchargeDetail> getSurcharges() {

        return surcharges;
    }

    /**
     * @param surcharges to set the surcharges
     */
    public void setSurcharges(
            List<SurchargeDetail> surcharges) {

        this.surcharges = surcharges;
    }

    @Override
    public String toString() {

        return "ShippingRateData [serviceDetails=" + serviceDetails + ", deliveryDayOfWeek=" + deliveryDayOfWeek
                + ", hal=" + hal + ", commitDetails=" + commitDetails + ", currency=" + currency + ", ratingType="
                + ratingType + ", totalBaseCharges=" + totalBaseCharges + ", totalSurcharges=" + totalSurcharges
                + ", totalTaxes=" + totalTaxes + ", totalNetCharges=" + totalNetCharges + ", totalRebates="
                + totalRebates + ", totalFreightDiscounts=" + totalFreightDiscounts + ", totalNetFreight="
                + totalNetFreight + ", totalNetFedExCharge=" + totalNetFedExCharge + ", totalDutiesAndTaxes="
                + totalDutiesAndTaxes + ", totalAncillaryFeesAndTaxes=" + totalAncillaryFeesAndTaxes
                + ", totalDutiesTaxesAndFees=" + totalDutiesTaxesAndFees + ", totalNetChargeWithDutiesAndTaxes="
                + totalNetChargeWithDutiesAndTaxes + ", surcharges=" + surcharges + "]";
    }

}
