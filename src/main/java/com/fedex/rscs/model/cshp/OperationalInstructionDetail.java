package com.fedex.rscs.model.cshp;

/**
 * OperationalInstruction model specification.
 * 
 * @author 5034922
 *
 */
public class OperationalInstructionDetail {

    private Integer number;
    private String content;

    
    public OperationalInstructionDetail() {}
    
    /**
     * @param number
     * @param content
     */
    public OperationalInstructionDetail(Integer number, String content) {

        this.number = number;
        this.content = content;
    }

    /**
     * @return the number
     */
    public Integer getNumber() {

        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(
            Integer number) {

        this.number = number;
    }

    /**
     * @return the content
     */
    public String getContent() {

        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(
            String content) {

        this.content = content;
    }

    @Override
    public String toString() {

        return "OperationalInstruction [number=" + number + ", content=" + content + "]";
    }


}
