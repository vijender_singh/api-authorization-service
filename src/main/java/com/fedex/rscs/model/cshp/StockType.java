package com.fedex.rscs.model.cshp;

/**
 * The enums is use to define the stock type. Based on stock type, client will take appropriate
 * actions.
 * 
 * @author 3798910
 *
 */
public enum StockType {
    PAPER_4X6, STOCK_4X6;
}
