package com.fedex.rscs.model.cshp;

/**
 * Define the Price Model
 * 
 * @author 3932968
 *
 */
public class Price {

    private String currency;
    private double amount;

    public Price() {}

    /**
     * @param currency
     * @param amount
     */
    public Price(String currency, double amount) {
        
        this.currency = currency;
        this.amount = amount;
    }
    
    /**
     * @return the currency
     */
    public String getCurrency() {
    
        return currency;
    }
    
    /**
     * @param currency the currency to set
     */
    public void setCurrency(
            String currency) {
    
        this.currency = currency;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
    
        return amount;
    }
    
    /**
     * @param amount the amount to set
     */
    public void setAmount(
            double amount) {
    
        this.amount = amount;
    }

    @Override
    public String toString() {

        return "Price [currency=" + currency + ", amount=" + amount + "]";
    }
    
}