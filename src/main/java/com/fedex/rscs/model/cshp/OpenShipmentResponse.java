package com.fedex.rscs.model.cshp;


/**
 * Model to class to capture the response of OpenShipment
 * 
 * @author 3798910
 *
 */
public class OpenShipmentResponse {

    private ShippingPaymentData shippingPaymentData;
    private ShippingRateData shippingRateData;
    private PreauthorizedRateData preauthorizedRateData;
    private CompleteShipmentDetail completeShipmentDetail;

    public OpenShipmentResponse() {}

    /**
     * @param shippingPaymentData
     * @param shippingRateData
     * @param completeShipmentDetail
     */
    public OpenShipmentResponse(ShippingPaymentData shippingPaymentData, ShippingRateData shippingRateData,
            CompleteShipmentDetail completeShipmentDetail) {

        this.shippingPaymentData = shippingPaymentData;
        this.shippingRateData = shippingRateData;
        this.completeShipmentDetail = completeShipmentDetail;
    }

    /**
     * @return the shippingPaymentData
     */
    public ShippingPaymentData getShippingPaymentData() {

        return shippingPaymentData;
    }

    /**
     * @param shippingPaymentData the shippingPaymentData to set
     */
    public void setShippingPaymentData(
            ShippingPaymentData shippingPaymentData) {

        this.shippingPaymentData = shippingPaymentData;
    }

    /**
     * @return the shippingRateData
     */
    public ShippingRateData getShippingRateData() {

        return shippingRateData;
    }

    /**
     * @param shippingRateData the shippingRateData to set
     */
    public void setShippingRateData(
            ShippingRateData shippingRateData) {

        this.shippingRateData = shippingRateData;
    }

    /**
     * @return the preauthorizedRateData
     */
    public PreauthorizedRateData getPreauthorizedRateData() {

        return preauthorizedRateData;
    }

    /**
     * @param preauthorizedRateData the preauthorizedRateData to set
     */
    public void setPreauthorizedRateData(
            PreauthorizedRateData preauthorizedRateData) {

        this.preauthorizedRateData = preauthorizedRateData;
    }

    /**
     * @return the completeShipmentDetail
     */
    public CompleteShipmentDetail getCompleteShipmentDetail() {

        return completeShipmentDetail;
    }

    /**
     * @param completeShipmentDetail the completeShipmentDetail to set
     */
    public void setCompleteShipmentDetail(
            CompleteShipmentDetail completeShipmentDetail) {

        this.completeShipmentDetail = completeShipmentDetail;
    }

    @Override
    public String toString() {

        return "OpenShipmentResponse [shippingPaymentData=" + shippingPaymentData + ", shippingRateData="
                + shippingRateData + ", preauthorizedRateData=" + preauthorizedRateData + ", completeShipmentDetail="
                + completeShipmentDetail + "]";
    }

}
