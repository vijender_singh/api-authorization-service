package com.fedex.rscs.model.cshp;


/**
 * Represents the supports electronic payment modes.
 * 
 * @author 981840
 *
 */
public enum EPaymentMode {

    CASH, CHECK, CREDIT_CARD
}
