package com.fedex.rscs.model.cshp;

/**
 * StringBarcode model specification.
 * 
 * @author 5034922
 *
 */
public class StringBarcodeDetail {

    private String type;
    private String value;

    /**
     * @param type
     * @param value
     */
    public StringBarcodeDetail(String type, String value) {

        this.type = type;
        this.value = value;
    }


    public StringBarcodeDetail() {

    }


    /**
     * @return the type
     */
    public String getType() {

        return type;
    }


    /**
     * @param type the type to set
     */
    public void setType(
            String type) {

        this.type = type;
    }


    /**
     * @return the value
     */
    public String getValue() {

        return value;
    }


    /**
     * @param value the value to set
     */
    public void setValue(
            String value) {

        this.value = value;
    }


    @Override
    public String toString() {

        return "StringBarcode [type=" + type + ", value=" + value + "]";
    }
}
