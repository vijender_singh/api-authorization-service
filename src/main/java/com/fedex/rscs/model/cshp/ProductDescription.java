package com.fedex.rscs.model.cshp;

import java.util.List;

/**
 * ProductDescription model specification.
 * 
 * @author 5034922
 *
 */
public class ProductDescription {

    protected String type;
    protected String code;
    protected String description;
    protected String astraDescription;
    protected List<ProductNameDetail> names;
    
    public ProductDescription() {}

    /**
     * @param type
     * @param code
     * @param description
     * @param astraDescription
     * @param names
     */
    public ProductDescription(String type, String code, String description, String astraDescription,
            List<ProductNameDetail> names) {

        this.type = type;
        this.code = code;
        this.description = description;
        this.astraDescription = astraDescription;
        this.names = names;
    }


    /**
     * @return the type
     */
    public String getType() {

        return type;
    }


    /**
     * @param type the type to set
     */
    public void setType(
            String type) {

        this.type = type;
    }


    /**
     * @return the code
     */
    public String getCode() {

        return code;
    }


    /**
     * @param code the code to set
     */
    public void setCode(
            String code) {

        this.code = code;
    }


    /**
     * @return the description
     */
    public String getDescription() {

        return description;
    }


    /**
     * @param description the description to set
     */
    public void setDescription(
            String description) {

        this.description = description;
    }


    /**
     * @return the astraDescription
     */
    public String getAstraDescription() {

        return astraDescription;
    }


    /**
     * @param astraDescription the astraDescription to set
     */
    public void setAstraDescription(
            String astraDescription) {

        this.astraDescription = astraDescription;
    }


    /**
     * @return the names
     */
    public List<ProductNameDetail> getNames() {

        return names;
    }


    /**
     * @param names the names to set
     */
    public void setNames(
            List<ProductNameDetail> names) {

        this.names = names;
    }


    @Override
    public String toString() {

        return "ProductDescription [type=" + type + ", code=" + code + ", description=" + description
                + ", astraDescription=" + astraDescription + ", names=" + names + "]";
    }


}
