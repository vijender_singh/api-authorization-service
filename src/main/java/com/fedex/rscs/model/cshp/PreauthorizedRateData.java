package com.fedex.rscs.model.cshp;

import java.util.List;

import com.fedex.rscs.client.srrs.dto.RateOption;
import com.fedex.rscs.model.common.PackagePreauthorizedRatedDetail;
import com.fedex.rscs.model.common.PreauthorizedRateCommitDetail;
import com.fedex.rscs.model.common.PreauthorizedRateServiceDetail;
import com.fedex.rscs.model.common.ShipmentPreauthorizedRatedDetail;

public class PreauthorizedRateData {

    private String serviceType;
    private String packagingType;
    private RateOption ratingType;
    private String signatureOption;
    private String deliveryDayOfWeek;
    private String deliveryStation;
    private String deliveryTimestamp;
    private String destinationAirportId;
    private boolean ineligibleForMoneyBackGuarantee;
    private String originServiceArea;
    private String destinationServiceArea;
    private String actualRateType;
    private PreauthorizedRateServiceDetail serviceDetails;
    private PreauthorizedRateCommitDetail commitDetails;
    private ShipmentPreauthorizedRatedDetail shipmentRateDetails;
    private List<PackagePreauthorizedRatedDetail> ratedPackages;

 
    public PreauthorizedRateData() {

    }


    /**
     * @return the serviceType
     */
    public String getServiceType() {

        return serviceType;
    }


    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(
            String serviceType) {

        this.serviceType = serviceType;
    }


    /**
     * @return the packagingType
     */
    public String getPackagingType() {

        return packagingType;
    }


    /**
     * @param packagingType the packagingType to set
     */
    public void setPackagingType(
            String packagingType) {

        this.packagingType = packagingType;
    }

    /**
     * @return the packagingType
     */
    public RateOption getRatingType() {

        return ratingType;
    }

    /**
     * @param packagingType the packagingType to set
     */
    public void setRatingType(
            RateOption ratingType) {

        this.ratingType = ratingType;
    }


    /**
     * @return the signatureOption
     */
    public String getSignatureOption() {

        return signatureOption;
    }


    /**
     * @param signatureOption the signatureOption to set
     */
    public void setSignatureOption(
            String signatureOption) {

        this.signatureOption = signatureOption;
    }


    /**
     * @return the deliveryDayOfWeek
     */
    public String getDeliveryDayOfWeek() {

        return deliveryDayOfWeek;
    }


    /**
     * @param deliveryDayOfWeek the deliveryDayOfWeek to set
     */
    public void setDeliveryDayOfWeek(
            String deliveryDayOfWeek) {

        this.deliveryDayOfWeek = deliveryDayOfWeek;
    }


    /**
     * @return the deliveryStation
     */
    public String getDeliveryStation() {

        return deliveryStation;
    }


    /**
     * @param deliveryStation the deliveryStation to set
     */
    public void setDeliveryStation(
            String deliveryStation) {

        this.deliveryStation = deliveryStation;
    }


    /**
     * @return the deliveryTimestamp
     */
    public String getDeliveryTimestamp() {

        return deliveryTimestamp;
    }


    /**
     * @param deliveryTimestamp the deliveryTimestamp to set
     */
    public void setDeliveryTimestamp(
            String deliveryTimestamp) {

        this.deliveryTimestamp = deliveryTimestamp;
    }


    /**
     * @return the destinationAirportId
     */
    public String getDestinationAirportId() {

        return destinationAirportId;
    }


    /**
     * @param destinationAirportId the destinationAirportId to set
     */
    public void setDestinationAirportId(
            String destinationAirportId) {

        this.destinationAirportId = destinationAirportId;
    }


    /**
     * @return the ineligibleForMoneyBackGuarantee
     */
    public boolean isIneligibleForMoneyBackGuarantee() {

        return ineligibleForMoneyBackGuarantee;
    }


    /**
     * @param ineligibleForMoneyBackGuarantee the ineligibleForMoneyBackGuarantee to set
     */
    public void setIneligibleForMoneyBackGuarantee(
            boolean ineligibleForMoneyBackGuarantee) {

        this.ineligibleForMoneyBackGuarantee = ineligibleForMoneyBackGuarantee;
    }


    /**
     * @return the originServiceArea
     */
    public String getOriginServiceArea() {

        return originServiceArea;
    }


    /**
     * @param originServiceArea the originServiceArea to set
     */
    public void setOriginServiceArea(
            String originServiceArea) {

        this.originServiceArea = originServiceArea;
    }


    /**
     * @return the destinationServiceArea
     */
    public String getDestinationServiceArea() {

        return destinationServiceArea;
    }


    /**
     * @param destinationServiceArea the destinationServiceArea to set
     */
    public void setDestinationServiceArea(
            String destinationServiceArea) {

        this.destinationServiceArea = destinationServiceArea;
    }


    /**
     * @return the actualRateType
     */
    public String getActualRateType() {

        return actualRateType;
    }


    /**
     * @param actualRateType the actualRateType to set
     */
    public void setActualRateType(
            String actualRateType) {

        this.actualRateType = actualRateType;
    }


    /**
     * @return the serviceDetails
     */
    public PreauthorizedRateServiceDetail getServiceDetails() {

        return serviceDetails;
    }


    /**
     * @param serviceDetails the serviceDetails to set
     */
    public void setServiceDetails(
            PreauthorizedRateServiceDetail serviceDetails) {

        this.serviceDetails = serviceDetails;
    }


    /**
     * @return the commitDetails
     */
    public PreauthorizedRateCommitDetail getCommitDetails() {

        return commitDetails;
    }


    /**
     * @param commitDetails the commitDetails to set
     */
    public void setCommitDetails(
            PreauthorizedRateCommitDetail commitDetails) {

        this.commitDetails = commitDetails;
    }


    /**
     * @return the shipmentRateDetails
     */
    public ShipmentPreauthorizedRatedDetail getShipmentRateDetails() {

        return shipmentRateDetails;
    }


    /**
     * @param shipmentRateDetails the shipmentRateDetails to set
     */
    public void setShipmentRateDetails(
            ShipmentPreauthorizedRatedDetail shipmentRateDetails) {

        this.shipmentRateDetails = shipmentRateDetails;
    }


    /**
     * @return the ratedPackages
     */
    public List<PackagePreauthorizedRatedDetail> getRatedPackages() {

        return ratedPackages;
    }


    /**
     * @param ratedPackages the ratedPackages to set
     */
    public void setRatedPackages(
            List<PackagePreauthorizedRatedDetail> ratedPackages) {

        this.ratedPackages = ratedPackages;
    }


    @Override
    public String toString() {

        return "PreauthorizedRateData [serviceType=" + serviceType + ", packagingType=" + packagingType
                + ", ratingType=" + ratingType + ", signatureOption=" + signatureOption + ", deliveryDayOfWeek="
                + deliveryDayOfWeek + ", deliveryStation=" + deliveryStation + ", deliveryTimestamp="
                + deliveryTimestamp + ", destinationAirportId=" + destinationAirportId
                + ", ineligibleForMoneyBackGuarantee=" + ineligibleForMoneyBackGuarantee + ", originServiceArea="
                + originServiceArea + ", destinationServiceArea=" + destinationServiceArea + ", actualRateType="
                + actualRateType + ", serviceDetails=" + serviceDetails + ", commitDetails=" + commitDetails
                + ", shipmentRateDetails=" + shipmentRateDetails + ", ratedPackages=" + ratedPackages + "]";
    }
}
