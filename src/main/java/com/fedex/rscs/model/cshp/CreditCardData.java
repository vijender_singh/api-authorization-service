package com.fedex.rscs.model.cshp;

/**
 * Define the Credit Card model
 * 
 * @author 3932968
 *
 */
public class CreditCardData {

    private String maskedCreditCard;
    private String type;
    private String expirationDate;

    public CreditCardData() {}

    /**
     * @param maskedCreditCard
     * @param type
     * @param expirationDate
     */
    public CreditCardData(String maskedCreditCard, String type, String expirationDate) {

        this.maskedCreditCard = maskedCreditCard;
        this.type = type;
        this.expirationDate = expirationDate;
    }

    /**
     * @return the maskedCreditCard
     */
    public String getMaskedCreditCard() {

        return maskedCreditCard;
    }

    /**
     * @param maskedCreditCard the maskedCreditCard to set
     */
    public void setMaskedCreditCard(
            String maskedCreditCard) {

        this.maskedCreditCard = maskedCreditCard;
    }

    /**
     * @return the type
     */
    public String getType() {

        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(
            String type) {

        this.type = type;
    }

    /**
     * @return the expirationDate
     */
    public String getExpirationDate() {

        return expirationDate;
    }

    /**
     * @param expirationDate the expirationDate to set
     */
    public void setExpirationDate(
            String expirationDate) {

        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {

        return "CreditCardData [maskedCreditCard=" + maskedCreditCard + ", type=" + type + ", expirationDate="
                + expirationDate + "]";
    }

}
