package com.fedex.rscs.model.cshp;

/**
 * Represents the Tracking Sequence model
 * 
 * @author 3798910
 *
 */
public class TrackingSequenceDetail {

    private String trackingNumber;
    private String barcode;
    private int sequence;

    public TrackingSequenceDetail() {}
    
    
    /**
     * @param trackingNumber
     * @param barcode
     * @param sequence
     */
    public TrackingSequenceDetail(String trackingNumber, String barcode, int sequence) {

        this.trackingNumber = trackingNumber;
        this.barcode = barcode;
        this.sequence = sequence;
    }



    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {

        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {

        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the barcode
     */
    public String getBarcode() {

        return barcode;
    }

    /**
     * @param barcode the barcode to set
     */
    public void setBarcode(
            String barcode) {

        this.barcode = barcode;
    }

    /**
     * @return the sequence
     */
    public int getSequence() {

        return sequence;
    }

    /**
     * @param sequence the sequence to set
     */
    public void setSequence(
            int sequence) {

        this.sequence = sequence;
    }

    @Override
    public String toString() {

        return "TrackingSequence [trackingNumber=" + trackingNumber + ", barcode=" + barcode + ", sequence=" + sequence
                + "]";
    }

}
