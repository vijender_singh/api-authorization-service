package com.fedex.rscs.model.cshp;

/**
 * Enum for special Service code
 * 
 * @author 3798910
 *
 */
public enum SpecialServiceCode {

    DIRECT("SIGNATURE_OPTION_DIRECT", "10"), 
    NON_STANDARD_CONTAINER("NON_STANDARD_CONTAINER", "16"), 
    INDIRECT("INDIRECT", "34"), 
    ADULT("ADULT", "35"), 
    NO_SIGNATURE_REQUIRED("NO_SIGNATURE_REQUIRED", "36"), 
    BATTERY("BATTERY", "93"), 
    DRY_ICE("DRY_ICE", "06"), 
    DANGEROUS_GOODS("DANGEROUS_GOODS", "A5"),
    EVENT_NOTIFICATION("EVENT_NOTIFICATION", "");

    private String type;
    private String specialServiceCode;

    /**
     * @param type
     * @param specialServiceCode
     */
    private SpecialServiceCode(String type, String specialServiceCode) {
        this.type = type;
        this.specialServiceCode = specialServiceCode;
    }

    /**
     * @return the type
     */
    public String getType() {

        return type;
    }

    /**
     * @return the specialServiceCode
     */
    public String getSpecialServiceCode() {

        return specialServiceCode;
    }

}

