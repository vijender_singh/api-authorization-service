package com.fedex.rscs.model.cshp;

import java.util.List;

/**
 * PackageOperationalDetail model specification.
 * 
 * @author 5034922
 *
 */
public class PackageOperationData {

    private String astraHandlingText;
    private List<OperationalInstructionDetail> operationalInstructions;
    private PackageBarcodeDetail barcode;
    private String groundServiceCode;
    

    public PackageOperationData() {}
    
    /**
     * @param astraHandlingText
     * @param operationalInstructions
     * @param barcodes
     * @param groundServiceCode
     */
    public PackageOperationData(String astraHandlingText, List<OperationalInstructionDetail> operationalInstructions,
            PackageBarcodeDetail barcode, String groundServiceCode) {

        this.astraHandlingText = astraHandlingText;
        this.operationalInstructions = operationalInstructions;
        this.barcode = barcode;
        this.groundServiceCode = groundServiceCode;
    }

    /**
     * @return the astraHandlingText
     */
    public String getAstraHandlingText() {

        return astraHandlingText;
    }

    /**
     * @param astraHandlingText the astraHandlingText to set
     */
    public void setAstraHandlingText(
            String astraHandlingText) {

        this.astraHandlingText = astraHandlingText;
    }

    /**
     * @return the operationalInstructions
     */
    public List<OperationalInstructionDetail> getOperationalInstructions() {

        return operationalInstructions;
    }

    /**
     * @param operationalInstructions the operationalInstructions to set
     */
    public void setOperationalInstructions(
            List<OperationalInstructionDetail> operationalInstructions) {

        this.operationalInstructions = operationalInstructions;
    }

    /**
     * 
     * @return the barcode
     */
    public PackageBarcodeDetail getBarcode() {
    
        return barcode;
    }

    /**
     * 
     * @param barcode to set the barcode
     */
    public void setBarcode(
            PackageBarcodeDetail barcode) {
    
        this.barcode = barcode;
    }

    /**
     * @return the groundServiceCode
     */
    public String getGroundServiceCode() {

        return groundServiceCode;
    }

    /**
     * @param groundServiceCode the groundServiceCode to set
     */
    public void setGroundServiceCode(
            String groundServiceCode) {

        this.groundServiceCode = groundServiceCode;
    }

    @Override
    public String toString() {

        return "PackageOperationalDetail [astraHandlingText=" + astraHandlingText + ", operationalInstructions="
                + operationalInstructions + ", barcode=" + barcode + ", groundServiceCode=" + groundServiceCode + "]";
    }
}
