package com.fedex.rscs.model.cshp;

/**
 * package confirmation details specification.
 * 
 * @author 5034922
 *
 */
public class ConfirmedPackageData {

    private TrackingSequenceDetail trackingId;
    private boolean master;
    private ShippingDocument label;


    public ConfirmedPackageData() {

    }

    public ConfirmedPackageData(TrackingSequenceDetail trackingId, boolean master, ShippingDocument label) {

    }

    /**
     * @return the trackingId
     */
    public TrackingSequenceDetail getTrackingId() {

        return trackingId;
    }

    /**
     * @param trackingId the trackingId to set
     */
    public void setTrackingId(
            TrackingSequenceDetail trackingId) {

        this.trackingId = trackingId;
    }

    /**
     * @return the master
     */
    public boolean isMaster() {

        return master;
    }

    /**
     * @param master the master to set
     */
    public void setMaster(
            boolean master) {

        this.master = master;
    }

    /**
     * @return the label
     */
    public ShippingDocument getLabel() {

        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(
            ShippingDocument label) {

        this.label = label;
    }

    @Override
    public String toString() {

        return "ConfirmedPackageData [trackingId=" + trackingId + ", master=" + master + ", label=" + label + "]";
    }

}
