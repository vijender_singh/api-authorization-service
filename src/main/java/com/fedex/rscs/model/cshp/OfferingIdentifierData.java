package com.fedex.rscs.model.cshp;

/**
 * OfferingIdentifierDetail model specification.
 * 
 * @author 5034922
 *
 */
public class OfferingIdentifierData {

    protected String id;
    protected String type;
    protected String code;
    
    
    public OfferingIdentifierData() {}
    
    /**
     * @param id
     * @param type
     * @param code
     */
    public OfferingIdentifierData(String id, String type, String code) {

        this.id = id;
        this.type = type;
        this.code = code;
    }

    /**
     * @return the id
     */
    public String getId() {

        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(
            String id) {

        this.id = id;
    }

    /**
     * @return the type
     */
    public String getType() {

        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(
            String type) {

        this.type = type;
    }

    /**
     * @return the code
     */
    public String getCode() {

        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(
            String code) {

        this.code = code;
    }

    @Override
    public String toString() {

        return "OfferingIdentifierDetail [id=" + id + ", type=" + type + ", code=" + code + "]";
    }

}
