package com.fedex.rscs.model.cshp;

import java.util.ArrayList;
import java.util.List;

import com.fedex.rscs.model.common.EventNotification;

/**
 * ConfirmedShipmentData model specification.
 * 
 * @author 5034922
 *
 */
public class ConfirmedShipmentData {

    private String carrierCode;
    private String carrierType;
    private TrackingSequenceDetail masterTrackingId;
    private Integer packageCount;
    private List<ConfirmedPackageData> packageLineItems;
    private List<EventNotification> notifications;
    private boolean lineItemSuccess;
    private TransactionStatus shipmentStatus;

    public ConfirmedShipmentData() {}

    /**
     * @param carrierCode
     * @param carrierType
     * @param masterTrackingId
     * @param packageCount
     * @param packageLineItems
     * @param notifications
     * @param lineItemSuccess
     */
    public ConfirmedShipmentData(String carrierCode, String carrierType, TrackingSequenceDetail masterTrackingId,
            Integer packageCount, List<ConfirmedPackageData> packageLineItems, List<EventNotification> notifications,
            boolean lineItemSuccess) {

        this.carrierCode = carrierCode;
        this.carrierType = carrierType;
        this.masterTrackingId = masterTrackingId;
        this.packageCount = packageCount;
        this.packageLineItems = packageLineItems;
        this.notifications = notifications;
        this.lineItemSuccess = lineItemSuccess;
    }

    /**
     * @return the notifications
     */
    public List<EventNotification> getNotifications() {
    
        return notifications;
    }
    
    /**
     * @param notifications the notifications to set
     */
    public void setNotifications(
            List<EventNotification> notifications) {
    
        this.notifications = notifications;
    }

    /**
     * @return the lineItemSuccess
     */
    public boolean isLineItemSuccess() {
    
        return lineItemSuccess;
    }

    /**
     * @param lineItemSuccess the lineItemSuccess to set
     */
    public void setLineItemSuccess(
            boolean lineItemSuccess) {
    
        this.lineItemSuccess = lineItemSuccess;
    }

    /**
     * @return the carrierCode
     */
    public String getCarrierCode() {

        return carrierCode;
    }

    /**
     * @param carrierCode the carrierCode to set
     */
    public void setCarrierCode(
            String carrierCode) {

        this.carrierCode = carrierCode;
    }

    /**
     * @return the carrierType
     */
    public String getCarrierType() {

        return carrierType;
    }

    /**
     * @param carrierType the carrierType to set
     */
    public void setCarrierType(
            String carrierType) {

        this.carrierType = carrierType;
    }

    /**
     * @return the masterTrackingId
     */
    public TrackingSequenceDetail getMasterTrackingId() {

        return masterTrackingId;
    }

    /**
     * @param masterTrackingId the masterTrackingId to set
     */
    public void setMasterTrackingId(
            TrackingSequenceDetail masterTrackingId) {

        this.masterTrackingId = masterTrackingId;
    }

    /**
     * @return the packageCount
     */
    public Integer getPackageCount() {

        return packageCount;
    }

    /**
     * @param packageCount the packageCount to set
     */
    public void setPackageCount(
            Integer packageCount) {

        this.packageCount = packageCount;
    }


    
    /**
     * @return the packageLineItems
     */
    public List<ConfirmedPackageData> getPackageLineItems() {
    
        return packageLineItems;
    }


    
    /**
     * @param packageLineItems the packageLineItems to set
     */
    public void setPackageLineItems(
            List<ConfirmedPackageData> packageLineItems) {
    
        this.packageLineItems = packageLineItems;
    }

    /**
     * Add Notification associated with the underline transaction.
     * 
     * @param PackageNotification
     */
    public void addNotification(
            final EventNotification notification) {

        if (notification != null) {
            if (this.notifications == null) {
                this.notifications = new ArrayList<>();
            }
            this.notifications.add(notification);
        }
    }
        
    /**
     * @return the shipmentStatus
     */
    public TransactionStatus getShipmentStatus() {

        return shipmentStatus;
    }

    /**
     * @param shipmentStatus to set the shipmentStatus
     */
    public void setShipmentStatus(
            TransactionStatus shipmentStatus) {
        
        this.shipmentStatus = shipmentStatus;
    }

    @Override
    public String toString() {

        return "ConfirmedShipmentData [carrierCode=" + carrierCode + ", carrierType=" + carrierType
                + ", masterTrackingId=" + masterTrackingId + ", packageCount=" + packageCount + ", packageLineItems="
                + packageLineItems + ", notifications=" + notifications + ", lineItemSuccess=" + lineItemSuccess
                + ", shipmentStatus=" + shipmentStatus + "]";
    }

}
