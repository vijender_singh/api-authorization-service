package com.fedex.rscs.model.cshp;

/**
 * TrackingId model specification
 * 
 * @author 5034922
 *
 */
public class TrackingIdDetail {

    private String trackingIdType;
    private String formId;
    private String uspsApplicationId;
    private String trackingNumber;
    
    
    public TrackingIdDetail() {}
    
    /**
     * @param trackingIdType
     * @param formId
     * @param uspsApplicationId
     * @param trackingNumber
     */
    public TrackingIdDetail(String trackingIdType, String formId, String uspsApplicationId, String trackingNumber) {

        this.trackingIdType = trackingIdType;
        this.formId = formId;
        this.uspsApplicationId = uspsApplicationId;
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the trackingIdType
     */
    public String getTrackingIdType() {

        return trackingIdType;
    }

    /**
     * @param trackingIdType the trackingIdType to set
     */
    public void setTrackingIdType(
            String trackingIdType) {

        this.trackingIdType = trackingIdType;
    }

    /**
     * @return the formId
     */
    public String getFormId() {

        return formId;
    }

    /**
     * @param formId the formId to set
     */
    public void setFormId(
            String formId) {

        this.formId = formId;
    }

    /**
     * @return the uspsApplicationId
     */
    public String getUspsApplicationId() {

        return uspsApplicationId;
    }

    /**
     * @param uspsApplicationId the uspsApplicationId to set
     */
    public void setUspsApplicationId(
            String uspsApplicationId) {

        this.uspsApplicationId = uspsApplicationId;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {

        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {

        this.trackingNumber = trackingNumber;
    }

    @Override
    public String toString() {

        return "TrackingId [trackingIdType=" + trackingIdType + ", formId=" + formId + ", uspsApplicationId="
                + uspsApplicationId + ", trackingNumber=" + trackingNumber + "]";
    }


}
