package com.fedex.rscs.model.cshp;


/**
 * The enum to define the transaction status for shipments.
 * 
 * CONFIRMED: Used to represent that shipment is confirmed. 
 * CREATED: Used to represent that shipment is in open state. 
 * VOID: Used to represent that shipment is deleted. 
 * MODIFIED: Used to represent modified shipment
 * RETURNED: Used to represent confirmed shipment is deleted.
 * 
 * @author 3798910
 *
 */
public enum TransactionStatus {

    CONFIRMED, CREATED, VOID, MODIFIED, RETURNED;

}
