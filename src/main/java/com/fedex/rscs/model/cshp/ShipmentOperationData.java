package com.fedex.rscs.model.cshp;

import java.util.List;

/**
 * ShipmentOperationalDetail model specification.
 * 
 * @author 5034922
 *
 */
public class ShipmentOperationData {

    private String ursaPrefixCode;
    private String ursaSuffixCode;
    private String transitTime;
    private String originLocationId;
    private String originLocationNumber;
    private String originServiceArea;
    private String destinationLocationId;
    private String destinationLocationNumber;
    private String destinationServiceArea;
    private String destinationLocationStateOrProvinceCode;
    private String deliveryDate;
    private String deliveryDay;
    private String commitDate;
    private String commitDay;
    private List<String> deliveryEligibilities;
    private String astraPlannedServiceLabel;
    private String astraDescription;
    private String postalCode;
    private String stateOrProvinceCode;
    private String countryCode;
    private String airportId;
    private String serviceCode;
    private String packagingCode;

    public ShipmentOperationData() {}

    /**
     * @return the ursaPrefixCode
     */
    public String getUrsaPrefixCode() {

        return ursaPrefixCode;
    }

    /**
     * @param ursaPrefixCode the ursaPrefixCode to set
     */
    public void setUrsaPrefixCode(
            String ursaPrefixCode) {

        this.ursaPrefixCode = ursaPrefixCode;
    }

    /**
     * @return the ursaSuffixCode
     */
    public String getUrsaSuffixCode() {

        return ursaSuffixCode;
    }

    /**
     * @param ursaSuffixCode the ursaSuffixCode to set
     */
    public void setUrsaSuffixCode(
            String ursaSuffixCode) {

        this.ursaSuffixCode = ursaSuffixCode;
    }

    /**
     * @return the transitTime
     */
    public String getTransitTime() {

        return transitTime;
    }

    /**
     * @param transitTime the transitTime to set
     */
    public void setTransitTime(
            String transitTime) {

        this.transitTime = transitTime;
    }

    /**
     * @return the originLocationId
     */
    public String getOriginLocationId() {

        return originLocationId;
    }

    /**
     * @param originLocationId the originLocationId to set
     */
    public void setOriginLocationId(
            String originLocationId) {

        this.originLocationId = originLocationId;
    }

    /**
     * @return the originLocationNumber
     */
    public String getOriginLocationNumber() {

        return originLocationNumber;
    }

    /**
     * @param originLocationNumber the originLocationNumber to set
     */
    public void setOriginLocationNumber(
            String originLocationNumber) {

        this.originLocationNumber = originLocationNumber;
    }

    /**
     * @return the originServiceArea
     */
    public String getOriginServiceArea() {

        return originServiceArea;
    }

    /**
     * @param originServiceArea the originServiceArea to set
     */
    public void setOriginServiceArea(
            String originServiceArea) {

        this.originServiceArea = originServiceArea;
    }

    /**
     * @return the destinationLocationId
     */
    public String getDestinationLocationId() {

        return destinationLocationId;
    }

    /**
     * @param destinationLocationId the destinationLocationId to set
     */
    public void setDestinationLocationId(
            String destinationLocationId) {

        this.destinationLocationId = destinationLocationId;
    }

    /**
     * @return the destinationLocationNumber
     */
    public String getDestinationLocationNumber() {

        return destinationLocationNumber;
    }

    /**
     * @param destinationLocationNumber the destinationLocationNumber to set
     */
    public void setDestinationLocationNumber(
            String destinationLocationNumber) {

        this.destinationLocationNumber = destinationLocationNumber;
    }

    /**
     * @return the destinationServiceArea
     */
    public String getDestinationServiceArea() {

        return destinationServiceArea;
    }

    /**
     * @param destinationServiceArea the destinationServiceArea to set
     */
    public void setDestinationServiceArea(
            String destinationServiceArea) {

        this.destinationServiceArea = destinationServiceArea;
    }

    /**
     * @return the destinationLocationStateOrProvinceCode
     */
    public String getDestinationLocationStateOrProvinceCode() {

        return destinationLocationStateOrProvinceCode;
    }

    /**
     * @param destinationLocationStateOrProvinceCode the destinationLocationStateOrProvinceCode to
     *        set
     */
    public void setDestinationLocationStateOrProvinceCode(
            String destinationLocationStateOrProvinceCode) {

        this.destinationLocationStateOrProvinceCode = destinationLocationStateOrProvinceCode;
    }

    /**
     * @return the deliveryDate
     */
    public String getDeliveryDate() {

        return deliveryDate;
    }

    /**
     * @param deliveryDate the deliveryDate to set
     */
    public void setDeliveryDate(
            String deliveryDate) {

        this.deliveryDate = deliveryDate;
    }

    /**
     * @return the deliveryDay
     */
    public String getDeliveryDay() {

        return deliveryDay;
    }

    /**
     * @param deliveryDay the deliveryDay to set
     */
    public void setDeliveryDay(
            String deliveryDay) {

        this.deliveryDay = deliveryDay;
    }

    /**
     * @return the commitDate
     */
    public String getCommitDate() {

        return commitDate;
    }

    /**
     * @param commitDate the commitDate to set
     */
    public void setCommitDate(
            String commitDate) {

        this.commitDate = commitDate;
    }

    /**
     * @return the commitDay
     */
    public String getCommitDay() {

        return commitDay;
    }

    /**
     * @param commitDay the commitDay to set
     */
    public void setCommitDay(
            String commitDay) {

        this.commitDay = commitDay;
    }

    /**
     * @return the deliveryEligibilities
     */
    public List<String> getDeliveryEligibilities() {

        return deliveryEligibilities;
    }

    /**
     * @param deliveryEligibilities the deliveryEligibilities to set
     */
    public void setDeliveryEligibilities(
            List<String> deliveryEligibilities) {

        this.deliveryEligibilities = deliveryEligibilities;
    }

    /**
     * @return the astraPlannedServiceLabel
     */
    public String getAstraPlannedServiceLabel() {

        return astraPlannedServiceLabel;
    }

    /**
     * @param astraPlannedServiceLabel the astraPlannedServiceLabel to set
     */
    public void setAstraPlannedServiceLabel(
            String astraPlannedServiceLabel) {

        this.astraPlannedServiceLabel = astraPlannedServiceLabel;
    }

    /**
     * @return the astraDescription
     */
    public String getAstraDescription() {

        return astraDescription;
    }

    /**
     * @param astraDescription the astraDescription to set
     */
    public void setAstraDescription(
            String astraDescription) {

        this.astraDescription = astraDescription;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {

        return postalCode;
    }

    /**
     * @param postalCode the postalCode to set
     */
    public void setPostalCode(
            String postalCode) {

        this.postalCode = postalCode;
    }

    /**
     * @return the stateOrProvinceCode
     */
    public String getStateOrProvinceCode() {

        return stateOrProvinceCode;
    }

    /**
     * @param stateOrProvinceCode the stateOrProvinceCode to set
     */
    public void setStateOrProvinceCode(
            String stateOrProvinceCode) {

        this.stateOrProvinceCode = stateOrProvinceCode;
    }

    /**
     * @return the countryCode
     */
    public String getCountryCode() {

        return countryCode;
    }

    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(
            String countryCode) {

        this.countryCode = countryCode;
    }

    /**
     * @return the airportId
     */
    public String getAirportId() {

        return airportId;
    }

    /**
     * @param airportId the airportId to set
     */
    public void setAirportId(
            String airportId) {

        this.airportId = airportId;
    }

    /**
     * @return the serviceCode
     */
    public String getServiceCode() {

        return serviceCode;
    }

    /**
     * @param serviceCode the serviceCode to set
     */
    public void setServiceCode(
            String serviceCode) {

        this.serviceCode = serviceCode;
    }

    /**
     * @return the packagingCode
     */
    public String getPackagingCode() {

        return packagingCode;
    }

    /**
     * @param packagingCode the packagingCode to set
     */
    public void setPackagingCode(
            String packagingCode) {

        this.packagingCode = packagingCode;
    }

    @Override
    public String toString() {

        return "ShipmentOperationalDetail [ursaPrefixCode=" + ursaPrefixCode + ", ursaSuffixCode=" + ursaSuffixCode
                + ", transitTime=" + transitTime + ", originLocationId=" + originLocationId + ", originLocationNumber="
                + originLocationNumber + ", originServiceArea=" + originServiceArea + ", destinationLocationId="
                + destinationLocationId + ", destinationLocationNumber=" + destinationLocationNumber
                + ", destinationServiceArea=" + destinationServiceArea + ", destinationLocationStateOrProvinceCode="
                + destinationLocationStateOrProvinceCode + ", deliveryDate=" + deliveryDate + ", deliveryDay="
                + deliveryDay + ", commitDate=" + commitDate + ", commitDay=" + commitDay + ", deliveryEligibilities="
                + deliveryEligibilities + ", astraPlannedServiceLabel=" + astraPlannedServiceLabel
                + ", astraDescription=" + astraDescription + ", postalCode=" + postalCode + ", stateOrProvinceCode="
                + stateOrProvinceCode + ", countryCode=" + countryCode + ", airportId=" + airportId + ", serviceCode="
                + serviceCode + ", packagingCode=" + packagingCode + "]";
    }

}
