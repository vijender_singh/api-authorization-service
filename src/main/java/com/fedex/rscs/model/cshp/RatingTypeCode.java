package com.fedex.rscs.model.cshp;

/**
 * @author 3798910
 *
 */
public enum RatingTypeCode {
    
    ONE_RATE("OR"), STANDARD_RATE("SR");

    private String ratingCode;

    RatingTypeCode(String ratingCode) {

        this.ratingCode = ratingCode;
    }

    public String getRatingCode() {

        return ratingCode;
    }
}
