package com.fedex.rscs.model.cshp;

import java.util.List;

/**
 * PackageBarcodes model specification.
 * 
 * @author 5034922
 *
 */
public class PackageBarcodeDetail {

    private List<BinaryBarcodeDetail> binaryBarcodes;
    private List<StringBarcodeDetail> stringBarcodes;

    /**
     * @param binaryBarcodes
     * @param stringBarcodes
     */
    public PackageBarcodeDetail(List<BinaryBarcodeDetail> binaryBarcodes, List<StringBarcodeDetail> stringBarcodes) {

        this.binaryBarcodes = binaryBarcodes;
        this.stringBarcodes = stringBarcodes;
    }

    /**
     * @return the binaryBarcodes
     */
    public List<BinaryBarcodeDetail> getBinaryBarcodes() {

        return binaryBarcodes;
    }

    /**
     * @param binaryBarcodes the binaryBarcodes to set
     */
    public void setBinaryBarcodes(
            List<BinaryBarcodeDetail> binaryBarcodes) {

        this.binaryBarcodes = binaryBarcodes;
    }

    /**
     * @return the stringBarcodes
     */
    public List<StringBarcodeDetail> getStringBarcodes() {

        return stringBarcodes;
    }

    /**
     * @param stringBarcodes the stringBarcodes to set
     */
    public void setStringBarcodes(
            List<StringBarcodeDetail> stringBarcodes) {

        this.stringBarcodes = stringBarcodes;
    }

    @Override
    public String toString() {

        return "PackageBarcodes [binaryBarcodes=" + binaryBarcodes + ", stringBarcodes=" + stringBarcodes + "]";
    }
}
