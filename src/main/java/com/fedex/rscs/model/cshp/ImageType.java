package com.fedex.rscs.model.cshp;
/**
 * The enums is use to define the image type.
 * <li>PNG
 * <li>PDF<br>
 * Based on image type, client will take appropriate actions.
 * 
 * @author 3798910
 *
 */
public enum ImageType {
    PNG, PDF, ZPLII;
}