package com.fedex.rscs.model;

/**
 * SpecialServicesRequested model specification.
 * 
 * @author 5034922
 *
 */
public class SpecialServiceDetail {

    private String specialServiceType;
    private String specialServiceSubType;
    private String displayText;
    private String description;

    public SpecialServiceDetail() {

    }

    /**
     * @param specialServiceType
     * @param specialServiceSubType
     * @param displayText
     * @param description
     */
    public SpecialServiceDetail(String specialServiceType, String specialServiceSubType, String displayText,
            String description) {

        this.specialServiceType = specialServiceType;
        this.specialServiceSubType = specialServiceSubType;
        this.displayText = displayText;
        this.description = description;
    }

    /**
     * @return the specialServiceType
     */
    public String getSpecialServiceType() {

        return specialServiceType;
    }

    /**
     * @param specialServiceType the specialServiceType to set
     */
    public void setSpecialServiceType(
            String specialServiceType) {

        this.specialServiceType = specialServiceType;
    }

    /**
     * @return the specialServiceSubType
     */
    public String getSpecialServiceSubType() {

        return specialServiceSubType;
    }

    /**
     * @param specialServiceSubType the specialServiceSubType to set
     */
    public void setSpecialServiceSubType(
            String specialServiceSubType) {

        this.specialServiceSubType = specialServiceSubType;
    }

    /**
     * @return the displayText
     */
    public String getDisplayText() {

        return displayText;
    }

    /**
     * @param displayText the displayText to set
     */
    public void setDisplayText(
            String displayText) {

        this.displayText = displayText;
    }

    /**
     * @return the description
     */
    public String getDescription() {

        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(
            String description) {

        this.description = description;
    }

    @Override
    public String toString() {

        return "SpecialServicesRequested [specialServiceType=" + specialServiceType + ", specialServiceSubType="
                + specialServiceSubType + ", displayText=" + displayText + ", description=" + description + "]";
    }

}
