package com.fedex.rscs.model;

/**
 * wapper class for open shipment blob
 * 
 * @author Surbhi.Gupta
 *
 */
public class OpenShipmentData {

    private OpenShipmentDetail openShipment;

    public OpenShipmentData() {}
    
    public OpenShipmentData(OpenShipmentDetail openShipment) {}

    /**
     * @return the openShipment
     */
    public OpenShipmentDetail getOpenShipment() {

        return openShipment;
    }

    /**
     * @param openShipment the openShipment to set
     */
    public void setOpenShipment(
            OpenShipmentDetail openShipment) {

        this.openShipment = openShipment;
    }

    @Override
    public String toString() {

        return "OpenShipmentData [openShipment=" + openShipment + "]";
    }

}
