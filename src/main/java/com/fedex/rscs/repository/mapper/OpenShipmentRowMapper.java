package com.fedex.rscs.repository.mapper;

import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.jdbc.core.RowMapper;

import com.fedex.rscs.model.OpenShipment;

/**
 * This class provides the custom JDBC template RowMapper implementation of type OpenShipment
 * 
 * @author Dhruv.Sood
 *
 */
public class OpenShipmentRowMapper implements RowMapper<OpenShipment> {

    @Override
    public OpenShipment mapRow(
            ResultSet rs,
            int rowNum)
            throws SQLException {

        OpenShipment openShipment = new OpenShipment();
        openShipment.setOpenShipmentId(rs.getString("OPEN_SHPMT_ID"));
        openShipment.setTrackingNumber(rs.getString("SHPMT_TRKNG_NBR"));
        openShipment.setPackageAcceptTmStmp(rs.getString("PKG_ACCPT_TMSTP"));
        openShipment.setInitialAcceptanceTmStmp(rs.getString("INITL_ACCPT_TMSTP"));
        openShipment.setFedexLocationId(rs.getString("FEDEX_LOC_ID"));
        openShipment.setOpCoLocationId(rs.getString("OPCO_LOC_ID"));
        openShipment.setOpCoCode(rs.getString("OPCO_CD"));
        openShipment.setMeterNumber(rs.getString("METER_NBR"));
        openShipment.setCityCenterAccountNumber(rs.getString("CITY_CNTR_ACCT_NBR"));
        openShipment.setFedexId(rs.getString("FEDEX_ID_NBR"));
        Blob shipmentDataBlob = rs.getBlob("SHPMT_BLOB_TXT");
        openShipment.setShipmentData(ObjectUtils.isEmpty(shipmentDataBlob) ? null
                : new String(shipmentDataBlob.getBytes(1, (int) shipmentDataBlob.length())));
        openShipment.setSoftwareId(rs.getString("SOFTWARE_ID"));
        openShipment.setDeviceId(rs.getString("DEVICE_ID_NBR"));
        openShipment.setOpenShipmentStatusCode(rs.getString("CUR_OPEN_SHPMT_STAT_CD"));
        openShipment.setOpenShipmentStatusTmStmp(rs.getString("CUR_OPEN_SHPMT_STAT_TMSTP"));
        return openShipment;
    }

}
