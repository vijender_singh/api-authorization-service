package com.fedex.rscs.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import com.fedex.rscs.dto.CarrierCodeType;
import com.fedex.rscs.dto.CarrierType;
import com.fedex.rscs.dto.PaymentType;
import com.fedex.rscs.model.AddressDetail;
import com.fedex.rscs.model.CarrierDetail;
import com.fedex.rscs.model.ContactDetail;
import com.fedex.rscs.model.OperatingCompany;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.SoldShipmentDetail;
import com.fedex.rscs.model.cshp.Price;

/**
 * This class provides the custom JDBC template RowMapper implementation of type SoldShipmentDetail
 * 
 * @author Shakti.Saurabh
 *
 */
public class SoldShipmentRowMapper implements RowMapper<SoldShipmentDetail> {

    private String locationId;
    
    private static final String PAYMENT_TYPE_CREDITCARD = "CREDITCARD";

    public SoldShipmentRowMapper(String locationId) {

        this.locationId = locationId;
    }

    @Override
    public SoldShipmentDetail mapRow(
            ResultSet rs,
            int rowNum)
            throws SQLException {

        SoldShipmentDetail soldShipmentDetail = new SoldShipmentDetail();
        soldShipmentDetail.setPackagingType(rs.getString("FEDEX_PKG_TYPE_CD"));
        soldShipmentDetail.setPackageCount(rs.getString("TOTAL_PKG_QTY"));
        soldShipmentDetail.setTrackingNumber(rs.getString("SHPMT_TRKNG_NBR"));
        soldShipmentDetail.setTotalAmount(new Price(rs.getString("CURRENCY_CD"), rs.getDouble("NET_CHARGE_AMT")));
        soldShipmentDetail.setServiceType(rs.getString("SHPNG_SVC_DESC"));
        soldShipmentDetail.setStatus(rs.getString("CUR_SHPMT_STATUS_CD"));
        soldShipmentDetail.setGuid(rs.getString("TRAN_SHPMT_REF_ID"));
        CarrierDetail carrierDetail = new CarrierDetail();
        String carrierCode = rs.getString("SHPNG_SVC_OPCO_NM");
        if (!StringUtils.isEmpty(carrierCode)) {
            carrierDetail.setOpco(carrierCode.equalsIgnoreCase(OperatingCompany.FEDEX_EXPRESS.name())
                    ? CarrierType.EXPRESS.name() : CarrierType.GROUND.name());
            carrierDetail.setCarrierCode(carrierCode.equalsIgnoreCase(OperatingCompany.FEDEX_EXPRESS.name())
                    ? CarrierCodeType.FDXE.name() : CarrierCodeType.FDXG.name());
        }
        soldShipmentDetail.setCarrierDetails(carrierDetail);
        PartyDetail shipper = new PartyDetail();
        ContactDetail contactDetail = new ContactDetail();
        contactDetail.setFullName(rs.getString("SNDR_NM"));
        contactDetail.setCompanyName(rs.getString("SNDR_COMPANY_NM"));
        shipper.setContact(contactDetail);
        AddressDetail addressDetail = new AddressDetail();
        addressDetail.setCity(rs.getString("SNDR_CITY_NM"));
        addressDetail.setPostalCode(rs.getString("SNDR_POSTAL_CD"));
        addressDetail
                .setResidential(rs.getString("SNDR_SAME_AS_CUST_FLG") != null && rs.getString("SNDR_SAME_AS_CUST_FLG")
                        .equalsIgnoreCase("Y"));
        addressDetail.setStateOrProvinceCode(rs.getString("SNDR_STATE_PROV_CD"));
        List<String> streetLines = new ArrayList<>(3);
        streetLines.add(rs.getString("SNDR_ADDR_1_TXT"));
        streetLines.add(rs.getString("SNDR_ADDR_2_TXT"));
        streetLines.add(rs.getString("SNDR_ADDR_3_TXT"));
        addressDetail.setStreetLines(streetLines);
        shipper.setAddress(addressDetail);
        soldShipmentDetail.setShipper(shipper);
        PartyDetail customer = new PartyDetail();
        ContactDetail customerContactDetail = new ContactDetail();
        customerContactDetail.setFullName(rs.getString("CUST_NM"));
        customerContactDetail.setCompanyName(rs.getString("CUST_COMPANY_NM"));
        customerContactDetail.setPhoneNumber(rs.getString("CUST_PHONE_1_NBR"));
        customerContactDetail.setPhoneExtension(rs.getString("CUST_PHONE_EXT_1_NBR"));
        customer.setContact(customerContactDetail);
        AddressDetail custAddressDetail = new AddressDetail();
        custAddressDetail.setCity(rs.getString("CUST_CITY_NM"));
        custAddressDetail.setPostalCode(rs.getString("CUST_POSTAL_CD"));
        custAddressDetail.setStateOrProvinceCode(rs.getString("CUST_STATE_PROV_CD"));
        List<String> custStreetLines = new ArrayList<>(3);
        custStreetLines.add(rs.getString("CUST_ADDR_1_TXT"));
        custStreetLines.add(rs.getString("CUST_ADDR_2_TXT"));
        custStreetLines.add(rs.getString("CUST_ADDR_3_TXT"));
        custAddressDetail.setStreetLines(custStreetLines);
        customer.setAddress(custAddressDetail);
        soldShipmentDetail.setCustomer(customer);
        PartyDetail recipient = new PartyDetail();
        ContactDetail recipientContactDetail = new ContactDetail();
        recipientContactDetail.setFullName(rs.getString("RECP_NM"));
        recipientContactDetail.setCompanyName(rs.getString("RECP_COMPANY_NM"));
        recipient.setContact(recipientContactDetail);
        AddressDetail resAddressDetail = new AddressDetail();
        resAddressDetail.setCity(rs.getString("RECP_CITY_NM"));
        resAddressDetail.setPostalCode(rs.getString("RECP_POSTAL_CD"));
        resAddressDetail.setStateOrProvinceCode(rs.getString("RECP_STATE_PROV_CD"));
        List<String> resStreetLines = new ArrayList<>(3);
        resStreetLines.add(rs.getString("RECP_ADDR_1_TXT"));
        resStreetLines.add(rs.getString("RECP_ADDR_2_TXT"));
        resStreetLines.add(rs.getString("RECP_ADDR_3_TXT"));
        resAddressDetail.setStreetLines(resStreetLines);
        recipient.setAddress(resAddressDetail);
        soldShipmentDetail.setRecipient(recipient);
        String paymentType = rs.getString("PAYMENT_TYPE_CD");
        if (PAYMENT_TYPE_CREDITCARD.equalsIgnoreCase(paymentType)) {
            paymentType = PaymentType.CREDIT_CARD.toString();
        }
        soldShipmentDetail.setPaymentType(paymentType);
        soldShipmentDetail.setTransactionDateTime(rs.getTimestamp("INITL_ACCPT_TMSTP").toInstant().toString());
        soldShipmentDetail.setRetailTransactionId(rs.getString("POS_TRAN_REF_ID"));
        String location = rs.getString("FEDEX_LOC_ID");
        soldShipmentDetail.setLocation(location);
        soldShipmentDetail.setValidLocation(!StringUtils.isEmpty(location) && location.equalsIgnoreCase(locationId));
        return soldShipmentDetail;

    }
}
