package com.fedex.rscs.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.fedex.rscs.model.ShipmentPackageDetail;

/**
 * This class provides the custom JDBC template RowMapper implementation of type
 * ShipmentPackageDetail
 * 
 * @author Mohit.kumar
 *
 */
public class ShipmentPackageRowMapper implements RowMapper<ShipmentPackageDetail> {

    @Override
    public ShipmentPackageDetail mapRow(
            ResultSet rs,
            int rowNum)
            throws SQLException {

        ShipmentPackageDetail shipmentPackageDetail = new ShipmentPackageDetail();
        shipmentPackageDetail.setAcceptTmStmp(rs.getString("PKG_ACCPT_TMSTP"));
        shipmentPackageDetail.setAmount(rs.getDouble("PKG_DECVAL_AMT"));
        shipmentPackageDetail.setBarcode(rs.getString("PKG_BARCD_CD"));
        shipmentPackageDetail.setDryIceWeight(rs.getDouble("DRY_ICE_WGT"));
        shipmentPackageDetail.setFormId(rs.getString("FORM_ID"));
        shipmentPackageDetail.setHeightQuantity(rs.getLong("PKG_HGT_QTY"));
        shipmentPackageDetail.setLengthQuantity(rs.getLong("PKG_LTH_QTY"));
        shipmentPackageDetail.setManualWeightFlag((rs.getString("MANL_WGT_FLG")
                .equals("N")) ? false : true);
        shipmentPackageDetail.setMasterFlag((rs.getString("MPS_MSTR_PKG_FLG")
                .equals("N")) ? false : true);
        shipmentPackageDetail.setPaaDamageStatText(rs.getString("PAA_DAMAGE_STAT_TXT"));
        shipmentPackageDetail.setPaaInspectionStatText(rs.getString("PAA_INSPT_STAT_TXT"));
        shipmentPackageDetail.setPaaPackByText(rs.getString("PAA_PACK_BY_TXT"));
        shipmentPackageDetail.setSeqNumber(rs.getInt("PKG_SEQ_NBR"));
        shipmentPackageDetail.setShipmentTrackingNumber(rs.getString("SHPMT_TRKNG_NBR"));
        shipmentPackageDetail.setTrackingNumber(rs.getString("PKG_TRKNG_NBR"));
        shipmentPackageDetail.setWeight(rs.getDouble("PKG_WGT"));
        shipmentPackageDetail.setWidthQuantity(rs.getLong("PKG_WIDTH_QTY"));
        return shipmentPackageDetail;
    }

}
