package com.fedex.rscs.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.fedex.rscs.dto.CarrierCodeType;
import com.fedex.rscs.dto.CarrierType;
import com.fedex.rscs.model.CarrierDetail;
import com.fedex.rscs.model.OperatingCompany;
import com.fedex.rscs.model.PackageLineItemsDetail;
import com.fedex.rscs.model.SoldShipmentSummary;
import com.fedex.rscs.model.cshp.TrackingSequenceDetail;

/**
 * This class provides the custom JDBC template ResultSetExtractor implementation of type
 * SoldShipmentSummary
 * 
 * @author 3798910
 *
 */
public class SoldShipmentSummaryRowMapper implements ResultSetExtractor {
    
    private static final String SHPMT_TRKNG_NBR = "SHPMT_TRKNG_NBR";
    
    private static final String MPS_MSTR_PKG_FLG = "MPS_MSTR_PKG_FLG";

    /**
     * Method to extract data for SoldShipmentSummary to represent one to many relationship for
     * SoldShipmentSummary with PackageLineItem
     */
    @Override
    public List<SoldShipmentSummary> extractData(
            ResultSet rs)
            throws SQLException {

        Map<String, SoldShipmentSummary> map = new HashMap<>();

        SoldShipmentSummary soldShipmentSummary = null;

        while (rs.next()) {
            String id = rs.getString(SHPMT_TRKNG_NBR);
            soldShipmentSummary = map.get(id);
            // To check whether parent object i.e. soldShipmentSummary is already created or not if
            // not then it will be populated from result set(as it is one to many relationship)
            if (soldShipmentSummary == null) {
                soldShipmentSummary = new SoldShipmentSummary();
                soldShipmentSummary.setTrackingNumber(rs.getString(SHPMT_TRKNG_NBR));
                soldShipmentSummary.setServiceType(rs.getString("SHPNG_SVC_DESC"));
                soldShipmentSummary.setStatus(rs.getString("CUR_SHPMT_STATUS_CD"));
                soldShipmentSummary.setGuid(rs.getString("TRAN_SHPMT_REF_ID"));
                CarrierDetail carrierDetail = new CarrierDetail();
                String carrierCode = rs.getString("SHPNG_SVC_OPCO_NM");
                if (!StringUtils.isEmpty(carrierCode)) {
                    carrierDetail.setOpco(carrierCode.equalsIgnoreCase(OperatingCompany.FEDEX_EXPRESS.name())
                            ? CarrierType.EXPRESS.name() : CarrierType.GROUND.name());
                    carrierDetail.setCarrierCode(carrierCode.equalsIgnoreCase(OperatingCompany.FEDEX_EXPRESS.name())
                            ? CarrierCodeType.FDXE.name() : CarrierCodeType.FDXG.name());
                }
                soldShipmentSummary.setCarrierDetails(carrierDetail);
                soldShipmentSummary.setTransactionDateTime(rs.getTimestamp("INITL_ACCPT_TMSTP").toInstant().toString());
                soldShipmentSummary.setRetailTransactionId(rs.getString("POS_TRAN_REF_ID"));
                PackageLineItemsDetail packageLineItemDetail = new PackageLineItemsDetail();
                packageLineItemDetail
                        .setMaster(rs.getString(MPS_MSTR_PKG_FLG) != null && rs.getString(MPS_MSTR_PKG_FLG)
                                .equalsIgnoreCase("Y"));
                TrackingSequenceDetail trackingSequenceDetail = new TrackingSequenceDetail();
                trackingSequenceDetail.setTrackingNumber(rs.getString(SHPMT_TRKNG_NBR));
                trackingSequenceDetail.setSequence(rs.getInt("PKG_SEQ_NBR"));
                trackingSequenceDetail.setBarcode(rs.getString("PKG_BARCD_CD"));
                packageLineItemDetail.setTrackingId(trackingSequenceDetail);
                List<PackageLineItemsDetail> packageLineItems = new ArrayList<>();
                packageLineItems.add(packageLineItemDetail);
                soldShipmentSummary.setPackageLineItems(packageLineItems);
                soldShipmentSummary.setMasterTrackingId(trackingSequenceDetail);
                map.put(id, soldShipmentSummary);
            } else {
                // In this scenario parent object is already created here the many part of
                // one-to-many i.e. adding package line object to soldShipmentSummary
                PackageLineItemsDetail packageLineItemDetail = new PackageLineItemsDetail();
                packageLineItemDetail
                        .setMaster(rs.getString(MPS_MSTR_PKG_FLG) != null && rs.getString(MPS_MSTR_PKG_FLG)
                                .equalsIgnoreCase("Y"));
                TrackingSequenceDetail trackingSequenceDetail = new TrackingSequenceDetail();
                trackingSequenceDetail.setTrackingNumber(rs.getString(SHPMT_TRKNG_NBR));
                trackingSequenceDetail.setSequence(rs.getInt("PKG_SEQ_NBR"));
                trackingSequenceDetail.setBarcode(rs.getString("PKG_BARCD_CD"));
                packageLineItemDetail.setTrackingId(trackingSequenceDetail);
                soldShipmentSummary.getPackageLineItems()
                        .add(packageLineItemDetail);
            }

        }

        // The values is in map represents soldShipmentSummaries list
        return new ArrayList<>(map.values());

    }
}
