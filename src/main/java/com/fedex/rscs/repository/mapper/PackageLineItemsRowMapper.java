package com.fedex.rscs.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.fedex.rscs.model.PackageLineItemsDetail;
import com.fedex.rscs.model.cshp.TrackingSequenceDetail;

/**
 * This class provides the custom JDBC template RowMapper implementation of type
 * PackageLineItemsDetail
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackageLineItemsRowMapper implements RowMapper<PackageLineItemsDetail> {

    @Override
    public PackageLineItemsDetail mapRow(
            ResultSet rs,
            int rowNum)
            throws SQLException {

        PackageLineItemsDetail packageLineItemsDetail = new PackageLineItemsDetail();
        packageLineItemsDetail.setMaster(rs.getString("MPS_MSTR_PKG_FLG") != null && rs.getString("MPS_MSTR_PKG_FLG")
                .equalsIgnoreCase("Y"));
        TrackingSequenceDetail trackingSequenceDetail = new TrackingSequenceDetail();
        trackingSequenceDetail.setTrackingNumber(rs.getString("SHPMT_TRKNG_NBR"));
        trackingSequenceDetail.setSequence(rs.getInt("PKG_SEQ_NBR"));
        trackingSequenceDetail.setBarcode(rs.getString("PKG_BARCD_CD"));
        packageLineItemsDetail.setTrackingId(trackingSequenceDetail);
        return packageLineItemsDetail;
    }

}
