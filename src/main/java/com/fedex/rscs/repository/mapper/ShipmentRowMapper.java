package com.fedex.rscs.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.fedex.rscs.model.ShipmentDetail;

/**
 * This class provides the custom JDBC template RowMapper implementation of type ShipmentDetail
 * 
 * @author Mohit.kumar
 *
 */
public class ShipmentRowMapper implements RowMapper<ShipmentDetail> {

    @Override
    public ShipmentDetail mapRow(
            ResultSet rs,
            int rowNum)
            throws SQLException {

        ShipmentDetail shipmentDetail = new ShipmentDetail();
        shipmentDetail.setAcceptTmStmp(rs.getString("PKG_ACCPT_TMSTP"));
        shipmentDetail.setAfterPickupFlag((rs.getString("AFTER_PICKUP_FLG")
                .equals("N")) ? false : true);
        shipmentDetail.setApiClientId(rs.getString("API_CLIENT_ID"));
        shipmentDetail.setApiClientTypeName(rs.getString("API_CLIENT_TYPE_NM"));
        shipmentDetail.setApiVersionId(rs.getString("API_VERS_ID"));
        shipmentDetail.setBillToAccountType(rs.getString("BILL_TO_ACCT_TYPE_CD"));
        shipmentDetail.setChargeAmount(rs.getDouble("CHARGE_AMT"));
        shipmentDetail.setCityCenterAccountNumber(rs.getString("CITY_CNTR_ACCT_NBR"));
        shipmentDetail.setCurrency(rs.getString("CURRENCY_CD"));
        shipmentDetail.setCurrentShipmentStatTmStmp(rs.getString("CUR_SHPMT_STAT_TMSTP"));
        shipmentDetail.setCurrentShipmentStatus(rs.getString("CUR_SHPMT_STATUS_CD"));
        shipmentDetail.setCustomerAccountNumber(rs.getString("CUST_ACCT_NBR"));
        shipmentDetail.setDelCommitDate(rs.getString("DEL_COMMIT_DT"));
        shipmentDetail.setDeviceId(rs.getString("DEVICE_ID_NBR"));
        shipmentDetail.setDevicePlatformType(rs.getString("DVC_PLATFORM_TYPE_CD"));
        shipmentDetail.setDimensionUom(rs.getString("DIM_UOM_CD"));
        shipmentDetail.setDiscountAmount(rs.getDouble("DISCOUNT_AMT"));
        shipmentDetail.setFedexId(rs.getString("FEDEX_ID_NBR"));
        shipmentDetail.setFedexLocationId(rs.getString("FEDEX_LOC_ID"));
        shipmentDetail.setFedexPackageType(rs.getString("FEDEX_PKG_TYPE_CD"));
        shipmentDetail.setGroundAccountNumber(rs.getString("FXG_ACCT_NBR"));
        shipmentDetail.setHalFedexLocationId(rs.getString("HAL_FEDEX_LOC_ID"));
        shipmentDetail.setInitialAcceptanceTmStmp(rs.getString("INITL_ACCPT_TMSTP"));
        shipmentDetail.setMeterNumber(rs.getString("METER_NBR"));
        shipmentDetail.setNetChargeAmount(rs.getDouble("NET_CHARGE_AMT"));
        shipmentDetail.setNonFedexPackageType(rs.getString("NON_FEDEX_PKG_TYPE_CD"));
        shipmentDetail.setOpCoCode(rs.getString("OPCO_CD"));
        shipmentDetail.setOpCoLocationId(rs.getString("OPCO_LOC_ID"));
        shipmentDetail.setOriginCountryCode(rs.getString("ORIG_CTRY_CD"));
        shipmentDetail.setPaymentType(rs.getString("PAYMENT_TYPE_CD"));
        shipmentDetail.setPosTransactionRefId(rs.getString("POS_TRAN_REF_ID"));
        shipmentDetail.setRatingType(rs.getString("RATING_TYPE_CD"));
        shipmentDetail.setShipmentTrackingNumber(rs.getString("SHPMT_TRKNG_NBR"));
        shipmentDetail.setShippingServiceName(rs.getString("SHPNG_SVC_NM"));
        shipmentDetail.setSoftwareId(rs.getString("SOFTWARE_ID"));
        shipmentDetail.setStandardPackageFlag((rs.getString("STANDARD_PKG_FLG")
                .equals("N")) ? false : true);
        shipmentDetail.setSurchargeAmount(rs.getDouble("SURCHARGE_AMT"));
        shipmentDetail.setTaxAmount(rs.getDouble("TAX_AMT"));
        shipmentDetail.setTotalPackageQuantity(rs.getInt("TOTAL_PKG_QTY"));
        shipmentDetail.setTransactionShipmentRefId(rs.getString("TRAN_SHPMT_REF_ID"));
        shipmentDetail.setWeightUom(rs.getString("WGT_UOM_CD"));
        shipmentDetail.setOpcoShippingServiceName(rs.getString("SHPNG_SVC_OPCO_NM"));
        return shipmentDetail;
    }

}
