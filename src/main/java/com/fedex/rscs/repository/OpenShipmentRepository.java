package com.fedex.rscs.repository;

import com.fedex.rscs.model.OpenShipment;
import com.fedex.rscs.model.OpenShipmentData;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;

/**
 * Interface for FXO RSCS SCHEMA
 * 
 * @author Surbhi.Gupta
 *
 */
public interface OpenShipmentRepository {

    /**
     * Method to save open shipment data in OPEN_SHIPMENT table
     * 
     * @param openShipment
     * @return
     */
    boolean save(
            OpenShipment openShipment);

    /**
     * Method to delete open shipment data in OPEN_SHIPMENT table
     * 
     * @param openShipment
     * @return
     */
    boolean delete(
            OpenShipment openShipment);

    /**
     * Method to execute retrieve open shipment
     * 
     * @param referenceId
     * @param locationId
     * @param trackingId
     * @return
     */
    OpenShipment retrieve(
            String referenceId,
            String locationId,
            String trackingId);
    
    /**
     * Method to update open shipment status
     * 
     * @param updateOpenShipmentStatusData
     * @return boolean
     */
    boolean updateStatus(
            UpdateOpenShipmentStatusData updateOpenShipmentStatusData);
    
    /**
     * Method to get Open Shipment Data from BLOB
     * 
     * @param shipmentData
     * @return
     */
    public OpenShipmentData getOpenShipmentDataFromBlob(
            String shipmentData);
    
}
