package com.fedex.rscs.repository;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.util.CollectionUtils;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.ServiceProcessorTerminalException;
import com.fedex.rscs.model.InterlineShipmentDetail;
import com.fedex.rscs.model.ShipmentContactDetail;
import com.fedex.rscs.model.ShipmentDetail;
import com.fedex.rscs.model.ShipmentPackageAdditionalService;
import com.fedex.rscs.model.ShipmentPackageDetail;
import com.fedex.rscs.model.SoldShipmentDetail;
import com.fedex.rscs.model.SoldShipmentSummary;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;
import com.fedex.rscs.repository.mapper.PackageLineItemsRowMapper;
import com.fedex.rscs.repository.mapper.ShipmentPackageRowMapper;
import com.fedex.rscs.repository.mapper.ShipmentRowMapper;
import com.fedex.rscs.repository.mapper.SoldShipmentRowMapper;
import com.fedex.rscs.repository.mapper.SoldShipmentSummaryRowMapper;

/**
 * This is just an example of how to integrate with another webservice using spring If your
 * application does not connect to another service, this class can be deleted
 */
@Profile("!mock")
@Repository
public class ConfirmShipmentRepositoryImpl implements ConfirmShipmentRepository {

    private static final Logger logger = LoggerFactory.getLogger(ConfirmShipmentRepositoryImpl.class);

    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    private final PlatformTransactionManager txManager;

    @Value("${soldShipment.search.duration.days:4}")
    private int soldShpmentsSearchDurationDays;
    private static final String APP_ID = "RSCS";
    private static final String CRTE_APPLI_ID = "crteApplicationId";
    private static final String LAST_MODFY_USER_ID = "lastModifyUserId";
    private static final String LAST_MODFY_APPLI_ID = "lastModifyApplicationId";
    private static final String CRTE_USER_ID = "crteUserId";
    private static final String PKG_ACCPT_TMSTP = "packageAcceptanceTimestamp";
    private static final String SHPMT_TRKNG_NBR = "shipmentTrackingNumber";
    private static final String TRANSACTION_TIMESTAMP_FORMAT = "yyyy:MM:dd:HH:mm:ss";
    private static final String ID = "id";
    private static final String LOCATION_ID = "locationId";
    private static final String FROM_DATE = "fromDate";
    private static final String TO_DATE = "toDate";
    private static final String STATUS_CODE = "statusCode";
    private static final String STATUSCODE = "CREATED";
    private static final String GROUND_TXN_RATE_FLG = "groundTxnRateFlag";

    @Autowired
    public ConfirmShipmentRepositoryImpl(final NamedParameterJdbcTemplate namedJdbcTemplate,
            final PlatformTransactionManager txManager) {

        this.namedJdbcTemplate = namedJdbcTemplate;
        this.txManager = txManager;
    }

    /**
     * Method to save the confirm open shipment into database for provided shipment detail
     * 
     * @param shipmentDetail
     */
    @Override
    @HystrixCommand(groupKey = "ConfirmShipmentRepository", commandKey = "saveConfirmedShipment")
    public boolean save(
            ShipmentDetail shipmentDetail) {

        MapSqlParameterSource parameters = createSqlParametersForConfirmShipment(shipmentDetail);

        // insert shipment query for confirm open shipment transactions.
        final String insertConfirmShipmentQuery =
                "INSERT INTO FXORSCS_SCHEMA.SHIPMENT ( SHPMT_TRKNG_NBR, PKG_ACCPT_TMSTP, INITL_ACCPT_TMSTP, FEDEX_ID_NBR, FEDEX_LOC_ID, OPCO_LOC_ID, OPCO_CD, "
                        + "DEVICE_ID_NBR, DVC_PLATFORM_TYPE_CD, API_CLIENT_TYPE_NM, API_VERS_ID, API_CLIENT_ID, ORIG_CTRY_CD, AFTER_PICKUP_FLG, FXG_ACCT_NBR, "
                        + "CITY_CNTR_ACCT_NBR, METER_NBR, SOFTWARE_ID, PAYMENT_TYPE_CD, CUST_ACCT_NBR, BILL_TO_ACCT_TYPE_CD, FEDEX_PKG_TYPE_CD, NON_FEDEX_PKG_TYPE_CD, STANDARD_PKG_FLG,"
                        + "TOTAL_PKG_QTY, CURRENCY_CD, WGT_UOM_CD, DIM_UOM_CD, RATING_TYPE_CD, SHPNG_SVC_NM, HAL_FEDEX_LOC_ID, CHARGE_AMT, DISCOUNT_AMT, SURCHARGE_AMT,"
                        + "TAX_AMT, NET_CHARGE_AMT, TRAN_SHPMT_REF_ID, POS_TRAN_REF_ID, DEL_COMMIT_DT, CUR_SHPMT_STATUS_CD, CUR_SHPMT_STAT_TMSTP, CRTE_APPLI_ID,"
                        + "CRTE_TMSTP, CRTE_USER_ID, LAST_MODFY_APPLI_ID, LAST_MODFY_TMSTP, LAST_MODFY_USER_ID, SHPNG_SVC_OPCO_NM,SHPNG_SVC_DESC, GRND_RTNG_SYS_NTFY_FLG )"
                        + "VALUES (:shipmentTrackingNumber, TO_DATE(:packageAcceptanceTimestamp,'YYYY:MM:dd:HH24:MI:SS'), :INITL_ACCPT_TMSTP, :FEDEX_ID_NBR, :FEDEX_LOC_ID, :OPCO_LOC_ID, :OPCO_CD, "
                        + ":DEVICE_ID_NBR, :DVC_PLATFORM_TYPE_CD, :API_CLIENT_TYPE_NM, :API_VERS_ID, :API_CLIENT_ID, :ORIG_CTRY_CD, :AFTER_PICKUP_FLG, :FXG_ACCT_NBR, "
                        + ":CITY_CNTR_ACCT_NBR, :METER_NBR, :SOFTWARE_ID, :PAYMENT_TYPE_CD, :CUST_ACCT_NBR, :BILL_TO_ACCT_TYPE_CD, :FEDEX_PKG_TYPE_CD, :NON_FEDEX_PKG_TYPE_CD, :STANDARD_PKG_FLG, "
                        + ":TOTAL_PKG_QTY, :CURRENCY_CD, :WGT_UOM_CD, :DIM_UOM_CD, :RATING_TYPE_CD, :SHPNG_SVC_NM, :HAL_FEDEX_LOC_ID, :CHARGE_AMT, :DISCOUNT_AMT, :SURCHARGE_AMT, "
                        + ":TAX_AMT, :NET_CHARGE_AMT, :TRAN_SHPMT_REF_ID, :POS_TRAN_REF_ID, TO_DATE(:DEL_COMMIT_DT,'YYYY:MM:dd:HH24:MI:SS'), :CUR_SHPMT_STATUS_CD, :CUR_SHPMT_STAT_TMSTP, :crteApplicationId, "
                        + "SYSDATE, :crteUserId, :lastModifyApplicationId, SYSDATE, :lastModifyUserId, :SHPNG_SVC_OPCO_NM, :SHPNG_SVC_DESC, :GRND_RTNG_SYS_NTFY_FLG )";

        TransactionDefinition txDefinition = new DefaultTransactionDefinition();
        TransactionStatus txStatus = txManager.getTransaction(txDefinition);

        try {
            logger.debug("Executing query to insert shipment information: {} with params : [ {} ]",
                    insertConfirmShipmentQuery, parameters);
            namedJdbcTemplate.update(insertConfirmShipmentQuery, parameters);
            saveShipmentContact(shipmentDetail.getFedexId(), shipmentDetail.getShipmentContactDetail());
            saveShipmentPackageDetails(shipmentDetail.getFedexId(), shipmentDetail.getShipmentPackageDetails());
            if (shipmentDetail.getInterlineShipmentDetail() != null) {
                saveInterlineShipment(shipmentDetail.getFedexId(), shipmentDetail.getInterlineShipmentDetail());
            }
            txManager.commit(txStatus);
            return true;
        } catch (Exception ex) {
            txManager.rollback(txStatus);
            logger.error("Failed to save transaction into database for given shipment details", ex);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.DB_INSERTION_FAILURE);
        }
    }

    @Override
    public boolean delete(
            String shipmentTrackingNumber) {
        
        return false;
    }

    /**
     * Method to save the confirm open shipment contact details into database
     * 
     * @param shipmentContactDetail
     */
    private void saveShipmentContact(
            String fedexId,
            ShipmentContactDetail shipmentContactDetail) {

        MapSqlParameterSource parameters =
                createSqlParametersForConfirmShipmentContactDetails(fedexId, shipmentContactDetail);

        // insert shipment contact details query for confirm open shipment
        // transactions.
        String insertShipmentContactDetailQuery =
                "INSERT INTO FXORSCS_SCHEMA.SHIPMENT_CONTACT ( SHPMT_TRKNG_NBR, PKG_ACCPT_TMSTP, RECP_NM, RECP_CITY_NM, RECP_STATE_PROV_CD, RECP_POSTAL_CD, RECP_COUNTRY_NM, "
                        + "RECP_COMPANY_NM, RECP_ADDR_1_TXT, RECP_ADDR_2_TXT, RECP_ADDR_3_TXT, RSDNL_RECP_ADDR_FLG, SNDR_NM, SNDR_CITY_NM, SNDR_STATE_PROV_CD, SNDR_POSTAL_CD, "
                        + "SNDR_COUNTRY_NM, SNDR_COMPANY_NM, SNDR_ADDR_1_TXT, SNDR_ADDR_2_TXT, SNDR_ADDR_3_TXT, SNDR_SAME_AS_CUST_FLG, CUST_NM, CUST_ADDR_1_TXT, CUST_ADDR_2_TXT, "
                        + "CUST_ADDR_3_TXT, CUST_CITY_NM, CUST_STATE_PROV_CD, CUST_POSTAL_CD, CUST_COUNTRY_NM, CUST_COMPANY_NM, CUST_PHONE_1_NBR, CUST_PHONE_EXT_1_NBR, "
                        + "CRTE_APPLI_ID, CRTE_TMSTP, CRTE_USER_ID, LAST_MODFY_APPLI_ID, LAST_MODFY_TMSTP, LAST_MODFY_USER_ID)"
                        + "VALUES (:shipmentTrackingNumber, TO_DATE(:packageAcceptanceTimestamp, 'YYYY:MM:dd:HH24:MI:SS'), :RECP_NM, :RECP_CITY_NM, :RECP_STATE_PROV_CD, :RECP_POSTAL_CD, :RECP_COUNTRY_NM, "
                        + ":RECP_COMPANY_NM, :RECP_ADDR_1_TXT, :RECP_ADDR_2_TXT, :RECP_ADDR_3_TXT, :RSDNL_RECP_ADDR_FLG, :SNDR_NM, :SNDR_CITY_NM, :SNDR_STATE_PROV_CD, :SNDR_POSTAL_CD, "
                        + ":SNDR_COUNTRY_NM, :SNDR_COMPANY_NM, :SNDR_ADDR_1_TXT, :SNDR_ADDR_2_TXT, :SNDR_ADDR_3_TXT, :SNDR_SAME_AS_CUST_FLG, :CUST_NM, :CUST_ADDR_1_TXT, :CUST_ADDR_2_TXT, "
                        + ":CUST_ADDR_3_TXT, :CUST_CITY_NM, :CUST_STATE_PROV_CD, :CUST_POSTAL_CD, :CUST_COUNTRY_NM, :CUST_COMPANY_NM, :CUST_PHONE_1_NBR, :CUST_PHONE_EXT_1_NBR, "
                        + ":crteApplicationId, SYSDATE, :crteUserId, :lastModifyApplicationId, SYSDATE, :lastModifyUserId)";

        logger.debug("Executing query to insert shipment contact information: {} with params : [ {} ]",
                insertShipmentContactDetailQuery, parameters);

        namedJdbcTemplate.update(insertShipmentContactDetailQuery, parameters);
    }

    /**
     * Method to save the confirm open shipment package details into database
     * 
     * @param fedexId
     * @param shipmentPackageDetail
     */
    private void saveShipmentPackageDetails(
            String fedexId,
            List<ShipmentPackageDetail> shipmentPackageDetail) {

        // insert shipment package details query for confirm open shipment
        // transactions.
        String insertShipmentPkgDetailQuery =
                "INSERT INTO FXORSCS_SCHEMA.SHIPMENT_PACKAGE ( SHPMT_TRKNG_NBR, PKG_ACCPT_TMSTP, PKG_TRKNG_NBR, PKG_BARCD_CD, FORM_ID, "
                        + "MPS_MSTR_PKG_FLG, PKG_SEQ_NBR, PKG_LTH_QTY,PKG_HGT_QTY, PKG_WIDTH_QTY, PKG_DECVAL_AMT, PKG_WGT,"
                        + " MANL_WGT_FLG, PAA_DAMAGE_STAT_TXT, PAA_PACK_BY_TXT, PAA_INSPT_STAT_TXT, DRY_ICE_WGT, CRTE_APPLI_ID, CRTE_TMSTP, CRTE_USER_ID, "
                        + " LAST_MODFY_APPLI_ID, LAST_MODFY_TMSTP, LAST_MODFY_USER_ID)"
                        + "VALUES (:shipmentTrackingNumber, TO_DATE(:packageAcceptanceTimestamp, 'YYYY:MM:dd:HH24:MI:SS'), :PKG_TRKNG_NBR, :PKG_BARCD_CD, :FORM_ID, :MPS_MSTR_PKG_FLG, :PKG_SEQ_NBR, "
                        + ":PKG_LTH_QTY, :PKG_HGT_QTY, :PKG_WIDTH_QTY, :PKG_DECVAL_AMT, :PKG_WGT, :MANL_WGT_FLG, :PAA_DAMAGE_STAT_TXT, "
                        + ":PAA_PACK_BY_TXT, :PAA_INSPT_STAT_TXT, :DRY_ICE_WGT, :crteApplicationId, SYSDATE, :crteUserId, :lastModifyApplicationId, SYSDATE, :lastModifyUserId)";

        List<SqlParameterSource> batchParameters = new ArrayList<>();

        for (ShipmentPackageDetail packageDetail : shipmentPackageDetail) {

            batchParameters.add(createSqlParametersForConfirmShipmentPackageDetails(fedexId, packageDetail));
        }

        logger.debug("Executing query to insert shipment package detail information: {} with params : [ {} ]",
                insertShipmentPkgDetailQuery, batchParameters);

        namedJdbcTemplate.batchUpdate(insertShipmentPkgDetailQuery, batchParameters.toArray(new SqlParameterSource[0]));

        for (ShipmentPackageDetail packageDetail : shipmentPackageDetail) {

            saveShipmentPackageAdditionalServices(fedexId, packageDetail);
        }
    }

    /**
     * Method to save the confirm open shipment package additional services data into database
     * 
     * @param fedexId
     * @param shipmentPkgDetail
     */
    private void saveShipmentPackageAdditionalServices(
            String fedexId,
            ShipmentPackageDetail shipmentPkgDetail) {

        // insert shipment package additional details query for confirm open
        // shipment transactions.
        String insertShipmentPkgAddtlServiceQuery =
                "INSERT INTO FXORSCS_SCHEMA.SHIPMENT_PACKAGE_ADDL_SVC ( SHPMT_TRKNG_NBR, PKG_ACCPT_TMSTP, PKG_TRKNG_NBR, SPCL_SVC_CD, SPCL_SVC_DESC, CRTE_APPLI_ID,"
                        + "CRTE_TMSTP, CRTE_USER_ID, LAST_MODFY_APPLI_ID, LAST_MODFY_TMSTP, LAST_MODFY_USER_ID)"
                        + "VALUES (:shipmentTrackingNumber, TO_DATE(:packageAcceptanceTimestamp, 'YYYY:MM:dd:HH24:MI:SS'), :PKG_TRKNG_NBR, :SPCL_SVC_CD, :SPCL_SVC_DESC, :crteApplicationId, "
                        + "SYSDATE, :crteUserId, :lastModifyApplicationId, SYSDATE, :lastModifyUserId)";

        List<SqlParameterSource> batchParameters = new ArrayList<>();

        for (ShipmentPackageAdditionalService shipmentPkgAddService : shipmentPkgDetail
                .getShipmentPackageAdditionalServices()) {
            batchParameters.add(
                    createSqlParametersForConfirmShipmentPackageAdditionalServices(fedexId, shipmentPkgAddService));
        }

        logger.debug(
                "Executing query to insert shipment  package additional service information: {} with params : [ {} ]",
                insertShipmentPkgAddtlServiceQuery, batchParameters);

        namedJdbcTemplate.batchUpdate(insertShipmentPkgAddtlServiceQuery,
                batchParameters.toArray(new SqlParameterSource[0]));
    }
    
    /**
     * Method to save the interline details into database
     * 
     * @param interlineShipmentDetail
     */
    private void saveInterlineShipment(
            String fedexId,
            InterlineShipmentDetail interlineShipmentDetail) {

        MapSqlParameterSource parameters =
                createSqlParametersForInterlineShipmentDetails(fedexId, interlineShipmentDetail);

        // insert interline shipment details query for confirm open interline shipment
        // transactions.
        StringBuilder insertInterlineShipmentDetailQuery = new StringBuilder("INSERT INTO FXORSCS_SCHEMA.intln_shpmt");
        insertInterlineShipmentDetailQuery.append(
                "( SHPMT_TRKNG_NBR, PKG_ACCPT_TMSTP, INTLN_ID, INTLN_EMP_NBR, SHPMT_WGT, LAST_MODFY_USER_ID, LAST_MODFY_TMSTP, LAST_MODFY_APPLI_ID,  CRTE_USER_ID, CRTE_TMSTP, CRTE_APPLI_ID)");
        insertInterlineShipmentDetailQuery.append(
                "VALUES (:shipmentTrackingNumber, TO_DATE(:packageAcceptanceTimestamp, 'YYYY:MM:dd:HH24:MI:SS'),");
        insertInterlineShipmentDetailQuery.append(
                ":INTLN_ID, :INTLN_EMP_NBR, :SHPMT_WGT, :lastModifyUserId, SYSDATE, :lastModifyApplicationId, :crteUserId, SYSDATE, :crteApplicationId)");
       
        logger.debug("Executing query to insert interline shipment information: {} with params : [ {} ]",
                insertInterlineShipmentDetailQuery, parameters);

        namedJdbcTemplate.update(insertInterlineShipmentDetailQuery.toString(), parameters);
    }

    /**
     * It will set the sql parameters to save the ConfirmOpenShipment object in DB
     * 
     * @param shipmentDetail
     * @return
     */
    private MapSqlParameterSource createSqlParametersForConfirmShipment(
            ShipmentDetail shipmentDetail) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(SHPMT_TRKNG_NBR, shipmentDetail.getShipmentTrackingNumber());
        parameters.addValue(PKG_ACCPT_TMSTP, ZonedDateTime.parse(shipmentDetail.getAcceptTmStmp())
                .format(DateTimeFormatter.ofPattern(TRANSACTION_TIMESTAMP_FORMAT)));
        parameters.addValue("INITL_ACCPT_TMSTP", ZonedDateTime.parse(shipmentDetail.getInitialAcceptanceTmStmp()));
        parameters.addValue("FEDEX_ID_NBR", shipmentDetail.getFedexId());
        parameters.addValue("FEDEX_LOC_ID", shipmentDetail.getFedexLocationId());
        parameters.addValue("OPCO_LOC_ID", shipmentDetail.getOpCoLocationId());
        parameters.addValue("OPCO_CD", shipmentDetail.getOpCoCode());
        parameters.addValue("DEVICE_ID_NBR", shipmentDetail.getDeviceId());
        parameters.addValue("DVC_PLATFORM_TYPE_CD", shipmentDetail.getDevicePlatformType());
        parameters.addValue("API_CLIENT_TYPE_NM", shipmentDetail.getApiClientTypeName());
        parameters.addValue("API_VERS_ID", shipmentDetail.getApiVersionId());
        parameters.addValue("API_CLIENT_ID", shipmentDetail.getApiClientId());
        parameters.addValue("ORIG_CTRY_CD", shipmentDetail.getOriginCountryCode());
        parameters.addValue("AFTER_PICKUP_FLG", shipmentDetail.isAfterPickupFlag() ? "Y" : "N");
        parameters.addValue("FXG_ACCT_NBR", shipmentDetail.getGroundAccountNumber());
        parameters.addValue("CITY_CNTR_ACCT_NBR", shipmentDetail.getCityCenterAccountNumber());
        parameters.addValue("METER_NBR", shipmentDetail.getMeterNumber());
        parameters.addValue("SOFTWARE_ID", shipmentDetail.getSoftwareId());
        parameters.addValue("PAYMENT_TYPE_CD", shipmentDetail.getPaymentType());
        parameters.addValue("CUST_ACCT_NBR", shipmentDetail.getCustomerAccountNumber());
        parameters.addValue("BILL_TO_ACCT_TYPE_CD", shipmentDetail.getBillToAccountType());
        parameters.addValue("FEDEX_PKG_TYPE_CD", shipmentDetail.getFedexPackageType());
        parameters.addValue("NON_FEDEX_PKG_TYPE_CD", shipmentDetail.getNonFedexPackageType());
        parameters.addValue("STANDARD_PKG_FLG", shipmentDetail.isStandardPackageFlag() ? "Y" : "N");
        parameters.addValue("TOTAL_PKG_QTY", shipmentDetail.getTotalPackageQuantity());
        parameters.addValue("CURRENCY_CD", shipmentDetail.getCurrency());
        parameters.addValue("WGT_UOM_CD", shipmentDetail.getWeightUom());
        parameters.addValue("DIM_UOM_CD", shipmentDetail.getDimensionUom());
        parameters.addValue("RATING_TYPE_CD", shipmentDetail.getRatingType());
        parameters.addValue("SHPNG_SVC_NM", shipmentDetail.getShippingServiceName());
        parameters.addValue("HAL_FEDEX_LOC_ID", shipmentDetail.getHalFedexLocationId());
        parameters.addValue("CHARGE_AMT", shipmentDetail.getChargeAmount());
        parameters.addValue("DISCOUNT_AMT", shipmentDetail.getDiscountAmount());
        parameters.addValue("TAX_AMT", shipmentDetail.getTaxAmount());
        parameters.addValue("SURCHARGE_AMT", shipmentDetail.getSurchargeAmount());
        parameters.addValue("NET_CHARGE_AMT", shipmentDetail.getNetChargeAmount());
        parameters.addValue("TRAN_SHPMT_REF_ID", shipmentDetail.getTransactionShipmentRefId());
        parameters.addValue("POS_TRAN_REF_ID", shipmentDetail.getPosTransactionRefId());
        parameters.addValue("DEL_COMMIT_DT", LocalDateTime.parse(shipmentDetail.getDelCommitDate())
                .format(DateTimeFormatter.ofPattern(TRANSACTION_TIMESTAMP_FORMAT)));

        parameters.addValue("CUR_SHPMT_STATUS_CD", shipmentDetail.getCurrentShipmentStatus());
        parameters.addValue("CUR_SHPMT_STAT_TMSTP", LocalDateTime.now());
        parameters.addValue(CRTE_APPLI_ID, APP_ID);
        parameters.addValue(CRTE_USER_ID, shipmentDetail.getFedexId());
        parameters.addValue(LAST_MODFY_APPLI_ID, APP_ID);
        parameters.addValue(LAST_MODFY_USER_ID, shipmentDetail.getFedexId());
        parameters.addValue("SHPNG_SVC_OPCO_NM", shipmentDetail.getOpcoShippingServiceName());
        parameters.addValue("SHPNG_SVC_DESC", shipmentDetail.getShippingServiceDesc());
        parameters.addValue("GRND_RTNG_SYS_NTFY_FLG", "N");
        return parameters;
    }

    /**
     * It will set the sql parameters to save the ConfirmOpenShipment contact details object in DB
     * 
     * @param shipmentContactDetail
     * @return
     */
    private MapSqlParameterSource createSqlParametersForConfirmShipmentContactDetails(
            String fedexId,
            ShipmentContactDetail shipmentContactDetail) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(SHPMT_TRKNG_NBR, shipmentContactDetail.getShipmentTrackingNumber());
        parameters.addValue(PKG_ACCPT_TMSTP, ZonedDateTime.parse(shipmentContactDetail.getAcceptTmStmp())
                .format(DateTimeFormatter.ofPattern(TRANSACTION_TIMESTAMP_FORMAT)));
        parameters.addValue("RECP_NM", shipmentContactDetail.getRecipientName());
        parameters.addValue("RECP_CITY_NM", shipmentContactDetail.getRecipientCityName());
        parameters.addValue("RECP_STATE_PROV_CD", shipmentContactDetail.getRecipientStateProvinceCode());
        parameters.addValue("RECP_POSTAL_CD", shipmentContactDetail.getRecipientPostalCode());
        parameters.addValue("RECP_COUNTRY_NM", shipmentContactDetail.getRecipientCountryName());
        parameters.addValue("RECP_COMPANY_NM", shipmentContactDetail.getRecipientCompanyName());
        parameters.addValue("RECP_ADDR_1_TXT", shipmentContactDetail.getRecipientAddress1Text());
        parameters.addValue("RECP_ADDR_2_TXT", shipmentContactDetail.getRecipientAddress2Text());
        parameters.addValue("RECP_ADDR_3_TXT", shipmentContactDetail.getRecipientAddress3Text());
        parameters.addValue("RSDNL_RECP_ADDR_FLG", shipmentContactDetail.isResidence() ? "Y" : "N");
        parameters.addValue("SNDR_NM", shipmentContactDetail.getSenderName());
        parameters.addValue("SNDR_CITY_NM", shipmentContactDetail.getSenderCityName());
        parameters.addValue("SNDR_STATE_PROV_CD", shipmentContactDetail.getSenderStateProvinceCode());
        parameters.addValue("SNDR_POSTAL_CD", shipmentContactDetail.getSenderPostalCode());
        parameters.addValue("SNDR_COUNTRY_NM", shipmentContactDetail.getSenderCountryName());
        parameters.addValue("SNDR_COMPANY_NM", shipmentContactDetail.getSenderCompanyName());
        parameters.addValue("SNDR_ADDR_1_TXT", shipmentContactDetail.getSenderAddress1Text());
        parameters.addValue("SNDR_ADDR_2_TXT", shipmentContactDetail.getSenderAddress2Text());
        parameters.addValue("SNDR_ADDR_3_TXT", shipmentContactDetail.getSenderAddress3Text());
        parameters.addValue("SNDR_SAME_AS_CUST_FLG", shipmentContactDetail.isSenderCustomer() ? "Y" : "N");
        parameters.addValue("CUST_NM", shipmentContactDetail.getCustomerName());
        parameters.addValue("CUST_ADDR_1_TXT", shipmentContactDetail.getCustomerAddress1Text());
        parameters.addValue("CUST_ADDR_2_TXT", shipmentContactDetail.getCustomerAddress2Text());
        parameters.addValue("CUST_ADDR_3_TXT", shipmentContactDetail.getCustomerAddress3Text());
        parameters.addValue("CUST_CITY_NM", shipmentContactDetail.getCustomerCityName());
        parameters.addValue("CUST_STATE_PROV_CD", shipmentContactDetail.getCustomerStateProvinceCode());
        parameters.addValue("CUST_POSTAL_CD", shipmentContactDetail.getCustomerPostalCode());
        parameters.addValue("CUST_COUNTRY_NM", shipmentContactDetail.getCustomerCountryName());
        parameters.addValue("CUST_COMPANY_NM", shipmentContactDetail.getCustomerCompanyName());
        parameters.addValue("CUST_PHONE_1_NBR", shipmentContactDetail.getCustomerPhoneNumber());
        parameters.addValue("CUST_PHONE_EXT_1_NBR", shipmentContactDetail.getCustomerPhoneExtensionNumber());
        parameters.addValue(CRTE_APPLI_ID, APP_ID);
        parameters.addValue(CRTE_USER_ID, fedexId);
        parameters.addValue(LAST_MODFY_APPLI_ID, APP_ID);
        parameters.addValue(LAST_MODFY_USER_ID, fedexId);

        return parameters;
    }

    /**
     * It will set the sql parameters to save the ConfirmOpenShipment package details object in DB
     * 
     * @param shipmentPackageDetail
     * @return
     */
    private MapSqlParameterSource createSqlParametersForConfirmShipmentPackageDetails(
            String fedexId,
            ShipmentPackageDetail shipmentPackageDetail) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(SHPMT_TRKNG_NBR, shipmentPackageDetail.getShipmentTrackingNumber());
        parameters.addValue(PKG_ACCPT_TMSTP, ZonedDateTime.parse(shipmentPackageDetail.getAcceptTmStmp())
                .format(DateTimeFormatter.ofPattern(TRANSACTION_TIMESTAMP_FORMAT)));
        parameters.addValue("PKG_TRKNG_NBR", shipmentPackageDetail.getTrackingNumber());
        parameters.addValue("PKG_BARCD_CD", shipmentPackageDetail.getBarcode());
        parameters.addValue("FORM_ID", shipmentPackageDetail.getFormId());
        parameters.addValue("MPS_MSTR_PKG_FLG", shipmentPackageDetail.isMasterFlag() ? "Y" : "N");
        parameters.addValue("PKG_SEQ_NBR", shipmentPackageDetail.getSeqNumber());
        parameters.addValue("PKG_LTH_QTY", shipmentPackageDetail.getLengthQuantity());
        parameters.addValue("PKG_HGT_QTY", shipmentPackageDetail.getHeightQuantity());
        parameters.addValue("PKG_WIDTH_QTY", shipmentPackageDetail.getWidthQuantity());
        parameters.addValue("PKG_DECVAL_AMT", shipmentPackageDetail.getAmount());
        parameters.addValue("PKG_WGT", shipmentPackageDetail.getWeight());
        parameters.addValue("MANL_WGT_FLG", shipmentPackageDetail.isManualWeightFlag() ? "Y" : "N");
        parameters.addValue("PAA_DAMAGE_STAT_TXT", shipmentPackageDetail.getPaaDamageStatText());
        parameters.addValue("PAA_PACK_BY_TXT", shipmentPackageDetail.getPaaPackByText());
        parameters.addValue("PAA_INSPT_STAT_TXT", shipmentPackageDetail.getPaaInspectionStatText());
        parameters.addValue("DRY_ICE_WGT", shipmentPackageDetail.getDryIceWeight());
        parameters.addValue(CRTE_APPLI_ID, APP_ID);
        parameters.addValue(CRTE_USER_ID, fedexId);
        parameters.addValue(LAST_MODFY_APPLI_ID, APP_ID);
        parameters.addValue(LAST_MODFY_USER_ID, fedexId);

        return parameters;
    }

    /**
     * It will set the sql parameters to save the ConfirmOpenShipment package additional details
     * object in DB
     * 
     * @param shpmtPkgAddtService
     * @return
     */
    private MapSqlParameterSource createSqlParametersForConfirmShipmentPackageAdditionalServices(
            String fedexId,
            ShipmentPackageAdditionalService shpmtPkgAddtService) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(SHPMT_TRKNG_NBR, shpmtPkgAddtService.getShipmentTrackingNumber());
        parameters.addValue(PKG_ACCPT_TMSTP, ZonedDateTime.parse(shpmtPkgAddtService.getAcceptTmStmp())
                .format(DateTimeFormatter.ofPattern(TRANSACTION_TIMESTAMP_FORMAT)));
        parameters.addValue("PKG_TRKNG_NBR", shpmtPkgAddtService.getTrackingNumber());
        parameters.addValue("SPCL_SVC_CD", shpmtPkgAddtService.getSpecialSerivceCode());
        parameters.addValue("SPCL_SVC_DESC", shpmtPkgAddtService.getSpecialServiceDescription());
        parameters.addValue(CRTE_APPLI_ID, APP_ID);
        parameters.addValue(CRTE_USER_ID, fedexId);
        parameters.addValue(LAST_MODFY_APPLI_ID, APP_ID);
        parameters.addValue(LAST_MODFY_USER_ID, fedexId);

        return parameters;
    }
    
    /**
     * It will set the sql parameters to save the interline shipment details object in DB
     * 
     * @param interlineShipmentDetail
     * @return
     */
    private MapSqlParameterSource createSqlParametersForInterlineShipmentDetails(
            String fedexId,
            InterlineShipmentDetail interlineShipmentDetail) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(SHPMT_TRKNG_NBR, interlineShipmentDetail.getShipmentTrackingNumber());
        parameters.addValue(PKG_ACCPT_TMSTP, ZonedDateTime.parse(interlineShipmentDetail.getAcceptTmStmp())
                .format(DateTimeFormatter.ofPattern(TRANSACTION_TIMESTAMP_FORMAT)));
        parameters.addValue("INTLN_ID", interlineShipmentDetail.getInterlineId());
        parameters.addValue("INTLN_EMP_NBR", interlineShipmentDetail.getEmployeeId());
        parameters.addValue("SHPMT_WGT", interlineShipmentDetail.getShipmentWeight());
        parameters.addValue(LAST_MODFY_USER_ID, fedexId);
        parameters.addValue(LAST_MODFY_APPLI_ID, APP_ID);        
        parameters.addValue(CRTE_USER_ID, fedexId);
        parameters.addValue(CRTE_APPLI_ID, APP_ID);
        return parameters;
    }

    /**
     * Method to retrieve the shipment information from database.
     * 
     * @param shipmentTrackingNumber
     * @param paymentRefId
     */
    @Override
    @HystrixCommand(groupKey = "ConfirmShipmentRepository", commandKey = "getShipmentDetails")
    public ShipmentDetail getShipmentDetails(
            String shipmentTrackingNumber,
            String paymentRefId) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(SHPMT_TRKNG_NBR, shipmentTrackingNumber);
        parameters.addValue("paymentRefId", paymentRefId);

        String getShipmentQuery =
                "select SHPMT_TRKNG_NBR,PKG_ACCPT_TMSTP,INITL_ACCPT_TMSTP,FEDEX_ID_NBR,FEDEX_LOC_ID,OPCO_LOC_ID,OPCO_CD,"
                        + "DEVICE_ID_NBR,DVC_PLATFORM_TYPE_CD,API_CLIENT_TYPE_NM,API_VERS_ID,API_CLIENT_ID,ORIG_CTRY_CD,AFTER_PICKUP_FLG,"
                        + "FXG_ACCT_NBR,CITY_CNTR_ACCT_NBR,METER_NBR,SOFTWARE_ID,PAYMENT_TYPE_CD,CUST_ACCT_NBR,BILL_TO_ACCT_TYPE_CD,FEDEX_PKG_TYPE_CD,"
                        + "NON_FEDEX_PKG_TYPE_CD,STANDARD_PKG_FLG,TOTAL_PKG_QTY,CURRENCY_CD,WGT_UOM_CD,DIM_UOM_CD,RATING_TYPE_CD,SHPNG_SVC_NM,"
                        + "HAL_FEDEX_LOC_ID,CHARGE_AMT,DISCOUNT_AMT,SURCHARGE_AMT,TAX_AMT,NET_CHARGE_AMT,TRAN_SHPMT_REF_ID,POS_TRAN_REF_ID,"
                        + "DEL_COMMIT_DT,CUR_SHPMT_STATUS_CD,CUR_SHPMT_STAT_TMSTP,CRTE_APPLI_ID,CRTE_TMSTP,CRTE_USER_ID,LAST_MODFY_APPLI_ID,"
                        + "LAST_MODFY_TMSTP,LAST_MODFY_USER_ID,SHPNG_SVC_OPCO_NM from FXORSCS_SCHEMA.SHIPMENT where SHPMT_TRKNG_NBR=:shipmentTrackingNumber and POS_TRAN_REF_ID=:paymentRefId";

        logger.debug("Executing query to fetch shipment information: {} with params : [ {} ]", getShipmentQuery,
                parameters);

        ShipmentDetail shipmentDetail =
                namedJdbcTemplate.queryForObject(getShipmentQuery, parameters, new ShipmentRowMapper());

        // This query will only execute if there is data present in the parent table....
        if (shipmentDetail != null) {

            String getShipmentPackageQuery =
                    "select SHPMT_TRKNG_NBR,PKG_ACCPT_TMSTP,PKG_TRKNG_NBR,PKG_BARCD_CD,FORM_ID,MPS_MSTR_PKG_FLG,PKG_SEQ_NBR,"
                            + "PKG_LTH_QTY,PKG_HGT_QTY,PKG_WIDTH_QTY,PKG_DECVAL_AMT,PKG_WGT,MANL_WGT_FLG,PAA_DAMAGE_STAT_TXT,PAA_PACK_BY_TXT,"
                            + "PAA_INSPT_STAT_TXT,DRY_ICE_WGT,CRTE_APPLI_ID,CRTE_TMSTP,CRTE_USER_ID,LAST_MODFY_APPLI_ID,LAST_MODFY_TMSTP,"
                            + "LAST_MODFY_USER_ID from FXORSCS_SCHEMA.SHIPMENT_PACKAGE where SHPMT_TRKNG_NBR=:shipmentTrackingNumber";

            logger.debug("Executing query to fetch shipment package information: {} with params : [ {} ]",
                    getShipmentPackageQuery, parameters);

            shipmentDetail.setShipmentPackageDetails(
                    namedJdbcTemplate.query(getShipmentPackageQuery, parameters, new ShipmentPackageRowMapper()));

            return shipmentDetail;
        }

        return null;
    }

    /**
     * Method to fetch open shipment information from SHIPMENT and related table by barcode.
     * 
     * @param id
     * @param locationId
     * @return List<SoldShipmentDetail>
     */
    @Override
    @HystrixCommand(groupKey = "ConfirmShipmentRepository", commandKey = "getSoldShipmentByBarCode")
    public List<SoldShipmentDetail> getSoldShipmentByBarCode(
            String id,
            String locationId) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(ID, id);

        String getSoldShipmentByBarCodeQuery =
                "select sh.SHPMT_TRKNG_NBR,SHPNG_SVC_OPCO_NM,FEDEX_PKG_TYPE_CD,TOTAL_PKG_QTY,CURRENCY_CD,NET_CHARGE_AMT,SHPNG_SVC_DESC,CUR_SHPMT_STATUS_CD,TRAN_SHPMT_REF_ID,OPCO_CD,OPCO_LOC_ID,SNDR_NM,SNDR_COMPANY_NM,SNDR_CITY_NM,SNDR_POSTAL_CD,SNDR_SAME_AS_CUST_FLG,SNDR_STATE_PROV_CD,SNDR_ADDR_1_TXT,SNDR_ADDR_2_TXT,SNDR_ADDR_3_TXT,CUST_NM,CUST_COMPANY_NM,CUST_CITY_NM,CUST_POSTAL_CD,CUST_PHONE_1_NBR,CUST_PHONE_EXT_1_NBR,CUST_STATE_PROV_CD,CUST_ADDR_1_TXT,CUST_ADDR_2_TXT,CUST_ADDR_3_TXT,RECP_NM,RECP_COMPANY_NM,RECP_CITY_NM,RECP_POSTAL_CD,RSDNL_RECP_ADDR_FLG,RECP_STATE_PROV_CD,RECP_ADDR_1_TXT,RECP_ADDR_2_TXT,RECP_ADDR_3_TXT,PAYMENT_TYPE_CD,sh.INITL_ACCPT_TMSTP,sh.POS_TRAN_REF_ID,FEDEX_LOC_ID from FXORSCS_SCHEMA.shipment sh inner join FXORSCS_SCHEMA.SHIPMENT_CONTACT sc on sh.SHPMT_TRKNG_NBR=sc.SHPMT_TRKNG_NBR inner join FXORSCS_SCHEMA.SHIPMENT_PACKAGE sp on sh.SHPMT_TRKNG_NBR=sp.SHPMT_TRKNG_NBR where PKG_BARCD_CD=:id  order by sh.PKG_ACCPT_TMSTP asc";

        logger.debug("Executing query to fetch sold shipment information by barcode : {} with params : [ {} ]",
                getSoldShipmentByBarCodeQuery, parameters);

        List<SoldShipmentDetail> soldShipmentDetails = namedJdbcTemplate.query(getSoldShipmentByBarCodeQuery,
                parameters, new SoldShipmentRowMapper(locationId));
        if (!CollectionUtils.isEmpty(soldShipmentDetails)) {
            setPackageLineItems(soldShipmentDetails);
        }
        return soldShipmentDetails;
    }

    /**
     * Method to fetch open shipment information from SHIPMENT and related table by guid.
     * 
     * @param id
     * @param locationId
     * @return List<SoldShipmentDetail>
     */
    @Override
    public List<SoldShipmentDetail> getSoldShipmentByGuid(
            String id,
            String locationId) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(ID, id);
        parameters.addValue(LOCATION_ID, locationId);

        String getSoldShipmentByGuidQuery =
                "select sh.SHPMT_TRKNG_NBR,SHPNG_SVC_OPCO_NM,FEDEX_PKG_TYPE_CD,TOTAL_PKG_QTY,CURRENCY_CD,NET_CHARGE_AMT,SHPNG_SVC_DESC,CUR_SHPMT_STATUS_CD,TRAN_SHPMT_REF_ID,OPCO_CD,OPCO_LOC_ID,SNDR_NM,SNDR_COMPANY_NM,SNDR_CITY_NM,SNDR_POSTAL_CD,SNDR_SAME_AS_CUST_FLG,SNDR_STATE_PROV_CD,SNDR_ADDR_1_TXT,SNDR_ADDR_2_TXT,SNDR_ADDR_3_TXT,CUST_NM,CUST_COMPANY_NM,CUST_CITY_NM,CUST_POSTAL_CD,CUST_PHONE_1_NBR,CUST_PHONE_EXT_1_NBR,CUST_STATE_PROV_CD,CUST_ADDR_1_TXT,CUST_ADDR_2_TXT,CUST_ADDR_3_TXT,RECP_NM,RECP_COMPANY_NM,RECP_CITY_NM,RECP_POSTAL_CD,RSDNL_RECP_ADDR_FLG,RECP_STATE_PROV_CD,RECP_ADDR_1_TXT,RECP_ADDR_2_TXT,RECP_ADDR_3_TXT,PAYMENT_TYPE_CD,sh.INITL_ACCPT_TMSTP,sh.POS_TRAN_REF_ID,FEDEX_LOC_ID from FXORSCS_SCHEMA.shipment sh inner join FXORSCS_SCHEMA.SHIPMENT_CONTACT sc on sh.SHPMT_TRKNG_NBR=sc.SHPMT_TRKNG_NBR  where sh.TRAN_SHPMT_REF_ID=:id  order by sh.PKG_ACCPT_TMSTP asc";
        
        logger.debug("Executing query to fetch sold shipment information by guid: {} with params : [ {} ]",
                getSoldShipmentByGuidQuery, parameters);

        List<SoldShipmentDetail> soldShipmentDetails =
                namedJdbcTemplate.query(getSoldShipmentByGuidQuery, parameters, new SoldShipmentRowMapper(locationId));
        if (!CollectionUtils.isEmpty(soldShipmentDetails)) {
            setPackageLineItems(soldShipmentDetails);
        }
        return soldShipmentDetails;

    }

    /**
     * Method to fetch open shipment information from SHIPMENT and related table by tracking number.
     * 
     * @param id
     * @param locationId
     * @return List<SoldShipmentDetail>
     */
    @Override
    public List<SoldShipmentDetail> getSoldShipmentByTrackingNumber(
            String id,
            String locationId) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(ID, id);

        String getSoldShipmentByTrackQuery =
                "select sh.SHPMT_TRKNG_NBR,SHPNG_SVC_OPCO_NM,FEDEX_PKG_TYPE_CD,TOTAL_PKG_QTY,CURRENCY_CD,NET_CHARGE_AMT,SHPNG_SVC_DESC,CUR_SHPMT_STATUS_CD,TRAN_SHPMT_REF_ID,OPCO_CD,OPCO_LOC_ID,SNDR_NM,SNDR_COMPANY_NM,SNDR_CITY_NM,SNDR_POSTAL_CD,SNDR_SAME_AS_CUST_FLG,SNDR_STATE_PROV_CD,SNDR_ADDR_1_TXT,SNDR_ADDR_2_TXT,SNDR_ADDR_3_TXT,CUST_NM,CUST_COMPANY_NM,CUST_CITY_NM,CUST_POSTAL_CD,CUST_PHONE_1_NBR,CUST_PHONE_EXT_1_NBR,CUST_STATE_PROV_CD,CUST_ADDR_1_TXT,CUST_ADDR_2_TXT,CUST_ADDR_3_TXT,RECP_NM,RECP_COMPANY_NM,RECP_CITY_NM,RECP_POSTAL_CD,RSDNL_RECP_ADDR_FLG,RECP_STATE_PROV_CD,RECP_ADDR_1_TXT,RECP_ADDR_2_TXT,RECP_ADDR_3_TXT,PAYMENT_TYPE_CD,sh.INITL_ACCPT_TMSTP,sh.POS_TRAN_REF_ID,FEDEX_LOC_ID from FXORSCS_SCHEMA.shipment sh inner join FXORSCS_SCHEMA.SHIPMENT_CONTACT sc on sh.SHPMT_TRKNG_NBR=sc.SHPMT_TRKNG_NBR  where sh.SHPMT_TRKNG_NBR=:id  order by sh.PKG_ACCPT_TMSTP asc";

        logger.debug("Executing query to fetch sold shipment information by tracking number: {} with params : [ {} ]",
                getSoldShipmentByTrackQuery, parameters);

        List<SoldShipmentDetail> soldShipmentDetails =
                namedJdbcTemplate.query(getSoldShipmentByTrackQuery, parameters, new SoldShipmentRowMapper(locationId));
        if (!CollectionUtils.isEmpty(soldShipmentDetails)) {
            setPackageLineItems(soldShipmentDetails);
        }
        return soldShipmentDetails;
    }
    
    /**
     * Method to fetch shipment information from SHIPMENT and related table by locationId and
     * operationaldate.
     * 
     * @param locationId
     * @param operationaldate
     * @return List<SoldShipmentSummary>
     */
    @Override
    public List<SoldShipmentSummary> getShipmentsByLocation(
            String locationId,
            String operationaldate) {

        ZonedDateTime givenZonedDateTime = ZonedDateTime.parse(operationaldate);

        String fromDate = givenZonedDateTime.minusDays((soldShpmentsSearchDurationDays))
                .with(LocalTime.MIN)
                .format(DateTimeFormatter.ofPattern(TRANSACTION_TIMESTAMP_FORMAT));

        String toDate = givenZonedDateTime.withZoneSameInstant(ZoneId.systemDefault())
                .with(LocalTime.MAX)
                .format(DateTimeFormatter.ofPattern(TRANSACTION_TIMESTAMP_FORMAT));

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(LOCATION_ID, locationId);
        parameters.addValue(FROM_DATE, fromDate);
        parameters.addValue(TO_DATE, toDate);
        parameters.addValue(STATUS_CODE, STATUSCODE);

        String getSoldShipmentByLocationQuery =
                "select sh.SHPMT_TRKNG_NBR,SHPNG_SVC_DESC,SHPNG_SVC_OPCO_NM,CUR_SHPMT_STATUS_CD,TRAN_SHPMT_REF_ID,POS_TRAN_REF_ID,MPS_MSTR_PKG_FLG,PKG_SEQ_NBR,PKG_BARCD_CD,OPCO_CD,sh.INITL_ACCPT_TMSTP from FXORSCS_SCHEMA.shipment sh inner join FXORSCS_SCHEMA.SHIPMENT_PACKAGE sp on sh.SHPMT_TRKNG_NBR=sp.SHPMT_TRKNG_NBR where FEDEX_LOC_ID=:locationId AND CUR_SHPMT_STATUS_CD=:statusCode AND sh.PKG_ACCPT_TMSTP BETWEEN TO_DATE(:fromDate,'YYYY:MM:dd:HH24:MI:SS') AND TO_DATE(:toDate,'YYYY:MM:dd:HH24:MI:SS') order by sh.PKG_ACCPT_TMSTP asc";

        logger.debug("Executing query to fetch sold shipment summeries information : {} with params : [ {} ]",
                getSoldShipmentByLocationQuery, parameters);
        return (List<SoldShipmentSummary>) namedJdbcTemplate.query(getSoldShipmentByLocationQuery, parameters,
                new SoldShipmentSummaryRowMapper());
    }
    
    /**
     * Method to update shipment status into the DB.
     */
    @Override
    @HystrixCommand(groupKey = "ConfirmShipmentRepository", commandKey = "updateStatus")
    public boolean updateStatus(
            UpdateOpenShipmentStatusData updateShipmentStatusData) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("referenceId", updateShipmentStatusData.getReferenceId());
        parameters.addValue(LOCATION_ID, updateShipmentStatusData.getLocationId());
        parameters.addValue("trackingId", updateShipmentStatusData.getTrackingId());
        parameters.addValue("openShipmentStatusCode", updateShipmentStatusData.getOpenShipmentStatusCode());
        parameters.addValue("appId", APP_ID);
        parameters.addValue("teamMemberId", updateShipmentStatusData.getTeamMemberId());
        parameters.addValue("shipStatTimeStamp", LocalDateTime.now());

        String updateShipmentStatusQuery =
                "UPDATE FXORSCS_SCHEMA.SHIPMENT SET CUR_SHPMT_STATUS_CD =:openShipmentStatusCode,LAST_MODFY_APPLI_ID =:appId,LAST_MODFY_TMSTP=SYSDATE,LAST_MODFY_USER_ID=:teamMemberId,CUR_SHPMT_STAT_TMSTP=:shipStatTimeStamp where POS_TRAN_REF_ID =:referenceId AND FEDEX_LOC_ID =:locationId AND SHPMT_TRKNG_NBR =:trackingId";

        logger.debug("Executing query to update shipment status: {} with params : [ {} ]", updateShipmentStatusQuery,
                parameters);
        return namedJdbcTemplate.update(updateShipmentStatusQuery, parameters) != 0;
    }

    /**
     * Method to set packageLineItems
     * 
     * @param soldShipmentDetails
     */
    private void setPackageLineItems(
            List<SoldShipmentDetail> soldShipmentDetails) {

        String getPackageItems =
                "select SHPMT_TRKNG_NBR,PKG_SEQ_NBR,PKG_BARCD_CD,MPS_MSTR_PKG_FLG from FXORSCS_SCHEMA.shipment_package where SHPMT_TRKNG_NBR=:id";

        for (SoldShipmentDetail soldShipmentDetail : soldShipmentDetails) {
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue(ID, soldShipmentDetail.getTrackingNumber());

            soldShipmentDetail.setPackageLineItems(
                    namedJdbcTemplate.query(getPackageItems, parameters, new PackageLineItemsRowMapper()));
            if (!CollectionUtils.isEmpty(soldShipmentDetail.getPackageLineItems())) {
                soldShipmentDetail.setMasterTrackingId(soldShipmentDetail.getPackageLineItems()
                        .get(0)
                        .getTrackingId());
            }
        }
    }

    /**
     * Method to save ground rate notification flag in database
     */
    @Override
    public void updateGroundRateNotification(
            String trackingId,
            String shipmentTimeStamp) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(SHPMT_TRKNG_NBR, trackingId);
        parameters.addValue(PKG_ACCPT_TMSTP, ZonedDateTime.parse(shipmentTimeStamp)
                .format(DateTimeFormatter.ofPattern(TRANSACTION_TIMESTAMP_FORMAT)));
        parameters.addValue(GROUND_TXN_RATE_FLG, "Y");

        String updateGroundTransactionRateFlagQuery =
                "UPDATE FXORSCS_SCHEMA.SHIPMENT SET GRND_RTNG_SYS_NTFY_FLG =:groundTxnRateFlag WHERE SHPMT_TRKNG_NBR =:shipmentTrackingNumber AND PKG_ACCPT_TMSTP =TO_DATE(:packageAcceptanceTimestamp,'YYYY:MM:dd:HH24:MI:SS')";

        logger.debug("Executing query to update gorund shipment rate flag : {} with params : [ {} ]",
                updateGroundTransactionRateFlagQuery, parameters);
        namedJdbcTemplate.update(updateGroundTransactionRateFlagQuery, parameters);
    }

}
