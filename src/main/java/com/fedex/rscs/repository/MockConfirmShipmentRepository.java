package com.fedex.rscs.repository;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.netflix.hystrix.exception.HystrixRuntimeException;

import com.fedex.common.cxs.exception.TerminalDbAccessException;
import com.fedex.rscs.model.AddressDetail;
import com.fedex.rscs.model.CarrierDetail;
import com.fedex.rscs.model.ContactDetail;
import com.fedex.rscs.model.PackageLineItemsDetail;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.ShipmentDetail;
import com.fedex.rscs.model.ShipmentPackageDetail;
import com.fedex.rscs.model.SoldShipmentDetail;
import com.fedex.rscs.model.SoldShipmentSummary;
import com.fedex.rscs.model.cshp.CreditCardData;
import com.fedex.rscs.model.cshp.Price;
import com.fedex.rscs.model.cshp.TrackingSequenceDetail;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;

/**
 * A mock implementation of {@link ConfirmShipmentRepository}. If you are using this as part of a
 * unit test, please evaluate whether use of Mockito would be more appropriate.
 */
@Repository
@Profile("mock")
public class MockConfirmShipmentRepository implements ConfirmShipmentRepository {

    private static final Logger logger = LoggerFactory.getLogger(MockConfirmShipmentRepository.class);

    private static final String TRACKING_ID = "123456789";
    private static final String DB_ACCESS_EXCEPTION = "Mock: Terminal DB Access Exception";
    private static final String TIME_STAMP = "2020-07-30T11:55:28.687+05:30";

    /**
     * Mock response for saving shipment
     */
    @Override
    public boolean save(
            ShipmentDetail shipmentDetail) {

        if (!ObjectUtils.isEmpty(shipmentDetail) && !StringUtils.isEmpty(shipmentDetail.getShipmentTrackingNumber())) {

            if (shipmentDetail.getShipmentTrackingNumber()
                    .equals(TRACKING_ID)) {

                logger.error(DB_ACCESS_EXCEPTION);
                throw new TerminalDbAccessException(DB_ACCESS_EXCEPTION);
            }

            logger.info("Mock: Shipment saved successfully !!!");
            return true;
        }

        logger.info("Mock: Shipment not saved !!!");
        return false;
    }

    /**
     * Mock response after deleting shipment
     */
    @Override
    public boolean delete(
            String shipmentTrackingNumber) {

        if (!StringUtils.isEmpty(shipmentTrackingNumber)) {

            if (shipmentTrackingNumber.equals(TRACKING_ID)) {

                logger.error(DB_ACCESS_EXCEPTION);
                throw new TerminalDbAccessException(DB_ACCESS_EXCEPTION);
            }

            logger.info("Mock: Shipment deleted successfully !!!");
            return true;
        }

        logger.info("Mock: Shipment not available for deletion !!!");
        return false;
    }

    /**
     * Mock Response for shipment information.
     */
    @Override
    public ShipmentDetail getShipmentDetails(
            String shipmentTrackingNumber,
            String paymentRefId) {

        if (!StringUtils.isEmpty(shipmentTrackingNumber) && !StringUtils.isEmpty(paymentRefId)) {

            if (shipmentTrackingNumber.equals(TRACKING_ID)) {

                logger.error(DB_ACCESS_EXCEPTION);
                throw new TerminalDbAccessException(DB_ACCESS_EXCEPTION);
            }

            logger.info("Mock: Shipment informatiion found !!!");
            return createShipmentDetail();
        }

        logger.info("Mock: Shipment not available !!!");
        return null;
    }

    /**
     * Method to create Shipment Detail
     * 
     * @return ShipmentDetail
     */
    private ShipmentDetail createShipmentDetail() {

        ShipmentDetail shipmentDetail = new ShipmentDetail();
        shipmentDetail.setAcceptTmStmp(TIME_STAMP);
        shipmentDetail.setAfterPickupFlag(false);
        shipmentDetail.setApiClientId("RSCS");
        shipmentDetail.setApiClientTypeName("FX");
        shipmentDetail.setApiVersionId("Major");
        shipmentDetail.setBillToAccountType("account");
        shipmentDetail.setChargeAmount(10);
        shipmentDetail.setCityCenterAccountNumber("1234");
        shipmentDetail.setCurrency("USD");
        shipmentDetail.setCurrentShipmentStatTmStmp(TIME_STAMP);
        shipmentDetail.setCurrentShipmentStatus("created");
        shipmentDetail.setCustomerAccountNumber("98978909");
        shipmentDetail.setDelCommitDate(TIME_STAMP);
        shipmentDetail.setDeviceId("Fuse");
        shipmentDetail.setDevicePlatformType("pos");
        shipmentDetail.setDimensionUom("IN");
        shipmentDetail.setDiscountAmount(0);
        shipmentDetail.setFedexId("00000");
        shipmentDetail.setFedexLocationId("DNEK");
        shipmentDetail.setFedexPackageType("YOUR_PACKAGING");
        shipmentDetail.setGroundAccountNumber("0123");
        shipmentDetail.setHalFedexLocationId("DFWD");
        shipmentDetail.setInitialAcceptanceTmStmp(TIME_STAMP);
        shipmentDetail.setMeterNumber("012");
        shipmentDetail.setNetChargeAmount(10.0);
        shipmentDetail.setNonFedexPackageType("Your_pack");
        shipmentDetail.setOpCoCode("FXO");
        shipmentDetail.setOpCoLocationId("DREK");
        shipmentDetail.setOriginCountryCode("US");
        shipmentDetail.setPaymentType("account");
        shipmentDetail.setPosTransactionRefId("888");
        shipmentDetail.setRatingType("one_rate");
        shipmentDetail.setShipmentPackageDetails(createShipmentPackageDetails());
        shipmentDetail.setShipmentTrackingNumber("1234567890");
        shipmentDetail.setShippingServiceName("0998");
        shipmentDetail.setSoftwareId("SSFE");
        shipmentDetail.setStandardPackageFlag(false);
        shipmentDetail.setSurchargeAmount(0.8);
        shipmentDetail.setTaxAmount(0);
        shipmentDetail.setTotalPackageQuantity(1);
        shipmentDetail.setTransactionShipmentRefId("123456");
        shipmentDetail.setWeightUom("LB");
        return shipmentDetail;
    }

    /**
     * Method to create List<ShipmentPackageDetail>
     * 
     * @return List<ShipmentPackageDetail>
     */
    private List<ShipmentPackageDetail> createShipmentPackageDetails() {

        List<ShipmentPackageDetail> shipmentPackageDetailList = new ArrayList<>();
        ShipmentPackageDetail shipmentPackageDetail = new ShipmentPackageDetail();
        shipmentPackageDetail.setAcceptTmStmp(TIME_STAMP);
        shipmentPackageDetail.setAmount(12);
        shipmentPackageDetail.setBarcode("1000000076543");
        shipmentPackageDetail.setDryIceWeight(0.01);
        shipmentPackageDetail.setFormId("098765");
        shipmentPackageDetail.setHeightQuantity(1);
        shipmentPackageDetail.setLengthQuantity(1);
        shipmentPackageDetail.setManualWeightFlag(false);
        shipmentPackageDetail.setMasterFlag(false);
        shipmentPackageDetail.setPaaDamageStatText("Known");
        shipmentPackageDetail.setPaaInspectionStatText("Inspected by fedex");
        shipmentPackageDetail.setPaaPackByText("Premetered");
        shipmentPackageDetail.setSeqNumber(1);
        shipmentPackageDetail.setShipmentTrackingNumber("12345678");
        shipmentPackageDetail.setTrackingNumber(TRACKING_ID);
        shipmentPackageDetail.setWeight(0.7);
        shipmentPackageDetail.setWidthQuantity(1);
        shipmentPackageDetailList.add(shipmentPackageDetail);
        return shipmentPackageDetailList;
    }

    /**
     * Mock method to get List of SoldShipmentDetail's by barcode
     */
    @Override
    public List<SoldShipmentDetail> getSoldShipmentByBarCode(
            String id,
            String locationId) {

        if (id.equalsIgnoreCase(TRACKING_ID)) {
            logger.error(DB_ACCESS_EXCEPTION);
            throw new TerminalDbAccessException("");
        }
        if (id.equalsIgnoreCase("123456780")) {
            logger.error("Mock: Throwing Hystrix Runtime Exception due to id=123456780");
            throw new HystrixRuntimeException(null, null, "", null, null);
        }
        return createSoldShipmentResourceDetails("barcode", id, 3);
    }

    /**
     * Mock method to get List of SoldShipmentDetail's by guid
     */
    @Override
    public List<SoldShipmentDetail> getSoldShipmentByGuid(
            String id,
            String locationId) {

        if (id.equalsIgnoreCase(TRACKING_ID)) {
            logger.error(DB_ACCESS_EXCEPTION);
            throw new TerminalDbAccessException("");
        }
        if (id.equalsIgnoreCase("123456780")) {
            logger.error("Mock: Throwing Hystrix Runtime Exception due to id=123456780");
            throw new HystrixRuntimeException(null, null, "", null, null);
        }
        return createSoldShipmentResourceDetails("guid", id, 3);
    }

    /**
     * Mock method to get List of SoldShipmentDetail's by trackingNumber
     */
    @Override
    public List<SoldShipmentDetail> getSoldShipmentByTrackingNumber(
            String id,
            String locationId) {

        if (id.equalsIgnoreCase(TRACKING_ID)) {
            logger.error(DB_ACCESS_EXCEPTION);
            throw new TerminalDbAccessException("");
        }
        if (id.equalsIgnoreCase("123456780")) {
            logger.error("Mock: Throwing Hystrix Runtime Exception due to id=123456780");
            throw new HystrixRuntimeException(null, null, "", null, null);
        }
        return createSoldShipmentResourceDetails("track", id, 3);
    }

    /**
     * Create SoldShipmentDetail's List
     * 
     * @return
     */
    private List<SoldShipmentDetail> createSoldShipmentResourceDetails(
            String type,
            String id,
            int size) {

        List<SoldShipmentDetail> shipmentDetails = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            SoldShipmentDetail shipmentDetail = new SoldShipmentDetail();
            shipmentDetail.setCustomer(createPartyDetail());
            shipmentDetail.setGuid(type.equalsIgnoreCase("guid") ? id : ("12345678912" + i));
            shipmentDetail.setPackageCount(i + "");
            List<PackageLineItemsDetail> packageLineItems = new ArrayList<>();
            PackageLineItemsDetail packageLineItemsDetail = new PackageLineItemsDetail();
            packageLineItemsDetail.setMaster(i % 2 == 0);
            TrackingSequenceDetail trackingSequenceDetail = new TrackingSequenceDetail();
            trackingSequenceDetail.setTrackingNumber(type.equalsIgnoreCase("track") ? id : ("12345678912" + i));
            trackingSequenceDetail.setBarcode(type.equalsIgnoreCase("barcode") ? id : ("12345678912" + i));
            trackingSequenceDetail.setSequence(i);
            packageLineItemsDetail.setTrackingId(trackingSequenceDetail);
            packageLineItems.add(packageLineItemsDetail);
            shipmentDetail.setPackageLineItems(packageLineItems);
            shipmentDetail.setMasterTrackingId(trackingSequenceDetail);
            shipmentDetail.setPackagingType("FEDEX_PAK");
            shipmentDetail.setPaymentType("ACCOUNT");
            shipmentDetail.setRecipient(createPartyDetail());
            shipmentDetail.setRetailTransactionId("12345678" + i);
            shipmentDetail.setServiceType("FIRST_OVERNIGHT");
            shipmentDetail.setShipper(createPartyDetail());
            shipmentDetail.setStatus("CREATED");
            shipmentDetail.setTotalAmount(new Price("USD", 100d));
            shipmentDetail.setTransactionDateTime(TIME_STAMP);
            CarrierDetail carrierDetail = new CarrierDetail();
            if (i % 2 == 0) {
                carrierDetail.setCarrierCode("FDXE");
                carrierDetail.setOpco("EXPRESS");
            } else {
                carrierDetail.setCarrierCode("FDXG");
                carrierDetail.setOpco("GROUND");
            }

            shipmentDetail.setCarrierDetails(carrierDetail);
            shipmentDetails.add(shipmentDetail);
        }
        return shipmentDetails;
    }

    /**
     * Create PartyDetail
     * 
     * @return
     */
    private PartyDetail createPartyDetail() {

        List<String> streetLines = new ArrayList<>();
        streetLines.add("4071 North Trail");

        AddressDetail addressDetail = new AddressDetail();
        addressDetail.setPostalCode("75023");
        addressDetail.setCountryCode("US");
        addressDetail.setStreetLines(streetLines);
        return new PartyDetail(addressDetail, createContactDetail(), "038000004",
                new CreditCardData("4005554444444460", "VISA", "12/2020"), "FEDEX_EXPRESS");
    }

    /**
     * Create ContactDetail
     * 
     * @return
     */
    private ContactDetail createContactDetail() {

        ContactDetail contactDetail = new ContactDetail();
        contactDetail.setContactId("test123");
        contactDetail.setFullName("Shakti Saurabh");
        contactDetail.setPhoneNumber("3900090");

        return contactDetail;
    }

    /**
     * Method to fetch shipment information from SHIPMENT and related table by locationId and
     * operationaldate.
     * 
     * @param locationId
     * @param operationaldate
     * @return List<SoldShipmentSummary>
     */
    @Override
    public List<SoldShipmentSummary> getShipmentsByLocation(
            String locationId,
            String operationaldate) {

        if (locationId.equalsIgnoreCase("DNEK")) {
            logger.error(DB_ACCESS_EXCEPTION);
            throw new TerminalDbAccessException("");
        }
        if (locationId.equalsIgnoreCase("LASA")) {
            logger.error("Mock: Throwing Hystrix Runtime Exception due to locationId = LASA");
            throw new HystrixRuntimeException(null, null, "", null, null);
        }
        return createShipmentDetails(15);
    }

    /**
     * Create SoldShipmentSummary's List
     * 
     * @return
     */
    private List<SoldShipmentSummary> createShipmentDetails(
            int size) {

        List<SoldShipmentSummary> shipmentDetails = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            SoldShipmentSummary shipmentDetail = new SoldShipmentSummary();
            List<PackageLineItemsDetail> packageLineItems = new ArrayList<>();
            PackageLineItemsDetail packageLineItemsDetail = new PackageLineItemsDetail();
            packageLineItemsDetail.setMaster(i % 2 == 0);
            TrackingSequenceDetail trackingSequenceDetail = new TrackingSequenceDetail();
            trackingSequenceDetail.setTrackingNumber("794918364888" + i);
            trackingSequenceDetail.setBarcode("9632001960750420540200794918364888" + i);
            trackingSequenceDetail.setSequence(0);
            packageLineItemsDetail.setTrackingId(trackingSequenceDetail);
            packageLineItems.add(packageLineItemsDetail);
            shipmentDetail.setGuid(("75042" + i));
            shipmentDetail.setPackageLineItems(packageLineItems);
            shipmentDetail.setMasterTrackingId(trackingSequenceDetail);
            shipmentDetail.setRetailTransactionId("7949183" + i);
            shipmentDetail.setServiceType("FIRST_OVERNIGHT");
            shipmentDetail.setStatus("CREATED");
            shipmentDetail.setTransactionDateTime(TIME_STAMP);
            CarrierDetail carrierDetail = new CarrierDetail();
            if (i % 2 == 0) {
                carrierDetail.setCarrierCode("FDXE");
                carrierDetail.setOpco("EXPRESS");
            } else {
                carrierDetail.setCarrierCode("FDXG");
                carrierDetail.setOpco("GROUND");
            }

            shipmentDetail.setCarrierDetails(carrierDetail);
            shipmentDetails.add(shipmentDetail);
        }
        return shipmentDetails;
    }

    @Override
    public boolean updateStatus(
            UpdateOpenShipmentStatusData updateOpenShipmentStatusData) {

        logger.info("Mock: Update status called and return false !!!");
        return false;
    }

    /**
     * Mock method to save ground rate notification flag
     *
     */
    @Override
    public void updateGroundRateNotification(
            String trackingId,
            String shipmentTimeStamp) {

        logger.info("Mock: update Ground Rate Notification return true !!!");
    }
}
