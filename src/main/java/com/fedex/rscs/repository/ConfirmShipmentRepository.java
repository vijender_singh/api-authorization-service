package com.fedex.rscs.repository;

import java.util.List;

import com.fedex.rscs.model.ShipmentDetail;
import com.fedex.rscs.model.SoldShipmentDetail;
import com.fedex.rscs.model.SoldShipmentSummary;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;

/**
 * Interface for FXO RSCS SCHEMA
 * 
 * @author shakti.saurabh
 *
 */
public interface ConfirmShipmentRepository {

    /**
     * Method to save confirm shipment data in SHIPMENT table
     * 
     * @param shipmentDetail
     * @return
     */
    boolean save(
            ShipmentDetail shipmentDetail);

    /**
     * Method to delete confirm shipment data in SHIPMENT table
     * 
     * @param shipmentTrackingNumber
     * @return
     */
    boolean delete(
            String shipmentTrackingNumber);
    
    /**
     * Method to fetch shipment information from SHIPMENT and related table.
     * 
     * @param shipmentTrackingNumber
     * @param paymentRefId
     * @return ShipmentDetail
     */
    ShipmentDetail getShipmentDetails(
            String shipmentTrackingNumber,
            String paymentRefId);

    /**
     * Method to fetch open shipment information from SHIPMENT and related table by barcode.
     * 
     * @param id
     * @param locationId
     * @return List<SoldShipmentResourceDetail>
     */
    List<SoldShipmentDetail> getSoldShipmentByBarCode(
            String id,
            String locationId);

    /**
     * Method to fetch open shipment information from SHIPMENT and related table by guid.
     * 
     * @param id
     * @param locationId
     * @return List<SoldShipmentResourceDetail>
     */
    List<SoldShipmentDetail> getSoldShipmentByGuid(
            String id,
            String locationId);

    /**
     * Method to fetch open shipment information from SHIPMENT and related table by tracking number.
     * 
     * @param id
     * @param locationId
     * @return List<SoldShipmentResourceDetail>
     */
    List<SoldShipmentDetail> getSoldShipmentByTrackingNumber(
            String id,
            String locationId);

    /**
     * Method to fetch shipment information from SHIPMENT and related table by locationId and
     * operationaldate.
     * 
     * @param locationId
     * @param operationaldate
     * @return List<SoldShipmentSummary>
     */
    List<SoldShipmentSummary> getShipmentsByLocation(
            String locationId,
            String operationaldate);
    

    /**
     * Method to update shipment status
     * 
     * @param updateOpenShipmentStatusData
     * @return boolean
     */
    boolean updateStatus(
            UpdateOpenShipmentStatusData updateOpenShipmentStatusData);

    /**
     * Method to update ground rate notification flag
     * 
     * @param trackingId
     * @param shipmentTimeStamp
     */
    void updateGroundRateNotification(
            String trackingId,
            String shipmentTimeStamp);
    
}
