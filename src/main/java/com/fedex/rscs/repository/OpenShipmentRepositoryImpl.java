package com.fedex.rscs.repository;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Types;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import com.fedex.rscs.model.OpenShipment;
import com.fedex.rscs.model.OpenShipmentData;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;
import com.fedex.rscs.repository.mapper.OpenShipmentRowMapper;

/**
 * This is just an example of how to integrate with another webservice using spring If your
 * application does not connect to another service, this class can be deleted
 */
@Profile("!mock")
@Repository
public class OpenShipmentRepositoryImpl implements OpenShipmentRepository {

    private static final Logger logger = LoggerFactory.getLogger(OpenShipmentRepositoryImpl.class);

    private ObjectMapper objectMapper;
    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    private static final String APP_ID = "RSCS";
    private static final String TRANSACTION_TIMESTAMP_FORMAT = "yyyy:MM:dd:HH:mm:ss";

    @Autowired
    public OpenShipmentRepositoryImpl(final NamedParameterJdbcTemplate namedJdbcTemplate) {

        this.namedJdbcTemplate = namedJdbcTemplate;
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * Method to save the open shipment details to the DB
     * 
     * @throws UnsupportedEncodingException
     */
    @Override
    @HystrixCommand(groupKey = "OpenShipmentRepository", commandKey = "saveOpenShipment")
    public boolean save(
            OpenShipment openShipment) {

        MapSqlParameterSource parameters = createSqlParametersForOpenShipment(openShipment);

        String saveOpenShipmentQuery =
                "INSERT INTO FXORSCS_SCHEMA.OPEN_SHIPMENT (OPEN_SHPMT_ID, SHPMT_TRKNG_NBR,PKG_ACCPT_TMSTP, INITL_ACCPT_TMSTP, FEDEX_LOC_ID, OPCO_LOC_ID,"
                        + " OPCO_CD, METER_NBR, CITY_CNTR_ACCT_NBR, FEDEX_ID_NBR, SHPMT_BLOB_TXT ,"
                        + "SOFTWARE_ID, DEVICE_ID_NBR, CUR_OPEN_SHPMT_STAT_CD, CUR_OPEN_SHPMT_STAT_TMSTP, CRTE_APPLI_ID, CRTE_TMSTP,"
                        + " CRTE_USER_ID, LAST_MODFY_APPLI_ID, LAST_MODFY_TMSTP, LAST_MODFY_USER_ID)"
                        + " VALUES (:OPEN_SHPMT_ID,:SHPMT_TRKNG_NBR,TO_DATE(:PKG_ACCPT_TMSTP,'YYYY:MM:dd:HH24:MI:SS'),:INITL_ACCPT_TMSTP,:FEDEX_LOC_ID,:OPCO_LOC_ID,"
                        + ":OPCO_CD,:METER_NBR,:CITY_CNTR_ACCT_NBR,:FEDEX_ID_NBR,:SHPMT_BLOB_TXT,:SOFTWARE_ID,:DEVICE_ID_NBR,:CUR_OPEN_SHPMT_STAT_CD,"
                        + ":CUR_OPEN_SHPMT_STAT_TMSTP,:CRTE_APPLI_ID,SYSDATE,:CRTE_USER_ID,:LAST_MODFY_APPLI_ID,SYSDATE,:LAST_MODFY_USER_ID)";

        logger.debug("Executing query to insert shipment transaction: {} with params : [ {} ]", saveOpenShipmentQuery,
                parameters);

        return namedJdbcTemplate.update(saveOpenShipmentQuery, parameters) != 0;
    }

    @Override
    public boolean delete(
            OpenShipment openShipment) {

        return false;
    }

    /**
     * Method to retrieve the open shipment details from the DB
     */
    @Override
    @HystrixCommand(groupKey = "OpenShipmentRepository", commandKey = "retrieve")
    public OpenShipment retrieve(
            String referenceId,
            String locationId,
            String trackingId) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("referenceId", referenceId);
        parameters.addValue("locationId", locationId);
        parameters.addValue("trackingId", trackingId);

        String retrieveOpenShipmentQuery =
                "SELECT OPEN_SHPMT_ID, SHPMT_TRKNG_NBR,PKG_ACCPT_TMSTP, INITL_ACCPT_TMSTP, FEDEX_LOC_ID, OPCO_LOC_ID, OPCO_CD, "
                        + "METER_NBR, CITY_CNTR_ACCT_NBR, FEDEX_ID_NBR, SHPMT_BLOB_TXT, SOFTWARE_ID, DEVICE_ID_NBR, CUR_OPEN_SHPMT_STAT_CD, "
                        + "CUR_OPEN_SHPMT_STAT_TMSTP, CRTE_APPLI_ID, CRTE_TMSTP, CRTE_USER_ID, LAST_MODFY_APPLI_ID, LAST_MODFY_TMSTP, "
                        + "LAST_MODFY_USER_ID from FXORSCS_SCHEMA.OPEN_SHIPMENT where OPEN_SHPMT_ID =:referenceId AND FEDEX_LOC_ID =:locationId "
                        + "AND SHPMT_TRKNG_NBR =:trackingId ";

        logger.debug("Executing query to retrieve open shipment information: {} with params : [ {} ]",
                retrieveOpenShipmentQuery, parameters);

        return namedJdbcTemplate.queryForObject(retrieveOpenShipmentQuery, parameters, new OpenShipmentRowMapper());
    }

    /**
     * It will set the sql parameters to save the OpenShipment object in DB
     * 
     * @param openShipment
     * @return
     */
    private MapSqlParameterSource createSqlParametersForOpenShipment(
            OpenShipment openShipment) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("OPEN_SHPMT_ID", openShipment.getOpenShipmentId());
        parameters.addValue("SHPMT_TRKNG_NBR", openShipment.getTrackingNumber());
        parameters.addValue("PKG_ACCPT_TMSTP", ZonedDateTime.parse(openShipment.getPackageAcceptTmStmp())
                .format(DateTimeFormatter.ofPattern(TRANSACTION_TIMESTAMP_FORMAT)));
        parameters.addValue("INITL_ACCPT_TMSTP", ZonedDateTime.parse(openShipment.getInitialAcceptanceTmStmp()));
        parameters.addValue("FEDEX_LOC_ID", openShipment.getFedexLocationId());
        parameters.addValue("OPCO_LOC_ID", openShipment.getOpCoLocationId());
        parameters.addValue("OPCO_CD", openShipment.getOpCoCode());
        parameters.addValue("METER_NBR", openShipment.getMeterNumber());
        parameters.addValue("CITY_CNTR_ACCT_NBR", openShipment.getCityCenterAccountNumber());
        parameters.addValue("FEDEX_ID_NBR", openShipment.getFedexId());
        byte[] byteData = (openShipment.getShipmentData()).getBytes();
        parameters.addValue("SHPMT_BLOB_TXT",
                new SqlLobValue(new ByteArrayInputStream(byteData), byteData.length, new DefaultLobHandler()),
                Types.BLOB);
        parameters.addValue("SOFTWARE_ID", openShipment.getSoftwareId());
        parameters.addValue("DEVICE_ID_NBR", openShipment.getDeviceId());
        parameters.addValue("CUR_OPEN_SHPMT_STAT_CD", openShipment.getOpenShipmentStatusCode());
        parameters.addValue("CUR_OPEN_SHPMT_STAT_TMSTP", LocalDateTime.now());
        parameters.addValue("CRTE_APPLI_ID", APP_ID);
        parameters.addValue("CRTE_USER_ID", openShipment.getFedexId());
        parameters.addValue("LAST_MODFY_APPLI_ID", APP_ID);
        parameters.addValue("LAST_MODFY_USER_ID", openShipment.getFedexId());

        return parameters;
    }

    /**
     * Method to update open shipment status into the DB.
     */
    @Override
    @HystrixCommand(groupKey = "OpenShipmentRepository", commandKey = "updateStatus")
    public boolean updateStatus(
            UpdateOpenShipmentStatusData updateOpenShipmentStatusData) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("referenceId", updateOpenShipmentStatusData.getReferenceId());
        parameters.addValue("locationId", updateOpenShipmentStatusData.getLocationId());
        parameters.addValue("trackingId", updateOpenShipmentStatusData.getTrackingId());
        parameters.addValue("openShipmentStatusCode", updateOpenShipmentStatusData.getOpenShipmentStatusCode());
        parameters.addValue("appId", APP_ID);
        parameters.addValue("teamMemberId", updateOpenShipmentStatusData.getTeamMemberId());

        String updateOpenShipmentStatusQuery =
                "UPDATE FXORSCS_SCHEMA.OPEN_SHIPMENT SET CUR_OPEN_SHPMT_STAT_CD =:openShipmentStatusCode,LAST_MODFY_APPLI_ID =:appId,LAST_MODFY_TMSTP=SYSDATE,LAST_MODFY_USER_ID=:teamMemberId "
                        + " where OPEN_SHPMT_ID =:referenceId AND FEDEX_LOC_ID =:locationId AND SHPMT_TRKNG_NBR =:trackingId";

        logger.debug("Executing query to update open shipment status: {} with params : [ {} ]",
                updateOpenShipmentStatusQuery, parameters);

        return namedJdbcTemplate.update(updateOpenShipmentStatusQuery, parameters) != 0;
    }

    /**
     * Used to get Open Shipment Data from BLOB
     * 
     * @param shipmentData
     * @return
     */
    public OpenShipmentData getOpenShipmentDataFromBlob(
            String shipmentData) {

        OpenShipmentData openShipmentData = null;
        try {
            openShipmentData = objectMapper.readValue(shipmentData, OpenShipmentData.class);
        } catch (IOException exp) {
            logger.error("Exception occured to get Open Shipment Data from BLOB while mapping json {}",
                    exp.getMessage());
        }
        return openShipmentData;
    }
}
