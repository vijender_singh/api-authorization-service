package com.fedex.rscs.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.fedex.common.cxs.exception.TerminalDbAccessException;
import com.fedex.common.cxs.exception.TransientDbAccessException;
import com.fedex.rscs.model.OpenShipment;
import com.fedex.rscs.model.OpenShipmentData;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;

/**
 * A mock implementation of {@link OpenShipmentRepository}. If you are using this as part of a unit
 * test, please evaluate whether use of Mockito would be more appropriate.
 */
@Repository
@Profile("mock")
public class MockOpenShipmentRepositoryImpl implements OpenShipmentRepository {

    private static final Logger logger = LoggerFactory.getLogger(MockOpenShipmentRepositoryImpl.class);

    /**
     * Mock response for saving open shipment
     */
    @Override
    public boolean save(
            OpenShipment openShipment) {

        if (!ObjectUtils.isEmpty(openShipment) && !StringUtils.isEmpty(openShipment.getOpenShipmentId())) {

            logger.info("Mock: Open Shipment saved successfully !!!");
            return true;
        }

        logger.info("Mock: Open Shipment not saved !!!");
        return false;
    }

    /**
     * Mock response after deleting open shipment
     */
    @Override
    public boolean delete(
            OpenShipment openShipment) {

        if (!ObjectUtils.isEmpty(openShipment) && !StringUtils.isEmpty(openShipment.getOpenShipmentId())) {

            logger.info("Mock: Open Shipment deleted successfully !!!");
            return true;
        }

        logger.info("Mock: Open Shipment not available for deletion !!!");
        return false;
    }
    
    /**
     * Mock response after retrieving open shipment
     * 
     * @param referenceId
     * @param locationId
     * @param trackingId
     * @return
     */
    @Override
    public OpenShipment retrieve(
            String referenceId,
            String locationId,
            String trackingId) {

        if (!StringUtils.isEmpty(referenceId) && !StringUtils.isEmpty(locationId) && !StringUtils.isEmpty(trackingId)) {
            if (referenceId.equals("123456789")) {

                logger.error("Mock: Throwing Terminal DB Access Exception due to referenceId=123456789 ");
                throw new TerminalDbAccessException("Terminal DB Access Exception");
            }

            if (referenceId.equals("123456788")) {

                logger.error("Mock: Throwing Transient DB Access Exception due to referenceId=123456788");
                throw new TransientDbAccessException("Transient DB Access Exception");
            }

            return createOpenShipment(referenceId, locationId, trackingId);
        }

        return null;
    }

    /**
     * Method to create object of OpenShipment
     * 
     * @param referenceId
     * @param locationId
     * @param trackingId
     * @return
     */
    private static OpenShipment createOpenShipment(
            String referenceId,
            String locationId,
            String trackingId) {

        // Instance of ShipmentData as JSON
        String shipmentData =
                "{\"openShipment\": {\"locationId\": \"TRLA\",\"shipmentIndex\": \"123456789\",\"carrierDetails\": {\"carrierCode\": \"FDXE\",\"opco\": \"EXPRESS\"},\"serviceType\": \"PRIORITY_OVERNIGHT\",\"packagingType\": \"YOUR_PACKAGING\",\"packageCount\": 0,\"recipient\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"fullName\": \"fedexsender\",\"companyName\": \"infogain\",\"emailId\": \"info@infogain.com\",\"phoneNumber\": \"4365798090\",\"phoneExtension\": \"+021\",\"phoneUsage\": \"PRIMARY\"}},\"specialServicesRequested\": {\"specialServicesRequestedType\":[ \"SIGNATURE_OPTION\"],\"holdAtLocationDetail\": {\"locationId\": \"DREK\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"}}},\"totalInsuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"totalWeight\": {\"unit\": \"LB\",\"value\": \"3\",\"measurementType\": \"MANUAL\"},\"packageLineItems\": [{\"dimensions\": {\"length\": \"1\",\"width\": \"1\",\"height\": \"1\",\"units\": \"IN\"},\"insuredValue\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"specialHandlingDetail\": \"CUSTOMER\",\"specialServicesDetails\": [{\"specialServiceType\": \"SIGNATURE_OPTION\",\"specialServiceSubType\": \"\",\"displayText\": \"ASR\",\"description\": \"Adult signature required\"}],\"weight\": {\"units\": \"LB\",\"value\": \"2\",\"measurementType\": \"MANUAL\"},\"signatureOption\": \"ADULT\",\"trackingId\": {\"trackingNumber\": \"123456789\",\"barcode\": \"11111111111111111111111111111111\",\"sequence\": 1},\"master\": true}],\"shipper\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"fullName\": \"fedexsender\",\"companyName\": \"infogain\",\"emailId\": \"info@infogain.com\",\"phoneNumber\": \"4365798090\",\"phoneExtension\": \"+021\",\"phoneUsage\": \"PRIMARY\"}},\"customer\": {\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"contact\": {\"fullName\": \"fedexsender\",\"companyName\": \"infogain\",\"emailId\": \"info@infogain.com\", \"phoneNumber\": \"4365798090\",\"phoneExtension\": \"+021\",\"phoneUsage\": \"PRIMARY\"}},\"rateDetails\": {\"serviceDetails\": {\"serviceType\": \"SIGNATURE_OPTTION\",\"serviceName\": \"\",\"description\": \"ASR\"},\"deliveryDayOfWeek\": \"FRI\",\"hal\": false,\"commitDetails\": {\"commitTimestamp\": \"2020-08-21T23:59:00\",\"dayOfWeek\": \"FRI\"},\"currency\": \"USD\",\"ratingType\": \"ONE_RATE\",\"totalBaseCharges\": \"1.23\",\"totalRebates\": \"1.23\",\"totalNetCharges\": \"1.23\",\"totalSurcharges\": \"1.23\",\"totalTaxes\": \"1.23\",\"totalFreightDiscounts\": \"1.23\",\"totalNetFreight\": \"1.23\",\"totalNetFedExCharge\": \"1.23\",\"totalDutiesAndTaxes\": \"1.23\",\"totalAncillaryFeesAndTaxes\": \"1.23\",\"totalDutiesTaxesAndFees\": \"1.23\",\"totalNetChargeWithDutiesAndTaxes\": \"1.23\",\"surcharges\": [{\"surchargeType\": \"INSURED_VALUE\",\"description\": \"Declared value\",\"amount\": \"1.23\"}]},\"paymentDetails\": {\"paymentType\": \"ACCOUNT\",\"paymentSource\": \"SPOS\",\"amount\": {\"currency\": \"USD\",\"amount\": \"1.23\"},\"responsibleParty\": \"SENDER\",\"payor\": {\"accountType\": \"FEDEX_EXPRESS\",\"address\": {\"streetLines\": [\"noida\"],\"city\": \"Baltimore\",\"stateOrProvinceCode\": \"MD\",\"postalCode\": \"21218\",\"countryCode\": \"US\",\"addressClassification\": \"HOME\"},\"accountNumber\": \"607410941\",\"creditCard\": {\"maskedCreditCard\": \"40XXXXXXXXXX4011\",\"type\": \"VISA\",\"expirationDate\": \"202024\"}}},\"labelSpecification\": {\"formatType\": \"OPERATIONAL_LABEL\",\"imageType\": \"PNG\",\"stockType\": \"STOCK_4X6\",\"printingOrientation\": \"TOP_EDGE_OF_TEXT_FIRST\"}}}";
        String timeStamp = "2020-07-30T11:55:28.687+05:30";

        OpenShipment openShipment = new OpenShipment();
        openShipment.setOpenShipmentId(referenceId);
        openShipment.setTrackingNumber(trackingId);
        openShipment.setFedexLocationId(locationId);
        openShipment.setCityCenterAccountNumber("033600003");
        openShipment.setDeviceId("SSFE");
        openShipment.setFedexId("3900090");
        openShipment.setInitialAcceptanceTmStmp(timeStamp);
        openShipment.setMeterNumber("033600003");
        openShipment.setOpCoCode("FXO");
        openShipment.setOpCoLocationId("DREK");
        openShipment.setOpenShipmentStatusCode("CREATED");
        openShipment.setOpenShipmentStatusTmStmp(timeStamp);
        openShipment.setPackageAcceptTmStmp(timeStamp);
        openShipment.setShipmentData(shipmentData);
        openShipment.setSoftwareId("SSFE");

        return openShipment;
    }

    /**
     * Mock response for update shipment status.
     */
    @Override
    public boolean updateStatus(
            UpdateOpenShipmentStatusData updateOpenShipmentStatusData) {

        if (!ObjectUtils.isEmpty(updateOpenShipmentStatusData.getReferenceId())
                && !ObjectUtils.isEmpty(updateOpenShipmentStatusData.getLocationId())
                && !ObjectUtils.isEmpty(updateOpenShipmentStatusData.getTrackingId())
                && !ObjectUtils.isEmpty(updateOpenShipmentStatusData.getOpenShipmentStatusCode())) {

            logger.info("Mock: Shipment status updated successfully.");
            return true;
        } else {

            logger.info("Mock: Open shipment record not found for updation.");
            return false;
        }
    }

    @Override
    public OpenShipmentData getOpenShipmentDataFromBlob(
            String shipmentData) {

        // TODO Auto-generated method stub
        return null;
    }

}
