package com.fedex.rscs.validator;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import com.fedex.rscs.dto.HomeDeliveryType;
import com.fedex.rscs.dto.ShipmentSpecialServiceDescription;
import com.fedex.rscs.exception.ErrorCategory;
import com.fedex.rscs.exception.ServiceInputValidationException;
import com.fedex.rscs.exception.ShipmentServiceErrorCode;
import com.fedex.rscs.model.ServiceConstant;

/**
 * This class is used to check the custom input validation at processor layer
 * 
 * @author 3932968
 *
 */
public class RetailShippingDataValidator {

    private RetailShippingDataValidator() {}

    public static void checkBlank(
            String value,
            String fieldName,
            String sectionName) {

        if (StringUtils.isBlank(value)) {
            processMissingFieldException(fieldName, sectionName);
        }
    }

    public static void checkObjectEmpty(
            Object value,
            String fieldName,
            String sectionName) {

        if (ObjectUtils.isEmpty(value)) {
            processMissingFieldException(fieldName, sectionName);
        }
    }


    public static <T> void checkEmpty(
            List<T> value,
            String fieldName,
            String sectionName) {

        if (ObjectUtils.isEmpty(value)) {
            processMissingFieldException(fieldName, sectionName);
        }
    }

    public static void checkInvalidLength(
            String value,
            String fieldName,
            String sectionName,
            int length) {

        if (value != null && value.length() > length) {
            processInvalidLengthException(fieldName, sectionName);
        }
    }

    public static void validateDeliveryDate(
            String value,
            String fieldName,
            String sectionName,
            String dateFormat) {

        try {
            LocalDate.parse(value, DateTimeFormatter.ofPattern(dateFormat)
                    .withResolverStyle(ResolverStyle.STRICT));
        } catch (DateTimeParseException ex) {
            processInvalidDateException(fieldName, sectionName);
        }
    }

    public static void validateSpecialService(
            ShipmentSpecialServiceDescription specialServicesRequested) {

        if (specialServicesRequested != null && !ObjectUtils.isEmpty(specialServicesRequested.getSpecialServices())
                && specialServicesRequested.getSpecialServices()
                        .stream()
                        .anyMatch(specialService -> specialService.getSpecialServiceType()
                                .equalsIgnoreCase(ServiceConstant.SPECIAL_SRVC_TYPE_HD))) {
            validateHomeDeliveryDetails(specialServicesRequested);
        }
    }

    private static void validateHomeDeliveryDetails(
            ShipmentSpecialServiceDescription specialServicesRequested) {

        RetailShippingDataValidator.checkObjectEmpty(specialServicesRequested.getHomeDeliveryDetail(),
                ServiceConstant.HOME_DELIVERY_DETAIL, ServiceConstant.SPECIAL_SRVC_REQ);

        // condition to validate if delivery date is of format YYYY-MM-DD in case of
        // DATE_CERTAIN delivery type.
        if (specialServicesRequested.getHomeDeliveryDetail()
                .getHomeDeliveryType()
                .equals(HomeDeliveryType.DATE_CERTAIN)) {

            RetailShippingDataValidator.checkBlank(specialServicesRequested.getHomeDeliveryDetail()
                    .getDeliveryDate(), ServiceConstant.DELIVERY_DATE, ServiceConstant.HOME_DELIVERY_DETAIL);

            RetailShippingDataValidator.validateDeliveryDate(specialServicesRequested.getHomeDeliveryDetail()
                    .getDeliveryDate(), ServiceConstant.DELIVERY_DATE, ServiceConstant.HOME_DELIVERY_DETAIL,
                    ServiceConstant.DELIVERY_DATE_FORMAT);
        }

    }

    private static void processMissingFieldException(
            String fieldName,
            String sectionName) {

        String code = sectionName.toUpperCase() + "." + fieldName.toUpperCase() + ".MISSING.PROPERTY";
        String errorMessage = "The " + fieldName + " is missing.";
        throw new ServiceInputValidationException(
                new ShipmentServiceErrorCode(code, errorMessage, ErrorCategory.BAD_REQUEST));
    }

    private static void processInvalidLengthException(
            String fieldName,
            String sectionName) {

        String code = sectionName.toUpperCase() + "." + fieldName.toUpperCase() + ".INVALID.LENGTH";
        String errorMessage = "The length of " + fieldName + " is invalid.";
        throw new ServiceInputValidationException(
                new ShipmentServiceErrorCode(code, errorMessage, ErrorCategory.BAD_REQUEST));
    }

    private static void processInvalidDateException(
            String fieldName,
            String sectionName) {

        String code = sectionName.toUpperCase() + "." + fieldName.toUpperCase() + ".INVALID";
        String errorMessage = "The " + fieldName + " is invalid.";
        throw new ServiceInputValidationException(
                new ShipmentServiceErrorCode(code, errorMessage, ErrorCategory.BAD_REQUEST));
    }
}
