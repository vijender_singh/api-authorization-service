package com.fedex.rscs.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import com.fedex.nxgen.ship.v17.ientities.DangerousGoodsDetail;
import com.fedex.nxgen.ship.v17.ientities.PackageSpecialServicesRequested;
import com.fedex.nxgen.ship.v17.ientities.ShippingDocument;
import com.fedex.nxgen.ship.v17.ientities.ShippingDocumentPart;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.client.ClientTransientException;
import com.fedex.rscs.model.RequestedPackageLineItemDetail;
import com.fedex.rscs.model.ServiceType;
import com.fedex.rscs.model.ShippingDocumentType;
import com.fedex.rscs.model.SpecialServiceDescriptionDetail;
import com.fedex.rscs.model.SpecialServiceDetail;
import com.fedex.rscs.model.cshp.SpecialServiceCode;


@Component
public class RetailShipmentCreationServiceUtil {

    private static final Logger logger = LoggerFactory.getLogger(RetailShipmentCreationServiceUtil.class);

    private static final String KEY_DELIMITER = "-";

    /**
     * Used to create customer transaction Id.
     * 
     * @param requestedDateTime
     * @param location
     * @return
     */
    public String getCustomerTransactionId(
            final String appId,
            final String requestedDateTime,
            final String location) {

        return (appId + KEY_DELIMITER + location + KEY_DELIMITER + requestedDateTime);

    }


    /**
     * Method to parse the {@link ShippingDocument} to decode and retrieve the complete label buffer
     * 
     * @param shippingDocument
     * @param shippingDocumentType
     * @return
     */
    public String decodePackageDocument(
            final ShippingDocument shippingDocument,
            ShippingDocumentType shippingDocumentType) {

        logger.debug("Decoding the image label buffer...");

        String documentImage = null;
        if (shippingDocument != null && shippingDocumentType.name()
                .equalsIgnoreCase(shippingDocument.getType()) && !ObjectUtils.isEmpty(shippingDocument.getParts())) {

            try (final ByteArrayOutputStream documentStream = new ByteArrayOutputStream()) {

                for (ShippingDocumentPart documentPart : shippingDocument.getParts()) {
                    if (!ObjectUtils.isEmpty(documentPart.getImage())) {
                        documentStream.write(documentPart.getImage(), 0, documentPart.getImage().length);
                    }
                }

                // Decoding byte array to String
                documentImage = Base64.getEncoder()
                        .encodeToString(documentStream.toByteArray());

            } catch (IOException e) {
                logger.error("Failed to decode the ShippingDocumentPart due to ", e);
                throw new ClientTransientException(ServiceErrorCode.INTERNAL_SERVER_ERROR);
            }

        }
        return documentImage;
    }

    /**
     * Used to create ApiClientId .
     * 
     * @param requestedDateTime
     * @param location
     * @return
     */
    public String getApiClientId(
            final String appName,
            final String apiVersionId) {

        return (appName + KEY_DELIMITER + apiVersionId);

    }

    /**
     * Use to return special service code
     * 
     * @param specialServicesList
     * @return
     */
    public String getSpecialServiceCode(
            SpecialServiceDetail specialServicesList) {

        if (specialServicesList.getSpecialServiceType()
                .equals("SIGNATURE_OPTION")) {

            return SpecialServiceCode.valueOf(specialServicesList.getSpecialServiceSubType())
                    .getSpecialServiceCode();
        }
        return SpecialServiceCode.valueOf(specialServicesList.getSpecialServiceType())
                .getSpecialServiceCode();

    }

    /**
     * Use to update service type when selected service is GROUND or HOME DELEVERY with BATTRY
     * 
     * @param requestedPackageLineItem
     * @param serviceType
     * @return
     */
    public PackageSpecialServicesRequested createPackageSpecialService(
            RequestedPackageLineItemDetail requestedPackageLineItem,
            String serviceType) {

        PackageSpecialServicesRequested packSpeService = new PackageSpecialServicesRequested();
        List<SpecialServiceDescriptionDetail> packageSpecialServices = new ArrayList<>();
        packageSpecialServices.addAll(requestedPackageLineItem.getPackageSpecialServicesRequested()
                .getSpecialServicesRequested());
        if (ServiceType.FEDEX_GROUND.name()
                .equalsIgnoreCase(serviceType)
                || ServiceType.GROUND_HOME_DELIVERY.name()
                        .equalsIgnoreCase(serviceType)) {
            boolean isremoved = packageSpecialServices.removeIf(element -> element.getSpecialServiceType()
                    .equalsIgnoreCase(SpecialServiceCode.BATTERY.getType()));
            if (isremoved) {
                packageSpecialServices.add(new SpecialServiceDescriptionDetail(
                        SpecialServiceCode.DANGEROUS_GOODS.getType(), null, null, null));
                DangerousGoodsDetail dgDetail = new DangerousGoodsDetail();
                dgDetail.setOptions(new String[] {SpecialServiceCode.BATTERY.getType()});
                packSpeService.setDangerousGoodsDetail(dgDetail);
            }
        }
        packSpeService.setSpecialServiceTypes(createSpecialServiceType(packageSpecialServices));
        return packSpeService;
    }

    /**
     * This method is used to return SpecialServiceType
     * 
     * @param list
     * @return String[]
     */
    private String[] createSpecialServiceType(
            List<SpecialServiceDescriptionDetail> list) {

        if (!CollectionUtils.isEmpty(list)) {
            List<String> serviceTypes = list.stream()
                    .map(SpecialServiceDescriptionDetail::getSpecialServiceType)
                    .collect(Collectors.toList());
            return serviceTypes.stream()
                    .toArray(String[]::new);
        }
        return new String[] {};
    }
}
