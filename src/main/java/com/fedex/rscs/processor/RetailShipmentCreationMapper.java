package com.fedex.rscs.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.util.CollectionUtils;

import com.fedex.rscs.client.srrs.dto.CarrierCode;
import com.fedex.rscs.dto.Address;
import com.fedex.rscs.dto.AddressClassification;
import com.fedex.rscs.dto.CarrierCodeType;
import com.fedex.rscs.dto.ConfirmShipmentRequestData;
import com.fedex.rscs.dto.ConfirmedShipmentResource;
import com.fedex.rscs.dto.CreateOpenShipmentRequest;
import com.fedex.rscs.dto.CreateOpenShipmentResponse;
import com.fedex.rscs.dto.CustomerContact;
import com.fedex.rscs.dto.LocationDetail;
import com.fedex.rscs.dto.LocationInfo;
import com.fedex.rscs.dto.PersonName;
import com.fedex.rscs.dto.Price;
import com.fedex.rscs.dto.RequestHeaderDetail;
import com.fedex.rscs.dto.RequestedPackageLineItem;
import com.fedex.rscs.dto.ShipmentTransactionDetail;
import com.fedex.rscs.dto.ShippingContact;
import com.fedex.rscs.dto.SoldShipmentResource;
import com.fedex.rscs.dto.SoldShipmentSummaryResource;
import com.fedex.rscs.dto.SpecialServiceDescription;
import com.fedex.rscs.dto.Weight;
import com.fedex.rscs.model.AddressDetail;
import com.fedex.rscs.model.ContactDetail;
import com.fedex.rscs.model.LocationContextDetail;
import com.fedex.rscs.model.RequestedPackageLineItemDetail;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.SoldShipmentDetail;
import com.fedex.rscs.model.SoldShipmentSummary;
import com.fedex.rscs.model.WeightDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.common.LocationDataDetail;
import com.fedex.rscs.model.common.PersonDetail;
import com.fedex.rscs.model.common.TransactionHeaderDetail;
import com.fedex.rscs.model.cshp.ConfirmedShipmentData;
import com.fedex.rscs.model.cshp.OpenShipmentResponse;

/**
 * Map struct mapper to map the DTO to Model and vice-versa on Processor Layer
 *
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RetailShipmentCreationMapper {

    RetailShipmentCreationMapper INSTANCE = Mappers.getMapper(RetailShipmentCreationMapper.class);

    /**
     * Mapper to convert CreateOpenShipmentRequest to RequestedShipmentDetail
     * 
     * @param createOpenShipmentRequest
     * @return
     */
    @Mappings({
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.shipper.address", target = "sender.address"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.recipient.address", target = "recipient.address"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.customer.address", target = "customer.address"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.shipper.contact", target = "sender.contact"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.recipient.contact", target = "recipient.contact"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.customer.contact", target = "customer.contact"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.specialServicesRequested.specialServices", target = "specialServicesRequested.specialServicesRequestedType"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.serviceType", target = "serviceType"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.packagingType", target = "packagingType"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.requestedPackageLineItems", target = "packageLineItems"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.specialServicesRequested.homeDeliveryDetail", target = "specialServicesRequested.homeDeliveryDetail"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.specialServicesRequested.holdAtLocationDetail.address", target = "specialServicesRequested.holdAtLocationDetail.address"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.specialServicesRequested.holdAtLocationDetail.locationId", target = "specialServicesRequested.holdAtLocationDetail.locationId"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.specialServicesRequested.holdAtLocationDetail.company.name", target = "specialServicesRequested.holdAtLocationDetail.contact.companyName"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.specialServicesRequested.holdAtLocationDetail.locationType", target = "specialServicesRequested.holdAtLocationDetail.locationType"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.specialServicesRequested.holdAtLocationDetail.displayName", target = "specialServicesRequested.holdAtLocationDetail.displayName"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.specialServicesRequested.holdAtLocationDetail.phoneNumber.phoneNumber.number", target = "specialServicesRequested.holdAtLocationDetail.contact.phoneNumber"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.specialServicesRequested.holdAtLocationDetail.phoneNumber.phoneNumber.extension", target = "specialServicesRequested.holdAtLocationDetail.contact.phoneExtension"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.specialServicesRequested.holdAtLocationDetail.phoneNumber.usage", target = "specialServicesRequested.holdAtLocationDetail.contact.phoneUsage"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.locationDetails.transactionLocationInfo.address", target = "origin.address"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.locationDetails.cityCenterAccountNumber", target = "origin.accountNumber"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.labelSpecification", target = "labelSpecification"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.interlineShippingDetails", target = "interlineShippingData")})

    RequestedShipmentDetail toModel(
            CreateOpenShipmentRequest createRequest);

    /**
     * to update address
     * 
     * @param address
     * @param addressDetail
     */
    void updateAddress(
            Address address,
            @MappingTarget AddressDetail addressDetail);

    /**
     * to update sender addressClassification
     * 
     * @param createRequest
     * @param requestedShipmentDetail
     */
    @Mappings({
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.shipper.address.addressClassification", target = "sender.address.residential")})
    void updateSenderAddressClassification(
            CreateOpenShipmentRequest createRequest,
            @MappingTarget RequestedShipmentDetail requestedShipmentDetail);

    /**
     * to update recipient addressClassification
     * 
     * @param createRequest
     * @param requestedShipmentDetail
     */
    @Mappings({
            @Mapping(source = "createRequest.createOpenShipmentRequest.requestedShipment.recipient.address.addressClassification", target = "recipient.address.residential")})
    void updateRecepientAddressClassification(
            CreateOpenShipmentRequest createRequest,
            @MappingTarget RequestedShipmentDetail requestedShipmentDetail);
    
    /**
     * to update customer addressClassification
     * 
     * @param createRequest
     * @param requestedShipmentDetail
     */
    @Mappings({
            @Mapping(source = "createRequest.createOpenShipmentRequest.customer.address.addressClassification", target = "customer.address.residential")})
    void updateCustomerAddressClassification(
            CreateOpenShipmentRequest createRequest,
            @MappingTarget RequestedShipmentDetail requestedShipmentDetail);

    /**
     * to update transaction code and express after pickup
     * 
     * @param createRequest
     * @param requestedShipmentDetail
     */
    @Mappings({
            @Mapping(source = "createRequest.createOpenShipmentRequest.locationDetails.transactionLocationInfo.code", target = "transactionLocationCode"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.locationDetails.expressAfterPickup", target = "expressAfterPickup"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.locationDetails.groundAfterPickup", target = "requestedShipmentDetail.groundAfterPickup")})
    void updateTransactionCodeAndExpressAfterPickup(
            CreateOpenShipmentRequest createRequest,
            @MappingTarget RequestedShipmentDetail requestedShipmentDetail);

    /**
     * to update requested date
     * 
     * @param createRequest
     * @param requestedShipmentDetail
     */
    @Mappings({
            @Mapping(source = "createRequest.createOpenShipmentRequest.headerDetails.requestDateTime", target = "shipTimestamp")})
    void updateRequestedDate(
            CreateOpenShipmentRequest createRequest,
            @MappingTarget RequestedShipmentDetail requestedShipmentDetail);

    /**
     * to update Shipment Payment Details
     * 
     * @param createRequest
     * @param requestedShipmentDetail
     */
    @Mappings({
            @Mapping(source = "createRequest.createOpenShipmentRequest.paymentOption.paymentType", target = "requestedShipmentDetail.shippingChargesPayment.paymentType"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.paymentOption.responsibleParty", target = "requestedShipmentDetail.shippingChargesPayment.responsibleParty"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.paymentOption.accountNumber", target = "requestedShipmentDetail.shippingChargesPayment.payor.accountNumber"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.paymentOption.interlineId", target = "requestedShipmentDetail.shippingChargesPayment.interlineId")})
    void updateShippingPaymentDetails(
            CreateOpenShipmentRequest createRequest,
            @MappingTarget RequestedShipmentDetail requestedShipmentDetail);

    /**
     * Method to assert address classification is residence or not
     * 
     * @param classification
     * @return
     */
    default boolean isResidence(
            AddressClassification classification) {

        return (classification != null && AddressClassification.HOME.equals(classification));
    }

    /**
     * Method to get special service type from SpecialServiceDescription object
     * 
     * @param specialServiceList
     * @return
     */
    default List<String> specialServicesRequestedType(
            List<SpecialServiceDescription> specialServices) {

        if (CollectionUtils.isEmpty(specialServices)) {
            return null;
        }
        return specialServices.stream()
                .map(SpecialServiceDescription::getSpecialServiceType)
                .collect(Collectors.toList());
    }

    /**
     * Mapper to convert OpenShipmentResponse to CreateOpenShipmentResponse object
     * 
     * @param createOpenShipmentResponse
     * @return
     */
    @Mappings({@Mapping(source = "openShipmentResponse.completeShipmentDetail", target = "openShipment"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.operationalDetail.destinationLocationId", target = "openShipment.locationId"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.index", target = "openShipment.referenceId"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.carrierCode", target = "openShipment.carrierDetails.carrierCode"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.serviceDescription.type", target = "openShipment.serviceType"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.packagingDescription.type", target = "openShipment.packagingType"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.recipient.address.residential", target = "openShipment.recipient.address.addressClassification"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.recipient.contact.fullName", target = "openShipment.recipient.contact.personName"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.recipient.contact.companyName", target = "openShipment.recipient.contact.company.name"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.recipient.contact.emailId", target = "openShipment.recipient.contact.emailDetails.emailAddress"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.specialServicesRequested.specialServicesRequestedType", target = "openShipment.specialServicesRequested.specialServices"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.completedPackageDetails", target = "openShipment.packageLineItems"),
            @Mapping(source = "openShipmentResponse.shippingRateData", target = "openShipment.rateDetails"),
            @Mapping(source = "openShipmentResponse.shippingRateData.commitDetails", target = "openShipment.commitDetails"),
            @Mapping(source = "openShipmentResponse.shippingPaymentData", target = "openShipment.paymentDetails"),
            @Mapping(source = "openShipmentResponse.shippingPaymentData.accountType", target = "openShipment.paymentDetails.payor.accountType"),
            @Mapping(source = "openShipmentResponse.shippingPaymentData.accountNumber", target = "openShipment.paymentDetails.payor.accountNumber"),
            @Mapping(source = "openShipmentResponse.shippingPaymentData.maskedCreditCard", target = "openShipment.paymentDetails.payor.creditCard.maskedCreditCard"),
            @Mapping(source = "openShipmentResponse.shippingPaymentData.type", target = "openShipment.paymentDetails.payor.creditCard.type"),
            @Mapping(source = "openShipmentResponse.shippingPaymentData.expirationDate", target = "openShipment.paymentDetails.payor.creditCard.expirationDate"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.specialServicesRequested.holdAtLocationDetail.contact.companyName", target = "openShipment.specialServicesRequested.holdAtLocationDetail.company.name"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.specialServicesRequested.holdAtLocationDetail.contact.phoneNumber", target = "openShipment.specialServicesRequested.holdAtLocationDetail.phoneNumber.phoneNumber.number"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.specialServicesRequested.holdAtLocationDetail.contact.phoneExtension", target = "openShipment.specialServicesRequested.holdAtLocationDetail.phoneNumber.phoneNumber.extension"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.specialServicesRequested.holdAtLocationDetail.displayName", target = "openShipment.specialServicesRequested.holdAtLocationDetail.displayName"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.specialServicesRequested.holdAtLocationDetail.locationType", target = "openShipment.specialServicesRequested.holdAtLocationDetail.locationType"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.specialServicesRequested.holdAtLocationDetail.contact.phoneUsage", target = "openShipment.specialServicesRequested.holdAtLocationDetail.phoneNumber.usage"),
            @Mapping(source = "openShipmentResponse.completeShipmentDetail.totalWeight.unit", target = "openShipment.totalWeight.units")})

    CreateOpenShipmentResponse toDto(
            OpenShipmentResponse openShipmentResponse);

    /**
     * Mapper to convert WorkstationDetails to WorkstationDetail object
     * 
     * @param workstationDetails
     * @return
     */
    @Mappings({
            @Mapping(source = "createRequest.createOpenShipmentRequest.headerDetails.workstationDetails.meterNumber", target = "workstationDetail.meterNumber"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.headerDetails.workstationDetails.softwareId", target = "workstationDetail.softwareId"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.locationDetails.cityCenterAccountNumber", target = "workstationDetail.accountNumber"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.headerDetails.workstationDetails.appName", target = "workstationDetail.appName"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.headerDetails.workstationDetails.appVersionId", target = "workstationDetail.appVersionId"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.headerDetails.workstationDetails.deviceId", target = "workstationDetail.deviceId"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.headerDetails.guid", target = "workstationDetail.guid"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.headerDetails.teamMemberId", target = "workstationDetail.teamMemberId"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.locationDetails.groundAccountNumber", target = "workstationDetail.groundAccountNumber")})
    void updateWorkstationDetail(
            CreateOpenShipmentRequest createRequest,
            @MappingTarget WorkstationDetail workstationDetail);

    /**
     * Mapper to convert RequestedPackageLineItems to RequestedPackageLineItemDetail object
     * 
     * @param requestedPackageLineItems
     * @return
     */
    @Mappings({
            @Mapping(source = "requestedPackageLineItems.batteryClassifications", target = "packageSpecialServicesRequested.batteryClassifications"),
            @Mapping(source = "requestedPackageLineItems.dryIceWeight.units", target = "packageSpecialServicesRequested.dryIceWeightDetails.unit"),
            @Mapping(source = "requestedPackageLineItems.dryIceWeight.value", target = "packageSpecialServicesRequested.dryIceWeightDetails.value"),
            @Mapping(source = "requestedPackageLineItems.dryIceWeight.measurementType", target = "packageSpecialServicesRequested.dryIceWeightDetails.measurementType")})
    RequestedPackageLineItemDetail toModel(
            RequestedPackageLineItem requestedPackageLineItems);

    /**
     * to update origin address
     * 
     * @param mainOpenShipmentRequest
     * @param requestedShipmentDetail
     */
    @Mappings({
            @Mapping(source = "createRequest.createOpenShipmentRequest.locationDetails.transactionLocationInfo.address", target = "requestedShipmentDetail.origin.address"),
            @Mapping(source = "createRequest.createOpenShipmentRequest.locationDetails.transactionLocationInfo.address.addressClassification", target = "requestedShipmentDetail.origin.address.residential"),})
    void updateOriginAddress(
            CreateOpenShipmentRequest createRequest,
            @MappingTarget RequestedShipmentDetail requestedShipmentDetail);

    /**
     * Convert to ShipmentConfirmationRequest
     * 
     * @param confirmShipmentRequest
     * @param details
     * @return
     */
    @Mappings({@Mapping(source = "details.cityCenterAccountNumber", target = "workstationDetail.accountNumber"),
            @Mapping(source = "details.btcMeterNumber", target = "workstationDetail.meterNumber"),
            @Mapping(source = "confirmShipmentRequest.headerDetails.workstationDetails.softwareId", target = "workstationDetail.softwareId"),
            @Mapping(source = "confirmShipmentRequest.headerDetails.workstationDetails.appName", target = "workstationDetail.appName"),
            @Mapping(source = "confirmShipmentRequest.headerDetails.workstationDetails.appVersionId", target = "workstationDetail.appVersionId"),
            @Mapping(source = "confirmShipmentRequest.headerDetails.workstationDetails.deviceId", target = "workstationDetail.deviceId"),
            @Mapping(source = "confirmShipmentRequest.headerDetails.teamMemberId", target = "workstationDetail.teamMemberId"),
            @Mapping(source = "confirmShipmentRequest.headerDetails.guid", target = "workstationDetail.guid"),
            @Mapping(source = "confirmShipmentRequest.headerDetails.requestDateTime", target = "shipmentTimeStamp"),
            @Mapping(source = "confirmShipmentRequest.requestedShipment.referenceId", target = "referenceId"),
            @Mapping(source = "confirmShipmentRequest.requestedShipment.masterTrackingId.trackingNumber", target = "trackingId"),
            @Mapping(source = "confirmShipmentRequest.locationDetails.transactionLocationInfo.code", target = "transactionLocationId"),
            @Mapping(source = "confirmShipmentRequest.labelSpecification", target = "labelSpecificationDetail"),
            @Mapping(source = "confirmShipmentRequest.paymentDetails", target = "paymentDetail"),
            @Mapping(source = "details", target = "locationDataDetail")})
    ShipmentConfirmationRequest toModel(
            ConfirmShipmentRequestData confirmShipmentRequest,
            LocationContextDetail details);

    /**
     * Convert to ShipmentTransactionDetail
     * 
     * @param shipmentData
     * @return
     */
    @Mappings({@Mapping(source = "shipmentData.masterTrackingId", target = "masterTrackingId"),
            @Mapping(source = "shipmentData.packageCount", target = "packageCount"),
            @Mapping(source = "shipmentData.packageLineItems", target = "packageLineItems"),
            @Mapping(source = "shipmentData.carrierCode", target = "carrierDetails.carrierCode"),
            @Mapping(source = "shipmentData.carrierType", target = "carrierDetails.carrierType"),
            @Mapping(source = "shipmentData.notifications", target = "packageNotifications")})

    ShipmentTransactionDetail toDto(
            ConfirmedShipmentData shipmentData);

    /**
     * Method to set the CarrierCodeType
     * 
     * @param type
     * @return
     */
    default CarrierCodeType getCarrierCodeType(
            String type) {

        if (StringUtils.isNotEmpty(type)) {
            return CarrierCodeType.valueOf(type);
        }
        return null;
    }

    /**
     * Method to set the CarrierCode
     * 
     * @param type
     * @return
     */
    default CarrierCode getCarrierCode(
            String type) {

        if (StringUtils.isNotEmpty(type)) {
            return CarrierCode.valueOf(type);
        }
        return null;
    }

    /**
     * Method to assert address classification is residence or not
     * 
     * @param isResidential
     * @return
     */
    default AddressClassification getAddressClassification(
            boolean isResidential) {

        if (isResidential) {
            return AddressClassification.HOME;
        }
        return AddressClassification.BUSINESS;
    }

    /**
     * To get value from Double object
     * 
     * @param totalInsuredValue
     * @return
     */
    default Price doubleToPrice(
            Double totalInsuredValue) {

        if (totalInsuredValue == null) {
            return null;
        }
        Price price = new Price();
        price.setAmount(totalInsuredValue);
        return price;
    }

    /**
     * To get value from Double object
     * 
     * @param totalWeight
     * @return
     */
    default Weight doubleToWeight(
            Double totalWeight) {

        if (totalWeight == null) {
            return null;
        }
        Weight weight = new Weight();
        weight.setValue(totalWeight.toString());
        return weight;
    }

    /**
     * Method to get special service type from String object
     * 
     * @param specialServicesRequestedType
     * @return
     */
    default List<SpecialServiceDescription> getSpecialServices(
            List<String> specialServicesRequestedType) {

        if (CollectionUtils.isEmpty(specialServicesRequestedType)) {
            return null;
        }
        List<SpecialServiceDescription> specialServices = new ArrayList<>();
        SpecialServiceDescription special = new SpecialServiceDescription();

        for (String services : specialServicesRequestedType) {
            special.setSpecialServiceType(services);
            specialServices.add(special);
        }
        return specialServices;
    }

    /**
     * To convert enum into string
     * 
     * @param custEnum
     * @return
     */
    default String enumToString(
            Enum<?> custEnum) {

        if (custEnum == null) {
            return null;
        }
        return custEnum.toString();
    }

    /**
     * To convert Double into string
     * 
     * @param amount
     * @return
     */
    default String doubleToString(
            Double amount) {

        if (amount == null) {
            return null;
        }
        return String.valueOf(amount);
    }

    /**
     * To convert String into Double
     * 
     * @param amount
     * @return
     */
    default Double stringToDouble(
            String amount) {

        if (amount == null) {
            return null;
        }
        return new Double(amount);
    }

    /**
     * To convert Weight to WeightDetail
     * 
     * @param weight
     * @return
     */
    default WeightDetail weightToWeightDetail(
            Weight weight) {

        if (weight == null) {
            return null;
        }

        WeightDetail weightDetail = new WeightDetail();

        weightDetail.setValue(weight.getValue());
        weightDetail.setUnit(weight.getUnits()
                .toString());
        if (weight.getMeasurementType() != null) {
            weightDetail.setMeasurementType(weight.getMeasurementType()
                    .toString());
        }

        return weightDetail;
    }

    /**
     * Method to convert Address Detail to Address
     * 
     * @param address
     * @param addressDetail
     */
    void updateOriginAddress(
            Address address,
            @MappingTarget AddressDetail addressDetail);
    
    /**
     * to update contact
     * 
     * @param contact
     * @param contactDetail
     */
    @Mappings({@Mapping(source = "contact.company.name", target = "companyName"),
            @Mapping(source = "contact.personName", target = "fullName"),
            @Mapping(source = "contact.emailDetails.emailAddress", target = "emailId")})

    void updateContact(
            ShippingContact contact,
            @MappingTarget ContactDetail contactDetail);
    
    /**
     * It will convert locaionDetail Dto to Model
     * 
     * @param locationDetail
     * @param details
     */
    @Mappings({@Mapping(source = "details.cityCenterAccountNumber", target = "cityCenterAccountNumber"),
            @Mapping(source = "details.groundAccountNumber", target = "groundAccountNumber"),
            @Mapping(source = "details.opCo", target = "opCo"),
            @Mapping(source = "details.opCoLocationId", target = "opCoLocationId")})
    LocationDataDetail toLocationDetail(
            LocationDetail locationDetail,
            LocationContextDetail details);
    
    /**
     * This method converts TransactionHeaderDetail dto to model
     * 
     * @param headerDetails
     * @param details
     * @return
     */
    @Mappings({@Mapping(source = "details.btcMeterNumber", target = "workstationDetails.meterNumber"),
            @Mapping(source = "headerDetails.workstationDetails.deviceId", target = "workstationDetails.deviceId"),
            @Mapping(source = "headerDetails.workstationDetails.softwareId", target = "workstationDetails.softwareId"),
            @Mapping(source = "headerDetails.workstationDetails.appName", target = "workstationDetails.appName"),
            @Mapping(source = "headerDetails.workstationDetails.appVersionId", target = "workstationDetails.appVersionId")})
    TransactionHeaderDetail toHeaderDetails(
            RequestHeaderDetail headerDetails,
            LocationContextDetail details);

    @Mappings({@Mapping(source = "carrierDetails.opco", target = "carrierDetails.carrierType")})
    SoldShipmentResource toDto(
            SoldShipmentDetail soldShipmentResourceDetail);

    /**
     * This method converts SoldShipmentSummary model to SoldShipmentSummaryResource dto
     * 
     * @param soldShipmentSummaryeDetail
     * @return
     */
    @Mappings({@Mapping(source = "carrierDetails.opco", target = "carrierDetails.carrierType")})
    SoldShipmentSummaryResource toDto(
            SoldShipmentSummary soldShipmentSummaryeDetail);

    /**
     * to update contact
     * 
     * @param contact
     * @param shippingContact
     */
    @Mappings({@Mapping(source = "contact.companyName", target = "company.name"),
            @Mapping(source = "contact.fullName", target = "personName"),
            @Mapping(source = "contact.emailId", target = "emailDetails.emailAddress")})

    void updateContact(
            ContactDetail contact,
            @MappingTarget ShippingContact shippingContact);

    /**
     * It will convert locationInfo Dto to Model
     * 
     * @param LocationInfo
     * @param LocationContextDetail
     */
    @Mappings({@Mapping(source = "locationData.cityCenterAccountNumber", target = "cityCenterAccountNumber"),
            @Mapping(source = "locationData.groundAccountNumber", target = "groundAccountNumber"),
            @Mapping(source = "locationData.opCo", target = "opCo"),
            @Mapping(source = "locationData.opCoLocationId", target = "opCoLocationId"),
            @Mapping(source = "locationDetails.code", target = "transactionLocationInfo.code"),
            @Mapping(source = "locationDetails.address", target = "transactionLocationInfo.address"),
            @Mapping(source = "locationDetails.code", target = "servingLocationInfo.code"),
            @Mapping(source = "locationDetails.address", target = "servingLocationInfo.address")})
    LocationDataDetail toLocationModel(
            LocationInfo locationDetails,
            LocationContextDetail locationData);

    /**
     * Mapper to convert PersonName to PersonDetail
     * 
     * @param customerName
     * @return PersonDetail
     */
    PersonDetail toPersonModel(
            PersonName customerName);

    /**
     * Mapper to convert headerDetails to WorkstationDetail
     * 
     * @param headerDetails
     * @param locationData
     * @return WorkstationDetail
     */
    @Mappings({@Mapping(source = "locationData.cityCenterAccountNumber", target = "accountNumber"),
            @Mapping(source = "locationData.groundAccountNumber", target = "groundAccountNumber"),
            @Mapping(source = "locationData.btcMeterNumber", target = "meterNumber"),
            @Mapping(source = "headerDetails.workstationDetails.deviceId", target = "deviceId"),
            @Mapping(source = "headerDetails.workstationDetails.softwareId", target = "softwareId"),
            @Mapping(source = "headerDetails.workstationDetails.appName", target = "appName"),
            @Mapping(source = "headerDetails.workstationDetails.appVersionId", target = "appVersionId"),
            @Mapping(source = "headerDetails.guid", target = "guid"),
            @Mapping(source = "headerDetails.teamMemberId", target = "teamMemberId")})
    WorkstationDetail toWorkstationDetail(
            RequestHeaderDetail headerDetails,
            LocationContextDetail locationData);
    
    /**
     * to update addressClassification
     * 
     * @param address
     * @param addressDetail
     */
    @Mapping(source = "addressClassification", target = "residential")
    void updateAddressClassification(
            Address address,
            @MappingTarget AddressDetail addressDetail);
    
    /**
     * This method will update the location data details using data retrieved from location service and
     * request DTOs
     * 
     * @param locationDataDetail
     * 
     * @param LocationDetail
     */
    @Mapping(source = "locationDetails.transactionLocationInfo", target = "transactionLocationInfo")
    @Mapping(source = "locationDetails.servingLocationInfo", target = "servingLocationInfo")
    @Mapping(source = "locationDataDetail.cityCenterAccountNumber", target = "cityCenterAccountNumber")
    @Mapping(source = "locationDataDetail.groundAccountNumber", target = "groundAccountNumber")
    @Mapping(source = "locationDetails.groundAfterPickup", target = "groundAfterPickup")
    @Mapping(source = "locationDetails.expressAfterPickup", target = "expressAfterPickup")
    @Mapping(source = "locationDataDetail.opCo", target = "opCo")
    @Mapping(source = "locationDataDetail.opCoLocationId", target = "opCoLocationId")
    LocationDataDetail updateLocationDataDetails(
            LocationDetail locationDetails,
            LocationDataDetail locationDataDetail);
    
    /**
     * Mapper to convert ShipmentTransactionDetail to ConfirmedShipmentResource object
     * 
     * @param transactionDetail
     * @return
     */
    public ConfirmedShipmentResource toDto(
            ShipmentTransactionDetail transactionDetail);
    
    /**
     * to update contact
     * 
     * @param contact
     * @param contactDetail
     */
    @Mappings({@Mapping(source = "contact.company.name", target = "companyName"),
            @Mapping(source = "contact.personName", target = "fullName"),
            @Mapping(source = "contact.emailDetails.emailAddress", target = "emailId"),
            @Mapping(source = "contact.emailDetails.alternateEmailAddress", target = "alternateEmailId")})
    void updateCustomerContact(
            CustomerContact contact,
            @MappingTarget ContactDetail contactDetail);

    /**
     * Convert to ConfirmedShipmentResource
     * 
     * @param shipmentData
     * @return
     */
    @Mapping(source = "shipmentData.masterTrackingId", target = "masterTrackingId")
    @Mapping(source = "shipmentData.packageCount", target = "packageCount")
    @Mapping(source = "shipmentData.packageLineItems", target = "packageLineItems")
    @Mapping(source = "shipmentData.carrierCode", target = "carrierDetails.carrierCode")
    @Mapping(source = "shipmentData.carrierType", target = "carrierDetails.carrierType")
    ConfirmedShipmentResource toConfirmedShipmentResource(
            ConfirmedShipmentData shipmentData);

}


