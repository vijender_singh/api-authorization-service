package com.fedex.rscs.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import com.fedex.common.cxs.dto.CXSAlert;
import com.fedex.common.cxs.dto.CXSAlert.AlertType;
import com.fedex.rscs.dto.CarrierCodeType;
import com.fedex.rscs.dto.CarrierType;
import com.fedex.rscs.dto.ConfirmShipmentRequestData;
import com.fedex.rscs.dto.ConfirmedShipmentResource;
import com.fedex.rscs.dto.CreateOpenShipmentRequest;
import com.fedex.rscs.dto.CreateOpenShipmentResponse;
import com.fedex.rscs.dto.DeleteSoldShipmentRequestData;
import com.fedex.rscs.dto.DeletedSoldShipmentResource;
import com.fedex.rscs.dto.DeletedSoldShipmentResponse;
import com.fedex.rscs.dto.HomeDeliveryType;
import com.fedex.rscs.dto.LocationDetail;
import com.fedex.rscs.dto.LocationInfo;
import com.fedex.rscs.dto.OpenShipmentPackageLineItem;
import com.fedex.rscs.dto.Party;
import com.fedex.rscs.dto.Person;
import com.fedex.rscs.dto.PersonName;
import com.fedex.rscs.dto.PhoneNumber;
import com.fedex.rscs.dto.PhoneNumberDetail;
import com.fedex.rscs.dto.RequestHeaderDetail;
import com.fedex.rscs.dto.RequestedPackageLineItem;
import com.fedex.rscs.dto.RequestedShipment;
import com.fedex.rscs.dto.ShipmentSpecialServiceDescription;
import com.fedex.rscs.dto.ShipmentTransactionDetail;
import com.fedex.rscs.dto.ShippingContact;
import com.fedex.rscs.dto.SoldShipmentResource;
import com.fedex.rscs.dto.SoldShipmentResponse;
import com.fedex.rscs.dto.SoldShipmentSummaryResource;
import com.fedex.rscs.dto.SoldShipmentSummaryResponse;
import com.fedex.rscs.dto.WeightUnit;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.model.ContactDetail;
import com.fedex.rscs.model.DeleteSoldShipmentData;
import com.fedex.rscs.model.DeletedSoldShipmentDetail;
import com.fedex.rscs.model.LocationContextDetail;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.RequestedPackageLineItemDetail;
import com.fedex.rscs.model.RequestedPackageSpecialServices;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.SoldShipmentDetail;
import com.fedex.rscs.model.SoldShipmentSummary;
import com.fedex.rscs.model.SpecialServiceDescriptionDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.common.LocationDataDetail;
import com.fedex.rscs.model.common.PersonDetail;
import com.fedex.rscs.model.common.TransactionHeaderDetail;
import com.fedex.rscs.model.cshp.CompletedPackageData;
import com.fedex.rscs.model.cshp.ConfirmedShipmentData;
import com.fedex.rscs.model.cshp.OpenShipmentResponse;

/**
 * This component provides the methods to map the DTO to Model and transform back the model objects
 * into DTO class objects.
 * 
 * @author 3932968
 *
 */
@Component
public class RetailShipmentCreationProcessorMapper {

    /**
     * Method to convert CreateOpenShipmentRequest to RequestedShipmentDetail
     * 
     * @param createOpenShipmentRequest
     * @return
     */
    RequestedShipmentDetail toModel(
            CreateOpenShipmentRequest createOpenShipmentRequest) {

        // method to validate if phone number object is not null in case of delivery type
        // DATE_CERTAIN and APPOINTMENT then set the recipient phone number
        updateHomeDeliveryPhoneNumber(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment());

        RequestedShipmentDetail requestedShipmentDetail =
                RetailShipmentCreationMapper.INSTANCE.toModel(createOpenShipmentRequest);
        RetailShipmentCreationMapper.INSTANCE.updateAddress(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress(),
                requestedShipmentDetail.getRecipient()
                        .getAddress());
        RetailShipmentCreationMapper.INSTANCE.updateAddress(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress(),
                requestedShipmentDetail.getCustomer()
                        .getAddress());
        
        RetailShipmentCreationMapper.INSTANCE
                .updateAddressClassification(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                        .getRequestedShipment()
                        .getRecipient()
                        .getAddress(),
                        requestedShipmentDetail.getRecipient().getAddress());
        RetailShipmentCreationMapper.INSTANCE
                .updateAddressClassification(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                        .getCustomer()
                        .getAddress(),
                        requestedShipmentDetail.getCustomer()
                                .getAddress());
        RetailShipmentCreationMapper.INSTANCE.updateContact(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getContact(),
                requestedShipmentDetail.getRecipient()
                        .getContact());
        updatePhoneNumber(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient(), (requestedShipmentDetail.getRecipient()));
        RetailShipmentCreationMapper.INSTANCE.updateCustomerContact(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getContact(),
                requestedShipmentDetail.getCustomer()
                        .getContact());
        if (createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getCustomer()
                .getContact() != null) {
            updateCustomerPhoneNumber(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                    .getCustomer(), (requestedShipmentDetail.getCustomer()));
        }
        if (createOpenShipmentRequest.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper() != null) {

            RetailShipmentCreationMapper.INSTANCE.updateAddress(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                    .getRequestedShipment()
                    .getShipper()
                    .getAddress(),
                    requestedShipmentDetail.getSender()
                            .getAddress());

            RetailShipmentCreationMapper.INSTANCE.updateContact(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                    .getRequestedShipment()
                    .getShipper()
                    .getContact(),
                    requestedShipmentDetail.getSender()
                            .getContact());
            updatePhoneNumber(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                    .getRequestedShipment()
                    .getShipper(), (requestedShipmentDetail.getSender()));

            RetailShipmentCreationMapper.INSTANCE
            .updateAddressClassification(createOpenShipmentRequest.getCreateOpenShipmentRequest()
                    .getRequestedShipment()
                    .getShipper()
                    .getAddress(),
                    requestedShipmentDetail.getSender().getAddress());            
        } else {
            requestedShipmentDetail.setSender(requestedShipmentDetail.getCustomer());
            requestedShipmentDetail.setSenderCustomer(true);
        }     
        RetailShipmentCreationMapper.INSTANCE.updateRequestedDate(createOpenShipmentRequest, requestedShipmentDetail);
        RetailShipmentCreationMapper.INSTANCE.updateTransactionCodeAndExpressAfterPickup(createOpenShipmentRequest, requestedShipmentDetail);
        RetailShipmentCreationMapper.INSTANCE.updateShippingPaymentDetails(createOpenShipmentRequest,
                requestedShipmentDetail);

        RetailShipmentCreationMapper.INSTANCE.updateOriginAddress(
                createOpenShipmentRequest.getCreateOpenShipmentRequest()
                        .getLocationDetails()
                        .getTransactionLocationInfo()
                        .getAddress(),
                requestedShipmentDetail.getOrigin()
                        .getAddress());
        List<RequestedPackageLineItem> requestedPackageLineItemsList =
                createOpenShipmentRequest.getCreateOpenShipmentRequest()
                        .getRequestedShipment()
                        .getRequestedPackageLineItems();
        // Added condition to set holdAtLocationDetail null when HOLD_AT_LOCATION" is not selected.
        if (requestedShipmentDetail.getSpecialServicesRequested() != null
                && !CollectionUtils.isEmpty(requestedShipmentDetail.getSpecialServicesRequested()
                        .getSpecialServicesRequestedType())) {
            Optional<SpecialServiceDescriptionDetail> queryResult =
                    requestedShipmentDetail.getSpecialServicesRequested()
                            .getSpecialServicesRequestedType()
                            .stream()
                            .filter(value -> value.getSpecialServiceType()
                                    .equalsIgnoreCase("HOLD_AT_LOCATION"))
                            .findFirst();
            if (!queryResult.isPresent()) {
                requestedShipmentDetail.getSpecialServicesRequested()
                        .setHoldAtLocationDetail(null);
            } else {
                requestedShipmentDetail.getSpecialServicesRequested()
                        .getHoldAtLocationDetail()
                        .setLocationType(null);
            }
        }
        else {
            // Added condition to set holdAtLocationDetail null when no special service is provided
            requestedShipmentDetail.getSpecialServicesRequested()
                    .setHoldAtLocationDetail(null);
        }
        List<RequestedPackageLineItemDetail> packageLineItems = new ArrayList<>();
        for (RequestedPackageLineItem requestedPackageLineItem : requestedPackageLineItemsList) {

            RequestedPackageLineItemDetail requestedPackageLineItemDetail =
                    RetailShipmentCreationMapper.INSTANCE.toModel(requestedPackageLineItem);

            if (!CollectionUtils.isEmpty(requestedPackageLineItem.getSpecialServicesRequested())) {
                RequestedPackageSpecialServices requestedPackageSpecialServices = new RequestedPackageSpecialServices();
                requestedPackageSpecialServices.setSpecialServicesRequested(requestedPackageLineItem
                        .getSpecialServicesRequested()
                        .stream()
                        .map(service -> new SpecialServiceDescriptionDetail(service.getSpecialServiceType(),
                                service.getSpecialServiceSubType(), service.getDisplayText(), service.getDescription()))
                        .collect(Collectors.toList()));
                requestedPackageLineItemDetail.getPackageSpecialServicesRequested()
                        .setSpecialServicesRequested(requestedPackageSpecialServices.getSpecialServicesRequested());
            }
            packageLineItems.add(requestedPackageLineItemDetail);

        }

        requestedShipmentDetail.setPackageLineItems(packageLineItems);
        return requestedShipmentDetail;
    }

    /**
     * Method to convert WorkstationDetails to WorkstationDetail
     * 
     * @param workstationDetail
     * @return
     */
    WorkstationDetail updateWorkstationDetail(
            CreateOpenShipmentRequest createOpenShipmentRequest) {

        WorkstationDetail workstationDetail = new WorkstationDetail();

        RetailShipmentCreationMapper.INSTANCE.updateWorkstationDetail(createOpenShipmentRequest, workstationDetail);

        return workstationDetail;
    }

    /**
     * Mapper to convert OpenShipmentResponse to CreateOpenShipmentResponse object
     * 
     * @param openShipmentResponse
     * @return
     */
    CreateOpenShipmentResponse toDto(
            OpenShipmentResponse openShipmentResponse) {

        CreateOpenShipmentResponse createOpenShipmentResponse =
                RetailShipmentCreationMapper.INSTANCE.toDto(openShipmentResponse);
        toDto(openShipmentResponse.getCompleteShipmentDetail()
                .getRecipient(),
                createOpenShipmentResponse.getOpenShipment()
                        .getRecipient());
        createOpenShipmentResponse.getOpenShipment()
                .getCarrierDetails()
                .setCarrierType(createCarrierType(createOpenShipmentResponse.getOpenShipment()
                        .getCarrierDetails()
                        .getCarrierCode()
                        .name()));
        List<CompletedPackageData> completedPackageDetails = openShipmentResponse.getCompleteShipmentDetail()
                .getCompletedPackageDetails();
        if (!ObjectUtils.isEmpty(completedPackageDetails)) {
            int length = completedPackageDetails.size();
            List<OpenShipmentPackageLineItem> packageLineItems = createOpenShipmentResponse.getOpenShipment()
                    .getPackageLineItems();
            for (int i = 0; i < length; i++) {
                packageLineItems.get(i)
                        .getWeight()
                        .setUnits(WeightUnit.valueOf(completedPackageDetails.get(i)
                                .getWeight()
                                .getUnit()));
            }
            createOpenShipmentResponse.getOpenShipment()
                    .setPackageLineItems(packageLineItems);
        }
        return createOpenShipmentResponse;
    }

    /**
     * Method to set recipient PhoneNumber and extension in DTO
     * 
     * @param partyDetail
     * @param parties
     * @return
     */
    private void toDto(
            PartyDetail partyDetail,
            Party party) {

        ShippingContact contact = party.getContact();
        List<PhoneNumberDetail> phoneNumberDetails = new ArrayList<>();
        PhoneNumberDetail phoneNumberDetail = new PhoneNumberDetail();
        PhoneNumber phoneNumber = new PhoneNumber();
        String number = partyDetail.getContact()
                .getPhoneNumber();
        String extension = partyDetail.getContact()
                .getPhoneExtension();
        phoneNumber.setNumber(number);
        phoneNumber.setExtension(extension);
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        phoneNumberDetails.add(phoneNumberDetail);
        contact.setPhoneNumberDetails(phoneNumberDetails);
        party.setContact(contact);
    }
    
    /**
     * Mapper to convert ConfirmShipmentRequest to ShipmentConfirmationRequest object
     * 
     * @param confirmShipmentRequest
     * @param details
     * @return
     */
    public ShipmentConfirmationRequest toModel(
            ConfirmShipmentRequestData confirmShipmentRequest,
            LocationContextDetail details) {

        ShipmentConfirmationRequest shipmentConfirmationRequest =
                RetailShipmentCreationMapper.INSTANCE.toModel(confirmShipmentRequest, details);
        shipmentConfirmationRequest.setLocationDataDetail(RetailShipmentCreationMapper.INSTANCE
                .updateLocationDataDetails(confirmShipmentRequest.getLocationDetails(),
                        shipmentConfirmationRequest.getLocationDataDetail()));
        
        return shipmentConfirmationRequest;
    }

    /**
     * Mapper to convert ConfirmedShipmentData to ShipmentTransactionDetail object
     * 
     * @param shipmentData
     * @return
     */
    public ShipmentTransactionDetail toDto(
            ConfirmedShipmentData shipmentData) {

        return RetailShipmentCreationMapper.INSTANCE.toDto(shipmentData);
    }

    /**
     * Mapper to convert LocationDetail dto to model
     * 
     * @param confirmShipmentRequest
     * @return
     */
    public LocationDataDetail toLocationDetailModel(
            LocationDetail locationDetail,
            LocationContextDetail details) {

        return RetailShipmentCreationMapper.INSTANCE.toLocationDetail(locationDetail, details);
    }

    /**
     * Mapper to return carrier types corresponding to various carrier codes
     * 
     * @param carrierCode
     * @return
     */
    private CarrierType createCarrierType(
            String carrierCode) {

        if (CarrierCodeType.FDXE.name()
                .equalsIgnoreCase(carrierCode)) {
            return CarrierType.EXPRESS;

        } else if (CarrierCodeType.FDXG.name()
                .equalsIgnoreCase(carrierCode)) {
            return CarrierType.GROUND;

        } else if (CarrierCodeType.FXSP.name()
                .equalsIgnoreCase(carrierCode)) {
            return CarrierType.SMARTPOST;

        }

        return null;
    }

    /**
     * @param headerDetails
     * @param details
     * @return
     */
    public TransactionHeaderDetail toModel(
            RequestHeaderDetail headerDetails,
            LocationContextDetail details) {

        return RetailShipmentCreationMapper.INSTANCE.toHeaderDetails(headerDetails, details);
    }
    
    /**
     * Mapper to convert List<SoldShipmentResourceDetail> to SoldShipmentResponse object
     * 
     * @param soldShipments
     * @return
     */
    public SoldShipmentResponse toDto(
            List<SoldShipmentDetail> soldShipments) {

        SoldShipmentResponse soldShipmentResponse = new SoldShipmentResponse();
        List<CXSAlert> alerts = new ArrayList<>();
        if (CollectionUtils.isEmpty(soldShipments)) {
            return soldShipmentResponse;
        }

        List<SoldShipmentResource> soldShipmentResources = new ArrayList<>(soldShipments.size());
        for (SoldShipmentDetail soldShipmentResourceDetail : soldShipments) {
            if (!soldShipmentResourceDetail.isValidLocation()) {
                alerts.add(new CXSAlert(ServiceErrorCode.INVALID_LOCATION_ID.name(),
                        "Shipment was originally created from different " + soldShipmentResourceDetail.getLocation()
                                + " location.",
                        AlertType.WARNING));
            }
            
            SoldShipmentResource soldShipmentResource =
                    RetailShipmentCreationMapper.INSTANCE.toDto(soldShipmentResourceDetail);
            if (soldShipmentResourceDetail.getShipper() != null) {
                ShippingContact shippingContact = new ShippingContact();
                RetailShipmentCreationMapper.INSTANCE.updateContact(soldShipmentResourceDetail.getShipper()
                        .getContact(), shippingContact);
                soldShipmentResource.getShipper()
                        .setContact(shippingContact);
                toDto(soldShipmentResourceDetail.getShipper(), soldShipmentResource.getShipper());
            }
            if (soldShipmentResourceDetail.getRecipient() != null) {
                ShippingContact recepientContact = new ShippingContact();
                RetailShipmentCreationMapper.INSTANCE.updateContact(soldShipmentResourceDetail.getRecipient()
                        .getContact(), recepientContact);
                soldShipmentResource.getRecipient()
                        .setContact(recepientContact);
                toDto(soldShipmentResourceDetail.getRecipient(), soldShipmentResource.getRecipient());
            }
            if (soldShipmentResourceDetail.getCustomer() != null) {
                ShippingContact customerContact = new ShippingContact();
                RetailShipmentCreationMapper.INSTANCE.updateContact(soldShipmentResourceDetail.getCustomer()
                        .getContact(), customerContact);
                soldShipmentResource.getCustomer()
                        .setContact(customerContact);
                toDto(soldShipmentResourceDetail.getCustomer(), soldShipmentResource.getCustomer());
            }
            soldShipmentResources.add(soldShipmentResource);
        }
        soldShipmentResponse.setSoldShipments(soldShipmentResources);
        soldShipmentResponse.withAlerts(alerts.stream()
                .toArray(CXSAlert[]::new));
        return soldShipmentResponse;
    }

    /**
     * Mapper to convert List<SoldShipmentSummary> to SoldShipmentSummaryResponse object
     * 
     * @param soldShipments
     * @return
     */
    public SoldShipmentSummaryResponse toSoldShipmentSummaryDto(
            List<SoldShipmentSummary> soldShipmentDetails) {

        SoldShipmentSummaryResponse soldShipmentSummaryResponse = new SoldShipmentSummaryResponse();
        if (CollectionUtils.isEmpty(soldShipmentDetails)) {
            return soldShipmentSummaryResponse;
        }

        List<SoldShipmentSummaryResource> soldShipmentResources = new ArrayList<>(soldShipmentDetails.size());
        for (SoldShipmentSummary soldShipmentSummaryeDetail : soldShipmentDetails) {
            soldShipmentResources.add(RetailShipmentCreationMapper.INSTANCE.toDto(soldShipmentSummaryeDetail));
        }
        soldShipmentSummaryResponse.setSoldShipmentSummaries(soldShipmentResources);
        return soldShipmentSummaryResponse;
    }

    /**
     * Mapper to convert LocationInfo dto to LocationDataDetail model
     * 
     * @param locationDetails
     * @param locationData
     * @return LocationDataDetail
     */
    public LocationDataDetail toLocationModel(
            LocationInfo locationDetails,
            LocationContextDetail locationData) {

        return RetailShipmentCreationMapper.INSTANCE.toLocationModel(locationDetails, locationData);
    }


    /**
     * Mapper to convert PersonName to PersonDetail
     * 
     * @param customerName
     * @return PersonDetail
     */
    public PersonDetail toPersonModel(
            PersonName customerName) {

        return RetailShipmentCreationMapper.INSTANCE.toPersonModel(customerName);
    }

    /**
     * Mapper to convert DeleteSoldShipmentDetail model to DeletedSoldShipmentResponse dto
     * 
     * @param deleteSoldShipmentDetail
     * @return DeletedSoldShipmentResponse
     */
    public DeletedSoldShipmentResponse toDto(
            DeletedSoldShipmentDetail deleteSoldShipmentDetail) {

        DeletedSoldShipmentResponse deletedSoldShipmentResponse = new DeletedSoldShipmentResponse();
        DeletedSoldShipmentResource deletedSoldShipmentResource = new DeletedSoldShipmentResource();
        if (deleteSoldShipmentDetail.getCarrierType() != null) {

            deletedSoldShipmentResource.setCarrierType(CarrierType.valueOf(deleteSoldShipmentDetail.getCarrierType()));
        }
        deletedSoldShipmentResource.setTrackingNumber(deleteSoldShipmentDetail.getTrackingNumber());
        deletedSoldShipmentResponse.setDeletedSoldShipment(deletedSoldShipmentResource);
        return deletedSoldShipmentResponse;
    }

    /**
     * Mapper to create WorkstationDetail model
     * 
     * @param headerDetails
     * @param locationData
     * @return WorkstationDetail
     */
    public WorkstationDetail toWorkStationModel(
            RequestHeaderDetail headerDetails,
            LocationContextDetail locationData) {

        return RetailShipmentCreationMapper.INSTANCE.toWorkstationDetail(headerDetails,locationData);
    }

    public DeleteSoldShipmentData toDeletSoldShipmentData(
            DeleteSoldShipmentRequestData deleteSoldShipmentsRequest,
            LocationContextDetail locationData) {

        DeleteSoldShipmentData deleteSoldShipmentData=new DeleteSoldShipmentData();
        deleteSoldShipmentData.setLocationId(deleteSoldShipmentsRequest.getLocationDetails().getCode());
        deleteSoldShipmentData.setRequestDateTime(deleteSoldShipmentsRequest.getHeaderDetails().getRequestDateTime());
        deleteSoldShipmentData.setShipmentTrackingNumber(deleteSoldShipmentsRequest.getTrackingId());
        deleteSoldShipmentData.setRetailTransactionId(deleteSoldShipmentsRequest.getRetailTransactionId());
        deleteSoldShipmentData.setLocationDetail(toLocationModel(deleteSoldShipmentsRequest.getLocationDetails(),locationData));
        deleteSoldShipmentData.setPersonDetail(toPersonModel(deleteSoldShipmentsRequest.getCustomerName()));
        deleteSoldShipmentData.setWorkstationDetail(toWorkStationModel(deleteSoldShipmentsRequest.getHeaderDetails(),locationData));
        return deleteSoldShipmentData;
    }
    
    /**
     * Method to set recipient and sender's phoneNumber and extension number
     * 
     * @param party
     * @return
     */
    private void updatePhoneNumber(
            Party party,
            PartyDetail partyDetails) {

        ContactDetail contactDetail = partyDetails.getContact();
        if (contactDetail != null && party.getContact() != null && !CollectionUtils.isEmpty(party.getContact()
                .getPhoneNumberDetails())) {
            contactDetail.setPhoneNumber(party.getContact()
                    .getPhoneNumberDetails()
                    .get(0)
                    .getPhoneNumber()
                    .getNumber());
            contactDetail.setPhoneExtension(party.getContact()
                    .getPhoneNumberDetails()
                    .get(0)
                    .getPhoneNumber()
                    .getExtension());
        }
        partyDetails.setContact(contactDetail);
    }

    /**
     * Method to set customer phoneNumber and extension number
     * 
     * @param person
     * @return
     */
    private void updateCustomerPhoneNumber(
            Person person,
            PartyDetail partyDetails) {

        ContactDetail contactDetail = partyDetails.getContact();
        if (contactDetail != null && !CollectionUtils.isEmpty(person.getContact()
                .getPhoneNumberDetails())) {
            contactDetail.setPhoneNumber(person.getContact()
                    .getPhoneNumberDetails()
                    .get(0)
                    .getPhoneNumber()
                    .getNumber());
            contactDetail.setPhoneExtension(person.getContact()
                    .getPhoneNumberDetails()
                    .get(0)
                    .getPhoneNumber()
                    .getExtension());
        }
        partyDetails.setContact(contactDetail);
    }
    
    /**
     * Mapper to convert ConfirmedShipmentData to ConfirmedShipmentResource object
     * 
     * @param shipmentData
     * @return
     */
    public ConfirmedShipmentResource toConfirmedShipmentResourceDto(
            ConfirmedShipmentData confirmedShipmentData) {

        return RetailShipmentCreationMapper.INSTANCE.toConfirmedShipmentResource(confirmedShipmentData);
        

    }

    /**
     * Method to update home delivery phone number in from recipient
     * 
     * @param requestedShipment
     * @return
     */
    private void updateHomeDeliveryPhoneNumber(
            RequestedShipment requestedShipment) {

        // condition to validate if phone number object is not null in case of delivery type
        // DATE_CERTAIN and APPOINTMENT then set the recipient phone number
        ShipmentSpecialServiceDescription specialServicesRequested = requestedShipment.getSpecialServicesRequested();
        if (specialServicesRequested != null && specialServicesRequested.getHomeDeliveryDetail() != null
                && !specialServicesRequested.getHomeDeliveryDetail()
                        .getHomeDeliveryType()
                        .equals(HomeDeliveryType.EVENING)
                && specialServicesRequested.getHomeDeliveryDetail()
                        .getPhoneNumber() == null) {

            specialServicesRequested.getHomeDeliveryDetail()
                    .setPhoneNumber(requestedShipment.getRecipient()
                            .getContact()
                            .getPhoneNumberDetails()
                            .get(0)
                            .getPhoneNumber());

        }

    }


}
