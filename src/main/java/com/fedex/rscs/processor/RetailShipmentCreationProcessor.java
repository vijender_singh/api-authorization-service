package com.fedex.rscs.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.netflix.hystrix.exception.HystrixRuntimeException;

import com.fedex.common.cxs.dto.CXSAlert;
import com.fedex.common.cxs.dto.CXSAlert.AlertType;
import com.fedex.common.cxs.exception.TerminalDbAccessException;
import com.fedex.common.cxs.exception.TransientDbAccessException;
import com.fedex.rscs.config.AppConfig;
import com.fedex.rscs.dto.Address;
import com.fedex.rscs.dto.ConfirmShipmentRequestData;
import com.fedex.rscs.dto.ConfirmedShipmentRequest;
import com.fedex.rscs.dto.ConfirmedShipmentResponse;
import com.fedex.rscs.dto.CreateOpenShipmentRequest;
import com.fedex.rscs.dto.CreateOpenShipmentResponse;
import com.fedex.rscs.dto.DeleteOpenShipmentResponse;
import com.fedex.rscs.dto.DeleteSoldShipmentRequest;
import com.fedex.rscs.dto.DeletedSoldShipmentResponse;
import com.fedex.rscs.dto.Notification;
import com.fedex.rscs.dto.NotificationType;
import com.fedex.rscs.dto.OpenShipmentCore;
import com.fedex.rscs.dto.OpenShipmentIdentifier;
import com.fedex.rscs.dto.PaymentType;
import com.fedex.rscs.dto.RetrieveOpenShipmentResponse;
import com.fedex.rscs.dto.ShipmentCheckoutRequest;
import com.fedex.rscs.dto.ShipmentCheckoutRequestData;
import com.fedex.rscs.dto.ShipmentCheckoutResourceData;
import com.fedex.rscs.dto.ShipmentCheckoutResponse;
import com.fedex.rscs.dto.ShipmentQueryType;
import com.fedex.rscs.dto.ShipmentTransactionDetail;
import com.fedex.rscs.dto.SoldShipmentResponse;
import com.fedex.rscs.dto.SoldShipmentSummaryResponse;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.ServiceProcessorTerminalException;
import com.fedex.rscs.model.DeletedSoldShipmentDetail;
import com.fedex.rscs.model.LocationContextDetail;
import com.fedex.rscs.model.ServiceConstant;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.SoldShipmentDetail;
import com.fedex.rscs.model.SoldShipmentSummary;
import com.fedex.rscs.model.cshp.ConfirmedShipmentData;
import com.fedex.rscs.repository.ConfirmShipmentRepository;
import com.fedex.rscs.service.ConfirmShipmentTransactionService;
import com.fedex.rscs.service.OpenShipmentTransactionService;
import com.fedex.rscs.service.RetailLocationDataProvider;
import com.fedex.rscs.service.TransactionRouter;
import com.fedex.rscs.validator.RetailShippingDataValidator;

/**
 * All business logic and data Orchestration should go here. This class represents the internal API
 * for this service
 */
@Service
public class RetailShipmentCreationProcessor {

    private static final Logger logger = LoggerFactory.getLogger(RetailShipmentCreationProcessor.class);
    private final OpenShipmentTransactionService openShipmentTransactionService;
    private final RetailShipmentCreationProcessorMapper retailShipmentCreationProcessorMapper;
    private final ConfirmShipmentTransactionService confirmShipmentTransactionService;
    private final RetailLocationDataProvider locationDataProvider;
    private final ConfirmShipmentRepository confirmShipmentRepository;
    private final TransactionRouter<ShipmentConfirmationRequest, ConfirmedShipmentData> checkoutRouter;
    private final AppConfig applicationConfig; 

    @Autowired
    public RetailShipmentCreationProcessor(final OpenShipmentTransactionService openShipmentTransactionService,
            final RetailShipmentCreationProcessorMapper retailShipmentCreationProcessorMapper,
            final ConfirmShipmentTransactionService confirmShipmentTransactionService,
            final RetailLocationDataProvider locationDataProvider,
            final ConfirmShipmentRepository confirmShipmentRepository,final TransactionRouter<ShipmentConfirmationRequest, ConfirmedShipmentData> checkoutRouter,
            final AppConfig applicationConfig) {

        this.openShipmentTransactionService = openShipmentTransactionService;
        this.retailShipmentCreationProcessorMapper = retailShipmentCreationProcessorMapper;
        this.confirmShipmentTransactionService = confirmShipmentTransactionService;
        this.locationDataProvider = locationDataProvider;
        this.confirmShipmentRepository = confirmShipmentRepository;
        this.checkoutRouter = checkoutRouter;
        this.applicationConfig = applicationConfig;

    }

    /**
     * Method to execute create open shipment
     * 
     * @param request
     * @return
     */
    public CreateOpenShipmentResponse createOpenShipment(
            CreateOpenShipmentRequest request) {

        logger.debug("Create openshipment request is called with request as: {}", request);

        checkAddress(request.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getRecipient()
                .getAddress());
        checkAddress(request.getCreateOpenShipmentRequest()
                .getCustomer()
                .getAddress());
        if (request.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getShipper() != null) {
            checkAddress(request.getCreateOpenShipmentRequest()
                    .getRequestedShipment()
                    .getShipper()
                    .getAddress());
        }

        // method to validate if HOME_DELIVERY_PREMIUM is passed as specialservice then deliveryType
        // and other details should be as expected

        RetailShippingDataValidator.validateSpecialService(request.getCreateOpenShipmentRequest()
                .getRequestedShipment()
                .getSpecialServicesRequested());

        try {
            LocationContextDetail details =
                    locationDataProvider.getContextDetails(request.getCreateOpenShipmentRequest()
                            .getLocationDetails()
                            .getTransactionLocationInfo()
                            .getCode());
            request.getCreateOpenShipmentRequest()
                    .getLocationDetails()
                    .setCityCenterAccountNumber(details.getCityCenterAccountNumber());
            request.getCreateOpenShipmentRequest()
                    .getLocationDetails()
                    .setGroundAccountNumber(details.getGroundAccountNumber());
            request.getCreateOpenShipmentRequest()
                    .getHeaderDetails()
                    .getWorkstationDetails()
                    .setMeterNumber(details.getBtcMeterNumber());

        } catch (ServiceProcessorTerminalException exception) {
            logger.error("Failed to process the location details request for request : {} ", request, exception);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.RETAIL_LOCATION_SERVICE_UNAVAILABLE);

        }

        return retailShipmentCreationProcessorMapper
                .toDto(openShipmentTransactionService.create(retailShipmentCreationProcessorMapper.toModel(request),
                        retailShipmentCreationProcessorMapper.updateWorkstationDetail(request)));
    }

    /**
     * Method to execute retrieve open shipment
     * 
     * @param referenceId
     * @param locationId
     * @param trackingId
     * @return
     */
    public RetrieveOpenShipmentResponse retrieveOpenShipment(
            String referenceId,
            String locationId,
            String trackingId) {

        logger.debug("RetrieveOpenShipment: referenceId({}), locationId({}), trackingId({})", referenceId, locationId,
                trackingId);
        validateInputParameters(referenceId,locationId,trackingId);
        CreateOpenShipmentResponse createOpenShipmentResponse = retailShipmentCreationProcessorMapper
                .toDto(openShipmentTransactionService.retrieveOpenShipment(referenceId, locationId, trackingId));

        List<OpenShipmentCore> openShipments = new ArrayList<>();
        OpenShipmentCore openShipmentCore = createOpenShipmentResponse.getOpenShipment();
        if (openShipmentCore != null && openShipmentCore.getPaymentDetails() != null
                && openShipmentCore.getPaymentDetails()
                        .getPayor() != null
                && PaymentType.NON_ACCOUNT == openShipmentCore.getPaymentDetails()
                        .getPaymentType()) {
            openShipmentCore.getPaymentDetails()
                    .getPayor()
                    .setAccountNumber(null);
        }
        openShipments.add(openShipmentCore);

        RetrieveOpenShipmentResponse retrieveOpenShipmentResponse = new RetrieveOpenShipmentResponse();
        retrieveOpenShipmentResponse.setOpenShipments(openShipments);

        return retrieveOpenShipmentResponse;
    }

    /**
     * Method to execute confirm shipment
     * 
     * @param request
     * @return
     */
    public ConfirmedShipmentResponse confirmShipments(
            ConfirmedShipmentRequest confirmedShipmentRequest) {

        logger.debug("Confirmed shipment request is called with request as: {}", confirmedShipmentRequest);

        ConfirmShipmentRequestData confirmShipmentRequestData = confirmedShipmentRequest.getConfirmedShipmentRequest();

        // Retrieving the location information
        LocationContextDetail details =
                locationDataProvider.getContextDetails(confirmShipmentRequestData.getLocationDetails()
                        .getTransactionLocationInfo()
                        .getCode());

        // Confirming the shipment
        ConfirmedShipmentData shipmentData = confirmShipmentTransactionService
                .confirmShipment(retailShipmentCreationProcessorMapper.toModel(confirmShipmentRequestData, details));

        ConfirmedShipmentResponse checkoutResponse = new ConfirmedShipmentResponse();
        checkoutResponse.setConfirmedShipment(
                retailShipmentCreationProcessorMapper.toConfirmedShipmentResourceDto(shipmentData));

        logger.debug("Confirmed Shipment call completed with response as: {} ", checkoutResponse);
        return checkoutResponse;
    }

    /**
     * Method to execute shipment checkouts
     * 
     * @param request
     * @return
     */
    public ShipmentCheckoutResponse checkoutShipments(
            ShipmentCheckoutRequest request) {

        logger.debug("Shipment checkout call started with request: {} ", request);
        ShipmentCheckoutResponse checkoutResponse = new ShipmentCheckoutResponse();

        ShipmentCheckoutRequestData shipmentCheckoutRequest = request.getShipmentCheckoutRequest();

        if (!CollectionUtils.isEmpty(request.getShipmentCheckoutRequest()
                .getRequestedShipment())) {

            ShipmentCheckoutResourceData checkoutResource = null;
            try {
                LocationContextDetail locationContextDetails =
                        locationDataProvider.getContextDetails(shipmentCheckoutRequest.getLocationDetails()
                                .getTransactionLocationInfo()
                                .getCode());
                if (applicationConfig.isAsyncCheckout()) {
                    checkoutResource = processCheckoutAsync(shipmentCheckoutRequest, locationContextDetails);
                } else {
                    checkoutResource = processCheckout(shipmentCheckoutRequest, locationContextDetails);
                }
                checkoutResponse.setShipmentCheckout(checkoutResource);


            } catch (ServiceProcessorTerminalException exception) {
                logger.error(
                        "Failed to process the location details request for request : {} , Exception Occured is : {}",
                        request, exception);
                throw new ServiceProcessorTerminalException(ServiceErrorCode.RETAIL_LOCATION_SERVICE_UNAVAILABLE);

            }
        }
            logger.debug("Shipment checkout call execution completed with response: {} ", checkoutResponse);
            return checkoutResponse;
        }


    /**
     * This method is used to process the checkout request synchronously and to process multiple
     * shipping line items sequentially.
     * 
     * @param confirmShipmentRequest
     * @param details
     * @return
     */
    private ShipmentCheckoutResourceData processCheckout(
            ShipmentCheckoutRequestData shipmentCheckoutRequest,
            LocationContextDetail locationDetails) {

        ShipmentTransactionDetail transactionDetail = null;
        List<ShipmentTransactionDetail> transactionDetails = new ArrayList<>();
        for (OpenShipmentIdentifier requestedShipment : shipmentCheckoutRequest.getRequestedShipment()) {
            try {
                ConfirmedShipmentData shipmentData =
                        confirmShipmentTransactionService.confirmShipment(retailShipmentCreationProcessorMapper
                                .toModel(new ConfirmShipmentRequestData(shipmentCheckoutRequest.getHeaderDetails(),
                                        shipmentCheckoutRequest.getLocationDetails(), requestedShipment,
                                        shipmentCheckoutRequest.getLabelSpecification(),
                                        shipmentCheckoutRequest.getPaymentDetails()), locationDetails));
                transactionDetail = retailShipmentCreationProcessorMapper.toDto(shipmentData);
                transactionDetail.setLineItemSuccess(true);

            } catch (ServiceProcessorTerminalException exception) {
                transactionDetail = new ShipmentTransactionDetail();
                transactionDetail.setMasterTrackingId(requestedShipment.getMasterTrackingId());
                transactionDetail.addNotification(new Notification(exception.getShipmentCreationErrorCode()
                        .getApiErrorCode(),
                        exception.getShipmentCreationErrorCode()
                                .getMessage(),
                        NotificationType.ERROR));
                transactionDetail.setLineItemSuccess(false);
            }
            transactionDetails.add(transactionDetail);
        }

        ShipmentCheckoutResourceData checkoutResource = new ShipmentCheckoutResourceData();
        checkoutResource.setTransactionDetails(transactionDetails);
        return checkoutResource;
    }
    
    /**
     * Method to execute delete open shipment
     * 
     * @param referenceId
     * @param locationId
     * @param trackingId
     * @return
     */
    public DeleteOpenShipmentResponse deleteOpenShipment(
            String referenceId,
            String locationId,
            String trackingId) {

        logger.debug("deleteOpenShipment called with : referenceId({}), locationId({}), trackingId({})", referenceId,
                locationId, trackingId);
        validateInputParameters(referenceId, locationId, trackingId);
        openShipmentTransactionService.deleteOpenShipment(referenceId, locationId, trackingId);

        return new DeleteOpenShipmentResponse(trackingId);
    }
    
    /**
     * Method to validate referenceId, locationId, trackingId
     * 
     * @param referenceId
     * @param locationId
     * @param trackingId
     */
    private void validateInputParameters(
            String referenceId,
            String locationId,
            String trackingId) {
        
        RetailShippingDataValidator.checkBlank(referenceId, ServiceConstant.REFERENCE_ID, ServiceConstant.OPENSHIPMENT);
        RetailShippingDataValidator.checkBlank(locationId, ServiceConstant.LOCATION_ID, ServiceConstant.OPENSHIPMENT);
        RetailShippingDataValidator.checkBlank(trackingId, ServiceConstant.TRACKING_ID, ServiceConstant.OPENSHIPMENT);

    }

    /**
     * Method to retrieve sold shipments on basis of tracking or reference id.
     * 
     * @param locationId
     * @param id
     * @param type
     * @return
     */
    public SoldShipmentResponse retrieveSoldShipments(
            String locationId,
            String id,
            ShipmentQueryType type) {

        logger.debug("Retrieve soldshipment is called with: locationId({}), id({}), type({})", locationId, id, type);
      
        RetailShippingDataValidator.checkBlank(locationId, ServiceConstant.LOCATION_ID, ServiceConstant.SOLDSHIPMENT);
        RetailShippingDataValidator.checkBlank(id, ServiceConstant.ID, ServiceConstant.SOLDSHIPMENT);
        RetailShippingDataValidator.checkObjectEmpty(type, ServiceConstant.TYPE, ServiceConstant.SOLDSHIPMENT);
        
        List<SoldShipmentDetail> soldShipments = null;
        try {
            switch (type) {
                case BARCODE:
                    soldShipments = confirmShipmentRepository.getSoldShipmentByBarCode(id, locationId);
                    break;
                case REFID:
                    soldShipments = confirmShipmentRepository.getSoldShipmentByGuid(id, locationId);
                    break;
                case TRACK:
                    soldShipments = confirmShipmentRepository.getSoldShipmentByTrackingNumber(id, locationId);

            }
        } catch (TerminalDbAccessException | TransientDbAccessException ex) {

            logger.error("No records found in database against provided type {} with id : {} and locationId : {}", type,
                    id, locationId, ex);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.DB_SEARCH_FAILURE);
        } catch (HystrixRuntimeException ex) {

            logger.error(
                    "Unable to connect with DB to retrieve open sold shipment details for  sold shipment id : {} and locationId : {} ",
                    id, locationId, ex);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.SYSTEM_UNAVAILABLE);
        }

        return retailShipmentCreationProcessorMapper.toDto(soldShipments);
    }

    /**
     * Method to retrieve sold shipment summaries on basis of operationaldate and locationId
     * 
     * @param locationId
     * @param operationaldate
     * @return
     */
    public SoldShipmentSummaryResponse retrieveSoldShipmentSummaries(
            String locationId,
            String operationaldate) {

        logger.debug("Retrieve soldshipment summaries is called with: locationId({}), operationaldate({})", locationId,
                operationaldate);
        
        RetailShippingDataValidator.checkBlank(locationId, ServiceConstant.LOCATION_ID, ServiceConstant.SOLDSHIPMENT);
        RetailShippingDataValidator.checkBlank(operationaldate, ServiceConstant.OPERATIONALDATE,
                ServiceConstant.SOLDSHIPMENT);

        List<SoldShipmentSummary> soldShipmentDetails = null;
        try {

            soldShipmentDetails = confirmShipmentRepository.getShipmentsByLocation(locationId, operationaldate);
            if (soldShipmentDetails != null) {
                logger.debug("total records retrieved for location {} are {} ", locationId, soldShipmentDetails.size());
            }
        } catch (TerminalDbAccessException | TransientDbAccessException ex) {

            logger.error("No records found in database against locationId : {}", locationId, ex);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.DB_SEARCH_FAILURE);
        } catch (HystrixRuntimeException ex) {

            logger.error(
                    "Unable to connect with DB to retrieve open sold shipment details for location id {} and opearational date {} ",
                    locationId, operationaldate, ex);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.SYSTEM_UNAVAILABLE);
        }

        return retailShipmentCreationProcessorMapper.toSoldShipmentSummaryDto(soldShipmentDetails);
    }
    
    /**
     * Method to execute delete sold shipments
     * 
     * @param request
     * @return
     */
    public DeletedSoldShipmentResponse deleteSoldShipments(
            DeleteSoldShipmentRequest request) {

        logger.debug("Delete soldshipment request is called with request as: {}", request);
        LocationContextDetail locationData = null;
        try {
            locationData = locationDataProvider.getContextDetails(request.getDeleteSoldShipmentsRequest()
                    .getLocationDetails()
                    .getCode());
        } catch (ServiceProcessorTerminalException exception) {
            logger.error("Failed to process the location details for request : {} , due to : {}", request, exception);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.RETAIL_LOCATION_SERVICE_UNAVAILABLE);
        }
        DeletedSoldShipmentDetail deleteSoldShipmentDetail =
                confirmShipmentTransactionService.stop(retailShipmentCreationProcessorMapper
                        .toDeletSoldShipmentData(request.getDeleteSoldShipmentsRequest(), locationData));

        DeletedSoldShipmentResponse deletedSoldShipmentResponse =
                retailShipmentCreationProcessorMapper.toDto(deleteSoldShipmentDetail);
        if (deleteSoldShipmentDetail.isUpdationError()) {

            logger.warn("Failed to update transaction into database");
            deletedSoldShipmentResponse.withAlerts(new CXSAlert(ServiceErrorCode.DB_UPDATION_FAILURE.getApiErrorCode(),
                    ServiceErrorCode.DB_UPDATION_FAILURE.getMessage(), AlertType.WARNING));
        }
        return deletedSoldShipmentResponse;
    }
    
    /**
     * This method is used to process the checkout request asynchronously and to process multiple
     * shipping line items in parallel.
     * 
     * @param shipmentCheckoutRequest
     * @param locationContextDetails
     * @return
     */
    private ShipmentCheckoutResourceData processCheckoutAsync(
            final ShipmentCheckoutRequestData shipmentCheckoutRequest,
            LocationContextDetail locationContextDetails) {

        List<ShipmentTransactionDetail> transactionDetails = new ArrayList<>();

        List<ShipmentConfirmationRequest> lineItems = shipmentCheckoutRequest.getRequestedShipment()
                .stream()
                .map(lineItem -> retailShipmentCreationProcessorMapper.toModel(new ConfirmShipmentRequestData(
                        shipmentCheckoutRequest.getHeaderDetails(), shipmentCheckoutRequest.getLocationDetails(),
                        lineItem, shipmentCheckoutRequest.getLabelSpecification(),
                        shipmentCheckoutRequest.getPaymentDetails()), locationContextDetails))
                .collect(Collectors.toList());

        Map<String, ConfirmedShipmentData> transactionResponse = checkoutRouter.process(lineItems);

        for (Entry<String, ConfirmedShipmentData> trackingId : transactionResponse.entrySet()) {
            transactionDetails.add(retailShipmentCreationProcessorMapper.toDto(trackingId.getValue()));
        }

        ShipmentCheckoutResourceData checkoutResource = new ShipmentCheckoutResourceData();
        checkoutResource.setTransactionDetails(transactionDetails);

        return checkoutResource;
    }
    
    /**
     * Method to validate the Address for null and maximum length check
     * 
     * @param address
     * @param addressOf
     */
    private void checkAddress(
            Address address) {

        logger.info("Address validation is in process");

        RetailShippingDataValidator.checkBlank(address.getCity(), ServiceConstant.CITY, ServiceConstant.ADDRESS);
        RetailShippingDataValidator.checkBlank(address.getStateOrProvinceCode(), ServiceConstant.STATEORPROVICECODE,
                ServiceConstant.ADDRESS);
        RetailShippingDataValidator.checkEmpty(address.getStreetLines(), ServiceConstant.STREETLINES,
                ServiceConstant.ADDRESS);
        RetailShippingDataValidator.checkBlank(address.getStreetLines()
                .get(0), ServiceConstant.STREETLINES, ServiceConstant.ADDRESS);
        for (String checkStreetLines : address.getStreetLines()) {
            RetailShippingDataValidator.checkInvalidLength(checkStreetLines, ServiceConstant.STREETLINES,
                    ServiceConstant.ADDRESS, 35);
        }

    }
}

