package com.fedex.rscs.service;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.fedex.rscs.model.InsuredValueDetail;
import com.fedex.rscs.model.OpenShipment;
import com.fedex.rscs.model.OpenShipmentDetail;
import com.fedex.rscs.model.PaymentDetail;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.WeightDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.cshp.CompleteShipmentDetail;
import com.fedex.rscs.model.cshp.OpenShipmentResponse;
import com.fedex.rscs.model.cshp.PreauthorizedRateData;
import com.fedex.rscs.model.cshp.ShippingRateData;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;

/**
 * Map struct mapper to map the one Model type to another Model type on Service Layer
 *
 * @author 3883424
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface OpenShipmentTransactionMapper {

    OpenShipmentTransactionMapper INSTANCE = Mappers.getMapper(OpenShipmentTransactionMapper.class);

    /**
     * to update Open Shipment Response Payment Data
     * 
     * @param openShipmentDetail
     * @param openShipmentResponse
     */
    @Mappings({
            @Mapping(source = "paymentDetail.paymentType", target = "openShipmentResponse.shippingPaymentData.paymentType"),
            @Mapping(source = "paymentDetail.amount", target = "openShipmentResponse.shippingPaymentData.amount"),
            @Mapping(source = "paymentDetail.responsibleParty", target = "openShipmentResponse.shippingPaymentData.responsibleParty"),
            @Mapping(source = "paymentDetail.payor.address", target = "openShipmentResponse.shippingPaymentData.payorAddress"),
            @Mapping(source = "paymentDetail.payor.accountType", target = "openShipmentResponse.shippingPaymentData.accountType"),
            @Mapping(source = "paymentDetail.payor.accountNumber", target = "openShipmentResponse.shippingPaymentData.accountNumber"),
            @Mapping(source = "paymentDetail.payor.creditCard.maskedCreditCard", target = "openShipmentResponse.shippingPaymentData.maskedCreditCard"),
            @Mapping(source = "paymentDetail.payor.creditCard.type", target = "openShipmentResponse.shippingPaymentData.type"),
            @Mapping(source = "paymentDetail.payor.creditCard.expirationDate", target = "openShipmentResponse.shippingPaymentData.expirationDate"),})
    void updateShipmentPaymentData(
            PaymentDetail paymentDetail,
            @MappingTarget OpenShipmentResponse openShipmentResponse);
    
    /**
     * to update Open Shipment Response Rating Data
     * 
     * @param openShipmentDetail
     * @param openShipmentResponse
     */
    @Mappings({
        @Mapping(source = "shippingRateData", target = "openShipmentResponse.shippingRateData")})
    void updateShipmentRateData(
            ShippingRateData shippingRateData,
            @MappingTarget OpenShipmentResponse openShipmentResponse);

    /**
     * to update Open Shipment Response Data
     * 
     * @param openShipmentDetail
     * @param openShipmentResponse
     */
    @Mappings({@Mapping(source = "openShipmentDetail.shipmentIndex", target = "completeShipmentDetail.index"),
            @Mapping(source = "openShipmentDetail.carrierDetails.carrierCode", target = "completeShipmentDetail.carrierCode"),
            @Mapping(source = "openShipmentDetail.packageLineItems", target = "completeShipmentDetail.completedPackageDetails"),
            @Mapping(source = "openShipmentDetail.locationId", target = "completeShipmentDetail.locationId"),
            @Mapping(source = "openShipmentDetail.packagingType", target = "completeShipmentDetail.packagingDescription.type"),
            @Mapping(source = "openShipmentDetail.rateDetails.serviceDetails.serviceName", target = "completeShipmentDetail.serviceDescription.type")})
    void updateCompleteShipmentDetail(
            OpenShipmentDetail openShipmentDetail,
            @MappingTarget CompleteShipmentDetail completeShipmentDetail);

    /**
     * Mapper to create OpenShipment from WorkstationDetail
     * 
     * @param openShipment
     * @return
     */
    @Mappings({@Mapping(source = "openShipment.cityCenterAccountNumber", target = "accountNumber"),
            @Mapping(source = "openShipment.fedexId", target = "teamMemberId")})
    WorkstationDetail mapToWorkstationDetail(
            OpenShipment openShipment);
    
    /**
     * Mapper to create UpdateOpenShipmentStatusData from ShipmentConfirmationRequest
     * 
     * @param request
     * @return
     */
    @Mappings({@Mapping(source = "request.referenceId", target = "referenceId"),
            @Mapping(source = "request.transactionLocationId", target = "locationId"),
            @Mapping(source = "request.trackingId", target = "trackingId"),
            @Mapping(source = "request.workstationDetail.teamMemberId", target = "teamMemberId")})
    UpdateOpenShipmentStatusData mapToUpdateOpenShipmentStatusData(
            ShipmentConfirmationRequest request);
    
    /**
     * Mapper to create ShippingRateData from PreAuthorizedRateData
     * 
     * @param request
     * @return
     */
    @Mappings({@Mapping(source = "request.shipmentRateDetails.totalNetCharge.currency", target = "currency"),
            @Mapping(source = "request.shipmentRateDetails.totalBaseCharge.amount", target = "totalBaseCharges"),
            @Mapping(source = "request.shipmentRateDetails.totalSurcharges.amount", target = "totalSurcharges"),
            @Mapping(source = "request.shipmentRateDetails.totalNetCharge.amount", target = "totalNetCharges"),
            @Mapping(source = "request.shipmentRateDetails.totalRebates.amount", target = "totalRebates"),
            @Mapping(source = "request.shipmentRateDetails.totalFreightDiscounts.amount", target = "totalFreightDiscounts"),
            @Mapping(source = "request.shipmentRateDetails.totalNetFreight.amount", target = "totalNetFreight"),
            @Mapping(source = "request.shipmentRateDetails.totalNetFedExCharge.amount", target = "totalNetFedExCharge"),
            @Mapping(source = "request.shipmentRateDetails.totalDutiesAndTaxes.amount", target = "totalDutiesAndTaxes"),
            @Mapping(source = "request.shipmentRateDetails.totalAncillaryFeesAndTaxes.amount", target = "totalAncillaryFeesAndTaxes"),
            @Mapping(source = "request.shipmentRateDetails.totalDutiesTaxesAndFees.amount", target = "totalDutiesTaxesAndFees"),
            @Mapping(source = "request.shipmentRateDetails.totalNetChargeWithDutiesAndTaxes.amount", target = "totalNetChargeWithDutiesAndTaxes"),
            @Mapping(source = "request.shipmentRateDetails.totalTaxes.amount", target = "totalTaxes"),
            @Mapping(source = "request.shipmentRateDetails.surcharges", target = "surcharges")})
    ShippingRateData mapPreAuthorizedRateDataToShippingRateData(
            PreauthorizedRateData request);
    
    /**
     * To get value from InsuredValueDetail object to Double
     * 
     * @param insuredValue
     * @return
     */
    default Double insuredValueToDouble(
            InsuredValueDetail insuredValue) {

        if (insuredValue == null) {
            return 0.0;
        }

        return new Double(insuredValue.getAmount());
    }

    /**
     * To convert WeightDetail to Double
     * 
     * @param weight
     * @return
     */
    default Double weightDetailToDouble(
            WeightDetail weight) {

        if (weight == null) {
            return 0.0;
        }

        return new Double(weight.getValue());
    }

}
