package com.fedex.rscs.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fedex.rscs.config.AppConfig;
import com.fedex.rscs.model.OpenShipmentData;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.repository.ConfirmShipmentRepository;
import com.fedex.services.common.jms.publish.FedExJmsPublisher;
import com.fedex.services.common.jms.publish.exception.JmsPublishUnavailableException;
import com.fedex.services.common.jms.publish.types.PublishInput;

/**
 * This service class create and JMSQ event to send ground rate data for ground shipments to JMS.
 * 
 * @author 3932968
 *
 */
@Service
public class RatingEventServiceImpl implements RatingEventService {

    private static final Logger logger = LoggerFactory.getLogger(RatingEventServiceImpl.class);

    private FedExJmsPublisher groundTransactionPublisher;
    private final GroundShippingRequestMapper mapper;
    private final AppConfig appConfig;
    private final ConfirmShipmentRepository confirmShipmentRepository;
    private final String GROUND_RATE_EVENT_TYPE_HEADER = "EventType";
    private final String GROUND_RATE_EVENT_TYPE = "SPOS";

    @Autowired
    @Lazy
    public RatingEventServiceImpl(
            @Qualifier("groundTransactionPublisher") final FedExJmsPublisher groundTransactionPublisher,
            final GroundShippingRequestMapper mapper, final AppConfig appConfig,
            final ConfirmShipmentRepository confirmShipmentRepository) {

        this.groundTransactionPublisher = groundTransactionPublisher;
        this.mapper = mapper;
        this.appConfig = appConfig;
        this.confirmShipmentRepository = confirmShipmentRepository;
    }

    /**
     * create a event to send ground rate to JMS Queue.
     */
    @Async("groundRateJMSPublishQueue")
    @Override
    public void sendGroundTransactionRate(
            ShipmentConfirmationRequest request,
            OpenShipmentData openShipment) {

        if (appConfig.isGroundShippingEvent()) { // temp flag condition added
            logger.debug("Event Created to publish ground rates to JMS");
            PublishInput input = null;
            try {
                input = PublishInput.builder()
                        .objectToPublish(mapper.createGroundShippingData(request, openShipment))
                        .addJmsHeader(GROUND_RATE_EVENT_TYPE_HEADER, GROUND_RATE_EVENT_TYPE)
                        .build();

                groundTransactionPublisher.publish(input);
                logger.debug("Messgage published to JMS Queue with input {} : ", input);
                confirmShipmentRepository.updateGroundRateNotification(request.getTrackingId(),
                        request.getShipmentTimeStamp());
                logger.debug("Ground rate flag updated in database for request {} : ", request);

            } catch (JmsPublishUnavailableException jpeu) {

                logger.error("JmsPublishUnavailableException : Unable to publish input {} to JMSQ due to Exception : ",
                        input, jpeu);
            } catch (Exception ex) {

                logger.error("Exception : Unable to publish input {} to JMSQ due exception : ", input, ex);
            }
        }
    }

}
