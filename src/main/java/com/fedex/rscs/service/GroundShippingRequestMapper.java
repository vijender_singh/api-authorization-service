package com.fedex.rscs.service;

import java.text.ParseException;

import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.fedex.rscs.dto.PaymentSource;
import com.fedex.rscs.dto.PaymentType;
import com.fedex.rscs.model.OpenShipmentData;
import com.fedex.rscs.model.PaymentDetail;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.ground.jms.GroundShippingData;
import com.fedex.rscs.model.ground.jms.OperationInfo;
import com.fedex.rscs.model.ground.jms.PrePaymentInfo;

/**
 * Mapper class to prepare ground event request
 * 
 * @author 5034922
 *
 */
@Component
public class GroundShippingRequestMapper {

    private static final String OPERATION = "Insert";

    public enum PrePaymentType {

        CCA("04"), CSH("05"), CHK("05");

        private String paymentCode;

        private PrePaymentType(String paymentCode) {

            this.paymentCode = paymentCode;
        }

        public String getPaymentCode() {

            return this.paymentCode;
        }
    }

    /**
     * 
     * @param request
     * @param openShipment
     * @return
     * @throws ParseException
     */
    public GroundShippingData createGroundShippingData(
            final ShipmentConfirmationRequest request,
            final OpenShipmentData openShipment) {

        GroundShippingData data = new GroundShippingData();
        data.setPrePaymentInfo(createPaymentInfo(request.getPaymentDetail(), request.getLocationDataDetail()
                .getGroundAccountNumber()));

        data.setRatedPackages(openShipment.getOpenShipment()
                .getPreauthorizedRateDetails()
                .getRatedPackages()
                .get(0));

        OperationInfo operationInfo = new OperationInfo();
        if (openShipment.getOpenShipment() != null) {
            if (!ObjectUtils.isEmpty(openShipment.getOpenShipment()
                    .getPackageLineItems()) && openShipment.getOpenShipment()
                            .getPackageLineItems()
                            .get(0)
                            .getTrackingId() != null) {

                operationInfo.setTrackID(openShipment.getOpenShipment()
                        .getPackageLineItems()
                        .get(0)
                        .getTrackingId()
                        .getTrackingNumber());

            }
            if (openShipment.getOpenShipment()
                    .getPreauthorizedRateDetails() != null
                    && openShipment.getOpenShipment()
                            .getPreauthorizedRateDetails()
                            .getShipmentRateDetails() != null) {
                operationInfo.setRateZone(openShipment.getOpenShipment()
                        .getPreauthorizedRateDetails()
                        .getShipmentRateDetails()
                        .getRateZone());
            }
        }

        operationInfo.setPostalCode(request.getLocationDataDetail()
                .getTransactionLocationInfo()
                .getAddress()
                .getPostalCode());

        operationInfo.setShipDate(request.getShipmentTimeStamp());

        operationInfo.setOperation(OPERATION);
        data.setOperationInfo(operationInfo);

        return data;
    }

    /**
     * Method to create payapmentInfo
     * 
     * @param paymentDetails
     * @return
     */
    private PrePaymentInfo createPaymentInfo(
            PaymentDetail paymentDetails,
            String shippierRefId) {

        PrePaymentInfo paymentInfo = null;
        if (paymentDetails != null) {
            paymentInfo = new PrePaymentInfo();
            paymentInfo.setSource(PaymentSource.SPOS.name());
            paymentInfo.setRefid(paymentDetails.getPaymentRefId());
            paymentInfo.setShipperNumber(shippierRefId);

            if (paymentDetails.getPaymentType()
                    .equalsIgnoreCase(PaymentType.CASH.name())
                    || paymentDetails.getPaymentType()
                            .equalsIgnoreCase(PaymentType.CHECK.name())) {

                paymentInfo.setPayMthdCode(PrePaymentType.CSH.paymentCode);
                paymentInfo.setMode(paymentDetails.getPaymentType()
                        .equalsIgnoreCase(PaymentType.CASH.name()) ? PrePaymentType.CSH.name()
                                : PrePaymentType.CHK.name());

            } else if (paymentDetails.getPayor() != null && paymentDetails.getPayor()
                    .getCreditCard() != null) {
                paymentInfo.setPayMthdCode(PrePaymentType.CCA.paymentCode);
                paymentInfo.setMode(PrePaymentType.CCA.name());
                paymentInfo.setCcType(paymentDetails.getPayor()
                        .getCreditCard()
                        .getType());
                paymentInfo.setCcExpirationDate(paymentDetails.getPayor()
                        .getCreditCard()
                        .getExpirationDate());
                paymentInfo.setMaskedCCNbr(paymentDetails.getPayor()
                        .getCreditCard()
                        .getMaskedCreditCard());
            }

        }
        return paymentInfo;
    }

}
