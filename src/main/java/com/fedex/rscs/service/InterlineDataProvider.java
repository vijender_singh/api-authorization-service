package com.fedex.rscs.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.fedex.rscs.client.rsss.RSSSClient;
import com.fedex.rscs.config.CacheConfiguration;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.ServiceProcessorTerminalException;
import com.fedex.rscs.exception.client.ClientTerminalException;

/**
 * This component provides the interline code data and works as a interline data repository. <br>
 * The source of data for this repository is application cache or RSSS service. It gets the
 * interline data either from local application cache or by communicating with RSSS.
 * 
 * @author 3932968
 *
 */
@Component
public class InterlineDataProvider {

    private static final Logger logger = LoggerFactory.getLogger(InterlineDataProvider.class);

    private static final String INTERLINE_EXCEPTION_MSG =
            "Failed to retrieve the interline information for interline code : {} due to : {} ";

    private RSSSClient rsssClient;

    @Autowired
    public InterlineDataProvider(final RSSSClient rsssClient) {

        this.rsssClient = rsssClient;
    }

    /**
     * Method to fetch the interline account details.
     * 
     * @param interlineCode
     * @return
     */
    @Cacheable(cacheNames = {CacheConfiguration.INTERLINE_CACHE}, key = "{#interlineId}")
    public String getInterlineAccountInfo(
            String interlineId) {

        logger.debug("Retrieve the interline information for interline id : {}", interlineId);
        try {

            return rsssClient.validateInterlineAccounts(interlineId);

        } catch (ClientTerminalException exception) {

            logger.error(INTERLINE_EXCEPTION_MSG, interlineId, exception);
            throw new ServiceProcessorTerminalException(exception.getShipmentCreationErrorCode());
        } catch (RuntimeException exception) {

            // This will handle all runtime exception like hystrix runtime exception.
            logger.error(INTERLINE_EXCEPTION_MSG, interlineId, exception);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.INTERLINE_ACCOUNT_SERVICE_UNAVAILABLE);
        }
    }
}
