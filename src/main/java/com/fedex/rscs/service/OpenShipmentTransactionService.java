package com.fedex.rscs.service;

import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.cshp.OpenShipmentResponse;

/**
 * Service Layer to create a open and modify shipment in CSHP
 * 
 * @author 3798910
 *
 */
public interface OpenShipmentTransactionService {

    /**
     * It will provide the detail of createOpenShipment based on provided request
     * 
     * @param createOpenRequest
     * @param workstationDetail
     * @return
     */
    OpenShipmentResponse create(
            RequestedShipmentDetail createOpenRequest,
            WorkstationDetail workstationDetail);

    /**
     * Method to retrieve open shipment details from DB
     * 
     * @param referenceId
     * @param locationId
     * @param trackingId
     * @return
     */
    OpenShipmentResponse retrieveOpenShipment(
            String referenceId,
            String locationId,
            String trackingId);

    /**
     * Method to delete open shipment details
     * 
     * @param referenceId
     * @param locationId
     * @param trackingId
     * @return
     */
    void deleteOpenShipment(
            String referenceId,
            String locationId,
            String trackingId);
}
