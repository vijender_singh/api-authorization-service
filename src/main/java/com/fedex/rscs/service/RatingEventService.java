package com.fedex.rscs.service;

import com.fedex.rscs.model.OpenShipmentData;
import com.fedex.rscs.model.ShipmentConfirmationRequest;

/**
 * Interface that contain method to re rate the ground shipments
 * 
 * @author 3932968
 *
 */
public interface RatingEventService {

    /**
     * create a event to re rate the ground shipments.
     * @return 
     */
    void sendGroundTransactionRate(
            ShipmentConfirmationRequest request,
            OpenShipmentData openShipment);

}
