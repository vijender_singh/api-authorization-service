package com.fedex.rscs.service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.exception.HystrixRuntimeException;

import com.fedex.common.cxs.exception.TerminalDbAccessException;
import com.fedex.common.cxs.exception.TransientDbAccessException;
import com.fedex.rscs.client.cshp.ShipServiceClient;
import com.fedex.rscs.client.pes.PackageEventServiceClient;
import com.fedex.rscs.dto.CarrierType;
import com.fedex.rscs.dto.NotificationType;
import com.fedex.rscs.dto.PaymentType;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.ServiceProcessorTerminalException;
import com.fedex.rscs.exception.client.ClientTerminalException;
import com.fedex.rscs.exception.client.ClientTransientException;
import com.fedex.rscs.model.DeleteSoldShipmentData;
import com.fedex.rscs.model.DeletedSoldShipmentDetail;
import com.fedex.rscs.model.OpenShipment;
import com.fedex.rscs.model.OpenShipmentData;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ServiceType;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.common.EventNotification;
import com.fedex.rscs.model.common.LocationDataDetail;
import com.fedex.rscs.model.cshp.CompleteShipmentDetail;
import com.fedex.rscs.model.cshp.ConfirmedShipmentData;
import com.fedex.rscs.model.cshp.TrackingIdDetail;
import com.fedex.rscs.model.cshp.TransactionStatus;
import com.fedex.rscs.repository.ConfirmShipmentRepository;
import com.fedex.rscs.repository.OpenShipmentRepository;

/**
 * Implementation of ConfirmShipmentTransactionService to conform an open shipment in CSHP
 * 
 * @author 5034922
 *
 */
@Service
public class ConfirmShipmentTransactionServiceImpl implements ConfirmShipmentTransactionService {

    private static final Logger logger = LoggerFactory.getLogger(ConfirmShipmentTransactionServiceImpl.class);

    private final ShipServiceClient shipServiceClient;
    private final ConfirmShipmentTransactionServiceMapper mapper;
    private final OpenShipmentRepository openShipmentRepository;
    private final ConfirmShipmentRepository confirmShipmentRepository;
    private final OpenShipmentTransactionServiceMapper openShipmentServiceMapper;
    private final PackageEventServiceClient packageEventServiceClient;
    private final RatingEventService eventService;
    
    @Autowired
    public ConfirmShipmentTransactionServiceImpl(final ShipServiceClient shipServiceClient,
            final ConfirmShipmentTransactionServiceMapper mapper, final OpenShipmentRepository openShipmentRepository,
            final ConfirmShipmentRepository confirmShipmentRepository,
            final OpenShipmentTransactionServiceMapper openShipmentServiceMapper,
            final PackageEventServiceClient packageEventServiceClient,
            final RatingEventService eventService) {

        this.shipServiceClient = shipServiceClient;
        this.mapper = mapper;
        this.openShipmentRepository = openShipmentRepository;
        this.confirmShipmentRepository = confirmShipmentRepository;
        this.openShipmentServiceMapper = openShipmentServiceMapper;
        this.packageEventServiceClient = packageEventServiceClient;
        this.eventService = eventService;
    }

    /**
     * update the shipment status.
     */
    @Override
    public ConfirmedShipmentData confirmShipment(
            ShipmentConfirmationRequest request) {

        logger.info("Calling OpenShipment repository to retrieve the shipment");
        ConfirmedShipmentData confirmedShipmentData = null;
        List<EventNotification> notifications = new ArrayList<>();
        EventNotification notification = new EventNotification();

        try {
            OpenShipment openShipment = openShipmentRepository.retrieve(request.getReferenceId(),
                    request.getTransactionLocationId(), request.getTrackingId());

            if (TransactionStatus.CONFIRMED.name()
                    .equals(openShipment.getOpenShipmentStatusCode())) {
                confirmedShipmentData = retrieveShipmentTransactionInfo(request);
            }
            if (confirmedShipmentData == null) {

                logger.info("Calling ShipServiceClient service to confirm the requested shipment");
                OpenShipmentData openShipmentData =
                        openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData());
                request.getWorkstationDetail()
                        .setAppVersionId(
                                openShipmentServiceMapper.getAppVersionIdFromCreateOpenShipment(openShipmentData));

                // updating labelSpecification from DB if not coming in request.
                mapper.updateLabelSpecification(request, openShipmentData);
                confirmedShipmentData = confirmOrModifyShipment(request, openShipment, request.getLocationDataDetail(),
                        openShipmentData);

            } else {
                logger.debug("Shipment is already confirmed.");
                notification = new EventNotification(ServiceErrorCode.SHIPMENT_ALREADY_CONFIRMED.getApiErrorCode(),
                        ServiceErrorCode.SHIPMENT_ALREADY_CONFIRMED.getMessage(), NotificationType.NOTE, null);
                notifications.add(notification);
                confirmedShipmentData.setNotifications(notifications);
            }
            return confirmedShipmentData;

        } catch (TerminalDbAccessException | TransientDbAccessException ex) {

            logger.error("Unable to connect with DB to retrieve open shipment details  request {}", request, ex);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.DB_SEARCH_FAILURE);
        } catch (HystrixRuntimeException ex) {

            logger.error("Unable to connect with DB to retrieve open shipment details for request  {}", request, ex);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.SYSTEM_UNAVAILABLE);
        }
    }

    /**
     * Method to retrieve shipment transaction information.
     * 
     * @param request
     * @return
     */
    private ConfirmedShipmentData retrieveShipmentTransactionInfo(
            ShipmentConfirmationRequest request) {

        try {
            return mapper.buildConfirmedShipmentResponse(
                    confirmShipmentRepository.getShipmentDetails(request.getTrackingId(), request.getReferenceId()));
        } catch (TerminalDbAccessException | TransientDbAccessException ex) {
            logger.warn(
                    "Unable to retrieve ShipmentTransactionInfo when ShipmentConfirmationRequest instance has state : {}",
                    request);
            logger.error("No records found in database against given trackingId and referenceId", ex);
        } catch (HystrixRuntimeException ex) {
            logger.error(
                    "Unable to connect with DB to retrieve open shipment details for ShipmentConfirmationRequest {}",
                    request, ex);
        }
        return null;
    }

    /**
     * This method is use to call Confirm Open Shipment from client .
     * 
     * @param request
     * @return
     */
    private CompleteShipmentDetail callConfirmOpenShipment(
            ShipmentConfirmationRequest request) {

        logger.info("Calling ShipServiceClient.confirmOpenShipment() to confirm the requested shipment");

        return shipServiceClient.confirmOpenShipment(request);
    }

    /**
     * This method is use to call Modify Open Shipment from client .
     * 
     * @param request
     * @param openShipment
     * @return
     */
    private CompleteShipmentDetail callModifyOpenShipment(
            ShipmentConfirmationRequest request,
            OpenShipment openShipment) {

        logger.info("Calling ShipServiceClient.modifyOpenShipment() to confirm the requested shipment");

        RequestedShipmentDetail requestedShipmentDtl = mapper.createRequestedShipmentDetail(request, openShipment);
        return shipServiceClient.modifyOpenShipment(requestedShipmentDtl, request.getWorkstationDetail());
    }
    
    /**
     * This method is use to call Confirm Open Shipment from client .
     * 
     * @param openShipment
     * @param request
     * @param locationDetail
     * @param data
     * @param completeShipmentDetail
     * @return
     */
    private void saveConfirmShipment(
            OpenShipment openShipment,
            ShipmentConfirmationRequest request,
            LocationDataDetail locationDetail,
            ConfirmedShipmentData data,
            CompleteShipmentDetail completeShipmentDetail) {

        List<EventNotification> notifications = new ArrayList<>();
        EventNotification notification =null;
        
        try {
            boolean saveResponse = confirmShipmentRepository.save(
                    mapper.buildShipmentDetails(openShipment, request, locationDetail, completeShipmentDetail, data));
            if (saveResponse) {
                openShipmentRepository
                        .updateStatus(openShipmentServiceMapper.buildUpdateOpenShipmentStatusData(request));
            }
        } catch (TerminalDbAccessException | TransientDbAccessException ex) {
            logger.error(
                    "Unable to save or update when instance states are OpenShipment : {} ,ShipmentConfirmationRequest :{} ,LocationDataDetail :{} ,CompleteShipmentDetail :{} ",
                    openShipment, request, locationDetail, completeShipmentDetail, ex);
            notification =
                    new EventNotification(ServiceErrorCode.DB_INSERTION_FAILURE.getApiErrorCode(),
                            ServiceErrorCode.DB_INSERTION_FAILURE.getMessage(), NotificationType.NOTE, null);
            notifications.add(notification);
            data.setNotifications(notifications);
        } catch (HystrixRuntimeException ex) {
            logger.error(
                    "Unable to save or update when instance states are OpenShipment : {} ,ShipmentConfirmationRequest :{} ,LocationDataDetail :{} ,CompleteShipmentDetail :{} ",
                    openShipment, request, locationDetail, completeShipmentDetail, ex);

            notification = new EventNotification(ServiceErrorCode.SYSTEM_UNAVAILABLE.getApiErrorCode(),
                    ServiceErrorCode.SYSTEM_UNAVAILABLE.getMessage(), NotificationType.NOTE, null);
            notifications.add(notification);
            data.setNotifications(notifications);
        }
    }

    /**
     * To post scans in PES Method to confirm or modify shipment and and post scan to PES
     * 
     * @param request
     * @param openShipment
     * @param locationDetail
     * @return
     */

    private ConfirmedShipmentData confirmOrModifyShipment(
            ShipmentConfirmationRequest request,
            OpenShipment openShipment,
            LocationDataDetail locationDetail,
            OpenShipmentData openShipmentData) {

        CompleteShipmentDetail reply = null;
        ConfirmedShipmentData confirmedShipmentData = null;
        boolean isScanSuccess = false;
        String timeStamp = request.getShipmentTimeStamp();
        boolean isGroundShipment = openShipmentData != null && openShipmentData.getOpenShipment() != null
                && openShipmentData.getOpenShipment()
                .getServiceType()!=null && (openShipmentData.getOpenShipment()
                        .getServiceType()
                        .equalsIgnoreCase(ServiceType.FEDEX_GROUND.name())
                || openShipmentData.getOpenShipment()
                        .getServiceType()
                        .equalsIgnoreCase(ServiceType.GROUND_HOME_DELIVERY.name()));

        if (locationDetail.isExpressAfterPickup() && !isGroundShipment) {
            request.setShipmentTimeStamp(ZonedDateTime.parse(request.getShipmentTimeStamp())
                    .plusDays(1)
                    .toString());
        } else if (locationDetail.isGroundAfterPickup() && isGroundShipment) {

            request.setShipmentTimeStamp(ZonedDateTime.parse(request.getShipmentTimeStamp())
                    .plusDays(1)
                    .toString());
        }
        try {
            if (StringUtils.equalsIgnoreCase(request.getPaymentDetail()
                    .getPaymentType(), PaymentType.ACCOUNT.name())) {
                reply = callConfirmOpenShipment(request);
            } else {
                reply = callModifyOpenShipment(request, openShipment);
            }
            request.setShipmentTimeStamp(timeStamp);
            confirmedShipmentData = mapper.buildResponse(reply);
        } catch (ClientTransientException e) {
            logger.error(
                    "ClientTransientException: Unexpected response from confirm or modify call when instances has state OpenShipment :{} , ShipmentConfirmationRequest:{} and if response received : {},",
                    openShipment, request, reply, e);
            throw new ServiceProcessorTerminalException(e.getShipmentCreationErrorCode());
        } catch (ClientTerminalException e) {
            logger.error(
                    "ClientTerminalException: Unexpected response fromconfirm or modify call when instances has state OpenShipment :{} , ShipmentConfirmationRequest:{} and if response received : {},",
                    openShipment, request, reply, e);

            throw new ServiceProcessorTerminalException(e.getShipmentCreationErrorCode());
        }
        try {
            isScanSuccess = packageEventServiceClient.postPackagePossessionEvents(mapper.buildPESRequest(request,
                    openShipment, confirmedShipmentData, reply, locationDetail, openShipmentData));
            if (isScanSuccess) {
                saveConfirmShipment(openShipment, request, locationDetail, confirmedShipmentData, reply);
                sendGroundTransactionRateData(request, openShipmentData);
            }
        } catch (ClientTransientException e) {
            logger.error(
                    "ClientTransientException : Unexpected response either from confirm or modify call when instances has state OpenShipment :{} , ShipmentConfirmationRequest:{} and if response received : {},",
                    openShipment, request, reply, e);
            deleteConfirmShipment(reply.getMasterTrackingId(), request.getWorkstationDetail(),
                    request.getTransactionLocationId());
            throw new ServiceProcessorTerminalException(e.getShipmentCreationErrorCode());
        } catch (ClientTerminalException e) {
            logger.error(
                    "ClientTerminalException: Unexpected response either from confirm or modify call when instances has state OpenShipment :{} , ShipmentConfirmationRequest:{} and if response received : {},",
                    openShipment, request, reply, e);
            deleteConfirmShipment(reply.getMasterTrackingId(), request.getWorkstationDetail(),
                    request.getTransactionLocationId());
            throw new ServiceProcessorTerminalException(e.getShipmentCreationErrorCode());
        } catch (HystrixRuntimeException e) {
            logger.error(
                    "HystrixRuntimeException: Unexpected response either from confirm or modify call when instances has state OpenShipment :{} , ShipmentConfirmationRequest:{} and if response received : {},",
                    openShipment, request, reply, e);
            deleteConfirmShipment(reply.getMasterTrackingId(), request.getWorkstationDetail(),
                    request.getTransactionLocationId());
            throw new ServiceProcessorTerminalException(ServiceErrorCode.PES_SERVICE_UNAVAILABLE);
        }

        return confirmedShipmentData;
    }

    /**
     * Method to send Ground transaction rate data to JMS
     * 
     * @param request
     * @param openShipmentData
     */
    private void sendGroundTransactionRateData(
            ShipmentConfirmationRequest request,
            OpenShipmentData openShipmentData) {

        if ((ServiceType.FEDEX_GROUND.name()
                .equalsIgnoreCase(openShipmentData.getOpenShipment()
                        .getServiceType())
                || ServiceType.GROUND_HOME_DELIVERY.name()
                        .equalsIgnoreCase(openShipmentData.getOpenShipment()
                                .getServiceType()))
                && StringUtils.equalsIgnoreCase(openShipmentData.getOpenShipment()
                        .getPaymentDetails()
                        .getPaymentType(), PaymentType.NON_ACCOUNT.name())) {

            try {
                eventService.sendGroundTransactionRate(request, openShipmentData);
            } catch (TerminalDbAccessException | TransientDbAccessException ex) {
                logger.error(
                        "TerminalDbAccessException :: Unable to update ground transaction rate flag with tracking id : {} , and ship timestamp  :{} ",
                        request.getTrackingId(), request.getShipmentTimeStamp(), ex);

            } catch (HystrixRuntimeException ex) {
                logger.error(
                        "HystrixRuntimeException :: Unable to update ground transaction rate flag with tracking id : {} , and ship timestamp  :{} ",
                        request.getTrackingId(), request.getShipmentTimeStamp(), ex);

            }
        }
    }

    /**
     * Method used to delete confirm shipment.
     * @param trackingDetail
     * @param workstationDetail
     * @param locationId
     */
    public void deleteConfirmShipment(
            TrackingIdDetail trackingDetail,
            WorkstationDetail workstationDetail,
            String locationId) {

        try {
            shipServiceClient.deleteShipment(trackingDetail, workstationDetail, locationId);
        } catch (ClientTransientException | ClientTerminalException e) {
            logger.error(
                    "Unable to delete shipment when Tracking detail has instance : {} , WorkstationDetail has instance : {} , LocationId is : {}",
                    trackingDetail, workstationDetail, locationId, e);
        }

    }

    /**
     * Method to stop the shipment.
     * 
     * @param DeleteSoldShipmentData
     * @return DeletedSoldShipmentDetail
     */
    @Override
    public DeletedSoldShipmentDetail stop(
            DeleteSoldShipmentData deleteSoldShipmentData) {

        boolean isScanSuccess = false;
        DeletedSoldShipmentDetail deletedSoldShipmentDetail=new DeletedSoldShipmentDetail();

        try {
            ConfirmedShipmentData confirmedShipmentData = mapper.buildConfirmedShipmentResponse(
                    confirmShipmentRepository.getShipmentDetails(deleteSoldShipmentData.getShipmentTrackingNumber(),
                            deleteSoldShipmentData.getRetailTransactionId()));
            if (confirmedShipmentData != null && confirmedShipmentData.getShipmentStatus() != null
                    && !TransactionStatus.RETURNED.name()
                            .equals(confirmedShipmentData.getShipmentStatus()
                                    .name())) {

                TrackingIdDetail trackingIdDetail = new TrackingIdDetail();
                if (confirmedShipmentData.getMasterTrackingId() != null) {

                    trackingIdDetail.setTrackingNumber(confirmedShipmentData.getMasterTrackingId()
                            .getTrackingNumber());

                    trackingIdDetail.setTrackingIdType(confirmedShipmentData.getCarrierType());
                }
                
                deleteShipmentAndPostScans(deleteSoldShipmentData, isScanSuccess, deletedSoldShipmentDetail,
                        confirmedShipmentData, trackingIdDetail);
            } else {

                logger.error("Shipment is already deleted from the table.");
                throw new ServiceProcessorTerminalException(ServiceErrorCode.SHIPMENT_ALREADY_DELETED);
            }

            deletedSoldShipmentDetail.setCarrierType(confirmedShipmentData.getCarrierType());
            deletedSoldShipmentDetail.setTrackingNumber(deleteSoldShipmentData.getShipmentTrackingNumber());
            return deletedSoldShipmentDetail;

        } catch (TerminalDbAccessException | TransientDbAccessException ex) {

            logger.error("No records found in database against given trackingId and referenceId {}, {}, with {}",
                    deleteSoldShipmentData.getShipmentTrackingNumber(), deleteSoldShipmentData.getRetailTransactionId(),
                    ex);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.DB_SEARCH_FAILURE);
        } catch (HystrixRuntimeException ex) {

            logger.error("Unable to connect with DB to retrieve confirm shipment details {}", ex);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.DB_SYSTEM_UNAVAILABLE);
        }

    }

    /**
     * Method to delete shipment from CSHP, post scans and update shipment status.
     * 
     * @param deleteSoldShipmentData
     * @param isScanSuccess
     * @return deletedSoldShipmentDetail
     * @param confirmedShipmentData
     * @param trackingIdDetail
     */
    private void deleteShipmentAndPostScans(
            DeleteSoldShipmentData deleteSoldShipmentData,
            boolean isScanSuccess,
            DeletedSoldShipmentDetail deletedSoldShipmentDetail,
            ConfirmedShipmentData confirmedShipmentData,
            TrackingIdDetail trackingIdDetail) {

        try {

            deleteConfirmShipment(trackingIdDetail, deleteSoldShipmentData.getWorkstationDetail(),
                    deleteSoldShipmentData.getLocationId());
            logger.info("Shipment is deleted from the CSHP database.");

            if (CarrierType.EXPRESS.name()
                    .equals(confirmedShipmentData.getCarrierType())) {
                isScanSuccess = packageEventServiceClient.postPackageCancellationEvents(
                        mapper.buildPESCancelRequest(deleteSoldShipmentData.getRequestDateTime(), confirmedShipmentData,
                                deleteSoldShipmentData.getWorkstationDetail(),
                                deleteSoldShipmentData.getLocationDetail(), deleteSoldShipmentData.getPersonDetail()));
            } else {
                isScanSuccess = true;
            }

            if (isScanSuccess) {
                confirmShipmentRepository.updateStatus(mapper.buildUpdateOpenShipmentStatusData(
                        deleteSoldShipmentData.getShipmentTrackingNumber(), deleteSoldShipmentData.getLocationId(),
                        TransactionStatus.RETURNED.name(), deleteSoldShipmentData.getWorkstationDetail()
                                .getTeamMemberId(),
                        deleteSoldShipmentData.getRetailTransactionId()));
            }

        } catch (TerminalDbAccessException | TransientDbAccessException ex) {

            logger.error("Unable to update shipment {} status to returned due to {}",
                    deleteSoldShipmentData.getShipmentTrackingNumber(), ex);

            deletedSoldShipmentDetail.setUpdationError(true);
        } catch (HystrixRuntimeException e) {

            if (isScanSuccess) {

                logger.error("Unable to update shipment {} status to returned due to {}",
                        deleteSoldShipmentData.getShipmentTrackingNumber(), e);

                deletedSoldShipmentDetail.setUpdationError(true);
            } else {
                logger.error(
                        "Unable to delete shipment when Tracking detail has instance : {} , WorkstationDetail has instance : {} , LocationId is : {}, with {}",
                        trackingIdDetail, deleteSoldShipmentData.getWorkstationDetail(),
                        deleteSoldShipmentData.getLocationId(), e);
                throw new ServiceProcessorTerminalException(ServiceErrorCode.PES_SERVICE_UNAVAILABLE);
            }
        }
    }

}
