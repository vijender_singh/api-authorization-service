package com.fedex.rscs.service;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.fedex.rscs.model.InterlineShippingData;

/**
 * Map struct mapper to map the one Model type to another Model type on Service Layer
 *
 * @author 3900109
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ConfirmShipmentTransactionMapper {

    ConfirmShipmentTransactionMapper INSTANCE = Mappers.getMapper(ConfirmShipmentTransactionMapper.class);

    /**
     * to update interline Shipping Data for building PES Request
     * 
     * @param interlineShippingData
     */
    @Mapping(target= "accountNumber", ignore = true)
    InterlineShippingData setInterlineDetails(
            InterlineShippingData interlineShippingData);

}
