package com.fedex.rscs.service;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.exception.HystrixRuntimeException;

import com.fedex.common.cxs.exception.TerminalDbAccessException;
import com.fedex.common.cxs.exception.TransientDbAccessException;
import com.fedex.rscs.client.cshp.ShipServiceClient;
import com.fedex.rscs.client.rsss.RSSSClient;
import com.fedex.rscs.client.srrs.SRRSClient;
import com.fedex.rscs.dto.RequestedPaymentType;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.ServiceProcessorTerminalException;
import com.fedex.rscs.exception.client.ClientTerminalException;
import com.fedex.rscs.exception.client.ClientTransientException;
import com.fedex.rscs.model.OpenShipment;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.cshp.CompleteShipmentDetail;
import com.fedex.rscs.model.cshp.OpenShipmentResponse;
import com.fedex.rscs.model.cshp.PreauthorizedRateData;
import com.fedex.rscs.model.cshp.ShippingRateData;
import com.fedex.rscs.model.cshp.TransactionStatus;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;
import com.fedex.rscs.repository.OpenShipmentRepository;

/**
 * Implementation of OpenShipmentTransaction to create a open and modify shipment in CSHP
 * 
 * @author 3798910
 *
 */
@Service
public class OpenShipmentTransactionServiceImpl implements OpenShipmentTransactionService {

    private static final Logger logger = LoggerFactory.getLogger(OpenShipmentTransactionServiceImpl.class);

    private final ShipServiceClient shipServiceClient;
    private final OpenShipmentTransactionServiceMapper shipmentMapper;
    private final OpenShipmentRepository openShipmentRepository;
    private final SRRSClient srrsClient;
    private final RSSSClient rsssClient;

    @Autowired
    public OpenShipmentTransactionServiceImpl(final ShipServiceClient shipServiceClient,
            final OpenShipmentTransactionServiceMapper shipmentMapper,
            final OpenShipmentRepository openShipmentRepository, final SRRSClient srrsClient, final RSSSClient rsssClient) {

        this.shipServiceClient = shipServiceClient;
        this.shipmentMapper = shipmentMapper;
        this.openShipmentRepository = openShipmentRepository;
        this.srrsClient = srrsClient;
        this.rsssClient = rsssClient;
    }

    /**
     * It will set the mandatory fields in the RequestedShipmentDetail with the help of service
     * mapper and then it will call the client method to create open shipment in CSHP and in
     * response we will get the index and tracking number
     * 
     */
    @Override
    public OpenShipmentResponse create(
            RequestedShipmentDetail requestedShipmentDetail,
            WorkstationDetail workstationDetail) {

        try {

            String accountNumber = workstationDetail.getAccountNumber();
            if (requestedShipmentDetail.getInterlineShippingData() != null
                    && requestedShipmentDetail.getShippingChargesPayment()
                            .getPaymentType()
                            .equals(RequestedPaymentType.NON_ACCOUNT.name())) {

                accountNumber = rsssClient.validateInterlineAccounts(requestedShipmentDetail.getInterlineShippingData()
                        .getInterlineId());
                requestedShipmentDetail.getInterlineShippingData().setAccountNumber(accountNumber);
                
            } else {

                requestedShipmentDetail.setInterlineShippingData(null);
                requestedShipmentDetail.getShippingChargesPayment()
                        .setInterlineId(null);
            }

            PreauthorizedRateData preauthorizedRateData =
                    srrsClient.getPreauthorizedRates(workstationDetail, requestedShipmentDetail);

            ShippingRateData shippingRateData =
                    shipmentMapper.mapPreAuthorizedRateDataToShippingRateData(preauthorizedRateData);

            RequestedShipmentDetail requestedShipment = shipmentMapper
                    .createRequestedShipmentDetail(requestedShipmentDetail, accountNumber, shippingRateData);

            CompleteShipmentDetail completeShipmentDetail =
                    shipServiceClient.createOpenShipment(requestedShipment, workstationDetail);

            OpenShipmentResponse openShipmentResponse = shipmentMapper
                    .createOpenShipmentResponse(completeShipmentDetail, requestedShipmentDetail, shippingRateData, preauthorizedRateData);

            OpenShipment openShipment =
                    shipmentMapper.createOpenShipment(openShipmentResponse, workstationDetail, requestedShipmentDetail);
            boolean saveResponse =
                    saveOpenShipment(openShipment, requestedShipmentDetail, completeShipmentDetail, workstationDetail);
            if (!saveResponse) {

                logger.error(
                        "Unable to insert data when RequestedShipmentDetail instance has state : {} , workstationDetail instance has state {} and OpenShipment has state {}",
                        requestedShipmentDetail, workstationDetail, openShipment);
                // delete open shipment from CSHP
                deleteOpenShipmentClient(requestedShipmentDetail.getShipTimestamp(),
                        requestedShipmentDetail.getTransactionLocationCode(), completeShipmentDetail.getIndex(),
                        workstationDetail);
                throw new ServiceProcessorTerminalException(ServiceErrorCode.DB_INSERTION_FAILURE);

            }

            return openShipmentResponse;

        } catch (ClientTransientException e) {

            logger.error("ClientTransientException received from CSHP.createOpenShipment(..) call {}",
                    requestedShipmentDetail, e);
            throw new ServiceProcessorTerminalException(e.getShipmentCreationErrorCode());

        } catch (ClientTerminalException e) {

            logger.error("ClientTerminalException exception received from CSHP.createOpenShipment(..) call {}",
                    requestedShipmentDetail, e);
            throw new ServiceProcessorTerminalException(e.getShipmentCreationErrorCode());

        } catch (HystrixRuntimeException e) {

            logger.error("Unexpected response received from SRRS.rateqoute(..) call {}", requestedShipmentDetail, e);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.RATING_SERVICE_UNAVAILABLE);

        }
    }

    /**
     * This method is created to save the Open Shipment
     * 
     * @param openShipment
     * @param completeShipmentDetail
     * @param requestedShipmentDetail
     * @param workstationDetail
     * @return boolean
     */
    private boolean saveOpenShipment(
            OpenShipment openShipment,
            RequestedShipmentDetail requestedShipmentDetail,
            CompleteShipmentDetail completeShipmentDetail,
            WorkstationDetail workstationDetail) {

        boolean isResponseSaved = false;
        try {

            isResponseSaved = openShipmentRepository.save(openShipment);

        } catch (TerminalDbAccessException | TransientDbAccessException ex) {
            logger.error("Unable to connect with DB to save open shipment details {}", openShipment, ex);
            // delete open shipment from CSHP
            deleteOpenShipmentClient(requestedShipmentDetail.getShipTimestamp(),
                    requestedShipmentDetail.getTransactionLocationCode(), completeShipmentDetail.getIndex(),
                    workstationDetail);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.DB_INSERTION_FAILURE);

        } catch (HystrixRuntimeException ex) {

            logger.error("Unable to connect with DB to retrieve open shipment details {}", openShipment, ex);
            // delete open shipment from CSHP
            deleteOpenShipmentClient(requestedShipmentDetail.getShipTimestamp(),
                    requestedShipmentDetail.getTransactionLocationCode(), completeShipmentDetail.getIndex(),
                    workstationDetail);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.SYSTEM_UNAVAILABLE);

        }
        return isResponseSaved;
    }

    /**
     * Method to retrieve the open shipment from database against given referenceId, locationId and
     * trackingId by calling Repository layer
     */
    @Override
    public OpenShipmentResponse retrieveOpenShipment(
            String referenceId,
            String locationId,
            String trackingId) {

        OpenShipmentResponse retrieveOpenShipmentResponse = null;
        try {
            OpenShipment openShipment = openShipmentRepository.retrieve(referenceId, locationId, trackingId);
            if (openShipment.getOpenShipmentStatusCode()
                    .equals(TransactionStatus.CREATED.name())) {
                retrieveOpenShipmentResponse = shipmentMapper.retrieveOpenShipmentResponse(openShipment);
            } else if (openShipment.getOpenShipmentStatusCode()
                    .equals(TransactionStatus.CONFIRMED.name())) {
                logger.error("Shipment already confirmed from the database");
                throw new ServiceProcessorTerminalException(ServiceErrorCode.OPEN_SHIPMENT_STATUS_CONFIRMED);
            } else {
                logger.error("Shipment already deleted from the database");
                throw new ServiceProcessorTerminalException(ServiceErrorCode.OPEN_SHIPMENT_STATUS_DELETED);
            }
        } catch (TerminalDbAccessException | TransientDbAccessException ex) {

            logger.warn("Unable to retrieve record against given referenceId : {}, locationId : {} and trackingId : {}",
                    referenceId, locationId, trackingId);
            logger.error("No records found in database against given referenceId, locationId and trackingId", ex);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.DB_SEARCH_FAILURE);
        } catch (HystrixRuntimeException ex) {

            logger.error("Unable to connect with DB to retrieve open shipment details {}", ex);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.SYSTEM_UNAVAILABLE);
        }

        return retrieveOpenShipmentResponse;
    }

    /**
     * Method to delete the open shipment from database against given requestedDateTime,
     * locationCode and index and workstationDetail by calling client layer
     */
    public void deleteOpenShipmentClient(
            String requestedDateTime,
            String locationCode,
            String index,
            WorkstationDetail workstationDetail) {

        try {
            shipServiceClient.deleteOpenShipment(shipmentMapper.deleteOpenShipmentRequest(requestedDateTime,
                    locationCode, index, workstationDetail));

        } catch (ClientTransientException | ClientTerminalException e) {

            logger.error(
                    "Unexpected response received from CSHP.deleteOpenShipment(..) call for requestedDateTime : {}, locationCode : {} , index : {}, workstaion Details : {} ",
                    requestedDateTime, locationCode, index, workstationDetail, e);
        }
    }
    
    /**
     * Method to delete the open shipment from database and also make a call to CSHP delete against
     * given referenceId, locationId and trackingId by calling Repository layer
     */
    @Override
    public void deleteOpenShipment(
            String referenceId,
            String locationId,
            String trackingId) {

        OpenShipment retrieveOpenShipment = null;
        boolean isDBStatusUpdated;
        try {
            retrieveOpenShipment = openShipmentRepository.retrieve(referenceId, locationId, trackingId);
            if (retrieveOpenShipment.getOpenShipmentStatusCode()
                    .equals(TransactionStatus.CREATED.name())) {
                isDBStatusUpdated = openShipmentRepository.updateStatus(new UpdateOpenShipmentStatusData(referenceId,
                        locationId, trackingId, TransactionStatus.VOID.name(), retrieveOpenShipment.getFedexId()));
            } else if (retrieveOpenShipment.getOpenShipmentStatusCode()
                    .equals(TransactionStatus.CONFIRMED.name())) {
                logger.error("Shipment already confirmed from the database");
                throw new ServiceProcessorTerminalException(ServiceErrorCode.OPEN_SHIPMENT_STATUS_CONFIRMED);
            } else {
                logger.error("Shipment already deleted from the database");
                throw new ServiceProcessorTerminalException(ServiceErrorCode.OPEN_SHIPMENT_STATUS_DELETED);
            }
        } catch (TerminalDbAccessException | TransientDbAccessException ex) {
            logger.error("Unable to delete records from database against given referenceId, locationId and trackingId",
                    ex);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.DB_DELETION_FAILURE);
        } catch (HystrixRuntimeException ex) {
            logger.error(
                    "Unable to connect with DB for given referenceId : {}, locationId : {} and trackingId : {} if openShipment retrieved : {}",
                    referenceId, locationId, trackingId, retrieveOpenShipment, ex);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.SYSTEM_UNAVAILABLE);
        }

        if (isDBStatusUpdated) {

            WorkstationDetail workstationDetail =
                    OpenShipmentTransactionMapper.INSTANCE.mapToWorkstationDetail(retrieveOpenShipment);
            workstationDetail.setAppVersionId(shipmentMapper.getAppVersionIdFromCreateOpenShipment(
                    openShipmentRepository.getOpenShipmentDataFromBlob(retrieveOpenShipment.getShipmentData())));

            // Make a call to CSHP deleteOpenShipment
            deleteOpenShipmentClient(ZonedDateTime.now()
                    .format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")), locationId, referenceId,
                    workstationDetail);
        } else {
            logger.error(
                    "Unable to delete records from database against given referenceId : {}, locationId : {} and trackingId : {} if openShipment retrieved : {}",
                    referenceId, locationId, trackingId, retrieveOpenShipment);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.DB_DELETION_FAILURE);
        }
    }

}
