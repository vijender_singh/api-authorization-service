package com.fedex.rscs.service.task;

import java.util.concurrent.Callable;

import com.fedex.rscs.dto.NotificationType;
import com.fedex.rscs.exception.ServiceProcessorTerminalException;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.common.EventNotification;
import com.fedex.rscs.model.cshp.ConfirmedShipmentData;
import com.fedex.rscs.model.cshp.TrackingSequenceDetail;
import com.fedex.rscs.service.ConfirmShipmentTransactionService;


/**
 * This task provides the functionality to checkout a shipment asynchronously.
 * 
 * @author 981840
 *
 */
public class ShipmentCheckoutTask implements Callable<ConfirmedShipmentData> {

    private ConfirmShipmentTransactionService transactionService;
    private ShipmentConfirmationRequest lineItem;

    public ShipmentCheckoutTask(ConfirmShipmentTransactionService transactionService,
            ShipmentConfirmationRequest lineItem) {
        super();
        this.transactionService = transactionService;
        this.lineItem = lineItem;
    }

    /*
     * This methods is called when a new thread is initialized and started for this task.
     * 
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public ConfirmedShipmentData call()
            throws Exception {

        ConfirmedShipmentData shipmentData = null;
        try {
            //process the shipment line item. 
            shipmentData = this.transactionService.confirmShipment(lineItem);
            shipmentData.setLineItemSuccess(true);

        } catch (ServiceProcessorTerminalException exception) {

            shipmentData = new ConfirmedShipmentData();
            shipmentData.setMasterTrackingId(new TrackingSequenceDetail(lineItem.getTrackingId(), null, 0));
            shipmentData.addNotification(new EventNotification(exception.getShipmentCreationErrorCode()
                    .getApiErrorCode(),
                    exception.getShipmentCreationErrorCode()
                            .getMessage(),
                    NotificationType.ERROR));
        }

        return shipmentData;
    }
    
}
