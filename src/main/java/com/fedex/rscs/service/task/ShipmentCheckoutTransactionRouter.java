package com.fedex.rscs.service.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fedex.common.cxs.exception.TerminalProcessorException;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.cshp.ConfirmedShipmentData;
import com.fedex.rscs.service.ConfirmShipmentTransactionService;
import com.fedex.rscs.service.TransactionRouter;

/**
 * This component provides the routing capability to the shipment transaction that has multiple
 * shipping line items and can be used to process the multiple line items in parallel.
 * 
 * @author 981840
 *
 */
@Component
public class ShipmentCheckoutTransactionRouter
        implements TransactionRouter<ShipmentConfirmationRequest, ConfirmedShipmentData>{

    private static final Logger logger = LoggerFactory.getLogger(ShipmentCheckoutTransactionRouter.class);
   
    private final ExecutorService executorService ;
    private final ConfirmShipmentTransactionService transactionService;

    @Autowired
    public ShipmentCheckoutTransactionRouter(ConfirmShipmentTransactionService transactionService,ExecutorService executorService) {
        this.transactionService = transactionService;
        this.executorService=executorService;
    }

    @Override
    public Map<String, ConfirmedShipmentData> join(
            Map<String, Future<ConfirmedShipmentData>> responseMap) {

        logger.debug("joining the transaction routes to provide the final result.");
        Map<String, ConfirmedShipmentData> result = new HashMap<>();
        for (Map.Entry<String, Future<ConfirmedShipmentData>> entry : responseMap.entrySet()) {
            // getting response from FutureTask
            ConfirmedShipmentData response = null;
            while (!entry.getValue()
                    .isDone()) {
                logger.trace("waiting for transaction routes to complete their journey.");
            }
            try {

                response = entry.getValue()
                        .get();
            } catch (InterruptedException | ExecutionException e) {
                Thread.currentThread()
                        .interrupt();
                throw new TerminalProcessorException("failed to process and join transaction routes.", e);
            }
            if (response != null) {
                result.put(entry.getKey(), response);
            }
        }

        return result;
    }
    
    @Override
    public Map<String, Future<ConfirmedShipmentData>> fork(
            List<ShipmentConfirmationRequest> request) {

        logger.debug("forking the transactions to multiple routes.");
        Map<String, Future<ConfirmedShipmentData>> tasks = new HashMap<>();
        request.forEach(lineItem -> {
            ShipmentCheckoutTask checkoutTask = new ShipmentCheckoutTask(transactionService, lineItem);
            tasks.put(lineItem.getTrackingId(), executorService.submit(checkoutTask));
        });
        return tasks;
    }
    
}
