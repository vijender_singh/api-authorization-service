package com.fedex.rscs.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import com.fedex.common.cxs.exception.TerminalProcessorException;

/**
 * This interface can be used to provide implementation logic to route and handle transaction.
 * 
 * @author 981840
 *
 * @param <T> represents the type of line item to be processed in underline transaction.
 * @param <V> represents the transaction response type of underline transaction.
 */
public interface TransactionRouter<T, V> {

    /**
     * This method provides the default implementation to provide logic to process the transaction.
     * 
     * @param request
     * @param transactionHeaderDetails
     * @param locationDetails
     * @return
     */
    public default Map<String, V> process(
            List<T> request) {

        Map<String, Future<V>> tasks = this.fork(request);
        return join(tasks);

    }

    /**
     * Implement this method to provide logic to fork the transaction to route line items into
     * multiple routes for parallel processing.
     * 
     * @param request
     * @param transactionHeaderDetails
     * @param locationDetails
     * @return
     */
    public Map<String, Future<V>> fork(
            List<T> request);

    /**
     * Method with default implementation to provide mechanism to join the transaction forked to
     * multiple routes.
     * 
     * @param responseMap
     * @return
     */
    public default Map<String, V> join(
            Map<String, Future<V>> transactionRoutes) {

        Map<String, V> result = new HashMap<>();
        for (Map.Entry<String, Future<V>> transactionRoute : transactionRoutes.entrySet()) {
            // getting response from FutureTask
            V response = null;
            try {
                while (!transactionRoute.getValue()
                        .isDone()) {
                    // wait for transaction route to reach to final state.
                }
                response = transactionRoute.getValue()
                        .get();
            } catch (Exception e) {
                throw new TerminalProcessorException("failed to process and join transaction routes.", e);
            }
            if (response != null) {
                result.put(transactionRoute.getKey(), response);
            }
        }

        return result;
    }

}

