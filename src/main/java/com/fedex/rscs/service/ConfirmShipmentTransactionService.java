package com.fedex.rscs.service;

import com.fedex.rscs.model.DeleteSoldShipmentData;
import com.fedex.rscs.model.DeletedSoldShipmentDetail;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.cshp.ConfirmedShipmentData;

/**
 * Service Layer to confirm an open shipment in CSHP.
 * 
 * @author 5034922
 *
 */
public interface ConfirmShipmentTransactionService {

    /**
     * This method is use to update the shipment status from open to confirmed.
     * 
     * @param request
     * @param locationDetail
     * @return
     */
    ConfirmedShipmentData confirmShipment(
            ShipmentConfirmationRequest request);

    /**
     * This method is use to stop the shipment.
     * 
     * @param deleteSoldShipmentData
     * @return DeletedSoldShipmentDetail
     */
    DeletedSoldShipmentDetail stop(
            DeleteSoldShipmentData deleteSoldShipmentData);
    
}
