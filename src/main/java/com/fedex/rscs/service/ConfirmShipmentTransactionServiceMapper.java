package com.fedex.rscs.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fedex.rscs.client.pes.dto.PAAIndicatorType;
import com.fedex.rscs.dto.CarrierCodeType;
import com.fedex.rscs.dto.CarrierType;
import com.fedex.rscs.dto.PackageSpecialHandlingType;
import com.fedex.rscs.model.InterlineShipmentDetail;
import com.fedex.rscs.model.OpenShipment;
import com.fedex.rscs.model.OpenShipmentData;
import com.fedex.rscs.model.OpenShipmentDetail;
import com.fedex.rscs.model.OperatingCompany;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.RequestedPackageLineItemDetail;
import com.fedex.rscs.model.RequestedPackageSpecialServices;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ServiceConstant;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.ShipmentContactDetail;
import com.fedex.rscs.model.ShipmentDetail;
import com.fedex.rscs.model.ShipmentPackageAdditionalService;
import com.fedex.rscs.model.ShipmentPackageDetail;
import com.fedex.rscs.model.SpecialServiceDescriptionDetail;
import com.fedex.rscs.model.SpecialServiceDetail;
import com.fedex.rscs.model.VariationOptionDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.common.DeviceDetail;
import com.fedex.rscs.model.common.LocationDataDetail;
import com.fedex.rscs.model.common.OfferingDetail;
import com.fedex.rscs.model.common.PackageCancellationEventDetailRequest;
import com.fedex.rscs.model.common.PackageLineItemDetail;
import com.fedex.rscs.model.common.PackagePossessionEventDetail;
import com.fedex.rscs.model.common.PackagePossessionEventDetailRequest;
import com.fedex.rscs.model.common.PersonDetail;
import com.fedex.rscs.model.common.TransactionHeaderDetail;
import com.fedex.rscs.model.cshp.ActionType;
import com.fedex.rscs.model.cshp.CompleteShipmentDetail;
import com.fedex.rscs.model.cshp.CompletedPackageData;
import com.fedex.rscs.model.cshp.ConfirmedPackageData;
import com.fedex.rscs.model.cshp.ConfirmedShipmentData;
import com.fedex.rscs.model.cshp.EPaymentMode;
import com.fedex.rscs.model.cshp.Price;
import com.fedex.rscs.model.cshp.RatingTypeCode;
import com.fedex.rscs.model.cshp.TrackingIdDetail;
import com.fedex.rscs.model.cshp.TrackingSequenceDetail;
import com.fedex.rscs.model.cshp.TransactionStatus;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;
import com.fedex.rscs.util.RetailShipmentCreationServiceUtil;

/**
 * This class will set the mandatory values in the provided request/response object of
 * ConfirmShipmentTransactionService.
 * 
 * @author 5034922
 *
 */
@Component
public class ConfirmShipmentTransactionServiceMapper {

   private static final Logger logger = LoggerFactory.getLogger(ConfirmShipmentTransactionServiceMapper.class);
   private static final String COUNTER_SHIPMENT = "COUNTER_SHIPMENT";

   
   private final RetailShipmentCreationServiceUtil serviceUtil;

    /**
     * @param serviceUtil
     */
    public ConfirmShipmentTransactionServiceMapper(final RetailShipmentCreationServiceUtil serviceUtil) {

        this.serviceUtil = serviceUtil;
    }

    @Autowired
    ObjectMapper objectMapper;
    
    private static final String BUSINESS_SERVICE_CENTER = "BUSINESS_SERVICE_CENTER";
    
    public ConfirmedShipmentData buildResponse(
            CompleteShipmentDetail reply) {


        ConfirmedShipmentData data = null;
        if (reply != null) {
            data = new ConfirmedShipmentData();
            CarrierCodeType code = CarrierCodeType.valueOf(reply.getCarrierCode());
            data.setCarrierCode(code.name());
            data.setCarrierType(code.name()
                    .equalsIgnoreCase(CarrierCodeType.FDXE.name()) ? CarrierType.EXPRESS.name()
                            : CarrierType.GROUND.name());

            data.setPackageCount(reply.getPackageCount());
            if (!CollectionUtils.isEmpty(reply.getCompletedPackageDetails())) {
                updatePackageLineItemAndMasterTrackingIdDtl(reply.getCompletedPackageDetails()
                        .get(0), reply, data);
            }
        }
        return data;
    }

    /**
     * Method to set packageLine item and master tracking id details
     * 
     * @param completedPackageData
     * @param details
     * @param data
     * @return
     */
    private void updatePackageLineItemAndMasterTrackingIdDtl(
            CompletedPackageData completedPackageData,
            CompleteShipmentDetail details,
            ConfirmedShipmentData data) {

        List<ConfirmedPackageData> packageDetails = null;
        ConfirmedPackageData packageData = null;
        TrackingSequenceDetail trackingSequence = null;

        if (completedPackageData != null) {
            packageDetails = new ArrayList<>();
            packageData = new ConfirmedPackageData();
            if (!CollectionUtils.isEmpty(completedPackageData.getTrackingIds())
                    && completedPackageData.getOperationalDetail() != null
                    && completedPackageData.getOperationalDetail()
                            .getBarcode() != null
                    && !CollectionUtils.isEmpty(completedPackageData.getOperationalDetail()
                            .getBarcode()
                            .getStringBarcodes())) {

                final String barcode = completedPackageData.getOperationalDetail()
                        .getBarcode()
                        .getStringBarcodes()
                        .get(0)
                        .getValue();
                TrackingIdDetail trckId = completedPackageData.getTrackingIds()
                        .get(0);
                trackingSequence = new TrackingSequenceDetail(trckId.getTrackingNumber(), barcode, 1);
            }

            packageData.setTrackingId(trackingSequence);
            packageData.setMaster(true);
            if (!CollectionUtils.isEmpty(details.getShipmentDocuments())) {
                packageData.setLabel(details.getShipmentDocuments()
                        .get(0));
            }
            packageDetails.add(packageData);
        }

        data.setPackageLineItems(packageDetails);
        data.setMasterTrackingId(trackingSequence);
    }

    /**
     * Build Confirmed Shipment response data
     * 
     * @param shipmentDetail
     * @return
     */
    public ConfirmedShipmentData buildConfirmedShipmentResponse(
            ShipmentDetail shipmentDetail) {

        ConfirmedShipmentData data = null;
        if (shipmentDetail != null) {
            data = new ConfirmedShipmentData();
            if (!StringUtils.isEmpty(shipmentDetail.getOpcoShippingServiceName())) {

                data.setCarrierType(shipmentDetail.getOpcoShippingServiceName()
                        .equalsIgnoreCase(OperatingCompany.FEDEX_EXPRESS.name()) ? CarrierType.EXPRESS.name()
                                : CarrierType.GROUND.name());
                data.setCarrierCode(shipmentDetail.getOpcoShippingServiceName()
                        .equalsIgnoreCase(OperatingCompany.FEDEX_EXPRESS.name()) ? CarrierCodeType.FDXE.name()
                                : CarrierCodeType.FDXG.name());
            }
            data.setPackageCount(shipmentDetail.getTotalPackageQuantity());
            data.setPackageLineItems(
                    createPackageLineItemsAndMasterTrkngId(shipmentDetail.getShipmentPackageDetails(), data));
            if (shipmentDetail.getCurrentShipmentStatus() != null) {

                data.setShipmentStatus(TransactionStatus.valueOf(shipmentDetail.getCurrentShipmentStatus()));
            }
        }
        return data;
    }

    /**
     * Build package line items for confirmed shipment response data
     * 
     * @param shipmentPackageDetails
     * @param data
     * @return
     */
    private List<ConfirmedPackageData> createPackageLineItemsAndMasterTrkngId(
            List<ShipmentPackageDetail> shipmentPackageDetails,
            ConfirmedShipmentData data) {

        List<ConfirmedPackageData> packageDetails = new ArrayList<>();
        ConfirmedPackageData packageData = null;
        TrackingSequenceDetail trackingSequence = null;

        for (ShipmentPackageDetail shipmentPackageDetail : shipmentPackageDetails) {

            packageData = new ConfirmedPackageData();
            trackingSequence = new TrackingSequenceDetail(shipmentPackageDetail.getTrackingNumber(),
                    shipmentPackageDetail.getBarcode(), shipmentPackageDetail.getSeqNumber());
            if (shipmentPackageDetail.isMasterFlag()) {
                data.setMasterTrackingId(trackingSequence);
                packageData.setMaster(true);
            }
            packageData.setTrackingId(trackingSequence);
            packageDetails.add(packageData);

        }
        return packageDetails;
    }

    /**
     * Method to create ShipmentDetail object to save in database
     * 
     * @param openShipment
     * @param request
     * @param locationDetail
     * @param confirmedShipmentData
     * @param data
     * @return
     */
    public ShipmentDetail buildShipmentDetails(
            OpenShipment openShipment,
            ShipmentConfirmationRequest request,
            LocationDataDetail locationDetail,
            CompleteShipmentDetail completeShipmentDetail,
            ConfirmedShipmentData data) {

        ShipmentDetail shipmentDetail = new ShipmentDetail();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        OpenShipmentData openShipmentData = new OpenShipmentData();
        try {
            openShipmentData = objectMapper.readValue(openShipment.getShipmentData(), OpenShipmentData.class);
        } catch (IOException e) {
            logger.error("Exception occured while mapping json {}", e.getMessage());
        }

        OpenShipmentDetail openShipmentDetail = openShipmentData.getOpenShipment();

        shipmentDetail.setShipmentTrackingNumber(completeShipmentDetail.getMasterTrackingId()
                .getTrackingNumber());
        shipmentDetail.setAcceptTmStmp(request.getShipmentTimeStamp());
        shipmentDetail.setInitialAcceptanceTmStmp(request.getShipmentTimeStamp());
        shipmentDetail.setFedexId(request.getWorkstationDetail()
                .getTeamMemberId());
        shipmentDetail.setFedexLocationId(request.getTransactionLocationId());
        shipmentDetail.setOriginCountryCode(locationDetail.getTransactionLocationInfo()
                .getAddress()
                .getCountryCode());
        shipmentDetail.setDevicePlatformType(ServiceConstant.DEVICE_PLATFORM_TYPE);
        shipmentDetail.setApiClientId(serviceUtil.getApiClientId(request.getWorkstationDetail()
                .getAppName(),
                request.getWorkstationDetail()
                        .getAppVersionId()));
        shipmentDetail.setApiVersionId(ServiceConstant.API_VERSION_ID);
        shipmentDetail.setApiClientTypeName(request.getWorkstationDetail()
                .getAppName());
        shipmentDetail.setDeviceId(request.getWorkstationDetail()
                .getDeviceId());
        shipmentDetail.setOpCoCode(locationDetail.getOpCo());
        shipmentDetail.setOpCoLocationId(locationDetail.getOpCoLocationId());
        shipmentDetail.setNonFedexPackageType(openShipmentDetail.getPhysicalPackagingType());
        shipmentDetail.setStandardPackageFlag(StringUtils.isEmpty(openShipmentDetail.getPhysicalPackagingType()));
        shipmentDetail.setTransactionShipmentRefId(request.getWorkstationDetail()
                .getGuid());
        shipmentDetail.setGroundAccountNumber(request.getWorkstationDetail()
                .getGroundAccountNumber());
        shipmentDetail.setCityCenterAccountNumber(request.getWorkstationDetail()
                .getAccountNumber());
        shipmentDetail.setMeterNumber(request.getWorkstationDetail()
                .getMeterNumber());
        shipmentDetail.setSoftwareId(request.getWorkstationDetail()
                .getSoftwareId());
        if (request.getPaymentDetail()
                .getPaymentType() != null) {
            shipmentDetail.setPaymentType(request.getPaymentDetail()
                    .getPaymentType()
                    .replace("_", ""));
        }
        shipmentDetail.setCustomerAccountNumber(openShipmentDetail.getPaymentDetails()
                .getPayor()
                .getAccountNumber());
        shipmentDetail.setBillToAccountType(openShipmentDetail.getPaymentDetails()
                .getResponsibleParty());
        shipmentDetail.setTotalPackageQuantity(completeShipmentDetail.getPackageCount());
        if (openShipmentDetail.getSpecialServicesRequested() != null && openShipmentDetail.getSpecialServicesRequested()
                .getHoldAtLocationDetail() != null) {
            shipmentDetail.setHalFedexLocationId(openShipmentDetail.getSpecialServicesRequested()
                    .getHoldAtLocationDetail()
                    .getLocationId());
        }
        shipmentDetail.setRatingType(RatingTypeCode.ONE_RATE.name()
                .equals(openShipmentDetail.getRateDetails()
                        .getRatingType()) ? RatingTypeCode.ONE_RATE.getRatingCode()
                                : RatingTypeCode.STANDARD_RATE.getRatingCode());
        shipmentDetail.setPosTransactionRefId(request.getPaymentDetail()
                .getPaymentRefId());
        shipmentDetail.setFedexPackageType(openShipmentDetail.getPackagingType());

        shipmentDetail.setShippingServiceName(openShipmentDetail.getRateDetails()
                .getServiceDetails()
                .getServiceType());
        shipmentDetail.setCurrency(openShipmentDetail.getTotalInsuredValue()
                .getCurrency());
        shipmentDetail.setWeightUom(openShipmentDetail.getTotalWeight()
                .getUnit());
        if (!CollectionUtils.isEmpty(openShipmentDetail.getPackageLineItems()) && openShipmentDetail.getPackageLineItems()
                .get(0)
                .getDimensions() != null) {
            shipmentDetail.setDimensionUom(openShipmentDetail.getPackageLineItems()
                    .get(0)
                    .getDimensions()
                    .getUnits());
        }
        shipmentDetail.setChargeAmount(openShipmentDetail.getRateDetails()
                .getTotalBaseCharges());
        shipmentDetail.setDiscountAmount(openShipmentDetail.getRateDetails()
                .getTotalFreightDiscounts());
        shipmentDetail.setSurchargeAmount(openShipmentDetail.getRateDetails()
                .getTotalSurcharges());
        shipmentDetail.setTaxAmount(openShipmentDetail.getRateDetails()
                .getTotalTaxes());
        shipmentDetail.setNetChargeAmount(openShipmentDetail.getRateDetails()
                .getTotalNetCharges());

        // The format of commitTimestamp will be YYYY-MM-DDTHH:MM:SS (2020-07-30T11:55:28)
        shipmentDetail.setDelCommitDate(openShipmentDetail.getRateDetails()
                .getCommitDetails()
                .getCommitTimestamp());
        shipmentDetail.setShipmentContactDetail(buildShipmentContactDetail(openShipmentDetail,
                request.getShipmentTimeStamp(), completeShipmentDetail.getMasterTrackingId()
                        .getTrackingNumber()));
        shipmentDetail.setCurrentShipmentStatTmStmp(request.getShipmentTimeStamp());
        shipmentDetail.setCurrentShipmentStatus(openShipment.getOpenShipmentStatusCode());
        shipmentDetail.setShipmentPackageDetails(
                buildShipmentPackageDetails(completeShipmentDetail, openShipmentDetail, request, data));
        shipmentDetail.setOpcoShippingServiceName(completeShipmentDetail.getCarrierCode()
                .equals(CarrierCodeType.FDXE.name()) ? OperatingCompany.FEDEX_EXPRESS.name()
                        : OperatingCompany.FEDEX_GROUND.name());
        if (OperatingCompany.FEDEX_EXPRESS.name()
                .equalsIgnoreCase(shipmentDetail.getOpcoShippingServiceName())) {
            shipmentDetail.setAfterPickupFlag(locationDetail.isExpressAfterPickup());
        } else {
            shipmentDetail.setAfterPickupFlag(locationDetail.isGroundAfterPickup());
        }
        shipmentDetail.setShippingServiceDesc(openShipmentDetail.getRateDetails()
                .getServiceDetails()
                .getServiceName());
        if (openShipmentDetail.getInterlineShippingData() != null) {
            shipmentDetail.setInterlineShipmentDetail(buildInterlineShipmentDetail(openShipmentDetail,
                    request.getShipmentTimeStamp(), completeShipmentDetail.getMasterTrackingId()
                            .getTrackingNumber()));
        }
        return shipmentDetail;
    }

    /**
     * Method to create ShipmentPackageDetail list to be stored in DB
     * 
     * @param completeShipmentDetail
     * @param openShipmentDetail
     * @param request 
     * @param data
     * @return
     */
    private List<ShipmentPackageDetail> buildShipmentPackageDetails(
            CompleteShipmentDetail completeShipmentDetail,
            OpenShipmentDetail openShipmentDetail,
            ShipmentConfirmationRequest request,
            ConfirmedShipmentData data) {

        List<ShipmentPackageDetail> shipmentPackageDetails = new ArrayList<>();
        ShipmentPackageDetail shipmentPackageDetail = new ShipmentPackageDetail();
        List<CompletedPackageData> completePackageDetails = openShipmentDetail.getPackageLineItems();
        for (CompletedPackageData completedPackageData : completePackageDetails) {

            shipmentPackageDetail.setMasterFlag(false);
            shipmentPackageDetail.setManualWeightFlag(true);
            shipmentPackageDetail.setAcceptTmStmp(request.getShipmentTimeStamp());
            shipmentPackageDetail.setFormId(completeShipmentDetail.getMasterTrackingId()
                    .getFormId());
            shipmentPackageDetail.setBarcode(data.getMasterTrackingId()
                    .getBarcode());
            shipmentPackageDetail.setAmount(Double.parseDouble(completedPackageData.getInsuredValue()
                    .getAmount()));
            shipmentPackageDetail.setWeight(Double.parseDouble(completedPackageData.getWeight()
                    .getValue()));
            if(openShipmentDetail.getPackageLineItems().get(0).getDimensions()!=null) {
            shipmentPackageDetail.setHeightQuantity(completedPackageData.getDimensions()
                    .getHeight());
            shipmentPackageDetail.setLengthQuantity(completedPackageData.getDimensions()
                    .getLength());
            shipmentPackageDetail.setSeqNumber(completedPackageData.getSequenceNumber());
            shipmentPackageDetail.setWidthQuantity(completedPackageData.getDimensions()
                    .getWidth());
            }
            shipmentPackageDetail.setShipmentTrackingNumber(completeShipmentDetail.getMasterTrackingId()
                    .getTrackingNumber());
            shipmentPackageDetail.setTrackingNumber(completeShipmentDetail.getMasterTrackingId()
                    .getTrackingNumber());
            shipmentPackageDetail.setPaaDamageStatText(
                    PackageSpecialHandlingType.valueOf(completedPackageData.getSpecialHandlingDetail())
                            .getDamageStatus());
            shipmentPackageDetail.setPaaInspectionStatText(
                    PackageSpecialHandlingType.valueOf(completedPackageData.getSpecialHandlingDetail())
                            .getInspectionStatus());
            shipmentPackageDetail.setPaaPackByText(
                    PackageSpecialHandlingType.valueOf(completedPackageData.getSpecialHandlingDetail())
                            .getPackedBy());
            if(completedPackageData.getDryIceWeightDetails()!=null) {
            shipmentPackageDetail.setDryIceWeight(Double.parseDouble(completedPackageData.getDryIceWeightDetails()
                    .getValue()));
            }
            shipmentPackageDetail.setShipmentPackageAdditionalServices(
                    buildShipmentPackageAdditionalServices(completeShipmentDetail.getMasterTrackingId()
                            .getTrackingNumber(), request.getShipmentTimeStamp(), completedPackageData));
            shipmentPackageDetails.add(shipmentPackageDetail);
        }
        shipmentPackageDetails.get(0)
                .setMasterFlag(true);
        return shipmentPackageDetails;
    }

    /**
     * Method to set values of List<ShipmentPackageAdditionalService> object
     * 
     * @param trackingNumber
     * @param shipmentTimeStamp
     * @param completedPackageData
     * @return
     */
    private List<ShipmentPackageAdditionalService> buildShipmentPackageAdditionalServices(
            String trackingNumber,
            String shipmentTimeStamp,
            CompletedPackageData completedPackageData) {

        List<ShipmentPackageAdditionalService> shipmentPackageAdditionalServices = new ArrayList<>();

        if (!CollectionUtils.isEmpty(completedPackageData.getSpecialServicesDetails())) {
            for (SpecialServiceDetail specialServicesList : completedPackageData.getSpecialServicesDetails()) {
                ShipmentPackageAdditionalService shipmentPackageAdditionalService =
                        new ShipmentPackageAdditionalService();
                shipmentPackageAdditionalService.setAcceptTmStmp(shipmentTimeStamp);
                shipmentPackageAdditionalService.setShipmentTrackingNumber(trackingNumber);
                shipmentPackageAdditionalService.setTrackingNumber(trackingNumber);
                shipmentPackageAdditionalService.setSpecialServiceDescription(specialServicesList.getDescription());
                shipmentPackageAdditionalService
                        .setSpecialSerivceCode(serviceUtil.getSpecialServiceCode(specialServicesList));
                shipmentPackageAdditionalServices.add(shipmentPackageAdditionalService);
            }
        }

        return shipmentPackageAdditionalServices;
    }

    /**
     * Method to create ShipmentContactDetail object
     * 
     * @param openShipmentDetail
     * @param shipmentTimeStamp
     * @param shipmentTrackingNumber
     * @return
     */
    private ShipmentContactDetail buildShipmentContactDetail(
            OpenShipmentDetail openShipmentDetail,
            String shipmentTimeStamp,
            String shipmentTrackingNumber) {

        ShipmentContactDetail shipmentContactDetail = new ShipmentContactDetail();
        shipmentContactDetail.setAcceptTmStmp(shipmentTimeStamp);
        shipmentContactDetail.setShipmentTrackingNumber(shipmentTrackingNumber);
        shipmentContactDetail.setRecipientName(openShipmentDetail.getRecipient()
                .getContact()
                .getFullName());
        shipmentContactDetail.setResidence(openShipmentDetail.getRecipient()
                .getAddress()
                .isResidential());
        shipmentContactDetail.setRecipientCityName(openShipmentDetail.getRecipient()
                .getAddress()
                .getCity());
        shipmentContactDetail.setRecipientStateProvinceCode(openShipmentDetail.getRecipient()
                .getAddress()
                .getStateOrProvinceCode());
        shipmentContactDetail.setRecipientPostalCode(openShipmentDetail.getRecipient()
                .getAddress()
                .getPostalCode());
        shipmentContactDetail.setRecipientCountryName(openShipmentDetail.getRecipient()
                .getAddress()
                .getCountryCode());
        shipmentContactDetail.setRecipientCompanyName(openShipmentDetail.getRecipient()
                .getContact()
                .getCompanyName());
        if (openShipmentDetail.getRecipient()
                .getAddress()
                .getStreetLines()
                .size() == 1) {
            shipmentContactDetail.setRecipientAddress1Text((openShipmentDetail.getRecipient()
                    .getAddress()
                    .getStreetLines()
                    .get(0)));
        }
        if (openShipmentDetail.getRecipient()
                .getAddress()
                .getStreetLines()
                .size() == 2) {
            shipmentContactDetail.setRecipientAddress1Text((openShipmentDetail.getRecipient()
                    .getAddress()
                    .getStreetLines()
                    .get(0)));
            shipmentContactDetail.setRecipientAddress2Text((openShipmentDetail.getRecipient()
                    .getAddress()
                    .getStreetLines()
                    .get(1)));
        }
        if (openShipmentDetail.getRecipient()
                .getAddress()
                .getStreetLines()
                .size() == 3) {
            shipmentContactDetail.setRecipientAddress1Text((openShipmentDetail.getRecipient()
                    .getAddress()
                    .getStreetLines()
                    .get(0)));
            shipmentContactDetail.setRecipientAddress2Text((openShipmentDetail.getRecipient()
                    .getAddress()
                    .getStreetLines()
                    .get(1)));
            shipmentContactDetail.setRecipientAddress3Text((openShipmentDetail.getRecipient()
                    .getAddress()
                    .getStreetLines()
                    .get(2)));
        }
        shipmentContactDetail.setSenderCustomer(openShipmentDetail.isSenderCustomer());
        shipmentContactDetail.setSenderCityName(openShipmentDetail.getShipper()
                .getAddress()
                .getCity());
        shipmentContactDetail.setSenderCompanyName(openShipmentDetail.getShipper()
                .getContact()
                .getCompanyName());
        shipmentContactDetail.setSenderCountryName(openShipmentDetail.getShipper()
                .getAddress()
                .getCountryCode());
        shipmentContactDetail.setSenderName(openShipmentDetail.getShipper()
                .getContact()
                .getFullName());
        shipmentContactDetail.setSenderPostalCode(openShipmentDetail.getShipper()
                .getAddress()
                .getPostalCode());
        shipmentContactDetail.setSenderStateProvinceCode(openShipmentDetail.getShipper()
                .getAddress()
                .getStateOrProvinceCode());
        if (openShipmentDetail.getShipper()
                .getAddress()
                .getStreetLines()
                .size() == 1) {
            shipmentContactDetail.setSenderAddress1Text((openShipmentDetail.getShipper()
                    .getAddress()
                    .getStreetLines()
                    .get(0)));
        }
        if (openShipmentDetail.getShipper()
                .getAddress()
                .getStreetLines()
                .size() == 2) {
            shipmentContactDetail.setSenderAddress1Text((openShipmentDetail.getShipper()
                    .getAddress()
                    .getStreetLines()
                    .get(0)));
            shipmentContactDetail.setSenderAddress2Text((openShipmentDetail.getShipper()
                    .getAddress()
                    .getStreetLines()
                    .get(1)));
        }
        if (openShipmentDetail.getShipper()
                .getAddress()
                .getStreetLines()
                .size() == 3) {
            shipmentContactDetail.setSenderAddress1Text((openShipmentDetail.getShipper()
                    .getAddress()
                    .getStreetLines()
                    .get(0)));
            shipmentContactDetail.setSenderAddress2Text((openShipmentDetail.getShipper()
                    .getAddress()
                    .getStreetLines()
                    .get(1)));
            shipmentContactDetail.setSenderAddress3Text((openShipmentDetail.getShipper()
                    .getAddress()
                    .getStreetLines()
                    .get(2)));
        }
        shipmentContactDetail.setCustomerCompanyName(openShipmentDetail.getCustomer()
                .getContact()
                .getCompanyName());
        shipmentContactDetail.setCustomerCityName(openShipmentDetail.getCustomer()
                .getAddress()
                .getCity());
        shipmentContactDetail.setCustomerCountryName(openShipmentDetail.getCustomer()
                .getAddress()
                .getCountryCode());
        shipmentContactDetail.setCustomerName(openShipmentDetail.getCustomer()
                .getContact()
                .getFullName());
        shipmentContactDetail.setCustomerPhoneExtensionNumber(openShipmentDetail.getCustomer()
                .getContact()
                .getPhoneExtension());
        shipmentContactDetail.setCustomerPhoneNumber(openShipmentDetail.getCustomer()
                .getContact()
                .getPhoneNumber());
        shipmentContactDetail.setCustomerPostalCode(openShipmentDetail.getCustomer()
                .getAddress()
                .getPostalCode());
        shipmentContactDetail.setCustomerStateProvinceCode(openShipmentDetail.getCustomer()
                .getAddress()
                .getStateOrProvinceCode());
        if (openShipmentDetail.getCustomer()
                .getAddress()
                .getStreetLines()
                .size() == 1) {
            shipmentContactDetail.setCustomerAddress1Text(openShipmentDetail.getCustomer()
                    .getAddress()
                    .getStreetLines()
                    .get(0));
        }
        if (openShipmentDetail.getCustomer()
                .getAddress()
                .getStreetLines()
                .size() == 2) {
            shipmentContactDetail.setCustomerAddress1Text(openShipmentDetail.getCustomer()
                    .getAddress()
                    .getStreetLines()
                    .get(0));
            shipmentContactDetail.setCustomerAddress2Text(openShipmentDetail.getCustomer()
                    .getAddress()
                    .getStreetLines()
                    .get(1));
        }
        if (openShipmentDetail.getCustomer()
                .getAddress()
                .getStreetLines()
                .size() == 3) {
            shipmentContactDetail.setCustomerAddress1Text(openShipmentDetail.getCustomer()
                    .getAddress()
                    .getStreetLines()
                    .get(0));
            shipmentContactDetail.setCustomerAddress2Text(openShipmentDetail.getCustomer()
                    .getAddress()
                    .getStreetLines()
                    .get(1));
            shipmentContactDetail.setCustomerAddress3Text(openShipmentDetail.getCustomer()
                    .getAddress()
                    .getStreetLines()
                    .get(2));
        }
        return shipmentContactDetail;
    }
    
    /**
     * Method to create InterlineShipmentDetail object to be stored in DB
     * 
     * @param openShipmentDetail
     * @param shipmentTimeStamp
     * @param shipmentTrackingNumber
     * @return
     */
    private InterlineShipmentDetail buildInterlineShipmentDetail(
            OpenShipmentDetail openShipmentDetail,
            String shipmentTimeStamp,
            String shipmentTrackingNumber) {

        InterlineShipmentDetail interlineShipmentDetail = new InterlineShipmentDetail();
        interlineShipmentDetail.setShipmentTrackingNumber(shipmentTrackingNumber);
        interlineShipmentDetail.setAcceptTmStmp(shipmentTimeStamp);
        interlineShipmentDetail.setInterlineId(openShipmentDetail.getInterlineShippingData()
                .getInterlineId());
        interlineShipmentDetail.setEmployeeId(openShipmentDetail.getInterlineShippingData()
                .getEmployeeId());
        interlineShipmentDetail.setShipmentWeight(openShipmentDetail.getTotalWeight()
                .getValue());
        return interlineShipmentDetail;
    }

    /**
     * Method to build RequestedShipmentDetail object as request for modify open shipment client
     * call
     * 
     * @param openShipment
     * @param request
     * @return
     */
    public RequestedShipmentDetail createRequestedShipmentDetail(
            ShipmentConfirmationRequest request,
            OpenShipment openShipment) {

        RequestedShipmentDetail requestedShipmentDetail = null;
        OpenShipmentDetail openShipmentDetail = null;
        if (openShipment != null) {
            requestedShipmentDetail = new RequestedShipmentDetail();

            requestedShipmentDetail.setShipTimestamp(request.getShipmentTimeStamp());
            requestedShipmentDetail.setTransactionLocationCode(request.getTransactionLocationId());
            requestedShipmentDetail.setDropOffType(BUSINESS_SERVICE_CENTER);

            List<VariationOptionDetail> variationOptions = new ArrayList<>();
            VariationOptionDetail variationOption = new VariationOptionDetail();
            variationOption.setId(ServiceConstant.SHIPMENT_CONTROL_VARIABLE_OPTION_ID);
            variationOption.setValues(ServiceConstant.SHIPMENT_CONTROL_VARIABLE_OPTION_VALUE);
            variationOptions.add(variationOption);
            requestedShipmentDetail.setVariationOptions(variationOptions);

            List<String> actions = new ArrayList<>();
            actions.add(ActionType.STRONG_VALIDATION.name());
            actions.add(ActionType.CONFIRM.name());
            requestedShipmentDetail.setActions(actions);

            if (StringUtils.isNotEmpty(openShipment.getShipmentData())) {
                OpenShipmentData openShipmentData = new OpenShipmentData();
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                try {
                    openShipmentData = objectMapper.readValue(openShipment.getShipmentData(), OpenShipmentData.class);
                } catch (IOException exp) {
                    logger.error("Exception occured while mapping json {}", exp.getMessage());
                }
                openShipmentDetail = openShipmentData.getOpenShipment();
                if (openShipmentDetail != null) {

                    return updateRequestedShipmentDetails(request, requestedShipmentDetail, openShipmentDetail);
                }
            }
        }
        return requestedShipmentDetail;
    }

    /**
     * Method to set Requested Package Line Item Detail
     * 
     * @param packageLineItems
     * @return
     */
    private List<RequestedPackageLineItemDetail> updatePackageLineItems(
            List<CompletedPackageData> packageLineItemList) {

        List<RequestedPackageLineItemDetail> pkgLineItemDetailList = null;
        RequestedPackageLineItemDetail pkgLineItemDetail = null;
        RequestedPackageSpecialServices reqPkgSpclServices = null;

        if (!CollectionUtils.isEmpty(packageLineItemList)) {
            pkgLineItemDetailList = new ArrayList<>();

            for (CompletedPackageData pkgData : packageLineItemList) {
                pkgLineItemDetail = new RequestedPackageLineItemDetail();
                reqPkgSpclServices = new RequestedPackageSpecialServices();

                pkgLineItemDetail.setSequenceNumber(pkgData.getSequenceNumber());
                pkgLineItemDetail.setGroupNumber(pkgData.getGroupNumber());
                pkgLineItemDetail.setGroupPackageCount(pkgData.getGroupNumber());
                pkgLineItemDetail.setInsuredValue(pkgData.getInsuredValue());
                pkgLineItemDetail.setDimensions(pkgData.getDimensions());
                pkgLineItemDetail.setWeight(pkgData.getWeight());
                pkgLineItemDetail.setSpecialHandlingDetail(pkgData.getSpecialHandlingDetail());
                pkgLineItemDetail.setCustomerReferences(pkgData.getCustomerReferences());

                reqPkgSpclServices.setBatteryClassifications(pkgData.getBatteryClassifications());
                reqPkgSpclServices.setDryIceWeightDetails(pkgData.getDryIceWeightDetails());
                reqPkgSpclServices
                        .setSpecialServicesRequested(setSpecialServicesRequested(pkgData.getSpecialServicesList()));

                pkgLineItemDetail.setPackageSpecialServicesRequested(reqPkgSpclServices);

                pkgLineItemDetailList.add(pkgLineItemDetail);
            }
        }
        return pkgLineItemDetailList;
    }

    /**
     * Method to set Special Service Description Detail
     * 
     * @param specialServicesList
     * @return
     */
    private List<SpecialServiceDescriptionDetail> setSpecialServicesRequested(
            List<SpecialServiceDetail> specialServicesList) {

        List<SpecialServiceDescriptionDetail> spclServiceDescDetailList = null;
        SpecialServiceDescriptionDetail spclServiceDescDetail = null;

        if (!CollectionUtils.isEmpty(specialServicesList)) {
            spclServiceDescDetailList = new ArrayList<>();

            for (SpecialServiceDetail spclSrvc : specialServicesList) {
                spclServiceDescDetail = new SpecialServiceDescriptionDetail();

                spclServiceDescDetail.setDescription(spclSrvc.getDescription());
                spclServiceDescDetail.setDisplayText(spclSrvc.getDisplayText());
                spclServiceDescDetail.setSpecialServiceSubType(spclSrvc.getSpecialServiceSubType());
                spclServiceDescDetail.setSpecialServiceType(spclSrvc.getSpecialServiceType());

                spclServiceDescDetailList.add(spclServiceDescDetail);
            }
        }
        return spclServiceDescDetailList;
    }

    /**
     * Method to update labelSpecification from DB, if not coming in confirm request
     * 
     * @param request
     * @param openShipmentData
     * @return
     */
    public void updateLabelSpecification(
            ShipmentConfirmationRequest request,
            OpenShipmentData openShipmentData) {

        if (request.getLabelSpecificationDetail() == null) {
            request.setLabelSpecificationDetail(openShipmentData.getOpenShipment()
                    .getLabelSpecification());
        }
    }
    
    /**
     * This method returns Open shipment details
     * 
     * @param openShipment
     * @return OpenShipmentDetail
     */
    private OpenShipmentDetail getOpenShipmentData(
            OpenShipment openShipment) {

        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        OpenShipmentData openShipmentData = new OpenShipmentData();
        try {
            openShipmentData = objectMapper.readValue(openShipment.getShipmentData(), OpenShipmentData.class);
        } catch (IOException e) {
            logger.error("Error Occured during parsing of openShipmentData {} ", e.getMessage(), e);
        }
        return openShipmentData.getOpenShipment();

    }


    /**
     * This method builds request data for PES call
     * 
     * @param request2
     * 
     * @param openShipment
     * @param confirmedShipmentData
     * @param reply
     * @param locationDetail
     * @return
     */
    public PackagePossessionEventDetailRequest buildPESRequest(
            ShipmentConfirmationRequest request,
            OpenShipment openShipment,
            ConfirmedShipmentData confirmedShipmentData,
            CompleteShipmentDetail reply,
            LocationDataDetail locationDetail,
            OpenShipmentData openShipmentData) {

        PackagePossessionEventDetailRequest packagePossessionEventDetailRequest =
                new PackagePossessionEventDetailRequest();
        List<PackagePossessionEventDetail> packagePossessionEventDetails = new ArrayList<>();
        packagePossessionEventDetailRequest.setLocationDetails(locationDetail);
        packagePossessionEventDetailRequest.setHeaderDetails(
                buildTransactionHeaderDetails(request.getShipmentTimeStamp(), request.getWorkstationDetail()));
        OpenShipmentDetail openShipmentDetail = getOpenShipmentData(openShipment);

        if (openShipmentDetail.getPaymentDetails() != null) {
            logger.debug("updated payment details PES");

            if (openShipmentDetail.getRateDetails() != null) {
                Price price = new Price();
                price.setAmount(openShipmentDetail.getRateDetails()
                        .getTotalNetCharges());
                price.setCurrency(openShipmentDetail.getRateDetails()
                        .getCurrency());
                request.getPaymentDetail()
                        .setAmount(price);
            }
            packagePossessionEventDetailRequest.setPaymentDetails(request.getPaymentDetail());
           
        }

        PackagePossessionEventDetail packagePossessionEventDetail = new PackagePossessionEventDetail();
        packagePossessionEventDetail.setService(new OfferingDetail());
        packagePossessionEventDetail.setPackageType(COUNTER_SHIPMENT);
        if (confirmedShipmentData.getMasterTrackingId() != null) {
            packagePossessionEventDetail.setBarcode(confirmedShipmentData.getMasterTrackingId()
                    .getBarcode());
        }
        if (reply.getMasterTrackingId() != null) {
            packagePossessionEventDetail.setTrackingNumber(reply.getMasterTrackingId()
                    .getTrackingNumber());
            packagePossessionEventDetail.setFormId(reply.getMasterTrackingId()
                    .getFormId());
        }
        if (openShipmentDetail.getRateDetails() != null) {
            Price price = new Price();
            price.setAmount(openShipmentDetail.getRateDetails()
                    .getTotalNetCharges());
            price.setCurrency(openShipmentDetail.getRateDetails()
                    .getCurrency());
            packagePossessionEventDetail.setShipmentChargeAmount(price);
        }
        if (openShipmentDetail.getRecipient() != null) {
            packagePossessionEventDetail.setDestinationAddress(openShipmentDetail.getRecipient()
                    .getAddress());
        }
        if (!CollectionUtils.isEmpty(openShipmentDetail.getPackageLineItems())
                && !StringUtils.isEmpty(openShipmentDetail.getPackageLineItems()
                        .get(0)
                        .getSpecialHandlingDetail())) {
            packagePossessionEventDetail
                    .setPaaIndicator(PAAIndicatorType.valueOf(openShipmentDetail.getPackageLineItems()
                            .get(0)
                            .getSpecialHandlingDetail()));
        }
        packagePossessionEventDetail.setOperatingCompany(setOperatingCompanyName(reply));
        // set the routingId as combination of ursa prefix and suffix.
        if (null != reply.getOperationalDetail() && StringUtils.isNotBlank(reply.getOperationalDetail()
                .getUrsaPrefixCode()) && StringUtils.isNotBlank(reply.getOperationalDetail()
                        .getUrsaSuffixCode())) {
            packagePossessionEventDetail.setRoutingId(reply.getOperationalDetail()
                    .getUrsaPrefixCode()
                    + reply.getOperationalDetail()
                            .getUrsaSuffixCode());
        }
        
        if (openShipmentData != null && openShipmentData.getOpenShipment() != null) {
         // sending service type to pes call
            if (openShipmentData.getOpenShipment()
                    .getSpecialServicesRequested() != null
                    && !CollectionUtils.isEmpty(openShipmentData.getOpenShipment()
                            .getSpecialServicesRequested()
                            .getSpecialServicesRequestedType())) {

                packagePossessionEventDetail.setServiceOptions(setServiceOptions(openShipmentData));
            }
            // sending interline data to pes call
            if (openShipmentData.getOpenShipment()
                    .getInterlineShippingData() != null) {

                packagePossessionEventDetail.setInterlineShippingDetails(
                        ConfirmShipmentTransactionMapper.INSTANCE.setInterlineDetails(openShipmentData.getOpenShipment()
                                .getInterlineShippingData()));
            }
        }
        packagePossessionEventDetail.setServiceAreaCode(reply.getPlannedServiceLevel());
        packagePossessionEventDetails.add(packagePossessionEventDetail);
        packagePossessionEventDetailRequest.setPackageLineItems(packagePossessionEventDetails);
        return packagePossessionEventDetailRequest;
    }
    
    /**
     * This method sets operating company name
     * 
     * @param reply
     * @return
     */
    private String setOperatingCompanyName(
            CompleteShipmentDetail reply) {

        String codeName = null;
        if (!StringUtils.isEmpty(reply.getCarrierCode())) {
            CarrierCodeType code = CarrierCodeType.valueOf(reply.getCarrierCode());
            codeName = code.name()
                    .equalsIgnoreCase(CarrierCodeType.FDXE.name()) ? CarrierType.EXPRESS.name()
                            : CarrierType.GROUND.name();
        }
        return codeName;
    }

    /**
     * This method will set the service options
     * 
     * @param openShipmentData
     * @return serviceOptions
     */
    private List<OfferingDetail> setServiceOptions(
            OpenShipmentData openShipmentData) {

        List<OfferingDetail> serviceOptions = new ArrayList<>();
        openShipmentData.getOpenShipment()
                .getSpecialServicesRequested()
                .getSpecialServicesRequestedType()
                .stream()
                .map(s -> {
                    OfferingDetail offeringDetail = new OfferingDetail();
                    offeringDetail.setType(s.getSpecialServiceType());
                    serviceOptions.add(offeringDetail);
                    return s;
                })
                .collect(Collectors.toList());
        return serviceOptions;
    }

    /**
     * This method builds request data for PES delete sold shipment call
     * 
     * @param requestDateTime
     * @param confirmedShipmentData
     * @param workstationDetail
     * @param locationDetail
     * @param personDetail
     * @return
     */
    public PackageCancellationEventDetailRequest buildPESCancelRequest(
            String requestDateTime,
            ConfirmedShipmentData confirmedShipmentData,
            WorkstationDetail workstationDetail,
            LocationDataDetail locationDetail,
            PersonDetail personDetail) {

        PackageCancellationEventDetailRequest packageCancellationEventDetailRequest =
                new PackageCancellationEventDetailRequest();
        packageCancellationEventDetailRequest.setLocationDetails(locationDetail);
        packageCancellationEventDetailRequest
                .setHeaderDetails(buildTransactionHeaderDetails(requestDateTime, workstationDetail));

        List<PackageLineItemDetail> packageLineItems = new ArrayList<>();
        PackageLineItemDetail packageLineItemDetail = new PackageLineItemDetail();
        packageLineItemDetail.setPackageType(COUNTER_SHIPMENT);

        PersonDetail personDetails = new PersonDetail();
        personDetails.setFirstName(personDetail.getFirstName());
        personDetails.setLastName(personDetail.getLastName());
        packageLineItemDetail.setCustomerName(personDetails);
        List<TrackingSequenceDetail> trackingSequenceDetails = new ArrayList<>();
        
        if(confirmedShipmentData.getMasterTrackingId() != null) {
        packageLineItemDetail.setPackageIdentifier(confirmedShipmentData.getMasterTrackingId());        
        trackingSequenceDetails.add(confirmedShipmentData.getMasterTrackingId());
       
        }        
        packageLineItemDetail.setChildPackages(trackingSequenceDetails);
        packageLineItems.add(packageLineItemDetail);
        
        packageCancellationEventDetailRequest.setPackageLineItems(packageLineItems);

        return packageCancellationEventDetailRequest;
    }

    /**
     * Method to build transaction header details
     * 
     * @param requestDateTime
     * @param workstationDetail
     * @return
     */
    private TransactionHeaderDetail buildTransactionHeaderDetails(
            String requestDateTime,
            WorkstationDetail workstationDetail) {

        TransactionHeaderDetail transactionHeaderDetail = new TransactionHeaderDetail();
        transactionHeaderDetail.setGuid(workstationDetail.getGuid());
        transactionHeaderDetail.setTeamMemberId(workstationDetail.getTeamMemberId());
        transactionHeaderDetail.setRequestDateTime(requestDateTime);
        
        DeviceDetail deviceDetail = new DeviceDetail();
        deviceDetail.setDeviceId(workstationDetail.getDeviceId());
        deviceDetail.setMeterNumber(workstationDetail.getMeterNumber());
        deviceDetail.setAppName(workstationDetail.getAppName());
        deviceDetail.setAppVersionId(workstationDetail.getAppVersionId());
        deviceDetail.setSoftwareId(workstationDetail.getSoftwareId());
        transactionHeaderDetail.setWorkstationDetails(deviceDetail);
        return transactionHeaderDetail;
    }

    /**
     * This method builds request data for returning UpdateOpenShipmentStatusData object
     * 
     * @param shipmentTrackingNumber
     * @param locationId
     * @param status
     * @param teamMemberId
     * @param posRefId
     * @return
     */
    public UpdateOpenShipmentStatusData buildUpdateOpenShipmentStatusData(
            String shipmentTrackingNumber,
            String locationId,
            String status,
            String teamMemberId,
            String posRefId) {

        UpdateOpenShipmentStatusData updateOpenShipmentStatusData = new UpdateOpenShipmentStatusData();
        updateOpenShipmentStatusData.setReferenceId(posRefId);
        updateOpenShipmentStatusData.setLocationId(locationId);
        updateOpenShipmentStatusData.setTrackingId(shipmentTrackingNumber);
        updateOpenShipmentStatusData.setOpenShipmentStatusCode(status);
        updateOpenShipmentStatusData.setTeamMemberId(teamMemberId);

        return updateOpenShipmentStatusData;
    }
    
    /**
     * This method is created to reduce complexity and this will add requsted shipment details from open
     * shipment data
     * 
     * @param request
     * @param requestedShipmentDetail
     * @param openShipmentDetail
     * @return requestedShipmentDetail
     */
    private RequestedShipmentDetail updateRequestedShipmentDetails(
            ShipmentConfirmationRequest request,
            RequestedShipmentDetail requestedShipmentDetail,
            OpenShipmentDetail openShipmentDetail) {

        logger.debug("updated payment details for modifyOpenShipment");
        if (request.getPaymentDetail() != null && openShipmentDetail.getPaymentDetails() != null
                && EnumUtils.isValidEnum(EPaymentMode.class, request.getPaymentDetail()
                        .getPaymentType())) {
            request.getPaymentDetail()
                    .setEpaymentMode(EPaymentMode.valueOf(request.getPaymentDetail()
                            .getPaymentType()));

            request.getPaymentDetail()
                    .setAmount(openShipmentDetail.getPaymentDetails()
                            .getAmount());
            PartyDetail payor = request.getPaymentDetail()
                    .getPayor();
            if (payor != null && payor.getCreditCard() != null && StringUtils.isNotBlank(payor.getCreditCard()
                    .getExpirationDate())) {
                payor.getCreditCard()
                        .setExpirationDate(payor.getCreditCard()
                                .getExpirationDate()
                                .replace("/", ""));
            } else {
                payor = new PartyDetail();
            }
            // set interlineAccountNumber for interline shipments
            if (openShipmentDetail.getInterlineShippingData() != null) {
                payor.setAccountNumber(openShipmentDetail.getInterlineShippingData()
                        .getAccountNumber());
                request.getPaymentDetail()
                        .setPayor(payor);
            }
            // set cityCenterAccountNumber for non-account shipments
            else {
                payor.setAccountNumber(request.getWorkstationDetail()
                        .getAccountNumber());
                request.getPaymentDetail()
                        .setPayor(payor);
            }
        }
        requestedShipmentDetail.setServiceType(openShipmentDetail.getServiceType());
        requestedShipmentDetail.setPackagingType(openShipmentDetail.getPackagingType());
        requestedShipmentDetail.setOrigin(openShipmentDetail.getShipper());
        requestedShipmentDetail.setSender(openShipmentDetail.getShipper());
        requestedShipmentDetail.setRecipient(openShipmentDetail.getRecipient());
        requestedShipmentDetail.setCustomer(openShipmentDetail.getCustomer());
        requestedShipmentDetail.setPackageCount(Integer.toString(openShipmentDetail.getPackageCount()));
        requestedShipmentDetail.setTransactionLocationCode(openShipmentDetail.getLocationId());
        requestedShipmentDetail.setIndex(openShipmentDetail.getShipmentIndex());
        requestedShipmentDetail.setShippingChargesPayment(request.getPaymentDetail());
        requestedShipmentDetail.setLabelSpecification(openShipmentDetail.getLabelSpecification());
        requestedShipmentDetail.setSpecialServicesRequested(openShipmentDetail.getSpecialServicesRequested());
        requestedShipmentDetail.setPackageLineItems(updatePackageLineItems(openShipmentDetail.getPackageLineItems()));
        return requestedShipmentDetail;
    }
}
