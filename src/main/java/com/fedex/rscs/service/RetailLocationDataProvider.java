package com.fedex.rscs.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.fedex.rscs.client.rtls.RetailLocationServiceClient;
import com.fedex.rscs.config.CacheConfiguration;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.ServiceProcessorTerminalException;
import com.fedex.rscs.exception.client.ClientTerminalException;
import com.fedex.rscs.model.LocationContextDetail;

/**
 * This component provides the location data and works as a location data repository. <br>
 * The source of data for this repository is application cache or retail location service. It gets
 * the retail location data either from local application cache or by communicating with retail
 * location service.
 * 
 * 
 * @author shashank.jain
 *
 */
@Component
public class RetailLocationDataProvider {

    private static final Logger logger = LoggerFactory.getLogger(RetailLocationDataProvider.class);

    private RetailLocationServiceClient locationServiceClient;

    @Autowired
    public RetailLocationDataProvider(final RetailLocationServiceClient locationServiceClient) {

        this.locationServiceClient = locationServiceClient;
    }

    /**
     * Method to fetch the location context details.
     * 
     * @param locationId
     * @return
     */
    @Cacheable(cacheNames = {CacheConfiguration.LOCATION_CACHE}, key = "{#locationId}")
    public LocationContextDetail getContextDetails(
            String locationId) {

        logger.info("retrieve the location information for location {}", locationId);
        try {
            return this.locationServiceClient.getLocationContextDetails(locationId);
        } catch (ClientTerminalException ex) {
            logger.error("failed to process the location request for location id {} due to ", locationId, ex);
            throw new ServiceProcessorTerminalException(ex.getShipmentCreationErrorCode());
        } catch (RuntimeException ex) {
            // It will catch all runtime exception like hystrix runtime exception.
            logger.error("Unable to connect with Context to retrieve Location details for location id {} due to ", locationId, ex);
            throw new ServiceProcessorTerminalException(ServiceErrorCode.RETAIL_LOCATION_SERVICE_UNAVAILABLE);
        }
    }

}
