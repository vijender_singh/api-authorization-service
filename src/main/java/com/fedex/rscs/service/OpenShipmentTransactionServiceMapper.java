package com.fedex.rscs.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import io.micrometer.core.instrument.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fedex.rscs.dto.CarrierCodeType;
import com.fedex.rscs.dto.CarrierType;
import com.fedex.rscs.dto.CreateOpenShipmentRequestData;
import com.fedex.rscs.dto.RequestedPaymentType;
import com.fedex.rscs.dto.RequestedShipment;
import com.fedex.rscs.dto.ResponsibleParty;
import com.fedex.rscs.dto.WeightSource;
import com.fedex.rscs.model.AssociatedAccountDetail;
import com.fedex.rscs.model.CarrierDetail;
import com.fedex.rscs.model.InsuredValueDetail;
import com.fedex.rscs.model.LabelSpecificationDetail;
import com.fedex.rscs.model.OpenShipment;
import com.fedex.rscs.model.OpenShipmentData;
import com.fedex.rscs.model.OpenShipmentDetail;
import com.fedex.rscs.model.PaymentDetail;
import com.fedex.rscs.model.RequestedPackageLineItemDetail;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ServiceConstant;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.ShipmentSpecialServiceDetail;
import com.fedex.rscs.model.ShippingCustomerReferenceDetails;
import com.fedex.rscs.model.SpecialServiceDescriptionDetail;
import com.fedex.rscs.model.SpecialServiceDetail;
import com.fedex.rscs.model.VariationOptionDetail;
import com.fedex.rscs.model.WeightDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.cshp.ActionType;
import com.fedex.rscs.model.cshp.CompleteShipmentDetail;
import com.fedex.rscs.model.cshp.CompletedPackageData;
import com.fedex.rscs.model.cshp.DeleteOpenShipmentDetail;
import com.fedex.rscs.model.cshp.EPaymentMode;
import com.fedex.rscs.model.cshp.ImageType;
import com.fedex.rscs.model.cshp.LabelFormatType;
import com.fedex.rscs.model.cshp.OpenShipmentResponse;
import com.fedex.rscs.model.cshp.PreauthorizedRateData;
import com.fedex.rscs.model.cshp.Price;
import com.fedex.rscs.model.cshp.PrintingOrientation;
import com.fedex.rscs.model.cshp.ShippingRateData;
import com.fedex.rscs.model.cshp.StockType;
import com.fedex.rscs.model.cshp.TrackingSequenceDetail;
import com.fedex.rscs.model.cshp.TransactionStatus;
import com.fedex.rscs.model.cshp.UpdateOpenShipmentStatusData;
import com.fedex.rscs.repository.OpenShipmentRepository;

/**
 * This class will set the mandatory values in the provided request object of
 * OpenShipmentTransactionService.
 * 
 * @author 3798910
 *
 */
@Component
public class OpenShipmentTransactionServiceMapper {

    private static final Logger logger = LoggerFactory.getLogger(OpenShipmentTransactionServiceMapper.class);

    public static final Integer SEQUENCE_NUMBER = 1;
    public static final Integer GROUP_NUMBER = 0;
    public static final Integer GROUP_PACKAGE_COUNT = 1;

    @Autowired
    OpenShipmentRepository openShipmentRepository;

    /**
     * This method will set the values of RequestedShipmentDetail object
     * 
     * @param requestedShipment
     * @return
     */
    public RequestedShipmentDetail createRequestedShipmentDetail(
            RequestedShipmentDetail requestedShipment,
            String accountNumber,
            ShippingRateData shippingRateData) {

        List<VariationOptionDetail> variationOptions = new ArrayList<>();
        VariationOptionDetail variationOption = new VariationOptionDetail();
        variationOption.setId(ServiceConstant.SHIPMENT_CONTROL_VARIABLE_OPTION_ID);
        variationOption.setValues(ServiceConstant.SHIPMENT_CONTROL_VARIABLE_OPTION_VALUE);
        variationOptions.add(variationOption);
        requestedShipment.setVariationOptions(variationOptions);

        List<String> actions = new ArrayList<>();
        actions.add(ActionType.STRONG_VALIDATION.name());
        requestedShipment.setActions(actions);

        if (requestedShipment.getShippingChargesPayment() != null) {

            if (requestedShipment.getShippingChargesPayment()
                    .getPaymentType()
                    .equals(RequestedPaymentType.ACCOUNT.name())) {
                requestedShipment.setShippingChargesPayment(
                        updateShippingChargesPaymentForAccount(requestedShipment));
            }

            if (requestedShipment.getShippingChargesPayment()
                    .getPaymentType()
                    .equals(RequestedPaymentType.NON_ACCOUNT.name())) {
                updateShippingChargesPaymentForNonAccount(
                        requestedShipment.getShippingChargesPayment(), accountNumber);
            }
        }
        if (!ObjectUtils.isEmpty(requestedShipment.getPackageLineItems())) {
            for (RequestedPackageLineItemDetail packageLineItems : requestedShipment.getPackageLineItems()) {

                packageLineItems.setGroupPackageCount(GROUP_PACKAGE_COUNT);
                packageLineItems.setSequenceNumber(SEQUENCE_NUMBER);
                packageLineItems.setGroupNumber(GROUP_NUMBER);

                if (StringUtils.isBlank(packageLineItems.getWeight()
                        .getMeasurementType())) {
                    packageLineItems.getWeight()
                            .setMeasurementType(WeightSource.MANUAL.toString());
                }

                if (packageLineItems.getPackageSpecialServicesRequested()
                        .getDryIceWeightDetails() != null
                        && StringUtils.isBlank(packageLineItems.getPackageSpecialServicesRequested()
                                .getDryIceWeightDetails()
                                .getMeasurementType())) {
                    packageLineItems.getPackageSpecialServicesRequested()
                            .getDryIceWeightDetails()
                            .setMeasurementType(WeightSource.MANUAL.toString());
                }
            }
        }
        if (requestedShipment.getLabelSpecification() == null) {
            LabelSpecificationDetail labelSpecification = createLabelSpecification();
            requestedShipment.setLabelSpecification(labelSpecification);
        }
        if (!ObjectUtils.isEmpty(requestedShipment.getPackageLineItems())) {
            requestedShipment.setPackageCount(String.valueOf(requestedShipment.getPackageLineItems()
                    .size()));
        }
        if (requestedShipment.getShippingChargesPayment() != null) {
            Price price = new Price();
            price.setAmount(shippingRateData.getTotalNetCharges());
            price.setCurrency(shippingRateData.getCurrency());
            requestedShipment.getShippingChargesPayment()
                    .setAmount(price);
        }
        
        // logic to set the phone number from recipient if not available in contact details for HAL
        ShipmentSpecialServiceDetail shpmtSpclSrvcDetail = requestedShipment.getSpecialServicesRequested();
        if (shpmtSpclSrvcDetail != null && shpmtSpclSrvcDetail.getHoldAtLocationDetail() != null
                && shpmtSpclSrvcDetail.getHoldAtLocationDetail()
                        .getContact() != null
                        && StringUtils.isEmpty(shpmtSpclSrvcDetail.getHoldAtLocationDetail()
                                .getContact()
                                .getPhoneNumber())) {
            shpmtSpclSrvcDetail.getHoldAtLocationDetail()
                    .getContact()
                    .setPhoneNumber(requestedShipment.getRecipient()
                            .getContact()
                            .getPhoneNumber());
        }
        return requestedShipment;
    }

    /**
     * Method to create the response object of OpenShipmentResponse
     * 
     * @param completeShipmentDetail
     * @param requestedShipmentDetail
     * @param shippingRateData
     * @param preauthorizedRateData
     * @return OpenShipmentResponse
     */
    public OpenShipmentResponse createOpenShipmentResponse(
            CompleteShipmentDetail completeShipmentDetail,
            RequestedShipmentDetail requestedShipmentDetail,
            ShippingRateData shippingRateData,
            PreauthorizedRateData preauthorizedRateData) {

        OpenShipmentResponse openShipmentResponse = new OpenShipmentResponse();

        List<CompletedPackageData> listOfCompletedPackageData = new ArrayList<>();
        for (CompletedPackageData completedPackageData : completeShipmentDetail.getCompletedPackageDetails()) {
            completedPackageData.setDimensions(requestedShipmentDetail.getPackageLineItems()
                    .get(0)
                    .getDimensions());
            completedPackageData.setInsuredValue(requestedShipmentDetail.getPackageLineItems()
                    .get(0)
                    .getInsuredValue());
            completedPackageData.setSpecialHandlingDetail(requestedShipmentDetail.getPackageLineItems()
                    .get(0)
                    .getSpecialHandlingDetail());
            completedPackageData.setWeight(requestedShipmentDetail.getPackageLineItems()
                    .get(0)
                    .getWeight());
            TrackingSequenceDetail trackingSequence = new TrackingSequenceDetail();
            trackingSequence.setTrackingNumber(completedPackageData.getTrackingIds()
                    .get(0)
                    .getTrackingNumber());
            completedPackageData.setTrackingId(trackingSequence);
            completedPackageData.getTrackingId()
                    .setSequence((completedPackageData.getSequenceNumber()));
            if (requestedShipmentDetail.getSpecialServicesRequested() != null
                    && !CollectionUtils.isEmpty(requestedShipmentDetail.getSpecialServicesRequested()
                            .getSpecialServicesRequestedType()))
                completedPackageData.setSpecialServicesDetails(requestedShipmentDetail.getSpecialServicesRequested()
                        .getSpecialServicesRequestedType()
                        .stream()
                        .map(st -> {
                            SpecialServiceDetail spSerDet = new SpecialServiceDetail();
                            spSerDet.setSpecialServiceType(st.getSpecialServiceType());
                            return spSerDet;
                        })
                        .collect(Collectors.toList()));
            listOfCompletedPackageData.add(completedPackageData);
        }
        completeShipmentDetail.setCompletedPackageDetails(listOfCompletedPackageData);
        completeShipmentDetail.setRecipient(requestedShipmentDetail.getRecipient());
        completeShipmentDetail.setSpecialServicesRequested(requestedShipmentDetail.getSpecialServicesRequested());
        completeShipmentDetail.setPackageCount(Integer.parseInt(requestedShipmentDetail.getPackageCount()));
        completeShipmentDetail.setTotalInsuredValue(createTotalInsuredValue(requestedShipmentDetail));
        completeShipmentDetail.setTotalWeight(createTotalWeight(requestedShipmentDetail));
        completeShipmentDetail.setLocationId(requestedShipmentDetail.getTransactionLocationCode());
        openShipmentResponse.setCompleteShipmentDetail(completeShipmentDetail);
        openShipmentResponse.setShippingRateData(shippingRateData);
        openShipmentResponse.setPreauthorizedRateData(preauthorizedRateData);
        return openShipmentResponse;
    }

    /**
     * Method to evaluate the total weight
     * 
     * @param requestedShipmentDetail
     * @return
     */
    private WeightDetail createTotalWeight(
            RequestedShipmentDetail requestedShipmentDetail) {

        WeightDetail weightDetail = new WeightDetail();
        Double totalWeight = 0.0;
        String unit = null;
        String measurementType=null;

        for (RequestedPackageLineItemDetail requestedPackageLineItem : requestedShipmentDetail.getPackageLineItems()) {

            if (requestedPackageLineItem.getWeight() != null) {

                totalWeight += Double.parseDouble(requestedPackageLineItem.getWeight()
                        .getValue());
                unit = requestedPackageLineItem.getWeight()
                        .getUnit();
                measurementType=requestedPackageLineItem.getWeight().getMeasurementType();
                
            }

        }
        weightDetail.setUnit(unit);
        weightDetail.setValue(totalWeight.toString());
        weightDetail.setMeasurementType(measurementType);
        return weightDetail;
    }

    /**
     * Method to evaluate total insured value
     * 
     * @param requestedShipmentDetail
     * @return
     */
    private InsuredValueDetail createTotalInsuredValue(
            RequestedShipmentDetail requestedShipmentDetail) {

        Double totalAmount = 0.0;
        String currency = null;
        InsuredValueDetail insuredValueDetail = new InsuredValueDetail();

        for (RequestedPackageLineItemDetail requestedPackageLineItem : requestedShipmentDetail.getPackageLineItems()) {
            if (!ObjectUtils.isEmpty(requestedPackageLineItem.getInsuredValue())) {
                totalAmount += Double.parseDouble(requestedPackageLineItem.getInsuredValue()
                        .getAmount());
                currency = requestedPackageLineItem.getInsuredValue()
                        .getCurrency();
            }
        }
        insuredValueDetail.setAmount(totalAmount.toString());
        insuredValueDetail.setCurrency(currency);
        return insuredValueDetail;
    }

    /**
     * Method to set the value in RequestedShipment object
     * 
     * @param createOpenShipmentRequest
     * @return
     */
    public RequestedShipment createRequestedShipment(
            CreateOpenShipmentRequestData createOpenShipmentRequest) {

        if (!ObjectUtils.isEmpty(createOpenShipmentRequest)
                && (!ObjectUtils.isEmpty(createOpenShipmentRequest.getRequestedShipment()))) {
            return createOpenShipmentRequest.getRequestedShipment();
        }
        return null;
    }

    /**
     * Method to set the object of LabelSpecificationDetail
     * 
     * @return
     */
    private LabelSpecificationDetail createLabelSpecification() {

        LabelSpecificationDetail labelSpecification = new LabelSpecificationDetail();
        labelSpecification.setFormatType(LabelFormatType.COMMON2D.name());
        labelSpecification.setImageType(ImageType.ZPLII.name());
        labelSpecification.setStockType(StockType.STOCK_4X6.name());
        labelSpecification.setPrintingOrientation(PrintingOrientation.TOP_EDGE_OF_TEXT_FIRST.name());
        return labelSpecification;
    }

    /**
     * Method to create object of OpenShipmentResponse
     * 
     * @param openShipment
     * @return
     */
    public OpenShipmentResponse retrieveOpenShipmentResponse(
            OpenShipment openShipment) {

        OpenShipmentResponse openShipmentResponse = new OpenShipmentResponse();
        OpenShipmentData openShipmentData = null;
        CompleteShipmentDetail completeShipmentDetail = new CompleteShipmentDetail();
        OpenShipmentDetail openShipmentDetail = null;
        if (!ObjectUtils.isEmpty(openShipment.getShipmentData())) {
            openShipmentData =
                    openShipmentRepository.getOpenShipmentDataFromBlob(openShipment.getShipmentData());

            if (!ObjectUtils.isEmpty(openShipmentData) && !ObjectUtils.isEmpty(openShipmentData.getOpenShipment())) {
                openShipmentDetail = openShipmentData.getOpenShipment();
                OpenShipmentTransactionMapper.INSTANCE.updateShipmentPaymentData(openShipmentDetail.getPaymentDetails(),
                        openShipmentResponse);
                OpenShipmentTransactionMapper.INSTANCE.updateShipmentRateData(openShipmentDetail.getRateDetails(),
                        openShipmentResponse);
            }
        }
        OpenShipmentTransactionMapper.INSTANCE.updateCompleteShipmentDetail(openShipmentDetail, completeShipmentDetail);

        openShipmentResponse.setCompleteShipmentDetail(completeShipmentDetail);
        return openShipmentResponse;
    }

    /**
     * Method to create object of DeleteOpenShipmentDetail
     * 
     * @param requestedDateTime, locationCode, index and workstationDetail
     * @return
     */
    public DeleteOpenShipmentDetail deleteOpenShipmentRequest(
            String requestedDateTime, String locationCode, String index,WorkstationDetail workstationDetail) {
        
        DeleteOpenShipmentDetail deleteOpenShipmentDetail=new DeleteOpenShipmentDetail();
        deleteOpenShipmentDetail.setRequestedDateTime(requestedDateTime);
        deleteOpenShipmentDetail.setLocationCode(locationCode);
        deleteOpenShipmentDetail.setIndex(index);
        deleteOpenShipmentDetail.setWorkstationDetail(workstationDetail);
        
        return deleteOpenShipmentDetail;
    }
    
    /**
     * Method to create OpenShipment object which will be stored in the Database
     * 
     * @param openShipmentResponse
     * @param workstationDetail
     * @param requestShipmentDetail
     * @return
     */
    public OpenShipment createOpenShipment(
            OpenShipmentResponse openShipmentResponse,
            WorkstationDetail workstationDetail,
            RequestedShipmentDetail requestShipmentDetail) {

        OpenShipment openShipment = new OpenShipment();
        openShipment.setCityCenterAccountNumber(workstationDetail.getAccountNumber());
        openShipment.setDeviceId(workstationDetail.getDeviceId());
        openShipment.setMeterNumber(workstationDetail.getMeterNumber());
        openShipment.setSoftwareId(workstationDetail.getSoftwareId());
        openShipment.setFedexId(workstationDetail.getTeamMemberId());

        openShipment.setPackageAcceptTmStmp(requestShipmentDetail.getShipTimestamp());
        openShipment.setInitialAcceptanceTmStmp(requestShipmentDetail.getShipTimestamp());        

        openShipment.setOpenShipmentId(openShipmentResponse.getCompleteShipmentDetail()
                .getIndex());

        openShipment.setFedexLocationId(openShipmentResponse.getCompleteShipmentDetail()
                .getLocationId());
        openShipment.setOpCoLocationId(openShipmentResponse.getCompleteShipmentDetail()
                .getLocationId());
        openShipment.setOpCoCode(openShipmentResponse.getCompleteShipmentDetail()
                .getCarrierCode());
        openShipment.setTrackingNumber(openShipmentResponse.getCompleteShipmentDetail()
                .getMasterTrackingId()
                .getTrackingNumber());
        openShipment.setOpenShipmentStatusCode(TransactionStatus.CREATED.name());
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            openShipment.setShipmentData(objectMapper.writeValueAsString(
                    createOpenShipmentBlob(openShipmentResponse, workstationDetail, requestShipmentDetail)));
        } catch (JsonProcessingException e) {
            logger.error("Error in converting openShipment object as a String", e);
        }

        return openShipment;
    }

    /**
     * Method to set the value of OpenShipmentData that will be stored as a Blob object
     * 
     * @param openShipmentResponse
     * @param requestedShipmentDetail
     * @return
     */
    public OpenShipmentData createOpenShipmentBlob(
            OpenShipmentResponse openShipmentResponse,
            WorkstationDetail workstationDetail,
            RequestedShipmentDetail requestedShipmentDetail) {

        OpenShipmentData openShipmentData = new OpenShipmentData();
        OpenShipmentDetail openShipment = new OpenShipmentDetail();
        CarrierDetail carrierDetail = new CarrierDetail();

        openShipment.setLabelSpecification(requestedShipmentDetail.getLabelSpecification());
        openShipment.setSenderCustomer(requestedShipmentDetail.isSenderCustomer());
        openShipment.setCustomer(requestedShipmentDetail.getCustomer());
        openShipment.setShipper(requestedShipmentDetail.getSender());
        openShipment.setPaymentDetails(requestedShipmentDetail.getShippingChargesPayment());

        openShipment.setPackageLineItems(openShipmentResponse.getCompleteShipmentDetail()
                .getCompletedPackageDetails());
        // method to set custReference,Batteryclassification and dryiceweightdetail from
        // requestedShipmentDetail
        if (!ObjectUtils.isEmpty(requestedShipmentDetail.getPackageLineItems())) {
            for (RequestedPackageLineItemDetail reqPkgLineItemDtl : requestedShipmentDetail.getPackageLineItems()) {
                updatePackageLineItemDetails(reqPkgLineItemDtl, openShipment);
            }
        }
        openShipment.setPackageCount(openShipmentResponse.getCompleteShipmentDetail()
                .getPackageCount());
        openShipment.setLocationId(openShipmentResponse.getCompleteShipmentDetail()
                .getLocationId());
        openShipment.setShipmentIndex(openShipmentResponse.getCompleteShipmentDetail()
                .getIndex());
        openShipment.setServiceType(requestedShipmentDetail.getServiceType());
        openShipment.setPackagingType(requestedShipmentDetail.getPackagingType());
        openShipment.setRecipient(openShipmentResponse.getCompleteShipmentDetail()
                .getRecipient());
        openShipment.setSpecialServicesRequested(openShipmentResponse.getCompleteShipmentDetail()
                .getSpecialServicesRequested());
        openShipment.setTotalInsuredValue(openShipmentResponse.getCompleteShipmentDetail()
                .getTotalInsuredValue());
        openShipment.setTotalWeight(openShipmentResponse.getCompleteShipmentDetail()
                .getTotalWeight());
        openShipment.setRateDetails(openShipmentResponse.getShippingRateData());
        openShipment.setPreauthorizedRateDetails(openShipmentResponse.getPreauthorizedRateData());
        carrierDetail.setOpco(((openShipmentResponse.getCompleteShipmentDetail()
                .getCarrierCode()).equals(CarrierCodeType.FDXE.name()) ? CarrierType.EXPRESS.name()
                        : CarrierType.GROUND.name()));

        carrierDetail.setCarrierCode(openShipmentResponse.getCompleteShipmentDetail()
                .getCarrierCode());

        openShipment.setCarrierDetails(carrierDetail);
        openShipment.setAppVersionId(workstationDetail.getAppVersionId());
        openShipment.setPhysicalPackagingType(requestedShipmentDetail.getPackageLineItems()
                .get(0)
                .getPhysicalPackagingType());
        if (requestedShipmentDetail.getInterlineShippingData() != null) {
            openShipment.setInterlineShippingData(requestedShipmentDetail.getInterlineShippingData());
        }
        openShipmentData.setOpenShipment(openShipment);

        return openShipmentData;

    }

    /**
     * Method to set data in openShipment Reference
     * 
     * @param reqPkgLineItemDtl
     * @param openShipment
     * @return
     */
    private void updatePackageLineItemDetails(
            RequestedPackageLineItemDetail reqPkgLineItemDtl,
            OpenShipmentDetail openShipment) {

        List<ShippingCustomerReferenceDetails> customerReferenceDetails = new ArrayList<>();
        ShippingCustomerReferenceDetails customerReference = null;

        if (!ObjectUtils.isEmpty(reqPkgLineItemDtl.getCustomerReferences())) {
            for (ShippingCustomerReferenceDetails custReferenceDtl : reqPkgLineItemDtl.getCustomerReferences()) {
                customerReference = new ShippingCustomerReferenceDetails();
                customerReference.setReferenceType(custReferenceDtl.getReferenceType());
                customerReference.setValue(custReferenceDtl.getValue());
                customerReferenceDetails.add(customerReference);
            }
        }

        if (!ObjectUtils.isEmpty(openShipment.getPackageLineItems())) {
            for (CompletedPackageData compltPkgData : openShipment.getPackageLineItems()) {

                compltPkgData.setCustomerReferences(customerReferenceDetails);
                if (!ObjectUtils.isEmpty(reqPkgLineItemDtl.getPackageSpecialServicesRequested())) {
                    compltPkgData.setBatteryClassifications(reqPkgLineItemDtl.getPackageSpecialServicesRequested()
                            .getBatteryClassifications());
                    compltPkgData.setDryIceWeightDetails(reqPkgLineItemDtl.getPackageSpecialServicesRequested()
                            .getDryIceWeightDetails());
                }

                compltPkgData.setSpecialServicesDetails(setSpecialServiceDetails(reqPkgLineItemDtl));
            }
        }
    }

    /**
     * This method will set the special service details
     * 
     * @param reqPkgLineItemDtl
     * @return
     */
    private List<SpecialServiceDetail> setSpecialServiceDetails(
            RequestedPackageLineItemDetail reqPkgLineItemDtl) {

        List<SpecialServiceDetail> specialServiceDetails = new ArrayList<>();
        if (!ObjectUtils.isEmpty(reqPkgLineItemDtl.getPackageSpecialServicesRequested())
                && !ObjectUtils.isEmpty(reqPkgLineItemDtl.getPackageSpecialServicesRequested()
                        .getSpecialServicesRequested())) {
            for (SpecialServiceDescriptionDetail specialServiceDescriptionDetail : reqPkgLineItemDtl
                    .getPackageSpecialServicesRequested()
                    .getSpecialServicesRequested()) {
                SpecialServiceDetail specialServiceDetail = new SpecialServiceDetail();

                specialServiceDetail.setDescription(specialServiceDescriptionDetail.getDescription());
                specialServiceDetail.setSpecialServiceType(specialServiceDescriptionDetail.getSpecialServiceType());
                specialServiceDetail.setDisplayText(specialServiceDescriptionDetail.getDisplayText());
                specialServiceDetail
                        .setSpecialServiceSubType(specialServiceDescriptionDetail.getSpecialServiceSubType());
                specialServiceDetails.add(specialServiceDetail);

            }
        }
        return specialServiceDetails;
    }

    /**
     * Method to set PaymentDetail for Payment-Type ACCOUNT
     * 
     * @param requestedShipment
     * @param accountNumber
     * @return
     */
    private PaymentDetail updateShippingChargesPaymentForAccount(
            RequestedShipmentDetail requestedShipment) {

        PaymentDetail paymentDetail = requestedShipment.getShippingChargesPayment();
        if (ObjectUtils.isEmpty(paymentDetail.getResponsibleParty())) {
            paymentDetail.setResponsibleParty(ResponsibleParty.SENDER.name());
        }

        if (paymentDetail.getResponsibleParty()
                .equals(ResponsibleParty.SENDER.name())) {
            List<AssociatedAccountDetail> alist = new ArrayList<>();
            AssociatedAccountDetail acc = new AssociatedAccountDetail();
            acc.setAccountNumber(paymentDetail.getPayor()
                    .getAccountNumber());
            alist.add(acc);
            paymentDetail.setAssociatedAccounts(alist);
        }

        return paymentDetail;
    }

    /**
     * Method to set PaymentDetail for Payment-Type NON_ACCOUNT
     * 
     * @param requestedShipment
     * @param accountNumber
     * @return
     */
    private void updateShippingChargesPaymentForNonAccount(
            PaymentDetail paymentDetails,
            String accountNumber) {

        paymentDetails.getPayor()
                .setAccountNumber(accountNumber);
        paymentDetails.setEpaymentMode(EPaymentMode.CASH);
    }
    
    /**
     * Method to create UpdateOpenShipmentStatusData object to update status in database
     * 
     * @param request
     * @return
     */
    public UpdateOpenShipmentStatusData buildUpdateOpenShipmentStatusData(
            ShipmentConfirmationRequest request) {

        UpdateOpenShipmentStatusData updateOpenShipmentStatusData =
                OpenShipmentTransactionMapper.INSTANCE.mapToUpdateOpenShipmentStatusData(request);
        updateOpenShipmentStatusData.setOpenShipmentStatusCode(TransactionStatus.CONFIRMED.name());
        return updateOpenShipmentStatusData;
    }

    /**
     * Method to retrieve appVersionId from OPEN_SHIPMENT database
     * 
     * @param openShipment
     * @return
     */
    public String getAppVersionIdFromCreateOpenShipment(
            OpenShipmentData openShipmentData) {

        if (openShipmentData != null) {
            return openShipmentData.getOpenShipment()
                    .getAppVersionId();
        }
        return null;

    }
    
    /**
     * Method to set ShippingRateData from PreauthorizedRateData
     * 
     * @param preauthorizedRateData
     * @return
     */
    public ShippingRateData mapPreAuthorizedRateDataToShippingRateData(
            PreauthorizedRateData preauthorizedRateData) {

        return OpenShipmentTransactionMapper.INSTANCE.mapPreAuthorizedRateDataToShippingRateData(preauthorizedRateData);

    }
    
}
