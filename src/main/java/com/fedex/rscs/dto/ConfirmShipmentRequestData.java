package com.fedex.rscs.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Represents the Confirm Shipment Request Data DTO
 * 
 * @author 3900109
 *
 */
public class ConfirmShipmentRequestData {

    @NotNull
    @Valid
    private RequestHeaderDetail headerDetails;
    @NotNull
    @Valid
    private LocationDetail locationDetails;
    @NotNull
    @Valid
    private OpenShipmentIdentifier requestedShipment;
    private LabelSpecification labelSpecification;
    @Valid
    private ShippingPaymentDetail paymentDetails;

    public ConfirmShipmentRequestData() {}

    /**
     * @param headerDetails
     * @param locationDetails
     * @param requestedShipment
     * @param labelSpecification
     * @param paymentDetails
     */
    public ConfirmShipmentRequestData(RequestHeaderDetail headerDetails, LocationDetail locationDetails,
            OpenShipmentIdentifier requestedShipment, LabelSpecification labelSpecification,
            ShippingPaymentDetail paymentDetails) {

        this.headerDetails = headerDetails;
        this.locationDetails = locationDetails;
        this.requestedShipment = requestedShipment;
        this.labelSpecification = labelSpecification;
        this.paymentDetails = paymentDetails;
    }

    /**
     * 
     * @return headerDetails
     */
    public RequestHeaderDetail getHeaderDetails() {

        return headerDetails;
    }

    /**
     * 
     * @param headerDetails to set headerDetails
     */
    public void setHeaderDetails(
            RequestHeaderDetail headerDetails) {

        this.headerDetails = headerDetails;
    }

    /**
     * 
     * @return locationDetails
     */
    public LocationDetail getLocationDetails() {

        return locationDetails;
    }

    /**
     * 
     * @param locationDetails to set locationDetails
     */
    public void setLocationDetails(
            LocationDetail locationDetails) {

        this.locationDetails = locationDetails;
    }

    /**
     * @return the requestedShipment
     */
    public OpenShipmentIdentifier getRequestedShipment() {

        return requestedShipment;
    }

    /**
     * @param requestedShipment the requestedShipment to set
     */
    public void setRequestedShipment(
            OpenShipmentIdentifier requestedShipment) {

        this.requestedShipment = requestedShipment;
    }

    /**
     * @return the labelSpecification
     */
    public LabelSpecification getLabelSpecification() {

        return labelSpecification;
    }

    /**
     * @param labelSpecification the labelSpecification to set
     */
    public void setLabelSpecification(
            LabelSpecification labelSpecification) {

        this.labelSpecification = labelSpecification;
    }

    /**
     * @return the paymentDetails
     */
    public ShippingPaymentDetail getPaymentDetails() {

        return paymentDetails;
    }

    /**
     * @param paymentDetails the paymentDetails to set
     */
    public void setPaymentDetails(
            ShippingPaymentDetail paymentDetails) {

        this.paymentDetails = paymentDetails;
    }

    @Override
    public String toString() {

        return "ConfirmShipmentRequestData [headerDetails=" + headerDetails + ", locationDetails=" + locationDetails
                + ", requestedShipment=" + requestedShipment + ", labelSpecification=" + labelSpecification
                + ", paymentDetails=" + paymentDetails + "]";
    }

}
