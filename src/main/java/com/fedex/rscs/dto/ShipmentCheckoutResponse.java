package com.fedex.rscs.dto;

import com.fedex.common.cxs.dto.CXSOutput;

/**
 * Represents the Shipment Checkout Response DTO
 * 
 * @author 3932968
 *
 */
public class ShipmentCheckoutResponse extends CXSOutput {

    private ShipmentCheckoutResourceData shipmentCheckout;

    public ShipmentCheckoutResponse() {

        super();
    }

    /**
     * @return the shipmentCheckout
     */
    public ShipmentCheckoutResourceData getShipmentCheckout() {

        return shipmentCheckout;
    }

    /**
     * @param shipmentCheckout the shipmentCheckout to set
     */
    public void setShipmentCheckout(
            ShipmentCheckoutResourceData shipmentCheckout) {

        this.shipmentCheckout = shipmentCheckout;
    }

    @Override
    public String toString() {

        return "ShipmentCheckoutResponse [shipmentCheckout=" + shipmentCheckout + "]";
    }

}
