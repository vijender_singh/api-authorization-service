package com.fedex.rscs.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Represents the Party DTO
 * 
 * @author 3932968
 *
 */
public class Party {

    @NotNull @Valid
    private Address address;
    @Valid
    private ShippingContact contact;

    public Party() {}

    /**
     * 
     * @param address
     * @param contact
     */
    public Party(Address address, ShippingContact contact) {

        this.address = address;
        this.contact = contact;
    }

    /**
     * 
     * @return address
     */
    public Address getAddress() {

        return address;
    }

    /**
     * 
     * @param address to set address
     */
    public void setAddress(
            Address address) {

        this.address = address;
    }

    /**
     * 
     * @return contact
     */
    public ShippingContact getContact() {

        return contact;
    }

    /**
     * 
     * @param contact to set contact
     */
    public void setContact(
            ShippingContact contact) {

        this.contact = contact;
    }

    @Override
    public String toString() {

        return "Party [address=" + address + ", contact=" + contact + "]";
    }

}
