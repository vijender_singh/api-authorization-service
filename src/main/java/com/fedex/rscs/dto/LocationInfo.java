package com.fedex.rscs.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Represents the Location Info DTO
 * 
 * @author 3932968
 *
 */
public class LocationInfo {

    @NotBlank
    private String code;
    @NotNull
    @Valid
    private Address address;

    public LocationInfo() {}

    /**
     * @param code
     * @param address
     */
    public LocationInfo(String code, Address address) {

        this.code = code;
        this.address = address;
    }

    /**
     * @return the code
     */
    public String getCode() {

        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(
            String code) {

        this.code = code;
    }

    /**
     * @return the address
     */
    public Address getAddress() {

        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(
            Address address) {

        this.address = address;
    }

    @Override
    public String toString() {

        return "Location [code=" + code + ", address=" + address + "]";
    }

}
