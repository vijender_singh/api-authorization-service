package com.fedex.rscs.dto;

import java.util.List;

/**
 * Represents the Confirmed Shipment Resource DTO
 * 
 * @author 3900109
 *
 */
public class ConfirmedShipmentResource {

    private String guid;
    private CarrierDetail carrierDetails;
    private TrackingSequence masterTrackingId;
    private int packageCount;
    private List<ConfirmedPackageDetail> packageLineItems;

    public ConfirmedShipmentResource() {}

    /**
     * @return the guid
     */
    public String getGuid() {

        return guid;
    }

    /**
     * @param guid the guid to set
     */
    public void setGuid(
            String guid) {

        this.guid = guid;
    }

    /**
     * @return the carrierDetails
     */
    public CarrierDetail getCarrierDetails() {

        return carrierDetails;
    }

    /**
     * @param carrierDetails the carrierDetails to set
     */
    public void setCarrierDetails(
            CarrierDetail carrierDetails) {

        this.carrierDetails = carrierDetails;
    }

    /**
     * @return the masterTrackingId
     */
    public TrackingSequence getMasterTrackingId() {

        return masterTrackingId;
    }

    /**
     * @param masterTrackingId the masterTrackingId to set
     */
    public void setMasterTrackingId(
            TrackingSequence masterTrackingId) {

        this.masterTrackingId = masterTrackingId;
    }

    /**
     * @return the packageCount
     */
    public int getPackageCount() {

        return packageCount;
    }

    /**
     * @param packageCount the packageCount to set
     */
    public void setPackageCount(
            int packageCount) {

        this.packageCount = packageCount;
    }

    /**
     * @return the packageLineItems
     */
    public List<ConfirmedPackageDetail> getPackageLineItems() {

        return packageLineItems;
    }

    /**
     * @param packageLineItems the packageLineItems to set
     */
    public void setPackageLineItems(
            List<ConfirmedPackageDetail> packageLineItems) {

        this.packageLineItems = packageLineItems;
    }

    @Override
    public String toString() {

        return "ConfirmedShipmentResource [guid=" + guid + ", carrierDetails=" + carrierDetails + ", masterTrackingId="
                + masterTrackingId + ", packageCount=" + packageCount + ", packageLineItems=" + packageLineItems + "]";
    }

}
