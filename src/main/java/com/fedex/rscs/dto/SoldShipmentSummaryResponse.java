package com.fedex.rscs.dto;

import java.util.List;

import com.fedex.common.cxs.dto.CXSOutput;

/**
 * Sold shipment summary response dto
 * 
 * @author 3932956
 *
 */
public class SoldShipmentSummaryResponse extends CXSOutput {

    private List<SoldShipmentSummaryResource> soldShipmentSummaries;

    public SoldShipmentSummaryResponse() {
        super();
    }

    /**
     * @return the soldShipmentSummaries
     */
    public List<SoldShipmentSummaryResource> getSoldShipmentSummaries() {

        return soldShipmentSummaries;
    }

    /**
     * @param soldShipmentSummaries the soldShipmentSummaries to set
     */
    public void setSoldShipmentSummaries(
            List<SoldShipmentSummaryResource> soldShipmentSummaries) {

        this.soldShipmentSummaries = soldShipmentSummaries;
    }

    @Override
    public String toString() {

        return "SoldShipmentSummaryResponse [soldShipmentSummaries=" + soldShipmentSummaries + "]";
    }

}
