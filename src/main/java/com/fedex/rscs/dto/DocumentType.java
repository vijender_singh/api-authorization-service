package com.fedex.rscs.dto;

/**
 * The enums is use to define the document type.
 * 
 * @author 3900109
 *
 */
public enum DocumentType {
    COMMERCIAL_INVOICE,
    CERTIFICATE_OF_ORIGIN,
    LABEL,
    PRO_FORMA_INVOICE;
  }

