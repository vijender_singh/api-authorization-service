package com.fedex.rscs.dto;


/**
 * This enum class contains customer verification methods
 * 
 * @author Vijender.Singh
 *
 */
public enum CustomerVerificationMethod {
    DRIVING_LICENSE, PASSPORT, STATE_ID, MILITARY_ID
}
