package com.fedex.rscs.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the Shipment Transaction Detail DTO
 * 
 * @author 3883424
 *
 */
public class ShipmentTransactionDetail {

    private String guid;
    private CarrierDetail carrierDetails;
    private TrackingSequence masterTrackingId;
    private int packageCount;
    private List<ConfirmedPackageDetail> packageLineItems;
    private boolean lineItemSuccess;
    private List<Notification> packageNotifications;

    public ShipmentTransactionDetail() {}

    /**
     * @return the guid
     */
    public String getGuid() {

        return guid;
    }

    /**
     * @param guid the guid to set
     */
    public void setGuid(
            String guid) {

        this.guid = guid;
    }

    /**
     * @return the carrierDetails
     */
    public CarrierDetail getCarrierDetails() {

        return carrierDetails;
    }

    /**
     * @param carrierDetails the carrierDetails to set
     */
    public void setCarrierDetails(
            CarrierDetail carrierDetails) {

        this.carrierDetails = carrierDetails;
    }

    /**
     * @return the masterTrackingId
     */
    public TrackingSequence getMasterTrackingId() {

        return masterTrackingId;
    }

    /**
     * @param masterTrackingId the masterTrackingId to set
     */
    public void setMasterTrackingId(
            TrackingSequence masterTrackingId) {

        this.masterTrackingId = masterTrackingId;
    }

    /**
     * @return the packageCount
     */
    public int getPackageCount() {

        return packageCount;
    }

    /**
     * @param packageCount the packageCount to set
     */
    public void setPackageCount(
            int packageCount) {

        this.packageCount = packageCount;
    }

    /**
     * @return the packageLineItems
     */
    public List<ConfirmedPackageDetail> getPackageLineItems() {

        return packageLineItems;
    }

    /**
     * @param packageLineItems the packageLineItems to set
     */
    public void setPackageLineItems(
            List<ConfirmedPackageDetail> packageLineItems) {

        this.packageLineItems = packageLineItems;
    }

    /**
     * @return the lineItemSuccess
     */
    public boolean isLineItemSuccess() {

        return lineItemSuccess;
    }

    /**
     * @param lineItemSuccess the lineItemSuccess to set
     */
    public void setLineItemSuccess(
            boolean lineItemSuccess) {

        this.lineItemSuccess = lineItemSuccess;
    }

    /**
     * @return the packageNotifications
     */
    public List<Notification> getPackageNotifications() {

        return packageNotifications;
    }

    /**
     * @param packageNotifications the packageNotifications to set
     */
    public void setPackageNotifications(
            List<Notification> packageNotifications) {

        this.packageNotifications = packageNotifications;
    }

    /**
     * Add Notification associated with the underline transaction.
     * 
     * @param Notification
     */
    public void addNotification(
            final Notification notification) {

        if(packageNotifications==null) {
            packageNotifications = new ArrayList<>();
        }
        if (null != notification) {
            this.packageNotifications.add(notification);
        }
    }

    @Override
    public String toString() {

        return "ShipmentTransactionDetail [guid=" + guid + ", carrierDetails=" + carrierDetails + ", masterTrackingId="
                + masterTrackingId + ", packageCount=" + packageCount + ", packageLineItems=" + packageLineItems
                + ", lineItemSuccess=" + lineItemSuccess + ", packageNotifications=" + packageNotifications + "]";
    }
}
