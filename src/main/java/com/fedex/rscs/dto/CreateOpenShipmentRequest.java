package com.fedex.rscs.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Represents the Create Open Shipment Request DTO
 * 
 * @author 3932968
 *
 */
public class CreateOpenShipmentRequest {

    @NotNull
    @Valid
    private CreateOpenShipmentRequestData createOpenShipmentRequest;

    public CreateOpenShipmentRequest() {}

    /**
     * @param createOpenShipmentRequest
     */
    public CreateOpenShipmentRequest(CreateOpenShipmentRequestData createOpenShipmentRequest) {

        this.createOpenShipmentRequest = createOpenShipmentRequest;
    }

    /**
     * @return the createOpenShipmentRequest
     */
    public CreateOpenShipmentRequestData getCreateOpenShipmentRequest() {

        return createOpenShipmentRequest;
    }

    /**
     * @param createOpenShipmentRequest the createOpenShipmentRequest to set
     */
    public void setCreateOpenShipmentRequest(
            CreateOpenShipmentRequestData createOpenShipmentRequest) {

        this.createOpenShipmentRequest = createOpenShipmentRequest;
    }

    @Override
    public String toString() {

        return "CreateOpenShipmentRequest [createOpenShipmentRequest=" + createOpenShipmentRequest + "]";
    }

}
