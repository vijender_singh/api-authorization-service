package com.fedex.rscs.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Delete sold shipment request data dto
 * 
 * @author 3932956
 *
 */
public class DeleteSoldShipmentRequestData {

    @NotNull
    @Valid
    private RequestHeaderDetail headerDetails;
    @NotNull
    @Valid
    private LocationInfo locationDetails;
    @NotBlank
    private String trackingId;
    @NotBlank
    private String retailTransactionId;
    @NotNull
    private PersonName customerName;
    
    public DeleteSoldShipmentRequestData() {
       
    }

    /**
     * @param headerDetails
     * @param locationDetails
     * @param trackingId
     * @param retailTransactionId
     * @param customerName
     */
    public DeleteSoldShipmentRequestData(RequestHeaderDetail headerDetails, LocationInfo locationDetails,
            String trackingId, String retailTransactionId, PersonName customerName) {

        this.headerDetails = headerDetails;
        this.locationDetails = locationDetails;
        this.trackingId = trackingId;
        this.retailTransactionId = retailTransactionId;
        this.customerName = customerName;
    }

    /**
     * @return the headerDetails
     */
    public RequestHeaderDetail getHeaderDetails() {

        return headerDetails;
    }

    /**
     * @param headerDetails the headerDetails to set
     */
    public void setHeaderDetails(
            RequestHeaderDetail headerDetails) {

        this.headerDetails = headerDetails;
    }

    /**
     * @return the locationDetails
     */
    public LocationInfo getLocationDetails() {

        return locationDetails;
    }

    /**
     * @param locationDetails the locationDetails to set
     */
    public void setLocationDetails(
            LocationInfo locationDetails) {

        this.locationDetails = locationDetails;
    }

    /**
     * @return the trackingId
     */
    public String getTrackingId() {

        return trackingId;
    }

    /**
     * @param trackingId the trackingId to set
     */
    public void setTrackingId(
            String trackingId) {

        this.trackingId = trackingId;
    }

    /**
     * @return the retailTransactionId
     */
    public String getRetailTransactionId() {

        return retailTransactionId;
    }

    /**
     * @param retailTransactionId the retailTransactionId to set
     */
    public void setRetailTransactionId(
            String retailTransactionId) {

        this.retailTransactionId = retailTransactionId;
    }

    /**
     * @return the customerName
     */
    public PersonName getCustomerName() {

        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(
            PersonName customerName) {

        this.customerName = customerName;
    }

    @Override
    public String toString() {

        return "DeleteSoldShipmentRequestData [headerDetails=" + headerDetails + ", locationDetails=" + locationDetails
                + ", trackingId=" + trackingId + ", retailTransactionId=" + retailTransactionId + ", customerName="
                + customerName + "]";
    }

}
