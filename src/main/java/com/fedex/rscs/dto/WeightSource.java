package com.fedex.rscs.dto;

/**
 * Enum to hold weight sources
 * 
 * @author 3932968
 *
 */
public enum WeightSource {
    MANUAL, SCALE;
}
