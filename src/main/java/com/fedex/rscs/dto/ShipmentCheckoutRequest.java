package com.fedex.rscs.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Represents the Shipment Checkout Request DTO
 * 
 * @author 3932968
 *
 */
public class ShipmentCheckoutRequest {

    @NotNull
    @Valid
    private ShipmentCheckoutRequestData shipmentCheckoutRequest;

    public ShipmentCheckoutRequest() {}

    /**
     * @param shipmentCheckoutRequest
     */
    public ShipmentCheckoutRequest(ShipmentCheckoutRequestData shipmentCheckoutRequest) {

        this.shipmentCheckoutRequest = shipmentCheckoutRequest;
    }

    /**
     * @return the shipmentCheckoutRequest
     */
    public ShipmentCheckoutRequestData getShipmentCheckoutRequest() {

        return shipmentCheckoutRequest;
    }

    /**
     * @param shipmentCheckoutRequest the shipmentCheckoutRequest to set
     */
    public void setShipmentCheckoutRequest(
            ShipmentCheckoutRequestData shipmentCheckoutRequest) {

        this.shipmentCheckoutRequest = shipmentCheckoutRequest;
    }

    @Override
    public String toString() {

        return "ShipmentCheckoutRequest [shipmentCheckoutRequest=" + shipmentCheckoutRequest + "]";
    }

}
