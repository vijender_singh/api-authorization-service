package com.fedex.rscs.dto;

/**
 * Define the Special Service Description DTO
 * 
 * @author 3932968
 *
 */
public class SpecialServiceDescription {

    private String specialServiceType;
    private String specialServiceSubType;
    private String displayText;
    private String description;

    public SpecialServiceDescription() {}

    /**
     * @param specialServiceType
     * @param specialServiceSubType
     * @param displayText
     * @param description
     */
    public SpecialServiceDescription(String specialServiceType, String specialServiceSubType, String displayText,
            String description) {

        this.specialServiceType = specialServiceType;
        this.specialServiceSubType = specialServiceSubType;
        this.displayText = displayText;
        this.description = description;
    }

    /**
     * 
     * @return specialServiceType
     */
    public String getSpecialServiceType() {

        return specialServiceType;
    }

    /**
     * 
     * @param specialServiceType to set specialServiceType
     */
    public void setSpecialServiceType(
            String specialServiceType) {

        this.specialServiceType = specialServiceType;
    }

    /**
     * 
     * @return specialServiceSubType
     */
    public String getSpecialServiceSubType() {

        return specialServiceSubType;
    }

    /**
     * 
     * @param specialServiceSubType to set specialServiceSubType
     */
    public void setSpecialServiceSubType(
            String specialServiceSubType) {

        this.specialServiceSubType = specialServiceSubType;
    }

    /**
     * 
     * @return displayText
     */
    public String getDisplayText() {

        return displayText;
    }

    /**
     * 
     * @param displayText to set displayText
     */
    public void setDisplayText(
            String displayText) {

        this.displayText = displayText;
    }

    /**
     * 
     * @return description
     */
    public String getDescription() {

        return description;
    }

    /**
     * 
     * @param description to set description
     */

    public void setDescription(
            String description) {

        this.description = description;
    }

    @Override
    public String toString() {

        return "SpecialServiceDescription [specialServiceType=" + specialServiceType + ", specialServiceSubType="
                + specialServiceSubType + ", displayText=" + displayText + ", description=" + description + "]";
    }
}