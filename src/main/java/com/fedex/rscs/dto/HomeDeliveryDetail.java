package com.fedex.rscs.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Home Delivery DTO to set the Home delivery details
 * 
 * @author Vijender.Singh
 *
 */
public class HomeDeliveryDetail {

    @NotNull
    private HomeDeliveryType homeDeliveryType;
    private String deliveryDate;
    @Valid
    private PhoneNumber phoneNumber;

    public HomeDeliveryDetail() {}

    /**
     * @param homeDeliveryType
     * @param deliveryDate
     * @param phoneNumber
     */
    public HomeDeliveryDetail(HomeDeliveryType homeDeliveryType, String deliveryDate, PhoneNumber phoneNumber) {

        this.homeDeliveryType = homeDeliveryType;
        this.deliveryDate = deliveryDate;
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return homeDeliveryType
     */
    public HomeDeliveryType getHomeDeliveryType() {

        return homeDeliveryType;
    }

    /**
     * @param homeDeliveryType to set homeDeliveryType 
     */
    public void setHomeDeliveryType(
            HomeDeliveryType homeDeliveryType) {

        this.homeDeliveryType = homeDeliveryType;
    }

    /**
     * @return deliveryDate
     */
    public String getDeliveryDate() {

        return deliveryDate;
    }

    /**
     * @param deliveryDate to set delivery date
     */
    public void setDeliveryDate(
            String deliveryDate) {

        this.deliveryDate = deliveryDate;
    }

    /**
     * @return phoneNumber
     */
    public PhoneNumber getPhoneNumber() {

        return phoneNumber;
    }

    /**
     * @param phoneNumber to set phone number
     */
    public void setPhoneNumber(
            PhoneNumber phoneNumber) {

        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {

        return "HomeDeliveryDetail [homeDeliveryType=" + homeDeliveryType + ", deliveryDate=" + deliveryDate + ", phoneNumber="
                + phoneNumber + "]";
    }
}