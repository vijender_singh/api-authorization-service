package com.fedex.rscs.dto;

import com.fedex.common.cxs.dto.CXSOutput;

/**
 * Delete open shipment response dto
 * 
 * @author 3932956
 *
 */
public class DeleteOpenShipmentResponse extends CXSOutput {

    private String trackingId;

    /**
     * Default constructor
     */
    public DeleteOpenShipmentResponse() {
        
        super();
    }

    /**
     * @param trackingId
     */
    public DeleteOpenShipmentResponse(String trackingId) {
        
        super();
        this.trackingId = trackingId;
    }

    /**
     * @return the trackingId
     */
    public String getTrackingId() {

        return trackingId;
    }

    /**
     * @param trackingId the trackingId to set
     */
    public void setTrackingId(
            String trackingId) {

        this.trackingId = trackingId;
    }

    @Override
    public String toString() {

        return "DeleteOpenShipmentResponse [trackingId=" + trackingId + "]";
    }

}
