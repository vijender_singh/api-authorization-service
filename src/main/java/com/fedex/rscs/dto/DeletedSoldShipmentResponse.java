package com.fedex.rscs.dto;

import com.fedex.common.cxs.dto.CXSOutput;

/**
 * Deleted sold shipment response dto
 * 
 * @author 3932956
 *
 */
public class DeletedSoldShipmentResponse extends CXSOutput {

    private DeletedSoldShipmentResource deletedSoldShipment;

    public DeletedSoldShipmentResponse() {

    }

    /**
     * @return the deletedSoldShipment
     */
    public DeletedSoldShipmentResource getDeletedSoldShipment() {

        return deletedSoldShipment;
    }

    /**
     * @param deletedSoldShipment the deletedSoldShipment to set
     */
    public void setDeletedSoldShipment(
            DeletedSoldShipmentResource deletedSoldShipment) {

        this.deletedSoldShipment = deletedSoldShipment;
    }

    @Override
    public String toString() {

        return "DeletedSoldShipmentResponse [deletedSoldShipment=" + deletedSoldShipment + "]";
    }

}
