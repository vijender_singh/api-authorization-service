package com.fedex.rscs.dto;

/**
 * Enum to hold Printing Orientation
 * 
 * @author 3932968
 *
 */
public enum PrintingOrientation {
    TOP_EDGE_OF_TEXT_FIRST;
}
