package com.fedex.rscs.dto;

/**
 * Enum to hold RatingType
 * 
 * @author 3932968
 *
 */
public enum RatingType {
    ONE_RATE, STANDARD_RATE;
}
