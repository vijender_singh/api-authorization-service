package com.fedex.rscs.dto;

import java.util.List;

/**
 * Represents the Shipment Checkout Resource Data DTO
 * 
 * @author 3883424
 *
 */
public class ShipmentCheckoutResourceData {

    private List<ShipmentTransactionDetail> transactionDetails;

    public ShipmentCheckoutResourceData() {}

    /**
     * @return the transactionDetails
     */
    public List<ShipmentTransactionDetail> getTransactionDetails() {

        return transactionDetails;
    }

    /**
     * @param transactionDetails the transactionDetails to set
     */
    public void setTransactionDetails(
            List<ShipmentTransactionDetail> transactionDetails) {

        this.transactionDetails = transactionDetails;
    }

    @Override
    public String toString() {

        return "ShipmentCheckoutResourceData [transactionDetails=" + transactionDetails + "]";
    }

}
