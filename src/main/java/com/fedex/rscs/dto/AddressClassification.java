package com.fedex.rscs.dto;

/**
 * Enum to hold Address Classification
 * 
 * @author 3932968
 *
 */
public enum AddressClassification {
    HOME, BUSINESS;
}
