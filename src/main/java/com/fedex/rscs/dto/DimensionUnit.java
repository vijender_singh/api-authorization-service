package com.fedex.rscs.dto;

/**
 * Enum to hold Dimension Unit
 * 
 * @author 3932968
 *
 */
public enum DimensionUnit {
    IN, CM;
}
