package com.fedex.rscs.dto;

/**
 * Represents the Money DTO
 * 
 * @author 3932968
 *
 */
public class Money {

    private String pattern = "^[0-9]+(\\.[0-9]+)?$";

    public Money() {}

    /**
     * @param pattern
     */
    public Money(String pattern) {

        this.pattern = pattern;
    }

    /**
     * 
     * @return pattern
     */
    public String getPattern() {

        return pattern;
    }

    /**
     * 
     * @param pattern to set pattern
     */
    public void setPattern(
            String pattern) {

        this.pattern = pattern;
    }

    @Override
    public String toString() {

        return "Money [pattern=" + pattern + "]";
    }

}
