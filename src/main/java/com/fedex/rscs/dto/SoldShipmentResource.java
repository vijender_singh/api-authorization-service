package com.fedex.rscs.dto;

import java.util.List;

/**
 * Sold shipment resource dto
 * 
 * @author 3932956
 *
 */
public class SoldShipmentResource {

    private String guid;
    private CarrierDetail carrierDetails;
    private TrackingSequence masterTrackingId;
    private String serviceType;
    private String transactionDateTime;
    private String status;
    private String retailTransactionId;
    private List<PackageLineItems> packageLineItems;
    private Party shipper;
    private Party recipient;
    private Party customer;
    private String packagingType;
    private String packageCount;
    private PaymentType paymentType;
    private Price totalAmount;

    public SoldShipmentResource() {

        // default constructor
    }

    /**
     * @return the guid
     */
    public String getGuid() {

        return guid;
    }

    /**
     * @param guid the guid to set
     */
    public void setGuid(
            String guid) {

        this.guid = guid;
    }

    /**
     * @return the carrierDetails
     */
    public CarrierDetail getCarrierDetails() {

        return carrierDetails;
    }

    /**
     * @param carrierDetails the carrierDetails to set
     */
    public void setCarrierDetails(
            CarrierDetail carrierDetails) {

        this.carrierDetails = carrierDetails;
    }

    /**
     * @return the masterTrackingId
     */
    public TrackingSequence getMasterTrackingId() {

        return masterTrackingId;
    }

    /**
     * @param masterTrackingId the masterTrackingId to set
     */
    public void setMasterTrackingId(
            TrackingSequence masterTrackingId) {

        this.masterTrackingId = masterTrackingId;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {

        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(
            String serviceType) {

        this.serviceType = serviceType;
    }

    /**
     * @return the transactionDateTime
     */
    public String getTransactionDateTime() {

        return transactionDateTime;
    }

    /**
     * @param transactionDateTime the transactionDateTime to set
     */
    public void setTransactionDateTime(
            String transactionDateTime) {

        this.transactionDateTime = transactionDateTime;
    }

    /**
     * @return the status
     */
    public String getStatus() {

        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(
            String status) {

        this.status = status;
    }

    /**
     * @return the retailTransactionId
     */
    public String getRetailTransactionId() {

        return retailTransactionId;
    }

    /**
     * @param retailTransactionId the retailTransactionId to set
     */
    public void setRetailTransactionId(
            String retailTransactionId) {

        this.retailTransactionId = retailTransactionId;
    }

    /**
     * @return the packageLineItems
     */
    public List<PackageLineItems> getPackageLineItems() {

        return packageLineItems;
    }

    /**
     * @param packageLineItems the packageLineItems to set
     */
    public void setPackageLineItems(
            List<PackageLineItems> packageLineItems) {

        this.packageLineItems = packageLineItems;
    }

    /**
     * @return the shipper
     */
    public Party getShipper() {

        return shipper;
    }

    /**
     * @param shipper the shipper to set
     */
    public void setShipper(
            Party shipper) {

        this.shipper = shipper;
    }

    /**
     * @return the recipient
     */
    public Party getRecipient() {

        return recipient;
    }

    /**
     * @param recipient the recipient to set
     */
    public void setRecipient(
            Party recipient) {

        this.recipient = recipient;
    }

    /**
     * @return the customer
     */
    public Party getCustomer() {

        return customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(
            Party customer) {

        this.customer = customer;
    }

    /**
     * @return the packagingType
     */
    public String getPackagingType() {

        return packagingType;
    }

    /**
     * @param packagingType the packagingType to set
     */
    public void setPackagingType(
            String packagingType) {

        this.packagingType = packagingType;
    }

    /**
     * @return the packageCount
     */
    public String getPackageCount() {

        return packageCount;
    }

    /**
     * @param packageCount the packageCount to set
     */
    public void setPackageCount(
            String packageCount) {

        this.packageCount = packageCount;
    }

    /**
     * @return the paymentType
     */
    public PaymentType getPaymentType() {

        return paymentType;
    }

    /**
     * @param paymentType the paymentType to set
     */
    public void setPaymentType(
            PaymentType paymentType) {

        this.paymentType = paymentType;
    }

    /**
     * @return the totalAmount
     */
    public Price getTotalAmount() {

        return totalAmount;
    }

    /**
     * @param totalAmount the totalAmount to set
     */
    public void setTotalAmount(
            Price totalAmount) {

        this.totalAmount = totalAmount;
    }

    @Override
    public String toString() {

        return "SoldShipmentResource [guid=" + guid + ", carrierDetails=" + carrierDetails + ", masterTrackingId="
                + masterTrackingId + ", serviceType=" + serviceType + ", transactionDateTime=" + transactionDateTime
                + ", status=" + status + ", retailTransactionId=" + retailTransactionId + ", packageLineItems="
                + packageLineItems + ", shipper=" + shipper + ", recipient=" + recipient + ", customer=" + customer
                + ", packagingType=" + packagingType + ", packageCount=" + packageCount + ", paymentType=" + paymentType
                + ", totalAmount=" + totalAmount + "]";
    }

}
