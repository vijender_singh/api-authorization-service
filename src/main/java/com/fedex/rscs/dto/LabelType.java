package com.fedex.rscs.dto;

/**
 * Enum to hold the Label Type
 * 
 * @author 3932968
 *
 */
public enum LabelType {
    OPERATIONAL_LABEL, COMMON2D;
}
