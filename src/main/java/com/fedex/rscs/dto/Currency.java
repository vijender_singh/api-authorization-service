package com.fedex.rscs.dto;

/**
 * Enum to hold currency
 * 
 * @author 3932968
 *
 */
public enum Currency {
    USD;
}
