package com.fedex.rscs.dto;

/**
 * The enums is use to define the type of notification.
 * <li>WARNING
 * <li>NOTE
 * <li>ERROR <br>
 * Based on notification type client will take appropriate actions.
 * 
 * @author 3883424
 *
 */
public enum NotificationType {
    WARNING, NOTE, ERROR
}
