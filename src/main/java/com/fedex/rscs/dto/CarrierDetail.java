package com.fedex.rscs.dto;

/**
 * Represents the CarrierDetail DTO
 * 
 * @author 3932968
 *
 */
public class CarrierDetail {

    private CarrierCodeType carrierCode;
    private CarrierType carrierType;

    public CarrierDetail() {}

    /**
     * @return the carrierCode
     */
    public CarrierCodeType getCarrierCode() {

        return carrierCode;
    }

    /**
     * @param carrierCode the carrierCode to set
     */
    public void setCarrierCode(
            CarrierCodeType carrierCode) {

        this.carrierCode = carrierCode;
    }

    /**
     * @return the carrierType
     */
    public CarrierType getCarrierType() {

        return carrierType;
    }

    /**
     * @param carrierType the carrierType to set
     */
    public void setCarrierType(
            CarrierType carrierType) {

        this.carrierType = carrierType;
    }

    @Override
    public String toString() {

        return "CarrierDetail [carrierCode=" + carrierCode + ", carrierType=" + carrierType + "]";
    }

}
