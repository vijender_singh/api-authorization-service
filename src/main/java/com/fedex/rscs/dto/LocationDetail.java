package com.fedex.rscs.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Represents the Location Detail DTO
 * 
 * @author 3932968
 *
 */
public class LocationDetail {

    private String cityCenterAccountNumber;
    private String groundAccountNumber;
    @NotNull
    @Valid
    private LocationInfo transactionLocationInfo;
    private boolean groundAfterPickup;
    private boolean expressAfterPickup;
    private LocationInfo servingLocationInfo;

    public LocationDetail() {}

    /**
     * @param cityCenterAccountNumber
     * @param groundAccountNumber
     * @param transactionLocationInfo
     * @param groundAfterPickup
     * @param expressAfterPickup
     * @param servingLocationInfo
     */
    public LocationDetail(String cityCenterAccountNumber, String groundAccountNumber,
            LocationInfo transactionLocationInfo, boolean groundAfterPickup, boolean expressAfterPickup,
            LocationInfo servingLocationInfo) {

        this.cityCenterAccountNumber = cityCenterAccountNumber;
        this.groundAccountNumber = groundAccountNumber;
        this.transactionLocationInfo = transactionLocationInfo;
        this.groundAfterPickup = groundAfterPickup;
        this.expressAfterPickup = expressAfterPickup;
        this.servingLocationInfo = servingLocationInfo;
    }

    /**
     * 
     * @return cityCenterAccountNumber
     */
    public String getCityCenterAccountNumber() {

        return cityCenterAccountNumber;
    }

    /**
     * 
     * @param cityCenterAccountNumber to set cityCenterAccountNumber
     */
    public void setCityCenterAccountNumber(
            String cityCenterAccountNumber) {

        this.cityCenterAccountNumber = cityCenterAccountNumber;
    }

    /**
     * 
     * @return groundAccountNumber
     */
    public String getGroundAccountNumber() {

        return groundAccountNumber;
    }

    /**
     * 
     * @param groundAccountNumber to set groundAccountNumber
     */
    public void setGroundAccountNumber(
            String groundAccountNumber) {

        this.groundAccountNumber = groundAccountNumber;
    }

    /**
     * 
     * @return transactionLocationInfo
     */
    public LocationInfo getTransactionLocationInfo() {

        return transactionLocationInfo;
    }

    /**
     * 
     * @param transactionLocationInfo to set transactionLocationInfo
     */
    public void setTransactionLocationInfo(
            LocationInfo transactionLocationInfo) {

        this.transactionLocationInfo = transactionLocationInfo;
    }

    /**
     * 
     * @return groundAfterPickup
     */
    public boolean isGroundAfterPickup() {

        return groundAfterPickup;
    }

    /**
     * 
     * @param groundAfterPickup to set groundAfterPickup
     */
    public void setGroundAfterPickup(
            boolean groundAfterPickup) {

        this.groundAfterPickup = groundAfterPickup;
    }

    /**
     * 
     * @return expressAfterPickup
     */
    public boolean isExpressAfterPickup() {

        return expressAfterPickup;
    }

    /**
     * 
     * @param expressAfterPickup to set expressAfterPickup
     */
    public void setExpressAfterPickup(
            boolean expressAfterPickup) {

        this.expressAfterPickup = expressAfterPickup;
    }

    /**
     * @return the servingLocationInfo
     */
    public LocationInfo getServingLocationInfo() {

        return servingLocationInfo;
    }

    /**
     * @param servingLocationInfo the servingLocationInfo to set
     */
    public void setServingLocationInfo(
            LocationInfo servingLocationInfo) {

        this.servingLocationInfo = servingLocationInfo;
    }

    @Override
    public String toString() {

        return "LocationDetail [cityCenterAccountNumber=" + cityCenterAccountNumber + ", groundAccountNumber="
                + groundAccountNumber + ", transactionLocationInfo=" + transactionLocationInfo + ", groundAfterPickup="
                + groundAfterPickup + ", expressAfterPickup=" + expressAfterPickup + ", servingLocationInfo="
                + servingLocationInfo + "]";
    }

}
