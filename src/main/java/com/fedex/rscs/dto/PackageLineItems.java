package com.fedex.rscs.dto;

/**
 * Package line items dto
 * 
 * @author 3932956
 *
 */
public class PackageLineItems {

    private TrackingSequence trackingId;
    private boolean master;

    public PackageLineItems() {

        // default constructor
    }

    /**
     * @return the trackingId
     */
    public TrackingSequence getTrackingId() {

        return trackingId;
    }

    /**
     * @param trackingId the trackingId to set
     */
    public void setTrackingId(
            TrackingSequence trackingId) {

        this.trackingId = trackingId;
    }

    /**
     * @return the master
     */
    public boolean isMaster() {

        return master;
    }

    /**
     * @param master the master to set
     */
    public void setMaster(
            boolean master) {

        this.master = master;
    }

    @Override
    public String toString() {

        return "PackageLineItems [trackingId=" + trackingId + ", master=" + master + "]";
    }

}
