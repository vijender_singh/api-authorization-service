package com.fedex.rscs.dto;

/**
 * Payment Source enum
 * 
 * @author 3811225
 *
 */
public enum PaymentSource {

    SPOS;
}
