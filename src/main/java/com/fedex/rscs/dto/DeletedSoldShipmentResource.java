package com.fedex.rscs.dto;

/**
 * Deleted sold shipment resource dto
 * 
 * @author 3932956
 *
 */
public class DeletedSoldShipmentResource {

    private String trackingNumber;
    private CarrierType carrierType;

    public DeletedSoldShipmentResource() {

    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {

        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {

        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the carrierType
     */
    public CarrierType getCarrierType() {

        return carrierType;
    }

    /**
     * @param carrierType the carrierType to set
     */
    public void setCarrierType(
            CarrierType carrierType) {

        this.carrierType = carrierType;
    }

    @Override
    public String toString() {

        return "DeletedSoldShipmentResource [trackingNumber=" + trackingNumber + ", carrierType=" + carrierType + "]";
    }

}
