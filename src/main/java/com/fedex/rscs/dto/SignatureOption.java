package com.fedex.rscs.dto;

/**
 * Enum to hold Signature Option
 * 
 * @author 3932968
 *
 */
public enum SignatureOption {
    ADULT, DIRECT, INDIRECT, NO_SIGNATURE_REQUIRED, SERVICE_DEFAULT;
}
