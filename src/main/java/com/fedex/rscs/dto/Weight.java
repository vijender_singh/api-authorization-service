package com.fedex.rscs.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Define the Weight DTO
 * 
 * @author 3932968
 *
 */
public class Weight {

    @NotNull
    private WeightUnit units;
    @NotBlank
    private String value;
    private WeightSource measurementType;

    public Weight() {}

    /**
     * @param units
     * @param value
     * @param measurementType
     */
    public Weight(WeightUnit units, String value, WeightSource measurementType) {

        this.units = units;
        this.value = value;
        this.measurementType = measurementType;
    }

    /**
     * @return the units
     */
    public WeightUnit getUnits() {

        return units;
    }

    /**
     * @param units the units to set
     */
    public void setUnits(
            WeightUnit units) {

        this.units = units;
    }

    /**
     * @return the value
     */
    public String getValue() {

        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(
            String value) {

        this.value = value;
    }

    /**
     * @return the measurementType
     */
    public WeightSource getMeasurementType() {

        return measurementType;
    }

    /**
     * @param measurementType the measurementType to set
     */
    public void setMeasurementType(
            WeightSource measurementType) {

        this.measurementType = measurementType;
    }

    @Override
    public String toString() {

        return "Weight [units=" + units + ", value=" + value + ", measurementType=" + measurementType + "]";
    }

}
