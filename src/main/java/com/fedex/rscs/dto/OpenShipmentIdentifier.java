package com.fedex.rscs.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Represents the Open Shipment Identifier DTO
 * 
 * @author 3900109
 *
 */
public class OpenShipmentIdentifier {

    @NotBlank
    private String referenceId;
    @NotNull
    @Valid
    private TrackingSequence masterTrackingId;

    public OpenShipmentIdentifier() {}

    /**
     * @param referenceId
     * @param masterTrackingId
     */
    public OpenShipmentIdentifier(String referenceId, TrackingSequence masterTrackingId) {

        this.referenceId = referenceId;
        this.masterTrackingId = masterTrackingId;
    }

    /**
     * @return the referenceId
     */
    public String getReferenceId() {

        return referenceId;
    }

    /**
     * @param referenceId the referenceId to set
     */
    public void setReferenceId(
            String referenceId) {

        this.referenceId = referenceId;
    }

    /**
     * @return the masterTrackingId
     */
    public TrackingSequence getMasterTrackingId() {

        return masterTrackingId;
    }

    /**
     * @param masterTrackingId the masterTrackingId to set
     */
    public void setMasterTrackingId(
            TrackingSequence masterTrackingId) {

        this.masterTrackingId = masterTrackingId;
    }

    @Override
    public String toString() {

        return "OpenShipmentIdentifier [referenceId=" + referenceId + ", masterTrackingId=" + masterTrackingId + "]";
    }
}
