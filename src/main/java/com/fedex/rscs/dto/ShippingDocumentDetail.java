package com.fedex.rscs.dto;

/**
 * Represents the Shipping Document Detail DTO
 * 
 * @author 3900109
 *
 */
public class ShippingDocumentDetail {

    private String image;
    private ImageType imageType;
    private StockType stockType;
    private DocumentType documentType;

    public ShippingDocumentDetail() {}

    /**
     * @return the image
     */
    public String getImage() {

        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(
            String image) {

        this.image = image;
    }

    /**
     * @return the imageType
     */
    public ImageType getImageType() {

        return imageType;
    }

    /**
     * @param imageType the imageType to set
     */
    public void setImageType(
            ImageType imageType) {

        this.imageType = imageType;
    }

    /**
     * @return the stockType
     */
    public StockType getStockType() {

        return stockType;
    }

    /**
     * @param stockType the stockType to set
     */
    public void setStockType(
            StockType stockType) {

        this.stockType = stockType;
    }

    /**
     * @return the documentType
     */
    public DocumentType getDocumentType() {

        return documentType;
    }

    /**
     * @param documentType the documentType to set
     */
    public void setDocumentType(
            DocumentType documentType) {

        this.documentType = documentType;
    }

    @Override
    public String toString() {

        return "ShippingDocumentDetail [image=" + image + ", imageType=" + imageType + ", stockType=" + stockType
                + ", documentType=" + documentType + "]";
    }
}
