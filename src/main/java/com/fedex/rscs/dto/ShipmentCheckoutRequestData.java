package com.fedex.rscs.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Represents the Shipment Checkout Request Data DTO
 * 
 * @author 3883424
 *
 */
public class ShipmentCheckoutRequestData {

    @NotNull
    @Valid
    private RequestHeaderDetail headerDetails;
    @NotNull
    @Valid
    private LocationDetail locationDetails;
    @NotNull
    @Valid
    private List<OpenShipmentIdentifier> requestedShipment;
    private LabelSpecification labelSpecification;
    @Valid
    private ShippingPaymentDetail paymentDetails;

    public ShipmentCheckoutRequestData() {}

    /**
     * @param headerDetails
     * @param locationDetails
     * @param requestedShipment
     * @param labelSpecification
     * @param paymentDetails
     */
    public ShipmentCheckoutRequestData(RequestHeaderDetail headerDetails, LocationDetail locationDetails,
            List<OpenShipmentIdentifier> requestedShipment, LabelSpecification labelSpecification,
            ShippingPaymentDetail paymentDetails) {

        this.headerDetails = headerDetails;
        this.locationDetails = locationDetails;
        this.requestedShipment = requestedShipment;
        this.labelSpecification = labelSpecification;
        this.paymentDetails = paymentDetails;
    }

    /**
     * 
     * @return headerDetails
     */
    public RequestHeaderDetail getHeaderDetails() {

        return headerDetails;
    }

    /**
     * 
     * @param headerDetails to set headerDetails
     */
    public void setHeaderDetails(
            RequestHeaderDetail headerDetails) {

        this.headerDetails = headerDetails;
    }

    /**
     * 
     * @return locationDetails
     */
    public LocationDetail getLocationDetails() {

        return locationDetails;
    }

    /**
     * 
     * @param locationDetails to set locationDetails
     */
    public void setLocationDetails(
            LocationDetail locationDetails) {

        this.locationDetails = locationDetails;
    }

    /**
     * @return the requestedShipment
     */
    public List<OpenShipmentIdentifier> getRequestedShipment() {

        return requestedShipment;
    }

    /**
     * @param requestedShipment the requestedShipment to set
     */
    public void setRequestedShipment(
            List<OpenShipmentIdentifier> requestedShipment) {

        this.requestedShipment = requestedShipment;
    }

    /**
     * @return the labelSpecification
     */
    public LabelSpecification getLabelSpecification() {

        return labelSpecification;
    }

    /**
     * @param labelSpecification the labelSpecification to set
     */
    public void setLabelSpecification(
            LabelSpecification labelSpecification) {

        this.labelSpecification = labelSpecification;
    }

    /**
     * @return the paymentDetails
     */
    public ShippingPaymentDetail getPaymentDetails() {

        return paymentDetails;
    }


    /**
     * @param paymentDetails the paymentDetails to set
     */
    public void setPaymentDetails(
            ShippingPaymentDetail paymentDetails) {

        this.paymentDetails = paymentDetails;
    }

    @Override
    public String toString() {

        return "ShipmentCheckoutRequestData [headerDetails=" + headerDetails + ", locationDetails=" + locationDetails
                + ", requestedShipment=" + requestedShipment + ", labelSpecification=" + labelSpecification
                + ", paymentDetails=" + paymentDetails + "]";
    }

}

