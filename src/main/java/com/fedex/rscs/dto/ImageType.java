package com.fedex.rscs.dto;

/**
 *  Created to define the image type as PNG or PDF
 * 
 * @author 3932968
 *
 */
public enum ImageType {
    PNG, PDF, ZPLII;
}
