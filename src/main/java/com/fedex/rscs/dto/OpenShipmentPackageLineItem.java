package com.fedex.rscs.dto;

import java.util.List;

/**
 * Represents the Open Shipment Package LineItem DTO
 * 
 * @author 3932968
 *
 */
public class OpenShipmentPackageLineItem {

    private Dimensions dimensions;
    private Price insuredValue;
    private PackageSpecialHandlingType specialHandlingDetail;
    private List<SpecialServiceDescription> specialServicesDetails;
    private Weight weight;
    private SignatureOption signatureOption;
    private TrackingSequence trackingId;
    private boolean master;

    public OpenShipmentPackageLineItem() {}
    
    /**
     * @return the dimensions
     */
    public Dimensions getDimensions() {

        return dimensions;
    }

    /**
     * @param dimensions the dimensions to set
     */
    public void setDimensions(
            Dimensions dimensions) {

        this.dimensions = dimensions;
    }

    /**
     * @return the insuredValue
     */
    public Price getInsuredValue() {

        return insuredValue;
    }

    /**
     * @param insuredValue the insuredValue to set
     */
    public void setInsuredValue(
            Price insuredValue) {

        this.insuredValue = insuredValue;
    }

    /**
     * @return the specialHandlingDetail
     */
    public PackageSpecialHandlingType getSpecialHandlingDetail() {

        return specialHandlingDetail;
    }

    /**
     * @param specialHandlingDetail the specialHandlingDetail to set
     */
    public void setSpecialHandlingDetail(
            PackageSpecialHandlingType specialHandlingDetail) {

        this.specialHandlingDetail = specialHandlingDetail;
    }

    /**
     * @return the specialServicesDetails
     */
    public List<SpecialServiceDescription> getSpecialServicesDetails() {

        return specialServicesDetails;
    }

    /**
     * @param specialServicesDetails the specialServicesDetails to set
     */
    public void setSpecialServicesDetails(
            List<SpecialServiceDescription> specialServicesDetails) {

        this.specialServicesDetails = specialServicesDetails;
    }

    /**
     * @return the weight
     */
    public Weight getWeight() {

        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(
            Weight weight) {

        this.weight = weight;
    }

    /**
     * @return the signatureOption
     */
    public SignatureOption getSignatureOption() {

        return signatureOption;
    }

    /**
     * @param signatureOption the signatureOption to set
     */
    public void setSignatureOption(
            SignatureOption signatureOption) {

        this.signatureOption = signatureOption;
    }

    /**
     * @return the trackingId
     */
    public TrackingSequence getTrackingId() {

        return trackingId;
    }

    /**
     * @param trackingId the trackingId to set
     */
    public void setTrackingId(
            TrackingSequence trackingId) {

        this.trackingId = trackingId;
    }

    /**
     * @return the master
     */
    public boolean isMaster() {

        return master;
    }

    /**
     * @param master the master to set
     */
    public void setMaster(
            boolean master) {

        this.master = master;
    }

    @Override
    public String toString() {

        return "OpenShipmentPackageLineItem [dimensions=" + dimensions + ", insuredValue=" + insuredValue
                + ", specialHandlingDetail=" + specialHandlingDetail + ", specialServicesDetails="
                + specialServicesDetails + ", weight=" + weight + ", signatureOption=" + signatureOption
                + ", trackingId=" + trackingId + ", master=" + master + "]";
    }

}
