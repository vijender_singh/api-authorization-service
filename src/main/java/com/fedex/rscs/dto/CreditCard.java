package com.fedex.rscs.dto;

/**
 * Define the Credit Card DTO
 * 
 * @author 3932968
 *
 */
public class CreditCard {

    private String maskedCreditCard;
    private CreditCardType type;
    private String expirationDate;

    public CreditCard() {}

    /**
     * @param maskedCreditCard
     * @param type
     * @param expirationDate
     */
    public CreditCard(String maskedCreditCard, CreditCardType type, String expirationDate) {

        this.maskedCreditCard = maskedCreditCard;
        this.type = type;
        this.expirationDate = expirationDate;
    }

    /**
     * @return the maskedCreditCard
     */
    public String getMaskedCreditCard() {

        return maskedCreditCard;
    }

    /**
     * @param maskedCreditCard the maskedCreditCard to set
     */
    public void setMaskedCreditCard(
            String maskedCreditCard) {

        this.maskedCreditCard = maskedCreditCard;
    }

    /**
     * @return the type
     */
    public CreditCardType getType() {

        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(
            CreditCardType type) {

        this.type = type;
    }

    /**
     * @return the expirationDate
     */
    public String getExpirationDate() {

        return expirationDate;
    }

    /**
     * @param expirationDate the expirationDate to set
     */
    public void setExpirationDate(
            String expirationDate) {

        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {

        return "CreditCard [ type=" + type + ", expirationDate=" + expirationDate + "]";
    }

}
