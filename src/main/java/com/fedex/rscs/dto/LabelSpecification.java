package com.fedex.rscs.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * Represents the Label Specification DTO.
 * 
 * @author 3932968
 *
 */
public class LabelSpecification {

    private String processingOptionsRequested;
    private List<String> disposition;
    @NotNull
    private LabelType formatType;
    @NotNull
    private ImageType imageType;
    @NotNull
    private StockType stockType;
    @NotNull
    private PrintingOrientation printingOrientation;
    private String rotation;
    private String labelOrder;
    private String localization;
    private String labelOrigin;
    private String customerSpecifiedDetail;

    public LabelSpecification() {

    }

    /**
     * @param formatType
     * @param imageType
     * @param stockType
     * @param printingOrientation
     */
    public LabelSpecification(LabelType formatType, ImageType imageType, StockType stockType,
            PrintingOrientation printingOrientation) {

        this.formatType = formatType;
        this.imageType = imageType;
        this.stockType = stockType;
        this.printingOrientation = printingOrientation;
    }

    /**
     * @return the processingOptionsRequested
     */
    public String getProcessingOptionsRequested() {

        return processingOptionsRequested;
    }

    /**
     * @param processingOptionsRequested the processingOptionsRequested to set
     */
    public void setProcessingOptionsRequested(
            String processingOptionsRequested) {

        this.processingOptionsRequested = processingOptionsRequested;
    }

    /**
     * @return the disposition
     */
    public List<String> getDisposition() {

        return disposition;
    }

    /**
     * @param disposition the disposition to set
     */
    public void setDisposition(
            List<String> disposition) {

        this.disposition = disposition;
    }

    /**
     * @return the formatType
     */
    public LabelType getFormatType() {

        return formatType;
    }

    /**
     * @param formatType the formatType to set
     */
    public void setFormatType(
            LabelType formatType) {

        this.formatType = formatType;
    }

    /**
     * @return the imageType
     */
    public ImageType getImageType() {

        return imageType;
    }

    /**
     * @param imageType the imageType to set
     */
    public void setImageType(
            ImageType imageType) {

        this.imageType = imageType;
    }

    /**
     * @return the stockType
     */
    public StockType getStockType() {

        return stockType;
    }

    /**
     * @param stockType the stockType to set
     */
    public void setStockType(
            StockType stockType) {

        this.stockType = stockType;
    }

    /**
     * @return the printingOrientation
     */
    public PrintingOrientation getPrintingOrientation() {

        return printingOrientation;
    }

    /**
     * @param printingOrientation the printingOrientation to set
     */
    public void setPrintingOrientation(
            PrintingOrientation printingOrientation) {

        this.printingOrientation = printingOrientation;
    }

    /**
     * @return the rotation
     */
    public String getRotation() {

        return rotation;
    }

    /**
     * @param rotation the rotation to set
     */
    public void setRotation(
            String rotation) {

        this.rotation = rotation;
    }

    /**
     * @return the labelOrder
     */
    public String getLabelOrder() {

        return labelOrder;
    }

    /**
     * @param labelOrder the labelOrder to set
     */
    public void setLabelOrder(
            String labelOrder) {

        this.labelOrder = labelOrder;
    }

    /**
     * @return the localization
     */
    public String getLocalization() {

        return localization;
    }

    /**
     * @param localization the localization to set
     */
    public void setLocalization(
            String localization) {

        this.localization = localization;
    }

    /**
     * @return the labelOrigin
     */
    public String getLabelOrigin() {

        return labelOrigin;
    }

    /**
     * @param labelOrigin the labelOrigin to set
     */
    public void setLabelOrigin(
            String labelOrigin) {

        this.labelOrigin = labelOrigin;
    }

    /**
     * @return the customerSpecifiedDetail
     */
    public String getCustomerSpecifiedDetail() {

        return customerSpecifiedDetail;
    }

    /**
     * @param customerSpecifiedDetail the customerSpecifiedDetail to set
     */
    public void setCustomerSpecifiedDetail(
            String customerSpecifiedDetail) {

        this.customerSpecifiedDetail = customerSpecifiedDetail;
    }

    @Override
    public String toString() {

        return "LabelSpecification [processingOptionsRequested=" + processingOptionsRequested + ", disposition="
                + disposition + ", formatType=" + formatType + ", imageType=" + imageType + ", stockType=" + stockType
                + ", printingOrientation=" + printingOrientation + ", rotation=" + rotation + ", labelOrder="
                + labelOrder + ", localization=" + localization + ", labelOrigin=" + labelOrigin
                + ", customerSpecifiedDetail=" + customerSpecifiedDetail + "]";
    }

}
