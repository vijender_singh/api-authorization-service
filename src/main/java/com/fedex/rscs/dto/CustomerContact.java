package com.fedex.rscs.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Size;

/**
 * Define the CustomerContact specification DTO
 * 
 * @author 3900109
 *
 */
public class CustomerContact {

    @Size(max = 35)
    private String personName;
    @Valid
    private Company company;
    private CustomerEmailDetail emailDetails;
    @Valid
    private List<PhoneNumberDetail> phoneNumberDetails;

    public CustomerContact() {}

    /**
     * @param personName
     * @param company
     * @param emailDetails
     * @param phoneNumberDetails
     */
    public CustomerContact(String personName, Company company, CustomerEmailDetail emailDetails,
            List<PhoneNumberDetail> phoneNumberDetails) {

        this.personName = personName;
        this.company = company;
        this.emailDetails = emailDetails;
        this.phoneNumberDetails = phoneNumberDetails;
    }

    /**
     * @return the personName
     */
    public String getPersonName() {

        return personName;
    }

    /**
     * @param personName the personName to set
     */
    public void setPersonName(
            String personName) {

        this.personName = personName;
    }

    /**
     * @return the company
     */
    public Company getCompany() {

        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(
            Company company) {

        this.company = company;
    }

    /**
     * @return the emailDetails
     */
    public CustomerEmailDetail getEmailDetails() {

        return emailDetails;
    }

    /**
     * @param emailDetails the emailDetails to set
     */
    public void setEmailDetails(
            CustomerEmailDetail emailDetails) {

        this.emailDetails = emailDetails;
    }

    /**
     * @return the phoneNumberDetails
     */
    public List<PhoneNumberDetail> getPhoneNumberDetails() {

        return phoneNumberDetails;
    }

    /**
     * @param phoneNumberDetails the phoneNumberDetails to set
     */
    public void setPhoneNumberDetails(
            List<PhoneNumberDetail> phoneNumberDetails) {

        this.phoneNumberDetails = phoneNumberDetails;
    }

    @Override
    public String toString() {

        return "CustomerContact [personName=" + personName + ", company=" + company + ", emailDetails=" + emailDetails
                + ", phoneNumberDetails=" + phoneNumberDetails + "]";
    }

}
