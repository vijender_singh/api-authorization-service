package com.fedex.rscs.dto;

/**
 * Enum to hold Payment Type
 * 
 * @author 3932968
 *
 */
public enum PaymentType {
    ACCOUNT, CASH, CREDIT_CARD, CHECK, NON_ACCOUNT;
}
