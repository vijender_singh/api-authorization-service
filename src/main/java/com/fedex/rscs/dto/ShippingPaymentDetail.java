package com.fedex.rscs.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Represents the Shipping Payment Detail DTO
 * 
 * @author 3932968
 *
 */
public class ShippingPaymentDetail {

    @NotNull
    private PaymentType paymentType;
    private Price amount;
    private ResponsibleParty responsibleParty;
    @Valid
    private Payor payor;
    @NotNull
    private PaymentSource paymentSource;
    private String  paymentRefId; 

    public ShippingPaymentDetail() {

        // default Constructor
    }

    /**
     * @param paymentType
     * @param amount
     * @param responsibleParty
     * @param payor
     * @param paymentSource
     */
    public ShippingPaymentDetail(PaymentType paymentType, Price amount, ResponsibleParty responsibleParty, Payor payor,
            PaymentSource paymentSource) {

        this.paymentType = paymentType;
        this.amount = amount;
        this.responsibleParty = responsibleParty;
        this.payor = payor;
        this.paymentSource = paymentSource;
    }

    /**
     * @return the paymentType
     */
    public PaymentType getPaymentType() {

        return paymentType;
    }

    /**
     * @param paymentType the paymentType to set
     */
    public void setPaymentType(
            PaymentType paymentType) {

        this.paymentType = paymentType;
    }

    /**
     * @return the amount
     */
    public Price getAmount() {

        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(
            Price amount) {

        this.amount = amount;
    }

    /**
     * @return the responsibleParty
     */
    public ResponsibleParty getResponsibleParty() {

        return responsibleParty;
    }

    /**
     * @param responsibleParty the responsibleParty to set
     */
    public void setResponsibleParty(
            ResponsibleParty responsibleParty) {

        this.responsibleParty = responsibleParty;
    }

    /**
     * @return the payor
     */
    public Payor getPayor() {

        return payor;
    }

    /**
     * @param payor the payor to set
     */
    public void setPayor(
            Payor payor) {

        this.payor = payor;
    }

    /**
     * @return the paymentSource
     */
    public PaymentSource getPaymentSource() {

        return paymentSource;
    }

    /**
     * @param paymentSource the paymentSource to set
     */
    public void setPaymentSource(
            PaymentSource paymentSource) {

        this.paymentSource = paymentSource;
    }

    
    public String getPaymentRefId() {
    
        return paymentRefId;
    }

    
    public void setPaymentRefId(
            String paymentRefId) {
    
        this.paymentRefId = paymentRefId;
    }

    @Override
    public String toString() {

        return "ShippingPaymentDetail [paymentType=" + paymentType + ", amount=" + amount + ", responsibleParty="
                + responsibleParty + ", payor=" + payor + ", paymentSource=" + paymentSource + ", paymentRefId="
                + paymentRefId + "]";
    }

}
