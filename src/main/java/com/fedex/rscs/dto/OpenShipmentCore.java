package com.fedex.rscs.dto;

import java.util.List;

import com.fedex.rscs.client.srrs.dto.CommitDetail;

/**
 * Represents the Open Shipment DTO
 * 
 * @author 3932968
 *
 */
public class OpenShipmentCore {

    private String locationId;
    private String referenceId;
    private CarrierDetail carrierDetails;
    private String serviceType;
    private String packagingType;
    private int packageCount;
    private Party recipient;
    private ShipmentSpecialServiceDescription specialServicesRequested;
    private Price totalInsuredValue;
    private Weight totalWeight;
    private List<OpenShipmentPackageLineItem> packageLineItems;
    private ShippingRateDetail rateDetails;
    private ShippingPaymentDetail paymentDetails;
    private CommitDetail commitDetails;

    /**
     * @return the locationId
     */
    public String getLocationId() {

        return locationId;
    }

    /**
     * @param locationId the locationId to set
     */
    public void setLocationId(
            String locationId) {

        this.locationId = locationId;
    }

    /**
     * @return the referenceId
     */
    public String getReferenceId() {

        return referenceId;
    }

    /**
     * @param referenceId the referenceId to set
     */
    public void setReferenceId(
            String referenceId) {

        this.referenceId = referenceId;
    }

    /**
     * @return the carrierDetails
     */
    public CarrierDetail getCarrierDetails() {

        return carrierDetails;
    }

    /**
     * @param carrierDetails the carrierDetails to set
     */
    public void setCarrierDetails(
            CarrierDetail carrierDetails) {

        this.carrierDetails = carrierDetails;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {

        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(
            String serviceType) {

        this.serviceType = serviceType;
    }

    /**
     * @return the packagingType
     */
    public String getPackagingType() {

        return packagingType;
    }

    /**
     * @param packagingType the packagingType to set
     */
    public void setPackagingType(
            String packagingType) {

        this.packagingType = packagingType;
    }

    /**
     * @return the packageCount
     */
    public int getPackageCount() {

        return packageCount;
    }

    /**
     * @param packageCount the packageCount to set
     */
    public void setPackageCount(
            int packageCount) {

        this.packageCount = packageCount;
    }

    /**
     * @return the recipient
     */
    public Party getRecipient() {

        return recipient;
    }

    /**
     * @param recipient the recipient to set
     */
    public void setRecipient(
            Party recipient) {

        this.recipient = recipient;
    }

    /**
     * @return the specialServicesRequested
     */
    public ShipmentSpecialServiceDescription getSpecialServicesRequested() {

        return specialServicesRequested;
    }

    /**
     * @param specialServicesRequested the specialServicesRequested to set
     */
    public void setSpecialServicesRequested(
            ShipmentSpecialServiceDescription specialServicesRequested) {

        this.specialServicesRequested = specialServicesRequested;
    }

    /**
     * @return the totalInsuredValue
     */
    public Price getTotalInsuredValue() {

        return totalInsuredValue;
    }

    /**
     * @param totalInsuredValue the totalInsuredValue to set
     */
    public void setTotalInsuredValue(
            Price totalInsuredValue) {

        this.totalInsuredValue = totalInsuredValue;
    }

    /**
     * @return the totalWeight
     */
    public Weight getTotalWeight() {

        return totalWeight;
    }

    /**
     * @param totalWeight the totalWeight to set
     */
    public void setTotalWeight(
            Weight totalWeight) {

        this.totalWeight = totalWeight;
    }

    /**
     * @return the packageLineItems
     */
    public List<OpenShipmentPackageLineItem> getPackageLineItems() {

        return packageLineItems;
    }

    /**
     * @param packageLineItems the packageLineItems to set
     */
    public void setPackageLineItems(
            List<OpenShipmentPackageLineItem> packageLineItems) {

        this.packageLineItems = packageLineItems;
    }

    /**
     * @return the rateDetails
     */
    public ShippingRateDetail getRateDetails() {

        return rateDetails;
    }

    /**
     * @param rateDetails the rateDetails to set
     */
    public void setRateDetails(
            ShippingRateDetail rateDetails) {

        this.rateDetails = rateDetails;
    }

    /**
     * @return the paymentDetails
     */
    public ShippingPaymentDetail getPaymentDetails() {

        return paymentDetails;
    }

    /**
     * @param paymentDetails the paymentDetails to set
     */
    public void setPaymentDetails(
            ShippingPaymentDetail paymentDetails) {

        this.paymentDetails = paymentDetails;
    }

    /**
     * @return the commitDetails
     */
    public CommitDetail getCommitDetails() {

        return commitDetails;
    }

    /**
     * @param commitDetails the commitDetails to set
     */
    public void setCommitDetails(
            CommitDetail commitDetails) {

        this.commitDetails = commitDetails;
    }

    @Override
    public String toString() {

        return "OpenShipmentCore [locationId=" + locationId + ", referenceId=" + referenceId + ", carrierDetails="
                + carrierDetails + ", serviceType=" + serviceType + ", packagingType=" + packagingType
                + ", packageCount=" + packageCount + ", recipient=" + recipient + ", specialServicesRequested="
                + specialServicesRequested + ", totalInsuredValue=" + totalInsuredValue + ", totalWeight=" + totalWeight
                + ", packageLineItems=" + packageLineItems + ", rateDetails=" + rateDetails + ", paymentDetails="
                + paymentDetails + ", commitDetails=" + commitDetails + "]";
    }
}
