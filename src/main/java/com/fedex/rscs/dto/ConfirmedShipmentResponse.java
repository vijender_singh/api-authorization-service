package com.fedex.rscs.dto;

import com.fedex.common.cxs.dto.CXSOutput;

/**
 * Represents the Confirmed Shipment Response DTO
 * 
 * @author 3932968
 *
 */
public class ConfirmedShipmentResponse extends CXSOutput {

    private ConfirmedShipmentResource confirmedShipment;

    public ConfirmedShipmentResponse() {

        super();
    }

    /**
     * @param confirmedShipment
     */
    public ConfirmedShipmentResponse(ConfirmedShipmentResource confirmedShipment) {

        super();
        this.confirmedShipment = confirmedShipment;
    }

    /**
     * @return the confirmedShipment
     */
    public ConfirmedShipmentResource getConfirmedShipment() {

        return confirmedShipment;
    }

    /**
     * @param confirmedShipment the confirmedShipment to set
     */
    public void setConfirmedShipment(
            ConfirmedShipmentResource confirmedShipment) {

        this.confirmedShipment = confirmedShipment;
    }

    @Override
    public String toString() {

        return "ConfirmedShipmentResponse [confirmedShipment=" + confirmedShipment + "]";
    }

}
