package com.fedex.rscs.dto;

/**
 * Enum to hold WeightUnit
 * 
 * @author 3932968
 *
 */
public enum WeightUnit {
    LB, KG;
}
