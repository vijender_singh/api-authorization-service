package com.fedex.rscs.dto;

import com.fedex.common.cxs.dto.CXSOutput;

/**
 * Represents the Create Open Shipment Response DTO
 * 
 * @author 3932968
 *
 */
public class CreateOpenShipmentResponse extends CXSOutput {

    private OpenShipmentCore openShipment;

    /**
     * 
     */
    public CreateOpenShipmentResponse() {}

    /**
     * @param openShipment
     */
    public CreateOpenShipmentResponse(OpenShipmentCore openShipment) {

        super();
        this.openShipment = openShipment;
    }


    /**
     * @return the openShipment
     */
    public OpenShipmentCore getOpenShipment() {

        return openShipment;
    }


    /**
     * @param openShipment the openShipment to set
     */
    public void setOpenShipment(
            OpenShipmentCore openShipment) {

        this.openShipment = openShipment;
    }

    @Override
    public String toString() {

        return "CreateOpenShipmentResponse [openShipment=" + openShipment + "]";
    }

}
