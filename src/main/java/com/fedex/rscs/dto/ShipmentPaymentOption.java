package com.fedex.rscs.dto;

import javax.validation.constraints.NotNull;

/**
 * Define the ShipmentPaymentOption DTO.
 * 
 * @author 3932968
 *
 */
public class ShipmentPaymentOption {

    @NotNull
    private RequestedPaymentType paymentType;
    private String accountNumber;
    private ResponsibleParty responsibleParty;
    @NotNull
    private PaymentSource paymentSource;
    private String interlineId;

    public ShipmentPaymentOption() {}

    /**
     * @param paymentType
     * @param accountNumber
     * @param responsibleParty
     * @param paymentSource
     */
    public ShipmentPaymentOption(RequestedPaymentType paymentType, String accountNumber,
            ResponsibleParty responsibleParty, PaymentSource paymentSource) {

        this.paymentType = paymentType;
        this.accountNumber = accountNumber;
        this.responsibleParty = responsibleParty;
        this.paymentSource = paymentSource;
    }

    /**
     * @return the paymentType
     */
    public RequestedPaymentType getPaymentType() {

        return paymentType;
    }

    /**
     * @param paymentType the paymentType to set
     */
    public void setPaymentType(
            RequestedPaymentType paymentType) {

        this.paymentType = paymentType;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {

        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(
            String accountNumber) {

        this.accountNumber = accountNumber;
    }

    /**
     * @return the responsibleParty
     */
    public ResponsibleParty getResponsibleParty() {

        return responsibleParty;
    }

    /**
     * @param responsibleParty the responsibleParty to set
     */
    public void setResponsibleParty(
            ResponsibleParty responsibleParty) {

        this.responsibleParty = responsibleParty;
    }

    /**
     * @return the paymentSource
     */
    public PaymentSource getPaymentSource() {

        return paymentSource;
    }

    /**
     * @param paymentSource the paymentSource to set
     */
    public void setPaymentSource(
            PaymentSource paymentSource) {

        this.paymentSource = paymentSource;
    }

    /**
     * @return the interlineId
     */
    public String getInterlineId() {

        return interlineId;
    }

    /**
     * @param interlineId the interlineId to set
     */
    public void setInterlineId(
            String interlineId) {

        this.interlineId = interlineId;
    }

    @Override
    public String toString() {

        return "ShipmentPaymentOption [paymentType=" + paymentType + ", accountNumber=" + accountNumber
                + ", responsibleParty=" + responsibleParty + ", paymentSource=" + paymentSource + ", interlineId="
                + interlineId + "]";
    }

}
