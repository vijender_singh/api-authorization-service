package com.fedex.rscs.dto;

import javax.validation.constraints.NotNull;

/**
 * Represents the Tracking Sequence DTO
 * 
 * @author 3932968
 *
 */
public class TrackingSequence {

    @NotNull
    private String trackingNumber;
    private String barcode;
    private int sequence;

    public TrackingSequence() {}

    
    /**
     * @param trackingNumber
     * @param barcode
     * @param sequence
     */
    public TrackingSequence(@NotNull String trackingNumber, String barcode, int sequence) {

        this.trackingNumber = trackingNumber;
        this.barcode = barcode;
        this.sequence = sequence;
    }


    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {

        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {

        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the barcode
     */
    public String getBarcode() {

        return barcode;
    }

    /**
     * @param barcode the barcode to set
     */
    public void setBarcode(
            String barcode) {

        this.barcode = barcode;
    }

    /**
     * @return the sequence
     */
    public int getSequence() {

        return sequence;
    }

    /**
     * @param sequence the sequence to set
     */
    public void setSequence(
            int sequence) {

        this.sequence = sequence;
    }

    @Override
    public String toString() {

        return "TrackingSequence [trackingNumber=" + trackingNumber + ", barcode=" + barcode + ", sequence=" + sequence
                + "]";
    }

}
