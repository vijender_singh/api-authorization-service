package com.fedex.rscs.dto;

import javax.validation.constraints.NotNull;

/**
 * Define the Price DTO
 * 
 * @author 3932968
 *
 */
public class Price {

    @NotNull
    private Currency currency;
    @NotNull
    private Double amount;

    public Price() {}

    /**
     * @param currency
     * @param amount
     */
    public Price(Currency currency, Double amount) {

        this.currency = currency;
        this.amount = amount;
    }

    /**
     * 
     * @return currency
     */
    public Currency getCurrency() {

        return currency;
    }

    /**
     * 
     * @param currency to set currency
     */
    public void setCurrency(
            Currency currency) {

        this.currency = currency;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {

        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(
            Double amount) {

        this.amount = amount;
    }

    @Override
    public String toString() {

        return "Price [currency=" + currency + ", amount=" + amount + "]";
    }

}
