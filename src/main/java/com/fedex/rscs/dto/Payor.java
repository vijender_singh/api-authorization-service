package com.fedex.rscs.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * Represents the Payor DTO
 * 
 * @author 3932968
 *
 */
public class Payor {

    private AccountType accountType;
    private String accountNumber;
    private CreditCard creditCard;
    @Valid
    private Address address;

    public Payor() {}

    /**
     * @param accountType
     * @param accountNumber
     * @param creditCard
     * @param address
     */
    public Payor(AccountType accountType, String accountNumber, CreditCard creditCard, Address address) {

        this.accountType = accountType;
        this.accountNumber = accountNumber;
        this.creditCard = creditCard;
        this.address = address;
    }

    /**
     * @return the address
     */
    public Address getAddress() {

        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(
            Address address) {

        this.address = address;
    }

    /**
     * @return the accountType
     */
    public AccountType getAccountType() {

        return accountType;
    }

    /**
     * @param accountType the accountType to set
     */
    public void setAccountType(
            AccountType accountType) {

        this.accountType = accountType;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {

        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(
            String accountNumber) {

        this.accountNumber = accountNumber;
    }

    /**
     * @return the creditCard
     */
    public CreditCard getCreditCard() {

        return creditCard;
    }

    /**
     * @param creditCard the creditCard to set
     */
    public void setCreditCard(
            CreditCard creditCard) {

        this.creditCard = creditCard;
    }

}
