package com.fedex.rscs.dto;

/**
 * Represents the Confirmed Package Detail DTO
 * 
 * @author 3900109
 *
 */
public class ConfirmedPackageDetail {

    private TrackingSequence trackingId;
    private boolean master;
    private ShippingDocumentDetail label;

    public ConfirmedPackageDetail() {}

    /**
     * @return the trackingId
     */
    public TrackingSequence getTrackingId() {

        return trackingId;
    }

    /**
     * @param trackingId the trackingId to set
     */
    public void setTrackingId(
            TrackingSequence trackingId) {

        this.trackingId = trackingId;
    }

    /**
     * @return the master
     */
    public boolean isMaster() {

        return master;
    }

    /**
     * @param master the master to set
     */
    public void setMaster(
            boolean master) {

        this.master = master;
    }

    /**
     * @return the label
     */
    public ShippingDocumentDetail getLabel() {

        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(
            ShippingDocumentDetail label) {

        this.label = label;
    }

    @Override
    public String toString() {

        return "ConfirmedPackageDetail [trackingId=" + trackingId + ", master=" + master + "]";
    }
}
