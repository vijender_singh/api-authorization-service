package com.fedex.rscs.dto;

/**
 * Enum to hold Responsible Party
 * 
 * @author 3932968
 *
 */
public enum ResponsibleParty {
    SENDER, RECIPIENT, THIRD_PARTY;
}
