package com.fedex.rscs.dto;

import java.util.List;

import com.fedex.common.cxs.dto.CXSOutput;

/**
 * Represents the Retrieve Open Shipment Response DTO
 * 
 * @author 3932956
 *
 */
public class RetrieveOpenShipmentResponse extends CXSOutput {

    private List<OpenShipmentCore> openShipments;

    public RetrieveOpenShipmentResponse() {

        super();
    }

    /**
     * @return the openShipment
     */
    public List<OpenShipmentCore> getOpenShipments() {

        return openShipments;
    }

    /**
     * @param openShipment the openShipment to set
     */
    public void setOpenShipments(
            List<OpenShipmentCore> openShipments) {

        this.openShipments = openShipments;
    }

    @Override
    public String toString() {

        return "RetrieveOpenShipmentResponse [openShipments=" + openShipments + "]";
    }

}
