package com.fedex.rscs.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Represents the Address DTO
 * 
 * @author 3932968
 *
 */
public class Address {

    @Size(max = 3)
    private List<String> streetLines;
    @Size(max = 50)
    private String city;
    @Size(max = 2)
    private String stateOrProvinceCode;
    @Size(max = 10)
    private String postalCode;
    @NotBlank
    @Size(max = 10)
    private String countryCode;
    private AddressClassification addressClassification;

    public Address() {}

    /**
     * @param streetLines
     * @param city
     * @param stateOrProvinceCode
     * @param postalCode
     * @param countryCode
     * @param addressClassification
     */
    public Address(List<String> streetLines, String city, String stateOrProvinceCode, String postalCode,
            String countryCode, AddressClassification addressClassification) {

        this.streetLines = streetLines;
        this.city = city;
        this.stateOrProvinceCode = stateOrProvinceCode;
        this.postalCode = postalCode;
        this.countryCode = countryCode;
        this.addressClassification = addressClassification;
    }

    /**
     * @return the streetLines
     */
    public List<String> getStreetLines() {

        return streetLines;
    }

    /**
     * @param streetLines the streetLines to set
     */
    public void setStreetLines(
            List<String> streetLines) {

        this.streetLines = streetLines;
    }

    /**
     * @return the city
     */
    public String getCity() {

        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(
            String city) {

        this.city = city;
    }

    /**
     * @return the stateOrProvinceCode
     */
    public String getStateOrProvinceCode() {

        return stateOrProvinceCode;
    }

    /**
     * @param stateOrProvinceCode the stateOrProvinceCode to set
     */
    public void setStateOrProvinceCode(
            String stateOrProvinceCode) {

        this.stateOrProvinceCode = stateOrProvinceCode;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {

        return postalCode;
    }

    /**
     * @param postalCode the postalCode to set
     */
    public void setPostalCode(
            String postalCode) {

        this.postalCode = postalCode;
    }

    /**
     * @return the countryCode
     */
    public String getCountryCode() {

        return countryCode;
    }

    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(
            String countryCode) {

        this.countryCode = countryCode;
    }

    /**
     * @return the addressClassification
     */
    public AddressClassification getAddressClassification() {

        return addressClassification;
    }

    /**
     * @param addressClassification the addressClassification to set
     */
    public void setAddressClassification(
            AddressClassification addressClassification) {

        this.addressClassification = addressClassification;
    }

    @Override
    public String toString() {

        return "Address [streetLines=" + streetLines + ", city=" + city + ", stateOrProvinceCode=" + stateOrProvinceCode
                + ", postalCode=" + postalCode + ", countryCode=" + countryCode + ", addressClassification="
                + addressClassification + "]";
    }

}

