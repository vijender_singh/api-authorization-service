/**
 * Domain Transfer Objects (DTOs) are intended to be the request and response objects received by the
 * controller layer and passed onto the processor layers.  Processors will transform the request objects
 * into the proper domain/canonical objects.
 */
package com.fedex.rscs.dto;