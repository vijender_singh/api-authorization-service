package com.fedex.rscs.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Represents the Create Open Shipment Request Data DTO
 * 
 * @author 3932968
 *
 */
public class CreateOpenShipmentRequestData {

    @NotNull
    @Valid
    private RequestHeaderDetail headerDetails;
    @NotNull
    @Valid
    private LocationDetail locationDetails;
    @NotNull
    @Valid
    private RequestedShipment requestedShipment;
    @NotNull
    @Valid
    private ShipmentPaymentOption paymentOption;
    @NotNull
    @Valid
    private LabelSpecification labelSpecification;
    @NotNull
    @Valid
    private Person customer;
    private CustomerVerificationMethod customerVerificationMethod;
    @Valid
    private InterlineShippingDetail interlineShippingDetails;

    public CreateOpenShipmentRequestData() {}

    /**
     * @param headerDetails
     * @param locationDetails
     * @param requestedShipment
     * @param paymentOption
     * @param labelSpecification
     * @param customer
     */
    public CreateOpenShipmentRequestData(RequestHeaderDetail headerDetails, LocationDetail locationDetails,
            RequestedShipment requestedShipment, ShipmentPaymentOption paymentOption,
            LabelSpecification labelSpecification, Person customer) {

        this.headerDetails = headerDetails;
        this.locationDetails = locationDetails;
        this.requestedShipment = requestedShipment;
        this.paymentOption = paymentOption;
        this.labelSpecification = labelSpecification;
        this.customer = customer;
    }

    /**
     * 
     * @return headerDetails
     */
    public RequestHeaderDetail getHeaderDetails() {

        return headerDetails;
    }

    /**
     * 
     * @param headerDetails to set headerDetails
     */
    public void setHeaderDetails(
            RequestHeaderDetail headerDetails) {

        this.headerDetails = headerDetails;
    }

    /**
     * 
     * @return locationDetails
     */
    public LocationDetail getLocationDetails() {

        return locationDetails;
    }

    /**
     * 
     * @param locationDetails to set locationDetails
     */
    public void setLocationDetails(
            LocationDetail locationDetails) {

        this.locationDetails = locationDetails;
    }

    /**
     * 
     * @return requestedShipment
     */
    public RequestedShipment getRequestedShipment() {

        return requestedShipment;
    }

    /**
     * 
     * @param requestedShipment to set requestedShipment
     */
    public void setRequestedShipment(
            RequestedShipment requestedShipment) {

        this.requestedShipment = requestedShipment;
    }

    /**
     * @return the paymentDetails
     */
    public ShipmentPaymentOption getPaymentOption() {

        return paymentOption;
    }

    /**
     * @param paymentDetails the paymentDetails to set
     */
    public void setPaymentOption(
            ShipmentPaymentOption paymentDetails) {

        this.paymentOption = paymentDetails;
    }

    /**
     * @return the labelSpecification
     */
    public LabelSpecification getLabelSpecification() {

        return labelSpecification;
    }

    /**
     * @param labelSpecification the labelSpecification to set
     */
    public void setLabelSpecification(
            LabelSpecification labelSpecification) {

        this.labelSpecification = labelSpecification;
    }

    /**
     * @return the customer
     */
    public Person getCustomer() {

        return customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(
            Person customer) {

        this.customer = customer;
    }

    /**
     * @return the customerVerificationMethod
     */
    public CustomerVerificationMethod getCustomerVerificationMethod() {

        return customerVerificationMethod;
    }

    /**
     * @param customerVerificationMethod the customerVerificationMethod to set
     */
    public void setCustomerVerificationMethod(
            CustomerVerificationMethod customerVerificationMethod) {

        this.customerVerificationMethod = customerVerificationMethod;
    }

    /**
     * @return the interlineShippingDetails
     */
    public InterlineShippingDetail getInterlineShippingDetails() {

        return interlineShippingDetails;
    }

    /**
     * @param interlineShippingDetails the interlineShippingDetails to set
     */
    public void setInterlineShippingDetails(
            InterlineShippingDetail interlineShippingDetails) {

        this.interlineShippingDetails = interlineShippingDetails;
    }

    @Override
    public String toString() {

        return "CreateOpenShipmentRequestData [headerDetails=" + headerDetails + ", locationDetails=" + locationDetails
                + ", requestedShipment=" + requestedShipment + ", paymentOption=" + paymentOption
                + ", labelSpecification=" + labelSpecification + ", customer=" + customer
                + ", customerVerificationMethod=" + customerVerificationMethod + ", interlineShippingDetails="
                + interlineShippingDetails + "]";
    }

}
