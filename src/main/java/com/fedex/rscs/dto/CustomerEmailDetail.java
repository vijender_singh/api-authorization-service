package com.fedex.rscs.dto;

/**
 * Define the CustomerEmailDetail specification DTO
 * 
 * @author 3900109
 *
 */
public class CustomerEmailDetail {

    private String emailAddress;
    private String alternateEmailAddress;

    public CustomerEmailDetail() {}

    /**
     * @param emailAddress
     * @param alternateEmailAddress
     */
    public CustomerEmailDetail(String emailAddress, String alternateEmailAddress) {

        this.emailAddress = emailAddress;
        this.alternateEmailAddress = alternateEmailAddress;
    }

    /**
     * 
     * @return emailAddress
     */
    public String getEmailAddress() {

        return emailAddress;
    }

    /**
     * 
     * @param emailAddress to set emailAddress
     */
    public void setEmailAddress(
            String emailAddress) {

        this.emailAddress = emailAddress;
    }

    /**
     * @return the alternateEmailAddress
     */
    public String getAlternateEmailAddress() {

        return alternateEmailAddress;
    }

    /**
     * @param alternateEmailAddress the alternateEmailAddress to set
     */
    public void setAlternateEmailAddress(
            String alternateEmailAddress) {

        this.alternateEmailAddress = alternateEmailAddress;
    }

    @Override
    public String toString() {

        return "CustomerEmailDetail [emailAddress=" + emailAddress + ", alternateEmailAddress=" + alternateEmailAddress
                + "]";
    }

}
