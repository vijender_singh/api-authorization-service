package com.fedex.rscs.dto;

/**
 * Battery classification type enum
 * 
 * @author 3932968
 *
 */
public enum BatteryClassificationType {
    
    LITHIUM_ION_PACKED_WITH_EQUIPMENT, LITHIUM_ION_CONTAINED_IN_EQUIPMENT, LITHIUM_METAL_PACKED_WITH_EQUIPMENT, LITHIUM_METAL_CONTAINED_IN_EQUIPMENT
}
