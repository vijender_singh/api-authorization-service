package com.fedex.rscs.dto;

import java.util.List;

/**
 * Sold shipment summary resource dto
 * 
 * @author 3932956
 *
 */
public class SoldShipmentSummaryResource {

    private String guid;
    private CarrierDetail carrierDetails;
    private TrackingSequence masterTrackingId;
    private String serviceType;
    private String transactionDateTime;
    private String status;
    private String retailTransactionId;
    private List<PackageLineItems> packageLineItems;

    public SoldShipmentSummaryResource() {

    }

    /**
     * @return the guid
     */
    public String getGuid() {

        return guid;
    }

    /**
     * @param guid the guid to set
     */
    public void setGuid(
            String guid) {

        this.guid = guid;
    }

    /**
     * @return the carrierDetails
     */
    public CarrierDetail getCarrierDetails() {

        return carrierDetails;
    }

    /**
     * @param carrierDetails the carrierDetails to set
     */
    public void setCarrierDetails(
            CarrierDetail carrierDetails) {

        this.carrierDetails = carrierDetails;
    }

    /**
     * @return the masterTrackingId
     */
    public TrackingSequence getMasterTrackingId() {

        return masterTrackingId;
    }

    /**
     * @param masterTrackingId the masterTrackingId to set
     */
    public void setMasterTrackingId(
            TrackingSequence masterTrackingId) {

        this.masterTrackingId = masterTrackingId;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {

        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(
            String serviceType) {

        this.serviceType = serviceType;
    }

    /**
     * @return the transactionDateTime
     */
    public String getTransactionDateTime() {

        return transactionDateTime;
    }

    /**
     * @param transactionDateTime the transactionDateTime to set
     */
    public void setTransactionDateTime(
            String transactionDateTime) {

        this.transactionDateTime = transactionDateTime;
    }

    /**
     * @return the status
     */
    public String getStatus() {

        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(
            String status) {

        this.status = status;
    }

    /**
     * @return the retailTransactionId
     */
    public String getRetailTransactionId() {

        return retailTransactionId;
    }

    /**
     * @param retailTransactionId the retailTransactionId to set
     */
    public void setRetailTransactionId(
            String retailTransactionId) {

        this.retailTransactionId = retailTransactionId;
    }

    /**
     * @return the packageLineItems
     */
    public List<PackageLineItems> getPackageLineItems() {

        return packageLineItems;
    }

    /**
     * @param packageLineItems the packageLineItems to set
     */
    public void setPackageLineItems(
            List<PackageLineItems> packageLineItems) {

        this.packageLineItems = packageLineItems;
    }

    @Override
    public String toString() {

        return "SoldShipmentSummaryResource [guid=" + guid + ", carrierDetails=" + carrierDetails
                + ", masterTrackingId=" + masterTrackingId + ", serviceType=" + serviceType + ", transactionDateTime="
                + transactionDateTime + ", status=" + status + ", retailTransactionId=" + retailTransactionId
                + ", packageLineItems=" + packageLineItems + "]";
    }

}
