package com.fedex.rscs.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Delete sold shipment dto
 * 
 * @author 3932956
 *
 */
public class DeleteSoldShipmentRequest {

    @Valid
    private DeleteSoldShipmentRequestData deleteSoldShipmentsRequest;

    public DeleteSoldShipmentRequest() {

    }

    /**
     * @param deleteSoldShipmentsRequest
     */
    public DeleteSoldShipmentRequest(DeleteSoldShipmentRequestData deleteSoldShipmentsRequest) {

        this.deleteSoldShipmentsRequest = deleteSoldShipmentsRequest;
    }

    /**
     * @return the deleteSoldShipmentsRequest
     */
    public DeleteSoldShipmentRequestData getDeleteSoldShipmentsRequest() {

        return deleteSoldShipmentsRequest;
    }

    /**
     * @param deleteSoldShipmentsRequest the deleteSoldShipmentsRequest to set
     */
    public void setDeleteSoldShipmentsRequest(
            DeleteSoldShipmentRequestData deleteSoldShipmentsRequest) {

        this.deleteSoldShipmentsRequest = deleteSoldShipmentsRequest;
    }

    @Override
    public String toString() {

        return "DeleteSoldShipmentRequest [deleteSoldShipmentsRequest=" + deleteSoldShipmentsRequest + "]";
    }

}
