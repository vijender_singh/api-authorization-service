package com.fedex.rscs.dto;

import java.util.List;

import com.fedex.common.cxs.dto.CXSOutput;

/**
 * Sold shipments response dto
 * 
 * @author 3932956
 *
 */
public class SoldShipmentResponse extends CXSOutput {

    private List<SoldShipmentResource> soldShipments;

    public SoldShipmentResponse() {

        super();
    }

    /**
     * @return the soldShipments
     */
    public List<SoldShipmentResource> getSoldShipments() {

        return soldShipments;
    }

    /**
     * @param soldShipments the soldShipments to set
     */
    public void setSoldShipments(
            List<SoldShipmentResource> soldShipments) {

        this.soldShipments = soldShipments;
    }

    @Override
    public String toString() {

        return "SoldShipmentResponse [soldShipments=" + soldShipments + "]";
    }

}
