package com.fedex.rscs.dto;

import java.util.List;

/**
 * Represents the Notifications associated with the transaction details.
 * 
 * @author 3883424
 *
 */
public class Notification {

    private String code;
    private String message;
    private List<String> instructions;
    private NotificationType notificationType;

    public Notification() {}

    public Notification(String code, String message, NotificationType notificationType) {

        this(code, message, notificationType, null);
    }

    public Notification(String code, String message, NotificationType notificationType, List<String> instructions) {

        super();
        this.code = code;
        this.message = message;
        this.instructions = instructions;
        this.notificationType = notificationType;
    }

    /**
     * get Notification code.
     * 
     * @return code
     */
    public String getCode() {

        return code;
    }

    /**
     * set Notification code
     * 
     * @param code
     */
    public void setCode(
            String code) {

        this.code = code;
    }

    /**
     * get message
     * 
     * @return message
     */
    public String getMessage() {

        return message;
    }

    /**
     * set the Notification message
     * 
     * @param message
     */
    public void setMessage(
            String message) {

        this.message = message;
    }

    /**
     * get Notification type
     * 
     * @return NotificationType
     */
    public NotificationType getNotificationType() {

        return notificationType;
    }

    /**
     * set the Notification type
     * 
     * @param NotificationType
     */
    public void setNotificationType(
            NotificationType notificationType) {

        this.notificationType = notificationType;
    }

    /**
     * get the additional instructions associated with the Notification.
     * 
     * @return
     */
    public List<String> getInstructions() {

        return instructions;
    }


    /**
     * set the additional instructions associated with the Notification.
     * 
     * @param instructions
     */
    public void setInstructions(
            List<String> instructions) {

        this.instructions = instructions;
    }

    @Override
    public String toString() {

        return "Notification [code=" + code + ", message=" + message + ", instructions=" + instructions
                + ", NotificationType=" + notificationType + "]";
    }

}

