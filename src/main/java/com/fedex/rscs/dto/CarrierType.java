package com.fedex.rscs.dto;

/**
 * Represents the CarrierType Enum
 * 
 * @author 3932968
 *
 */
public enum CarrierType {
    EXPRESS, GROUND, SMARTPOST;
}
