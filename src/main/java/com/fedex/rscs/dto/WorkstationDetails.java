package com.fedex.rscs.dto;

/**
 * DTO to hold Workstation Details
 * 
 * @author 3932968
 *
 */
public class WorkstationDetails {

    private String deviceId;
    private String meterNumber;
    private String softwareId;
    private AppName appName;
    private String appVersionId;

    public WorkstationDetails() {}

    /**
     * @param deviceId
     * @param meterNumber
     * @param softwareId
     * @param appName
     * @param appVersionId
     */
    public WorkstationDetails(String deviceId, String meterNumber, String softwareId, AppName appName,
            String appVersionId) {

        this.deviceId = deviceId;
        this.meterNumber = meterNumber;
        this.softwareId = softwareId;
        this.appName = appName;
        this.appVersionId = appVersionId;
    }

    /**
     * @return the deviceId
     */
    public String getDeviceId() {

        return deviceId;
    }

    /**
     * @param deviceId the deviceId to set
     */
    public void setDeviceId(
            String deviceId) {

        this.deviceId = deviceId;
    }

    /**
     * @return the meterNumber
     */
    public String getMeterNumber() {

        return meterNumber;
    }

    /**
     * @param meterNumber the meterNumber to set
     */
    public void setMeterNumber(
            String meterNumber) {

        this.meterNumber = meterNumber;
    }

    /**
     * @return the softwareId
     */
    public String getSoftwareId() {

        return softwareId;
    }

    /**
     * @param softwareId the softwareId to set
     */
    public void setSoftwareId(
            String softwareId) {

        this.softwareId = softwareId;
    }

    /**
     * @return the appName
     */
    public AppName getAppName() {

        return appName;
    }

    /**
     * @param appName the appName to set
     */
    public void setAppName(
            AppName appName) {

        this.appName = appName;
    }

    /**
     * @return the appVersionId
     */
    public String getAppVersionId() {

        return appVersionId;
    }

    /**
     * @param appVersionId to set appVersionId
     */
    public void setAppVersionId(
            String appVersionId) {

        this.appVersionId = appVersionId;
    }

    @Override
    public String toString() {

        return "WorkstationDetails [deviceId=" + deviceId + ", meterNumber=" + meterNumber + ", softwareId="
                + softwareId + ", appName=" + appName + ", appVersionId=" + appVersionId + "]";
    }

}

