package com.fedex.rscs.dto;

/**
 * The shipment query type model to retrieve a shipment record.
 *
 * @author 3900094
 */
public enum ShipmentQueryType {
    TRACK, REFID, BARCODE
}
