package com.fedex.rscs.dto;

/**
 * Represents the CarrierCode Enum
 * 
 * @author 3932968
 *
 */
public enum CarrierCodeType {
    FDXE, FDXG, FXSP;
}
