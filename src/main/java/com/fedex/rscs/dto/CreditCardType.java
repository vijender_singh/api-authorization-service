package com.fedex.rscs.dto;

/**
 * Represents the Credit Card Type Enum
 * 
 * @author 3932968
 *
 */
public enum CreditCardType {
    VISA, MASTERCARD, DISCOVER, DINERS, AMEX;
}
