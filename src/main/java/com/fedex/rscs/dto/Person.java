package com.fedex.rscs.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Represents the Person DTO
 * 
 * @author 3900109
 *
 */
public class Person {

    @NotNull @Valid
    private Address address;
    @Valid
    private CustomerContact contact;

    public Person() {}

    /**
     * 
     * @param address
     * @param contact
     */
    public Person(Address address, CustomerContact contact) {

        this.address = address;
        this.contact = contact;
    }

    /**
     * 
     * @return address
     */
    public Address getAddress() {

        return address;
    }

    /**
     * 
     * @param address to set address
     */
    public void setAddress(
            Address address) {

        this.address = address;
    }

    /**
     * 
     * @return contact
     */
    public CustomerContact getContact() {

        return contact;
    }

    /**
     * 
     * @param contact to set contact
     */
    public void setContact(
            CustomerContact contact) {

        this.contact = contact;
    }

    @Override
    public String toString() {

        return "Person [address=" + address + ", contact=" + contact + "]";
    }

}
