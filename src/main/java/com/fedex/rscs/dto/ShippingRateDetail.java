package com.fedex.rscs.dto;

/**
 * Represents the Shipping Rate Detail DTO
 * 
 * @author 3932968
 *
 */
public class ShippingRateDetail {

    private Currency currency;
    private RatingType ratingType;
    private Double totalBaseCharges;
    private Double totalSurcharges;
    private Double totalTaxes;
    private Double totalNetCharges;

    public ShippingRateDetail() {}

    /**
     * @param currency
     * @param ratingType
     * @param totalBaseCharges
     * @param totalSurcharges
     * @param totalTaxes
     * @param totalNetCharges
     */
    public ShippingRateDetail(Currency currency, RatingType ratingType, Double totalBaseCharges,
            Double totalSurcharges, Double totalTaxes, Double totalNetCharges) {

        this.currency = currency;
        this.ratingType = ratingType;
        this.totalBaseCharges = totalBaseCharges;
        this.totalSurcharges = totalSurcharges;
        this.totalTaxes = totalTaxes;
        this.totalNetCharges = totalNetCharges;
    }

    /**
     * @return the currency
     */
    public Currency getCurrency() {

        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(
            Currency currency) {

        this.currency = currency;
    }

    /**
     * @return the ratingType
     */
    public RatingType getRatingType() {

        return ratingType;
    }

    /**
     * @param ratingType the ratingType to set
     */
    public void setRatingType(
            RatingType ratingType) {

        this.ratingType = ratingType;
    }

    /**
     * @return the totalBaseCharges
     */
    public Double getTotalBaseCharges() {

        return totalBaseCharges;
    }

    /**
     * @param totalBaseCharges the totalBaseCharges to set
     */
    public void setTotalBaseCharges(
            Double totalBaseCharges) {

        this.totalBaseCharges = totalBaseCharges;
    }

    /**
     * @return the totalSurcharges
     */
    public Double getTotalSurcharges() {

        return totalSurcharges;
    }

    /**
     * @param totalSurcharges the totalSurcharges to set
     */
    public void setTotalSurcharges(
            Double totalSurcharges) {

        this.totalSurcharges = totalSurcharges;
    }

    /**
     * @return the totalTaxes
     */
    public Double getTotalTaxes() {

        return totalTaxes;
    }

    /**
     * @param totalTaxes the totalTaxes to set
     */
    public void setTotalTaxes(
            Double totalTaxes) {

        this.totalTaxes = totalTaxes;
    }

    /**
     * @return the totalNetCharges
     */
    public Double getTotalNetCharges() {

        return totalNetCharges;
    }

    /**
     * @param totalNetCharges the totalNetCharges to set
     */
    public void setTotalNetCharges(
            Double totalNetCharges) {

        this.totalNetCharges = totalNetCharges;
    }

    @Override
    public String toString() {

        return "ShippingRateDetail [currency=" + currency + ", ratingType=" + ratingType + ", totalBaseCharges="
                + totalBaseCharges + ", totalSurcharges=" + totalSurcharges + ", totalTaxes=" + totalTaxes
                + ", totalNetCharges=" + totalNetCharges + "]";
    }

}
