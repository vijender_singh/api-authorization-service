package com.fedex.rscs.dto;

/**
 * Enum to hold Stock Type
 * 
 * @author 3932968
 *
 */
public enum StockType {
    STOCK_4X6, PAPER_4X6;
}

