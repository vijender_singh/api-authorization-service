package com.fedex.rscs.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Represents the Confirmed Shipment Request DTO
 * 
 * @author 3932968
 *
 */
public class ConfirmedShipmentRequest {

    @NotNull
    @Valid
    private ConfirmShipmentRequestData confirmedShipmentRequest;

    public ConfirmedShipmentRequest() {}

    /**
     * @param confirmedShipmentRequest
     */
    public ConfirmedShipmentRequest(ConfirmShipmentRequestData confirmedShipmentRequest) {

        this.confirmedShipmentRequest = confirmedShipmentRequest;
    }

    /**
     * @return the confirmedShipmentRequest
     */
    public ConfirmShipmentRequestData getConfirmedShipmentRequest() {

        return confirmedShipmentRequest;
    }

    /**
     * @param confirmedShipmentRequest the confirmedShipmentRequest to set
     */
    public void setConfirmedShipmentRequest(
            ConfirmShipmentRequestData confirmedShipmentRequest) {

        this.confirmedShipmentRequest = confirmedShipmentRequest;
    }

    @Override
    public String toString() {

        return "ConfirmedShipmentRequest [confirmedShipmentRequest=" + confirmedShipmentRequest + "]";
    }

}
