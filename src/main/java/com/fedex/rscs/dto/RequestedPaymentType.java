package com.fedex.rscs.dto;

/**
 * Enum to hold Requested Payment Type
 * 
 * @author 3932968
 *
 */
public enum RequestedPaymentType {
    ACCOUNT, NON_ACCOUNT;
}
