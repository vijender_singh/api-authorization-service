package com.fedex.rscs.dto;

/**
 * Shipping customer reference type enum
 * 
 * @author 3932968
 *
 */
public enum ShippingCustomerReferenceType {
    CUSTOMER_REFERENCE, P_O_NUMBER, INVOICE_NUMBER, DEPARTMENT_NUMBER
}
