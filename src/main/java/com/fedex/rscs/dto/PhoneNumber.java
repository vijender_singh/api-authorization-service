package com.fedex.rscs.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Define the Phone Number DTO
 * 
 * @author 3932968
 *
 */
public class PhoneNumber {

    @NotBlank
    @Size(min = 10, max = 10) 
    @Pattern(regexp = "\\d+")
    private String number;
    @Size(max = 5)
    private String extension;

    public PhoneNumber() {}

    /**
     * 
     * @param number
     * @param extension
     */
    public PhoneNumber(String number, String extension) {

        this.number = number;
        this.extension = extension;
    }

    /**
     * 
     * @return number
     */
    public String getNumber() {

        return number;
    }

    /**
     * 
     * @param number to set number
     */
    public void setNumber(
            String number) {

        this.number = number;
    }

    /**
     * 
     * @return extension
     */
    public String getExtension() {

        return extension;
    }

    /**
     * 
     * @param extension to set extension
     */
    public void setExtension(
            String extension) {

        this.extension = extension;
    }

    @Override
    public String toString() {

        return "PhoneNumber [number=" + number + ", extension=" + extension + "]";
    }

}
