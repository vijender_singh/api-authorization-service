package com.fedex.rscs.dto;

/**
 * Define the EmailDetail specification DTO
 * 
 * @author 3932968
 *
 */
public class EmailDetail {

    private String emailAddress;

    public EmailDetail() {}

    /**
     * 
     * @param emailAddress
     */
    public EmailDetail(String emailAddress) {

        this.emailAddress = emailAddress;
    }

    /**
     * 
     * @return emailAddress
     */
    public String getEmailAddress() {

        return emailAddress;
    }

    /**
     * 
     * @param emailAddress to set emailAddress
     */
    public void setEmailAddress(
            String emailAddress) {

        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {

        return "EmailDetail [emailAddress=" + emailAddress + "]";
    }

}
