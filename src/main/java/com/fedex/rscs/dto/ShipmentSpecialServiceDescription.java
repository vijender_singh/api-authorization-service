package com.fedex.rscs.dto;

import java.util.List;

import javax.validation.Valid;

/**
 * Define the Shipment Special Service Description DTO.
 * 
 * @author 3932968
 *
 */
public class ShipmentSpecialServiceDescription {

    private List<SpecialServiceDescription> specialServices;
    private HoldAtLocationDetail holdAtLocationDetail;
    @Valid
    private HomeDeliveryDetail homeDeliveryDetail;

    public ShipmentSpecialServiceDescription() {}

    /**
     * @param specialServices
     * @param holdAtLocationDetail
     * @param homeDeliveryDetail
     */
    public ShipmentSpecialServiceDescription(List<SpecialServiceDescription> specialServices,
            HoldAtLocationDetail holdAtLocationDetail, HomeDeliveryDetail homeDeliveryDetail) {

        this.specialServices = specialServices;
        this.holdAtLocationDetail = holdAtLocationDetail;
        this.homeDeliveryDetail = homeDeliveryDetail;
    }

    /**
     * 
     * @return specialServices
     */
    public List<SpecialServiceDescription> getSpecialServices() {

        return specialServices;
    }

    /**
     * 
     * @param specialServices to set specialServices
     */
    public void setSpecialServices(
            List<SpecialServiceDescription> specialServices) {

        this.specialServices = specialServices;
    }

    /**
     * 
     * @return holdAtLocationDetail
     */
    public HoldAtLocationDetail getHoldAtLocationDetail() {

        return holdAtLocationDetail;
    }

    /**
     * 
     * @param holdAtLocationDetail to set holdAtLocationDetail
     */
    public void setHoldAtLocationDetail(
            HoldAtLocationDetail holdAtLocationDetail) {

        this.holdAtLocationDetail = holdAtLocationDetail;
    }

    /**
     * @return homeDeliveryDetail
     */
    public HomeDeliveryDetail getHomeDeliveryDetail() {

        return homeDeliveryDetail;
    }

    /**
     * Set home delivery details
     * 
     * @param homeDeliveryDetail
     */
    public void setHomeDeliveryDetail(
            HomeDeliveryDetail homeDeliveryDetail) {

        this.homeDeliveryDetail = homeDeliveryDetail;
    }

    @Override
    public String toString() {

        return "ShipmentSpecialServiceDescription [specialServices=" + specialServices + ", holdAtLocationDetail="
                + holdAtLocationDetail + ", homeDeliveryDetail=" + homeDeliveryDetail + "]";
    }
}
