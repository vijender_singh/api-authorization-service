package com.fedex.rscs.dto;

/**
 * Enum to define the delivery type for home delivery options.
 * 
 * @author Vijender.Singh
 *
 */
public enum HomeDeliveryType {
    APPOINTMENT, DATE_CERTAIN, EVENING
}
