package com.fedex.rscs.dto;

import javax.validation.constraints.Size;

/**
 * Define the Company Specification DTO
 * 
 * @author 3932968
 *
 */
public class Company {

    @Size(max = 80)
    private String name;

    public Company() {}

    /**
     * 
     * @param name
     */
    public Company(String name) {

        this.name = name;
    }

    /**
     * 
     * @return name
     */
    public String getName() {

        return name;
    }

    /**
     * 
     * @param name to set name
     */
    public void setName(
            String name) {

        this.name = name;
    }

    @Override
    public String toString() {

        return "Company [name=" + name + "]";
    }

}
