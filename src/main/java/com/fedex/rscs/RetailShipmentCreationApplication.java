package com.fedex.rscs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.fedex.common.cxs.CXSCommonLib;

@SpringBootApplication
@ComponentScan(basePackageClasses = {RetailShipmentCreationApplication.class, CXSCommonLib.class})
public class RetailShipmentCreationApplication {

    @java.lang.SuppressWarnings("squid:S2095")
    public static void main(final String[]args) {
        SpringApplication.run(RetailShipmentCreationApplication.class, args);
    }

}
