package com.fedex.rscs.client.rtls.dto;

import com.fedex.common.cxs.dto.CXSOutput;

/**
 * Represents the retail location service response
 * 
 * @author 5146946
 *
 */
public class LocationDataResource extends CXSOutput {

    private LocationData retailLocation;

    public LocationDataResource() {}

    /**
     * @param retailLocation
     */
    public LocationDataResource(LocationData retailLocation) {

        this.retailLocation = retailLocation;
    }

    /**
     * @return the retailLocation
     */
    public LocationData getRetailLocation() {

        return retailLocation;
    }

    /**
     * @param retailLocation the retailLocation to set
     */
    public void setRetailLocation(
            LocationData retailLocation) {

        this.retailLocation = retailLocation;
    }

    @Override
    public String toString() {

        return "ContextResource [retailLocation=" + retailLocation + "]";
    }


}
