package com.fedex.rscs.client.rtls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import com.fedex.common.cxs.dto.CXSEnvelope;
import com.fedex.rscs.client.rtls.dto.LocationDataResource;
import com.fedex.rscs.client.rtls.mapper.RetailLocationServiceClientMapper;
import com.fedex.rscs.config.AppConfig;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.client.ClientTerminalException;
import com.fedex.rscs.model.LocationContextDetail;

/**
 * This class provides the implementation for the Retail Location client in order to communicate
 * with Location Service and get the location context details.
 * 
 * @author shashank.jain
 *
 */
@Service
@Profile("!mock")
public class RetailLocationServiceClientImpl implements RetailLocationServiceClient {

    private static final Logger logger = LoggerFactory.getLogger(RetailLocationServiceClientImpl.class);
    private static final String RETAIL_LOCATION_URI = "/location/fedexoffice/v2/retaillocations/";

    private final AppConfig appConfig;
    private final OAuth2RestTemplate restTemplate;

    @Autowired
    public RetailLocationServiceClientImpl(final OAuth2RestTemplate restTemplate, final AppConfig appConfig) {

        this.restTemplate = restTemplate;
        this.appConfig = appConfig;
    }

    /**
     * This method is calling the retail location API to get the store specific information which
     * will be used in other FAST services.
     */
    @Override
    @HystrixCommand(groupKey = "RetailLocationServiceClientImpl", commandKey = "getLocationContextDetails")
    public LocationContextDetail getLocationContextDetails(
            final String locId) {

        logger.info("Execution started for service endpoint: {} ", RETAIL_LOCATION_URI);
        String retailLocationServiceEndpoint = appConfig.getRtlServiceUri() + RETAIL_LOCATION_URI;

        logger.debug("Request for retail location service with locationId: {} ", locId);
        ResponseEntity<CXSEnvelope<LocationDataResource>> response;

        response = restTemplate.exchange(retailLocationServiceEndpoint + locId, HttpMethod.GET, null,
                new ParameterizedTypeReference<CXSEnvelope<LocationDataResource>>() {});

        if (response.getStatusCode() == HttpStatus.OK && !ObjectUtils.isEmpty(response.getBody()
                .getOutput())
                && !ObjectUtils.isEmpty(response.getBody()
                        .getOutput()
                        .getRetailLocation())) {

            logger.debug("Response received from retail location service: {} ", response.getBody()
                    .getOutput());

            return RetailLocationServiceClientMapper.INSTANCE.toModel(response.getBody()
                    .getOutput()
                    .getRetailLocation());
        } else {

            logger.error("response : {} not as expected from retail location service with locId : {} ", response,
                    locId);
            throw new ClientTerminalException(ServiceErrorCode.INVALID_LOCATION_ID);
        }
    }

}
