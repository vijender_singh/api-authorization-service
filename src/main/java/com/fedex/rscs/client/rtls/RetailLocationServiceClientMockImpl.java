package com.fedex.rscs.client.rtls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.fedex.rscs.client.pes.PackageEventServiceClientMockImpl;
import com.fedex.rscs.model.LocationContextDetail;

/**
 * Mock implementation for Location context detail.
 * 
 * @author shashank.jain
 *
 */
@Service
@Profile("mock")
public class RetailLocationServiceClientMockImpl implements RetailLocationServiceClient {
    
    private static final Logger logger = LoggerFactory.getLogger(RetailLocationServiceClientMockImpl.class);

    /**
     * mock data for location context
     */
    @Override
    public LocationContextDetail getLocationContextDetails(
            final String locId) {

        logger.info("Mock: Execution of retail location service started...");
        return new LocationContextDetail("075300000", "4188704", "4188705", "4188706", "FDXE", "FDXG");
    }

}
