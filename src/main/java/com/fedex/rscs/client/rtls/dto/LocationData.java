package com.fedex.rscs.client.rtls.dto;

/**
 * Location context details model specification.
 */
public class LocationData {

    private String cityCenterAccountNumber;
    private String groundAccountNumber;
    private String btcMeterNumber;
    private String cfMeterNumber;
    private String opCo;
    private String opCoLocationId;


    public LocationData() {

    }
    
    /**
     * @param cityCenterAccountNumber
     * @param groundAccountNumber
     * @param btcMeterNumber
     * @param cfMeterNumber
     * @param opCo
     * @param opCoLocationId
     */
    public LocationData(String cityCenterAccountNumber, String groundAccountNumber, String btcMeterNumber,
            String cfMeterNumber, String opCo, String opCoLocationId) {

        this.cityCenterAccountNumber = cityCenterAccountNumber;
        this.groundAccountNumber = groundAccountNumber;
        this.btcMeterNumber = btcMeterNumber;
        this.cfMeterNumber = cfMeterNumber;
        this.opCo = opCo;
        this.opCoLocationId = opCoLocationId;
    }

    /**
     * @return the cityCenterAccountNumber
     */
    public String getCityCenterAccountNumber() {

        return cityCenterAccountNumber;
    }

    /**
     * @param cityCenterAccountNumber the cityCenterAccountNumber to set
     */
    public void setCityCenterAccountNumber(
            String cityCenterAccountNumber) {

        this.cityCenterAccountNumber = cityCenterAccountNumber;
    }

    /**
     * @return the groundAccountNumber
     */
    public String getGroundAccountNumber() {

        return groundAccountNumber;
    }

    /**
     * @param groundAccountNumber the groundAccountNumber to set
     */
    public void setGroundAccountNumber(
            String groundAccountNumber) {

        this.groundAccountNumber = groundAccountNumber;
    }

    /**
     * @return the btcMeterNumber
     */
    public String getBtcMeterNumber() {

        return btcMeterNumber;
    }

    /**
     * @param btcMeterNumber the btcMeterNumber to set
     */
    public void setBtcMeterNumber(
            String btcMeterNumber) {

        this.btcMeterNumber = btcMeterNumber;
    }

    /**
     * @return the cfMeterNumber
     */
    public String getCfMeterNumber() {

        return cfMeterNumber;
    }

    /**
     * @param cfMeterNumber the cfMeterNumber to set
     */
    public void setCfMeterNumber(
            String cfMeterNumber) {

        this.cfMeterNumber = cfMeterNumber;
    }
    
    /**
     * @return the opCo
     */
    public String getOpCo() {

        return opCo;
    }

    /**
     * @param opCo the opCo to set
     */
    public void setOpCo(
            String opCo) {

        this.opCo = opCo;
    }

    /**
     * @return the opCoLocationId
     */
    public String getOpCoLocationId() {

        return opCoLocationId;
    }

    /**
     * @param opCoLocationId the opCoLocationId to set
     */
    public void setOpCoLocationId(
            String opCoLocationId) {

        this.opCoLocationId = opCoLocationId;
    }

    @Override
    public String toString() {

        return "LocationData [cityCenterAccountNumber=" + cityCenterAccountNumber + ", groundAccountNumber="
                + groundAccountNumber + ", btcMeterNumber=" + btcMeterNumber + ", cfMeterNumber=" + cfMeterNumber
                + ", opCo=" + opCo + ", opCoLocationId=" + opCoLocationId + "]";
    }
    
}
