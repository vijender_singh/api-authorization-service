package com.fedex.rscs.client.rtls.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.fedex.rscs.client.rtls.dto.LocationData;
import com.fedex.rscs.model.LocationContextDetail;


/**
 * Mapper to handle the conversion of retail location service request and response.
 * 
 * @author shashank.jain
 *
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RetailLocationServiceClientMapper {

    RetailLocationServiceClientMapper INSTANCE = Mappers.getMapper(RetailLocationServiceClientMapper.class);


    /**
     * Mapper to convert {@link LocationData} DTO to {@link LocationContextDetail} model.43
     * 
     * @param locationData
     * @return
     */
    LocationContextDetail toModel(
            LocationData locationData);


}
