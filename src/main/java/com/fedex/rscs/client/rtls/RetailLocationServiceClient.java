package com.fedex.rscs.client.rtls;

import com.fedex.rscs.model.LocationContextDetail;

/**
 * This interface provides the necessary methods to communicate with retail location service.
 * 
 * @author shashank.jain
 *
 */
public interface RetailLocationServiceClient {

    /**
     * This method provide the necessary context information that will use in FAST services.
     * 
     * @param locationId
     * @return
     */
    LocationContextDetail getLocationContextDetails(
            final String locationId);

}
