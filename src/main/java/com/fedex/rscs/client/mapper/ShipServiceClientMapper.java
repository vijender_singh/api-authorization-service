
package com.fedex.rscs.client.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.fedex.nxgen.ship.v17.ientities.BinaryBarcode;
import com.fedex.nxgen.ship.v17.ientities.ClientDetail;
import com.fedex.nxgen.ship.v17.ientities.OfferingIdentifierDetail;
import com.fedex.nxgen.ship.v17.ientities.OperationalInstruction;
import com.fedex.nxgen.ship.v17.ientities.ProductName;
import com.fedex.nxgen.ship.v17.ientities.ShipmentOperationalDetail;
import com.fedex.nxgen.ship.v17.ientities.StringBarcode;
import com.fedex.nxgen.ship.v17.ientities.TrackingId;
import com.fedex.nxgen.ship.v17.ientities.VersionId;
import com.fedex.rscs.model.VersionDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.cshp.BinaryBarcodeDetail;
import com.fedex.rscs.model.cshp.OfferingIdentifierData;
import com.fedex.rscs.model.cshp.OperationalInstructionDetail;
import com.fedex.rscs.model.cshp.ProductNameDetail;
import com.fedex.rscs.model.cshp.ShipmentOperationData;
import com.fedex.rscs.model.cshp.StringBarcodeDetail;
import com.fedex.rscs.model.cshp.TrackingIdDetail;



/**
 * Map struct mapper for model conversion for ship service calls
 * 
 * @author Samicheen Khariwal
 *
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ShipServiceClientMapper {
    
    ShipServiceClientMapper INSTANCE = Mappers.getMapper(ShipServiceClientMapper.class);
    
    /**
     * Mapper to convert WorkstationDetail to ClientDetail object
     * 
     * @param workstationDetail
     * @return
     */
    @Mappings({
        @Mapping(source = "workstationDetail.appVersionId", target = "softwareRelease")
    })
    ClientDetail toModel(
            WorkstationDetail workstationDetail);

    /**
     * Mapper to convert VersionDetail to VersionId object
     * 
     * @param versionDetail
     * @return
     */
    VersionId toModel(
            VersionDetail versionDetail);

    /**
     * Mapper to convert TrackingIdDetail to TrackingId object
     * 
     * @param trackDetail
     * @return
     */
    TrackingId toModel(
            TrackingIdDetail trackDetail);

    /**
     * Mapper to convert ShipmentOperationalDetail to ShipmentOperationData object
     * 
     * @param shipmentOperationalDetail
     * @return
     */
    ShipmentOperationData toModel(
            ShipmentOperationalDetail shipmentOperationalDetail);

    /**
     * Mapper to convert StringBarcode to StringBarcodeDetail
     * 
     * @param stringBarcodes
     * @return
     */
    List<StringBarcodeDetail> toModel(
            StringBarcode[] stringBarcodes);

    /**
     * Mapper to convert BinaryBarcode to BinaryBarcodeDetail
     * 
     * @param binaryBarcodes
     * @return
     */
    List<BinaryBarcodeDetail> toModel(
            BinaryBarcode[] binaryBarcodes);

    /**
     * Mapper to convert OperationalInstruction to OperationalInstructionDetail
     * 
     * @param operationalInstructions
     * @return
     */
    List<OperationalInstructionDetail> toModel(
            OperationalInstruction[] operationalInstructions);

    /**
     * Mapper to convert OfferingIdentifierDetail to OfferingIdentifierData
     * 
     * @param identifier
     * @return
     */
    OfferingIdentifierData toModel(
            OfferingIdentifierDetail identifier);

    /**
     * Mapper to convert ProductName to ProductNameDetail
     * 
     * @param names
     * @return
     */
    List<ProductNameDetail> toModel(
            ProductName[] names);

    /**
     * Mapper to convert TrackingId to TrackingIdDetail
     * 
     * @param masterTrackingId
     * @return
     */
    TrackingIdDetail toModel(
            TrackingId masterTrackingId);

    /**
     * Mapper to convert TrackingId[] to List<TrackingIdDetail>
     * 
     * @param trackingIds
     * @return
     */
    List<TrackingIdDetail> toModel(
            TrackingId[] trackingIds);
    
}
