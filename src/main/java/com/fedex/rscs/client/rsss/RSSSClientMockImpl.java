package com.fedex.rscs.client.rsss;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fedex.rscs.client.rsss.dto.InterlineAccountResource;
import com.fedex.rscs.client.rsss.dto.InterlineAccountResponse;
import com.fedex.rscs.client.rsss.dto.ShippingEnablementDetail;
import com.fedex.rscs.dto.CarrierType;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.client.ClientTerminalException;

/**
 * This class provides the mock implementation of RSSSClient interface.
 * 
 * @author 3932968
 *
 */
@Service
@Profile("mock")
public class RSSSClientMockImpl implements RSSSClient {

    private static final Logger logger = LoggerFactory.getLogger(RSSSClientMockImpl.class);
    private final ObjectMapper objectMapper;

    @Autowired
    public RSSSClientMockImpl(final ObjectMapper objectMapper) {

        this.objectMapper = objectMapper;
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * Method to implement validate interline account data mock call.
     * 
     * @param interlineId
     * @return
     */
    @Override
    public String validateInterlineAccounts(
            String interlineId) {

        logger.info("Execution for mock RSSS InterlineAccounts call is started..... ");
        if ("100127".equalsIgnoreCase(interlineId)) {
            InterlineAccountResponse interlineAccountResponse = new InterlineAccountResponse();

            InterlineAccountResource interlineAccountResource = new InterlineAccountResource();
            interlineAccountResource.setAccountNumber("164462765");
            interlineAccountResource.setInterlineCode("G4");
            interlineAccountResource.setInterlineId("100127");
            interlineAccountResource.setInterlineNumber("008");
            interlineAccountResource.setInterlineName("ALLEGIANT AIRLINES");
            interlineAccountResource.setShippingEnablementDetail(buildShippingEnablementDetail());
            interlineAccountResponse.setInterlineAccount(interlineAccountResource);

            return interlineAccountResponse.getInterlineAccount()
                    .getAccountNumber();

        } else if ("1234".equalsIgnoreCase(interlineId)) {

            logger.error("Mock: Throwing Client Terminal Exception due to interlineId = 1234");
            throw new ClientTerminalException(ServiceErrorCode.INVALID_INTERLINE_ACCOUNT);
        } else {

            logger.error("Mock: Throwing Client Terminal Exception due to interlineId != 100127");
            throw new ClientTerminalException(ServiceErrorCode.INTERLINE_ACCOUNT_SERVICE_UNAVAILABLE);
        }

    }

    /**
     * Method to build the {@link ShippingEnablementDetail} object instance with mock data.
     * 
     * @return
     */
    private ShippingEnablementDetail buildShippingEnablementDetail() {

        ShippingEnablementDetail shippingEnablementDetail = new ShippingEnablementDetail();
        List<CarrierType> carrierTypeList = new ArrayList<>();
        carrierTypeList.add(CarrierType.EXPRESS);

        shippingEnablementDetail.setAllowedOneRate(true);
        shippingEnablementDetail.setAllowedCarriers(carrierTypeList);

        return shippingEnablementDetail;
    }
}
