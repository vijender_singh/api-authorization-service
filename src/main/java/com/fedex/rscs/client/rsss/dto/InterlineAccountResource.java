package com.fedex.rscs.client.rsss.dto;

/**
 * Define the Dto for interline account info resource.
 * 
 * @author 3932968
 *
 */
public class InterlineAccountResource {

    private String interlineId;
    private String interlineCode;
    private String interlineNumber;
    private String interlineName;
    private String accountNumber;
    private ShippingEnablementDetail shippingEnablementDetail;

    public InterlineAccountResource() {

    }

    /**
     * @return the interlineCode
     */
    public String getInterlineCode() {

        return interlineCode;
    }

    /**
     * @param interlineCode the interlineCode to set
     */
    public void setInterlineCode(
            String interlineCode) {

        this.interlineCode = interlineCode;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {

        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(
            String accountNumber) {

        this.accountNumber = accountNumber;
    }

    /**
     * @return the shippingEnablementDetail
     */
    public ShippingEnablementDetail getShippingEnablementDetail() {

        return shippingEnablementDetail;
    }

    /**
     * @param shippingEnablementDetail the shippingEnablementDetail to set
     */
    public void setShippingEnablementDetail(
            ShippingEnablementDetail shippingEnablementDetail) {

        this.shippingEnablementDetail = shippingEnablementDetail;
    }

    /**
     * @return the interlineId
     */
    public String getInterlineId() {

        return interlineId;
    }

    /**
     * @param interlineId the interlineId to set
     */
    public void setInterlineId(
            String interlineId) {

        this.interlineId = interlineId;
    }

    /**
     * @return the interlineNumber
     */
    public String getInterlineNumber() {

        return interlineNumber;
    }

    /**
     * @param interlineNumber the interlineNumber to set
     */
    public void setInterlineNumber(
            String interlineNumber) {

        this.interlineNumber = interlineNumber;
    }

    /**
     * @return the interlineName
     */
    public String getInterlineName() {

        return interlineName;
    }

    /**
     * @param interlineName the interlineName to set
     */
    public void setInterlineName(
            String interlineName) {

        this.interlineName = interlineName;
    }

    @Override
    public String toString() {

        return "InterlineAccountResource [interlineId=" + interlineId + ", interlineCode=" + interlineCode
                + ", interlineNumber=" + interlineNumber + ", interlineName=" + interlineName + ", accountNumber="
                + accountNumber + ", shippingEnablementDetail=" + shippingEnablementDetail + "]";
    }

}
