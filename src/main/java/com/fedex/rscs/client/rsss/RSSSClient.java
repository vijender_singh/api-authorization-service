package com.fedex.rscs.client.rsss;

/**
 * This interface provides the necessary methods to communicate with Retail Shipping Support
 * service.
 * 
 * @author 3932968
 *
 */
public interface RSSSClient {

    /**
     * Method to validate interline account data is valid or not.
     * 
     * @param interlineCode
     * @return
     */
    String validateInterlineAccounts(
            String interlineId);
}
