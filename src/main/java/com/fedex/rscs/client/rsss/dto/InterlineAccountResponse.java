package com.fedex.rscs.client.rsss.dto;

import com.fedex.common.cxs.dto.CXSOutput;

/**
 * Define the Interline Account Information Resource dto.
 * 
 * @author 3932968
 *
 */
public class InterlineAccountResponse extends CXSOutput {

    private InterlineAccountResource interlineAccount;

    public InterlineAccountResponse() {}

    /**
     * @return the interlineAccount
     */
    public InterlineAccountResource getInterlineAccount() {

        return interlineAccount;
    }

    /**
     * @param interlineAccount the interlineAccount to set
     */
    public void setInterlineAccount(
            InterlineAccountResource interlineAccount) {

        this.interlineAccount = interlineAccount;
    }

    @Override
    public String toString() {

        return "InterlineAccountResponse [interlineAccount=" + interlineAccount + "]";
    }

}
