package com.fedex.rscs.client.rsss.dto;

import java.util.List;

import com.fedex.rscs.dto.CarrierType;

/**
 * Define the ShippingEnablementDetail specification dto.
 * 
 * @author 3932968
 *
 */
public class ShippingEnablementDetail {

    private boolean allowedOneRate;
    private List<CarrierType> allowedCarriers;

    public ShippingEnablementDetail() {}

    /**
     * @return allowedOneRate
     */
    public boolean isAllowedOneRate() {

        return allowedOneRate;
    }

    /**
     * @param allowedOneRate to set allowedOneRate
     */
    public void setAllowedOneRate(
            boolean allowedOneRate) {

        this.allowedOneRate = allowedOneRate;
    }

    /**
     * @return the allowedCarriers
     */
    public List<CarrierType> getAllowedCarriers() {

        return allowedCarriers;
    }

    /**
     * @param allowedCarriers the allowedCarriers to set
     */
    public void setAllowedCarriers(
            List<CarrierType> allowedCarriers) {

        this.allowedCarriers = allowedCarriers;
    }

    @Override
    public String toString() {

        return "ShippingEnablementDetail [allowedOneRate=" + allowedOneRate + ", allowedCarriers=" + allowedCarriers
                + "]";
    }

}
