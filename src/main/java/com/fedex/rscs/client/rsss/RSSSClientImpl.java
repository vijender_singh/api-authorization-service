package com.fedex.rscs.client.rsss;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.util.UriComponentsBuilder;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import com.fedex.common.cxs.dto.CXSEnvelope;
import com.fedex.rscs.client.rsss.dto.InterlineAccountResponse;
import com.fedex.rscs.client.util.RetailShipmentCreationServiceClientUtil;
import com.fedex.rscs.config.AppConfig;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.client.ClientTerminalException;

/**
 * This class provides the implementation of RSSSClient interface.
 * 
 * @author 3932968
 *
 */
@Service
@Profile("!mock")
public class RSSSClientImpl implements RSSSClient {

    private static final Logger logger = LoggerFactory.getLogger(RSSSClientImpl.class);
    private static final String INTERLINE_ACCOUNT_SERVICE_URI = "/retailshipping/fedexoffice/v1/interlineaccounts";
    
    private final RetailShipmentCreationServiceClientUtil clientUtil;    
    private final AppConfig appConfig;
    private final OAuth2RestTemplate restTemplate;

    @Autowired
    public RSSSClientImpl(final OAuth2RestTemplate restTemplate, final AppConfig appConfig,
            final RetailShipmentCreationServiceClientUtil clientUtil) {

        this.appConfig = appConfig;
        this.restTemplate = restTemplate;
        this.clientUtil = clientUtil;
    }

    /**
     * Method to implement validate interline account data call.
     * 
     * @param interlineId
     * @return
     */
    @Override
    @HystrixCommand(groupKey = "RSSSClientImpl", commandKey = "validateInterlineAccounts")
    public String validateInterlineAccounts(
            String interlineId) {

        logger.info("Execution started for service endpoint: {} ", INTERLINE_ACCOUNT_SERVICE_URI);
        String supportServiceEndpoint = appConfig.getSupportServiceUri() + INTERLINE_ACCOUNT_SERVICE_URI;

        try {

            logger.debug("Request for RSSS interlineAccounts with interlineId: {} ", interlineId);
            UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(supportServiceEndpoint)
                    .path("/" + interlineId);
            ResponseEntity<CXSEnvelope<InterlineAccountResponse>> response =
                    restTemplate.exchange(uriComponentsBuilder.toUriString(), HttpMethod.GET, null,
                            new ParameterizedTypeReference<CXSEnvelope<InterlineAccountResponse>>() {});

            if (response.getStatusCode() == HttpStatus.OK && !ObjectUtils.isEmpty(response.getBody()
                    .getOutput())) {

                logger.debug("Response received from RSSS interlineAccounts: {} ", response.getBody()
                        .getOutput());

                return response.getBody()
                        .getOutput()
                        .getInterlineAccount()
                        .getAccountNumber();

            } else {

                logger.error(" response : {} not as expected with provided Interline Id : {} and endpoint : {}",
                        response, interlineId, supportServiceEndpoint);
                throw new ClientTerminalException(ServiceErrorCode.INTERNAL_SERVER_ERROR);
            }

        } catch (HttpStatusCodeException httpException) {

            logger.error("failed to process the request of RSSS interlineAccounts due to ", httpException);
            throw new ClientTerminalException(clientUtil.processErrorResponse(httpException));
        } catch (RuntimeException runTimeException) {

            logger.error("failed to process the request of RSSS interlineAccounts due to ", runTimeException);
            throw new ClientTerminalException(ServiceErrorCode.INTERLINE_ACCOUNT_SERVICE_UNAVAILABLE);
        }
    }
}
