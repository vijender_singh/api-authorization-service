package com.fedex.rscs.client.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.HttpStatusCodeException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.common.cxs.dto.CXSEnvelope;
import com.fedex.common.cxs.dto.CXSError;
import com.fedex.rscs.exception.ErrorCategory;
import com.fedex.rscs.exception.ShipmentServiceErrorCode;

@Component
public class RetailShipmentCreationServiceClientUtil {

    private static final Logger logger = LoggerFactory.getLogger(RetailShipmentCreationServiceClientUtil.class);

    private ObjectMapper mapper;

    public RetailShipmentCreationServiceClientUtil() {

        mapper = new ObjectMapper();
    }

    /**
     * This method can internally be used by service client to handle the http error response.
     * 
     * @param httpError
     * @return
     */
    public ShipmentServiceErrorCode processErrorResponse(
            HttpStatusCodeException httpError) {

        logger.info("processing httpException error response....");

        ErrorCategory errorCategory = ErrorCategory.INTERNAL_SERVER_ERROR;
        ShipmentServiceErrorCode errorCode = null;
        // As these errors are received from the external service call, convert all client errors as
        // bad
        // request errors and server errors as internal server or service unavailable error, so that
        // the underline complex errors can be hidden from end client and all these errors can be
        // handled internally.
        if (httpError.getStatusCode()
                .is4xxClientError()) {
            errorCategory = ErrorCategory.BAD_REQUEST;
        } else if (httpError.getStatusCode()
                .is5xxServerError()) {

            errorCategory =
                    (httpError.getStatusCode() == HttpStatus.SERVICE_UNAVAILABLE) ? ErrorCategory.SERVICE_UNAVAILABLE
                            : ErrorCategory.INTERNAL_SERVER_ERROR;
        }

        try {
            // check if the error response contains CXS errors then use the first CXS error to get
            // the error details. Else use the one received as http exception error code and
            // message.
            CXSEnvelope errorResponse = mapper.readValue(httpError.getResponseBodyAsString(), CXSEnvelope.class);
            if (errorResponse != null && !ObjectUtils.isEmpty(errorResponse.getErrors())) {

                CXSError error = (CXSError) errorResponse.getErrors()
                        .get(0);
                errorCode = new ShipmentServiceErrorCode(error.getCode(), error.getMessage(), errorCategory);
            }
        } catch (Exception exp) {
            logger.error("unable to parse error response as CXS response due to ", exp);
        }
        if (errorCode == null) {
            errorCode = new ShipmentServiceErrorCode(errorCategory.name(), httpError.getMessage(), errorCategory);
        }

        return errorCode;
    }
}