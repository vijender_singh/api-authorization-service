package com.fedex.rscs.client.srrs.dto;

/**
 * Enum to hold Application Names
 * 
 * @author 3932968
 *
 */
public enum AppName {
    FASTLANE, FUSE, ON_DEMAND;
}
