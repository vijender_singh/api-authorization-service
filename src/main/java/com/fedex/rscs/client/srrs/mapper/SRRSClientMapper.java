package com.fedex.rscs.client.srrs.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.fedex.rscs.client.srrs.dto.Address;
import com.fedex.rscs.client.srrs.dto.Company;
import com.fedex.rscs.client.srrs.dto.PreauthorizedRateDetail;
import com.fedex.rscs.client.srrs.dto.ShippingRateOption;
import com.fedex.rscs.model.AddressDetail;
import com.fedex.rscs.model.cshp.PreauthorizedRateData;
import com.fedex.rscs.model.cshp.ShippingRateData;

/**
 * Map struct mapper for model conversion for Rating call.
 * 
 * @author 3900094
 *
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface SRRSClientMapper {

    SRRSClientMapper INSTANCE = Mappers.getMapper(SRRSClientMapper.class);

    /**
     * Mapper to convert ShippingRateOption to ShippingRateData
     * 
     * @param rateOption
     * @return
     */
    @Mappings({@Mapping(source = "rateOption.rateDetails.currency", target = "currency"),
            @Mapping(source = "rateOption.rateDetails.ratingType", target = "ratingType"),
            @Mapping(source = "rateOption.rateDetails.totalBaseCharge", target = "totalBaseCharges"),
            @Mapping(source = "rateOption.rateDetails.totalRebates", target = "totalRebates"),
            @Mapping(source = "rateOption.rateDetails.totalNetCharges", target = "totalNetCharges"),
            @Mapping(source = "rateOption.rateDetails.totalSurcharges", target = "totalSurcharges"),
            @Mapping(source = "rateOption.rateDetails.totalTaxes", target = "totalTaxes"),
            @Mapping(source = "rateOption.rateDetails.totalFreightDiscounts", target = "totalFreightDiscounts"),
            @Mapping(source = "rateOption.rateDetails.totalNetFreight", target = "totalNetFreight"),
            @Mapping(source = "rateOption.rateDetails.totalNetFedExCharge", target = "totalNetFedExCharge"),
            @Mapping(source = "rateOption.rateDetails.totalDutiesAndTaxes", target = "totalDutiesAndTaxes"),
            @Mapping(source = "rateOption.rateDetails.totalAncillaryFeesAndTaxes", target = "totalAncillaryFeesAndTaxes"),
            @Mapping(source = "rateOption.rateDetails.totalDutiesTaxesAndFees", target = "totalDutiesTaxesAndFees"),
            @Mapping(source = "rateOption.rateDetails.totalNetChargeWithDutiesAndTaxes", target = "totalNetChargeWithDutiesAndTaxes"),
            @Mapping(source = "rateOption.rateDetails.surcharges", target = "surcharges")})
    ShippingRateData toModel(
            ShippingRateOption rateOption);

    /**
     * Mapper to convert PreauthorizedRateDetail to PreauthorizedRateData
     * 
     * @param preauthorizedRateDetail
     * @return
     */
    PreauthorizedRateData toModel(
            PreauthorizedRateDetail preauthorizedRateDetail);

    /**
     * Mapper to convert AddressDetail to Address
     * 
     * @param address
     * @return
     */
    Address toDto(
            AddressDetail address);

    /**
     * Mapper to convert companyName to Company
     * 
     * @param companyName
     * @return
     */
    Company toCompanyDto(
            String companyName);

}
