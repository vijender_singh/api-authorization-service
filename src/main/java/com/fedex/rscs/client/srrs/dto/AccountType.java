package com.fedex.rscs.client.srrs.dto;

/**
 * Enum to hold Account Type
 * 
 * @author 3932968
 *
 */
public enum AccountType {
    FEDEX_EXPRESS, FEDEX_GROUND;
}
