package com.fedex.rscs.client.srrs.dto;

import java.util.List;

/**
 * Shipping Rate Control DTO
 * 
 * @author 3932956
 *
 */
public class ShippingRateControls {

    private List<RateOption> rateOptions;
    private List<CarrierCode> carrierCodes;

    public ShippingRateControls() {

    }

    /**
     * @param rateOptions
     * @param carrierCodes
     */
    public ShippingRateControls(List<RateOption> rateOptions, List<CarrierCode> carrierCodes) {

        this.rateOptions = rateOptions;
        this.carrierCodes = carrierCodes;
    }

    /**
     * @return the rateOptions
     */
    public List<RateOption> getRateOptions() {

        return rateOptions;
    }

    /**
     * @param rateOptions the rateOptions to set
     */
    public void setRateOptions(
            List<RateOption> rateOptions) {

        this.rateOptions = rateOptions;
    }

    /**
     * @return the carrierCodes
     */
    public List<CarrierCode> getCarrierCodes() {

        return carrierCodes;
    }

    /**
     * @param carrierCodes the carrierCodes to set
     */
    public void setCarrierCodes(
            List<CarrierCode> carrierCodes) {

        this.carrierCodes = carrierCodes;
    }

    @Override
    public String toString() {

        return "ShippingRateControls [rateOptions=" + rateOptions + ", carrierCodes=" + carrierCodes + "]";
    }

}
