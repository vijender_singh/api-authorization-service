package com.fedex.rscs.client.srrs;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fedex.rscs.client.srrs.dto.PreauthorizedRateResource;
import com.fedex.rscs.client.srrs.dto.ShipmentRateQuotesResource;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.client.ClientTerminalException;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.cshp.PreauthorizedRateData;
import com.fedex.rscs.model.cshp.ShippingRateData;

/**
 * Mock Implementation of SRRS Rate call.
 * 
 * @author 3900094
 *
 */
@Service
@Profile("mock")
public class SRRSClientMockImpl implements SRRSClient {

    private static final Logger logger = LoggerFactory.getLogger(SRRSClientMockImpl.class);

    private static final String PACKAGE_TYPE = "FEDEX_ENVELOPE";

    private final ObjectMapper objectMapper;
    private final SRRSResponseMapper responseMapper;

    @Autowired
    public SRRSClientMockImpl(final ObjectMapper objectMapper, final SRRSResponseMapper responseMapper) {

        this.objectMapper = objectMapper;
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.responseMapper = responseMapper;
    }

    /**
     * This method is mock implementation of SRRSClient method.
     * 
     * @param workStationDetail
     * @param requestedShipmentDetail
     */
    @Override
    public ShippingRateData getRateOptions(
            WorkstationDetail workStationDetail,
            RequestedShipmentDetail requestedShipmentDetail) {

        if (requestedShipmentDetail.getInterlineShippingData() != null) {
            requestedShipmentDetail.getShippingChargesPayment()
                    .setInterlineId(requestedShipmentDetail.getInterlineShippingData()
                            .getInterlineId());
        }

        logger.debug(
                "Mock: Prepare Request for SRRS.getRateOptions with requestedShipmentDetail {} and workstationDetail {}",
                requestedShipmentDetail, workStationDetail);

        ShippingRateData shippingRateData = null;
        if (!ObjectUtils.isEmpty(requestedShipmentDetail.getPackagingType())
                && requestedShipmentDetail.getPackagingType()
                        .equalsIgnoreCase(PACKAGE_TYPE)) {

            logger.error("Mock: Throwing Client Terminal Exception due to PackagingType = FEDEX_ENVELOPE ");
            throw new ClientTerminalException(ServiceErrorCode.RATING_SERVICE_UNAVAILABLE);

        }

        shippingRateData = responseMapper.transformRateResponse(createSuccessRateResponseForOneRate());

        logger.debug("Received response of mock rate call: {}", shippingRateData);
        return shippingRateData;
    }
    
    public PreauthorizedRateData getPreauthorizedRates(
            WorkstationDetail workStationDetail,
            RequestedShipmentDetail requestedShipmentDetail) {

        if (requestedShipmentDetail.getInterlineShippingData() != null) {
            requestedShipmentDetail.getShippingChargesPayment()
                    .setInterlineId(requestedShipmentDetail.getInterlineShippingData()
                            .getInterlineId());
        }

        logger.debug(
                "Mock: Prepare Request for SRRS.getPreauthorizedRates with requestedShipmentDetail {} and workstationDetail {}",
                requestedShipmentDetail, workStationDetail);

        PreauthorizedRateData preauthorizedRateData = null;
        if (!ObjectUtils.isEmpty(requestedShipmentDetail.getPackagingType())
                && requestedShipmentDetail.getPackagingType()
                        .equalsIgnoreCase(PACKAGE_TYPE)) {

            logger.error("Mock: Throwing Client Terminal Exception due to PackagingType = FEDEX_ENVELOPE ");
            throw new ClientTerminalException(ServiceErrorCode.RATING_SERVICE_UNAVAILABLE);

        }

        preauthorizedRateData = responseMapper.transformPreauthorizedRateResponse(createSuccessPreauthorizedRateResponse());

        logger.debug("Received response of mock preauthorized rate call: {}", preauthorizedRateData);
        return preauthorizedRateData;
    }

    /**
     * Method to provide one rate mock success response of ShipmentRateQuotesResource.
     * 
     * @return ShipmentRateQuotesResource
     */
    private ShipmentRateQuotesResource createSuccessRateResponseForOneRate() {

        String rateReplyJson =
                "{\"quoteDate\":\"2020-09-07\",\"rateOptions\":[{\"packagingType\":\"FEDEX_PAK\",\"signatureOption\":\"SERVICE_DEFAULT\",\"serviceDetails\":{\"serviceType\":\"PRIORITY_OVERNIGHT\",\"serviceName\":\"FedEx Priority Overnight\",\"description\":\"Priority Overnight\"},\"deliveryDayOfWeek\":\"TUE\",\"hal\":true,\"commitDetails\":{\"commitTimestamp\":\"2020-09-01T09:00:00\",\"dayOfWeek\":\"TUE\"},\"currency\":\"USD\",\"ratingType\":\"ONE_RATE\",\"totalBaseCharge\":38.45,\"totalRebates\":0,\"totalNetCharges\":38.45,\"totalSurcharges\":0,\"totalTaxes\":0,\"totalFreightDiscounts\":0,\"totalNetFreight\":38.45,\"totalNetFedExCharge\":38.45,\"totalDutiesAndTaxes\":0,\"totalAncillaryFeesAndTaxes\":0,\"totalDutiesTaxesAndFees\":0,\"totalNetChargeWithDutiesAndTaxes\":38.45,\"surcharges\":[{\"surchargeType\":\"INSURED_VALUE\",\"description\":\"Declared value\",\"amount\":0}]},{\"packagingType\":\"FEDEX_PAK\",\"signatureOption\":\"SERVICE_DEFAULT\",\"serviceDetails\":{\"serviceType\":\"PRIORITY_OVERNIGHT\",\"serviceName\":\"FedEx Priority Overnight\",\"description\":\"Priority Overnight\"},\"deliveryDayOfWeek\":\"TUE\",\"hal\":true,\"commitDetails\":{\"commitTimestamp\":\"2020-09-01T09:00:00\",\"dayOfWeek\":\"TUE\"},\"currency\":\"USD\",\"ratingType\":\"STANDARD_RATE\",\"totalBaseCharge\":30.31,\"totalRebates\":0,\"totalNetCharges\":31.83,\"totalSurcharges\":1.52,\"totalTaxes\":0,\"totalFreightDiscounts\":0,\"totalNetFreight\":30.31,\"totalNetFedExCharge\":31.83,\"totalDutiesAndTaxes\":0,\"totalAncillaryFeesAndTaxes\":0,\"totalDutiesTaxesAndFees\":0,\"totalNetChargeWithDutiesAndTaxes\":31.83,\"surcharges\":[{\"surchargeType\":\"INSURED_VALUE\",\"description\":\"Declared value\",\"amount\":0},{\"surchargeType\":\"FUEL\",\"description\":\"Fuel\",\"amount\":1.52}]}]}";

        try {
            return objectMapper.readValue(rateReplyJson, ShipmentRateQuotesResource.class);
        } catch (IOException exp) {
            logger.error("Exception occured while mapping json {}", exp.getMessage());
        }
        return null;
    }
    
    /**
     * Method to provide preauthorized rate mock success response.
     * 
     * @return PreauthorizedRateResource
     */
    private PreauthorizedRateResource createSuccessPreauthorizedRateResponse() {

        String rateReplyJson =
                "{\"preauthorizedRateDetail\":{\"serviceType\":\"FIRST_OVERNIGHT\",\"packagingType\":\"YOUR_PACKAGING\",\"signatureOption\":\"SERVICE_DEFAULT\",\"deliveryDayOfWeek\":\"TUE\",\"deliveryStation\":\"DALA \",\"deliveryTimestamp\":\"2020-08-25T08:00:00\",\"destinationAirportId\":\"DFW\",\"ineligibleForMoneyBackGuarantee\":false,\"originServiceArea\":\"A1\",\"destinationServiceArea\":\"A1\",\"actualRateType\":\"PAYOR_ACCOUNT_PACKAGE\",\"serviceDetails\":{\"serviceId\":\"EP1000000006\",\"code\":\"06\",\"serviceType\":\"FIRST_OVERNIGHT\",\"operatingOrgCodes\":[\"FXE\"],\"serviceCategory\":\"parcel\",\"astraDescription\":\"1ST OVR\"},\"commitDetails\":{\"commodityName\":\"\",\"serviceType\":\"FIRST_OVERNIGHT\",\"serviceDescription\":{\"serviceId\":\"EP1000000006\",\"code\":\"06\",\"serviceType\":\"FIRST_OVERNIGHT\",\"operatingOrgCodes\":[\"FXE\"],\"serviceCategory\":\"parcel\",\"astraDescription\":\"1ST OVR\"},\"derivedOriginDetail\":{\"countryCode\":\"US\",\"stateOrProvinceCode\":\"TX\",\"postalCode\":\"75261\",\"serviceArea\":\"A1\",\"locationId\":\"DALA \",\"locationNumber\":0,\"airportId\":\" \"},\"derivedDestinationDetail\":{\"countryCode\":\"US\",\"stateOrProvinceCode\":\"TX\",\"postalCode\":\"75261\",\"serviceArea\":\"A1\",\"locationId\":\"DALA \",\"locationNumber\":0,\"airportId\":\"DFW\"},\"destinationServiceArea\":\"A1\",\"brokerToDestinationDays\":0,\"documentContent\":\"NON_DOCUMENTS\"},\"shipmentRateDetails\":{\"rateType\":\"PAYOR_ACCOUNT_PACKAGE\",\"rateScale\":\"6\",\"rateZone\":\"02\",\"pricingCode\":\"PACKAGE\",\"ratedWeightMethod\":\"ACTUAL\",\"minimumChargeType\":\"\",\"specialRatingApplied\":[],\"dimDivisor\":0,\"fuelSurchargePercent\":5,\"totalBillingWeight\":{\"units\":\"LB\",\"value\":2},\"totalBaseCharge\":{\"currency\":\"USD\",\"amount\":60.71},\"totalFreightDiscounts\":{\"currency\":\"USD\",\"amount\":0},\"totalNetFreight\":{\"currency\":\"USD\",\"amount\":60.71},\"totalSurcharges\":{\"currency\":\"USD\",\"amount\":3.04},\"totalNetFedExCharge\":{\"currency\":\"USD\",\"amount\":63.75},\"totalTaxes\":{\"currency\":\"USD\",\"amount\":0},\"totalNetCharge\":{\"currency\":\"USD\",\"amount\":63.75},\"totalRebates\":{\"currency\":\"USD\",\"amount\":0},\"totalDutiesAndTaxes\":{\"currency\":\"USD\",\"amount\":0},\"totalAncillaryFeesAndTaxes\":{\"currency\":\"USD\",\"amount\":0},\"totalDutiesTaxesAndFees\":{\"currency\":\"USD\",\"amount\":0},\"totalNetChargeWithDutiesAndTaxes\":{\"currency\":\"USD\",\"amount\":63.75},\"freightDiscounts\":[],\"rebates\":[],\"surcharges\":[{\"surchargeType\":\"INSURED_VALUE\",\"description\":\"Insured value\",\"amount\":0},{\"surchargeType\":\"FUEL\",\"description\":\"Fuel\",\"amount\":3.04}],\"taxes\":[],\"ancillaryFeesAndTaxes\":[]},\"ratedPackages\":[{\"groupNumber\":0,\"rateType\":\"PAYOR_ACCOUNT_PACKAGE\",\"ratedWeightMethod\":\"ACTUAL\",\"minimumChargeType\":\"\",\"billingWeight\":{\"units\":\"LB\",\"value\":2},\"baseCharge\":{\"currency\":\"USD\",\"amount\":60.71},\"totalFreightDiscounts\":{\"currency\":\"USD\",\"amount\":0},\"netFreight\":{\"currency\":\"USD\",\"amount\":60.71},\"totalSurcharges\":{\"currency\":\"USD\",\"amount\":3.04},\"netFedExCharge\":{\"currency\":\"USD\",\"amount\":63.75},\"totalTaxes\":{\"currency\":\"USD\",\"amount\":0},\"netCharge\":{\"currency\":\"USD\",\"amount\":63.75},\"totalRebates\":{\"currency\":\"USD\",\"amount\":0},\"freightDiscounts\":[],\"rebates\":[],\"surcharges\":[{\"surchargeType\":\"INSURED_VALUE\",\"description\":\"Insured value\",\"amount\":0},{\"surchargeType\":\"FUEL\",\"description\":\"Fuel\",\"amount\":3.04}],\"taxes\":[]}]}}";

        try {
            return objectMapper.readValue(rateReplyJson, PreauthorizedRateResource.class);
        } catch (IOException exp) {
            logger.error("Exception occured while mapping json {}", exp.getMessage());
        }
        return null;
    }
}
