package com.fedex.rscs.client.srrs.dto;

import java.util.List;

/**
 * Define the Contact specification DTO
 * 
 * @author 3932968
 *
 */
public class ShippingContact {

    private String personName;
    private Company company;
    private EmailDetail emailDetails;
    private List<PhoneNumberDetail> phoneNumberDetails;

    public ShippingContact() {}

    /**
     * @param personName
     * @param company
     * @param emailDetails
     * @param phoneNumberDetails
     */
    public ShippingContact(String personName, Company company, EmailDetail emailDetails,
            List<PhoneNumberDetail> phoneNumberDetails) {
        this.personName = personName;
        this.company = company;
        this.emailDetails = emailDetails;
        this.phoneNumberDetails = phoneNumberDetails;
    }

    /**
     * @return the personName
     */
    public String getPersonName() {

        return personName;
    }

    /**
     * @param personName the personName to set
     */
    public void setPersonName(
            String personName) {

        this.personName = personName;
    }

    /**
     * @return the company
     */
    public Company getCompany() {

        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(
            Company company) {

        this.company = company;
    }

    /**
     * @return the emailDetails
     */
    public EmailDetail getEmailDetails() {

        return emailDetails;
    }

    /**
     * @param emailDetails the emailDetails to set
     */
    public void setEmailDetails(
            EmailDetail emailDetails) {

        this.emailDetails = emailDetails;
    }

    /**
     * @return the phoneNumberDetails
     */
    public List<PhoneNumberDetail> getPhoneNumberDetails() {

        return phoneNumberDetails;
    }

    /**
     * @param phoneNumberDetails the phoneNumberDetails to set
     */
    public void setPhoneNumberDetails(
            List<PhoneNumberDetail> phoneNumberDetails) {

        this.phoneNumberDetails = phoneNumberDetails;
    }

    @Override
    public String toString() {

        return "ShippingContact [personName=" + personName + ", company=" + company + ", emailDetails=" + emailDetails
                + ", phoneNumberDetails=" + phoneNumberDetails + "]";
    }

}
