package com.fedex.rscs.client.srrs.dto;

import com.fedex.common.cxs.dto.CXSOutput;

/**
 * Wrapper of ShipmentRateQuotesResource DTO
 * 
 * @author 3900094
 *
 */
public class ShipmentRateQuotesResponse extends CXSOutput {

    private ShipmentRateQuotesResource rateQuote;

    public ShipmentRateQuotesResponse() {

        super();
    }

    /**
     * @return the rateQuote
     */
    public ShipmentRateQuotesResource getRateQuote() {

        return rateQuote;
    }

    /**
     * @param rateQuote to set the rateQuote
     */
    public void setRateQuote(
            ShipmentRateQuotesResource rateQuote) {

        this.rateQuote = rateQuote;
    }

    @Override
    public String toString() {

        return "ShipmentRateQuotesResponse [rateQuote=" + rateQuote + "]";
    }

}
