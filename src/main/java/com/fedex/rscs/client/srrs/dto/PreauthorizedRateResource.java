package com.fedex.rscs.client.srrs.dto;

import com.fedex.common.cxs.dto.CXSOutput;

/**
 * Define PreauthorizedRateResource Specification DTO.
 * 
 * @author 5034922
 *
 */
public class PreauthorizedRateResource extends CXSOutput {

    private PreauthorizedRateDetail preauthorizedRateDetail;

    public PreauthorizedRateResource() {

    }

    /**
     * @param preauthorizedRateDetail
     */
    public PreauthorizedRateResource(PreauthorizedRateDetail preauthorizedRateDetail) {

        this.preauthorizedRateDetail = preauthorizedRateDetail;
    }


    /**
     * @return the preauthorizedRateDetail
     */
    public PreauthorizedRateDetail getPreauthorizedRateDetail() {

        return preauthorizedRateDetail;
    }


    /**
     * @param preauthorizedRateDetail the preauthorizedRateDetail to set
     */
    public void setPreauthorizedRateDetail(
            PreauthorizedRateDetail preauthorizedRateDetail) {

        this.preauthorizedRateDetail = preauthorizedRateDetail;
    }


    @Override
    public String toString() {

        return "PreauthorizedRateResource [preauthorizedRateDetail=" + preauthorizedRateDetail + "]";
    }
}
