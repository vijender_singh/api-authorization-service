package com.fedex.rscs.client.srrs.dto;

/**
 * Enum to hold Payment Type
 * 
 * @author 3932968
 *
 */
public enum PaymentType {
    ACCOUNT, NON_ACCOUNT;
}
