package com.fedex.rscs.client.srrs.dto;

/**
 * Represents the Location Info DTO
 * 
 * @author 3900094
 *
 */
public class LocationInfo {

    private String code;
    private Address address;

    public LocationInfo() {}

    /**
     * @param code
     * @param address
     */
    public LocationInfo(String code, Address address) {

        this.code = code;
        this.address = address;
    }

    /**
     * @return the code
     */
    public String getCode() {

        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(
            String code) {

        this.code = code;
    }

    /**
     * @return the address
     */
    public Address getAddress() {

        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(
            Address address) {

        this.address = address;
    }

    @Override
    public String toString() {

        return "Location [code=" + code + ", address=" + address + "]";
    }

}
