package com.fedex.rscs.client.srrs.dto;

import java.util.Arrays;
import java.util.List;

/**
 * Represents the Requested Package LineItems DTO
 * 
 * @author 3932968
 *
 */
public class RequestedPackageLineItem {

    private Dimensions dimensions;
    private Price insuredValue;
    private PackageSpecialHandlingType specialHandlingDetail;
    private List<SpecialServiceDescription> specialServicesRequested;
    private Weight weight;
    private String physicalPackagingType;
    private Weight dryIceWeight;
    private List<BatteryClassificationType> batteryClassifications;
    private List<ShippingCustomerReferenceDetail> customerReferences;

    /**
     * 
     * @return dimensions
     */
    public Dimensions getDimensions() {

        return dimensions;
    }

    /**
     * 
     * @param dimensions to set dimensions
     */
    public void setDimensions(
            Dimensions dimensions) {

        this.dimensions = dimensions;
    }

    /**
     * 
     * @return insuredValue
     */
    public Price getInsuredValue() {

        return insuredValue;
    }

    /**
     * 
     * @param setInsuredValue to set setInsuredValue
     */
    public void setInsuredValue(
            Price insuredValue) {

        this.insuredValue = insuredValue;
    }

    /**
     * 
     * @return specialHandlingDetail
     */
    public PackageSpecialHandlingType getSpecialHandlingDetail() {

        return specialHandlingDetail;
    }

    /**
     * 
     * @param specialHandlingDetail to set specialHandlingDetail
     */
    public void setSpecialHandlingDetail(
            PackageSpecialHandlingType specialHandlingDetail) {

        this.specialHandlingDetail = specialHandlingDetail;
    }

    /**
     * 
     * @return specialServicesRequested
     */
    public List<SpecialServiceDescription> getSpecialServicesRequested() {

        return specialServicesRequested;
    }

    /**
     * 
     * @param specialServicesRequested to set specialServicesRequested
     */
    public void setSpecialServicesRequested(
            List<SpecialServiceDescription> specialServicesRequested) {

        this.specialServicesRequested = specialServicesRequested;
    }

    /**
     * 
     * @return weight
     */
    public Weight getWeight() {

        return weight;
    }

    /**
     * 
     * @param weight to set weight
     */
    public void setWeight(
            Weight weight) {

        this.weight = weight;
    }

    /**
     * @return the customerReferences
     */
    public List<ShippingCustomerReferenceDetail> getCustomerReferences() {

        return customerReferences;
    }

    /**
     * @param customerReferences to set customerReferences
     */
    public void setCustomerReferences(
            List<ShippingCustomerReferenceDetail> customerReferences) {

        this.customerReferences = customerReferences;
    }

    public String getPhysicalPackagingType() {

        return physicalPackagingType;
    }


    public void setPhysicalPackagingType(
            String physicalPackagingType) {

        this.physicalPackagingType = physicalPackagingType;
    }


    public Weight getDryIceWeight() {

        return dryIceWeight;
    }


    public void setDryIceWeight(
            Weight dryIceWeight) {

        this.dryIceWeight = dryIceWeight;
    }


    public List<BatteryClassificationType> getBatteryClassifications() {

        return batteryClassifications;
    }


    public void setBatteryClassifications(
            List<BatteryClassificationType> batteryClassifications) {

        this.batteryClassifications = batteryClassifications;
    }

    @Override
    public String toString() {

        return "RequestedPackageLineItem [dimensions=" + dimensions + ", insuredValue=" + insuredValue
                + ", specialHandlingDetail=" + specialHandlingDetail + ", specialServicesRequested="
                + specialServicesRequested + ", weight=" + weight + ", physicalPackagingType=" + physicalPackagingType
                + ", dryIceWeight=" + dryIceWeight + ", batteryClassifications="
                + batteryClassifications + ", customerReferences=" + customerReferences + "]";
    }

}
