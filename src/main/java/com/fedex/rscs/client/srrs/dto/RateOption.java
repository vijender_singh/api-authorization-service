package com.fedex.rscs.client.srrs.dto;

/**
 * Rate Options enum
 * 
 * @author 3932956
 *
 */
public enum RateOption {

    ONE_RATE, STANDARD_RATE;
}
