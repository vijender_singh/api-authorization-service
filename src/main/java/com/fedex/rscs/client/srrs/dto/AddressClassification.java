package com.fedex.rscs.client.srrs.dto;

/**
 * Enum to hold Address Classification
 * 
 * @author 3932968
 *
 */
public enum AddressClassification {
    HOME, BUSINESS;
}
