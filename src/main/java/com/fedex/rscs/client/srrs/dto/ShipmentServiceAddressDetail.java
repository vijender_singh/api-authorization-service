package com.fedex.rscs.client.srrs.dto;

/**
 * Define the ShipmentServiceAddressDetail specification DTO
 * 
 * @author 5034922
 *
 */
public class ShipmentServiceAddressDetail {

    private String countryCode;
    private String stateOrProvinceCode;
    private String postalCode;
    private String serviceArea;
    private String locationId;
    private int locationNumber;
    private String airportId;
    
    public ShipmentServiceAddressDetail() {
        
    }
    
    /**
     * @return the countryCode
     */
    public String getCountryCode() {
    
        return countryCode;
    }
    
    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(
            String countryCode) {
    
        this.countryCode = countryCode;
    }
    
    /**
     * @return the stateOrProvinceCode
     */
    public String getStateOrProvinceCode() {
    
        return stateOrProvinceCode;
    }
    
    /**
     * @param stateOrProvinceCode the stateOrProvinceCode to set
     */
    public void setStateOrProvinceCode(
            String stateOrProvinceCode) {
    
        this.stateOrProvinceCode = stateOrProvinceCode;
    }
    
    /**
     * @return the postalCode
     */
    public String getPostalCode() {
    
        return postalCode;
    }
    
    /**
     * @param postalCode the postalCode to set
     */
    public void setPostalCode(
            String postalCode) {
    
        this.postalCode = postalCode;
    }
    
    /**
     * @return the serviceArea
     */
    public String getServiceArea() {
    
        return serviceArea;
    }
    
    /**
     * @param serviceArea the serviceArea to set
     */
    public void setServiceArea(
            String serviceArea) {
    
        this.serviceArea = serviceArea;
    }
    
    /**
     * @return the locationId
     */
    public String getLocationId() {
    
        return locationId;
    }
    
    /**
     * @param locationId the locationId to set
     */
    public void setLocationId(
            String locationId) {
    
        this.locationId = locationId;
    }
    
    /**
     * @return the locationNumber
     */
    public int getLocationNumber() {
    
        return locationNumber;
    }
    
    /**
     * @param locationNumber the locationNumber to set
     */
    public void setLocationNumber(
            int locationNumber) {
    
        this.locationNumber = locationNumber;
    }
    
    /**
     * @return the airportId
     */
    public String getAirportId() {
    
        return airportId;
    }
    
    /**
     * @param airportId the airportId to set
     */
    public void setAirportId(
            String airportId) {
    
        this.airportId = airportId;
    }

    @Override
    public String toString() {

        return "ShipmentServiceAddressDetail [countryCode=" + countryCode + ", stateOrProvinceCode="
                + stateOrProvinceCode + ", postalCode=" + postalCode + ", serviceArea=" + serviceArea + ", locationId="
                + locationId + ", locationNumber=" + locationNumber + ", airportId=" + airportId + "]";
    }
    
    

}
