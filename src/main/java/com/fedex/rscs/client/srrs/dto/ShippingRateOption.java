package com.fedex.rscs.client.srrs.dto;

/**
 * ShippingRateOption DTO
 * 
 * @author 3932956
 *
 */
public class ShippingRateOption {

    private String packagingType;
    private String signatureOption;
    private ServiceDescription serviceDetails;
    private String deliveryDayOfWeek;
    private boolean hal;
    private CommitDetail commitDetails;
    private ShippingRateDetail rateDetails;

    public ShippingRateOption() {}

    /**
     * @param packagingType
     * @param signatureOption
     * @param serviceDetails
     * @param deliveryDayOfWeek
     * @param hal
     * @param commitDetails
     * @param rateDetails
     */
    public ShippingRateOption(String packagingType, String signatureOption, ServiceDescription serviceDetails,
            String deliveryDayOfWeek, boolean hal, CommitDetail commitDetails, ShippingRateDetail rateDetails) {

        this.packagingType = packagingType;
        this.signatureOption = signatureOption;
        this.serviceDetails = serviceDetails;
        this.deliveryDayOfWeek = deliveryDayOfWeek;
        this.hal = hal;
        this.commitDetails = commitDetails;
        this.rateDetails = rateDetails;
    }

    /**
     * @return the packagingType
     */
    public String getPackagingType() {

        return packagingType;
    }

    /**
     * @param packagingType the packagingType to set
     */
    public void setPackagingType(
            String packagingType) {

        this.packagingType = packagingType;
    }

    /**
     * @return the signatureOption
     */
    public String getSignatureOption() {

        return signatureOption;
    }

    /**
     * @param signatureOption the signatureOption to set
     */
    public void setSignatureOption(
            String signatureOption) {

        this.signatureOption = signatureOption;
    }

    /**
     * @return the serviceDetails
     */
    public ServiceDescription getServiceDetails() {

        return serviceDetails;
    }

    /**
     * @param serviceDetails the serviceDetails to set
     */
    public void setServiceDetails(
            ServiceDescription serviceDetails) {

        this.serviceDetails = serviceDetails;
    }

    /**
     * @return the deliveryDayOfWeek
     */
    public String getDeliveryDayOfWeek() {

        return deliveryDayOfWeek;
    }

    /**
     * @param deliveryDayOfWeek the deliveryDayOfWeek to set
     */
    public void setDeliveryDayOfWeek(
            String deliveryDayOfWeek) {

        this.deliveryDayOfWeek = deliveryDayOfWeek;
    }

    /**
     * @return the hal
     */
    public boolean isHal() {

        return hal;
    }

    /**
     * @param hal the hal to set
     */
    public void setHal(
            boolean hal) {

        this.hal = hal;
    }

    /**
     * @return the commitDetails
     */
    public CommitDetail getCommitDetails() {

        return commitDetails;
    }

    /**
     * @param commitDetails the commitDetails to set
     */
    public void setCommitDetails(
            CommitDetail commitDetails) {

        this.commitDetails = commitDetails;
    }

    /**
     * @return the rateDetails
     */
    public ShippingRateDetail getRateDetails() {

        return rateDetails;
    }

    /**
     * @param rateDetails to set the rateDetails
     */
    public void setRateDetails(
            ShippingRateDetail rateDetails) {

        this.rateDetails = rateDetails;
    }

    @Override
    public String toString() {

        return "ShippingRateOption [packagingType=" + packagingType + ", signatureOption=" + signatureOption
                + ", serviceDetails=" + serviceDetails + ", deliveryDayOfWeek=" + deliveryDayOfWeek + ", hal=" + hal
                + ", commitDetails=" + commitDetails + ", rateDetails=" + rateDetails + "]";
    }

}
