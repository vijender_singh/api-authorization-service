package com.fedex.rscs.client.srrs.dto;

/**
 * Define the Price DTO
 * 
 * @author 3932968
 *
 */
public class Price {

    private Currency currency;
    private double amount;

    public Price() {}

    /**
     * @param currency
     * @param amount
     */
    public Price(Currency currency, double amount) {

        this.currency = currency;
        this.amount = amount;
    }

    /**
     * 
     * @return currency
     */
    public Currency getCurrency() {

        return currency;
    }

    /**
     * 
     * @param currency to set currency
     */
    public void setCurrency(
            Currency currency) {

        this.currency = currency;
    }

    /**
     * @return the amount
     */
    public double getAmount() {

        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(
            double amount) {

        this.amount = amount;
    }

    @Override
    public String toString() {

        return "Price [currency=" + currency + ", amount=" + amount + "]";
    }

}
