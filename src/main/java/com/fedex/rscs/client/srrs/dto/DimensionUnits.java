package com.fedex.rscs.client.srrs.dto;

/**
 * Enum to hold dimension units
 * 
 * @author 3932956
 *
 */
public enum DimensionUnits {

    IN, CM;
}
