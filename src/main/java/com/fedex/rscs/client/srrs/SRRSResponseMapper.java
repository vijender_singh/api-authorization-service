package com.fedex.rscs.client.srrs;

import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.fedex.rscs.client.srrs.dto.PreauthorizedRateResource;
import com.fedex.rscs.client.srrs.dto.ShipmentRateQuotesResource;
import com.fedex.rscs.client.srrs.mapper.SRRSClientMapper;
import com.fedex.rscs.model.cshp.PreauthorizedRateData;
import com.fedex.rscs.model.cshp.ShippingRateData;

/**
 * This class is used to map ShipmentRateQuotesResource to ShippingRateData.
 * 
 * @author 3900094
 *
 */
@Component
public class SRRSResponseMapper {

    /**
     * This method will be used to transform the rate response.
     * 
     * @param ShipmentRateQuotesResource
     * @return ShippingRateData
     */
    public ShippingRateData transformRateResponse(
            ShipmentRateQuotesResource dto) {

        ShippingRateData response = null;

        if (!ObjectUtils.isEmpty(dto.getRateOptions())) {

            return SRRSClientMapper.INSTANCE.toModel(dto.getRateOptions()
                    .get(0));
        }

        return response;
    }
    
    /**
     * This method will be used to transform the rate response.
     * 
     * @param ShipmentRateQuotesResource
     * @return ShippingRateData
     */
    public PreauthorizedRateData transformPreauthorizedRateResponse(
            PreauthorizedRateResource dto) {

        PreauthorizedRateData response = null;

        if (!ObjectUtils.isEmpty(dto.getPreauthorizedRateDetail())) {

            return SRRSClientMapper.INSTANCE.toModel(dto.getPreauthorizedRateDetail());
        }

        return response;
    }

}
