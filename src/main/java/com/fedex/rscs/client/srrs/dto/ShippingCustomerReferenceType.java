package com.fedex.rscs.client.srrs.dto;

/**
 * Shipping customer reference type enum
 * 
 * @author Samicheen Khariwal
 *
 */
public enum ShippingCustomerReferenceType {
    CUSTOMER_REFERENCE, P_O_NUMBER, INVOICE_NUMBER, DEPARTMENT_NUMBER
}
