package com.fedex.rscs.client.srrs.dto;

/**
 * Define the Company Specification DTO
 * 
 * @author 3932968
 *
 */
public class Company {

    private String name;

    public Company() {}

    /**
     * 
     * @param name
     */
    public Company(String name) {

        this.name = name;
    }

    /**
     * 
     * @return name
     */
    public String getName() {

        return name;
    }

    /**
     * 
     * @param name to set name
     */
    public void setName(
            String name) {

        this.name = name;
    }

    @Override
    public String toString() {

        return "Company [name=" + name + "]";
    }

}
