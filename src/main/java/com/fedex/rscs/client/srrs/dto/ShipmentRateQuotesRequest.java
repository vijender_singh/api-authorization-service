package com.fedex.rscs.client.srrs.dto;

/**
 * Wrapper of Shipment Rate Quotes Request.
 * 
 * @author 3900094
 *
 */
public class ShipmentRateQuotesRequest {

    private ShipmentRateQuotesRequestData shipmentRateQuoteRequest;

    public ShipmentRateQuotesRequest() {}

    /**
     * @param shipmentRateQuoteRequest
     */
    public ShipmentRateQuotesRequest(ShipmentRateQuotesRequestData shipmentRateQuoteRequest) {

        this.shipmentRateQuoteRequest = shipmentRateQuoteRequest;
    }

    /**
     * @return the shipmentRateQuoteRequest
     */
    public ShipmentRateQuotesRequestData getShipmentRateQuoteRequest() {

        return shipmentRateQuoteRequest;
    }

    /**
     * @param shipmentRateQuoteRequest to set the shipmentRateQuoteRequest
     */
    public void setShipmentRateQuoteRequest(
            ShipmentRateQuotesRequestData shipmentRateQuoteRequest) {

        this.shipmentRateQuoteRequest = shipmentRateQuoteRequest;
    }

    @Override
    public String toString() {

        return "ShipmentRateQuotesRequest [shipmentRateQuoteRequest=" + shipmentRateQuoteRequest + "]";
    }

}
