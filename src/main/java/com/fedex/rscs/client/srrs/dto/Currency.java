package com.fedex.rscs.client.srrs.dto;

/**
 * Enum to hold currency
 * 
 * @author 3932968
 *
 */
public enum Currency {
    USD;
}
