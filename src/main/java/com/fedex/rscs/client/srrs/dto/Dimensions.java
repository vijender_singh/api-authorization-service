package com.fedex.rscs.client.srrs.dto;

/**
 * Define the Dimensions DTO.
 * 
 * @author 3932968
 *
 */
public class Dimensions {

    private String length;
    private String width;
    private String height;
    private DimensionUnits units;

    public Dimensions() {}

    /**
     * @param length
     * @param width
     * @param height
     * @param units
     */
    public Dimensions(String length, String width, String height, DimensionUnits units) {

        this.length = length;
        this.width = width;
        this.height = height;
        this.units = units;
    }

    /**
     * 
     * @return length
     */
    public String getLength() {

        return length;
    }

    /**
     * 
     * @param length to set length
     */
    public void setLength(
            String length) {

        this.length = length;
    }

    /**
     * 
     * @return width
     */
    public String getWidth() {

        return width;
    }

    /**
     * 
     * @param width to set width
     */
    public void setWidth(
            String width) {

        this.width = width;
    }

    /**
     * 
     * @return height
     */
    public String getHeight() {

        return height;
    }

    /**
     * 
     * @param height to set height
     */
    public void setHeight(
            String height) {

        this.height = height;
    }

    /**
     * @return units
     */
    public DimensionUnits getUnits() {

        return units;
    }

    /**
     * @param units to set the units
     */
    public void setUnits(
            DimensionUnits units) {

        this.units = units;
    }

    @Override
    public String toString() {

        return "Dimensions [length=" + length + ", width=" + width + ", height=" + height + ", units=" + units + "]";
    }

}
