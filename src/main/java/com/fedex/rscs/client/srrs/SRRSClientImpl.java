package com.fedex.rscs.client.srrs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.HttpStatusCodeException;

import com.fedex.common.cxs.dto.CXSEnvelope;
import com.fedex.rscs.client.srrs.dto.PreauthorizedRateResponse;
import com.fedex.rscs.client.srrs.dto.ShipmentRateQuotesRequest;
import com.fedex.rscs.client.srrs.dto.ShipmentRateQuotesResponse;
import com.fedex.rscs.client.util.RetailShipmentCreationServiceClientUtil;
import com.fedex.rscs.config.AppConfig;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.client.ClientTerminalException;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.cshp.PreauthorizedRateData;
import com.fedex.rscs.model.cshp.ShippingRateData;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

/**
 * This class is implementation of SRRSClient interface.
 * 
 * @author 3900094
 *
 */
@Service
@Profile("!mock")
public class SRRSClientImpl implements SRRSClient {

    private static final Logger logger = LoggerFactory.getLogger(SRRSClientImpl.class);
    private static final String RATING_SERVICE_URI = "/retailshipping/fedexoffice/v1/ratequotes";
    private static final String PRE_AUTHORIZED_RATE_URI = "/retailshipping/fedexoffice/v1/preauthorizedrates";

    private final AppConfig appConfig;
    private final OAuth2RestTemplate restTemplate;
    private final SRRSResponseMapper responseMapper;
    private final SRRSRequestBuilder requestBuilder;
    private final RetailShipmentCreationServiceClientUtil clientUtil;

    @Autowired
    public SRRSClientImpl(final OAuth2RestTemplate restTemplate, final AppConfig appConfig,
            final SRRSResponseMapper responseMapper, final SRRSRequestBuilder requestBuilder,
            final RetailShipmentCreationServiceClientUtil clientUtil)
    {

        this.appConfig = appConfig;
        this.restTemplate = restTemplate;
        this.responseMapper = responseMapper;
        this.requestBuilder = requestBuilder;
        this.clientUtil = clientUtil;
    }

    /**
     * This method is implementation of SRRSClient method.
     * 
     * @param workStationDetail
     * @param requestedShipmentDetail
     * @return OpenShipmentResponse
     */
    @Override
    @HystrixCommand(groupKey = "SRRSClientImpl", commandKey = "getRateOptions")
    public ShippingRateData getRateOptions(
            WorkstationDetail workStationDetail,
            RequestedShipmentDetail requestedShipmentDetail) {

        logger.info("Execution started for service endpoint: {} ", RATING_SERVICE_URI);
        String ratingServiceEndpoint = appConfig.getRateServiceUri() + RATING_SERVICE_URI;
        ShipmentRateQuotesRequest rateRequest = null;
        ResponseEntity<CXSEnvelope<ShipmentRateQuotesResponse>> response;
        try {

            rateRequest = requestBuilder.buildRateRequest(workStationDetail, requestedShipmentDetail);

            logger.debug("Prepare Request for SRRS rate call {}", rateRequest);

            response = restTemplate.exchange(ratingServiceEndpoint, HttpMethod.POST,
                    new HttpEntity<ShipmentRateQuotesRequest>(rateRequest),
                    new ParameterizedTypeReference<CXSEnvelope<ShipmentRateQuotesResponse>>() {});

            if (response.getStatusCode() == HttpStatus.OK && response.getBody() != null && response.getBody()
                    .getOutput() != null && response.getBody()
                            .getOutput()
                            .getRateQuote() != null
                    && !ObjectUtils.isEmpty(response.getBody()
                            .getOutput()
                            .getRateQuote()
                            .getRateOptions())) {

                logger.debug("Received rating service response: {} ", response.getBody()
                        .getOutput()
                        .getRateQuote());

                return responseMapper.transformRateResponse(response.getBody()
                        .getOutput()
                        .getRateQuote());
            } else {

                logger.error("Response : {} not as expected with ShipmentRateQuotesRequest : {} and endpoint : {}",
                        response, rateRequest, ratingServiceEndpoint);
                throw new ClientTerminalException(ServiceErrorCode.INTERNAL_SERVER_ERROR);
            }

        } catch (HttpStatusCodeException httpException) {

            logger.error("failed to process the rate request {} ", rateRequest, httpException);
            throw new ClientTerminalException(clientUtil.processErrorResponse(httpException));
        } catch (RuntimeException runTimeException) {

            logger.error("runtime exception occured when ShipmentRateQuotesRequest : {} and endpoint : {}", rateRequest,
                    ratingServiceEndpoint, runTimeException);
            throw new ClientTerminalException(ServiceErrorCode.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * This method is implementation of getPreauthorizedRates method.
     * 
     * @param workStationDetail
     * @param requestedShipmentDetail
     * @return PreauthorizedRateData
     */
    @Override
    @HystrixCommand(groupKey = "SRRSClientImpl", commandKey = "getPreauthorizedRates")
    public PreauthorizedRateData getPreauthorizedRates(
            WorkstationDetail workStationDetail,
            RequestedShipmentDetail requestedShipmentDetail) {

        logger.info("Execution started for service endpoint: {} ", PRE_AUTHORIZED_RATE_URI);
        String ratingServiceEndpoint = appConfig.getRateServiceUri() + PRE_AUTHORIZED_RATE_URI;
        ShipmentRateQuotesRequest rateRequest = null;
        ResponseEntity<CXSEnvelope<PreauthorizedRateResponse>> response;
        try {

            rateRequest = requestBuilder.buildRateRequest(workStationDetail, requestedShipmentDetail);

            logger.debug("Prepare Request for SRRS pre-authorized rate call {}", rateRequest);

            response = restTemplate.exchange(ratingServiceEndpoint, HttpMethod.POST,
                    new HttpEntity<ShipmentRateQuotesRequest>(rateRequest),
                    new ParameterizedTypeReference<CXSEnvelope<PreauthorizedRateResponse>>() {});

            if (response.getStatusCode() == HttpStatus.OK && response.getBody() != null && response.getBody()
                    .getOutput() != null && response.getBody()
                            .getOutput()
                            .getPreauthorizedRate() != null
                    && !ObjectUtils.isEmpty(response.getBody()
                            .getOutput()
                            .getPreauthorizedRate()
                            .getPreauthorizedRateDetail())) {

                logger.debug("Received rating service response: {} ", response.getBody()
                        .getOutput()
                        .getPreauthorizedRate());

                return responseMapper.transformPreauthorizedRateResponse(response.getBody()
                        .getOutput()
                        .getPreauthorizedRate());
            } else {

                logger.error("Response : {} not as expected with ShipmentRateQuotesRequest : {} and endpoint : {}",
                        response, rateRequest, ratingServiceEndpoint);
                throw new ClientTerminalException(ServiceErrorCode.INTERNAL_SERVER_ERROR);
            }

        } catch (HttpStatusCodeException httpException) {

            logger.error("failed to process the pre-authorized rate request {} ", rateRequest, httpException);
            throw new ClientTerminalException(clientUtil.processErrorResponse(httpException));
        } catch (RuntimeException runTimeException) {

            logger.error("runtime exception occured when ShipmentRateQuotesRequest : {} and endpoint : {}", rateRequest,
                    ratingServiceEndpoint, runTimeException);
            throw new ClientTerminalException(ServiceErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

}
