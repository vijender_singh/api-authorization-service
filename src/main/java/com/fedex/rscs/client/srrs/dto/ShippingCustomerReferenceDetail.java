package com.fedex.rscs.client.srrs.dto;

/**
 * Define the Shipping Customer reference detail DTO
 * 
 * @author Samicheen Khariwal
 *
 */
public class ShippingCustomerReferenceDetail {

    private ShippingCustomerReferenceType referenceType;
    private String value;

    public ShippingCustomerReferenceDetail() {};

    /**
     * @param referenceType
     * @param value
     */
    public ShippingCustomerReferenceDetail(ShippingCustomerReferenceType referenceType, String value) {

        this.referenceType = referenceType;
        this.value = value;
    }

    /**
     * @return the referenceType
     */
    public ShippingCustomerReferenceType getReferenceType() {

        return referenceType;
    }

    /**
     * @param referenceType the referenceType to set
     */
    public void setReferenceType(
            ShippingCustomerReferenceType referenceType) {

        this.referenceType = referenceType;
    }

    /**
     * @return the value
     */
    public String getValue() {

        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(
            String value) {

        this.value = value;
    }

    @Override
    public String toString() {

        return "ShippingCustomerReferenceDetail [referenceType=" + referenceType + ", value=" + value + "]";
    }
}
