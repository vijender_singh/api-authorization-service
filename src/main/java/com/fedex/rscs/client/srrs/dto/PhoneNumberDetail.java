package com.fedex.rscs.client.srrs.dto;

/**
 * Define the Phone Number Detail DTO
 * 
 * @author 3932968
 *
 */
public class PhoneNumberDetail {

    private PhoneNumber phoneNumber;
    private Usage usage;

    public PhoneNumberDetail() {}

    /**
     * 
     * @param phoneNumber
     * @param usage
     */
    public PhoneNumberDetail(PhoneNumber phoneNumber, Usage usage) {

        this.phoneNumber = phoneNumber;
        this.usage = usage;
    }

    /**
     * 
     * @return phoneNumber
     */
    public PhoneNumber getPhoneNumber() {

        return phoneNumber;
    }

    /**
     * 
     * @param phoneNumber to set phoneNumber
     */
    public void setPhoneNumber(
            PhoneNumber phoneNumber) {

        this.phoneNumber = phoneNumber;
    }

    /**
     * 
     * @return usage
     */
    public Usage getUsage() {

        return usage;
    }

    /**
     * 
     * @param usage to set usage
     */
    public void setUsage(
            Usage usage) {

        this.usage = usage;
    }

    @Override
    public String toString() {

        return "PhoneNumberDetail [phoneNumber=" + phoneNumber + ", usage=" + usage + "]";
    }

}
