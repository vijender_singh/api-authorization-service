package com.fedex.rscs.client.srrs.dto;

/**
 * Enum to hold packaging handling types
 * 
 * @author 3932968
 *
 */
public enum PackageSpecialHandlingType {
    CUSTOMER, FEDEX_OFFICE, FEDEX_DAMAGE_KNOWN, INSPECTED, INSPECTED_DAMAGE_KNOWN, CUSTOMER_REFUSED_INSPECTION, REFUSED_INSPECTION_DAMAGE_KNOWN;
}
