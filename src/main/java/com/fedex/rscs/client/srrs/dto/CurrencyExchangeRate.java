package com.fedex.rscs.client.srrs.dto;

import java.math.BigDecimal;

/**
 * Define CurrencyExchangeRate specification DTO.
 * 
 * @author 5034922
 *
 */
public class CurrencyExchangeRate {

    private Currency fromCurrency;
    private Currency intoCurrency;
    private BigDecimal rate;

    public CurrencyExchangeRate() {

    }


    /**
     * @return the fromCurrency
     */
    public Currency getFromCurrency() {

        return fromCurrency;
    }


    /**
     * @param fromCurrency the fromCurrency to set
     */
    public void setFromCurrency(
            Currency fromCurrency) {

        this.fromCurrency = fromCurrency;
    }


    /**
     * @return the intoCurrency
     */
    public Currency getIntoCurrency() {

        return intoCurrency;
    }


    /**
     * @param intoCurrency the intoCurrency to set
     */
    public void setIntoCurrency(
            Currency intoCurrency) {

        this.intoCurrency = intoCurrency;
    }


    /**
     * @return the rate
     */
    public BigDecimal getRate() {

        return rate;
    }


    /**
     * @param rate the rate to set
     */
    public void setRate(
            BigDecimal rate) {

        this.rate = rate;
    }


    @Override
    public String toString() {

        return "CurrencyExchangeRate [fromCurrency=" + fromCurrency + ", intoCurrency=" + intoCurrency + ", rate="
                + rate + "]";
    }

}
