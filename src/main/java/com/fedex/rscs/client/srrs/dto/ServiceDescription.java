package com.fedex.rscs.client.srrs.dto;

/**
 * ServiceDescription DTO
 * 
 * @author 3932956
 *
 */
public class ServiceDescription {

    private String serviceType;
    private String serviceName;
    private String description;

    public ServiceDescription() {

    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {

        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(
            String serviceType) {

        this.serviceType = serviceType;
    }

    /**
     * @return the serviceName
     */
    public String getServiceName() {

        return serviceName;
    }

    /**
     * @param serviceName the serviceName to set
     */
    public void setServiceName(
            String serviceName) {

        this.serviceName = serviceName;
    }

    /**
     * @return the description
     */
    public String getDescription() {

        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(
            String description) {

        this.description = description;
    }

    @Override
    public String toString() {

        return "ServiceDescription [serviceType=" + serviceType + ", serviceName=" + serviceName + ", description="
                + description + "]";
    }

}
