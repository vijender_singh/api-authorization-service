package com.fedex.rscs.client.srrs.dto;

/**
 * Represents the Shipping Payment Detail DTO
 * 
 * @author 3932968
 *
 */
public class ShippingPaymentOption {

    private PaymentType paymentType;
    private ResponsibleParty responsibleParty;
    private String accountNumber;
    private String interlineId;

    public ShippingPaymentOption() {}

    /**
     * @param paymentType
     * @param responsibleParty
     * @param accountNumber
     */
    public ShippingPaymentOption(PaymentType paymentType, ResponsibleParty responsibleParty, String accountNumber) {

        this.paymentType = paymentType;
        this.responsibleParty = responsibleParty;
        this.accountNumber = accountNumber;
    }

    /**
     * @return the paymentType
     */
    public PaymentType getPaymentType() {

        return paymentType;
    }

    /**
     * @param paymentType the paymentType to set
     */
    public void setPaymentType(
            PaymentType paymentType) {

        this.paymentType = paymentType;
    }

    /**
     * @return the responsibleParty
     */
    public ResponsibleParty getResponsibleParty() {

        return responsibleParty;
    }

    /**
     * @param responsibleParty the responsibleParty to set
     */
    public void setResponsibleParty(
            ResponsibleParty responsibleParty) {

        this.responsibleParty = responsibleParty;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {

        return accountNumber;
    }

    /**
     * @param accountNumber to set the accountNumber
     */
    public void setAccountNumber(
            String accountNumber) {

        this.accountNumber = accountNumber;
    }

    /**
     * @return the interlineId
     */
    public String getInterlineId() {

        return interlineId;
    }

    /**
     * @param interlineId the interlineId to set
     */
    public void setInterlineId(
            String interlineId) {

        this.interlineId = interlineId;
    }

    @Override
    public String toString() {

        return "ShippingPaymentOption [paymentType=" + paymentType + ", responsibleParty=" + responsibleParty
                + ", accountNumber=" + accountNumber + ", interlineId=" + interlineId + "]";
    }

}
