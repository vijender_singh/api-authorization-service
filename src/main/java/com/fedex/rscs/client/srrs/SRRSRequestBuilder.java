package com.fedex.rscs.client.srrs;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.fedex.rscs.client.srrs.dto.Address;
import com.fedex.rscs.client.srrs.dto.AddressClassification;
import com.fedex.rscs.client.srrs.dto.AppName;
import com.fedex.rscs.client.srrs.dto.Company;
import com.fedex.rscs.client.srrs.dto.Currency;
import com.fedex.rscs.client.srrs.dto.DimensionUnits;
import com.fedex.rscs.client.srrs.dto.Dimensions;
import com.fedex.rscs.client.srrs.dto.EmailDetail;
import com.fedex.rscs.client.srrs.dto.HoldAtLocationDetail;
import com.fedex.rscs.client.srrs.dto.HomeDeliveryDetail;
import com.fedex.rscs.client.srrs.dto.LocationDetail;
import com.fedex.rscs.client.srrs.dto.LocationInfo;
import com.fedex.rscs.client.srrs.dto.PackageSpecialHandlingType;
import com.fedex.rscs.client.srrs.dto.Party;
import com.fedex.rscs.client.srrs.dto.PaymentType;
import com.fedex.rscs.client.srrs.dto.PhoneNumber;
import com.fedex.rscs.client.srrs.dto.PhoneNumberDetail;
import com.fedex.rscs.client.srrs.dto.Price;
import com.fedex.rscs.client.srrs.dto.RequestHeaderDetail;
import com.fedex.rscs.client.srrs.dto.RequestedPackageLineItem;
import com.fedex.rscs.client.srrs.dto.RequestedShipment;
import com.fedex.rscs.client.srrs.dto.ResponsibleParty;
import com.fedex.rscs.client.srrs.dto.ShipmentRateQuotesRequest;
import com.fedex.rscs.client.srrs.dto.ShipmentRateQuotesRequestData;
import com.fedex.rscs.client.srrs.dto.ShipmentSpecialServiceDescription;
import com.fedex.rscs.client.srrs.dto.ShippingContact;
import com.fedex.rscs.client.srrs.dto.ShippingPaymentOption;
import com.fedex.rscs.client.srrs.dto.SpecialServiceDescription;
import com.fedex.rscs.client.srrs.dto.Units;
import com.fedex.rscs.client.srrs.dto.Usage;
import com.fedex.rscs.client.srrs.dto.Weight;
import com.fedex.rscs.client.srrs.dto.WorkstationDetails;
import com.fedex.rscs.client.srrs.mapper.SRRSClientMapper;
import com.fedex.rscs.dto.LocationType;
import com.fedex.rscs.model.AddressDetail;
import com.fedex.rscs.model.BatteryClassificationType;
import com.fedex.rscs.model.ContactDetail;
import com.fedex.rscs.model.DimensionDetail;
import com.fedex.rscs.model.HoldAtLocDetail;
import com.fedex.rscs.model.HomeDelvDetail;
import com.fedex.rscs.model.InsuredValueDetail;
import com.fedex.rscs.model.InterlineShippingData;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.PaymentDetail;
import com.fedex.rscs.model.RequestedPackageLineItemDetail;
import com.fedex.rscs.model.RequestedPackageSpecialServices;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ShipmentSpecialServiceDetail;
import com.fedex.rscs.model.SpecialServiceDescriptionDetail;
import com.fedex.rscs.model.WeightDetail;
import com.fedex.rscs.model.WorkstationDetail;

/**
 * This class is used to build request for SRRS rate call.
 * 
 * @author 3900094
 *
 */
@Component
public class SRRSRequestBuilder {

    /**
     * Method to build rate request
     * 
     * @param workStationDetail
     * @param requestedShipmentDetail
     * @return ShipmentRateQuotesRequest
     */
    public ShipmentRateQuotesRequest buildRateRequest(
            WorkstationDetail workStationDetail,
            RequestedShipmentDetail requestedShipmentDetail) {

        ShipmentRateQuotesRequest request = new ShipmentRateQuotesRequest();
        ShipmentRateQuotesRequestData rateRequest = new ShipmentRateQuotesRequestData();
        rateRequest.setHeaderDetails(createHeaderDetails(requestedShipmentDetail, workStationDetail));
        rateRequest.setLocationDetails(createLocationDetail(requestedShipmentDetail));
        rateRequest.setPaymentOption(createPaymentDetails(requestedShipmentDetail.getShippingChargesPayment(),
                requestedShipmentDetail.getInterlineShippingData()));
        rateRequest.setRequestedShipment(createRequestedShipment(requestedShipmentDetail));
        request.setShipmentRateQuoteRequest(rateRequest);
        return request;
    }

    /**
     * Method to create header details
     * 
     * @param requestedShipmentDetail
     * @param workStationDetail
     * @return
     */
    private RequestHeaderDetail createHeaderDetails(
            RequestedShipmentDetail requestedShipmentDetail,
            WorkstationDetail workStationDetail) {

        RequestHeaderDetail requestHeaderDetail = new RequestHeaderDetail();
        requestHeaderDetail.setRequestDateTime(requestedShipmentDetail.getShipTimestamp());
        requestHeaderDetail.setWorkstationDetails(createWorkStationDetails(workStationDetail));
        return requestHeaderDetail;
    }

    /**
     * Method to create workstation details
     * 
     * @param workStationDetail
     * @return
     */
    private WorkstationDetails createWorkStationDetails(
            WorkstationDetail workStationDetail) {

        WorkstationDetails workstationDetails = new WorkstationDetails();
        workstationDetails.setAppName(AppName.valueOf(workStationDetail.getAppName()));
        workstationDetails.setAppVersionId(workStationDetail.getAppVersionId());
        workstationDetails.setDeviceId(workStationDetail.getDeviceId());
        workstationDetails.setMeterNumber(workStationDetail.getMeterNumber());
        workstationDetails.setSoftwareId(workStationDetail.getSoftwareId());
        return workstationDetails;
    }

    /**
     * Method to create requested shipment
     * 
     * @param requestedShipmentDetail
     * @return
     */
    private RequestedShipment createRequestedShipment(
            RequestedShipmentDetail requestedShipmentDetail) {

        RequestedShipment requestedShipment = new RequestedShipment();
        requestedShipment.setPackagingType(requestedShipmentDetail.getPackagingType());
        requestedShipment.setRecipient(createParty(requestedShipmentDetail.getRecipient()));
        requestedShipment
                .setRequestedPackageLineItems(createRequestedPackage(requestedShipmentDetail.getPackageLineItems()));
        requestedShipment.setServiceType(requestedShipmentDetail.getServiceType());
        requestedShipment.setShipper(createParty(requestedShipmentDetail.getSender()));
        requestedShipment.setSpecialServicesRequested(
                createSpecialServiceRequested(requestedShipmentDetail.getSpecialServicesRequested()));
        return requestedShipment;
    }

    /**
     * Method to create special service requested
     * 
     * @param specialServicesRequested
     * @return
     */
    private ShipmentSpecialServiceDescription createSpecialServiceRequested(
            ShipmentSpecialServiceDetail specialServicesRequested) {

        ShipmentSpecialServiceDescription shipmentSpecialServiceDescription = new ShipmentSpecialServiceDescription();
        shipmentSpecialServiceDescription.setSpecialServices(
                createShipSpecialServices(specialServicesRequested.getSpecialServicesRequestedType()));
        shipmentSpecialServiceDescription
                .setHoldAtLocationDetail(createHalDetail(specialServicesRequested.getHoldAtLocationDetail()));
        shipmentSpecialServiceDescription
        .setHomeDeliveryDetail(createHomeDeliveryDetail(specialServicesRequested.getHomeDeliveryDetail()));
        return shipmentSpecialServiceDescription;
    }

    /**
     * Method to create holdAtLocation address
     * 
     * @param holdAtLocationDetail
     * @return
     */
    private HoldAtLocationDetail createHalDetail(
            HoldAtLocDetail sourceDetail) {

        HoldAtLocationDetail holdAtLocDetail = null;
        if (sourceDetail != null) {
            holdAtLocDetail = new HoldAtLocationDetail();
            holdAtLocDetail.setAddress(createAddress(sourceDetail.getAddress()));
            holdAtLocDetail.setLocationId(sourceDetail.getLocationId());
            if (sourceDetail.getContact() != null) {
                holdAtLocDetail.setCompany(SRRSClientMapper.INSTANCE.toCompanyDto(sourceDetail.getContact()
                        .getCompanyName()));
                holdAtLocDetail.setPhoneNumber(createPhoneNumberForHal(sourceDetail.getContact()));
            }
            if (sourceDetail.getLocationType() != null) {
                holdAtLocDetail.setLocationType(LocationType.valueOf(sourceDetail.getLocationType()));
            }
            holdAtLocDetail.setDisplayName(sourceDetail.getDisplayName());
        }
        return holdAtLocDetail;
    }

    /**
     * Method to create phone number for HAL
     * 
     * @param contact
     * @return
     */
    private PhoneNumberDetail createPhoneNumberForHal(
            ContactDetail contact) {

        PhoneNumberDetail phoneNumberDetail = null;
        phoneNumberDetail = new PhoneNumberDetail();
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setExtension(contact.getPhoneExtension());
        phoneNumber.setNumber(contact.getPhoneNumber());
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        if (contact.getPhoneUsage() != null) {
            phoneNumberDetail.setUsage(Usage.valueOf(contact.getPhoneUsage()));
        }
        return phoneNumberDetail;
    }

    /**
     * Method to create shipment special servieces
     * 
     * @param specialServicesRequestedType
     * @return
     */
    private List<SpecialServiceDescription> createShipSpecialServices(
            List<SpecialServiceDescriptionDetail> specialServicesRequestedType) {

        List<SpecialServiceDescription> specialServiceDescriptionList = new ArrayList<>();
        SpecialServiceDescription specialServiceDescription = null;

        if (!ObjectUtils.isEmpty(specialServicesRequestedType)) {

            for (SpecialServiceDescriptionDetail specialService : specialServicesRequestedType) {

                specialServiceDescription = new SpecialServiceDescription();
                specialServiceDescription.setSpecialServiceType(specialService.getSpecialServiceType());
                specialServiceDescription.setSpecialServiceSubType(specialService.getSpecialServiceSubType());
                specialServiceDescription.setDisplayText(specialService.getDisplayText());
                specialServiceDescription.setDescription(specialService.getDescription());
                specialServiceDescriptionList.add(specialServiceDescription);
            }
        }
        return specialServiceDescriptionList;
    }

    /**
     * Method to create package line item
     * 
     * @param packageLineItems
     * @return
     */
    private List<RequestedPackageLineItem> createRequestedPackage(
            List<RequestedPackageLineItemDetail> packageLineItems) {

        List<RequestedPackageLineItem> requestedPackageLineItemList = new ArrayList<>();
        RequestedPackageLineItem requestedPackageLineItems = null;

        for (RequestedPackageLineItemDetail packageLineItemDetail : packageLineItems) {

            requestedPackageLineItems = new RequestedPackageLineItem();
            if (packageLineItemDetail.getDimensions() != null) {
                requestedPackageLineItems.setDimensions(createDimension(packageLineItemDetail.getDimensions()));
            }
            requestedPackageLineItems.setInsuredValue(createInsuredValue(packageLineItemDetail.getInsuredValue()));
            requestedPackageLineItems.setSpecialHandlingDetail(
                    PackageSpecialHandlingType.valueOf(packageLineItemDetail.getSpecialHandlingDetail()));
            requestedPackageLineItems.setSpecialServicesRequested(
                    createSpecialService(packageLineItemDetail.getPackageSpecialServicesRequested()));
            requestedPackageLineItems.setWeight(createWeight(packageLineItemDetail.getWeight()));
            if (packageLineItemDetail.getPackageSpecialServicesRequested() != null
                    && packageLineItemDetail.getPackageSpecialServicesRequested()
                            .getDryIceWeightDetails() != null) {
                requestedPackageLineItems
                        .setDryIceWeight(createWeight(packageLineItemDetail.getPackageSpecialServicesRequested()
                                .getDryIceWeightDetails()));
            }
            if (!ObjectUtils.isEmpty(packageLineItemDetail.getPackageSpecialServicesRequested()
                    .getBatteryClassifications())) {
                List<com.fedex.rscs.client.srrs.dto.BatteryClassificationType> batteyClass = new ArrayList<>();
                for (BatteryClassificationType batteryClassificationType : packageLineItemDetail
                        .getPackageSpecialServicesRequested()
                        .getBatteryClassifications()) {
                    batteyClass.add(com.fedex.rscs.client.srrs.dto.BatteryClassificationType
                            .valueOf(batteryClassificationType.name()));
                }
                requestedPackageLineItems.setBatteryClassifications(batteyClass);
            }

            requestedPackageLineItemList.add(requestedPackageLineItems);
        }
        return requestedPackageLineItemList;
    }

    /**
     * Method to create weight
     * 
     * @param weightDetail
     * @return
     */
    private Weight createWeight(
            WeightDetail weightDetail) {

        Weight weight = new Weight();
        weight.setUnits(Units.valueOf(weightDetail.getUnit()));
        weight.setValue(weightDetail.getValue());
        return weight;
    }

    /**
     * Method to create special service
     * 
     * @param signatureOption
     * @return
     */
    private List<SpecialServiceDescription> createSpecialService(
            RequestedPackageSpecialServices specialService) {

        List<SpecialServiceDescription> specialServiceDescriptionList = new ArrayList<>();
        
        if (!ObjectUtils.isEmpty(specialService) && !ObjectUtils.isEmpty(specialService.getSpecialServicesRequested())) {
            for(SpecialServiceDescriptionDetail details: specialService.getSpecialServicesRequested()) {
                SpecialServiceDescription specialServiceDescription = new SpecialServiceDescription();
                specialServiceDescription.setDescription(details.getDescription());
                specialServiceDescription.setDisplayText(details.getDisplayText());
                specialServiceDescription.setSpecialServiceSubType(details.getSpecialServiceSubType());
                specialServiceDescription.setSpecialServiceType(details.getSpecialServiceType());
                specialServiceDescriptionList.add(specialServiceDescription);
            }
        }
        
        return specialServiceDescriptionList;
    }

    /**
     * Method to create insured value
     * 
     * @param insuredValue
     * @return
     */
    private Price createInsuredValue(
            InsuredValueDetail insuredValue) {

        Price price = new Price();
        price.setAmount(Double.parseDouble(insuredValue.getAmount()));
        price.setCurrency(Currency.valueOf(insuredValue.getCurrency()));
        return price;
    }

    /**
     * Method to create dimension
     * 
     * @param dimensionDetail
     * @return
     */
    private Dimensions createDimension(
            DimensionDetail dimensionDetail) {

        Dimensions dimension = new Dimensions();
        dimension.setHeight(dimensionDetail.getHeight()
                .toString());
        dimension.setLength(dimensionDetail.getLength()
                .toString());
        dimension.setWidth(dimensionDetail.getWidth()
                .toString());
        if (!ObjectUtils.isEmpty(dimensionDetail.getUnits())) {
            dimension.setUnits(DimensionUnits.valueOf(dimensionDetail.getUnits()));
        }
        return dimension;
    }

    /**
     * Method to create party
     * 
     * @param recipient
     * @return
     */
    private Party createParty(
            PartyDetail recipient) {

        Party party = new Party();
        party.setAddress(createAddress(recipient.getAddress()));
        party.setContact(createContact(recipient.getContact()));
        return party;
    }

    /**
     * Method to create contact
     * 
     * @param contact
     * @return
     */
    private ShippingContact createContact(
            ContactDetail contact) {

        ShippingContact shippingContact = null;
        if (!ObjectUtils.isEmpty(contact)) {
            shippingContact = new ShippingContact();
            shippingContact.setCompany(new Company(contact.getCompanyName()));
            shippingContact.setEmailDetails(new EmailDetail(contact.getEmailId()));
            shippingContact.setPersonName(contact.getFullName());
            shippingContact.setPhoneNumberDetails(createPhoneNumber(contact));
        }
        return shippingContact;
    }

    /**
     * Method to create phone number
     * 
     * @param contact
     * @return
     */
    private List<PhoneNumberDetail> createPhoneNumber(
            ContactDetail contact) {

        List<PhoneNumberDetail> phoneNumberDetails=new ArrayList<>();
        PhoneNumberDetail phoneNumberDetail = null;
        phoneNumberDetail = new PhoneNumberDetail();
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setExtension(contact.getPhoneExtension());
        phoneNumber.setNumber(contact.getPhoneNumber());
        phoneNumberDetail.setPhoneNumber(phoneNumber);
        if (!ObjectUtils.isEmpty(contact.getPhoneUsage())) {
            phoneNumberDetail.setUsage(Usage.valueOf(contact.getPhoneUsage()));
        }
        phoneNumberDetails.add(phoneNumberDetail);
        return phoneNumberDetails;
    }

    /**
     * Method to create payment detail
     * 
     * @param paymentDetail
     * @param interlineShippingData
     * @return
     */
    private ShippingPaymentOption createPaymentDetails(
            PaymentDetail paymentDetail,
            InterlineShippingData interlineShippingData) {

        ShippingPaymentOption shippingPaymentDetail = null;
        if (!ObjectUtils.isEmpty(paymentDetail)) {
            shippingPaymentDetail = new ShippingPaymentOption();
            shippingPaymentDetail.setPaymentType(PaymentType.valueOf(paymentDetail.getPaymentType()));
            if (!ObjectUtils.isEmpty(paymentDetail.getResponsibleParty())) {
                shippingPaymentDetail
                        .setResponsibleParty(ResponsibleParty.valueOf(paymentDetail.getResponsibleParty()));
            }
            if (!ObjectUtils.isEmpty(paymentDetail.getPayor())) {
                shippingPaymentDetail.setAccountNumber(paymentDetail.getPayor()
                        .getAccountNumber());
            }
            if (interlineShippingData != null && interlineShippingData.getInterlineId() != null) {
                shippingPaymentDetail.setInterlineId(interlineShippingData.getInterlineId());
            }
        }
        return shippingPaymentDetail;
    }

    /**
     * Method to create address
     * 
     * @param addressDetail
     * @return
     */
    private Address createAddress(
            AddressDetail addressDetail) {

        Address address = null;
        if (!ObjectUtils.isEmpty(addressDetail)) {

            address = new Address();
            address.setCity(addressDetail.getCity());
            address.setCountryCode(addressDetail.getCountryCode());
            address.setPostalCode(addressDetail.getPostalCode());
            address.setStateOrProvinceCode(addressDetail.getStateOrProvinceCode());
            address.setStreetLines(addressDetail.getStreetLines());
            // set the address classification to HOME if recipient address is residential address.
            address.setAddressClassification(
                    addressDetail.isResidential() ? AddressClassification.HOME : AddressClassification.BUSINESS);

        }
        return address;
    }

    /**
     * Method to create location detail
     * 
     * @param requestedShipmentDetail
     * @return
     */
    private LocationDetail createLocationDetail(
            RequestedShipmentDetail requestedShipmentDetail) {

        LocationDetail locationDetail = new LocationDetail();
        if (!ObjectUtils.isEmpty(requestedShipmentDetail.getOrigin())) {

            locationDetail.setExpressAfterPickup(requestedShipmentDetail.isExpressAfterPickup());
            locationDetail.setGroundAfterPickup(requestedShipmentDetail.isGroundAfterPickup());
            locationDetail.setCityCenterAccountNumber(requestedShipmentDetail.getOrigin()
                    .getAccountNumber());
            locationDetail.setTransactionLocationInfo(createLocationInfo(requestedShipmentDetail));
        }

        return locationDetail;
    }

    /**
     * Method to create location info
     * 
     * @param requestedShipmentDetail
     * @return
     */
    private LocationInfo createLocationInfo(
            RequestedShipmentDetail requestedShipmentDetail) {

        LocationInfo locationInfo = new LocationInfo();
        locationInfo.setCode(requestedShipmentDetail.getTransactionLocationCode());
        locationInfo.setAddress(SRRSClientMapper.INSTANCE.toDto(requestedShipmentDetail.getOrigin()
                .getAddress()));
        return locationInfo;
    }

    /**
     * This method will set the home delivery details for rate quote call
     * 
     * @param homeDeliveryDetail
     * @return
     */
    private HomeDeliveryDetail createHomeDeliveryDetail(
            HomeDelvDetail homeDelvDetail) {

        HomeDeliveryDetail homeDeliveryDetail = null;
        if (homeDelvDetail != null) {
            homeDeliveryDetail = new HomeDeliveryDetail();
            homeDeliveryDetail.setHomeDeliveryType(homeDelvDetail.getHomeDeliveryType());
            homeDeliveryDetail.setDeliveryDate(homeDelvDetail.getDeliveryDate());
            PhoneNumber phoneNumber = null;
            if (homeDelvDetail.getPhoneNumber() != null) {
                phoneNumber = new PhoneNumber();
                phoneNumber.setNumber(homeDelvDetail.getPhoneNumber()
                        .getNumber());
                phoneNumber.setExtension(homeDelvDetail.getPhoneNumber()
                        .getExtension());
            }
            homeDeliveryDetail.setPhoneNumber(phoneNumber);
        }

        return homeDeliveryDetail;
    }
}
