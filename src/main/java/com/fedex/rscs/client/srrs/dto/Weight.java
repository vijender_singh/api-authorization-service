package com.fedex.rscs.client.srrs.dto;

/**
 * Define the Weight DTO
 * 
 * @author 3932968
 *
 */
public class Weight {

    private Units units;
    private String value;
    private WeightSource measurementType;

    public Weight() {}

    /**
     * @param units
     * @param value
     * @param measurementType
     */
    public Weight(Units units, String value, WeightSource measurementType) {

        this.units = units;
        this.value = value;
        this.measurementType = measurementType;
    }

    /**
     * @return the units
     */
    public Units getUnits() {

        return units;
    }

    /**
     * @param units the units to set
     */
    public void setUnits(
            Units units) {

        this.units = units;
    }

    /**
     * @return the value
     */
    public String getValue() {

        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(
            String value) {

        this.value = value;
    }

    /**
     * @return the measurementType
     */
    public WeightSource getMeasurementType() {

        return measurementType;
    }

    /**
     * @param measurementType the measurementType to set
     */
    public void setMeasurementType(
            WeightSource measurementType) {

        this.measurementType = measurementType;
    }

    @Override
    public String toString() {

        return "Weight [units=" + units + ", value=" + value + ", measurementType=" + measurementType + "]";
    }

}
