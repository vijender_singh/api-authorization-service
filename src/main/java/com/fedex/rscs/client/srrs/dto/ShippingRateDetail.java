package com.fedex.rscs.client.srrs.dto;

import java.util.List;

import com.fedex.rscs.model.common.ShippingRateSurcharge;

public class ShippingRateDetail {

    private Currency currency;
    private RateOption ratingType;
    private double totalBaseCharge;
    private double totalRebates;
    private double totalNetCharges;
    private double totalSurcharges;
    private double totalTaxes;
    private double totalFreightDiscounts;
    private double totalNetFreight;
    private double totalNetFedExCharge;
    private double totalDutiesAndTaxes;
    private double totalAncillaryFeesAndTaxes;
    private double totalDutiesTaxesAndFees;
    private double totalNetChargeWithDutiesAndTaxes;
    private List<ShippingRateSurcharge> surcharges;

    public ShippingRateDetail() {}

    /**
     * @return the currency
     */
    public Currency getCurrency() {

        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(
            Currency currency) {

        this.currency = currency;
    }

    /**
     * @return the ratingType
     */
    public RateOption getRatingType() {

        return ratingType;
    }

    /**
     * @param ratingType the ratingType to set
     */
    public void setRatingType(
            RateOption ratingType) {

        this.ratingType = ratingType;
    }

    /**
     * @return the totalBaseCharge
     */
    public double getTotalBaseCharge() {

        return totalBaseCharge;
    }

    /**
     * @param totalBaseCharge the totalBaseCharge to set
     */
    public void setTotalBaseCharge(
            double totalBaseCharge) {

        this.totalBaseCharge = totalBaseCharge;
    }

    /**
     * @return the totalRebates
     */
    public double getTotalRebates() {

        return totalRebates;
    }

    /**
     * @param totalRebates the totalRebates to set
     */
    public void setTotalRebates(
            double totalRebates) {

        this.totalRebates = totalRebates;
    }

    /**
     * @return the totalNetCharges
     */
    public double getTotalNetCharges() {

        return totalNetCharges;
    }

    /**
     * @param totalNetCharges the totalNetCharges to set
     */
    public void setTotalNetCharges(
            double totalNetCharges) {

        this.totalNetCharges = totalNetCharges;
    }

    /**
     * @return the totalSurcharges
     */
    public double getTotalSurcharges() {

        return totalSurcharges;
    }

    /**
     * @param totalSurcharges the totalSurcharges to set
     */
    public void setTotalSurcharges(
            double totalSurcharges) {

        this.totalSurcharges = totalSurcharges;
    }

    /**
     * @return the totalTaxes
     */
    public double getTotalTaxes() {

        return totalTaxes;
    }

    /**
     * @param totalTaxes the totalTaxes to set
     */
    public void setTotalTaxes(
            double totalTaxes) {

        this.totalTaxes = totalTaxes;
    }


    /**
     * @return the totalFreightDiscounts
     */
    public double getTotalFreightDiscounts() {

        return totalFreightDiscounts;
    }

    /**
     * @param totalFreightDiscounts the totalFreightDiscounts to set
     */
    public void setTotalFreightDiscounts(
            double totalFreightDiscounts) {

        this.totalFreightDiscounts = totalFreightDiscounts;
    }

    /**
     * @return the totalNetFreight
     */
    public double getTotalNetFreight() {

        return totalNetFreight;
    }

    /**
     * @param totalNetFreight the totalNetFreight to set
     */
    public void setTotalNetFreight(
            double totalNetFreight) {

        this.totalNetFreight = totalNetFreight;
    }

    /**
     * @return the totalNetFedExCharge
     */
    public double getTotalNetFedExCharge() {

        return totalNetFedExCharge;
    }

    /**
     * @param totalNetFedExCharge the totalNetFedExCharge to set
     */
    public void setTotalNetFedExCharge(
            double totalNetFedExCharge) {

        this.totalNetFedExCharge = totalNetFedExCharge;
    }

    /**
     * @return the totalDutiesAndTaxes
     */
    public double getTotalDutiesAndTaxes() {

        return totalDutiesAndTaxes;
    }

    /**
     * @param totalDutiesAndTaxes the totalDutiesAndTaxes to set
     */
    public void setTotalDutiesAndTaxes(
            double totalDutiesAndTaxes) {

        this.totalDutiesAndTaxes = totalDutiesAndTaxes;
    }

    /**
     * @return the totalAncillaryFeesAndTaxes
     */
    public double getTotalAncillaryFeesAndTaxes() {

        return totalAncillaryFeesAndTaxes;
    }

    /**
     * @param totalAncillaryFeesAndTaxes the totalAncillaryFeesAndTaxes to set
     */
    public void setTotalAncillaryFeesAndTaxes(
            double totalAncillaryFeesAndTaxes) {

        this.totalAncillaryFeesAndTaxes = totalAncillaryFeesAndTaxes;
    }

    /**
     * @return the totalDutiesTaxesAndFees
     */
    public double getTotalDutiesTaxesAndFees() {

        return totalDutiesTaxesAndFees;
    }

    /**
     * @param totalDutiesTaxesAndFees the totalDutiesTaxesAndFees to set
     */
    public void setTotalDutiesTaxesAndFees(
            double totalDutiesTaxesAndFees) {

        this.totalDutiesTaxesAndFees = totalDutiesTaxesAndFees;
    }

    /**
     * @return the totalNetChargeWithDutiesAndTaxes
     */
    public double getTotalNetChargeWithDutiesAndTaxes() {

        return totalNetChargeWithDutiesAndTaxes;
    }

    /**
     * @param totalNetChargeWithDutiesAndTaxes the totalNetChargeWithDutiesAndTaxes to set
     */
    public void setTotalNetChargeWithDutiesAndTaxes(
            double totalNetChargeWithDutiesAndTaxes) {

        this.totalNetChargeWithDutiesAndTaxes = totalNetChargeWithDutiesAndTaxes;
    }

    /**
     * @return the surcharges
     */
    public List<ShippingRateSurcharge> getSurcharges() {

        return surcharges;
    }

    /**
     * @param surcharges the surcharges to set
     */
    public void setSurcharges(
            List<ShippingRateSurcharge> surcharges) {

        this.surcharges = surcharges;
    }

    @Override
    public String toString() {

        return "ShippingRateDetail [currency=" + currency + ", ratingType=" + ratingType + ", totalBaseCharge="
                + totalBaseCharge + ", totalRebates=" + totalRebates + ", totalNetCharges=" + totalNetCharges
                + ", totalSurcharges=" + totalSurcharges + ", totalTaxes=" + totalTaxes + ", totalFreightDiscounts="
                + totalFreightDiscounts + ", totalNetFreight=" + totalNetFreight + ", totalNetFedExCharge="
                + totalNetFedExCharge + ", totalDutiesAndTaxes=" + totalDutiesAndTaxes + ", totalAncillaryFeesAndTaxes="
                + totalAncillaryFeesAndTaxes + ", totalDutiesTaxesAndFees=" + totalDutiesTaxesAndFees
                + ", totalNetChargeWithDutiesAndTaxes=" + totalNetChargeWithDutiesAndTaxes + ", surcharges="
                + surcharges + "]";
    }
}
