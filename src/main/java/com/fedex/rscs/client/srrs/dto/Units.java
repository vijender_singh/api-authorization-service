package com.fedex.rscs.client.srrs.dto;

/**
 * Enum to hold units
 * 
 * @author 3932968
 *
 */
public enum Units {
    LB, KG;
}
