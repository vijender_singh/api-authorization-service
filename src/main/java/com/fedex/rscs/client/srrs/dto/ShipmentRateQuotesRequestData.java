package com.fedex.rscs.client.srrs.dto;

/**
 * Shipment Rate Quotes Request
 * 
 * @author 3900094
 *
 */
public class ShipmentRateQuotesRequestData {

    private RequestHeaderDetail headerDetails;
    private LocationDetail locationDetails;
    private RequestedShipment requestedShipment;
    private ShippingPaymentOption paymentOption;
    private ShippingRateControls rateControlParameters;


    public ShipmentRateQuotesRequestData() {}

    /**
     * @param headerDetails
     * @param locationDetails
     * @param requestedShipment
     * @param paymentOption
     * @param rateControlParameters
     */
    public ShipmentRateQuotesRequestData(RequestHeaderDetail headerDetails, LocationDetail locationDetails,
            RequestedShipment requestedShipment, ShippingPaymentOption paymentOption,
            ShippingRateControls rateControlParameters) {

        this.headerDetails = headerDetails;
        this.locationDetails = locationDetails;
        this.requestedShipment = requestedShipment;
        this.paymentOption = paymentOption;
        this.rateControlParameters = rateControlParameters;
    }

    /**
     * @return the headerDetails
     */
    public RequestHeaderDetail getHeaderDetails() {

        return headerDetails;
    }

    /**
     * @param headerDetails the headerDetails to set
     */
    public void setHeaderDetails(
            RequestHeaderDetail headerDetails) {

        this.headerDetails = headerDetails;
    }

    /**
     * @return the locationDetails
     */
    public LocationDetail getLocationDetails() {

        return locationDetails;
    }

    /**
     * @param locationDetails the locationDetails to set
     */
    public void setLocationDetails(
            LocationDetail locationDetails) {

        this.locationDetails = locationDetails;
    }

    /**
     * @return the requestedShipment
     */
    public RequestedShipment getRequestedShipment() {

        return requestedShipment;
    }

    /**
     * @param requestedShipment the requestedShipment to set
     */
    public void setRequestedShipment(
            RequestedShipment requestedShipment) {

        this.requestedShipment = requestedShipment;
    }

    /**
     * @return the paymentOption
     */
    public ShippingPaymentOption getPaymentOption() {

        return paymentOption;
    }

    /**
     * @param paymentOption the paymentOption to set
     */
    public void setPaymentOption(
            ShippingPaymentOption paymentOption) {

        this.paymentOption = paymentOption;
    }

    /**
     * @return the rateControlParameters
     */
    public ShippingRateControls getRateControlParameters() {

        return rateControlParameters;
    }

    /**
     * @param rateControlParameters the rateControlParameters to set
     */
    public void setRateControlParameters(
            ShippingRateControls rateControlParameters) {

        this.rateControlParameters = rateControlParameters;
    }

    @Override
    public String toString() {

        return "ShipmentRateQuotesRequestData [headerDetails=" + headerDetails + ", locationDetails=" + locationDetails
                + ", requestedShipment=" + requestedShipment + ", paymentOption=" + paymentOption
                + ", rateControlParameters=" + rateControlParameters + "]";
    }

}
