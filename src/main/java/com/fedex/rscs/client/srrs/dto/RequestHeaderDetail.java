package com.fedex.rscs.client.srrs.dto;

/**
 * Represents the DTO for CoreRequestHeaderDetail
 * 
 * @author 3900094
 */
public class RequestHeaderDetail {

    private String guid;
    private String teamMemberId;
    private String requestDateTime;
    private WorkstationDetails workstationDetails;

    public RequestHeaderDetail() {}

    /**
     * @param guid
     * @param teamMemberId
     * @param requestDateTime
     * @param workstationDetails
     */
    public RequestHeaderDetail(String guid, String teamMemberId, String requestDateTime,
            WorkstationDetails workstationDetails) {

        this.guid = guid;
        this.teamMemberId = teamMemberId;
        this.requestDateTime = requestDateTime;
        this.workstationDetails = workstationDetails;
    }

    /**
     * @return the guid
     */
    public String getGuid() {

        return guid;
    }

    /**
     * @param guid the guid to set
     */
    public void setGuid(
            String guid) {

        this.guid = guid;
    }

    /**
     * @return the teamMemberId
     */
    public String getTeamMemberId() {

        return teamMemberId;
    }

    /**
     * @param teamMemberId the teamMemberId to set
     */
    public void setTeamMemberId(
            String teamMemberId) {

        this.teamMemberId = teamMemberId;
    }

    /**
     * @return the requestDateTime
     */
    public String getRequestDateTime() {

        return requestDateTime;
    }

    /**
     * @param requestDateTime the requestDateTime to set
     */
    public void setRequestDateTime(
            String requestDateTime) {

        this.requestDateTime = requestDateTime;
    }

    /**
     * @return the workstationDetails
     */
    public WorkstationDetails getWorkstationDetails() {

        return workstationDetails;
    }

    /**
     * @param workstationDetails the workstationDetails to set
     */
    public void setWorkstationDetails(
            WorkstationDetails workstationDetails) {

        this.workstationDetails = workstationDetails;
    }

    @Override
    public String toString() {

        return "RequestHeaderDetail [guid=" + guid + ", teamMemberId=" + teamMemberId + ", requestDateTime="
                + requestDateTime + ", workstationDetails=" + workstationDetails + "]";
    }

}
