package com.fedex.rscs.client.srrs.dto;

/**
 * Commit Detail DTO
 * 
 * @author 3932956
 *
 */
public class CommitDetail {

    private String commitTimestamp;
    private String dayOfWeek;

    public CommitDetail() {

    }

    /**
     * @return the commitTimestamp
     */
    public String getCommitTimestamp() {

        return commitTimestamp;
    }

    /**
     * @param commitTimestamp the commitTimestamp to set
     */
    public void setCommitTimestamp(
            String commitTimestamp) {

        this.commitTimestamp = commitTimestamp;
    }

    /**
     * @return the dayOfWeek
     */
    public String getDayOfWeek() {

        return dayOfWeek;
    }

    /**
     * @param dayOfWeek the dayOfWeek to set
     */
    public void setDayOfWeek(
            String dayOfWeek) {

        this.dayOfWeek = dayOfWeek;
    }

    @Override
    public String toString() {

        return "CommitDetail [commitTimestamp=" + commitTimestamp + ", dayOfWeek=" + dayOfWeek + "]";
    }

}
