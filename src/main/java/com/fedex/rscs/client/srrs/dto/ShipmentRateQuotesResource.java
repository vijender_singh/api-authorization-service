package com.fedex.rscs.client.srrs.dto;

import java.util.List;

/**
 * ShipmentRateQuotesResource DTO
 * 
 * @author 3932956
 *
 */
public class ShipmentRateQuotesResource {

    private String quoteDate;
    private List<ShippingRateOption> rateOptions;

    public ShipmentRateQuotesResource() {}

    /**
     * @return the quoteDate
     */
    public String getQuoteDate() {

        return quoteDate;
    }

    /**
     * @param quoteDate the quoteDate to set
     */
    public void setQuoteDate(
            String quoteDate) {

        this.quoteDate = quoteDate;
    }

    /**
     * @return the rateOptions
     */
    public List<ShippingRateOption> getRateOptions() {

        return rateOptions;
    }

    /**
     * @param rateOptions the rateOptions to set
     */
    public void setRateOptions(
            List<ShippingRateOption> rateOptions) {

        this.rateOptions = rateOptions;
    }

    @Override
    public String toString() {

        return "ShipmentRateQuotesResource [quoteDate=" + quoteDate + ", rateOptions=" + rateOptions + "]";
    }

}
