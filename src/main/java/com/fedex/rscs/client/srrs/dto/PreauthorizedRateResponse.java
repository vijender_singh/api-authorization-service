package com.fedex.rscs.client.srrs.dto;

import com.fedex.common.cxs.dto.CXSOutput;

/**
 * Wrapper of PreauthorizedRateResource DTO
 * 
 * @author Samicheen Khariwal
 *
 */
public class PreauthorizedRateResponse extends CXSOutput {

    private PreauthorizedRateResource preauthorizedRate;

    public PreauthorizedRateResponse() {}

    /**
     * @return the preauthorizedRate
     */
    public PreauthorizedRateResource getPreauthorizedRate() {
    
        return preauthorizedRate;
    }
    
    /**
     * @param preauthorizedRate the preauthorizedRate to set
     */
    public void setPreauthorizedRate(
            PreauthorizedRateResource preauthorizedRate) {
    
        this.preauthorizedRate = preauthorizedRate;
    }

    @Override
    public String toString() {

        return "PreauthorizedRateResponse [preauthorizedRate=" + preauthorizedRate + "]";
    }
}
