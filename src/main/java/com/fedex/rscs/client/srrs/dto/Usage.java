package com.fedex.rscs.client.srrs.dto;

/**
 * ENUM to hold Usage .
 * 
 * @author 3932968
 *
 */
public enum Usage {
    PRIMARY, SECONDARY;
}
