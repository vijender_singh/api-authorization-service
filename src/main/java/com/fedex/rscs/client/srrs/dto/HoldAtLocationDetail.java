package com.fedex.rscs.client.srrs.dto;

import com.fedex.rscs.dto.LocationType;

/**
 * Define the HoldAtLocationDetail DTO
 * 
 * @author 3932968
 *
 */
public class HoldAtLocationDetail {

    private String locationId;
    private Address address;
    private Company company;
    private PhoneNumberDetail phoneNumber;
    private LocationType locationType;
    private String displayName;

    public HoldAtLocationDetail() {}

    /**
     * @param locationId
     * @param address
     * @param company
     * @param phoneNumber
     * @param locationType
     * @param displayName
     */
    public HoldAtLocationDetail(String locationId, Address address, Company company, PhoneNumberDetail phoneNumber,
            LocationType locationType, String displayName) {

        this.locationId = locationId;
        this.address = address;
        this.company = company;
        this.phoneNumber = phoneNumber;
        this.locationType = locationType;
        this.displayName = displayName;
    }



    /**
     * 
     * @return locationId
     */
    public String getLocationId() {

        return locationId;
    }

    /**
     * 
     * @param locationId to set locationId
     */
    public void setLocationId(
            String locationId) {

        this.locationId = locationId;
    }

    /**
     * 
     * @return address
     */
    public Address getAddress() {

        return address;
    }

    /**
     * 
     * @param address to set address
     */
    public void setAddress(
            Address address) {

        this.address = address;
    }

    /**
     * @return the company
     */
    public Company getCompany() {

        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(
            Company company) {

        this.company = company;
    }

    /**
     * @return the phoneNumber
     */
    public PhoneNumberDetail getPhoneNumber() {

        return phoneNumber;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(
            PhoneNumberDetail phoneNumber) {

        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the locationType
     */
    public LocationType getLocationType() {

        return locationType;
    }

    /**
     * @param locationType the locationType to set
     */
    public void setLocationType(
            LocationType locationType) {

        this.locationType = locationType;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {

        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(
            String displayName) {

        this.displayName = displayName;
    }

    @Override
    public String toString() {

        return "HoldAtLocationDetail [locationId=" + locationId + ", address=" + address + ", company=" + company
                + ", phoneNumber=" + phoneNumber + ", locationType=" + locationType + ", displayName=" + displayName
                + "]";
    }

}
