package com.fedex.rscs.client.srrs.dto;

/**
 * Enum to hold weight sources
 * 
 * @author 3932968
 *
 */
public enum WeightSource {
    MANUAL, SCALE;
}
