package com.fedex.rscs.client.srrs;

import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.cshp.PreauthorizedRateData;
import com.fedex.rscs.model.cshp.ShippingRateData;

/**
 * This interface provides the necessary methods to communicate to Shipping Retail Rating service.
 * 
 * @author 3900094
 *
 */
public interface SRRSClient {

    /**
     * Method for calling SRRS rate call.
     * 
     * @param workStationDetail
     * @param requestedShipmentDetail
     * @return
     */
    ShippingRateData getRateOptions(
            WorkstationDetail workStationDetail,
            RequestedShipmentDetail requestedShipmentDetail);
    
    /**
     * Method for calling SRRS preauthorized rate call.
     * 
     * @param workStationDetail
     * @param requestedShipmentDetail
     * @return
     */
    PreauthorizedRateData getPreauthorizedRates(
            WorkstationDetail workStationDetail,
            RequestedShipmentDetail requestedShipmentDetail);

}
