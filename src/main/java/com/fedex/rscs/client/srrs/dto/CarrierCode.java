package com.fedex.rscs.client.srrs.dto;

/**
 * Represents the CarrierCode Enum
 * 
 * @author 3932968
 *
 */
public enum CarrierCode {
    EXPRESS, GROUND;
}
