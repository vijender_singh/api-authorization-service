package com.fedex.rscs.client.srrs.dto;

/**
 * Define the Phone Number DTO
 * 
 * @author 3932968
 *
 */
public class PhoneNumber {

    private String number;
    private String extension;

    public PhoneNumber() {}

    /**
     * 
     * @param number
     * @param extension
     */
    public PhoneNumber(String number, String extension) {

        this.number = number;
        this.extension = extension;
    }

    /**
     * 
     * @return number
     */
    public String getNumber() {

        return number;
    }

    /**
     * 
     * @param number to set number
     */
    public void setNumber(
            String number) {

        this.number = number;
    }

    /**
     * 
     * @return extension
     */
    public String getExtension() {

        return extension;
    }

    /**
     * 
     * @param extension to set extension
     */
    public void setExtension(
            String extension) {

        this.extension = extension;
    }

    @Override
    public String toString() {

        return "PhoneNumber [number=" + number + ", extension=" + extension + "]";
    }

}
