package com.fedex.rscs.client.srrs.dto;

import java.util.List;

/**
 * Define the Requested Shipment DTO
 * 
 * @author 3932968
 *
 */
public class RequestedShipment {

    private String serviceType;
    private String packagingType;
    private Party shipper;
    private Party recipient;
    private ShipmentSpecialServiceDescription specialServicesRequested;
    private List<RequestedPackageLineItem> requestedPackageLineItems;

    public RequestedShipment() {}

    /**
     * @param serviceType
     * @param packagingType
     * @param shipper
     * @param recipient
     * @param specialServicesRequested
     * @param requestedPackageLineItems
     */
    public RequestedShipment(String serviceType, String packagingType, Party shipper, Party recipient,
            ShipmentSpecialServiceDescription specialServicesRequested,
            List<RequestedPackageLineItem> requestedPackageLineItems) {

        this.serviceType = serviceType;
        this.packagingType = packagingType;
        this.shipper = shipper;
        this.recipient = recipient;
        this.specialServicesRequested = specialServicesRequested;
        this.requestedPackageLineItems = requestedPackageLineItems;
    }

    /**
     * 
     * @return serviceType
     */
    public String getServiceType() {

        return serviceType;
    }

    /**
     * 
     * @param serviceType to set serviceType
     */
    public void setServiceType(
            String serviceType) {

        this.serviceType = serviceType;
    }

    /**
     * 
     * @return packagingType
     */
    public String getPackagingType() {

        return packagingType;
    }

    /**
     * 
     * @param packagingType to set packagingType
     */
    public void setPackagingType(
            String packagingType) {

        this.packagingType = packagingType;
    }

    /**
     * 
     * @return shipper
     */
    public Party getShipper() {

        return shipper;
    }

    /**
     * 
     * @param shipper to set shipper
     */
    public void setShipper(
            Party shipper) {

        this.shipper = shipper;
    }

    /**
     * 
     * @return recipient
     */
    public Party getRecipient() {

        return recipient;
    }

    /**
     * 
     * @param recipient to set recipient
     */
    public void setRecipient(
            Party recipient) {

        this.recipient = recipient;
    }

    /**
     * 
     * @return specialServicesRequested
     */
    public ShipmentSpecialServiceDescription getSpecialServicesRequested() {

        return specialServicesRequested;
    }

    /**
     * 
     * @param specialServicesRequested to set specialServicesRequested
     */
    public void setSpecialServicesRequested(
            ShipmentSpecialServiceDescription specialServicesRequested) {

        this.specialServicesRequested = specialServicesRequested;
    }

    /**
     * 
     * @return requestedPackageLineItems
     */
    public List<RequestedPackageLineItem> getRequestedPackageLineItems() {

        return requestedPackageLineItems;
    }

    /**
     * 
     * @param requestedPackageLineItems to set requestedPackageLineItems
     */
    public void setRequestedPackageLineItems(
            List<RequestedPackageLineItem> requestedPackageLineItems) {

        this.requestedPackageLineItems = requestedPackageLineItems;
    }

    @Override
    public String toString() {

        return "RequestedShipment [serviceType=" + serviceType + ", packagingType=" + packagingType + ", shipper="
                + shipper + ", recipient=" + recipient + ", specialServicesRequested=" + specialServicesRequested
                + ", requestedPackageLineItems=" + requestedPackageLineItems + "]";
    }

}
