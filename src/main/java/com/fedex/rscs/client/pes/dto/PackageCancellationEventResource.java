package com.fedex.rscs.client.pes.dto;

/**
 * Represents Package Cancellation Event Resource DTO
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackageCancellationEventResource {
    
    private String trackingNumber;
    private boolean cancelled;
    private Notification eventNotification;
    
    public PackageCancellationEventResource() {}
    
    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
    
        return trackingNumber;
    }
    
    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {
    
        this.trackingNumber = trackingNumber;
    }
    
    /**
     * @return the cancelled
     */
    public boolean isCancelled() {
    
        return cancelled;
    }
    
    /**
     * @param cancelled the cancelled to set
     */
    public void setCancelled(
            boolean cancelled) {
    
        this.cancelled = cancelled;
    }
    
    /**
     * @return the eventNotification
     */
    public Notification getEventNotification() {
    
        return eventNotification;
    }
    
    /**
     * @param eventNotification the eventNotification to set
     */
    public void setEventNotification(
            Notification eventNotification) {
    
        this.eventNotification = eventNotification;
    }

    @Override
    public String toString() {

        return "PackageCancellationEventResource [trackingNumber=" + trackingNumber + ", cancelled=" + cancelled
                + ", eventNotification=" + eventNotification + "]";
    }
    
}
