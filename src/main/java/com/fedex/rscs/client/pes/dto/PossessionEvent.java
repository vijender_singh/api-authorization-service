package com.fedex.rscs.client.pes.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the premetered shipment transaction details DTO.
 * It is mapped to Model's {@link com.fedex.rscs.model.common.PackageTransactionLineItem} object.
 * 
 * @author Shakti.Saurabh
 *
 */
public class PossessionEvent {

    private String trackingNumber;
    private boolean accepted;
    private List<Notification> eventNotification = new ArrayList<>();

    public PossessionEvent() {}
            
    /**
     * @param trackingNumber
     * @param accepted
     * @param eventNotification
     */
    public PossessionEvent(String trackingNumber, boolean accepted, List<Notification> eventNotification) {

        this.trackingNumber = trackingNumber;
        this.accepted = accepted;
        this.eventNotification = eventNotification;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {

        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {

        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the accepted
     */
    public boolean isAccepted() {

        return accepted;
    }

    /**
     * @param accepted the accepted to set
     */
    public void setAccepted(
            boolean accepted) {

        this.accepted = accepted;
    }

    /**
     * @return the eventNotification
     */
    public List<Notification> getEventNotification() {
    
        return eventNotification;
    }
    
    /**
     * @param eventNotification the eventNotification to set
     */
    public void setEventNotification(
            List<Notification> eventNotification) {
    
        this.eventNotification = eventNotification;
    }

    @Override
    public String toString() {

        return "PremeteredTransactionDetail [trackingNumber=" + trackingNumber + ", accepted=" + accepted
                + ", eventNotification=" + eventNotification + "]";
    }

}
