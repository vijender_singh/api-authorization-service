package com.fedex.rscs.client.pes.dto;

import java.util.List;

import com.fedex.rscs.dto.LocationDetail;
import com.fedex.rscs.dto.RequestHeaderDetail;

/**
 * Represents Package Cancellation Event Request Data DTO
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackageCancellationEventRequestData {

    private RequestHeaderDetail headerDetails;
    private LocationDetail locationDetails;
    private List<PackageLineItem> packageLineItems;

    public PackageCancellationEventRequestData() {}

    /**
     * @param headerDetails
     * @param locationDetails
     * @param packageLineItems
     */
    public PackageCancellationEventRequestData(RequestHeaderDetail headerDetails, LocationDetail locationDetails,
            List<PackageLineItem> packageLineItems) {

        this.headerDetails = headerDetails;
        this.locationDetails = locationDetails;
        this.packageLineItems = packageLineItems;
    }

    /**
     * @return the headerDetails
     */
    public RequestHeaderDetail getHeaderDetails() {

        return headerDetails;
    }

    /**
     * @param headerDetails the headerDetails to set
     */
    public void setHeaderDetails(
            RequestHeaderDetail headerDetails) {

        this.headerDetails = headerDetails;
    }

    /**
     * @return the locationDetails
     */
    public LocationDetail getLocationDetails() {

        return locationDetails;
    }

    /**
     * @param locationDetails the locationDetails to set
     */
    public void setLocationDetails(
            LocationDetail locationDetails) {

        this.locationDetails = locationDetails;
    }

    /**
     * @return the packageLineItems
     */
    public List<PackageLineItem> getPackageLineItems() {

        return packageLineItems;
    }

    /**
     * @param packageLineItems the packageLineItems to set
     */
    public void setPackageLineItems(
            List<PackageLineItem> packageLineItems) {

        this.packageLineItems = packageLineItems;
    }

    @Override
    public String toString() {

        return "PackageCancellationEventRequestData [headerDetails=" + headerDetails + ", locationDetails="
                + locationDetails + ", packageLineItems=" + packageLineItems + "]";
    }

}
