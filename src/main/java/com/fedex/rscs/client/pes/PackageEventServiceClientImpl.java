package com.fedex.rscs.client.pes;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.HttpStatusCodeException;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import com.fedex.common.cxs.dto.CXSEnvelope;
import com.fedex.rscs.client.pes.dto.Notification;
import com.fedex.rscs.client.pes.dto.PackageCancellationEventRequest;
import com.fedex.rscs.client.pes.dto.PackageCancellationEventRequestData;
import com.fedex.rscs.client.pes.dto.PackageCancellationEventResponse;
import com.fedex.rscs.client.pes.dto.PackagePossessionEventRequest;
import com.fedex.rscs.client.pes.dto.PackagePossessionEventRequestData;
import com.fedex.rscs.client.pes.dto.PackagePossessionEventResource;
import com.fedex.rscs.client.pes.mapper.PackageEventServiceClientMapper;
import com.fedex.rscs.client.util.RetailShipmentCreationServiceClientUtil;
import com.fedex.rscs.config.AppConfig;
import com.fedex.rscs.exception.ErrorCategory;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.ShipmentServiceErrorCode;
import com.fedex.rscs.exception.client.ClientTerminalException;
import com.fedex.rscs.model.common.PackageCancellationEventDetailRequest;
import com.fedex.rscs.model.common.PackagePossessionEventDetailRequest;


/**
 * This class provides the implementation for the Package event client in order to communicate with
 * Package event Service and post the Package Possession Events.
 * 
 * @author Shakti.Saurabh
 *
 */
@Service
@Profile("!mock")
public class PackageEventServiceClientImpl implements PackageEventServiceClient {

    private static final Logger logger = LoggerFactory.getLogger(PackageEventServiceClientImpl.class);
    private static final String PACKAGE_POSSESSION_EVENT_ENDPOINT_URI = "/retailshipping/fedexoffice/v1/packagepossessionevents";
    private static final String PACKAGE_CANCELLATION_EVENT_ENDPOINT_URI = "/retailshipping/fedexoffice/v1/packagecancellationevents";

    private final AppConfig appConfig;
    private final OAuth2RestTemplate restTemplate;
    private final RetailShipmentCreationServiceClientUtil clientUtil;

    @Autowired
    public PackageEventServiceClientImpl(final OAuth2RestTemplate restTemplate, final AppConfig appConfig,
            final RetailShipmentCreationServiceClientUtil clientUtil) {

        this.restTemplate = restTemplate;
        this.appConfig = appConfig;
        this.clientUtil = clientUtil;
    }

    /**
     * This method post PackagePossessionEvents on PESClient which can be further used for other
     * fast services call .
     * 
     * @param PackagePossessionEventDetailResource
     * @return
     */
    @Override
    @HystrixCommand(groupKey = "PackageEventServiceClientImpl", commandKey = "postPackagePossessionEvents")

    public boolean postPackagePossessionEvents(
            PackagePossessionEventDetailRequest packagePossessionEventDetailRequest) {

        logger.info("Execution started for service endpoint: {} ", PACKAGE_POSSESSION_EVENT_ENDPOINT_URI);
        String packageEventServiceEndpoint = appConfig.getPesServiceUri() + PACKAGE_POSSESSION_EVENT_ENDPOINT_URI;
        try {
            PackagePossessionEventRequestData packagePossessionEventRequestData =
                    PackageEventServiceClientMapper.INSTANCE.toDto(packagePossessionEventDetailRequest);

            logger.debug("Request for RPES packagePossessionEvents: {} ", packagePossessionEventRequestData);
            ResponseEntity<CXSEnvelope<PackagePossessionEventResource>> response =
                    restTemplate.exchange(packageEventServiceEndpoint, HttpMethod.POST,
                            new HttpEntity<PackagePossessionEventRequest>(
                                    new PackagePossessionEventRequest(packagePossessionEventRequestData)),
                            new ParameterizedTypeReference<CXSEnvelope<PackagePossessionEventResource>>() {});

            if (response.getStatusCode() == HttpStatus.OK && response.getBody() != null && response.getBody()
                    .getOutput() != null) {

                logger.debug("Response received from RPES packagePossessionEvents: {} ", response.getBody());
                if (!ObjectUtils.isEmpty(response.getBody()
                        .getOutput()
                        .getLineItems())
                        && response.getBody()
                                .getOutput()
                                .getLineItems()
                                .get(0)
                                .isAccepted()) {
                    return true;
                } else {
                    logger.error("Response : {} not as expected when PackagePossessionEventDetailRequest : {}",
                            response, packagePossessionEventDetailRequest);
                    throw new ClientTerminalException(processPESErrorResponse(response.getBody()
                            .getOutput()
                            .getLineItems()
                            .get(0)
                            .getEventNotification()));
                }

            } else {
                logger.error("Response : {} not as expected when PackagePossessionEventDetailRequest : {}", response,
                        packagePossessionEventDetailRequest);
                throw new ClientTerminalException(ServiceErrorCode.PES_SERVICE_UNAVAILABLE);
            }

        } catch (HttpStatusCodeException httpException) {
            logger.error("failed to process the request {} due to ", packagePossessionEventDetailRequest,
                    httpException);
            throw new ClientTerminalException(clientUtil.processErrorResponse(httpException));
        } catch (RuntimeException runTimeException) {
            logger.error("failed to process the request {} due to ", packagePossessionEventDetailRequest,
                    runTimeException);
            throw new ClientTerminalException(ServiceErrorCode.PES_SERVICE_UNAVAILABLE);
        }
    }
    
    /**
     * This method post PackageCancellationEvent on PESClient for cancellation which can be further
     * used for other fast services call .
     * 
     * @param PackageCancellationEventDetailRequest
     * @return
     */
    @Override
    @HystrixCommand(groupKey = "PackageEventServiceClientImpl", commandKey = "postPackageCancellationEvents")
    public boolean postPackageCancellationEvents(
            PackageCancellationEventDetailRequest packageCancellationEventDetailRequest) {

        logger.info("Execution started for service endpoint: {} ", PACKAGE_CANCELLATION_EVENT_ENDPOINT_URI);
        String packageEventServiceEndpoint = appConfig.getPesServiceUri() + PACKAGE_CANCELLATION_EVENT_ENDPOINT_URI;
        try {
            PackageCancellationEventRequestData packageCancellationEventRequestData =
                    PackageEventServiceClientMapper.INSTANCE.toDto(packageCancellationEventDetailRequest);

            logger.debug("Request for RPES packageCancellationEvents: {} ", packageCancellationEventRequestData);
            ResponseEntity<CXSEnvelope<PackageCancellationEventResponse>> response =
                    restTemplate.exchange(packageEventServiceEndpoint, HttpMethod.POST,
                            new HttpEntity<PackageCancellationEventRequest>(
                                    new PackageCancellationEventRequest(packageCancellationEventRequestData)),
                            new ParameterizedTypeReference<CXSEnvelope<PackageCancellationEventResponse>>() {});

            if (response.getStatusCode() == HttpStatus.OK && !ObjectUtils.isEmpty(response.getBody()
                    .getOutput())) {

                logger.debug("Response received from RPES packageCancellationEvents: {} ", response.getBody());
                if (!ObjectUtils.isEmpty(response.getBody()
                        .getOutput()
                        .getPackageCancellationEvents())
                        && response.getBody()
                                .getOutput()
                                .getPackageCancellationEvents()
                                .get(0)
                                .isCancelled()) {
                    return true;
                } else {
                    logger.error("Response : {} not as expected when packageCancellationEventDetailRequest : {}",
                            response, packageCancellationEventDetailRequest);
                    throw new ClientTerminalException(processPESErrorResponse(response.getBody()
                            .getOutput()
                            .getPackageCancellationEvents()
                            .get(0)
                            .getEventNotification()));
                }

            } else {
                logger.error("Response : {} not as expected when PackageCancellationEventDetailRequest : {}", response,
                        packageCancellationEventDetailRequest);
                throw new ClientTerminalException(processPESErrorResponse(response.getBody()
                        .getOutput()
                        .getPackageCancellationEvents()
                        .get(0)
                        .getEventNotification()));
            }

        } catch (HttpStatusCodeException httpException) {
            logger.error("failed to process the request due to ", httpException);
            throw new ClientTerminalException(clientUtil.processErrorResponse(httpException));
        } catch (RuntimeException runTimeException) {
            logger.warn("Runtime exception occured when PackageCancellationEventDetailRequest : {}",
                    packageCancellationEventDetailRequest);
            logger.error("failed to process the request due to ", runTimeException);
            throw new ClientTerminalException(ServiceErrorCode.PES_SERVICE_UNAVAILABLE);
        }
    }

    
    /**
     * This method processes PES error
     * 
     * @param eventNotification
     * @return
     */
    private ShipmentServiceErrorCode processPESErrorResponse(
            List<Notification> eventNotification) {

        logger.info("processing PES error response....");
        ShipmentServiceErrorCode errorCode = null;
        if (!ObjectUtils.isEmpty(eventNotification)) {

            errorCode = new ShipmentServiceErrorCode(eventNotification.get(0)
                    .getCode(),
                    eventNotification.get(0)
                            .getMessage(),
                    ErrorCategory.INTERNAL_SERVER_ERROR);
        }
        return errorCode;
    }
    
    /**
     * This method processes PES error
     * 
     * @param eventNotification
     * @return
     */
    private ShipmentServiceErrorCode processPESErrorResponse(
            Notification eventNotification) {

        logger.info("processing PES error response....");
        ShipmentServiceErrorCode errorCode = null;
        if (!ObjectUtils.isEmpty(eventNotification)) {

            errorCode = new ShipmentServiceErrorCode(eventNotification.getCode(), eventNotification.getMessage(),
                    ErrorCategory.INTERNAL_SERVER_ERROR);
        }
        return errorCode;
    }
}
