package com.fedex.rscs.client.pes.dto;

/**
 * Object representing barcode and tracking number
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackageIdentifier {

    private String trackingNumber;

    private String barcode;

    public PackageIdentifier() {}

    /**
     * Parameterized Constructor
     * 
     * @param trackingNumber
     * @param barcode
     */
    public PackageIdentifier(String trackingNumber, String barcode) {

        this.trackingNumber = trackingNumber;
        this.barcode = barcode;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {

        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {

        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the barcode
     */
    public String getBarcode() {

        return barcode;
    }

    /**
     * @param barcode the barcode to set
     */
    public void setBarcode(
            String barcode) {

        this.barcode = barcode;
    }

    @Override
    public String toString() {

        return "PackageIdentifier [trackingNumber=" + trackingNumber + ", barcode=" + barcode + ",]";
    }

}
