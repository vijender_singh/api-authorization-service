package com.fedex.rscs.client.pes.dto;

/**
 * Represents Package Cancellation Event Request DTO
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackageCancellationEventRequest {

    private PackageCancellationEventRequestData packageCancellationEventRequest;

    public PackageCancellationEventRequest() {}

    /**
     * @param packageCancellationEventRequest
     */
    public PackageCancellationEventRequest(PackageCancellationEventRequestData packageCancellationEventRequest) {

        this.packageCancellationEventRequest = packageCancellationEventRequest;
    }

    /**
     * @return the packageCancellationEventRequest
     */
    public PackageCancellationEventRequestData getPackageCancellationEventRequest() {

        return packageCancellationEventRequest;
    }

    /**
     * @param packageCancellationEventRequest the packageCancellationEventRequest to set
     */
    public void setPackageCancellationEventRequest(
            PackageCancellationEventRequestData packageCancellationEventRequest) {

        this.packageCancellationEventRequest = packageCancellationEventRequest;
    }

    @Override
    public String toString() {

        return "PackageCancellationEventRequest [packageCancellationEventRequest=" + packageCancellationEventRequest
                + "]";
    }

}
