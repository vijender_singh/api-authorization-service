package com.fedex.rscs.client.pes.dto;

/**
 * Enum for PAA indicator type
 * @author Shakti.Saurabh
 *
 */
public enum PAAIndicatorType {

    CUSTOMER("NO_KNOWN_DAMAGE", "NOT_INSPECTED", "CUSTOMER"), 
    FEDEX_OFFICE("NO_KNOWN_DAMAGE", "NOT_INSPECTED", "FEDEX_OFFICE"), 
    FEDEX_DAMAGE_KNOWN("DAMAGE_KNOWN", "NOT_INSPECTED", "FEDEX_OFFICE"), 
    INSPECTED("NO_KNOWN_DAMAGE", "INSPECTED", "CUSTOMER"), 
    INSPECTED_DAMAGE_KNOWN("DAMAGE_KNOWN", "INSPECTED", "CUSTOMER"), 
    CUSTOMER_REFUSED_INSPECTION("NO_KNOWN_DAMAGE", "CUSTOMER_REFUSED_INSPECTION", "CUSTOMER"), 
    REFUSED_INSPECTION_DAMAGE_KNOWN("DAMAGE_KNOWN", "CUSTOMER_REFUSED_INSPECTION", "CUSTOMER");

    private String damageStatus;
    private String inspectionStatus;
    private String packedBy;

    PAAIndicatorType(String damageStatus, String inspectionStatus, String packedBy) {

        this.damageStatus = damageStatus;
        this.inspectionStatus = inspectionStatus;
        this.packedBy = packedBy;
    }

    /**
     * @return the damageStatus
     */
    public String getDamageStatus() {
    
        return damageStatus;
    }
    
    /**
     * @return the inspectionStatus
     */
    public String getInspectionStatus() {
    
        return inspectionStatus;
    }
    
    /**
     * @return the packedBy
     */
    public String getPackedBy() {
    
        return packedBy;
    }

}
