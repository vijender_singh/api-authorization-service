package com.fedex.rscs.client.pes.dto;

import java.util.List;

import com.fedex.common.cxs.dto.CXSOutput;

/**
 * Represents Package Cancellation Event Response DTO
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackageCancellationEventResponse extends CXSOutput {
    
    private List<PackageCancellationEventResource> packageCancellationEvents;
    
    public PackageCancellationEventResponse() {
        
        super();
    }

    /**
     * @return the packageCancellationEvents
     */
    public List<PackageCancellationEventResource> getPackageCancellationEvents() {
    
        return packageCancellationEvents;
    }
    
    /**
     * @param packageCancellationEvents the packageCancellationEvents to set
     */
    public void setPackageCancellationEvents(
            List<PackageCancellationEventResource> packageCancellationEvents) {
    
        this.packageCancellationEvents = packageCancellationEvents;
    }

    @Override
    public String toString() {

        return "PackageCancellationEventResponse [packageCancellationEvents=" + packageCancellationEvents + "]";
    }

}
