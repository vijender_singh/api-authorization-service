package com.fedex.rscs.client.pes.mapper;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.fedex.rscs.client.pes.dto.PackageCancellationEventRequestData;
import com.fedex.rscs.client.pes.dto.PackagePossessionEventRequestData;
import com.fedex.rscs.dto.Address;
import com.fedex.rscs.dto.AddressClassification;
import com.fedex.rscs.model.AddressDetail;
import com.fedex.rscs.model.common.PackageCancellationEventDetailRequest;
import com.fedex.rscs.model.common.PackagePossessionEventDetailRequest;

/**
 * Mapper to handle the conversion of package event service request and response.
 * 
 * @author Shakti.Saurabh
 *
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PackageEventServiceClientMapper {

    PackageEventServiceClientMapper INSTANCE = Mappers.getMapper(PackageEventServiceClientMapper.class);
    
    /**
     * Mapper to convert {@link PackagePossessionEventDetailRequest} model to
     * {@link PackagePossessionEventRequestData} dto
     * 
     * @param PackagePossessionEventDetailRequest
     * @return
     */
    public PackagePossessionEventRequestData toDto(
            PackagePossessionEventDetailRequest packagePossessionEventDetailRequest);

    /**
     * Mapper to convert {@link PackageCancellationEventDetailRequest} model to
     * {@link PackageCancellationEventRequestData} dto
     * 
     * @param PackageCancellationEventDetailRequest
     * @return
     */
    public PackageCancellationEventRequestData toDto(
            PackageCancellationEventDetailRequest packageCancellationEventDetailRequest);

    /**
     * To convert enum into string
     * 
     * @param custEnum
     * @return
     */
    default String enumToString(
            Enum<?> custEnum) {

        if (custEnum == null) {
            return null;
        }
        return custEnum.toString();
    }

    /**
     * To convert AddressDetail into Address
     * 
     * @param AddressDetail
     * @return
     */
    default Address addressDetailToAddress(
            AddressDetail addressDetail) {

        if (addressDetail == null) {
            return null;
        }

        Address address = new Address();

        List<String> list = addressDetail.getStreetLines();
        if (list != null) {
            address.setStreetLines(new ArrayList<String>(list));
        }
        address.setCity(addressDetail.getCity());
        address.setStateOrProvinceCode(addressDetail.getStateOrProvinceCode());
        address.setPostalCode(addressDetail.getPostalCode()
                .replace("-", ""));
        address.setCountryCode(addressDetail.getCountryCode());
        address.setAddressClassification(
                addressDetail.isResidential() ? AddressClassification.HOME : AddressClassification.BUSINESS);

        return address;
    }

}
