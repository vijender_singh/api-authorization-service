package com.fedex.rscs.client.pes.dto;

import java.util.List;

import com.fedex.rscs.dto.LocationDetail;
import com.fedex.rscs.dto.RequestHeaderDetail;

/**
 * 
 * Represents Package Possession Event Request DTO It is mapped to Model's
 * {@link com.fedex.rscs.model.common.} object.
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackagePossessionEventRequestData {

    private RequestHeaderDetail headerDetails;
    private LocationDetail locationDetails;
    private ShippingPaymentDetail paymentDetails;
    private List<PackagePossessionEventDetail> packageLineItems;

    public PackagePossessionEventRequestData() {}

    /**
     * 
     * @param headerDetails
     * @param locationDetails
     * @param paymentDetails
     * @param packageLineItems
     */
    public PackagePossessionEventRequestData(final RequestHeaderDetail headerDetails,
            final LocationDetail locationDetails, final ShippingPaymentDetail paymentDetails,
            List<PackagePossessionEventDetail> packageLineItems) {

        this.locationDetails = locationDetails;
        this.headerDetails = headerDetails;
        this.paymentDetails = paymentDetails;
        this.packageLineItems = packageLineItems;
    }

    /**
     * 
     * @return headerDetails
     */
    public RequestHeaderDetail getHeaderDetails() {

        return headerDetails;
    }

    /**
     * 
     * @param headerDetails set headerDetails
     */
    public void setHeaderDetails(
            RequestHeaderDetail headerDetails) {

        this.headerDetails = headerDetails;
    }

    /**
     * @return locationDetails
     */
    public LocationDetail getLocationDetails() {

        return locationDetails;
    }

    /**
     * @param locationDetails set locationDetails
     */
    public void setLocationDetails(
            LocationDetail locationDetails) {

        this.locationDetails = locationDetails;
    }

    /**
     * @return paymentDetails
     */
    public ShippingPaymentDetail getPaymentDetails() {

        return paymentDetails;
    }

    /**
     * @param paymentDetails set paymentDetails
     */
    public void setPaymentDetails(
            ShippingPaymentDetail paymentDetails) {

        this.paymentDetails = paymentDetails;
    }

    /**
     * @return the packageLineItems
     */
    public List<PackagePossessionEventDetail> getPackageLineItems() {

        return packageLineItems;
    }

    /**
     * @param packageLineItems the packageLineItems to set
     */
    public void setPackageLineItems(
            List<PackagePossessionEventDetail> packageLineItems) {

        this.packageLineItems = packageLineItems;
    }

    @Override
    public String toString() {

        return "PackagePossessionEventRequest [headerDetails=" + headerDetails + ", locationDetails=" + locationDetails
                + ", paymentDetails=" + paymentDetails + ", packageLineItems=" + packageLineItems + "]";
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + ((headerDetails == null) ? 0 : headerDetails.hashCode());
        result = prime * result + ((locationDetails == null) ? 0 : locationDetails.hashCode());
        result = prime * result + ((paymentDetails == null) ? 0 : paymentDetails.hashCode());
        result = prime * result + ((packageLineItems == null) ? 0 : packageLineItems.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(
            Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof PackagePossessionEventRequestData))
            return false;
        PackagePossessionEventRequestData other = (PackagePossessionEventRequestData) obj;
        if (headerDetails == null) {
            if (other.headerDetails != null)
                return false;
        } else if (!headerDetails.equals(other.headerDetails))
            return false;
        if (locationDetails == null) {
            if (other.locationDetails != null)
                return false;
        } else if (!locationDetails.equals(other.locationDetails))
            return false;
        if (paymentDetails == null) {
            if (other.paymentDetails != null)
                return false;
        } else if (!paymentDetails.equals(other.paymentDetails))
            return false;
        if (packageLineItems == null) {
            if (other.packageLineItems != null)
                return false;
        } else if (!packageLineItems.equals(other.packageLineItems))
            return false;
        return true;
    }

}

