package com.fedex.rscs.client.pes.dto;

/**
 * Represent service type of the shipment It is mapped to Model's
 * {@link com.fedex.rscs.model.OfferingDetail} object.
 * 
 * @author Shakti.Saurabh
 *
 */
public class OfferingIdentifierDetail {

    private String id;
    private String type;
    private String code;

    public OfferingIdentifierDetail() {}

    /**
     * @param id
     * @param type
     * @param code
     */
    public OfferingIdentifierDetail(String id, String type, String code) {
        
        this.id = id;
        this.type = type;
        this.code = code;
    }

    /**
     * @return the id
     */
    public String getId() {

        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(
            String id) {

        this.id = id;
    }

    /**
     * @return the type
     */
    public String getType() {

        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(
            String type) {

        this.type = type;
    }

    /**
     * @return the code
     */
    public String getCode() {

        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(
            String code) {

        this.code = code;
    }
    
    @Override
    public String toString() {

        return "OfferingIdentifierDetail [id=" + id + ", type=" + type + ", code=" + code + "]";
    }
}
