package com.fedex.rscs.client.pes.dto;

/**
 * 
 * Represents Package Possession Event Request DTO
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackagePossessionEventRequest {

    private PackagePossessionEventRequestData packagePossessionEventRequest;

    public PackagePossessionEventRequest() {}

    /**
     * @param packagePossessionEventRequest
     */
    public PackagePossessionEventRequest(PackagePossessionEventRequestData packagePossessionEventRequest) {

        this.packagePossessionEventRequest = packagePossessionEventRequest;
    }

    /**
     * @return the packagePossessionEventRequest
     */
    public PackagePossessionEventRequestData getPackagePossessionEventRequest() {

        return packagePossessionEventRequest;
    }

    /**
     * @param packagePossessionEventRequest the packagePossessionEventRequest to set
     */
    public void setPackagePossessionEventRequest(
            PackagePossessionEventRequestData packagePossessionEventRequest) {

        this.packagePossessionEventRequest = packagePossessionEventRequest;
    }

    @Override
    public String toString() {

        return "PackagePossessionEventRequestData [packagePossessionEventRequest=" + packagePossessionEventRequest
                + "]";
    }
}

