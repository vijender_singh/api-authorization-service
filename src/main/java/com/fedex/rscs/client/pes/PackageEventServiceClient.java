package com.fedex.rscs.client.pes;

import com.fedex.rscs.model.common.PackageCancellationEventDetailRequest;
import com.fedex.rscs.model.common.PackagePossessionEventDetailRequest;

/**
 * This interface provides the necessary methods to communicate with Package event service.
 * 
 * @author Shakti.Saurabh
 *
 */
public interface PackageEventServiceClient {

    /**
     * This method post PackagePossessionEvents on PESClient .
     * 
     * @param PackagePossessionEventDetailResource
     * @return isScanPostSuccess
     */
    boolean postPackagePossessionEvents(
            PackagePossessionEventDetailRequest packagePossessionEventDetailRequest);

    /**
     * This method post PackageCancellationEvent for cancellation on PESClient .
     * 
     * @param PackageCancellationEventDetailRequest
     * @return isCancellationSuccess
     */
    boolean postPackageCancellationEvents(
            PackageCancellationEventDetailRequest packageCancellationEventDetailRequest);
}
