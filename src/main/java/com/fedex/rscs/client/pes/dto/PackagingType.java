package com.fedex.rscs.client.pes.dto;

/**
 * The package type for the shipments
 * 
 * @author Shakti.Saurabh
 *
 */
public enum PackagingType {
    COUNTER_SHIPMENT, PREMETERED_SHIPMENT
}


