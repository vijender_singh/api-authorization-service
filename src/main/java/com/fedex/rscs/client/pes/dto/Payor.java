package com.fedex.rscs.client.pes.dto;

import com.fedex.rscs.dto.AccountType;
import com.fedex.rscs.dto.CreditCard;

/**
 * Represents the Payor DTO
 * 
 * @author Shakti.Saurabh
 *
 */
public class Payor {

    private AccountType accountType;
    private String accountNumber;
    private CreditCard creditCard;

    public Payor() {}

    /**
     * @param accountType
     * @param accountNumber
     * @param creditCard
     */
    public Payor(AccountType accountType, String accountNumber, CreditCard creditCard) {

        this.accountType = accountType;
        this.accountNumber = accountNumber;
        this.creditCard = creditCard;
    }

    /**
     * @return the accountType
     */
    public AccountType getAccountType() {

        return accountType;
    }

    /**
     * @param accountType the accountType to set
     */
    public void setAccountType(
            AccountType accountType) {

        this.accountType = accountType;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {

        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(
            String accountNumber) {

        this.accountNumber = accountNumber;
    }

    /**
     * @return the creditCard
     */
    public CreditCard getCreditCard() {

        return creditCard;
    }

    /**
     * @param creditCard the creditCard to set
     */
    public void setCreditCard(
            CreditCard creditCard) {

        this.creditCard = creditCard;
    }
}
