package com.fedex.rscs.client.pes.dto;

import java.util.List;

/**
 * Represents Package Cancellation Event Response DTO
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackageCancellationEventResponseDetail  {
    
    private List<PackageCancellationEventResource> packageCancellationEvents;
    
    public PackageCancellationEventResponseDetail() { }

    /**
     * @return the packageCancellationEvents
     */
    public List<PackageCancellationEventResource> getPackageCancellationEvents() {
    
        return packageCancellationEvents;
    }
    
    /**
     * @param packageCancellationEvents the packageCancellationEvents to set
     */
    public void setPackageCancellationEvents(
            List<PackageCancellationEventResource> packageCancellationEvents) {
    
        this.packageCancellationEvents = packageCancellationEvents;
    }

    @Override
    public String toString() {

        return "PackageCancellationEventResponse [packageCancellationEvents=" + packageCancellationEvents + "]";
    }

}
