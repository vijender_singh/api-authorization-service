package com.fedex.rscs.client.pes.dto;

import java.util.List;

/**
 * Represents Package Line Item DTO
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackageLineItem {

    private PackagingType packageType;
    private PackageIdentifier packageIdentifier;
    private PersonName customerName;
    private List<PackageIdentifier> childPackages;

    public PackageLineItem() {}

    /**
     * @param packageType
     * @param packageIdentifier
     * @param customerName
     * @param childPackages
     */
    public PackageLineItem(PackagingType packageType, PackageIdentifier packageIdentifier, PersonName customerName,
            List<PackageIdentifier> childPackages) {

        this.packageType = packageType;
        this.packageIdentifier = packageIdentifier;
        this.customerName = customerName;
        this.childPackages = childPackages;
    }

    /**
     * @return the packageType
     */
    public PackagingType getPackageType() {

        return packageType;
    }

    /**
     * @param packageType the packageType to set
     */
    public void setPackageType(
            PackagingType packageType) {

        this.packageType = packageType;
    }

    /**
     * @return the packageIdentifier
     */
    public PackageIdentifier getPackageIdentifier() {

        return packageIdentifier;
    }

    /**
     * @param packageIdentifier the packageIdentifier to set
     */
    public void setPackageIdentifier(
            PackageIdentifier packageIdentifier) {

        this.packageIdentifier = packageIdentifier;
    }

    /**
     * @return the customerName
     */
    public PersonName getCustomerName() {

        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(
            PersonName customerName) {

        this.customerName = customerName;
    }

    /**
     * @return the childPackages
     */
    public List<PackageIdentifier> getChildPackages() {

        return childPackages;
    }

    /**
     * @param childPackages the childPackages to set
     */
    public void setChildPackages(
            List<PackageIdentifier> childPackages) {

        this.childPackages = childPackages;
    }

    @Override
    public String toString() {

        return "PackageLineItem [packageType=" + packageType + ", packageIdentifier=" + packageIdentifier
                + ", customerName=" + customerName + ", childPackages=" + childPackages + "]";
    }

}
