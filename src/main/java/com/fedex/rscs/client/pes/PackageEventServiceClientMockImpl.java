package com.fedex.rscs.client.pes;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.client.ClientTerminalException;
import com.fedex.rscs.model.common.PackageCancellationEventDetailRequest;
import com.fedex.rscs.model.common.PackagePossessionEventDetailRequest;

/**
 * Mock implementation for Package event service.
 * 
 * @author Shakti.Saurabh
 *
 */
@Service
@Profile("mock")
public class PackageEventServiceClientMockImpl implements PackageEventServiceClient {

    private static final Logger logger = LoggerFactory.getLogger(PackageEventServiceClientMockImpl.class);
    private final ObjectMapper objectMapper;

    @Autowired
    public PackageEventServiceClientMockImpl(final ObjectMapper objectMapper) {

        this.objectMapper = objectMapper;
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * This method is mock implementation of PESClient method.
     * 
     * @param packagePossessionEventRequest
     */
    @Override
    public boolean postPackagePossessionEvents(
            PackagePossessionEventDetailRequest packagePossessionEventDetailRequest) {

        logger.info("Execution for mock RPES PackagePossessionEvents call is started..... ");
        boolean scanPost = false;

        if (!ObjectUtils.isEmpty(packagePossessionEventDetailRequest)
                && (!ObjectUtils.isEmpty(packagePossessionEventDetailRequest.getPaymentDetails())
                        && (!StringUtils.isEmpty(packagePossessionEventDetailRequest.getPaymentDetails()
                                .getPaymentType()))
                        && packagePossessionEventDetailRequest.getPaymentDetails()
                                .getPaymentType()
                                .equalsIgnoreCase("CASH"))) {

            logger.error("Mock: Throwing Client Terminal Exception due to PaymentType=CASH");
            throw new ClientTerminalException(ServiceErrorCode.PES_SERVICE_UNAVAILABLE);

        }

        if (!ObjectUtils.isEmpty(packagePossessionEventDetailRequest)
                && (!ObjectUtils.isEmpty(packagePossessionEventDetailRequest.getHeaderDetails())
                        && !StringUtils.isEmpty(packagePossessionEventDetailRequest.getHeaderDetails()
                                .getTeamMemberId())
                        && !StringUtils.isNumeric(packagePossessionEventDetailRequest.getHeaderDetails()
                                .getTeamMemberId()))) {

            scanPost = true;

        }

        return scanPost;
    }

    /**
     * This method is mock implementation of PESClient cancellation method.
     * 
     * @param packageCancellationEventDetailRequest
     */
    @Override
    public boolean postPackageCancellationEvents(
            PackageCancellationEventDetailRequest packageCancellationEventDetailRequest) {

        logger.info("Execution for mock RPES PackageCancellationEvents call is started..... ");
        boolean scanPost = false;

        if (!ObjectUtils.isEmpty(packageCancellationEventDetailRequest)
                && (!ObjectUtils.isEmpty(packageCancellationEventDetailRequest.getHeaderDetails())
                        && !StringUtils.isEmpty(packageCancellationEventDetailRequest.getHeaderDetails()
                                .getTeamMemberId())
                        && !StringUtils.isNumeric(packageCancellationEventDetailRequest.getHeaderDetails()
                                .getTeamMemberId()))) {

            if ("3900090".equalsIgnoreCase(packageCancellationEventDetailRequest.getHeaderDetails()
                    .getTeamMemberId())) {
                logger.error("Mock: Throwing Client Terminal Exception due to teamMemberId=3900090");
                throw new ClientTerminalException(ServiceErrorCode.PES_SERVICE_UNAVAILABLE);
            }
            scanPost = true;

        }

        return scanPost;
    }

}
