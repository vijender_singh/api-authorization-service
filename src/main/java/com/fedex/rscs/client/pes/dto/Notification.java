package com.fedex.rscs.client.pes.dto;

import java.util.List;

import com.fedex.rscs.dto.NotificationType;


/**
 * Represents the notifications associated with the transaction processed through the system. It can be
 * used to provide the notifications of NOTE/WARNING/ERROR to support the partial batch processing.
 * It is mapped to Model's {@link com.fedex.rscs.model.common.EventNotification} object.
 * 
 * @author 981840
 *
 */
public class Notification {

    private String code;
    private String message;
    private List<String> instructions;
    private NotificationType notificationType;

    public Notification() {}

    public Notification(String code, String message, NotificationType notificationType) {
        this(code, message, notificationType, null);
    }

    public Notification(String code, String message, NotificationType notificationType, List<String> instructions) {
        super();
        this.code = code;
        this.message = message;
        this.instructions = instructions;
        this.notificationType = notificationType;
    }

    /**
     * get notification code.
     * 
     * @return code
     */
    public String getCode() {

        return code;
    }

    /**
     * set notification code
     * 
     * @param code
     */
    public void setCode(
            String code) {

        this.code = code;
    }

    /**
     * get message
     * 
     * @return message
     */
    public String getMessage() {

        return message;
    }

    /**
     * set the notification message
     * 
     * @param message
     */
    public void setMessage(
            String message) {

        this.message = message;
    }

    /**
     * get notification type
     * 
     * @return notificationType
     */
    public NotificationType getNotificationType() {

        return notificationType;
    }

    /**
     * set the notification type
     * 
     * @param notificationType
     */
    public void setNotificationType(
            NotificationType notificationType) {

        this.notificationType = notificationType;
    }

    /**
     * get the additional instructions associated with the notification.
     * 
     * @return
     */
    public List<String> getInstructions() {

        return instructions;
    }


    /**
     * set the additional instructions associated with the notification.
     * 
     * @param instructions
     */
    public void setInstructions(
            List<String> instructions) {

        this.instructions = instructions;
    }

    @Override
    public String toString() {

        return "notification [code=" + code + ", message=" + message + ", instructions=" + instructions + ", notificationType="
                + notificationType + "]";
    }

}
