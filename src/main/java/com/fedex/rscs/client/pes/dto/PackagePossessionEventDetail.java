package com.fedex.rscs.client.pes.dto;

import java.util.List;

import com.fedex.rscs.dto.Address;
import com.fedex.rscs.dto.InterlineShippingDetail;
import com.fedex.rscs.dto.Price;


/**
 * Represent PackagePossessionEventDetail DTO object.
 * 
 * @author Shakti.Saurabh
 */
public class PackagePossessionEventDetail {

    private String barcode;
    private String formId;
    private String operatingCompany;
    private PAAIndicatorType paaIndicator;
    private String trackingNumber;
    private boolean reprintAstra;
    private PackagingType packageType;
    private Address destinationAddress;
    private Price shipmentChargeAmount;
    private OfferingIdentifierDetail service;
    private List<OfferingIdentifierDetail> serviceOptions;
    private String routingId;
    private String serviceAreaCode;
    private String groundAccountNumber;
    private List<MPSChild> childPackages;
    private InterlineShippingDetail interlineShippingDetails;
    private boolean manualBarcodeEntry;

    public PackagePossessionEventDetail() {}

    /**
     * @return barcode
     */
    public String getBarcode() {

        return barcode;
    }

    /**
     * @param barcode the barcode to set
     */
    public void setBarcode(
            String barcode) {

        this.barcode = barcode;
    }

    /**
     * @return formId
     */
    public String getFormId() {

        return formId;
    }

    /**
     * @param formId the formId to set
     */
    public void setFormId(
            String formId) {

        this.formId = formId;
    }

    /**
     * @return operatingCompany
     */
    public String getOperatingCompany() {

        return operatingCompany;
    }

    /**
     * @param operatingCompany the operatingCompany to set
     */
    public void setOperatingCompany(
            String operatingCompany) {

        this.operatingCompany = operatingCompany;
    }

    /**
     * @return paaIndicator
     */
    public PAAIndicatorType getPaaIndicator() {

        return paaIndicator;
    }

    /**
     * @param paaIndicator the paaIndicator to set
     */
    public void setPaaIndicator(
            PAAIndicatorType paaIndicator) {

        this.paaIndicator = paaIndicator;
    }

    /**
     * @return trackingNumber
     */
    public String getTrackingNumber() {

        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {

        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the packageType
     */
    public PackagingType getPackageType() {

        return packageType;
    }

    /**
     * @param packageType the packageType to set
     */
    public void setPackageType(
            PackagingType packageType) {

        this.packageType = packageType;
    }

    /**
     * @return the shipmentChargeAmount
     */
    public Price getShipmentChargeAmount() {

        return shipmentChargeAmount;
    }

    /**
     * @param shipmentChargeAmount the shipmentChargeAmount to set
     */
    public void setShipmentChargeAmount(
            Price shipmentChargeAmount) {

        this.shipmentChargeAmount = shipmentChargeAmount;
    }

    /**
     * @return the groundAccountNumber
     */
    public String getGroundAccountNumber() {

        return groundAccountNumber;
    }

    /**
     * @param groundAccountNumber the groundAccountNumber to set
     */
    public void setGroundAccountNumber(
            String groundAccountNumber) {

        this.groundAccountNumber = groundAccountNumber;
    }

    /**
     * @return the serviceOptions
     */
    public List<OfferingIdentifierDetail> getServiceOptions() {

        return serviceOptions;
    }

    /**
     * @param serviceOptions the serviceOptions to set
     */
    public void setServiceOptions(
            List<OfferingIdentifierDetail> serviceOptions) {

        this.serviceOptions = serviceOptions;
    }

    /**
     * @return the reprintAstra
     */
    public boolean isReprintAstra() {

        return reprintAstra;
    }

    /**
     * @param reprintAstra the reprintAstra to set
     */
    public void setReprintAstra(
            boolean reprintAstra) {

        this.reprintAstra = reprintAstra;
    }

    /**
     * @return OfferingIdentifierDetail
     */
    public OfferingIdentifierDetail getService() {

        return service;
    }

    /**
     * @param serviceArea the serviceArea to set
     */
    public void setService(
            OfferingIdentifierDetail service) {

        this.service = service;
    }

    /**
     * returns the routing ID for the shipment barcode.
     * 
     * @return
     */
    public String getRoutingId() {

        return routingId;
    }

    /**
     * sets the routing ID for the shipment barcode.
     * 
     * @param routingId
     */
    public void setRoutingId(
            String routingId) {

        this.routingId = routingId;
    }

    /**
     * @return the destinationAddress
     */
    public Address getDestinationAddress() {

        return destinationAddress;
    }

    /**
     * @param destinationAddress the destinationAddress to set
     */
    public void setDestinationAddress(
            Address destinationAddress) {

        this.destinationAddress = destinationAddress;
    }

    /**
     * @return the serviceArea
     */
    public String getServiceAreaCode() {

        return serviceAreaCode;
    }

    /**
     * @param serviceArea the serviceArea to set
     */
    public void setServiceAreaCode(
            String serviceAreaCode) {

        this.serviceAreaCode = serviceAreaCode;
    }

    /**
     * @return the childPackages
     */
    public List<MPSChild> getChildPackages() {

        return childPackages;
    }

    /**
     * @param childPackages the childPackages to set
     */
    public void setChildPackages(
            List<MPSChild> childPackages) {

        this.childPackages = childPackages;
    }
       
    /**
     * @return the interlineShippingDetails
     */
    public InterlineShippingDetail getInterlineShippingDetails() {

        return interlineShippingDetails;
    }

    /**
     * @param interlineShippingDetails the interlineShippingDetails to set
     */
    public void setInterlineShippingDetails(
            InterlineShippingDetail interlineShippingDetails) {

        this.interlineShippingDetails = interlineShippingDetails;
    }
    
    /**
     * @return the manualBarcodeEntry
     */
    public boolean isManualBarcodeEntry() {

        return manualBarcodeEntry;
    }

    /**
     * @param manualBarcodeEntry the manualBarcodeEntry to set
     */
    public void setManualBarcodeEntry(
            boolean manualBarcodeEntry) {

        this.manualBarcodeEntry = manualBarcodeEntry;
    }

    @Override
    public String toString() {

        return "PackagePossessionEventDetail [barcode=" + barcode + ", formId=" + formId + ", operatingCompany="
                + operatingCompany + ", paaIndicator=" + paaIndicator + ", trackingNumber=" + trackingNumber
                + ", reprintAstra=" + reprintAstra + ", packageType=" + packageType + ", destinationAddress="
                + destinationAddress + ", shipmentChargeAmount=" + shipmentChargeAmount + ", service=" + service
                + ", serviceOptions=" + serviceOptions + ", routingId=" + routingId + ", serviceAreaCode="
                + serviceAreaCode + ", groundAccountNumber=" + groundAccountNumber + ", childPackages=" + childPackages
                + ", interlineShippingDetails=" + interlineShippingDetails + ", manualBarcodeEntry="
                + manualBarcodeEntry + "]";
    }

}
