package com.fedex.rscs.client.pes.dto;

import java.util.ArrayList;
import java.util.List;

import com.fedex.common.cxs.dto.CXSOutput;

/**
 * Represents the package possession event response DTO.
 * 
 * @author Shakti.Saurabh
 *
 */
public class PackagePossessionEventResource extends CXSOutput {

    private List<PossessionEvent> lineItems = new ArrayList<>();

    public PackagePossessionEventResource() {
        super();
    }

    /**
     * @return the lineItems
     */
    public List<PossessionEvent> getLineItems() {
    
        return lineItems;
    }

    /**
     * @param lineItems the lineItems to set
     */
    public void setLineItems(
            List<PossessionEvent> lineItems) {
    
        this.lineItems = lineItems;
    }

    @Override
    public String toString() {

        return "PackagePossessionEventResource [lineItems=" + lineItems + "]";
    }

}
