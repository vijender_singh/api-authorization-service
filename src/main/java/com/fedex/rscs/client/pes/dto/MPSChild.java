package com.fedex.rscs.client.pes.dto;

/**
 * Represent MPSChild DTO object.
 * 
 * @author Shakti.Saurabh
 */
public class MPSChild {

    private String barcode;
    private String formId;
    private String operatingCompany;
    private PAAIndicatorType paaIndicator;
    private String trackingNumber;
    private boolean reprintAstra;
    private Integer sequence;

    public MPSChild() {}

    /**
     * @param barcode
     * @param formId
     * @param operatingCompany
     * @param paaIndicator
     * @param trackingNumber
     * @param reprintAstra
     * @param sequence
     */
    public MPSChild(String barcode, String formId, String operatingCompany, PAAIndicatorType paaIndicator,
            String trackingNumber, boolean reprintAstra, Integer sequence) {

        this.barcode = barcode;
        this.formId = formId;
        this.operatingCompany = operatingCompany;
        this.paaIndicator = paaIndicator;
        this.trackingNumber = trackingNumber;
        this.reprintAstra = reprintAstra;
        this.sequence = sequence;
    }

    /**
     * @return barcode
     */
    public String getBarcode() {

        return barcode;
    }

    /**
     * @param barcode the barcode to set
     */
    public void setBarcode(
            String barcode) {

        this.barcode = barcode;
    }

    /**
     * @return formId
     */
    public String getFormId() {

        return formId;
    }

    /**
     * @param formId the formId to set
     */
    public void setFormId(
            String formId) {

        this.formId = formId;
    }

    /**
     * @return operatingCompany
     */
    public String getOperatingCompany() {

        return operatingCompany;
    }

    /**
     * @param operatingCompany the operatingCompany to set
     */
    public void setOperatingCompany(
            String operatingCompany) {

        this.operatingCompany = operatingCompany;
    }

    /**
     * @return paaIndicator
     */
    public PAAIndicatorType getPaaIndicator() {

        return paaIndicator;
    }

    /**
     * @param paaIndicator the paaIndicator to set
     */
    public void setPaaIndicator(
            PAAIndicatorType paaIndicator) {

        this.paaIndicator = paaIndicator;
    }

    /**
     * @return trackingNumber
     */
    public String getTrackingNumber() {

        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(
            String trackingNumber) {

        this.trackingNumber = trackingNumber;
    }

    /**
     * @return reprintAstra
     */
    public boolean isReprintAstra() {

        return reprintAstra;
    }

    /**
     * 
     * @param reprintAstra the reprintAstra to set
     */
    public void setReprintAstra(
            boolean reprintAstra) {

        this.reprintAstra = reprintAstra;
    }

    /**
     * @return the sequence
     */
    public Integer getSequence() {
    
        return sequence;
    }

    /**
     * @param sequence the sequence to set
     */
    public void setSequence(
            Integer sequence) {
    
        this.sequence = sequence;
    }

    @Override
    public String toString() {

        return "MPSChild [barcode=" + barcode + ", formId=" + formId + ", operatingCompany=" + operatingCompany
                + ", paaIndicator=" + paaIndicator + ", trackingNumber=" + trackingNumber + ", reprintAstra="
                + reprintAstra + ", sequence=" + sequence + "]";
    }

}
