package com.fedex.rscs.client.cshp;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.fedex.nxgen.ship.v17.ientities.BinaryBarcode;
import com.fedex.nxgen.ship.v17.ientities.CompletedPackageDetail;
import com.fedex.nxgen.ship.v17.ientities.ConfirmOpenShipmentReply;
import com.fedex.nxgen.ship.v17.ientities.CreateOpenShipmentReply;
import com.fedex.nxgen.ship.v17.ientities.ModifyOpenShipmentReply;
import com.fedex.nxgen.ship.v17.ientities.Notification;
import com.fedex.nxgen.ship.v17.ientities.OfferingIdentifierDetail;
import com.fedex.nxgen.ship.v17.ientities.OperationalInstruction;
import com.fedex.nxgen.ship.v17.ientities.PackageBarcodes;
import com.fedex.nxgen.ship.v17.ientities.PackageOperationalDetail;
import com.fedex.nxgen.ship.v17.ientities.PackagingDescription;
import com.fedex.nxgen.ship.v17.ientities.ProductName;
import com.fedex.nxgen.ship.v17.ientities.ServiceDescription;
import com.fedex.nxgen.ship.v17.ientities.ShipmentOperationalDetail;
import com.fedex.nxgen.ship.v17.ientities.ShipmentReply;
import com.fedex.nxgen.ship.v17.ientities.SpecialServiceDescription;
import com.fedex.nxgen.ship.v17.ientities.StringBarcode;
import com.fedex.nxgen.ship.v17.ientities.TrackingId;
import com.fedex.rscs.client.mapper.ShipServiceClientMapper;
import com.fedex.rscs.exception.ErrorCategory;
import com.fedex.rscs.exception.ErrorCode;
import com.fedex.rscs.exception.ServiceErrorCode;
import com.fedex.rscs.exception.ShipmentServiceErrorCode;
import com.fedex.rscs.exception.client.ClientTerminalException;
import com.fedex.rscs.exception.client.ClientTransientException;
import com.fedex.rscs.model.Severity;
import com.fedex.rscs.model.ShippingDocumentType;
import com.fedex.rscs.model.cshp.BinaryBarcodeDetail;
import com.fedex.rscs.model.cshp.CompleteShipmentDetail;
import com.fedex.rscs.model.cshp.CompletedPackageData;
import com.fedex.rscs.model.cshp.OfferingIdentifierData;
import com.fedex.rscs.model.cshp.OperationalInstructionDetail;
import com.fedex.rscs.model.cshp.PackageBarcodeDetail;
import com.fedex.rscs.model.cshp.PackageOperationData;
import com.fedex.rscs.model.cshp.ProductDescription;
import com.fedex.rscs.model.cshp.ProductNameDetail;
import com.fedex.rscs.model.cshp.ShipmentOperationData;
import com.fedex.rscs.model.cshp.ShippingDocument;
import com.fedex.rscs.model.cshp.SpecialServiceDescriptionDetail;
import com.fedex.rscs.model.cshp.StringBarcodeDetail;
import com.fedex.rscs.model.cshp.TrackingIdDetail;
import com.fedex.rscs.util.RetailShipmentCreationServiceUtil;

/**
 * This class used to map cshp response model to service model.
 * 
 * @author 5034922
 *
 */
@Component
public class ShipServiceResponseMapper {
    
    private static final Logger logger = LoggerFactory.getLogger(ShipServiceResponseMapper.class);

    private final RetailShipmentCreationServiceUtil retailShipmentCreationServiceUtil;

    @Autowired
    public ShipServiceResponseMapper(final RetailShipmentCreationServiceUtil retailShipmentCreationServiceUtil) {

        this.retailShipmentCreationServiceUtil = retailShipmentCreationServiceUtil;
    }

    public CompleteShipmentDetail mapToShipmentDetail(
            final CreateOpenShipmentReply createOpenShipmentReply) {

        CompleteShipmentDetail completeShipmentDetail = null;

        // handle the empty or null severity from CSHP.
        if (createOpenShipmentReply == null || createOpenShipmentReply.getHighestSeverity() == null) {
            logger.error("mapToShipmentDetail: CSHP service call is failed due to empty response : {}",
                    createOpenShipmentReply);
            throw new ClientTransientException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);

        } // Handle FAILURE severity of CSHP
        else if (createOpenShipmentReply.getHighestSeverity()
                .equals(Severity.FAILURE.name())) {
            logger.error("mapToShipmentDetail : CSHP service call is failed due to failure severity with response: {}",
                    createOpenShipmentReply);
            throw new ClientTerminalException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);

        } // Consider NOTE, WARNING or SUCCESS severity response from CSHP as success.
        else if (Severity.valueOf(createOpenShipmentReply.getHighestSeverity())
                .isAtOrMoreSevereThan(Severity.NOTE)) {
            if (createOpenShipmentReply.getCompletedShipmentDetail() != null) {
                completeShipmentDetail = new CompleteShipmentDetail();
                completeShipmentDetail.setUsDomestic(createOpenShipmentReply.getCompletedShipmentDetail()
                        .getUsDomestic());
                completeShipmentDetail.setCarrierCode(createOpenShipmentReply.getCompletedShipmentDetail()
                        .getCarrierCode());
                completeShipmentDetail
                        .setMasterTrackingId(createTrackingId(createOpenShipmentReply.getCompletedShipmentDetail()
                                .getMasterTrackingId()));
                completeShipmentDetail.setServiceDescription(
                        createServiceDescription(createOpenShipmentReply.getCompletedShipmentDetail()
                                .getServiceDescription()));
                completeShipmentDetail.setPackagingDescription(
                        createPackagingDescription(createOpenShipmentReply.getCompletedShipmentDetail()
                                .getPackagingDescription()));
                completeShipmentDetail.setSpecialServiceDescriptions(
                        createSpecialServiceDescription(createOpenShipmentReply.getCompletedShipmentDetail()
                                .getSpecialServiceDescriptions()));
                completeShipmentDetail.setOperationalDetail(
                        createOperationalDetail(createOpenShipmentReply.getCompletedShipmentDetail()
                                .getOperationalDetail()));
                completeShipmentDetail.setCompletedPackageDetails(
                        createCompletedPackageDetails(createOpenShipmentReply.getCompletedShipmentDetail()
                                .getCompletedPackageDetails()));
                completeShipmentDetail.setIndex(createOpenShipmentReply.getIndex());
            }
        } // Handle ERROR severity of CSHP
        else {
            logger.error("mapToShipmentDetail : CSHP service call is failed with erroneous response as: {}",
                    createOpenShipmentReply);
            Notification[] notificationList = createOpenShipmentReply.getNotifications();
            if (!ObjectUtils.isEmpty(notificationList)) {
                throw new ClientTerminalException(processClientError(
                        ServiceErrorCode.INVALID_SHIPMENT_DETAILS.getApiErrorCode(), notificationList[0].getMessage(),
                        ServiceErrorCode.INVALID_SHIPMENT_DETAILS.getStatus()));
            }
        }
        return completeShipmentDetail;
    }

    /**
     * Method to build client error to ShipmentCreationServiceErrorCode
     * 
     * @param code
     * @param errorMessage
     * @param errorCategory
     * @return
     */
    private ErrorCode processClientError(
            String code,
            String errorMessage,
            ErrorCategory errorCategory) {

        return new ShipmentServiceErrorCode(code, errorMessage, errorCategory);
    }

    /**
     * Response Mapper for ModifyOpenShipmentReply
     * 
     * @param reply
     * @return
     */
    public CompleteShipmentDetail buildModifyOpenShipmentResponse(
            final ModifyOpenShipmentReply modifyOpenShipmentReply) {

        CompleteShipmentDetail completeShipmentDetail = null;

        // handle the empty or null severity from CSHP.
        if (modifyOpenShipmentReply == null || modifyOpenShipmentReply.getHighestSeverity() == null) {
            logger.error("buildModifyOpenShipmentResponse : CSHP service call is failed due to empty response: {}",
                    modifyOpenShipmentReply);
            throw new ClientTransientException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);

        } // Handle FAILURE severity of CSHP
        else if (modifyOpenShipmentReply.getHighestSeverity()
                .equals(Severity.FAILURE.name())) {
            logger.error(
                    "buildModifyOpenShipmentResponse : CSHP service call is failed due to failure severity with response: {}",
                    modifyOpenShipmentReply);
            throw new ClientTerminalException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);

        } // Consider NOTE, WARNING or SUCCESS severity response from CSHP as success.
        else if (Severity.valueOf(modifyOpenShipmentReply.getHighestSeverity())
                .isAtOrMoreSevereThan(Severity.NOTE)) {

            if (modifyOpenShipmentReply.getCompletedShipmentDetail() != null) {
                completeShipmentDetail = new CompleteShipmentDetail();
                completeShipmentDetail.setUsDomestic(modifyOpenShipmentReply.getCompletedShipmentDetail()
                        .getUsDomestic());
                completeShipmentDetail.setCarrierCode(modifyOpenShipmentReply.getCompletedShipmentDetail()
                        .getCarrierCode());
                completeShipmentDetail
                        .setMasterTrackingId(createTrackingId(modifyOpenShipmentReply.getCompletedShipmentDetail()
                                .getMasterTrackingId()));
                completeShipmentDetail.setServiceDescription(
                        createServiceDescription(modifyOpenShipmentReply.getCompletedShipmentDetail()
                                .getServiceDescription()));
                completeShipmentDetail.setPackagingDescription(
                        createPackagingDescription(modifyOpenShipmentReply.getCompletedShipmentDetail()
                                .getPackagingDescription()));
                completeShipmentDetail.setSpecialServiceDescriptions(
                        createSpecialServiceDescription(modifyOpenShipmentReply.getCompletedShipmentDetail()
                                .getSpecialServiceDescriptions()));
                completeShipmentDetail.setOperationalDetail(
                        createOperationalDetail(modifyOpenShipmentReply.getCompletedShipmentDetail()
                                .getOperationalDetail()));
                completeShipmentDetail.setCompletedPackageDetails(
                        createCompletePackageDetails(modifyOpenShipmentReply.getCompletedShipmentDetail()
                                .getCompletedPackageDetails()));
                completeShipmentDetail.setShipmentDocuments(
                        createShippingDocument(modifyOpenShipmentReply.getCompletedShipmentDetail()
                                .getCompletedPackageDetails()));
                if (!ObjectUtils.isEmpty(modifyOpenShipmentReply.getCompletedShipmentDetail()
                        .getCompletedPackageDetails())) {
                    completeShipmentDetail.setPackageCount(modifyOpenShipmentReply.getCompletedShipmentDetail()
                            .getCompletedPackageDetails().length);
                }

                try {
                    completeShipmentDetail.setPlannedServiceLevel(modifyOpenShipmentReply.getCompletedShipmentDetail()
                            .getOperationalDetail()
                            .getOriginServiceArea());
                } catch (Exception e) {
                    logger.error("OperationalDetail is not present in response received from CHSP Response {}",
                            modifyOpenShipmentReply, e);
                }
            }
        } // Handle ERROR severity of CSHP
        else {
            logger.error("buildModifyOpenShipmentResponse: CSHP service call is failed with erroneous response as: {}",
                    modifyOpenShipmentReply);
            if (!ObjectUtils.isEmpty(modifyOpenShipmentReply.getNotifications())) {
                throw new ClientTerminalException(
                        processClientError(ServiceErrorCode.INVALID_SHIPMENT_DETAILS.getApiErrorCode(),
                                modifyOpenShipmentReply.getNotifications()[0].getMessage(),
                                ServiceErrorCode.INVALID_SHIPMENT_DETAILS.getStatus()));
            }
        }
        return completeShipmentDetail;
    }

    /**
     * Method to return List of CompletedPackageData
     * 
     * @param completedPackageDetails
     * @return List<CompletedPackageData>
     */
    private List<CompletedPackageData> createCompletedPackageDetails(
            CompletedPackageDetail[] completedPackageDetails) {

        List<CompletedPackageData> listCompletedPackageDetail = null;
        CompletedPackageData completedPackageDetail = null;
        if (!ObjectUtils.isEmpty(completedPackageDetails)) {
            listCompletedPackageDetail = new ArrayList<>();
            for (CompletedPackageDetail completePackageDetail : completedPackageDetails) {
                completedPackageDetail = new CompletedPackageData();
                completedPackageDetail.setGroupNumber(completePackageDetail.getGroupNumber());
                completedPackageDetail.setSequenceNumber(completePackageDetail.getSequenceNumber());
                completedPackageDetail.setTrackingIds(getTrackingIds(completePackageDetail.getTrackingIds()));
                completedPackageDetail.setSignatureOption(completePackageDetail.getSignatureOption());
                listCompletedPackageDetail.add(completedPackageDetail);
            }
        }
        return listCompletedPackageDetail;
    }

    /**
     * Method to convert TrackingId to TrackingIdDetail
     * 
     * @param trackingIds
     * @return
     */
    private List<TrackingIdDetail> getTrackingIds(
            TrackingId[] trackingIds) {

        return ShipServiceClientMapper.INSTANCE.toModel(trackingIds);
    }

    /**
     * Method to return PackageOperationData object
     * 
     * @param operationalDetail
     * @return
     */
    private PackageOperationData createOperationalDetails(
            PackageOperationalDetail operationalDetail) {

        PackageOperationData packageOperationalDetail = null;
        if (operationalDetail != null) {
            packageOperationalDetail = new PackageOperationData(operationalDetail.getAstraHandlingText(),
                    createOperationalInstructiions(operationalDetail.getOperationalInstructions()),
                    createBarCode(operationalDetail.getBarcodes()), operationalDetail.getGroundServiceCode());
        }
        return packageOperationalDetail;
    }

    /**
     * Method to return PackageBarcodesDetail object
     * 
     * @param barcodes
     * @return
     */
    private PackageBarcodeDetail createBarCode(
            PackageBarcodes barcodes) {

        PackageBarcodeDetail packageBarcodesDetail = null;
        if (barcodes != null) {
            packageBarcodesDetail = new PackageBarcodeDetail(createBinaryBarcode(barcodes.getBinaryBarcodes()),
                    createStringBarcode(barcodes.getStringBarcodes()));
        }
        return packageBarcodesDetail;
    }

    /**
     * Method to return List of StringBarcodeDetail
     * 
     * @param stringBarcodes
     * @return
     */
    private List<StringBarcodeDetail> createStringBarcode(
            StringBarcode[] stringBarcodes) {

        return ShipServiceClientMapper.INSTANCE.toModel(stringBarcodes);
    }

    /**
     * Method to return List of BinaryBarcodeDetail
     * 
     * @param binaryBarcodes
     * @return
     */
    private List<BinaryBarcodeDetail> createBinaryBarcode(
            BinaryBarcode[] binaryBarcodes) {

        return ShipServiceClientMapper.INSTANCE.toModel(binaryBarcodes);
    }

    /**
     * Method to return List of OperationalInstructionDetail
     * 
     * @param operationalInstructions
     * @return List<OperationalInstructionDetail
     */
    private List<OperationalInstructionDetail> createOperationalInstructiions(
            OperationalInstruction[] operationalInstructions) {

        return ShipServiceClientMapper.INSTANCE.toModel(operationalInstructions);
    }

    /**
     * Method to return ShipmentOperationData object
     * 
     * @param operationalDetail
     * @return ShipmentOperationData
     */
    private ShipmentOperationData createOperationalDetail(
            ShipmentOperationalDetail operationalDetail) {

        return ShipServiceClientMapper.INSTANCE.toModel(operationalDetail);
    }

    /**
     * Method to return List of SpecialServiceDescriptionDetail
     * 
     * @param specialServiceDescriptions
     * @return List<SpecialServiceDescriptionDetail>
     */
    private List<SpecialServiceDescriptionDetail> createSpecialServiceDescription(
            SpecialServiceDescription[] specialServiceDescriptions) {

        List<SpecialServiceDescriptionDetail> listSpecialServiceDescriptionDetail = null;
        if (!ObjectUtils.isEmpty(specialServiceDescriptions)) {
            listSpecialServiceDescriptionDetail = new ArrayList<>();
            SpecialServiceDescriptionDetail specialServiceDescriptionDetail = new SpecialServiceDescriptionDetail();
            for (SpecialServiceDescription specialServiceDescription : specialServiceDescriptions) {
                specialServiceDescriptionDetail
                        .setIdentifier(createIdentifier(specialServiceDescription.getIdentifier()));
                specialServiceDescriptionDetail.setNames(createNames(specialServiceDescription.getNames()));
                listSpecialServiceDescriptionDetail.add(specialServiceDescriptionDetail);
            }
        }
        return listSpecialServiceDescriptionDetail;
    }

    /**
     * Method to return OfferingIdentifierData object
     * 
     * @param identifier
     * @return OfferingIdentifierData
     */
    private OfferingIdentifierData createIdentifier(
            OfferingIdentifierDetail identifier) {

        return ShipServiceClientMapper.INSTANCE.toModel(identifier);
    }

    /**
     * Method to return ProductDescription object
     * 
     * @param packagingDescription
     * @return ProductDescription
     */
    private ProductDescription createPackagingDescription(
            PackagingDescription packagingDescription) {

        ProductDescription productDescription = null;
        if (packagingDescription != null) {
            productDescription = new ProductDescription(packagingDescription.getPackagingType(),
                    packagingDescription.getCode(), packagingDescription.getDescription(),
                    packagingDescription.getAstraDescription(), createNames(packagingDescription.getNames()));
        }
        return productDescription;
    }

    /**
     * Method to return ProductDescription object
     * 
     * @param serviceDescription
     * @return ProductDescription
     */
    private ProductDescription createServiceDescription(
            ServiceDescription serviceDescription) {

        ProductDescription productDescription = null;
        if (serviceDescription != null) {
            productDescription = new ProductDescription(serviceDescription.getServiceType(),
                    serviceDescription.getCode(), serviceDescription.getDescription(),
                    serviceDescription.getAstraDescription(), createNames(serviceDescription.getNames()));
        }
        return productDescription;
    }

    /**
     * Method to return List of ProductNameDetail
     * 
     * @param names
     * @return List<ProductNameDetail>
     */
    private List<ProductNameDetail> createNames(
            ProductName[] names) {

        return ShipServiceClientMapper.INSTANCE.toModel(names);
    }

    /**
     * Method to return TrackingIdDetail object
     * 
     * @param masterTrackingId
     * @return TrackingIdDetail
     */
    private TrackingIdDetail createTrackingId(
            TrackingId masterTrackingId) {

        return ShipServiceClientMapper.INSTANCE.toModel(masterTrackingId);
    }

    /**
     * Method to return ProductDescription object
     * 
     * @param serviceDescription
     * @return ProductDescription
     */
    private List<ShippingDocument> createShippingDocument(
            CompletedPackageDetail[] completedPackageDetails) {

        List<ShippingDocument> listShippingDocument = null;
        ShippingDocument shippingDocument = new ShippingDocument();
        if (!ObjectUtils.isEmpty(completedPackageDetails)) {
            listShippingDocument = new ArrayList<>();
            for (CompletedPackageDetail completePackageDetail : completedPackageDetails) {

                if (completePackageDetail.getLabel() != null) {
                    shippingDocument.setType(completePackageDetail.getLabel()
                            .getType());
                    shippingDocument.setShippingDocumentDisposition(completePackageDetail.getLabel()
                            .getShippingDocumentDisposition());
                    shippingDocument.setImageType(completePackageDetail.getLabel()
                            .getImageType());
                    shippingDocument.setResolution(completePackageDetail.getLabel()
                            .getResolution());
                    shippingDocument.setCopiesToPrint(completePackageDetail.getLabel()
                            .getCopiesToPrint());
                    shippingDocument.setImage(retailShipmentCreationServiceUtil.decodePackageDocument(
                            completePackageDetail.getLabel(), ShippingDocumentType.OUTBOUND_LABEL));
                }
                listShippingDocument.add(shippingDocument);
            }
        }
        return listShippingDocument;
    }

    /**
     * Method to return List of full CompletedPackageData
     * 
     * @param completedPackageDetails
     * @return List<CompletedPackageData>
     */
    private List<CompletedPackageData> createCompletePackageDetails(
            CompletedPackageDetail[] completedPackageDetails) {

        List<CompletedPackageData> listCompletedPackageDetail = null;
        if (!ObjectUtils.isEmpty(completedPackageDetails)) {
            listCompletedPackageDetail = new ArrayList<>();
            for (CompletedPackageDetail detail : completedPackageDetails) {
                CompletedPackageData completedPackageDetail = new CompletedPackageData();
                completedPackageDetail.setGroupNumber(detail.getGroupNumber());
                completedPackageDetail
                        .setTrackingIds(ShipServiceClientMapper.INSTANCE.toModel(detail.getTrackingIds()));
                completedPackageDetail.setSequenceNumber(detail.getSequenceNumber());
                completedPackageDetail.setSignatureOption(detail.getSignatureOption());
                completedPackageDetail.setOperationalDetail(createOperationalDetails(detail.getOperationalDetail()));
                listCompletedPackageDetail.add(completedPackageDetail);
            }
        }
        return listCompletedPackageDetail;
    }

    /**
     * Response Mapper for ConfirmOpenShipmentReply
     * 
     * @param reply
     * @return
     */
    public CompleteShipmentDetail buildConfirmOpenShipmentResponse(
            final ConfirmOpenShipmentReply reply) {

        CompleteShipmentDetail completeShipmentDetail = null;
        // handle the empty or null severity from CSHP.
        if (reply == null || reply.getHighestSeverity() == null) {
            logger.error("buildConfirmOpenShipmentResponse: CSHP service call is failed due to empty response: {}",
                    reply);
            throw new ClientTransientException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);

        } // Handle FAILURE severity of CSHP
        else if (reply.getHighestSeverity()
                .equals(Severity.FAILURE.name())) {
            logger.error("buildConfirmOpenShipmentResponse : CSHP service call is failed due to failure severity with response: {}", reply);
            throw new ClientTerminalException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);

        } // Consider NOTE, WARNING or SUCCESS severity response from CSHP as success.
        else if (Severity.valueOf(reply.getHighestSeverity())
                .isAtOrMoreSevereThan(Severity.NOTE)) {

            if (!ObjectUtils.isEmpty(reply.getCompletedShipmentDetail())) {
                completeShipmentDetail = new CompleteShipmentDetail();
                completeShipmentDetail.setUsDomestic(reply.getCompletedShipmentDetail()
                        .getUsDomestic());
                completeShipmentDetail.setCarrierCode(reply.getCompletedShipmentDetail()
                        .getCarrierCode());
                completeShipmentDetail.setMasterTrackingId(createTrackingId(reply.getCompletedShipmentDetail()
                        .getMasterTrackingId()));
                completeShipmentDetail.setServiceDescription(createServiceDescription(reply.getCompletedShipmentDetail()
                        .getServiceDescription()));
                completeShipmentDetail
                        .setPackagingDescription(createPackagingDescription(reply.getCompletedShipmentDetail()
                                .getPackagingDescription()));
                completeShipmentDetail.setSpecialServiceDescriptions(
                        createSpecialServiceDescription(reply.getCompletedShipmentDetail()
                                .getSpecialServiceDescriptions()));
                completeShipmentDetail.setOperationalDetail(createOperationalDetail(reply.getCompletedShipmentDetail()
                        .getOperationalDetail()));
                completeShipmentDetail
                        .setCompletedPackageDetails(createCompletePackageDetails(reply.getCompletedShipmentDetail()
                                .getCompletedPackageDetails()));
                completeShipmentDetail.setShipmentDocuments(createShippingDocument(reply.getCompletedShipmentDetail()
                        .getCompletedPackageDetails()));
                if (!ObjectUtils.isEmpty(reply.getCompletedShipmentDetail()
                        .getCompletedPackageDetails())) {
                    completeShipmentDetail.setPackageCount(reply.getCompletedShipmentDetail()
                            .getCompletedPackageDetails().length);
                }

                try {
                    completeShipmentDetail.setPlannedServiceLevel(reply.getCompletedShipmentDetail()
                            .getOperationalDetail()
                            .getOriginServiceArea());
                } catch (Exception e) {
                    logger.error("OperationalDetail is not present in response received from CHSP Response {}", reply,
                            e);
                }

            }
        } // Handle ERROR severity of CSHP
        else {
            logger.error("CSHP service call is failed with erroneous response as: {}", reply);
            if (!ObjectUtils.isEmpty(reply.getNotifications())) {
                throw new ClientTerminalException(
                        processClientError(ServiceErrorCode.INVALID_SHIPMENT_DETAILS.getApiErrorCode(),
                                reply.getNotifications()[0].getMessage(),
                                ServiceErrorCode.INVALID_SHIPMENT_DETAILS.getStatus()));
            }
        }
        return completeShipmentDetail;
    }

    /**
     * Response mapper for delete shipment
     * 
     * @param reply
     * @return
     */
    public String buildDeleteShipmentResponse(
            ShipmentReply reply) {

        String severity = null;
        // handle the empty or null severity from CSHP.
        if (reply == null || reply.getHighestSeverity() == null) {

            logger.error("CSHP service call is failed due to empty response: {}", reply);
            throw new ClientTransientException(ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE);

        }
        // Handle FAILURE severity of CSHP
        else if (reply.getHighestSeverity()
                .equals(Severity.FAILURE.name())) {

            Notification[] notificationList = reply.getNotifications();
            if (!ObjectUtils.isEmpty(notificationList)) {

                logger.error("CSHP service call is failed due to failure response: {}", reply);
                throw new ClientTerminalException(processClientError(
                        ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getApiErrorCode(), notificationList[0].getMessage(),
                        ServiceErrorCode.CSHP_SYSTEM_UNAVAILABLE.getStatus()));
            }

        }
        // Consider NOTE, WARNING or SUCCESS severity response from CSHP as success.
        else if (Severity.valueOf(reply.getHighestSeverity())
                .isAtOrMoreSevereThan(Severity.NOTE)) {

            severity = reply.getHighestSeverity();
        }
        // Handle ERROR severity of CSHP
        else {
            if (!ObjectUtils.isEmpty(reply.getNotifications())) {

                logger.error("CSHP service call is failed with erroneous response as: {}", reply);
                throw new ClientTerminalException(
                        processClientError(ServiceErrorCode.INVALID_SHIPMENT_DETAILS.getApiErrorCode(),
                                reply.getNotifications()[0].getMessage(),
                                ServiceErrorCode.INVALID_SHIPMENT_DETAILS.getStatus()));
            }
        }
        return severity;
    }

}
