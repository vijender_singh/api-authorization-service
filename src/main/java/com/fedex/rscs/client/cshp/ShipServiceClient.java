package com.fedex.rscs.client.cshp;

import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.cshp.CompleteShipmentDetail;
import com.fedex.rscs.model.cshp.DeleteOpenShipmentDetail;
import com.fedex.rscs.model.cshp.TrackingIdDetail;

/**
 * Interface to CSHP service end point
 * 
 * @author Ankit.Bisht
 */
public interface ShipServiceClient {

    /**
     * This method is used to provide complete shipment details for account shipment.
     * 
     */
    CompleteShipmentDetail createOpenShipment(
            RequestedShipmentDetail requestedShipment,
            WorkstationDetail workstationDetail);

    /**
     * This method is used to delete open shipment.
     * 
     */
    void deleteOpenShipment(
            DeleteOpenShipmentDetail deleteOpenShipmentDetail);

    /**
     * This method is used to delete shipment from CSHP.
     * 
     */
    String deleteShipment(
            TrackingIdDetail trackingDetail,
            WorkstationDetail workstationDetail,
            String locationId);

    /*
     * This method is used to provide complete shipment details for modify account shipment.
     * 
     */
    CompleteShipmentDetail modifyOpenShipment(
            RequestedShipmentDetail requestedShipment,
            WorkstationDetail workstationDetail);

    /*
     * This method is used to provide complete shipment details for confirm open shipment.
     * 
     */
    CompleteShipmentDetail confirmOpenShipment(
            ShipmentConfirmationRequest request);

}
