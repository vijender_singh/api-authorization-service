package com.fedex.rscs.client.cshp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fedex.nxgen.ship.v17.ientities.ConfirmOpenShipmentReply;
import com.fedex.nxgen.ship.v17.ientities.ConfirmOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.CreateOpenShipmentReply;
import com.fedex.nxgen.ship.v17.ientities.CreateOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.DeleteOpenShipmentReply;
import com.fedex.nxgen.ship.v17.ientities.DeleteOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.DeleteShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.ModifyOpenShipmentReply;
import com.fedex.nxgen.ship.v17.ientities.ModifyOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.ShipInterface;
import com.fedex.nxgen.ship.v17.ientities.ShipmentReply;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.cshp.CompleteShipmentDetail;
import com.fedex.rscs.model.cshp.DeleteOpenShipmentDetail;
import com.fedex.rscs.model.cshp.TrackingIdDetail;

/**
 * Client implementation of create shipment Service (CSHP) end points.
 * 
 * @author 5034922
 *
 */
@Service
@Profile("!mock")
public class ShipServiceClientImpl implements ShipServiceClient {

    private static final Logger logger = LoggerFactory.getLogger(ShipServiceClientImpl.class);
    private final ShipServiceRequestBuilder requestBuilder;
    private final ShipServiceResponseMapper responseMapper;
    private final ShipInterface shipInterface;


    @Autowired
    public ShipServiceClientImpl(final ShipServiceRequestBuilder requestBuilder,
            final ShipServiceResponseMapper responseMapper, final ShipInterface shipInterface) {

        this.requestBuilder = requestBuilder;
        this.responseMapper = responseMapper;
        this.shipInterface = shipInterface;
    }

    /**
     * Call for create open shipment
     */
    @Override
    public CompleteShipmentDetail createOpenShipment(
            final RequestedShipmentDetail requestedShipment,
            final WorkstationDetail workstationDetail) {

        CreateOpenShipmentRequest request = requestBuilder.buildRequest(requestedShipment, workstationDetail);
        logger.debug("Prepare Request for CSHP.createOpenShipment {}", request);
        CreateOpenShipmentReply reply = shipInterface.createOpenShipment(request);
        logger.debug("Received CSHP.createOpenShipment response: {}", reply);

        return responseMapper.mapToShipmentDetail(reply);
    }

    /**
     * Call for delete open shipment
     */
    @Async("cshpEventsQueue")
    @Override
    public void deleteOpenShipment(
            final DeleteOpenShipmentDetail deleteOpenShipmentDetail) {

        DeleteOpenShipmentRequest request = requestBuilder.buildDeleteOpenShipmentRequest(deleteOpenShipmentDetail);
        logger.debug("Prepare Request for CSHP.deleteOpenShipment {}", request);
        DeleteOpenShipmentReply reply = shipInterface.deleteOpenShipment(request);
        logger.debug("Received CSHP.deleteOpenShipment response: {}", reply);
    }

    /**
     * Call for modify open shipment
     */
    @Override
    public CompleteShipmentDetail modifyOpenShipment(
            final RequestedShipmentDetail requestedShipment,
            final WorkstationDetail workstationDetail) {

        ModifyOpenShipmentRequest request = requestBuilder.buildModifyRequest(requestedShipment, workstationDetail);
        logger.debug("Prepare Request for CSHP.modifyOpenShipment {}", request);
        ModifyOpenShipmentReply reply = shipInterface.modifyOpenShipment(request);
        logger.debug("Received CSHP.modifyOpenShipment response: {}", reply);

        return responseMapper.buildModifyOpenShipmentResponse(reply);
    }

    /**
     * Call for delete shipment
     */
    @Override
    public String deleteShipment(
            TrackingIdDetail trackingDetail,
            WorkstationDetail workstationDetail,
            String locationId) {

        DeleteShipmentRequest request =
                requestBuilder.buildDeleteShipmentRequest(trackingDetail, workstationDetail, locationId);
        logger.debug("Prepare Request for CSHP.deleteShipment {}", request);
        ShipmentReply reply = shipInterface.deleteShipment(request);
        logger.debug("Received CSHP.deleteShipment response: {}", reply);

        return responseMapper.buildDeleteShipmentResponse(reply);

    }

    /**
     * Call for confirm open shipment
     */
    @Override
    public CompleteShipmentDetail confirmOpenShipment(
            ShipmentConfirmationRequest confirmShipmentRequest) {

        ConfirmOpenShipmentRequest request = requestBuilder.buildConfirmRequest(confirmShipmentRequest);
        logger.debug("Prepare Request for CSHP.confirmOpenShipment {}", request);
        ConfirmOpenShipmentReply reply = shipInterface.confirmOpenShipment(request);
        logger.debug("Received CSHP.confirmOpenShipment response: {}", reply);

        return responseMapper.buildConfirmOpenShipmentResponse(reply);
    }

}
