package com.fedex.rscs.client.cshp;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import com.fedex.nxgen.ship.v17.ientities.Address;
import com.fedex.nxgen.ship.v17.ientities.AssociatedAccount;
import com.fedex.nxgen.ship.v17.ientities.AsynchronousProcessingOptionsRequested;
import com.fedex.nxgen.ship.v17.ientities.BatteryClassificationDetail;
import com.fedex.nxgen.ship.v17.ientities.ConfirmOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.Contact;
import com.fedex.nxgen.ship.v17.ientities.ContactAndAddress;
import com.fedex.nxgen.ship.v17.ientities.CreateOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.CustomerReference;
import com.fedex.nxgen.ship.v17.ientities.DeleteOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.DeleteShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.Dimensions;
import com.fedex.nxgen.ship.v17.ientities.EMailDetail;
import com.fedex.nxgen.ship.v17.ientities.EPaymentDetail;
import com.fedex.nxgen.ship.v17.ientities.HoldAtLocationDetail;
import com.fedex.nxgen.ship.v17.ientities.HomeDeliveryPremiumDetail;
import com.fedex.nxgen.ship.v17.ientities.LabelSpecification;
import com.fedex.nxgen.ship.v17.ientities.Localization;
import com.fedex.nxgen.ship.v17.ientities.ModifyOpenShipmentRequest;
import com.fedex.nxgen.ship.v17.ientities.Money;
import com.fedex.nxgen.ship.v17.ientities.NotificationDetail;
import com.fedex.nxgen.ship.v17.ientities.PackageSpecialHandlingDetail;
import com.fedex.nxgen.ship.v17.ientities.PackageSpecialServicesRequested;
import com.fedex.nxgen.ship.v17.ientities.Party;
import com.fedex.nxgen.ship.v17.ientities.Payment;
import com.fedex.nxgen.ship.v17.ientities.Payor;
import com.fedex.nxgen.ship.v17.ientities.RequestedPackageLineItem;
import com.fedex.nxgen.ship.v17.ientities.RequestedShipment;
import com.fedex.nxgen.ship.v17.ientities.ShipmentEventNotificationDetail;
import com.fedex.nxgen.ship.v17.ientities.ShipmentEventNotificationSpecification;
import com.fedex.nxgen.ship.v17.ientities.ShipmentNotificationFormatSpecification;
import com.fedex.nxgen.ship.v17.ientities.ShipmentSpecialServicesRequested;
import com.fedex.nxgen.ship.v17.ientities.ShipmentVariationOptionDetail;
import com.fedex.nxgen.ship.v17.ientities.SignatureOptionDetail;
import com.fedex.nxgen.ship.v17.ientities.TransactionDetail;
import com.fedex.nxgen.ship.v17.ientities.Weight;
import com.fedex.rscs.client.mapper.ShipServiceClientMapper;
import com.fedex.rscs.config.cshp.ShipServiceConfiguration;
import com.fedex.rscs.dto.PackageSpecialHandlingType;
import com.fedex.rscs.dto.ResponsibleParty;
import com.fedex.rscs.model.AddressDetail;
import com.fedex.rscs.model.AssociatedAccountDetail;
import com.fedex.rscs.model.BatteryClassificationType;
import com.fedex.rscs.model.ContactDetail;
import com.fedex.rscs.model.DimensionDetail;
import com.fedex.rscs.model.HoldAtLocDetail;
import com.fedex.rscs.model.HomeDelvDetail;
import com.fedex.rscs.model.InsuredValueDetail;
import com.fedex.rscs.model.LabelSpecificationDetail;
import com.fedex.rscs.model.PartyDetail;
import com.fedex.rscs.model.PaymentDetail;
import com.fedex.rscs.model.RequestedPackageLineItemDetail;
import com.fedex.rscs.model.RequestedShipmentDetail;
import com.fedex.rscs.model.ServiceType;
import com.fedex.rscs.model.ShipmentConfirmationRequest;
import com.fedex.rscs.model.ShipmentNotificationRoleType;
import com.fedex.rscs.model.ShipmentSpecialServiceDetail;
import com.fedex.rscs.model.ShippingCustomerReferenceDetails;
import com.fedex.rscs.model.SpecialServiceDescriptionDetail;
import com.fedex.rscs.model.VariationOptionDetail;
import com.fedex.rscs.model.WeightDetail;
import com.fedex.rscs.model.WorkstationDetail;
import com.fedex.rscs.model.cshp.DeleteOpenShipmentDetail;
import com.fedex.rscs.model.cshp.Price;
import com.fedex.rscs.model.cshp.SpecialServiceCode;
import com.fedex.rscs.model.cshp.TrackingIdDetail;
import com.fedex.rscs.util.RetailShipmentCreationServiceUtil;


/**
 * This class used to build requests objects for cshp service calls
 * 
 * @author 5034922
 *
 */
@Component
public class ShipServiceRequestBuilder {

    private static final String DELETE_ALL_PACKAGES = "DELETE_ALL_PACKAGES";
    private static final String DATE_FORMATTER = "yyyy:MM:dd:HH:mm:ss";
    private static final String DEFAULT_CURRENCY_CODE = "USD";
    private static final String DEFAULT_WEIGHT_UOM = "LB";
    private static final String DEFAULT_PROCESSING_OPTION_TYPE = "SYNCHRONOUS_ONLY";
    private static final String[] EVENTS = {"ON_SHIPMENT", "ON_EXCEPTION", "ON_DELIVERY"};
    private static final String FORMAT_SPECIFICATION = "TEXT";
    private static final String LANGUAGE_CODE = "en";
    public static final String PAYMENT_TYPE_EPAYMENT = "EPAYMENT";
    public static final String EPAYMENT_PROCESSOR = "RETAIL_SINGLE_POINT_OF_SALE";
    public static final String NO_SIGNATURE_RELEASE_NUMBER = "3785346";


    private final RetailShipmentCreationServiceUtil serviceUtil;
    private final ShipServiceConfiguration serviceConfig;

    public ShipServiceRequestBuilder(final RetailShipmentCreationServiceUtil serviceUtil,
            final ShipServiceConfiguration serviceConfig) {

        this.serviceUtil = serviceUtil;
        this.serviceConfig = serviceConfig;
    }

    /**
     * Method to build request for create open shipment
     * 
     * @param requestedShipment
     * @param workstationDetail
     * @return
     */
    public CreateOpenShipmentRequest buildRequest(
            final RequestedShipmentDetail requestedShipment,
            final WorkstationDetail workstationDetail) {

        CreateOpenShipmentRequest createOpenShipmentRequest = new CreateOpenShipmentRequest();
        createOpenShipmentRequest.setClientDetail(ShipServiceClientMapper.INSTANCE.toModel(workstationDetail));
        String timeStamp = requestedShipment.getShipTimestamp();
        requestedShipment.setShipTimestamp(requestedShipment.getShipTimestamp());
        boolean isGroundShipment = requestedShipment.getServiceType()
                .equalsIgnoreCase(ServiceType.FEDEX_GROUND.name())
                || requestedShipment.getServiceType()
                        .equalsIgnoreCase(ServiceType.GROUND_HOME_DELIVERY.name());

        if (requestedShipment.isExpressAfterPickup() && !isGroundShipment) {
            requestedShipment.setShipTimestamp(ZonedDateTime.parse(requestedShipment.getShipTimestamp())
                    .plusDays(1)
                    .toString());
        } else if (requestedShipment.isGroundAfterPickup() && isGroundShipment) {

            requestedShipment.setShipTimestamp(ZonedDateTime.parse(requestedShipment.getShipTimestamp())
                    .plusDays(1)
                    .toString());
        }
        createOpenShipmentRequest.setTransactionDetail(getTransactionDetail(requestedShipment.getShipTimestamp(),
                requestedShipment.getTransactionLocationCode()));
        createOpenShipmentRequest
                .setVersion(ShipServiceClientMapper.INSTANCE.toModel(this.serviceConfig.getVersionId()));

        if (!CollectionUtils.isEmpty(requestedShipment.getActions())) {
            createOpenShipmentRequest.setActions(requestedShipment.getActions()
                    .stream()
                    .toArray(String[]::new));
        }

        createOpenShipmentRequest.setRequestedShipment(createRequestedShipment(requestedShipment));
        // made this configurable but we may change it back to use APPLICATION_ID once the
        // environment will be stable.
        createOpenShipmentRequest.setApplicationId(serviceConfig.getClientApplicationId());
        requestedShipment.setShipTimestamp(timeStamp);
        return createOpenShipmentRequest;

    }

    /**
     * This method returns ModifyOpenShipmentRequest
     * 
     * @param requestedShipment
     * @param workstationDetail
     * @return
     */
    public ModifyOpenShipmentRequest buildModifyRequest(
            final RequestedShipmentDetail requestedShipment,
            WorkstationDetail workstationDetail) {

        ModifyOpenShipmentRequest modifyRequest = new ModifyOpenShipmentRequest();
        modifyRequest.setClientDetail(ShipServiceClientMapper.INSTANCE.toModel(workstationDetail));
        modifyRequest.setTransactionDetail(getTransactionDetail(requestedShipment.getShipTimestamp(),
                requestedShipment.getTransactionLocationCode()));
        modifyRequest.setVersion(ShipServiceClientMapper.INSTANCE.toModel(this.serviceConfig.getVersionId()));
        modifyRequest.setActions(requestedShipment.getActions()
                .stream()
                .toArray(String[]::new));
        modifyRequest.setRequestedShipment(createRequestedShipment(requestedShipment));
        modifyRequest.setApplicationId(serviceConfig.getClientApplicationId());
        modifyRequest.setIndex(requestedShipment.getIndex());
        return modifyRequest;
    }

    /**
     * Method to build request for delete open shipment
     * 
     * @param deleteOpenShipmentDetail
     * @return
     */
    public DeleteOpenShipmentRequest buildDeleteOpenShipmentRequest(
            final DeleteOpenShipmentDetail deleteOpenShipmentDetail) {

        DeleteOpenShipmentRequest deleteOpenShipmentRequest = new DeleteOpenShipmentRequest();
        deleteOpenShipmentRequest.setClientDetail(
                ShipServiceClientMapper.INSTANCE.toModel(deleteOpenShipmentDetail.getWorkstationDetail()));
        deleteOpenShipmentRequest.setTransactionDetail(getTransactionDetail(
                deleteOpenShipmentDetail.getRequestedDateTime(), deleteOpenShipmentDetail.getLocationCode()));
        deleteOpenShipmentRequest
                .setVersion(ShipServiceClientMapper.INSTANCE.toModel(this.serviceConfig.getVersionId()));
        deleteOpenShipmentRequest.setIndex(deleteOpenShipmentDetail.getIndex());
        deleteOpenShipmentRequest.setApplicationId(serviceConfig.getClientApplicationId());

        return deleteOpenShipmentRequest;
    }

    /**
     * Create request for DeleteShipment
     * 
     * @param trackDetail
     * @param workstationDetails
     * @return
     */
    public DeleteShipmentRequest buildDeleteShipmentRequest(
            TrackingIdDetail trackDetail,
            WorkstationDetail workstationDetails,
            String locationId) {

        DeleteShipmentRequest request = new DeleteShipmentRequest();
        // will set utc time after decision made
        String date = LocalDateTime.now()
                .format(DateTimeFormatter.ofPattern(DATE_FORMATTER));
        request.setShipTimestamp(date);
        request.setDeletionControl(DELETE_ALL_PACKAGES);
        request.setClientDetail(ShipServiceClientMapper.INSTANCE.toModel(workstationDetails));
        request.setTransactionDetail(getTransactionDetail(date, locationId));
        request.setVersion(ShipServiceClientMapper.INSTANCE.toModel(this.serviceConfig.getVersionId()));
        request.setTrackingId(ShipServiceClientMapper.INSTANCE.toModel(trackDetail));
        request.setApplicationId(serviceConfig.getClientApplicationId());

        return request;
    }

    /**
     * This method is used to return RequestedShipment
     * 
     * @param requestedShipment
     * @return
     */
    private RequestedShipment createRequestedShipment(
            RequestedShipmentDetail requestedShipment) {

        RequestedShipment req = new RequestedShipment();
        req.setShipTimestamp(requestedShipment.getShipTimestamp());
        req.setDropoffType(requestedShipment.getDropOffType());
        req.setServiceType(requestedShipment.getServiceType());
        req.setPackagingType(requestedShipment.getPackagingType());
        req.setVariationOptions(createVariationOptions(requestedShipment.getVariationOptions()));
        setTotalWeightAndInsuredValue(req, requestedShipment.getPackageLineItems());
        req.setShipper(createParty(requestedShipment.getSender()));
        req.setRecipient(createParty(requestedShipment.getRecipient()));
        if (requestedShipment.getOrigin() != null) {
            req.setOrigin(createContactAndAddress(requestedShipment.getOrigin()
                    .getContact(),
                    requestedShipment.getOrigin()
                            .getAddress()));
        }

        req.setShippingChargesPayment(createPayment(requestedShipment.getShippingChargesPayment()));
        req.setLabelSpecification(createLabelSpecification(requestedShipment.getLabelSpecification()));
        req.setRequestedPackageLineItems(createRequestedPackageLineItems(requestedShipment.getPackageLineItems(),
                requestedShipment.getServiceType()));

        if (requestedShipment.getSpecialServicesRequested() != null) {

            ShipmentSpecialServicesRequested shipRequest = new ShipmentSpecialServicesRequested();
            // set Event notifications details
            shipRequest.setEventNotificationDetail(createEmailNotificationDetail(requestedShipment));
            // set EVENT_NOTIFICATION special service Types if details are present
            if (shipRequest.getEventNotificationDetail() != null
                    && !CollectionUtils.isEmpty(requestedShipment.getSpecialServicesRequested()
                            .getSpecialServicesRequestedType())) {

                requestedShipment.getSpecialServicesRequested()
                        .getSpecialServicesRequestedType()
                        .add(new SpecialServiceDescriptionDetail(SpecialServiceCode.EVENT_NOTIFICATION.getType(), null,
                                null, null));

            } else if (shipRequest.getEventNotificationDetail() != null) {

                List<SpecialServiceDescriptionDetail> specialServiceDescriptionDetails = new ArrayList<>();
                specialServiceDescriptionDetails.add(new SpecialServiceDescriptionDetail(
                        SpecialServiceCode.EVENT_NOTIFICATION.getType(), null, null, null));
                requestedShipment.getSpecialServicesRequested()
                        .setSpecialServicesRequestedType(specialServiceDescriptionDetails);

            }
            // Set requested special services to CSHP request
            req.setSpecialServicesRequested(
                    createSpecialServicesRequested(requestedShipment.getSpecialServicesRequested(), shipRequest));

            // Set SpecialServicesRequested object to null if SpecialServicesRequestedType list is
            // empty
            if (CollectionUtils.isEmpty(requestedShipment.getSpecialServicesRequested()
                    .getSpecialServicesRequestedType())) {

                req.setSpecialServicesRequested(null);
            }
        }

        req.setBlockInsightVisibility(true);
        if (StringUtils.isNotEmpty(requestedShipment.getPackageCount())) {
            req.setPackageCount(Integer.parseInt(requestedShipment.getPackageCount()));
        }
        return req;
    }

    /**
     * Method to set total weight and insuredValue
     * 
     * @param req
     * @param packageLineItems
     */
    private void setTotalWeightAndInsuredValue(
            RequestedShipment req,
            List<RequestedPackageLineItemDetail> packageLineItems) {

        Weight weight = new Weight();
        weight.setValue(0d);
        Money money = new Money();
        money.setAmount(0d);
        weight.setUnits(DEFAULT_WEIGHT_UOM);
        money.setCurrency(DEFAULT_CURRENCY_CODE);
        if (!CollectionUtils.isEmpty(packageLineItems)) {
            for (RequestedPackageLineItemDetail reqPacDet : packageLineItems) {
                if (reqPacDet.getWeight() != null) {

                    weight.setUnits(reqPacDet.getWeight()
                            .getUnit());
                    weight.setValue(weight.getValue() + Double.parseDouble(reqPacDet.getWeight()
                            .getValue()));
                }

                if (reqPacDet.getInsuredValue() != null) {
                    money.setAmount(money.getAmount() + Double.parseDouble(reqPacDet.getInsuredValue()
                            .getAmount()));
                    money.setCurrency(reqPacDet.getInsuredValue()
                            .getCurrency());
                }

            }
        }
        req.setTotalWeight(weight);
        req.setTotalInsuredValue(money);

    }

    /**
     * This method is to create ShipmentSpecialServicesRequested
     * 
     * @param shipmentSpecialServiceDetail
     * @param shipRequest
     * @return
     */
    private ShipmentSpecialServicesRequested createSpecialServicesRequested(
            ShipmentSpecialServiceDetail shipmentSpecialServiceDetail,
            ShipmentSpecialServicesRequested shipRequest) {

        if (!CollectionUtils.isEmpty(shipmentSpecialServiceDetail.getSpecialServicesRequestedType())) {
            List<String> serviceTypes = shipmentSpecialServiceDetail.getSpecialServicesRequestedType()
                    .stream()
                    .map(SpecialServiceDescriptionDetail::getSpecialServiceType)
                    .collect(Collectors.toList());
            shipRequest.setSpecialServiceTypes(serviceTypes.stream()
                    .toArray(String[]::new));
        }

        if (shipmentSpecialServiceDetail.getHoldAtLocationDetail() != null) {
            shipRequest.setHoldAtLocationDetail(
                    createHoldAtLocationDetails(shipmentSpecialServiceDetail.getHoldAtLocationDetail()));
        }

        if (shipmentSpecialServiceDetail.getHomeDeliveryDetail() != null) {
            shipRequest.setHomeDeliveryPremiumDetail(
                    createHomeDeliveryPremiumDetail(shipmentSpecialServiceDetail.getHomeDeliveryDetail()));
        }

        return shipRequest;
    }

    /**
     * This method is used to return HoldAtLocationDetail
     * 
     * @param holdAtLocDetail
     * @return
     */
    private HoldAtLocationDetail createHoldAtLocationDetails(
            HoldAtLocDetail holdAtLocDetail) {

        HoldAtLocationDetail holdAtLocationDetail = null;
        if (holdAtLocDetail != null) {
            holdAtLocationDetail = new HoldAtLocationDetail();
            holdAtLocationDetail.setLocationId(holdAtLocDetail.getLocationId());
            holdAtLocationDetail.setLocationType(holdAtLocDetail.getLocationType());
            holdAtLocationDetail
                    .setLocationContactAndAddress(createContactAndAddress(null, holdAtLocDetail.getAddress()));
            if (holdAtLocDetail.getContact() != null) {
                holdAtLocationDetail.setPhoneNumber(holdAtLocDetail.getContact()
                        .getPhoneNumber());
            }
        }

        return holdAtLocationDetail;
    }

    /**
     * This method is used to return ShipmentVariationOptionDetail[]
     * 
     * @param variationOptionDetails
     * @return
     */
    private ShipmentVariationOptionDetail[] createVariationOptions(
            List<VariationOptionDetail> variationOptionDetails) {

        List<ShipmentVariationOptionDetail> listOfShipmentVariationOptionDetail = new ArrayList<>();

        if (!CollectionUtils.isEmpty(variationOptionDetails)) {
            for (VariationOptionDetail variationOptionDetail : variationOptionDetails) {

                ShipmentVariationOptionDetail shipmentVariationOptionDetail = new ShipmentVariationOptionDetail();
                shipmentVariationOptionDetail.setId(variationOptionDetail.getId());
                shipmentVariationOptionDetail.setValues(variationOptionDetail.getValues()
                        .stream()
                        .toArray(String[]::new));
                listOfShipmentVariationOptionDetail.add(shipmentVariationOptionDetail);
            }
        }

        return listOfShipmentVariationOptionDetail.stream()
                .toArray(ShipmentVariationOptionDetail[]::new);
    }

    /**
     * This method is used to return Payment
     * 
     * @param paymentDetail
     * @return
     */
    private Payment createPayment(
            PaymentDetail paymentDetail) {

        Payment payment = new Payment();
        if (paymentDetail != null) {
            payment.setPayor(createPayor(paymentDetail));

            // consider it as epayment if epayment mode is provided.
            if (paymentDetail.getEpaymentMode() != null) {

                payment.setPaymentType(PAYMENT_TYPE_EPAYMENT);

                EPaymentDetail epayment = new EPaymentDetail();
                epayment.setAmount(createPrice(paymentDetail.getAmount()));
                epayment.setEPaymentMode(paymentDetail.getEpaymentMode()
                        .name());
                epayment.setEPaymentProcessor(EPAYMENT_PROCESSOR);
                epayment.setId(paymentDetail.getPaymentRefId());
                if (paymentDetail.getPayor() != null && paymentDetail.getPayor()
                        .getCreditCard() != null) {

                    epayment.setCreditCardExpirationDate(paymentDetail.getPayor()
                            .getCreditCard()
                            .getExpirationDate());
                    epayment.setCreditCardType(paymentDetail.getPayor()
                            .getCreditCard()
                            .getType());
                    epayment.setMaskedCreditCard(paymentDetail.getPayor()
                            .getCreditCard()
                            .getMaskedCreditCard());

                }
                payment.setEPaymentDetail(epayment);
            } else {
                final String responsibleParty = paymentDetail.getResponsibleParty()
                        .equalsIgnoreCase(ResponsibleParty.SENDER.name()) ? ResponsibleParty.THIRD_PARTY.name()
                                : paymentDetail.getResponsibleParty();
                payment.setPaymentType(responsibleParty);
            }
        }
        return payment;
    }

    /**
     * This method is used to return money
     * 
     * @param amount
     * @return
     */
    private Money createPrice(
            Price amount) {

        Money money = null;
        if (amount != null) {
            money = new Money();
            money.setAmount(amount.getAmount());
            money.setCurrency(amount.getCurrency());
        }

        return money;
    }

    /**
     * This method is used to return Payor
     * 
     * @param paymentDetail
     * @return
     */
    private Payor createPayor(
            PaymentDetail paymentDetail) {

        Payor payor = new Payor();

        if (!CollectionUtils.isEmpty(paymentDetail.getAssociatedAccounts())) {

            payor.setAssociatedAccounts(createAssociatedList(paymentDetail.getAssociatedAccounts()));
        }

        if (paymentDetail.getPayor() != null) {
            PartyDetail party = paymentDetail.getPayor();
            payor.setResponsibleParty(createParty(party));
            payor.getResponsibleParty()
                    .setAccountNumber(party.getAccountNumber());
        }

        return payor;
    }

    /**
     * This method is used to return AssociatedAccount[]
     * 
     * @param associatedAccounts
     * @return
     */
    private AssociatedAccount[] createAssociatedList(
            List<AssociatedAccountDetail> associatedAccounts) {

        List<AssociatedAccount> listAssAcctList = new ArrayList<>();
        AssociatedAccount associatedAccount = new AssociatedAccount();
        for (AssociatedAccountDetail associatedAccountDetail : associatedAccounts) {
            associatedAccount.setAccountNumber(associatedAccountDetail.getAccountNumber());
            associatedAccount.setType(associatedAccountDetail.getType());
            listAssAcctList.add(associatedAccount);
        }

        return listAssAcctList.stream()
                .toArray(AssociatedAccount[]::new);
    }

    /**
     * This method is used to return Party
     * 
     * @param partyDetail
     * @return
     */
    private Party createParty(
            PartyDetail partyDetail) {

        Party party = new Party();
        party.setAddress(createAddress(partyDetail.getAddress()));
        party.setContact(createContact(partyDetail.getContact()));

        return party;
    }

    /**
     * This method is used to return LabelSpecification
     * 
     * @param labelSpecification
     * @return
     */
    private LabelSpecification createLabelSpecification(
            LabelSpecificationDetail labelSpecification) {

        LabelSpecification lab = new LabelSpecification();
        lab.setLabelFormatType(labelSpecification.getFormatType());
        lab.setImageType(labelSpecification.getImageType());
        lab.setLabelStockType(labelSpecification.getStockType());
        lab.setLabelPrintingOrientation(labelSpecification.getPrintingOrientation());
        return lab;
    }

    /**
     * This method is used to return ContactAndAddress
     * 
     * @param contactDetail
     * @param addressDetail
     * @return
     */
    private ContactAndAddress createContactAndAddress(
            ContactDetail contactDetail,
            AddressDetail addressDetail) {

        ContactAndAddress contactAndAddress = new ContactAndAddress();
        contactAndAddress.setAddress(createAddress(addressDetail));
        contactAndAddress.setContact(createContact(contactDetail));

        return contactAndAddress;
    }

    /**
     * This method is used to return Contact
     * 
     * @return
     */
    private Contact createContact(
            ContactDetail contactDetail) {

        Contact contact = null;
        if (contactDetail != null) {
            contact = new Contact();
            contact.setCompanyName(contactDetail.getCompanyName());
            contact.setContactId(contactDetail.getContactId());
            contact.setEMailAddress(contactDetail.getEmailId());
            contact.setPersonName(contactDetail.getFullName());
            contact.setPhoneNumber(contactDetail.getPhoneNumber());
            contact.setPhoneExtension(contactDetail.getPhoneExtension());
        }

        return contact;
    }

    /**
     * This method is used to return Address
     * 
     * @return
     */
    private Address createAddress(
            AddressDetail addressDetail) {

        Address address = null;
        if (addressDetail != null) {
            address = new Address();
            address.setStateOrProvinceCode(addressDetail.getStateOrProvinceCode());
            address.setPostalCode(addressDetail.getPostalCode());
            address.setCountryCode(addressDetail.getCountryCode());
            address.setCity(addressDetail.getCity());
            address.setResidential(addressDetail.isResidential());
            if (!CollectionUtils.isEmpty(addressDetail.getStreetLines())) {
                address.setStreetLines(addressDetail.getStreetLines()
                        .stream()
                        .toArray(String[]::new));
            }
        }
        return address;
    }

    /**
     * Model for RequestedPackageLineItem[]
     * 
     * @param requestedPackageLineItems
     * @return
     */
    private RequestedPackageLineItem[] createRequestedPackageLineItems(
            List<RequestedPackageLineItemDetail> requestedPackageLineItems,  String serviceType) {

        List<RequestedPackageLineItem> listofRequestedPackageLineItem = new ArrayList<>();

        if (!CollectionUtils.isEmpty(requestedPackageLineItems)) {
            for (RequestedPackageLineItemDetail requestedPackageLineItem : requestedPackageLineItems) {

                RequestedPackageLineItem requestedPackageLineItm = new RequestedPackageLineItem();
                requestedPackageLineItm.setDimensions(createDimensions(requestedPackageLineItem.getDimensions()));
                requestedPackageLineItm.setInsuredValue(createInsuredValue(requestedPackageLineItem.getInsuredValue()));
                requestedPackageLineItm.setWeight(createWeight(requestedPackageLineItem.getWeight()));
                requestedPackageLineItm.setSequenceNumber(requestedPackageLineItem.getSequenceNumber());
                requestedPackageLineItm.setGroupNumber(requestedPackageLineItem.getGroupNumber());
                if (requestedPackageLineItem.getGroupPackageCount() != null) {
                    requestedPackageLineItm.setGroupPackageCount(requestedPackageLineItem.getGroupPackageCount());
                }
                requestedPackageLineItm
                        .setSpecialServicesRequested(createPackageSpecialService(requestedPackageLineItem, serviceType));
                requestedPackageLineItm.setSpecialHandlingDetail(createSpecialHandling(
                        requestedPackageLineItem.getSpecialHandlingDetail()));
                requestedPackageLineItm.setCustomerReferences(
                        createCustomerReferences(requestedPackageLineItem.getCustomerReferences()));

                listofRequestedPackageLineItem.add(requestedPackageLineItm);
            }
        }

        return listofRequestedPackageLineItem.stream()
                .toArray(RequestedPackageLineItem[]::new);
    }

    /**
     * This method is used to return PackageSpecialHandlingDetail
     * 
     * @param specialHandlingInspectionStatus
     * @param packedBy
     * @return PackageSpecialHandlingDetail
     */

    private PackageSpecialHandlingDetail createSpecialHandling(
            String specialHandlingInspectionStatus) {

        PackageSpecialHandlingDetail packSpecDetail = new PackageSpecialHandlingDetail();
        switch (specialHandlingInspectionStatus) {
            case "CUSTOMER":
                packSpecDetail.setInspectionStatus(PackageSpecialHandlingType.CUSTOMER.getInspectionStatus());
                packSpecDetail.setDamageKnown(PackageSpecialHandlingType.CUSTOMER.getDamageStatus());
                packSpecDetail.setPackedBy(PackageSpecialHandlingType.CUSTOMER.getPackedBy());
                break;
            case "FEDEX_OFFICE":
                packSpecDetail.setInspectionStatus(PackageSpecialHandlingType.FEDEX_OFFICE.getInspectionStatus());
                packSpecDetail.setDamageKnown(PackageSpecialHandlingType.FEDEX_OFFICE.getDamageStatus());
                packSpecDetail.setPackedBy(PackageSpecialHandlingType.FEDEX_OFFICE.getPackedBy());
                break;
            case "FEDEX_DAMAGE_KNOWN":
                packSpecDetail.setInspectionStatus(PackageSpecialHandlingType.FEDEX_DAMAGE_KNOWN.getInspectionStatus());
                packSpecDetail.setDamageKnown(PackageSpecialHandlingType.FEDEX_DAMAGE_KNOWN.getDamageStatus());
                packSpecDetail.setPackedBy(PackageSpecialHandlingType.FEDEX_DAMAGE_KNOWN.getPackedBy());
                break;
            case "INSPECTED":
                packSpecDetail.setInspectionStatus(PackageSpecialHandlingType.INSPECTED.getInspectionStatus());
                packSpecDetail.setDamageKnown(PackageSpecialHandlingType.INSPECTED.getDamageStatus());
                packSpecDetail.setPackedBy(PackageSpecialHandlingType.INSPECTED.getPackedBy());
                break;
            case "INSPECTED_DAMAGE_KNOWN":
                packSpecDetail
                        .setInspectionStatus(PackageSpecialHandlingType.INSPECTED_DAMAGE_KNOWN.getInspectionStatus());
                packSpecDetail.setDamageKnown(PackageSpecialHandlingType.INSPECTED_DAMAGE_KNOWN.getDamageStatus());
                packSpecDetail.setPackedBy(PackageSpecialHandlingType.INSPECTED_DAMAGE_KNOWN.getPackedBy());
                break;
            case "CUSTOMER_REFUSED_INSPECTION":
                packSpecDetail.setInspectionStatus(
                        PackageSpecialHandlingType.CUSTOMER_REFUSED_INSPECTION.getInspectionStatus());
                packSpecDetail.setDamageKnown(PackageSpecialHandlingType.CUSTOMER_REFUSED_INSPECTION.getDamageStatus());
                packSpecDetail.setPackedBy(PackageSpecialHandlingType.CUSTOMER_REFUSED_INSPECTION.getPackedBy());
                break;
            case "REFUSED_INSPECTION_DAMAGE_KNOWN":
                packSpecDetail.setInspectionStatus(
                        PackageSpecialHandlingType.REFUSED_INSPECTION_DAMAGE_KNOWN.getInspectionStatus());
                packSpecDetail
                        .setDamageKnown(PackageSpecialHandlingType.REFUSED_INSPECTION_DAMAGE_KNOWN.getDamageStatus());
                packSpecDetail.setPackedBy(PackageSpecialHandlingType.REFUSED_INSPECTION_DAMAGE_KNOWN.getPackedBy());
                break;
            default:
                break;
        }
        return packSpecDetail;
    }

    /**
     * This method is used to return PackageSpecialServicesRequested
     * 
     * @param requestedPackageLineItem
     * @return PackageSpecialServicesRequested
     */
    private PackageSpecialServicesRequested createPackageSpecialService(
            RequestedPackageLineItemDetail requestedPackageLineItem,
            String serviceType) {

        PackageSpecialServicesRequested packSpeService = null;
        if (requestedPackageLineItem.getPackageSpecialServicesRequested() != null
                && !ObjectUtils.isEmpty(requestedPackageLineItem.getPackageSpecialServicesRequested()
                        .getSpecialServicesRequested())) {

            packSpeService = serviceUtil.createPackageSpecialService(requestedPackageLineItem, serviceType);

            if (requestedPackageLineItem.getPackageSpecialServicesRequested()
                    .getDryIceWeightDetails() != null) {
                packSpeService
                        .setDryIceWeight(createWeight(requestedPackageLineItem.getPackageSpecialServicesRequested()
                                .getDryIceWeightDetails()));
            }
            if (!CollectionUtils.isEmpty(requestedPackageLineItem.getPackageSpecialServicesRequested()
                    .getBatteryClassifications())
                    && !(ServiceType.FEDEX_GROUND.name()
                            .equalsIgnoreCase(serviceType)
                            || ServiceType.GROUND_HOME_DELIVERY.name()
                                    .equalsIgnoreCase(serviceType))) {
                packSpeService.setBatteryDetails(
                        createBatteryClassification(requestedPackageLineItem.getPackageSpecialServicesRequested()
                                .getBatteryClassifications()));
            }
            packSpeService.setSignatureOptionDetail(
                    createShipmentSpecialService(requestedPackageLineItem.getPackageSpecialServicesRequested()
                            .getSpecialServicesRequested()));
        }

        return packSpeService;
    }

    /**
     * This method is used to return SignatureOptionDetail
     * 
     * @param specialServiceDetail
     * @return SignatureOptionDetail
     */
    private SignatureOptionDetail createShipmentSpecialService(
            List<SpecialServiceDescriptionDetail> specialServiceDetail) {

        SignatureOptionDetail signOptDetail = null;
        if (!CollectionUtils.isEmpty(specialServiceDetail)) {
            for (SpecialServiceDescriptionDetail signature : specialServiceDetail) {
                if (signature.getSpecialServiceType()
                        .equalsIgnoreCase("SIGNATURE_OPTION")) {
                    signOptDetail = new SignatureOptionDetail();
                    if (signature.getSpecialServiceSubType()
                            .equalsIgnoreCase("NO_SIGNATURE_REQUIRED")) {
                        signOptDetail.setSignatureReleaseNumber(NO_SIGNATURE_RELEASE_NUMBER);
                    }
                    signOptDetail.setOptionType(signature.getSpecialServiceSubType());
                }
            }

        }

        return signOptDetail;
    }


    /**
     * This method is used to return Dimensions
     * 
     * @param dimensionDetails
     * @return
     */
    private Dimensions createDimensions(
            DimensionDetail dimensionDetails) {

        Dimensions dimensions = null;
        if (dimensionDetails != null) {
            dimensions = new Dimensions();
            dimensions.setHeight(dimensionDetails.getHeight());
            dimensions.setLength(dimensionDetails.getLength());
            dimensions.setWidth(dimensionDetails.getWidth());
            dimensions.setUnits(dimensionDetails.getUnits());
        }

        return dimensions;
    }

    /**
     * This method is used to return Weight
     * 
     * @param weightDetails
     * @return
     */
    private Weight createWeight(
            WeightDetail weightDetails) {

        Weight weight = null;
        if (weightDetails != null) {
            weight = new Weight();
            weight.setValue(Double.parseDouble(weightDetails.getValue()));
            weight.setUnits(weightDetails.getUnit());
        }

        return weight;
    }

    /**
     * This method is used to return money
     * 
     * @param insuredValue
     * @return
     */
    private Money createInsuredValue(
            InsuredValueDetail insuredValue) {

        Money money = null;
        if (insuredValue != null) {
            money = new Money();
            money.setAmount(Double.parseDouble(insuredValue.getAmount()));
            money.setCurrency(insuredValue.getCurrency());
        }

        return money;
    }

    /**
     * This method is used to return TransactionDetail
     * 
     * @return
     */
    private TransactionDetail getTransactionDetail(
            String requestedDateTime,
            String locationCode) {

        Localization localization = new Localization();
        TransactionDetail transactionDetail = new TransactionDetail();
        transactionDetail.setCustomerTransactionId(serviceUtil
                .getCustomerTransactionId(serviceConfig.getClientApplicationId(), requestedDateTime, locationCode));
        transactionDetail.setTracing(false);
        transactionDetail.setLocalization(localization);

        return transactionDetail;
    }

    /**
     * Method to build request for confirm open shipment
     * 
     * @param confirmShipmentRequest
     * @return
     */
    public ConfirmOpenShipmentRequest buildConfirmRequest(
            ShipmentConfirmationRequest confirmShipmentRequest) {

        ConfirmOpenShipmentRequest confirmOpenShipmentRequest = new ConfirmOpenShipmentRequest();
        confirmOpenShipmentRequest.setClientDetail(
                ShipServiceClientMapper.INSTANCE.toModel(confirmShipmentRequest.getWorkstationDetail()));
        confirmOpenShipmentRequest.setTransactionDetail(getTransactionDetail(
                confirmShipmentRequest.getShipmentTimeStamp(), confirmShipmentRequest.getTransactionLocationId()));
        confirmOpenShipmentRequest
                .setVersion(ShipServiceClientMapper.INSTANCE.toModel(this.serviceConfig.getVersionId()));
        confirmOpenShipmentRequest.setApplicationId(serviceConfig.getClientApplicationId());
        confirmOpenShipmentRequest.setIndex(confirmShipmentRequest.getReferenceId());
        confirmOpenShipmentRequest
                .setLabelSpecification(createLabelSpecification(confirmShipmentRequest.getLabelSpecificationDetail()));
        confirmOpenShipmentRequest.setAsynchronousProcessingOptions(createAsynchronousProcessingOptionsRequested());

        return confirmOpenShipmentRequest;
    }

    /**
     * This method is used to return AsynchronousProcessingOptionsRequested
     * 
     * @return
     */
    private AsynchronousProcessingOptionsRequested createAsynchronousProcessingOptionsRequested() {

        AsynchronousProcessingOptionsRequested asynchronousProcessingDetail =
                new AsynchronousProcessingOptionsRequested();
        List<String> asynchronousProcessingTypeList = new ArrayList<>();
        asynchronousProcessingTypeList.add(DEFAULT_PROCESSING_OPTION_TYPE);
        asynchronousProcessingDetail.setOptions(asynchronousProcessingTypeList.stream()
                .toArray(String[]::new));

        return asynchronousProcessingDetail;
    }

    /**
     * Model for BatteryClassificationDetail[]
     * 
     * @param batteryClassificationTypes
     * @return
     */
    private BatteryClassificationDetail[] createBatteryClassification(
            List<BatteryClassificationType> batteryClassificationTypes) {

        BatteryClassificationDetail[] batteryClassificationDetails =
                new BatteryClassificationDetail[batteryClassificationTypes.size()];
        BatteryClassificationDetail batteryClassificationDetail = null;
        for (int i = 0; i < batteryClassificationTypes.size(); i++) {
            batteryClassificationDetail = new BatteryClassificationDetail();
            batteryClassificationDetail.setMaterial(batteryClassificationTypes.get(i)
                    .getMaterial());
            batteryClassificationDetail.setPacking(batteryClassificationTypes.get(i)
                    .getPacking());
            batteryClassificationDetail.setRegulatorySubType(batteryClassificationTypes.get(i)
                    .getRegulatorySubType());
            batteryClassificationDetails[i] = batteryClassificationDetail;
        }
        return batteryClassificationDetails;
    }

    /**
     * Model for ShippingCustomerReferenceDetail[]
     * 
     * 
     * @param customerReferences
     * @return
     */
    private CustomerReference[] createCustomerReferences(
            List<ShippingCustomerReferenceDetails> customerReferenceList) {

        List<CustomerReference> customerReferences = new ArrayList<>();
        if (!CollectionUtils.isEmpty(customerReferenceList)) {

            for (ShippingCustomerReferenceDetails customerReference : customerReferenceList) {

                CustomerReference custRefernce = new CustomerReference();
                custRefernce.setValue(customerReference.getValue());
                custRefernce.setCustomerReferenceType(customerReference.getReferenceType());
                customerReferences.add(custRefernce);
            }
        }

        return customerReferences.stream()
                .toArray(CustomerReference[]::new);
    }

    /**
     * This method is used to set email notifications
     * 
     * @param requestedShipment
     * @return
     */
    private ShipmentEventNotificationDetail createEmailNotificationDetail(
            RequestedShipmentDetail requestedShipment) {

        ShipmentEventNotificationDetail notification = null;
        List<ShipmentEventNotificationSpecification> eventNotifications = new ArrayList<>();
        if (requestedShipment.getSender() != null && requestedShipment.getSender()
                .getContact() != null
                && StringUtils.isNotEmpty(requestedShipment.getSender()
                        .getContact()
                        .getEmailId())) {

            eventNotifications.add(createShipmentEventNotification(ShipmentNotificationRoleType.SHIPPER.name(),
                    requestedShipment.getSender()
                            .getContact()
                            .getEmailId()));
        }
        if (requestedShipment.getRecipient() != null && requestedShipment.getRecipient()
                .getContact() != null
                && StringUtils.isNotEmpty(requestedShipment.getRecipient()
                        .getContact()
                        .getEmailId())) {

            eventNotifications.add(createShipmentEventNotification(ShipmentNotificationRoleType.RECIPIENT.name(),
                    requestedShipment.getRecipient()
                            .getContact()
                            .getEmailId()));
        }
        if (requestedShipment.getCustomer() != null && requestedShipment.getCustomer()
                .getContact() != null) {
            // set eventNotification for customer primary emailId
            if (StringUtils.isNotEmpty(requestedShipment.getCustomer()
                    .getContact()
                    .getEmailId())) {

                eventNotifications.add(createShipmentEventNotification(ShipmentNotificationRoleType.OTHER.name(),
                        requestedShipment.getCustomer()
                                .getContact()
                                .getEmailId()));
            }
            // set eventNotification for customer alternate emailId
            if (StringUtils.isNotEmpty(requestedShipment.getCustomer()
                    .getContact()
                    .getAlternateEmailId())) {
                eventNotifications.add(createShipmentEventNotification(ShipmentNotificationRoleType.OTHER.name(),
                        requestedShipment.getCustomer()
                                .getContact()
                                .getAlternateEmailId()));

            }
        }
        if (!CollectionUtils.isEmpty(eventNotifications)) {
            notification = new ShipmentEventNotificationDetail();
            notification.setEventNotifications(eventNotifications.stream()
                    .toArray(ShipmentEventNotificationSpecification[]::new));
        }

        return notification;
    }

    /**
     * This common method is used to set email notifications
     * 
     * @param notificationRoleType
     * @param emailId
     * @return
     */
    private ShipmentEventNotificationSpecification createShipmentEventNotification(
            String notificationRoleType,
            String emailId) {

        ShipmentEventNotificationSpecification specification = new ShipmentEventNotificationSpecification();
        specification.setRole(notificationRoleType);
        specification.setEvents(EVENTS);
        NotificationDetail notificationDetail = new NotificationDetail();
        EMailDetail emailDetail = new EMailDetail();
        emailDetail.setEmailAddress(emailId);
        notificationDetail.setEmailDetail(emailDetail);
        Localization localization = new Localization();
        localization.setLanguageCode(LANGUAGE_CODE);
        notificationDetail.setLocalization(localization);
        specification.setNotificationDetail(notificationDetail);

        ShipmentNotificationFormatSpecification formatSpecification = new ShipmentNotificationFormatSpecification();
        formatSpecification.setType(FORMAT_SPECIFICATION);
        specification.setFormatSpecification(formatSpecification);

        return specification;
    }

    /**
     * This method will set the home delivery details for create Open shipment call
     * 
     * @param homeDeliveryDetail
     * @return
     */
    private HomeDeliveryPremiumDetail createHomeDeliveryPremiumDetail(
            HomeDelvDetail homeDeliveryDetail) {

        HomeDeliveryPremiumDetail homeDeliveryPremiumDetail = new HomeDeliveryPremiumDetail();
        homeDeliveryPremiumDetail.setHomeDeliveryPremiumType(homeDeliveryDetail.getHomeDeliveryType()
                .name());
        homeDeliveryPremiumDetail.setDate(homeDeliveryDetail.getDeliveryDate());
        if (homeDeliveryDetail.getPhoneNumber() != null) {
            homeDeliveryPremiumDetail.setPhoneNumber(homeDeliveryDetail.getPhoneNumber()
                    .getNumber());
        }

        return homeDeliveryPremiumDetail;
    }
}
