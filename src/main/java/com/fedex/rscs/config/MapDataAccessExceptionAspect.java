package com.fedex.rscs.config;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.fedex.common.cxs.exception.DbAccessException;
import com.fedex.common.cxs.exception.ExceptionsHelper;

/**
 * Configure the application so that exceptions are mapped appropriately between layers
 *
 * @see ExceptionsHelper
 */
@Aspect
@Component
public class MapDataAccessExceptionAspect {


    @Pointcut("within(@org.springframework.stereotype.Component com.fedex.rscs..*)")
    public void isFedExComponent() {
        // intentionally empty
    }

    @Pointcut("within(@org.springframework.stereotype.Service com.fedex.rscs..*)")
    public void isFedExService() {
        // intentionally empty
    }

    @Pointcut("within(@org.springframework.stereotype.Repository com.fedex.rscs..*)")
    public void isFedExRepository() {
        // intentionally empty
    }

    @Pointcut("execution(public * *(..))")
    public void isPublicMethod() {
        // intentionally empty
    }

    /**
     * Intercept all <b>public</b> methods of a FedEx code declaring a @Service or @Component and immediately
     * execute them.  Capture any internal FedEx DataAccessExceptions thrown and map them to an appropriate
     * ProcessorException.
     *
     * @param proceedingJoinPoint context and control for the method that we're intercepting
     * @throws Throwable Any exception that might bubble out
     */
    // @Around commented to skip CXS error conversion for component and service stereotype.
    // @Around("isPublicMethod() && (isFedExComponent() || isFedExService())") 
    public Object mapServiceExceptions(final ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        Object returnValue = null;
        try {
            returnValue = proceedingJoinPoint.proceed();
        } catch (final DbAccessException originalEx) {
            ExceptionsHelper.handleFedExProcessorException(originalEx);
        }

        return returnValue;
    }
    
    /**
     * Intercept all <b>public</b> methods of a FedEx code declaring a @Repository and immediately
     * execute them. Capture any internal spring DataAccessExceptions thrown and map them to an
     * appropriate internal FedEx DbAccessException..
     *
     * @param proceedingJoinPoint context and control for the method that we're intercepting
     * @throws Throwable Any exception that might bubble out
     */
    @Around("isPublicMethod() && isFedExRepository()")
    public Object mapRepoExceptions(
            final ProceedingJoinPoint proceedingJoinPoint)
            throws Throwable {

        Object returnValue = null;
        try {
            returnValue = proceedingJoinPoint.proceed();
        } catch (final DataAccessException originalEx) {
            ExceptionsHelper.handleDataAccessException(originalEx);
        }

        return returnValue;
    }

}
