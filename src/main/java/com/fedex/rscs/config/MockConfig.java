package com.fedex.rscs.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;



/**
 * Beans declared in this class will only be used if the "mock" profile is active
 *
 * This profile is suitable for system testing or running the application when remote access is not available.
 *
 * Unit tests can use this profile, but whenever possible should favor directly instantiating the code under test
 * without use of spring context.
 */
@Configuration
@Profile("mock")
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class MockConfig {

}
