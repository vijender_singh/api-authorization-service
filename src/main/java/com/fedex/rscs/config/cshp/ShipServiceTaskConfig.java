package com.fedex.rscs.config.cshp;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Defines the beans required for ship service event asynchronous task.
 * 
 * @author Samicheen Khariwal
 *
 */
@Configuration
@EnableAsync
public class ShipServiceTaskConfig {
    
    @Bean("cshpEventsQueue")
    public TaskExecutor getScanEventTaskExecutor(
            ShipServiceEventTaskConfiguration shipServiceEventTaskConfigurations) {

        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(shipServiceEventTaskConfigurations.getPoolSize());
        executor.setMaxPoolSize(shipServiceEventTaskConfigurations.getMaxpoolSize());
        executor.setWaitForTasksToCompleteOnShutdown(shipServiceEventTaskConfigurations.isWaitForCompletion());
        executor.setThreadNamePrefix(shipServiceEventTaskConfigurations.getPrefix());
        return executor;
    }

}
