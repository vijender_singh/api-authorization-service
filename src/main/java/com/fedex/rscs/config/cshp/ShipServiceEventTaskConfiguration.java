package com.fedex.rscs.config.cshp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Component to maintain the Configuration for ScanEvent task.
 * 
 * @author Samicheen Khariwal
 *
 */
@Configuration
public class ShipServiceEventTaskConfiguration {
    
    @Value("${shipServiceEvents.task.pool.size:20}")
    private Integer poolSize;

    @Value("${shipServiceEvents.task.pool.max.size:1000}")
    private Integer maxpoolSize;

    @Value("${shipServiceEvents.task.completion.wait:true}")
    private boolean waitForCompletion;

    @Value("${shipServiceEvents.task.name.prefix:ShipServiceEvent}")
    private String prefix;

    /**
     * @return
     */
    public Integer getPoolSize() {

        return poolSize;
    }


    /**
     * @return
     */
    public Integer getMaxpoolSize() {

        return maxpoolSize;
    }


    /**
     * @return
     */
    public boolean isWaitForCompletion() {

        return waitForCompletion;
    }


    /**
     * @return
     */
    public String getPrefix() {

        return prefix;
    }
}
