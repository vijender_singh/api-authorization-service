package com.fedex.rscs.config.cshp;

import java.util.Properties;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.fedex.nxgen.servicefactory.v2.ServiceFactory;
import com.fedex.nxgen.servicefactory.v2.ServiceFactoryException;
import com.fedex.nxgen.ship.v17.ientities.ShipInterface;
import com.fedex.rscs.model.VersionDetail;


/**
 * Configuration class for CSHP service
 * 
 * @author Ankit.Bisht
 *
 */
@Configuration
@ComponentScan(basePackages = "com.fedex.nxgen.redux")
public class ShipServiceConfiguration {

    @Value("${cshp.destination}")
    private String destination;

    @Value("${cshp.ejb.url}")
    private String ejbUrl;

    @Value("${cshp.fast.fail.impl}")
    private String failImpl;

    @Value("${cshp.max.concurrent.connect.attempts}")
    private String maxConnectAttempts;

    @Value("${cshp.max.concurrent.invoke.attempts}")
    private String maxInvokeAttempts;

    @Value("${cshp.service.reset.interval.msecs}")
    private String resetInterval;

    @Value("${cshp.service.timeout.interval.msecs}")
    private String timeoutInterval;

    @Value("${cshp.service.timeout.threshold}")
    private String timeoutThreshold;

    @Value("${cshp.jndi.name}")
    private String jndiName;
    
    @Value("${cshp.jndi.request.timeout.msecs}")
    private String jndiTimeout;
    
    @Value("${cshp.version.serviceId}")
    private String serviceId;
    
    @Value("${cshp.version.intermediate}")
    private String intermediateVersion;

    @Value("${cshp.version.major}")
    private String majorVersion;

    @Value("${cshp.version.minor}")
    private String minorVersion;

    @Value("${cshp.auth.remote.appId:APP315}")
    private String authRemoteAppId;

    @Value("${cshp.auth.token.provider:com.fedex.nxgen.redux.ReduxTokenGeneratorImpl}")
    private String authTokenProvider;

    @Value("${cshp.auth.enabled:false}")
    private boolean authEnabled;

    @Value("${cshp.client.applicationId:rscs}")
    private String clientApplicationId;
    
    /**
     * Map properties to CSHP service properties object.
     */
    @Bean(name = "cshpConfiguration")
    public Properties cshpServiceProperties() {

        Properties cshpServiceConfigProperties = new Properties();
        cshpServiceConfigProperties.setProperty("destination", destination);
        cshpServiceConfigProperties.setProperty("ejb.url", ejbUrl);
        cshpServiceConfigProperties.setProperty("fast.fail.impl", failImpl);
        cshpServiceConfigProperties.setProperty("max.concurrent.connect.attempts", maxConnectAttempts);
        cshpServiceConfigProperties.setProperty("max.concurrent.invoke.attempts", maxInvokeAttempts);
        cshpServiceConfigProperties.setProperty("service.reset.interval.msecs", resetInterval);
        cshpServiceConfigProperties.setProperty("service.timeout.interval.msecs", timeoutInterval);
        cshpServiceConfigProperties.setProperty("service.timeout.threshold", timeoutThreshold);
        cshpServiceConfigProperties.setProperty("cshp.intermediate.version", intermediateVersion);
        cshpServiceConfigProperties.setProperty("jndi.name", jndiName);
        cshpServiceConfigProperties.setProperty("jndi.request.timeout.msecs", jndiTimeout);
        cshpServiceConfigProperties.setProperty("cshp.major.version", majorVersion);
        cshpServiceConfigProperties.setProperty("cshp.minor.version", minorVersion);
        if (authEnabled) {
            cshpServiceConfigProperties.setProperty("remote.app.id", authRemoteAppId);
            cshpServiceConfigProperties.setProperty("token.impl.class", authTokenProvider);
        }
        return cshpServiceConfigProperties;
    }

    /**
     * Obtain an REST interface proxy to invoke cshp methods
     *
     * @return ShipInterface
     * @throws ServiceFactoryException
     */
    @Bean
    @Lazy
    public ShipInterface getCshpServiceInterface(
            @Qualifier("cshpConfiguration") Properties cshpServiceProperties)
            throws ServiceFactoryException {

        return (ShipInterface) ServiceFactory.getServiceInstance(ShipInterface.class, cshpServiceProperties);
    }
    
    /**
     * Method to provide the {@link VersionDetail} using the available configurations.
     * 
     * @return versionId
     */
    public VersionDetail getVersionId() {

        VersionDetail versionId = new VersionDetail();
        versionId.setServiceId(serviceId);
        versionId.setMajor(NumberUtils.isNumber(majorVersion) ? Integer.valueOf(majorVersion) : 17);
        versionId.setIntermediate(NumberUtils.isNumber(intermediateVersion) ? Integer.valueOf(intermediateVersion) : 0);
        versionId.setMinor(NumberUtils.isNumber(minorVersion) ? Integer.valueOf(minorVersion) : 0);
        return versionId;
    }

    /**
     * Method to get the CSHP client application Id.
     * 
     * @return
     */
    public String getClientApplicationId() {

        return clientApplicationId;
    }    

}
