package com.fedex.rscs.config.jms;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Defines the beans required for ground rate JMS service event asynchronous task.
 * 
 * @author Vijender Singh
 *
 */
@Configuration
@EnableAsync
public class GroundRateJMSServiceTaskConfig {

    @Bean("groundRateJMSPublishQueue")
    public TaskExecutor getGroundRateEventTaskExecutor(
            GroundRateJMSEventTaskConfiguration groundRateEventTaskConfiguration) {

        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(groundRateEventTaskConfiguration.getPoolSize());
        executor.setMaxPoolSize(groundRateEventTaskConfiguration.getMaxpoolSize());
        executor.setWaitForTasksToCompleteOnShutdown(groundRateEventTaskConfiguration.isWaitForCompletion());
        executor.setThreadNamePrefix(groundRateEventTaskConfiguration.getPrefix());
        return executor;
    }

}
