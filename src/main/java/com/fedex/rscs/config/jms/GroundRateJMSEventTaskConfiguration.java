package com.fedex.rscs.config.jms;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Component to maintain the Configuration for Ground Rate event task.
 * 
 * @author Vijender Singh
 *
 */
@Configuration
public class GroundRateJMSEventTaskConfiguration {

    @Value("${groundRateJMSEvents.task.pool.size:20}")
    private Integer poolSize;

    @Value("${groundRateJMSEvents.task.pool.max.size:1000}")
    private Integer maxpoolSize;

    @Value("${groundRateJMSEvents.task.completion.wait:false}")
    private boolean waitForCompletion;

    @Value("${groundRateJMSEvents.task.name.prefix:GroundRateJMSService}")
    private String prefix;

    /**
     * @return
     */
    public Integer getPoolSize() {

        return poolSize;
    }

    /**
     * @return
     */
    public Integer getMaxpoolSize() {

        return maxpoolSize;
    }

    /**
     * @return
     */
    public boolean isWaitForCompletion() {

        return waitForCompletion;
    }

    /**
     * @return
     */
    public String getPrefix() {

        return prefix;
    }
}
