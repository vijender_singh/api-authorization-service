package com.fedex.rscs.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Beans declared here will be used if both mock and cloud profiles are disabled. 
 * This will run the service in "standalone" mode, i.e. "local" mode.  This mode of operation is suitable
 * for running the application on a laptop or on a VM outside of a PaaS ecosystem.
 */
@Configuration
@Profile({"!cloud","!mock"})
@Conditional(value={AndProfilesCondition.class})
@EnableCircuitBreaker
@ConfigurationProperties
public class NonCloudConfig {

}
