package com.fedex.rscs.config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.Profile;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.util.MultiValueMap;

/**
 * A {@link Condition} intended to be used by spring's {@link org.springframework.context.annotation.Conditional}
 * annotation.  This allows you to specify multiple profiles in a {@link Profile} annotation and have it only be
 * activated if both profiles are active.
 *
 * <code>
 *     @Configuration
 *     @Profile({"!cloud","!mock"})
 *     @Conditional(value={AndProfilesCondition.class})
 *     public class MySimpleConfiguration {
 *         // this configuration is loaded only if both the cloud and mock profiles are NOT active
 *     }
 * </code>
 */
public class AndProfilesCondition implements Condition {

    private static final String VALUE = "value";
    private static final String DEFAULT_PROFILE = "default";

    @Override
    public boolean matches(final ConditionContext context, final AnnotatedTypeMetadata metadata) {
        if (context.getEnvironment() == null) {
            return true;
        }
        MultiValueMap<String, Object> attrs = metadata.getAllAnnotationAttributes(Profile.class.getName());
        if (attrs == null) {
            return true;
        }

        Set<String> activeProfilesSet = Arrays.stream(context.getEnvironment().getActiveProfiles())
                .collect(Collectors.toSet());
        String[] definedProfiles = (String[]) attrs.getFirst(VALUE);
        Set<String> allowedProfiles = new HashSet<>(1);
        Set<String> restrictedProfiles = new HashSet<>(1);
        if (activeProfilesSet.isEmpty()) {
            activeProfilesSet.add(DEFAULT_PROFILE);
        }
        for (String nextDefinedProfile : definedProfiles) {
            if (!nextDefinedProfile.isEmpty() && nextDefinedProfile.charAt(0) == '!') {
                restrictedProfiles.add(nextDefinedProfile.substring(1, nextDefinedProfile.length()));
                continue;
            }
            allowedProfiles.add(nextDefinedProfile);
        }
        boolean allowed = true;
        for (String allowedProfile : allowedProfiles) {
            allowed = allowed && activeProfilesSet.contains(allowedProfile);
        }
        boolean restricted = true;
        for (String restrictedProfile : restrictedProfiles) {
            restricted = restricted && !activeProfilesSet.contains(restrictedProfile);
        }
        return allowed && restricted;
    }

}
