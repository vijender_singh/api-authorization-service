package com.fedex.rscs.config;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * This component provides the data source specific configurations.
 * 
 * @author shashank jain
 *
 */
@Configuration
@EnableTransactionManagement
@Profile("!mock")
public class DataSourceConfig {

    @Value("${spring.datasource.username}")
    private String dbUsername;
    @Value("${spring.datasource.url}")
    private String dbUrl;
    @Value("${spring.datasource.password}")
    private String dbPassword;
    @Value("${spring.datasource.driver-class-name}")
    private String dbDriverClass;

    /**
     * Provides the {@link DataSource} bean based on the provided configurations.
     * 
     * @return
     */
    @Bean
    public DataSource dataSource() {

        return DataSourceBuilder.create()
                .driverClassName(dbDriverClass)
                .url(dbUrl)
                .username(dbUsername)
                .password(dbPassword)
                .build();
    }

    /**
     * Provides the {@link JdbcTemplate} bean based on the provided and configured data source.
     * 
     * @param ds
     * @return
     */
    @Bean
    public JdbcTemplate jdbcTemplate(
            DataSource ds) {

        return new JdbcTemplate(ds);
    }

    /**
     * Provides the {@link NamedParameterJdbcTemplate} bean based on the provided and configured
     * data source.
     * 
     * @param ds
     * @return
     */
    @Bean
    public NamedParameterJdbcTemplate namedJdbcTemplate(
            DataSource ds) {

        return new NamedParameterJdbcTemplate(ds);
    }
    
    /**
     * Provides the {@link PlatformTransactionManager} bean based on the provided and configured
     * data source.
     * 
     * @param ds
     * @return
     */
	@Bean
	public PlatformTransactionManager transactionManager(DataSource ds) {
		return new DataSourceTransactionManager(ds);
	}

}
