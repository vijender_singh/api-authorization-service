package com.fedex.rscs.config;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

import com.fedex.auth.token.autoconfigure.DefaultOAuthAgentSecurity;

@Configuration
@EnableOAuth2Client
@DefaultOAuthAgentSecurity(permitGet = {"/health", "/actuator/**", "/v2/api-docs/**", "/swagger-resources/**",
        "/swagger-ui/**"}, permitPost = {"/actuator/refresh"})
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${security.oauth.client.client-id}")
    private String clientId;

    @Value("${security.oauth.client.client-secret}")
    private String clientSecret;

    @Value("${security.oauth.client.access-token-uri}")
    private String accessTokenUri;

    @Override
    public void configure(
            WebSecurity webSecurity)
            throws Exception {

        webSecurity.ignoring()
                .requestMatchers(EndpointRequest.to("health"));
    }

    /**
     * Bean to get the instance of {@link ClientCredentialsResourceDetails} for TAZ based token
     * authorization.
     * 
     * @return
     */
    @Bean
    public ClientCredentialsResourceDetails defaultOAuth2Resource() {

        ClientCredentialsResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
        resourceDetails.setClientId(clientId);
        resourceDetails.setClientSecret(clientSecret);
        resourceDetails.setAccessTokenUri(accessTokenUri);
        resourceDetails.setId("taz_default_resource");
        return resourceDetails;
    }

    /**
     * Bean to get the instance of {@link OAuth2RestTemplate} for TAZ token based authorization for
     * communication with FedEx CXS services.
     * 
     * @param oauth2ClientContext
     * @param resource
     * @return
     */
    @Bean
    public OAuth2RestTemplate oauthUserRestTemplate(
            OAuth2ClientContext oauth2ClientContext,
            @Qualifier("defaultOAuth2Resource") OAuth2ProtectedResourceDetails resource) {

        return new OAuth2RestTemplate(resource, new DefaultOAuth2ClientContext());
    }
}
