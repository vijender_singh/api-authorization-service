package com.fedex.rscs.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.config.java.AbstractCloudConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Beans declared in this class will only be used if the "cloud" profile is enabled
 * This profile should only be used when deploying to a PaaS environment such as Cloud Foundry
 * Use the default (i.e. local) profile instead for any other deployment type including cloudops
 */
@Configuration
@Profile("cloud")
@EnableCircuitBreaker
@ConfigurationProperties
public class CloudConfig extends AbstractCloudConfig {

}
