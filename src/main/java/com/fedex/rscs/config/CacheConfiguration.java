package com.fedex.rscs.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.benmanes.caffeine.cache.Caffeine;

/**
 * This component is used to enable the caching capability in application and to define all the
 * configurations related to caching used in application.
 * 
 * @author shashank.jain
 *
 */
@EnableCaching
@Configuration
public class CacheConfiguration {

    public static final String LOCATION_CACHE = "locations_cache";
    public static final String INTERLINE_CACHE = "interline_cache";

    /**
     * This beans initialize the cache manager instance and register required cache entries into it
     * using {@link CaffeineCache} cache provider.
     *
     * @return the cache manager
     */
    @Bean
    public CacheManager cacheManager() {

        // Since the
        // cache is built in application memory, currently keeping it hard coded
        // based on the
        // business need to avoid any performance issues.However configurations
        // may later be
        // externalize after performance testing and evaluation.
        List<CaffeineCache> caches = new ArrayList<>();
        final CaffeineCache locationsCache = buildCache(LOCATION_CACHE, 2000, 86400);
        caches.add(locationsCache);
        // Adding interline code cache, max size and expiration time will be changed as per
        // requirement.
        final CaffeineCache interlineCodeCache = buildCache(INTERLINE_CACHE, 2000, 86400);
        caches.add(interlineCodeCache);
        final SimpleCacheManager manager = new SimpleCacheManager();
        manager.setCaches(Arrays.asList(locationsCache));
        manager.setCaches(caches);
        return manager;

    }

    /**
     * Internal method to build the cache with defined cache configurations.
     *
     * @param name : the cache name to be registered
     * @param maxSize the max size of the cache
     * @param expirationTime the expiration time for the cache in seconds.
     * @return the CaffeineCache
     */
    private CaffeineCache buildCache(
            final String name,
            final int maxSize,
            final int expirationTime) {

        return new CaffeineCache(name, Caffeine.newBuilder()
                .expireAfterWrite(expirationTime, TimeUnit.SECONDS)
                .maximumSize(maxSize)
                .build());
    }
}
