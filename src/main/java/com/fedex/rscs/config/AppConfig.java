package com.fedex.rscs.config;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * component to provide the application level configurations.
 * 
 * @author shashank.jain
 *
 */
@Configuration
public class AppConfig {

    @Value("${retailLocation.rest.url}")
    private String rtlServiceUri;
    @Value("${rateService.rest.url}")
    private String rateServiceUri;
    @Value("${pesService.rest.url}")
    private String pesServiceUri;
    @Value("${checkout.async.enabled:true}")
    private boolean asyncCheckout;
    @Value("${supportService.rest.url}")
    private String supportServiceUri;
    @Value("${executor.fixedThreadPool.size:3}")
    private int executorThreadPoolSize;
    @Value("${ground.shipping.rating.event:false}")
    private boolean groundShippingEvent;

    /**
     * @return the rtlServiceUri
     */
    public String getRtlServiceUri() {

        return rtlServiceUri;
    }

    public String getRateServiceUri() {

        return rateServiceUri;
    }

    /**
     * @return the pesServiceUri
     */
    public String getPesServiceUri() {

        return pesServiceUri;
    }

    public boolean isAsyncCheckout() {

        return asyncCheckout;
    }

    /**
     * @return the supportServiceUri
     */
    public String getSupportServiceUri() {

        return supportServiceUri;
    }

    /**
     * @return the executorThreadPoolSize
     */
    public int getExecutorThreadPoolSize() {

        return executorThreadPoolSize;
    }

    @Bean
    public ExecutorService executorService() {

        return Executors.newFixedThreadPool(executorThreadPoolSize);
    }

    /**
     * @return the groundShippingEvent
     */
    public boolean isGroundShippingEvent() {

        return groundShippingEvent;
    }

    /**
     * @param groundShippingEvent the groundShippingEvent to set
     */
    public void setGroundShippingEvent(
            boolean groundShippingEvent) {

        this.groundShippingEvent = groundShippingEvent;
    }

}
