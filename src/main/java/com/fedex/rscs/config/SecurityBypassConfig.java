package com.fedex.rscs.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Profile("noauth")
@Configuration
@Order(1)
public class SecurityBypassConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(
            HttpSecurity http)
            throws Exception {

        http.authorizeRequests()
                .anyRequest()
                .permitAll()
                .and()
                .formLogin()
                .disable()
                .httpBasic()
                .disable()
                .csrf()
                .disable()
                // disable page caching
                .headers()
                .cacheControl();

        http.addFilterBefore(getCorsFilter(), ChannelProcessingFilter.class);
    }
    
    public CorsFilter getCorsFilter() {

        return new CorsFilter(urlBasedCorsConfigurationSource());
    }

    public UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource() {

        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowCredentials(true);
        configuration.addAllowedOrigin("*");
        configuration.addAllowedHeader("*");
        configuration.addAllowedMethod("*");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}