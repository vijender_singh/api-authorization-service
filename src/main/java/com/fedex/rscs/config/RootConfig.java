package com.fedex.rscs.config;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;

/**
 * This is the base configuration, applied regardless of what profiles are currently active.
 */
@EnableSwagger2
@Configuration
public class RootConfig {

    @Bean
    public ClientHttpRequestFactory httpRequestFactory(
            HttpClient httpClient) {

        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient);
        factory.setConnectionRequestTimeout(3000);
        factory.setConnectTimeout(3000);
        factory.setReadTimeout(10000);
        return factory;
    }

    @Bean
    public HttpClient httpClient() {

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        return HttpClientBuilder.create()
                .setConnectionManager(connectionManager)
                .build();
    }

    @Bean
    public RestTemplateCustomizer restTemplateCustomizer(
            final ClientHttpRequestFactory factory) {

        return restTemplate -> restTemplate.setRequestFactory(factory);
    }

    @Bean
    public RestTemplate restTemplate(
            ClientHttpRequestFactory factory) {

        return new RestTemplate(factory);
    }

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer objectMapperBuilderCustomizer() {

        return builder -> {
            // by default, jackson automatically ignores unknown properties. That is to say,
            // if a client specified a property "adfasdf" and the request bean doesn't have a matching
            // property, jackson will silently ignore it. We have a CXS requirement to instead
            // return a failure. Here we're configuring Jackson to thrown an exception. This exception
            // is handled as part of ControllerAdvice.
            builder.failOnUnknownProperties(true);

            // by default, jackson will happily accept a request that defines the same property multiple times.
            // This configuration tells jackson "hey, don't do that". The net result is that by enabling
            // this jackson feature, a JsonProcessingException is thrown.
            builder.featuresToEnable(JsonParser.Feature.STRICT_DUPLICATE_DETECTION);
        };
    }

    @Bean
    public MappingJackson2HttpMessageConverter jacksonHttpConverter(
            final Jackson2ObjectMapperBuilder builder) {

        builder.serializationInclusion(JsonInclude.Include.NON_EMPTY);
        return new MappingJackson2HttpMessageConverter(builder.build());
    }

    @Bean
    public Docket updateApi() {

        return new Docket(DocumentationType.SWAGGER_2).groupName("RSCS")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.fedex.rscs.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {

        return new ApiInfoBuilder().title("Retail Shipment Creation Service")
                .description("Retail Shipment Creation Service is used to shipment creation related work.")
                .contact(new Contact("FedEx", "https://www.fedex.com/en-us/home.html", "support@fedex.com"))
                .version("1.0")
                .build();
    }

    @Bean
    public SecurityScheme apiKey() {

        return new ApiKey("mykey", "api_key", "header");
    }
}
