package com.fedex.rscs.exception;

/**
 * This class provides the generic implementation for the {@link ShipmentErrorCode} and can be used
 * to return any custom API error codes and messages.
 * 
 * @author Surbhi.Gupta
 *
 */
public class ShipmentServiceErrorCode implements ErrorCode {

    private String apiErrorCode;
    private String apiMessage;
    private ErrorCategory status;

    /**
     * @param apiErrorCode
     * @param message
     * @param status
     */
    public ShipmentServiceErrorCode(String apiErrorCode, String message, ErrorCategory status) {

        this.apiErrorCode = apiErrorCode;
        this.apiMessage = message;
        this.status = status;
    }

    @Override
    public String getApiErrorCode() {

        return this.apiErrorCode;
    }

    @Override
    public void setApiErrorCode(
            String apiErrorCode) {

        this.apiErrorCode = apiErrorCode;

    }

    @Override
    public String getMessage() {

        return this.apiMessage;
    }

    @Override
    public void setMessage(
            String message) {

        this.apiMessage = message;

    }

    @Override
    public ErrorCategory getStatus() {

        return this.status;
    }

    @Override
    public void setStatus(
            ErrorCategory status) {

        this.status = status;
    }

}
