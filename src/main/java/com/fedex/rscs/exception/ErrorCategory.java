package com.fedex.rscs.exception;

/**
 * ENUM to classify the error category.
 * 
 * @author Surbhi.Gupta
 *
 */
public enum ErrorCategory {

    BAD_REQUEST, SERVICE_UNAVAILABLE, INTERNAL_SERVER_ERROR, INFORMATIONAL
}
