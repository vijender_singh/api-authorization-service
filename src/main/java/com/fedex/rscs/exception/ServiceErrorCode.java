package com.fedex.rscs.exception;

/**
 * This enum provides the instance definition of error codes that will be used in the application
 * internal layers.
 * 
 * @author shashank.jain
 *
 */
public enum ServiceErrorCode implements ErrorCode {

    INVALID_LOCATION_ID("INVALID.LOCATION.ID", "Requested Location details not found.", ErrorCategory.BAD_REQUEST),
    RATING_SERVICE_UNAVAILABLE("RATING.SERVICE.UNAVAILABLE", "Rating service is not available.Please try again later.",
    ErrorCategory.SERVICE_UNAVAILABLE), 
    INTERNAL_SERVER_ERROR("INTERNAL.SERVER.ERROR","Unable to process request.", 
    ErrorCategory.INTERNAL_SERVER_ERROR),
    PES_SERVICE_UNAVAILABLE("PES.SERVICE.UNAVAILABLE", "Package event service is not available.Please try again later.",
    ErrorCategory.SERVICE_UNAVAILABLE),
    SHIPMENT_ALREADY_CONFIRMED("SHIPMENT.ALREADY.CONFIRMED", "Shipment Already Confirmed and paymentRefId matches in transaction table.",
            ErrorCategory.INFORMATIONAL), 
    INVALID_SHIPMENT_DETAILS("INVALID.SHIPMENT.DETAILS", "Invalid shipment details found.", ErrorCategory.BAD_REQUEST),
    DB_SYSTEM_UNAVAILABLE("DB.SYSTEM.UNAVAILABLE", "Unable to process request. Please try again later.",
            ErrorCategory.SERVICE_UNAVAILABLE),
    SHIPMENT_ALREADY_DELETED("SHIPMENT.ALREADY.DELETED", " This shipment is already deleted.",
            ErrorCategory.BAD_REQUEST),
    RETAIL_LOCATION_SERVICE_UNAVAILABLE("RETAIL.LOCATION.SERVICE.UNAVAILABLE", "Unable to process request. Please try again later.",
            ErrorCategory.SERVICE_UNAVAILABLE),
    DB_SEARCH_FAILURE("DB.SEARCH.FAILURE", "Failed to search record in shipment database.", ErrorCategory.BAD_REQUEST),
    CSHP_SYSTEM_UNAVAILABLE("CSHP.SYSTEM.UNAVAILABLE", "Unable to process request. Please try again later.",
            ErrorCategory.SERVICE_UNAVAILABLE),
    SYSTEM_UNAVAILABLE("DB.SYSTEM.UNAVAILABLE", "Unable to process request. Please try again later.",
            ErrorCategory.SERVICE_UNAVAILABLE),
    DB_INSERTION_FAILURE("DB.INSERTION.FAILURE", "Failed to save transaction into database.",
            ErrorCategory.INTERNAL_SERVER_ERROR),   
    DB_DELETION_FAILURE("DB.DELETION.FAILURE", "Failed to delete record from database.",
            ErrorCategory.INTERNAL_SERVER_ERROR),   
    CONFIRM_SHIPMENT_DELETION_FAILURE("CONFIRM.SHIPMENT.DELETION.FAILURE", "Confirmed Shipment Deletion Failed from CSHP",
            ErrorCategory.INTERNAL_SERVER_ERROR),    
    OPEN_SHIPMENT_STATUS_CONFIRMED("OPEN.SHIPMENT.STATUS.CONFIRMED", " Shipment already confirmed from the database",
            ErrorCategory.BAD_REQUEST),
    OPEN_SHIPMENT_STATUS_DELETED("OPEN.SHIPMENT.STATUS.DELETED", " Shipment already deleted from the database",
            ErrorCategory.BAD_REQUEST),    
    DB_UPDATION_FAILURE("DB.UPDATION.FAILURE", "Failed to update transaction into database.",
            ErrorCategory.INTERNAL_SERVER_ERROR),
    INTERLINE_ACCOUNT_SERVICE_UNAVAILABLE("INTERLINE.ACCOUNT.SERVICE.UNAVAILABLE",
            "Interline account service is not available. Please try again later.", ErrorCategory.SERVICE_UNAVAILABLE),
    INVALID_INTERLINE_ACCOUNT("INVALID.INTERLINE.ACCOUNT", "Requested Interline details not found.",
            ErrorCategory.BAD_REQUEST);
    
    private String apiErrorCode;
    private String message;
    private ErrorCategory status;

    /**
     * @param apiErrorCode
     * @param message
     * @param status
     */
    private ServiceErrorCode(String apiErrorCode, String message, ErrorCategory status) {

        this.apiErrorCode = apiErrorCode;
        this.message = message;
        this.status = status;
    }

    /**
     * @return the apiErrorCode
     */
    public String getApiErrorCode() {

        return apiErrorCode;
    }

    /**
     * @param apiErrorCode the apiErrorCode to set
     */
    public void setApiErrorCode(
            String apiErrorCode) {

        this.apiErrorCode = apiErrorCode;
    }

    /**
     * @return the message
     */
    public String getMessage() {

        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(
            String message) {

        this.message = message;
    }

    /**
     * @return the status
     */
    public ErrorCategory getStatus() {

        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(
            ErrorCategory status) {

        this.status = status;
    }

}
