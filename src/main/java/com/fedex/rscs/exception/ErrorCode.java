package com.fedex.rscs.exception;

/**
 * Interface to represent the Internal Error code definition.
 * 
 * @author shashank.jain
 *
 */
public interface ErrorCode {

    /**
     * 
     * @return apiErrorCode
     */
    String getApiErrorCode();

    /**
     * 
     * @param apiErrorCode to set the apiErrorCode.
     */
    void setApiErrorCode(
            String apiErrorCode);

    /**
     * 
     * @return message.
     */
    String getMessage();

    /**
     * 
     * @param message to set message
     */
    void setMessage(
            String message);

    /**
     * 
     * @return status
     */
    ErrorCategory getStatus();

    /**
     * 
     * @param status to set status
     */
    void setStatus(
            ErrorCategory status);

}
