package com.fedex.rscs.exception.client;

import com.fedex.common.cxs.exception.TransientDbAccessException;
import com.fedex.rscs.exception.ErrorCode;

/**
 * Represent the shipment creation Service Transient Exceptions
 * 
 * @author Surbhi.Gupta
 *
 */
public class ClientTransientException extends TransientDbAccessException {

    /**
     * auto generated serialVersionUID
     */
    private static final long serialVersionUID = -8223244528163996975L;

    private String errorMessage;
    private ErrorCode errorCode;

    /**
     * @param errorMessage
     */
    public ClientTransientException(String errorMessage) {

        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    /**
     * @param errorCode
     */
    public ClientTransientException(ErrorCode errorCode) {

        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    /**
     * @param errorMessage
     * @param errorCode
     */
    public ClientTransientException(String errorMessage, ErrorCode errorCode) {

        super(errorMessage);
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {

        return errorMessage;
    }

    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(
            String errorMessage) {

        this.errorMessage = errorMessage;
    }

    
    /**
     * @return the errorCode
     */
    public ErrorCode getShipmentCreationErrorCode() {
    
        return errorCode;
    }

    
    /**
     * @param errorCode the errorCode to set
     */
    public void setShipmentCreationErrorCode(
            ErrorCode errorCode) {
    
        this.errorCode = errorCode;
    }

}
