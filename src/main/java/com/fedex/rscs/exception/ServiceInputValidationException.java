package com.fedex.rscs.exception;

import com.fedex.common.cxs.exception.TerminalProcessorException;

/**
 * This class is wrapper for TerminalProcessorException used to throw terminal exception for input
 * validations at Processor layer
 * 
 * @author 3932968
 *
 */
public class ServiceInputValidationException extends TerminalProcessorException {

    /**
     * auto generated serialVersionUID
     */
    private static final long serialVersionUID = -7972070429833372620L;

    private String errorMessage;
    private ErrorCode errorCode;

    /**
     * @param errorMessage
     */
    public ServiceInputValidationException(String errorMessage) {

        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    /**
     * @param errorCode
     */
    public ServiceInputValidationException(ErrorCode errorCode) {

        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    /**
     * @param errorMessage
     * @param errorCode
     */
    public ServiceInputValidationException(String errorMessage, ErrorCode errorCode) {

        super(errorMessage);
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {

        return errorMessage;
    }

    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(
            String errorMessage) {

        this.errorMessage = errorMessage;
    }

    /**
     * @return the errorCode
     */
    public ErrorCode getShipmentCreationErrorCode() {

        return errorCode;
    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setShipmentCreationErrorCode(
            ErrorCode errorCode) {

        this.errorCode = errorCode;
    }

}
