package com.fedex.rscs.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fedex.common.cxs.dto.CXSEnvelope;
import com.fedex.common.cxs.validation.BeanValidator;
import com.fedex.rscs.dto.ConfirmedShipmentRequest;
import com.fedex.rscs.dto.ConfirmedShipmentResponse;
import com.fedex.rscs.dto.CreateOpenShipmentRequest;
import com.fedex.rscs.dto.CreateOpenShipmentResponse;
import com.fedex.rscs.dto.DeleteOpenShipmentResponse;
import com.fedex.rscs.dto.DeleteSoldShipmentRequest;
import com.fedex.rscs.dto.DeletedSoldShipmentResponse;
import com.fedex.rscs.dto.RetrieveOpenShipmentResponse;
import com.fedex.rscs.dto.ShipmentCheckoutRequest;
import com.fedex.rscs.dto.ShipmentCheckoutResponse;
import com.fedex.rscs.dto.ShipmentQueryType;
import com.fedex.rscs.dto.SoldShipmentResponse;
import com.fedex.rscs.dto.SoldShipmentSummaryResponse;
import com.fedex.rscs.processor.RetailShipmentCreationProcessor;

/**
 * A sample controller for your service. This is an example of a RESTful endpoint used by your
 * application.
 */
@RestController
@RequestMapping("/retailshipping/fedexoffice/v1")
public class RetailShipmentCreationController {

    private static final Logger logger = LoggerFactory.getLogger(RetailShipmentCreationController.class);

    private final BeanValidator validator;
    private final RetailShipmentCreationProcessor processor;

    @Autowired
    public RetailShipmentCreationController(final BeanValidator validator,
            final RetailShipmentCreationProcessor processor) {

        this.validator = validator;
        this.processor = processor;
    }

    /**
     * Method provides endpoint to execute create open shipment
     * 
     * @param request
     * @return
     */
    @PostMapping(value = "/openshipments")
    public ResponseEntity<CXSEnvelope<CreateOpenShipmentResponse>> createOpenShipments(
            @RequestBody final CreateOpenShipmentRequest request) {

        logger.info("A request for create openshipment is in process");

        validator.assertValid(request);

        final CreateOpenShipmentResponse createOpenShipmentResponse = processor.createOpenShipment(request);

        logger.debug("CreateOpenShipment call completed with response as: {}", createOpenShipmentResponse);
        return ResponseEntity.status(HttpStatus.OK)
                .body(CXSEnvelope.success(createOpenShipmentResponse));

    }

    /**
     * Method provides endpoint to execute retrieve open shipment
     * 
     * @param referenceId
     * @param locationId
     * @param trackingId
     * @return
     */
    @GetMapping(value = "/openshipments")
    public ResponseEntity<CXSEnvelope<RetrieveOpenShipmentResponse>> retrieveOpenShipment(
            @RequestParam final String referenceId,
            @RequestParam final String locationId,
            @RequestParam final String trackingId) {

        logger.info("A request for retrieve openshipment is in process");

        final RetrieveOpenShipmentResponse retrieveOpenShipmentResponse =
                processor.retrieveOpenShipment(referenceId, locationId, trackingId);

        logger.debug("RetrieveOpenShipment call completed with response as: {} ", retrieveOpenShipmentResponse);
        return ResponseEntity.status(HttpStatus.OK)
                .body(CXSEnvelope.success(retrieveOpenShipmentResponse));

    }

    /**
     * Method provides endpoint to execute confirmed shipments
     * 
     * @param request
     * @return
     */
    @PostMapping(value = "/confirmedshipments")
    public ResponseEntity<CXSEnvelope<ConfirmedShipmentResponse>> confirmShipments(
            @RequestBody final ConfirmedShipmentRequest request) {

        logger.info("A request for confirmedshipments is in process");

        validator.assertValid(request);

        final ConfirmedShipmentResponse confirmedShipmentResponse = processor.confirmShipments(request);

        logger.debug("ConfirmedShipment call completed with response as: {} ", confirmedShipmentResponse);
        return ResponseEntity.status(HttpStatus.OK)
                .body(CXSEnvelope.success(confirmedShipmentResponse));
    }

    /**
     * Method provides endpoint to execute shipment checkouts
     * 
     * @param request
     * @return
     */
    @PostMapping(value = "/shipmentcheckouts")
    public ResponseEntity<CXSEnvelope<ShipmentCheckoutResponse>> confirmShipmentsCheckout(
            @RequestBody final ShipmentCheckoutRequest request) {

        logger.info("A request for shipmentcheckouts is in process");

        validator.assertValid(request);

        final ShipmentCheckoutResponse shipmentCheckoutResponse = processor.checkoutShipments(request);

        logger.debug("ShipmentCheckout call completed with response as: {} ", shipmentCheckoutResponse);
        return ResponseEntity.status(HttpStatus.OK)
                .body(CXSEnvelope.success(shipmentCheckoutResponse));

    }
    
    /**
     * Method provides endpoint to execute delete open shipment
     * 
     * @param referenceId
     * @param locationId
     * @param trackingId
     * @return
     */
    @DeleteMapping(value = "/openshipments")
    public ResponseEntity<CXSEnvelope<DeleteOpenShipmentResponse>> deleteOpenShipment(
            @RequestParam final String referenceId,
            @RequestParam final String locationId,
            @RequestParam final String trackingId) {

        logger.info("A request for delete openshipment is in process");

        final DeleteOpenShipmentResponse deleteOpenShipmentResponse =
                processor.deleteOpenShipment(referenceId, locationId, trackingId);

        logger.debug("DeleteOpenShipment call completed with response as: {} ", deleteOpenShipmentResponse);
        return ResponseEntity.status(HttpStatus.OK)
                .body(CXSEnvelope.success(deleteOpenShipmentResponse));

    }
    
    /**
     * Method provides endpoint to retrieve sold shipments on basis of tracking or reference id
     * 
     * @param locationId
     * @param id
     * @param type
     * @return
     */
    @GetMapping(value = "/locations/{locationId}/soldshipments")
    public ResponseEntity<CXSEnvelope<SoldShipmentResponse>> retrieveSoldShipments(
            @PathVariable String locationId,
            @RequestParam final String id,
            @RequestParam final ShipmentQueryType type) {

        logger.info("A request for retrieve soldshipments is in process");

        final SoldShipmentResponse soldShipmentResponse = processor.retrieveSoldShipments(locationId, id, type);

        logger.debug("SoldShipment call completed with response as: {} ", soldShipmentResponse);
        return ResponseEntity.status(HttpStatus.OK)
                .body(CXSEnvelope.success(soldShipmentResponse));

    }
    
    /**
     * Method provides endpoint to retrieve sold shipment summaries on basis of operationaldate
     * 
     * @param locationId
     * @param operationaldate
     * @return
     */
    @GetMapping(value = "/locations/{locationId}/soldshipmentsummaries")
    public ResponseEntity<CXSEnvelope<SoldShipmentSummaryResponse>> retrieveSoldShipmentSummaries(
            @PathVariable String locationId,
            @RequestParam final String operationaldate) {

        logger.info("A request for  retrieve soldshipmentsummaries is in process");

        final SoldShipmentSummaryResponse soldShipmentResponse =
                processor.retrieveSoldShipmentSummaries(locationId, operationaldate);

        logger.debug("SoldShipmentSummary call completed with response as: {} ", soldShipmentResponse);
        return ResponseEntity.status(HttpStatus.OK)
                .body(CXSEnvelope.success(soldShipmentResponse));

    }
    
    /**
     * Method provides endpoint to execute delete sold shipments
     * 
     * @param request
     * @return
     */
    @PostMapping(value = "/deletesoldshipments")
    public ResponseEntity<CXSEnvelope<DeletedSoldShipmentResponse>> deleteSoldShipments(
            @RequestBody final DeleteSoldShipmentRequest request) {

        logger.info("A request for deletesoldshipments is in process");

        validator.assertValid(request);

        final DeletedSoldShipmentResponse deletedSoldShipmentResponse = processor.deleteSoldShipments(request);

        logger.debug("DeletedSoldShipment call completed with response as: {} ", deletedSoldShipmentResponse);
        return ResponseEntity.status(HttpStatus.OK)
                .body(CXSEnvelope.success(deletedSoldShipmentResponse));

    }
    
}
