package com.fedex.rscs.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.fedex.common.cxs.dto.CXSEnvelope;
import com.fedex.common.cxs.dto.CXSError;
import com.fedex.common.cxs.exception.TerminalProcessorException;
import com.fedex.rscs.exception.ServiceInputValidationException;
import com.fedex.rscs.exception.ServiceProcessorTerminalException;

/**
 * Custom exception handlers for RetailShipmentCreation live within this class
 */
@ControllerAdvice(basePackageClasses = RetailShipmentCreationController.class)
public class RetailShipmentCreationControllerAdvice {

    private final static Logger logger = LoggerFactory.getLogger(RetailShipmentCreationControllerAdvice.class);

    /**
     * Handle all the TerminalProcessorException Exceptions.
     * 
     * @param ex
     * @return
     */
    @ExceptionHandler(TerminalProcessorException.class)
    public ResponseEntity<CXSEnvelope> handleClientTerminalException(
            final ServiceProcessorTerminalException ex) {

        logger.error("An unsupported operation was encountered during execution", ex);
        HttpStatus status =
                (null != ex.getShipmentCreationErrorCode()) ? HttpStatus.valueOf(ex.getShipmentCreationErrorCode()
                        .getStatus()
                        .name()) : HttpStatus.INTERNAL_SERVER_ERROR;

        final CXSError cxsError = new CXSError(String.valueOf(ex.getShipmentCreationErrorCode()
                .getApiErrorCode()),
                ex.getShipmentCreationErrorCode()
                        .getMessage());

        return ResponseEntity.status(status)
                .body(CXSEnvelope.error(cxsError));
    }

    /**
     * Handle all the validateEndpoint(ServiceInputValidationException) Exceptions.
     * 
     * @param ex
     * @return
     */
    @ExceptionHandler(ServiceInputValidationException.class)
    public ResponseEntity<CXSEnvelope> handleServiceInputValidationException(
            final ServiceInputValidationException ex) {

        logger.error("An unsupported operation was encountered during execution", ex);

        HttpStatus status = (null != ex.getShipmentCreationErrorCode()) ? HttpStatus.valueOf(ex.getShipmentCreationErrorCode()
                .getStatus()
                .name()) : HttpStatus.INTERNAL_SERVER_ERROR;

        final CXSError cxsError = new CXSError(ex.getShipmentCreationErrorCode()
                .getApiErrorCode(),
                ex.getShipmentCreationErrorCode()
                        .getMessage());

        final CXSEnvelope cxsEnvelope = CXSEnvelope.error(cxsError);

        return ResponseEntity.status(status)
                .body(cxsEnvelope);
    }
}
