package com.fedex.rscs.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller to provide the end point for health check.
 * 
 * @author 3802280
 */
@RestController
public class HealthCheckController {

    @RequestMapping(value = "/health", method = RequestMethod.HEAD)
    public ResponseEntity<ServerStatus> respondUp() {

        return ResponseEntity.status(HttpStatus.OK)
                .body(new ServerStatus());
    }

    @GetMapping(value = "/health")
    public ResponseEntity<ServerStatus> respondUpForGet() {

        return ResponseEntity.status(HttpStatus.OK)
                .body(new ServerStatus());
    }

    private static class ServerStatus {

        String status = "UP";

        public String getStatus() {

            return status;
        }
    }
}
