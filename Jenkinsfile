library 'reference-pipeline'
library 'AppServiceAccount'
pipeline {
   agent {
        label 'Weblogic'
    }
    parameters {         
                
        choice(name: 'CF_ENV', choices: 'development\nrelease\nperformance\nstaging\nproduction', description: 'Target Environment')
        string(defaultValue: "git@gitlab.prod.fedex.com:APP3531582/retail-shipment-creation-service", description: 'Source Code URL', name: 'sourceCodeUrl')
        choice(name: 'gitBranch', choices: 'develop\nmaster', description: 'git Branch')
        string(defaultValue: "", description: 'Deploy Version', name: 'deploymentVersion')
        booleanParam(defaultValue: false, description: 'Do you want to Deploy a particular Artifact Version?', name: 'DeploySelectedVersion')
        booleanParam(defaultValue: false, description: 'Do you want to enable qaulity gate', name: 'enableQualityGate')
        booleanParam(defaultValue: false, description: 'Do you want to enable fortify', name: 'enableFortify')        
        string(defaultValue: "shashank.jain.osv@fedex.com, sriram.goteti@fedex.com, ankit.kashyap.osv@fedex.com,vijender.singh.osv@fedex.com", description: 'Deployment Approver Email ID', name: 'deploymentApproverEmail')
        string(defaultValue: "981840,482764,973901,5034922", description: 'Deployment Approver ID', name: 'deploymentApproverId')
        string(defaultValue: "RT-ShipDev@corp.ds.fedex.com, RT-ShipOffshore_Vikings_@corp.ds.fedex.com", description: 'Success Email List', name: 'successEmail')
        string(defaultValue: "RT-ShipDev@corp.ds.fedex.com, RT-ShipOffshore_Vikings_@corp.ds.fedex.com", description: 'Failure Email List', name: 'errorEmail')        
        }
    options {
        buildDiscarder(logRotator(numToKeepStr: '10'))
    }
    tools {
        jdk 'JAVA_8'
        maven 'Maven 3.3.9'
    }
    environment {
        GIT_URL = "git@gitlab.prod.fedex.com:APP3531582/retail-shipment-creation-service"
        CF_APP_NAME = "retail-shipment-creation-service"
        CF_PAM_ID = "1756001"
        EAI_NUMBER = "3531582"
        EAI_NAME = "retail-shipment-creation-service"
        APP_CM_EMP_IDS="${params.deploymentApproverId}"
        NEXUS_CREDS_ID="jenkins-nexus-credentials"
        TAZ_CLIENT_ID = credentials('TAZ_CLIENT_ID')
        JMS_USER_NAME = credentials('JMS_USERNAME')
        JMS_PASSWORD_DEV = credentials('JMS_PASSWORD')
        JMS_URL_DEV = credentials('JMS_PROVIDER_URL')
        JMS_PASSWORD_TEST = credentials('JMS_PASSWORD')
        JMS_URL_TEST = credentials('JMS_PROVIDER_URL')
        TAZ_DEV_TOKEN_URI = credentials('TAZ_DEV_TOKEN_URI')
        TAZ_TEST_TOKEN_URI = credentials('TAZ_TEST_TOKEN_URI')
        TAZ_STAGE_TOKEN_URI = credentials('TAZ_STAGE_TOKEN_URI')
        TAZ_PROD_TOKEN_URI = credentials('TAZ_PROD_TOKEN_URI')
        TAZ_DEV_CLIENT_SECRET = credentials('TAZ_DEV_CLIENT_SECRET')
        TAZ_TEST_CLIENT_SECRET = credentials('TAZ_TEST_CLIENT_SECRET')
        TAZ_STAGE_CLIENT_SECRET = credentials('TAZ_STAGING_CLIENT_SECRET')
        TAZ_PROD_CLIENT_SECRET = credentials('TAZ_PROD_CLIENT_SECRET')
        RSCS_DB_URL_DEV = credentials('RSCS_DB_URL_DEV')		
        RSCS_DB_URL_TEST = credentials('RSCS_DB_URL_TEST')
        RSCS_DB_USERNAME_DEV = credentials('RSCS_DB_USERNAME_DEV')        		
        RSCS_DB_USERNAME_TEST = credentials('RSCS_DB_USERNAME_TEST')		
        RSCS_DB_PASSWORD_DEV = credentials('RSCS_DB_PASSWORD_DEV')        		
        RSCS_DB_PASSWORD_TEST = credentials('RSCS_DB_PASSWORD_TEST')
        RSCS_DB_URL_STAGE = credentials('RSCS_DB_URL_STAGE')
        RSCS_DB_USERNAME_STAGE = credentials('RSCS_DB_USERNAME_STAGE')
        RSCS_DB_PASSWORD_STAGE = credentials('RSCS_DB_PASSWORD_STAGE')
        RSCS_DB_URL_PROD = credentials('RSCS_DB_URL_PROD')
        RSCS_DB_USERNAME_PROD = credentials('RSCS_DB_USERNAME_PROD')
        RSCS_DB_PASSWORD_PROD = credentials('RSCS_DB_PASSWORD_PROD')
        APP_VERSION = ""
        GIT_BRANCH = "${params.gitBranch}"
        APP_GROUP = ""
        JAR_PATH = ""
        UPS_PROPERTIES = ""
        NEXUS_REPO = ""
        RELEASE_FLAG = false
        APPD_USER = ""
        APPD_PLAN = "fedex1-test"
        APPD_AGENT_APP_NAME =""
        APP_NAME_FOR_APPD = "FXO-retail-shipment-creation-service-3531582"
        REPLICAS = 1
        ENV_PROPERTIES = ""
        CUPS_PROPERTIES = ""
        RSCS_P12_KEYSTORE_PWD_DEV = credentials('RSCS_P12_KEYSTORE_PWD_DEV')        		
        RSCS_DEV_FRAMEWORK_CLIENT_CREDENTIAL_DEV = credentials('RSCS_DEV_FRAMEWORK_CLIENT_CREDENTIAL_DEV')                  
        RSCS_P12_KEYSTORE_PWD_TEST = credentials('RSCS_P12_KEYSTORE_PWD_TEST')        		
        RSCS_DEV_FRAMEWORK_CLIENT_CREDENTIAL_TEST = credentials('RSCS_DEV_FRAMEWORK_CLIENT_CREDENTIAL_TEST')
        RSCS_P12_KEYSTORE_PWD_STAGE = credentials('RSCS_P12_KEYSTORE_PWD_STAGE')        		
        RSCS_DEV_FRAMEWORK_CLIENT_CREDENTIAL_STAGE = credentials('RSCS_DEV_FRAMEWORK_CLIENT_CREDENTIAL_STAGE')
	    RSCS_P12_KEYSTORE_PWD_PROD = credentials('RSCS_P12_KEYSTORE_PWD_STAGE')        		
        RSCS_DEV_FRAMEWORK_CLIENT_CREDENTIAL_PROD = credentials('RSCS_DEV_FRAMEWORK_CLIENT_CREDENTIAL_STAGE')
    }
    stages {
        stage('Initialize') {
            steps{
                checkout([$class: 'GitSCM', branches: [[name: "*/${params.gitBranch}"]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'WipeWorkspace']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'fxo-rscs', url: params.sourceCodeUrl]]])
                    script {
                        def readpom = readMavenPom file: ''
                        version = readpom.version;
                        groupId = readpom.groupId;
                        APP_VERSION  = "${version}"
                        APP_GROUP = "com.fedex.office"
                        println "Artifact Version from pom ************************* ${version}"
                        println "Artifact groupId from pom ************************* ${groupId}"
                        println "App version is ${APP_VERSION}"
                        println "Group is ${APP_GROUP}"
                        
                        if(GIT_BRANCH.contains('master')) {
                            NEXUS_REPO = "release"
                            NEXUS_VERSION="${APP_VERSION}-RELEASE"
                            RELEASE_FLAG = true
                            if (params.DeploySelectedVersion) {
                                NEXUS_VERSION = params.deploymentVersion
                                println "Selected Nexus Version for deployment is --> ${NEXUS_VERSION}"
                            }
                        }else{
                            NEXUS_REPO = "snapshot"
                            NEXUS_VERSION="${APP_VERSION}-SNAPSHOT"
                            }
                        println "Nexus Repo is ${NEXUS_REPO}"
                        println "Nexus Version is ${NEXUS_VERSION}"
                    }
            }
        }
         stage('Build') {
             when {
                 anyOf {
                     environment name: 'CF_ENV', value: 'development'
                     environment name: 'CF_ENV', value: 'release'
                     branch 'develop'
                     branch 'master'
                 }
                 not {
                     anyOf {
                         environment name: 'CF_ENV', value: 'staging'
                         environment name: 'CF_ENV', value: 'production'
                     }
                 }
             }
             steps {
                 println "Building source from branch ${GIT_BRANCH}"
                 withCredentials([certificate(credentialsId: 'CERTIFICATE_APP3531582_TEST', keystoreVariable: 'CERT_FILE_TEST'), 
                 certificate(credentialsId: 'CERTIFICATE_APP3531582_PROD', keystoreVariable: 'CERT_FILE_PROD')] ) {
                   sh 'echo $CERT_FILE_TEST'
                   sh 'echo $CERT_FILE_PROD'
                   sh 'mvn clean package -s settings.xml'
                 }
             }
         }
 
     stage('SonarQube & NexusIQ') {
            parallel {
                stage('SonarQube') {
                     when {
                         anyOf {
                             environment name: 'CF_ENV', value: 'development'
                             environment name: 'CF_ENV', value: 'release'
                             branch 'develop'
                             branch 'master'
                         }
                         not {
                             anyOf {
                                 environment name: 'CF_ENV', value: 'staging'
                                 environment name: 'CF_ENV', value: 'production'
                             }
                         }
                     }
                     steps {
                       script {
                         println "Running SonarQube"
                         println "APP Name    ************************* ${CF_APP_NAME}"
                         println "APP Group   ************************* ${groupId}"
                         println "App Version ************************* ${APP_VERSION}"
                         sonarqube projectName: '${CF_APP_NAME}',
                             projectKey: "${groupId}:${CF_APP_NAME}",
                             projectVersion: "${APP_VERSION}-SNAPSHOT",
                             src: 'src/main/java',
                             test: 'src/test',
                             binaries: 'target/classes',
                             repo: 'git',
                             scmDisabled: 'false',
                             exclusions: '**/*com/fedex/common/cxs/**,**/*src/main/resources/**,**/*com/fedex/rscs/config/**,**/*RetailShipmentCreationControllerAdvice.java,**/*HealthCheckController.java,**/*src/test/resources/**,**/*src/it/**,**/*com/fedex/rscs/client/mapper/**,**/*com/fedex/rscs/exception/**,**/*com/fedex/rscs/client/**/**Mock**,**/*com/fedex/rscs/dto/**,**/*com/fedex/rscs/**/dto/**,**/*com/fedex/rscs/model/**,**/*com/fedex/rscs/repository/**Mock**,**/*com/fedex/rscs/client/**/**Mock**,**/*com/fedex/rscs/client/**/dto/**,**/*com/fedex/rscs/repository/mapper/**,**/*com/fedex/rscs/service/TransactionRouter.java,**/*com/fedex/rscs/client/pes/mapper/**,**/*com/fedex/rscs/service/OpenShipmentTransactionMapper.java,**/*com/fedex/rscs/service/ConfirmShipmentTransactionMapper.java,**/*com/fedex/rscs/RetailShipmentCreationApplication.java,**/*com/fedex/rscs/processor/RetailShipmentCreationMapper.java'
                     }
                     }
                 }
                 stage('NexusIQ') {
                     when {
                         anyOf {
                             environment name: 'CF_ENV', value: 'development'
                             environment name: 'CF_ENV', value: 'release'
                             branch 'develop'
                             branch 'master'
                         }
                         not {
                             anyOf {
                                 environment name: 'CF_ENV', value: 'staging'
                                 environment name: 'CF_ENV', value: 'production'
                             }
                         }
                     }
                     steps {
                        nexusPolicyEvaluation iqApplication: "RSCS-3531582", iqStage: 'build'
                     }
                 }
             }
        }
        
        stage('RelyBP Sonar Scanner - Static Validations') {
                     when {
                         anyOf {
                             environment name: 'CF_ENV', value: 'development'
                             environment name: 'CF_ENV', value: 'release'
                             branch 'develop'
                             branch 'master'
                         }
                         not {
                             anyOf {
                                 environment name: 'CF_ENV', value: 'staging'
                                 environment name: 'CF_ENV', value: 'production'
                             }
                         }
                     }
                     steps {
                       script {
                         println "Running SonarQube - Rely BP Statc Validation"
                         sonarqube (
                             projectName: '${CF_APP_NAME}',
                             projectKey: "${groupId}:${CF_APP_NAME}",
                             projectVersion: "${APP_VERSION}-SNAPSHOT",
                             src: 'src/main/java',
                             test: 'src/test',
                             binaries: 'target/classes',
                             eai: '${EAI_NUMBER}',
                             enableRelyBP: "true",
                             exclusions: '**/*com/fedex/common/cxs/**,**/*src/main/resources/**,**/*com/fedex/rscs/config/**,**/*RetailShipmentCreationControllerAdvice.java,**/*HealthCheckController.java,**/*src/test/resources/**,**/*src/it/**,**/*com/fedex/rscs/client/mapper/**,**/*com/fedex/rscs/exception/**,**/*com/fedex/rscs/client/**/**Mock**,**/*com/fedex/rscs/dto/**,**/*com/fedex/rscs/**/dto/**,**/*com/fedex/rscs/model/**,**/*com/fedex/rscs/repository/**Mock**,**/*com/fedex/rscs/client/**/**Mock**,**/*com/fedex/rscs/client/**/dto/**,**/*com/fedex/rscs/repository/mapper/**,**/*com/fedex/rscs/service/TransactionRouter.java,**/*com/fedex/rscs/client/pes/mapper/**,**/*com/fedex/rscs/service/OpenShipmentTransactionMapper.java,**/*com/fedex/rscs/service/ConfirmShipmentTransactionMapper.java,**/*com/fedex/rscs/RetailShipmentCreationApplication.java,**/*com/fedex/rscs/processor/RetailShipmentCreationMapper.java'
                                )
                         }
                     }
                 }    
                 
      stage('Quality Gate - Accept the Default') {
        when {             
                expression {return params.enableQualityGate ==~ /true/ && params.CF_ENV ==~ /development|release/ }
            }
         steps {
             sonarQualityGate()
            }
       }
      
      stage('stashing source ') {
          when {             
                expression {return params.enableFortify ==~ /true/ && params.CF_ENV ==~ /development/ }
            }        
         steps {
             stash includes: '**', name: 'source'
             }
        }
      
      stage("Get Fortify Scripts"){
        agent {label 'master'}
         when {             
                expression {return params.enableFortify ==~ /true/ && params.CF_ENV ==~ /development/ }
            }
         steps {
             getFortifyScripts()
             }
         }
      stage('Run Fortify Analysis') { 
          when {             
                expression {return params.enableFortify ==~ /true/ && params.CF_ENV ==~ /development/ }
            }
         steps {
             startFortifyAnalysis("${EAI_NUMBER}_${EAI_NAME}")
            }
        } 
      
      stage('Nexus Staging') {
         when {
             anyOf {  
                 environment name: 'CF_ENV', value: 'development'              
                 environment name: 'CF_ENV', value: 'release'
                 branch 'develop'
                 branch 'master'
            }
            not {
                 anyOf {
                     environment name: 'CF_ENV', value: 'staging'
                     environment name: 'CF_ENV', value: 'production'
                 }
             }
            
         }
         steps{
             println "Uploading jar to Nexus ${CF_APP_NAME}"
             println "cred: ${NEXUS_CREDS_ID}"
             println "EAI: ${EAI_NUMBER}"
             println "Group: ${APP_GROUP}"
             println "Nexus Repo: ${NEXUS_REPO}"
             println "Nexus Version ${NEXUS_VERSION}"
             nexusArtifactUploader artifacts: [[artifactId: "${CF_APP_NAME}", classifier: '', file: "target/${CF_APP_NAME}-${APP_VERSION}.jar", type: 'jar']],
             credentialsId: "${NEXUS_CREDS_ID}",
             groupId: "eai${EAI_NUMBER}.${APP_GROUP}",
             nexusUrl: 'nexus.prod.cloud.fedex.com:8443/nexus',                   
             nexusVersion: 'nexus3',
             protocol: 'https',
             repository: "${NEXUS_REPO}",
             version: "${NEXUS_VERSION}"
         }
        
     }
    
    stage('Nexus Pull') {
        steps{
            println "Downloading from nexus repo..."
                script{
                    downloadNexusArtifact groupId: "eai${EAI_NUMBER}.${APP_GROUP}",
                    artifactId: "${CF_APP_NAME}",
                    repo:"${NEXUS_REPO}",
                    release: "${RELEASE_FLAG}".toBoolean(),
                    extension: "jar",
                    version: "${NEXUS_VERSION}",
                    downloadFileName: "${CF_APP_NAME}.jar"
                }
            }
    }
        stage('Deploy to Development CLWDEV1') {
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'development'
                    branch 'develop'
                }
            }
            steps {
                deployToPCF("development", "clwdev1", "FXO-retail-shipment-creation-service-3531582")
                addNewRoute("clwdev1","${CF_ENV}","${CF_APP_NAME}", "app.clwdev1.paas.fedex.com")               
                disableGSLBRoute("clwdev1","${CF_ENV}","${CF_APP_NAME}")
                executeRelyDynamicScan("clwdev1","${CF_ENV}","${CF_APP_NAME}")
                executeIntegrationTests("${TAZ_CLIENT_ID}","${TAZ_DEV_CLIENT_SECRET}")
            }
        }
        stage('Deploy to Development CLWDEV2') {
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'development'
                    branch 'develop'
                }
            }
            steps {
                deployToPCF("development", "clwdev2", "FXO-retail-shipment-creation-service-3531582")
                addNewRoute("clwdev2","${CF_ENV}","${CF_APP_NAME}", "app.clwdev2.paas.fedex.com")
                disableGSLBRoute("clwdev1","${CF_ENV}","${CF_APP_NAME}")
            }
        }
        stage('Deploy to Development CLWDEV3') {
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'development'
                    branch 'develop'
                }
            }
            steps {
                deployToPCF("development", "clwdev3", "FXO-retail-shipment-creation-service-3531582")
                addNewRoute("clwdev3","${CF_ENV}","${CF_APP_NAME}", "app.clwdev3.paas.fedex.com")
                disableGSLBRoute("clwdev1","${CF_ENV}","${CF_APP_NAME}")
            }
        }
		stage('approval for release space') {
            agent none
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'release'
                }
            }
            options {
                timeout(time: 5, unit: 'MINUTES')
            }
            steps {
                script {
                    input(
                        message: "Do you want to deploy in the $CF_ENV environment?",
                        submitter: "$APP_CM_EMP_IDS"
                    )
                    echo ("Confirmation Accepted")
                }
            }
        }
        stage('Deploy to Release CLWDEV1') {
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'release'
                    branch 'master'
                }
                not {
                    anyOf {
                        environment name: 'CF_ENV', value: 'staging'
                        environment name: 'CF_ENV', value: 'production'
                    }
                }
            }
            steps {
                    deployToPCF("release", "clwdev1", "FXO-retail-shipment-creation-service-CLW1-3531582")
                    executeRelyDynamicScan("clwdev1","${CF_ENV}","${CF_APP_NAME}")
            }
        }
        stage('Deploy to Release CLWDEV2') {
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'release'
                    branch 'master'
                }
                not {
                    anyOf {
                        environment name: 'CF_ENV', value: 'staging'
                        environment name: 'CF_ENV', value: 'production'
                    }
                }
            }
            steps {
                    deployToPCF("release", "clwdev2", "FXO-retail-shipment-creation-service-CLW2-3531582")
            }
        }
        stage('Deploy to Release CLWDEV3') {
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'release'
                    branch 'master'
                }
                not {
                    anyOf {
                        environment name: 'CF_ENV', value: 'staging'
                        environment name: 'CF_ENV', value: 'production'
                    }
                }
            }
            steps {
                    deployToPCF("release", "clwdev3", "FXO-retail-shipment-creation-service-CLW3-3531582")
            }
        }
        stage('Deploy to performance CLWDEV1') {
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'performance'
                    branch 'master'
                }
                not {
                    anyOf {
                        environment name: 'CF_ENV', value: 'staging'
                        environment name: 'CF_ENV', value: 'production'
                    }
                }
            }
            steps {
                    deployToPCF("performance", "clwdev1", "FXO-retail-shipment-creation-service-CLW1-3531582")
            }
        }
        stage('approval for Staging space') {
            agent none
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'staging'
                }
            }
            options {
                timeout(time: 5, unit: 'MINUTES')
            }
            steps {
                script {
                    input(
                        message: "Do you want to deploy in the $CF_ENV environment?",
                        submitter: "$APP_CM_EMP_IDS"
                    )
                    echo ("Confirmation Accepted")
                }
            }
        }
        stage('Deploy to Staging on CLW1 ') {
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'staging'
                }
            }
            steps {
                deployToPCF("staging", "clwcc1", "FXO-retail-shipment-creation-service-CLW1-3531582")
            }
        }
        stage('Deploy to Staging on CLW2 ') {
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'staging'
                }
            }
            steps {
                deployToPCF("staging", "clwcc2", "FXO-retail-shipment-creation-service-CLW2-3531582")
            }
        }
        stage('Deploy to Staging on CLW3 ') {
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'staging'
                }
            }
            steps {
                deployToPCF("staging", "clwcc3", "FXO-retail-shipment-creation-service-CLW3-3531582")
            }
        }
        stage('Deploy to Staging on wtccf2') {
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'staging'
                }
            }
            steps {
                deployToPCF("staging", "wtccf2", "FXO-retail-shipment-creation-service-WAP-3531582")
            }
        }
        stage('approval for production') {
            agent none
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'production'
                }
            }
            options {
                timeout(time: 5, unit: 'MINUTES')
            }
            steps {
                script {
                    input(
                        message: "Do you want to deploy in the $CF_ENV environment?",
                        submitter: "$APP_CM_EMP_IDS"
                    )
                    echo ("Confirmation Accepted")
                }
            }
        }
        stage('Deploy to Production on wtccf2') {
           environment {
                    CF_APP_NAME = "retail-shipment-creation-service"
           }
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'production'
                }
            }
            steps {
                deployToPCF("production", "wtccf2", "FXO-retail-shipment-creation-service-WAP-3531582")
                updateProdRoute("wtccf2")
            }
        }
        stage('Deploy to Production on clwcc1') {
           environment {
                    CF_APP_NAME = "retail-shipment-creation-service"
           }
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'production'
                }
            }
            steps {
                deployToPCF("production", "clwcc1", "FXO-retail-shipment-creation-service-WAP-3531582")
                updateProdRoute("clwcc1")
            }
        }
        stage('Deploy to Production on CLW2') {
            environment {
                    CF_APP_NAME = "retail-shipment-creation-service"
           }
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'production'
                }
            }
            steps {
                deployToPCF("production", "clwcc2", "FXO-retail-shipment-creation-service-CLW2-3531582")
                updateProdRoute("clwcc2")
            }
        }
        stage('Deploy to Production on CLW3') {
            environment {
                    CF_APP_NAME = "retail-shipment-creation-service"
           }
            when {
                anyOf {
                    environment name: 'CF_ENV', value: 'production'
                }
            }
            steps {
                deployToPCF("production", "clwcc3", "FXO-retail-shipment-creation-service-CLW3-3531582")
                updateProdRoute("clwcc3")
            }
        }

    }
    post {
        success {
            mail(to: params.successEmail, subject: "Pipeline successful for ${CF_APP_NAME}  deployment in ${CF_ENV} space", body: "${CF_APP_NAME} Pipeline is successfully executed. - ${BUILD_URL}")
        }
        failure {
           mail(to: params.errorEmail, subject: "Pipeline Failed for ${CF_APP_NAME}  deployment in ${CF_ENV} space", body: "Something is wrong with ${CF_APP_NAME} Pipeline. - ${BUILD_URL}")
        }
    }  
}
def deployToPCF(String cloudSpace, String foundation, String appDAppName) {
    CF_ENV = cloudSpace
    APP_NAME_FOR_APPD = appDAppName
    JAR_PATH = "${EAI_NAME}.jar"
    CF_API = "https://api.sys.${foundation}.paas.fedex.com"
    if(CF_ENV.equalsIgnoreCase('development')){
        CF_LEVEL = "dev"
        APPD_AGENT_APP_NAME = "OFFICE-L1"
        CF_APP_NAME="3531582-retail-shipment-creation-service-${foundation}"
        ENV_PROPERTIES = ["spring.cloud.config.label":"develop","spring.cloud.config.profile":"dev","spring.cloud.config.uri":"https://context-service-dev.app.wtcdev2.paas.fedex.com", "JBP_CONFIG_CONTAINER_SECURITY_PROVIDER":"{key_manager_enabled: false}","spring.profiles.active":"dev, cloud, noauth"]
        CUPS_PROPERTIES = ["credential-ups": ["tazClientId": "${TAZ_CLIENT_ID}", "tazClientSecret" :"${TAZ_DEV_CLIENT_SECRET}","tazTokenURI" :"${TAZ_DEV_TOKEN_URI}", "dbUrl" :"${RSCS_DB_URL_DEV}", "dbUsername" :"${RSCS_DB_USERNAME_DEV}", "dbPassword" :"${RSCS_DB_PASSWORD_DEV}", "p12KeystorePwd" :"${RSCS_P12_KEYSTORE_PWD_DEV}", "frameworkClientCredential" :"${RSCS_DEV_FRAMEWORK_CLIENT_CREDENTIAL_DEV}", "jmsProviderUrl" :"${JMS_URL_DEV}", "jmsUsername" :"${JMS_USER_NAME}", "jmsPassword" :"${JMS_PASSWORD_DEV}"]]
    }else if(CF_ENV.equalsIgnoreCase('release')){
        CF_LEVEL = "test"
        APPD_AGENT_APP_NAME = "OFFICE-L3"
        ENV_PROPERTIES = ["spring.cloud.config.label":"master","spring.cloud.config.profile":"release","spring.cloud.config.uri":"https://context-service-test.devapp.paas.fedex.com","JBP_CONFIG_CONTAINER_SECURITY_PROVIDER":"{key_manager_enabled: false}","spring.profiles.active":"release, cloud"]
        CUPS_PROPERTIES = ["credential-ups": ["tazClientId": "${TAZ_CLIENT_ID}", "tazClientSecret" :"${TAZ_TEST_CLIENT_SECRET}","tazTokenURI" :"${TAZ_TEST_TOKEN_URI}", "dbUrl" :"${RSCS_DB_URL_TEST}", "dbUsername" :"${RSCS_DB_USERNAME_TEST}", "dbPassword" :"${RSCS_DB_PASSWORD_TEST}", "p12KeystorePwd" :"${RSCS_P12_KEYSTORE_PWD_TEST}", "frameworkClientCredential" :"${RSCS_DEV_FRAMEWORK_CLIENT_CREDENTIAL_TEST}", "jmsProviderUrl" :"${JMS_URL_TEST}", "jmsUsername" :"${JMS_USER_NAME}", "jmsPassword" :"${JMS_PASSWORD_TEST}"]]
    }else if(CF_ENV.equalsIgnoreCase('performance')){
        CF_APP_NAME= "retail-shipping-creation-service-dev"
    	CF_ENV = "release"
        CF_LEVEL = "perf"
        APPD_AGENT_APP_NAME = "OFFICE-L4"        
        ENV_PROPERTIES = ["spring.cloud.config.label":"master","spring.cloud.config.uri":"https://package-configuration-service-dev-perf.app.wtcdev1.paas.fedex.com","JBP_CONFIG_CONTAINER_SECURITY_PROVIDER":"{key_manager_enabled: false}","spring.profiles.active":"cloud"]
        CUPS_PROPERTIES = ["credential-ups": ["tazClientId": "${TAZ_CLIENT_ID}", "tazClientSecret" :"${TAZ_TEST_CLIENT_SECRET}","tazTokenURI" :"${TAZ_TEST_TOKEN_URI}", "dbUrl" :"${RSCS_DB_URL_TEST}", "dbUsername" :"${RSCS_DB_USERNAME_TEST}", "dbPassword" :"${RSCS_DB_PASSWORD_TEST}", "p12KeystorePwd" :"${RSCS_P12_KEYSTORE_PWD_TEST}", "frameworkClientCredential" :"${RSCS_DEV_FRAMEWORK_CLIENT_CREDENTIAL_TEST}"]]
    }else if(CF_ENV.equalsIgnoreCase('staging')){
        CF_LEVEL = "staging"
        APPD_AGENT_APP_NAME = "OFFICE-L6"
        APPD_PLAN = "fedex1"
        REPLICAS = 1
        ENV_PROPERTIES = ["spring.cloud.config.label":"master","spring.cloud.config.profile":"staging","spring.cloud.config.uri":"https://context-service-staging.app.paas.fedex.com","JBP_CONFIG_CONTAINER_SECURITY_PROVIDER":"{key_manager_enabled: false}","spring.profiles.active":"staging, cloud"]
        CUPS_PROPERTIES = ["credential-ups": ["tazClientId": "${TAZ_CLIENT_ID}", "tazClientSecret" :"${TAZ_STAGE_CLIENT_SECRET}","tazTokenURI" :"${TAZ_STAGE_TOKEN_URI}", "dbUrl" :"${RSCS_DB_URL_STAGE}", "dbUsername" :"${RSCS_DB_USERNAME_STAGE}", "dbPassword" :"${RSCS_DB_PASSWORD_STAGE}", "p12KeystorePwd" :"${RSCS_P12_KEYSTORE_PWD_STAGE}", "frameworkClientCredential" :"${RSCS_DEV_FRAMEWORK_CLIENT_CREDENTIAL_STAGE}"]]
    }else if(CF_ENV.equalsIgnoreCase('production')){
        CF_LEVEL = "prod"
        APPD_AGENT_APP_NAME = "OFFICE"
        APPD_PLAN = "fedex1"
        REPLICAS = 1
        ENV_PROPERTIES = ["spring.cloud.config.label":"master","spring.cloud.config.profile":"production","spring.cloud.config.uri":"https://context-service.app.paas.fedex.com","JBP_CONFIG_CONTAINER_SECURITY_PROVIDER":"{key_manager_enabled: false}","spring.profiles.active":"prod, cloud"]
        CUPS_PROPERTIES = ["credential-ups": ["tazClientId": "${TAZ_CLIENT_ID}", "tazClientSecret" :"${TAZ_PROD_CLIENT_SECRET}","tazTokenURI" :"${TAZ_PROD_TOKEN_URI}", "dbUrl" :"${RSCS_DB_URL_PROD}", "dbUsername" :"${RSCS_DB_USERNAME_PROD}", "dbPassword" :"${RSCS_DB_PASSWORD_PROD}", "p12KeystorePwd" :"${RSCS_P12_KEYSTORE_PWD_PROD}", "frameworkClientCredential" :"${RSCS_DEV_FRAMEWORK_CLIENT_CREDENTIAL_PROD}"]]
    }

    println 'Deploying to PCF'
    
      pcfBlueGreenDeploy pamId: "${CF_PAM_ID}",
        url: "${CF_API}",
        stage: "${CF_ENV}",
        appName: "${CF_APP_NAME}",
        instances: "${REPLICAS}",
        jarPath: "${JAR_PATH}",
        foundation: "${foundation}",
        appdPlan: "${APPD_PLAN}",
        eaiNum: "${EAI_NUMBER}",
        eaiAppName: "${EAI_NAME}",
        props: ENV_PROPERTIES,
        level: "${CF_LEVEL}",
        gslbName: "${CF_APP_NAME}-${CF_LEVEL}",
        appdAgentAppName: "${APPD_AGENT_APP_NAME}",
        appdServiceName: "${APP_NAME_FOR_APPD}",
        appdAgentTierName: "${APP_NAME_FOR_APPD}",
        extCupsCmd: CUPS_PROPERTIES,
        smoke:{
            def URL = "https://${CF_APP_NAME}-${CF_LEVEL}-tmp.app.${foundation}.paas.fedex.com/health"
            def VerifyText = "UP"
            def SERVICE_STATUS = sh (script: "curl -f -s ${URL} > /dev/null", returnStatus: true)
            echo "$SERVICE_STATUS"
            if( SERVICE_STATUS != 0  )
                error "Unable to connect to service (${SERVICE_STATUS}): ${URL}"
            def SERVICE_BODY = sh ( script: "curl -s ${URL}", returnStdout: true ).trim()
            assert SERVICE_BODY.contains(VerifyText) : "JSON Does not contain '${VerifyText}' for ${URL}"
        }
}
def updateProdRoute(String dcFoundation) {
    CF_API = "https://api.sys.${dcFoundation}.paas.fedex.com"
    env.DC_FOUNDATION = dcFoundation
    PAM_ID = "1756001"       
    CF_ENV = "production"
    println 'Adding additional Production route nad scaling instances after successful deployment...'
    pcfDeploy pamId: "${PAM_ID}",
              url: "${CF_API}",
              space: "${CF_ENV}",
              cfcmd: "--version"
                        
    sh '''#!/bin/bash
        set -x     
        export PATH=${PATH}:${WORKSPACE}
        cf map-route $CF_APP_NAME app.$DC_FOUNDATION.paas.fedex.com -n $CF_APP_NAME
        cf map-route $CF_APP_NAME app.paas.fedex.com -n $CF_APP_NAME
        cf delete-route -f app.$DC_FOUNDATION.paas.fedex.com --hostname $CF_APP_NAME-prod
        cf delete-route -f app.paas.fedex.com --hostname $CF_APP_NAME-prod
        pwd
       ''' 
}

def scaleInstances(String foundation) {
    CF_API = "https://api.sys.${foundation}.paas.fedex.com"
    PAM_ID = "1756001"       
    CF_ENV = "staging"
    println 'Scaling Instances for staging space'
    pcfDeploy pamId: "${PAM_ID}",
              url: "${CF_API}",
              space: "${CF_ENV}",
              cfcmd: "--version"
                        
    sh '''#!/bin/bash
        set -x     
        export PATH=${PATH}:${WORKSPACE}         
        cf scale retail-shipment-creation-service -i 3
        pwd
       ''' 
}

def executeRelyDynamicScan(String foundation,String space,String appName) {
    CF_API = "https://api.sys.${foundation}.paas.fedex.com"
    PAM_ID = "1756001"
    println 'Executing Rely BP dynamic scan for retail-shipment-creation-service in ${space}.'
    pcfRelyDynamic (
        pam_ID: "${PAM_ID}",
        sonar_projectname: "retail-shipment-creation-service",
        url: "${CF_API}",
        space: "${space}",
        appName: "${appName}"
    )                        
}

def disableGSLBRoute(String dcFoundation, String space, String appName) {
    CF_API = "https://api.sys.${dcFoundation}.paas.fedex.com"
    PAM_ID = "1756001"
    env.CF_SPACE = space
    env.CF_APP_NAME = appName       
    println 'Disabling GSLB route for ${CF_APP_NAME} in ${CF_SPACE} environment.'
    pcfDeploy pamId: "${PAM_ID}",
              url: "${CF_API}",
              space: "${CF_SPACE}",
              cfcmd: "--version"

    if(space.equalsIgnoreCase('development')){
        env.CF_LEVEL = "dev"
        env.CF_GSLB_URL = "devapp.paas.fedex.com"
    }else if(space.equalsIgnoreCase('release')){
        env.CF_LEVEL = "test"
        env.CF_GSLB_URL = "devapp.paas.fedex.com"
    }else if(space.equalsIgnoreCase('performance')){
        env.CF_LEVEL = "perf"
        env.CF_GSLB_URL = "devapp.paas.fedex.com"
    }else if(space.equalsIgnoreCase('staging')){
        env.CF_LEVEL = "staging"
    }else if(space.equalsIgnoreCase('production')){
        env.CF_LEVEL = "prod"
        env.CF_GSLB_URL = "app.paas.fedex.com"
    }
                        
    sh '''#!/bin/bash
        set -x     
        export PATH=${PATH}:${WORKSPACE}         
        cf delete-route -f $CF_GSLB_URL --hostname $CF_APP_NAME-$CF_LEVEL
       ''' 
}

def executeIntegrationTests(String tazClientId, String tazDevClientSecret) {

    def itProperties = "-Dconfig.file=integration-test.properties"
    if (!itProperties) {
        itProperties = '-DskipITs'
    }
	
    sh "mvn -s settings.xml verify ${itProperties}  -DtazClientId=${tazClientId}  -DtazDevClientSecret=${tazDevClientSecret} -Dskip.surefire.tests=true -Dit.test=ServiceHealthCheckIT"

}

def addNewRoute(String dcFoundation, String space, String appName, String domain) {
    CF_API = "https://api.sys.${dcFoundation}.paas.fedex.com"
    PAM_ID = "1756001"
    env.CF_SPACE = space
    env.CF_APP_NAME = appName  
    
    if(space.equalsIgnoreCase('development')){
        env.CF_LEVEL = "dev"
        env.CF_GSLB_URL = "devapp.paas.fedex.com"
    }else if(space.equalsIgnoreCase('release')){
        env.CF_LEVEL = "test"
        env.CF_GSLB_URL = "devapp.paas.fedex.com"
    }else if(space.equalsIgnoreCase('performance')){
        env.CF_LEVEL = "perf"
        env.CF_GSLB_URL = "devapp.paas.fedex.com"
    }else if(space.equalsIgnoreCase('staging')){
        env.CF_LEVEL = "staging"
    }else if(space.equalsIgnoreCase('production')){
        env.CF_LEVEL = "prod"
        env.CF_GSLB_URL = "app.paas.fedex.com"
    }    

   String command = "map-route ${CF_APP_NAME} ${domain} -n ${EAI_NAME}-${CF_LEVEL}"
   String mapGSLBRoute = "map-route $CF_APP_NAME $CF_GSLB_URL -n ${EAI_NAME}-$CF_LEVEL"
   String deleteAdditionalGSLBRoute= "delete-route -f $CF_GSLB_URL --hostname $CF_APP_NAME-$CF_LEVEL"
   String deleteRoute= "delete-route -f ${domain} --hostname $CF_APP_NAME-$CF_LEVEL"
   String[] additionalCommands
   
   additionalCommands = [
                    mapGSLBRoute,
                    deleteAdditionalGSLBRoute,
                    deleteRoute]   
   
   println command

   pcfDeploy  pamId: "${PAM_ID}",
              url: "${CF_API}",
              space: "${CF_SPACE}",
              cfcmd: command,
              extCfCmd: additionalCommands
              
}

