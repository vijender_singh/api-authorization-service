# Self Service Backout Plan

If there is an issue, the previous version will be deployed from Nexus to PCF (see `ci` for Jenkins details).

However, if possible, a fail-forward approach should be used.


Option 1: If blue/green deployment patter is used:
	**All below steps must be ideally be run inside the smoketests sub-pipeline when failure is detected.**
* 	Ensure the revert any rename of the original application. For example if the app was renamed as 'retail-shipment-creation-service-old', then the rollback step would rename that back to 'retail-shipment-creation-service'
* 	Also remember to unmap any temporary route for the original app
* 	Delete the temporary route(s)
* 	Cleanup the new application by deleting the application from PCF foundation
* 	Unmap and delete any routes for the new app
* 	Delete any temporary services if created and applicable
*   Reach out to 981840@fedex.com,973901@fedex.com


Option 2: If the deployment was NOT blue/green, the rollback process is manual:
* 	Delete the application and cleanup all routes, pertinent service in PCF
* 	Download the previous artifact from the artifact repository. Hopefully it was already tagged 
* 	Initiate any database rollbacks if applicable
* 	Redeploy the application to PCF
* 	Trigger the smoke tests to validate the environment
